package com.localvalu.merchant.blueunitt1n;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.localvalu.merchant.blueunitt1n.activity.DashBoardActivity;
import com.localvalu.merchant.blueunitt1n.activity.WebActivity;
import com.utils.base.AppBaseActivity;

public class ActivityConfig implements LocalValuConstants
{

    public static void startDashBoardActivity(Activity activity) {
        Intent intent = new Intent(activity, DashBoardActivity.class);
        intent.putExtra(AppBaseActivity.EXTRA_OVERRIDE_TRANSITION, false);
        activity.startActivity(intent);
        activity.finish();
    }

    public static void startWebActivity(Activity activity, String title, String url) {
        Bundle bundle = new Bundle();
        bundle.putString(EXTRA_TITLE, title);
        bundle.putString(EXTRA_URL, url);

        Intent intent = new Intent(activity, WebActivity.class);
        intent.putExtra(AppBaseActivity.EXTRA_OVERRIDE_TRANSITION, false);
        intent.putExtras(bundle);
        activity.startActivity(intent);
    }

    public static void startPhoneCallActivity(Activity activity, String mobileNumber) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + mobileNumber));
        activity.startActivity(intent);
    }

    public static void startEmailActivity(Activity activity, String emailAddress) {
        Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:" + emailAddress));
        intent.putExtra(Intent.EXTRA_SUBJECT, "");
        intent.putExtra(Intent.EXTRA_TEXT, "");
//        intent.putExtra(Intent.EXTRA_HTML_TEXT, "");    // If you are using HTML in your body text
//        activity.startActivity(Intent.createChooser(intent, "Chooser Title"));
        activity.startActivity(intent);
    }

    public static void startShareActivity(Activity activity, String data) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_TEXT, data);
        intent.setType("text/plain");
        activity.startActivity(intent);
    }

    public static void startWebActivity(Activity activity, String url) {
        Intent sendIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        activity.startActivity(sendIntent);
    }
}
