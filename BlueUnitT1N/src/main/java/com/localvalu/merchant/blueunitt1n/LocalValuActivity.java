package com.localvalu.merchant.blueunitt1n;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.localvalu.coremerchantapp.BuildConfig;
import com.localvalu.coremerchantapp.data.base.RestBasedErrorDetail;
import com.localvalu.coremerchantapp.data.base.RestBasedOutput;
import com.localvalu.coremerchantapp.data.input.InsertVisitEntryInput;
import com.localvalu.coremerchantapp.data.input.MyTransactionInput;
import com.localvalu.coremerchantapp.data.input.OrderDetailsInput;
import com.localvalu.coremerchantapp.data.input.OrderListInput;
import com.localvalu.coremerchantapp.data.input.OrderStatusChangeInput;
import com.localvalu.coremerchantapp.data.input.SaleTransactionInput;
import com.localvalu.coremerchantapp.data.input.SignInInput;
import com.localvalu.coremerchantapp.data.input.TableBookingListInput;
import com.localvalu.coremerchantapp.data.input.TableBookingStatusChangeInput;
import com.localvalu.coremerchantapp.data.input.TokenBalanceInput;
import com.localvalu.coremerchantapp.data.input.TransferLoyaltyTokenInput;
import com.localvalu.coremerchantapp.data.input.VisitEntryHistoryInput;
import com.localvalu.coremerchantapp.data.output.MyTransactionOutput;
import com.localvalu.coremerchantapp.data.output.MyTransactionResult;
import com.localvalu.coremerchantapp.data.output.SaleTransactionOutput;
import com.localvalu.coremerchantapp.data.output.SaleTransactionResult;
import com.localvalu.coremerchantapp.data.output.SignInOutput;
import com.localvalu.coremerchantapp.data.output.SignInResult;
import com.localvalu.coremerchantapp.data.output.TokenBalanceOutput;
import com.localvalu.coremerchantapp.data.output.TokenBalanceResult;
import com.localvalu.coremerchantapp.data.output.TransferLoyaltyTokenOutput;
import com.localvalu.coremerchantapp.data.output.TransferLoyaltyTokenResult;
import com.localvalu.coremerchantapp.data.output.order.OrderDetailsResult;
import com.localvalu.coremerchantapp.data.output.order.OrderDetailsResultOutput;
import com.localvalu.coremerchantapp.data.output.order.OrderListOutput;
import com.localvalu.coremerchantapp.data.output.order.OrderListResult;
import com.localvalu.coremerchantapp.data.output.order.OrderStatusChangeOutput;
import com.localvalu.coremerchantapp.data.output.order.OrderStatusChangeResult;
import com.localvalu.coremerchantapp.data.output.tablebooking.TableBookingListOutput;
import com.localvalu.coremerchantapp.data.output.tablebooking.TableBookingListResult;
import com.localvalu.coremerchantapp.data.output.tablebooking.TableBookingStatusChangeOutput;
import com.localvalu.coremerchantapp.data.output.tablebooking.TableBookingStatusChangeResult;
import com.localvalu.coremerchantapp.data.output.trace.TraceHistoryReportResult;
import com.localvalu.coremerchantapp.data.output.trace.TraceHistoryReportResultOutput;
import com.localvalu.coremerchantapp.data.output.trace.VisitEntryResult;
import com.localvalu.coremerchantapp.data.output.trace.VisitEntryResultOutput;
import com.utils.base.AppBaseActivity;
import com.utils.base.AppBaseFragment;
import com.utils.base.Task;
import com.utils.helper.preference.PreferenceFactory;
import com.utils.helper.toast.ToastFactory;
import com.utils.rest.RestHelper;
import com.utils.rest.RestResultReceiver;
import com.utils.validator.Validator;


public class LocalValuActivity extends AppBaseActivity implements LocalValuConstants, RestHandler
{

    private static final String TAG = LocalValuActivity.class.getSimpleName();

    private static final int GPS_REQUEST_CODE = 1000; // arbitrary code

    protected Fragment currentFragment = null;

    private boolean googlePlayerServiceSupport = false;

    public <T extends AppBaseFragment> T getFragment(String tag, Class<T> fragment)
    {
        AppBaseFragment appBaseFragment = super.getFragment((Class<AppBaseFragment>) fragment);
        return (T) appBaseFragment;
    }

    public void updateFragment(int containerViewId, Fragment fragment, boolean addToBackStack, boolean replace)
    {
        currentFragment = fragment;
        super.updateFragmentView(containerViewId, fragment, addToBackStack, replace);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        checkPlayServicesAndInit();
    }

    @Override
    public void init()
    {

    }

    @Override
    protected void onResume()
    {
        super.onResume();
    }

    private void checkPlayServicesAndInit()
    {
        GoogleApiAvailability api = GoogleApiAvailability.getInstance();
        int status = api.isGooglePlayServicesAvailable(this);
        if (status != ConnectionResult.SUCCESS)
        {
            googlePlayerServiceSupport = false;

            /*if (api.isUserResolvableError(status))
            {
                // onActivityResult() will be called with GPS_REQUEST_CODE
                Dialog dialog = api.getErrorDialog(this, status, GPS_REQUEST_CODE,
                        new DialogInterface.OnCancelListener()
                        {
                            @Override
                            public void onCancel(DialogInterface dialog)
                            {
                                // GPS update was cancelled.
                                // Do toast or dialog (probably better to do) here.
                                finish();

                                googlePlayerServiceSupport = false;
                            }
                        });
                dialog.show();
            }
            else
            {
                // unrecoverable error

                googlePlayerServiceSupport = false;
            }*/
        }
        else
        {
           googlePlayerServiceSupport = true;
        }
    }

    public void showToast(String message)
    {
        View view = LayoutInflater.from(this).inflate(R.layout.view_app_toast, null, false);
        TextView textView = view.findViewById(R.id.app_toast_textView);
        textView.setText(message);

        ToastFactory toastFactory = super.getToastFactory();
        toastFactory.setCustomView(true);
        toastFactory.setView(view);
        toastFactory.toast(message);
    }


    @Override
    public void callMyTransaction(String fromDate, String toDate, Task<MyTransactionResult> task)
    {
        showProgressBar();

        MyTransactionInput myTransactionInput = new MyTransactionInput();
        myTransactionInput.setMerchantId(Integer.parseInt(PreferenceFactory.getInstance().getString(PREF_RETAILER_ID)));
        myTransactionInput.setAccountId(Integer.parseInt(PreferenceFactory.getInstance().getString(PREF_ACCOUNT_ID)));
        myTransactionInput.setFromDate(fromDate);
        myTransactionInput.setToDate(toDate);

        RestHelper restHelper = new RestHelper(BuildConfig.URL_BASE_DOTNET);
        restHelper.sendPost("MerchantAppTransactionHistory", myTransactionInput, MyTransactionOutput.class, (RestResultReceiver<MyTransactionOutput>) data ->
        {
            dismissProgressBar();
            validateResult(Validator.isValid(data) ? data.getResult() : null, task);
        });
    }

    @Override
    public void callTransferLoyaltyToken(String currentBalance, String transferAmount, String balanceAfterTransfer, String toAccountId, Task<TransferLoyaltyTokenResult> task)
    {
        if (!Validator.isValid(toAccountId) || toAccountId.equals("localvalu"))
        {
            showToast("Enter customer unique code");
            return;
        }

        showProgressBar();

        TransferLoyaltyTokenInput transferLoyaltyTokenInput = new TransferLoyaltyTokenInput();
        transferLoyaltyTokenInput.setRetailerId(PreferenceFactory.getInstance().getString(PREF_RETAILER_ID));
        transferLoyaltyTokenInput.setCurrentBalance(currentBalance);
        transferLoyaltyTokenInput.setTransferAmount(transferAmount);
        transferLoyaltyTokenInput.setBalanceAfterTransfer(balanceAfterTransfer);
        transferLoyaltyTokenInput.setToAccountID(toAccountId);

        RestHelper restHelper = new RestHelper(BuildConfig.URL_BASE_DOTNET);
        restHelper.sendPost("MerchantTransferLT", transferLoyaltyTokenInput, TransferLoyaltyTokenOutput.class, (RestResultReceiver<TransferLoyaltyTokenOutput>) data ->
        {
            dismissProgressBar();
            validateResult(Validator.isValid(data) ? data.getResult() : null, task);
        });
    }

    @Override
    public void callOrderList(String retailerId, Task<OrderListResult> task)
    {
        if (!Validator.isValid(retailerId))
        {
            showToast("Enter Account Id");
            return;
        }

        showProgressBar();

        OrderListInput orderListInput = new OrderListInput();
        orderListInput.setRetailerId(retailerId);

        RestHelper restHelper = new RestHelper(BuildConfig.URL_BASE_PHP1);
        restHelper.sendPost("MerchantAppV2/getOrderDetailsForADay", orderListInput, OrderListOutput.class, (RestResultReceiver<OrderListOutput>) data ->
        {
            dismissProgressBar();
            validateResult(Validator.isValid(data) ? data.getOrderListResult() : null, task);
        });
    }

    @Override
    public void callOrderDetails(String orderId, Task<OrderDetailsResult> task)
    {
        if (!Validator.isValid(orderId))
        {
            showToast("Enter order Id");
            return;
        }

        showProgressBar();

        OrderDetailsInput orderDetailsResult = new OrderDetailsInput();
        orderDetailsResult.setOrderId(orderId);

        RestHelper restHelper = new RestHelper(BuildConfig.URL_BASE_PHP1);
        restHelper.sendPost("MerchantAppV2/getInduvidualOrderDetails", orderDetailsResult, OrderDetailsResultOutput.class, (RestResultReceiver<OrderDetailsResultOutput>) data ->
        {
            dismissProgressBar();
            validateResult(Validator.isValid(data) ? data.getOrderDetailsResult() : null, task);
        });
    }

    @Override
    public void callOrderAtTableList(String retailerId, Task<OrderListResult> task)
    {
        if (!Validator.isValid(retailerId))
        {
            showToast("Enter Account Id");
            return;
        }

        showProgressBar();

        OrderListInput orderListInput = new OrderListInput();
        orderListInput.setRetailerId(retailerId);

        RestHelper restHelper = new RestHelper(BuildConfig.URL_BASE_PHP1);
        restHelper.sendPost("MerchantAppV2/getTableOrderDetailsForADay", orderListInput, OrderListOutput.class, (RestResultReceiver<OrderListOutput>) data -> {
            dismissProgressBar();
            validateResult(Validator.isValid(data) ? data.getOrderListResult() : null, task);
        });
    }

    @Override
    public void callOrderAtTableDetails(String orderId, Task<OrderDetailsResult> task)
    {
        if (!Validator.isValid(orderId))
        {
            showToast("Enter order Id");
            return;
        }

        showProgressBar();

        OrderDetailsInput orderDetailsResult = new OrderDetailsInput();
        orderDetailsResult.setOrderId(orderId);

        RestHelper restHelper = new RestHelper(BuildConfig.URL_BASE_PHP1);
        restHelper.sendPost("MerchantAppV2/getInduvidualTableOrderDetails", orderDetailsResult, OrderDetailsResultOutput.class, (RestResultReceiver<OrderDetailsResultOutput>) data -> {
            dismissProgressBar();
            validateResult(Validator.isValid(data) ? data.getOrderDetailsResult() : null, task);
        });
    }

    @Override
    public void callOrderStatusChange(String orderId, String status, Task<OrderStatusChangeResult> task)
    {
        if (!Validator.isValid(orderId))
        {
            showToast("Order Id cannot be empty");
            return;
        }
        else if (!Validator.isValid(status))
        {
            showToast("Status cannot be emtpy");
            return;
        }

        showProgressBar();

        OrderStatusChangeInput orderStatusChangeInput = new OrderStatusChangeInput();
        orderStatusChangeInput.setOrderId(orderId);
        orderStatusChangeInput.setStatus(status);

        RestHelper restHelper = new RestHelper(BuildConfig.URL_BASE_PHP1);
        restHelper.sendPost("MerchantAppV2/ChangeOrderStatus", orderStatusChangeInput, OrderStatusChangeOutput.class, (RestResultReceiver<OrderStatusChangeOutput>) data ->
        {
            dismissProgressBar();
            validateResult(Validator.isValid(data) ? data.getOrderStatusChangeResult() : null, task);
        });
    }

    @Override
    public void callTableBookingList(String retailerId, Task<TableBookingListResult> task)
    {
        if (!Validator.isValid(retailerId))
        {
            showToast("callTableBookingList");
            return;
        }

        showProgressBar();

        TableBookingListInput tableBookingListInput = new TableBookingListInput();
        tableBookingListInput.setRetailerId(retailerId);


        RestHelper restHelper = new RestHelper(BuildConfig.URL_BASE_PHP1);
        restHelper.sendPost("MerchantApp/TableBookingList", tableBookingListInput, TableBookingListOutput.class, (RestResultReceiver<TableBookingListOutput>) data ->
        {
            dismissProgressBar();
            validateResult(Validator.isValid(data) ? data.getTableBookingListResult() : null, task);
        });
    }

    @Override
    public void callTableBookingStatusChange(String tableBookId, String status, Task<TableBookingStatusChangeResult> task)
    {
        if (!Validator.isValid(tableBookId))
        {
            showToast("Book Id cannot be empty");
            return;
        }
        else if (!Validator.isValid(status))
        {
            showToast("Status cannot be emtpy");
            return;
        }

        showProgressBar();

        TableBookingStatusChangeInput tableBookingStatusChangeInput = new TableBookingStatusChangeInput();
        tableBookingStatusChangeInput.setTableBookid(tableBookId);
        tableBookingStatusChangeInput.setStatus(status);
        Log.d(TAG, "callTableBookingStatusChange: " + tableBookingStatusChangeInput.toString());


        RestHelper restHelper = new RestHelper(BuildConfig.URL_BASE_PHP1);
        restHelper.sendPost("MerchantApp/ChangeTableBookStatus", tableBookingStatusChangeInput, TableBookingStatusChangeOutput.class, (RestResultReceiver<TableBookingStatusChangeOutput>) data ->
        {
            dismissProgressBar();
            validateResult(Validator.isValid(data) ? data.getTableBookingStatusChangeResult() : null, task);
        });
    }

    @Override
    public void callInsertVisitEntry(int merchantId, String QRCode, String visitDate, Task<VisitEntryResult> task)
    {
        if (!Validator.isValid(merchantId))
        {
            showToast("Merchant Id cannot be empty");
            return;
        }
        else if (!Validator.isValid(QRCode))
        {
            showToast("QRCode cannot be empty");
            return;
        }
        else if (!Validator.isValid(visitDate))
        {
            showToast("Visit Date cannot be empty");
            return;
        }

        showProgressBar();

        InsertVisitEntryInput insertVisitEntryInput = new InsertVisitEntryInput();
        insertVisitEntryInput.setMerchantId(merchantId);
        insertVisitEntryInput.setQRCode(QRCode);
        insertVisitEntryInput.setVisitDate(visitDate);
        Log.d(TAG, "callInsertVisitEntry: " + insertVisitEntryInput.toString());


        RestHelper restHelper = new RestHelper(BuildConfig.URL_BASE_DOTNET);
        restHelper.sendPost("TraceHistory_Insert", insertVisitEntryInput, VisitEntryResultOutput.class, (RestResultReceiver<VisitEntryResultOutput>) data ->
        {
            dismissProgressBar();
            validateResult(Validator.isValid(data) ? data.getVisitEntryResult() : null, task);
        });
    }

    @Override
    public void callVisitEntryHistory(int merchantId, String fromDate, String toDate, Task<TraceHistoryReportResult> task)
    {

        if (!Validator.isValid(merchantId))
        {
            showToast("Merchant Id cannot be empty");
            return;
        }
        else if (!Validator.isValid(fromDate))
        {
            showToast("From date  cannot be empty");
            return;
        }
        else if (!Validator.isValid(toDate))
        {
            showToast("To Date cannot be empty");
            return;
        }

        showProgressBar();

        VisitEntryHistoryInput visitEntryHistoryInput = new VisitEntryHistoryInput();
        visitEntryHistoryInput.setMerchantId(merchantId);
        visitEntryHistoryInput.setFromDate(fromDate);
        visitEntryHistoryInput.setToDate(toDate);
        Log.d(TAG, "callVisitEntryHistory: " + visitEntryHistoryInput.toString());


        RestHelper restHelper = new RestHelper(BuildConfig.URL_BASE_DOTNET);
        restHelper.sendPost("TraceHistoryReport", visitEntryHistoryInput, TraceHistoryReportResultOutput.class, (RestResultReceiver<TraceHistoryReportResultOutput>) data ->
        {
            dismissProgressBar();
            validateResult(Validator.isValid(data) ? data.getTraceHistoryReportResult() : null, task);
        });

    }

    private void validateResult(RestBasedOutput restBasedOutput, Task task)
    {
        if (Validator.isValid(restBasedOutput))
        {
            RestBasedErrorDetail errorDetail = restBasedOutput.getErrorDetail();

            if (Validator.isValid(errorDetail))
            {
                String errorMessage = errorDetail.getErrorMessage();

                if (errorDetail.getErrorCode() != 0)
                {
                    task.onError(Validator.isValid(errorMessage) ? errorMessage : "No error message");
                }
                else
                {
                    task.onSuccess(restBasedOutput);
                }
            }
            else
            {
                task.onSuccess(restBasedOutput);
            }
        }
        else
        {
            task.onError("Invalid response");
        }
    }

    public boolean isGooglePlayerServiceSupport()
    {
        return googlePlayerServiceSupport;
    }

}
