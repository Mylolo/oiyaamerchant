package com.localvalu.merchant.blueunitt1n;

public interface LocalValuConstants
{
    String EXTRA_TITLE = "extraTitle";
    String EXTRA_URL = "extraUrl";

    String PREF_RETAILER_ID = "prefRetailerId";
    String PREF_ACCOUNT_ID = "prefAccountId";
    String PREF_EMAIL = "prefEmail";
    String PREF_FCM_ID = "prefFCMId";
    String PREF_PUSHY_ID = "prefPushyId";
    String PREF_BOOKING_TYPE_ID = "prefBookingTypeId";
    String PREF_RETAILER_TYPE = "prefRetailerType";

    String ORDER_ONLINE="4";
    String ORDER_AT_TABLE="5";
    String ORDER_AT_STORE="6";
}
