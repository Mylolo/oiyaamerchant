package com.localvalu.merchant.blueunitt1n;

import com.localvalu.coremerchantapp.data.output.MyTransactionResult;
import com.localvalu.coremerchantapp.data.output.SaleTransactionResult;
import com.localvalu.coremerchantapp.data.output.SignInResult;
import com.localvalu.coremerchantapp.data.output.TokenBalanceResult;
import com.localvalu.coremerchantapp.data.output.TransferLoyaltyTokenResult;
import com.localvalu.coremerchantapp.data.output.order.OrderDetailsResult;
import com.localvalu.coremerchantapp.data.output.order.OrderListResult;
import com.localvalu.coremerchantapp.data.output.order.OrderStatusChangeResult;
import com.localvalu.coremerchantapp.data.output.tablebooking.TableBookingListResult;
import com.localvalu.coremerchantapp.data.output.tablebooking.TableBookingStatusChangeResult;
import com.localvalu.coremerchantapp.data.output.trace.TraceHistoryReportResult;
import com.localvalu.coremerchantapp.data.output.trace.VisitEntryResult;
import com.utils.base.Task;

public interface RestHandler
{

    void callMyTransaction(String fromDate, String toDate, Task<MyTransactionResult> task);

    void callTransferLoyaltyToken(String currentBalance, String transferAmount, String balanceAfterTransfer, String toAccountId, Task<TransferLoyaltyTokenResult> task);

    void callOrderList(String userId, Task<OrderListResult> task);

    void callOrderDetails(String orderId, Task<OrderDetailsResult> task);

    void callOrderAtTableList(String userId, Task<OrderListResult> task);

    void callOrderAtTableDetails(String orderId, Task<OrderDetailsResult> task);

    void callOrderStatusChange(String orderId, String status, Task<OrderStatusChangeResult> task);

    void callTableBookingList(String retailerId, Task<TableBookingListResult> task);

    void callTableBookingStatusChange(String tableBookId, String status, Task<TableBookingStatusChangeResult> task);

    void callInsertVisitEntry(int merchantId, String QRCode, String visitDate, Task<VisitEntryResult> task);

    void callVisitEntryHistory(int merchantId, String fromDate, String toDate, Task<TraceHistoryReportResult> task);
}
