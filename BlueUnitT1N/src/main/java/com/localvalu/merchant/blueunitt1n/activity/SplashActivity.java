package com.localvalu.merchant.blueunitt1n.activity;

import android.app.ActivityManager;
import android.app.admin.DevicePolicyManager;
import android.app.admin.SystemUpdatePolicy;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.os.UserManager;
import android.provider.Settings;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContract;
import androidx.activity.result.contract.ActivityResultContracts;

import com.localvalu.coremerchantapp.LocalValuActivity;
import com.localvalu.merchant.blueunitt1n.ActivityConfig;
import com.localvalu.merchant.blueunitt1n.R;
import com.localvalu.merchant.blueunitt1n.components.MyAdmin;
import com.utils.base.Task;
import com.utils.data.DangerousPermission;
import com.utils.helper.permission.PermissionHandler;
import com.utils.util.Util;

import java.io.IOException;
import java.util.List;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class SplashActivity extends LocalValuActivity implements View.OnClickListener
{
    private static final String TAG = SplashActivity.class.getSimpleName();

    private Handler handler;
    private Runnable runnable = new Runnable()
    {
        @Override
        public void run()
        {
            ActivityConfig.startDashBoardActivity(SplashActivity.this);
        }
    };

    //KIOSK
    private static final int KIOSK_MODE_REQUEST = 48;
    final int flags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
            | View.SYSTEM_UI_FLAG_FULLSCREEN
            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
    private ComponentName mAdminComponentName;
    private DevicePolicyManager mDevicePolicyManager;
    protected PowerManager.WakeLock mWakeLock;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        //initKioskMode();
        init();
    }

    private void initKioskMode()
    {
        Log.d(TAG,"initKioskMode");
        mDevicePolicyManager = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
        mAdminComponentName = new ComponentName("com.localvalu.merchant.blueunitt1n","com.localvalu.merchant.blueunitt1n.components.MyAdmin");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P)
        {
            try
            {
                mDevicePolicyManager.setLockTaskFeatures(mAdminComponentName,
                        DevicePolicyManager.LOCK_TASK_FEATURE_HOME |
                                DevicePolicyManager.LOCK_TASK_FEATURE_OVERVIEW);

            }
            catch (Exception ex)
            {
                Log.d(TAG,ex.getLocalizedMessage());
                try
                {
                    Runtime.getRuntime().exec("adb shell dpm set-device-admin --user 0 com.localvalu.merchant.blueunitt1n.components/.MyAdmin");
                }
                catch (Exception runTimeException)
                {
                    runTimeException.printStackTrace();
                }
            }
        }

        if (!mDevicePolicyManager.isAdminActive(mAdminComponentName))
        {
            Log.d(TAG, "onCreate: isAdminActive");
            Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
            intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, mAdminComponentName);
            mStartForResult.launch(intent);
            return;
        }
        else
        {
            Log.d(TAG, "onCreate: isAdminActive no ");
            try
            {
                //Runtime.getRuntime().exec("adb shell dpm set-device-owner com.localvalu.merchant.blueunitt1n.components/.MyAdmin");
                Log.d(TAG, "onCreate:  isAdminActive yes working ");
            }
            catch (Exception e)
            {
                Log.d(TAG, "onCreate: IOException isAdminActive no " + e.getMessage().toString());
                e.printStackTrace();
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2)
        {
            if (mDevicePolicyManager.isDeviceOwnerApp(getPackageName()))
            {
                // App is whitelisted
                setDefaultCosuPolicies(true);
            }
            else
            {
                // did you provision the app using <adb shell dpm set-device-owner ...> ?
            }
        }

        if (mDevicePolicyManager.isDeviceOwnerApp(getPackageName()))
        {
            Log.d(TAG, "onCreate: isDeviceOwnerApp ");
            mDevicePolicyManager.setLockTaskPackages(mAdminComponentName, new String[]{getPackageName()});
            startLockTask();

            final PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
            this.mWakeLock = pm.newWakeLock(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, TAG + "_wake_lock");
            this.mWakeLock.acquire();

            Util.setStatusBarColor(this, R.color.white);
            init();
        }

        else
        {
            Log.d(TAG, "onCreate: isDeviceOwnerApp no ");
        }

        /* This code together with the one in onDestroy()
         * will make the screen be always on until this Activity gets destroyed. */

    }

    ActivityResultLauncher<Intent> mStartForResult = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>()
    {
        @Override
        public void onActivityResult(ActivityResult result)
        {

        }
    });

    @Override
    public void init()
    {
        checkPermissionAccess(Util.asList(DangerousPermission.CAMERA, DangerousPermission.WRITE_EXTERNAL_STORAGE));
    }

    @Override
    public void onClick(View view)
    {
        int id = view.getId();
    }

    private void checkPermissionAccess(List<DangerousPermission> dangerousPermissions)
    {
        getPermissionFactory().requestPermission(this, dangerousPermissions, new PermissionHandler()
        {
            @Override
            public void result(List<DangerousPermission> granted, List<DangerousPermission> denied)
            {
                processApp();
            }

            @Override
            public boolean permissionRationale()
            {
                return false;
            }

            @Override
            public void permissionRationaleFor(List<DangerousPermission> dangerousPermissions)
            {
                checkPermissionAccess(dangerousPermissions);
            }

            @Override
            public void info(String message)
            {
                processApp();
            }
        });
    }

    private void processApp()
    {
        Util.makeDelay(1500, new Task<Boolean>()
        {
            @Override
            public void onSuccess(Boolean result)
            {
                ActivityConfig.startDashBoardActivity(SplashActivity.this);
            }

            @Override
            public void onError(String message)
            {

            }
        });
        //handler = new Handler();
        //handler.postDelayed(runnable,1500);
    }

    /*@Override
    public void onResume()
    {
        super.onResume();
        mAdminComponentName = MyAdmin.getComponentName(this);
        if (mDevicePolicyManager.isLockTaskPermitted(getPackageName()))
        {
            Log.d(TAG, "onResume: sucess");
            DevicePolicyManager mDevicePolicyManager = (DevicePolicyManager) getSystemService(
                    Context.DEVICE_POLICY_SERVICE);
            ComponentName mAdminComponentName = new ComponentName(getApplicationContext(), MyAdmin.class);
            mDevicePolicyManager.setLockTaskPackages(mAdminComponentName, new String[]{getPackageName()});
            startLockTask();
        }
        else
        {
            Log.d(TAG, "onResume: isLock Permitted no");

            try
            {
                //runShellCommand("adb shell dpm set-device-owner com.wattoeat.restaurant/.MyAdmin");
            }
            catch (Exception e)
            {
                Log.d(TAG, "onResume: Exception " + e.getMessage().toString());
                e.printStackTrace();
            }
            // Because the package isn't allowlisted, calling startLockTask() here
            // would put the activity into screen pinning mode.
        }
    }*/

    private void runShellCommand(String command) throws Exception
    {
        Process process = Runtime.getRuntime().exec(command);
        process.waitFor();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {

        if (keyCode == KeyEvent.KEYCODE_VOLUME_UP)
        {
            Toast.makeText(this, "Volume button is disabled", Toast.LENGTH_SHORT).show();
            return true;
        }

        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN)
        {
            Toast.makeText(this, "Volume button is disabled", Toast.LENGTH_SHORT).show();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    /*@Override
    protected void onStart()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            if (mDevicePolicyManager.isLockTaskPermitted(this.getPackageName()))
            {
                ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                {
                    if (am.getLockTaskModeState() == ActivityManager.LOCK_TASK_MODE_NONE)
                    {
                        setDefaultCosuPolicies(true);
                        startLockTask();
                    }
                }
            }
        }
        super.onStart();
    }*/

    private void setDefaultCosuPolicies(boolean active)
    {

        // Set user restrictions
        setUserRestriction(UserManager.DISALLOW_SAFE_BOOT, active);
        setUserRestriction(UserManager.DISALLOW_FACTORY_RESET, active);
        setUserRestriction(UserManager.DISALLOW_ADD_USER, active);
        setUserRestriction(UserManager.DISALLOW_MOUNT_PHYSICAL_MEDIA, active);
        setUserRestriction(UserManager.DISALLOW_ADJUST_VOLUME, active);

        // Disable keyguard and status bar
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            mDevicePolicyManager.setKeyguardDisabled(mAdminComponentName, active);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            mDevicePolicyManager.setStatusBarDisabled(mAdminComponentName, active);
        }

        // Enable STAY_ON_WHILE_PLUGGED_IN
        enableStayOnWhilePluggedIn(active);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            // Set system update policy
            if (active)
            {

                mDevicePolicyManager.setSystemUpdatePolicy(mAdminComponentName, SystemUpdatePolicy.createWindowedInstallPolicy(60, 120));

            }
            else
            {
                mDevicePolicyManager.setSystemUpdatePolicy(mAdminComponentName, null);
            }
        }
        // set this Activity as a lock task package
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            mDevicePolicyManager.setLockTaskPackages(mAdminComponentName, active ? new String[]{getPackageName()} : new String[]{});
        }

        IntentFilter intentFilter = new IntentFilter(Intent.ACTION_MAIN);
        intentFilter.addCategory(Intent.CATEGORY_HOME);
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);

        if (active)
        {
            // set Cosu activity as home intent receiver so that it is started
            // on reboot
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            {
                mDevicePolicyManager.addPersistentPreferredActivity(mAdminComponentName, intentFilter, new ComponentName(getPackageName(), MyAdmin.class.getName()));
            }
        }
        else
        {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            {
                mDevicePolicyManager.clearPackagePersistentPreferredActivities(mAdminComponentName, getPackageName());
            }
        }
    }

    private void setUserRestriction(String restriction, boolean disallow)
    {
        if (disallow)
        {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            {
                mDevicePolicyManager.addUserRestriction(mAdminComponentName, restriction);
            }
        }
        else
        {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            {
                mDevicePolicyManager.clearUserRestriction(mAdminComponentName, restriction);
            }
        }
    }

    private void enableStayOnWhilePluggedIn(boolean enabled)
    {
        if (enabled)
        {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            {
                mDevicePolicyManager.setGlobalSetting(mAdminComponentName, Settings.Global.STAY_ON_WHILE_PLUGGED_IN, Integer.toString(BatteryManager.BATTERY_PLUGGED_AC | BatteryManager.BATTERY_PLUGGED_USB | BatteryManager.BATTERY_PLUGGED_WIRELESS));
            }
        }
        else
        {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            {
                mDevicePolicyManager.setGlobalSetting(mAdminComponentName, Settings.Global.STAY_ON_WHILE_PLUGGED_IN, "0");
            }
        }
    }

    @Override
    public void onDestroy()
    {
        if(this.mWakeLock!=null)
        {
            this.mWakeLock.release();
        }
        super.onDestroy();
    }


}
