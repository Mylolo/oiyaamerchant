package com.localvalu.merchant.blueunitt1n.components;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.speech.tts.TextToSpeech;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.localvalu.coremerchantapp.BuildConfig;
import com.localvalu.coremerchantapp.data.base.RestBasedErrorDetail;
import com.localvalu.coremerchantapp.data.base.RestBasedOutput;
import com.localvalu.coremerchantapp.data.input.OrderListInput;
import com.localvalu.coremerchantapp.data.input.TableBookingListInput;
import com.localvalu.coremerchantapp.data.output.order.OrderListOutput;
import com.localvalu.coremerchantapp.data.output.order.OrderListResult;
import com.localvalu.coremerchantapp.data.output.tablebooking.TableBookingListOutput;
import com.localvalu.coremerchantapp.data.output.tablebooking.TableBookingListResult;
import com.localvalu.coremerchantapp.utils.AppConstants;
import com.localvalu.coremerchantapp.utils.AppUtils;
import com.localvalu.merchant.blueunitt1n.R;
import com.localvalu.merchant.blueunitt1n.activity.DashBoardActivity;
import com.utils.base.Task;
import com.utils.rest.RestHelper;
import com.utils.rest.RestResultReceiver;
import com.utils.validator.Validator;

import java.util.Locale;

import static com.localvalu.coremerchantapp.LocalValuConstants.ORDER_AT_STORE;
import static com.localvalu.coremerchantapp.LocalValuConstants.ORDER_AT_TABLE;
import static com.localvalu.coremerchantapp.utils.AppUtils.ACTION_NOTIFICATION_END;
import static com.localvalu.coremerchantapp.utils.AppUtils.ACTION_NOTIFICATION_START;
import static com.localvalu.coremerchantapp.utils.AppUtils.BROADCAST_ACTION;
import static com.localvalu.coremerchantapp.utils.AppUtils.BROADCAST_ACTION_ORDER_AT_TABLE_LIST;
import static com.localvalu.coremerchantapp.utils.AppUtils.BROADCAST_ACTION_TABLE_BOOKING_LIST;

public class NewRequestsCheckService extends Service implements TextToSpeech.OnInitListener
{
    private static final String TAG = "NewRequestsCheckService";


    private final IBinder mBinder = new MyBinder();
    private Handler mHandler;
    //default interval for syncing data
    public static final long DEFAULT_SYNC_INTERVAL = 30 * 1000;
    private String retailerId = "", bookingTypeId = "",retailerType="";
    private int lastSize = 0, lastSizeOrderAtTable = 0,lastSizeTableBookingList=0;
    private TextToSpeech mTts;
    private String spokenText = "";
    private int cycleCount = 0;
    public static final int NOTIFICATION_ID = 1;

    @Override
    public void onCreate()
    {
        super.onCreate();
        if (Build.VERSION.SDK_INT >= 26)
        {
            String CHANNEL_ID = getString(R.string.default_local_channel_id);
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);

            ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).createNotificationChannel(channel);

            /*Intent stopSelf = new Intent(this, NewRequestsCheckService.class);
            stopSelf.setAction(ACTION_NOTIFICATION_END);
            PendingIntent pStopSelf = PendingIntent.getService(this, 0, stopSelf,0);*/

            Intent contentIntent = new Intent(this, DashBoardActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, contentIntent,0);

            Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setContentTitle("Running...")
                    .setContentText("")
                    .setSmallIcon(R.drawable.ic_stat_name)
                    .setAutoCancel(true)
                    .setContentIntent(pendingIntent)
                    //.addAction(R.drawable.ic_exit_to_app, getString(R.string.lbl_exit), pStopSelf)
                    .build();

            startForeground(NOTIFICATION_ID, notification);
        }
        mTts = new TextToSpeech(this, this);
        spokenText = "Your voice service started";
    }

    // task to be run here
    private Runnable runnableService = new Runnable()
    {
        @Override
        public void run()
        {
            cycleCount = cycleCount + 1;
            syncOrderOnlineData();
            syncOrderAtTableData();
            if(retailerType!=null)
            {
                switch (retailerType)
                {
                    case AppConstants.RETAILER_TYPE_EATS:
                        syncTableBookingData();
                        break;
                    case AppConstants.RETAILER_TYPE_LOCAL:
                        break;
                }
            }
            // Repeat this runnable code block again every ... min
            mHandler.postDelayed(runnableService, DEFAULT_SYNC_INTERVAL);
        }
    };

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        Log.d(TAG, "onStartCommand");
        if(intent!=null)
        {
            if (intent.getAction() != null)
            {
                switch (intent.getAction())
                {
                    case ACTION_NOTIFICATION_START:
                        retailerId = intent.getExtras().getString(AppUtils.BUNDLE_RETAILER_ID);
                        bookingTypeId = intent.getExtras().getString(AppUtils.BUNDLE_BOOKING_TYPE_ID);
                        retailerType = intent.getExtras().getString(AppUtils.BUNDLE_RETAILER_TYPE);
                        // Create the Handler object
                        mHandler = new Handler();
                        // Execute a runnable task as soon as possible
                        mHandler.post(runnableService);
                        break;
                    case ACTION_NOTIFICATION_END:
                        stopSelf();
                        break;
                }
            }
        }


        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent)
    {
        return mBinder;
    }

    public class MyBinder extends Binder
    {
        public NewRequestsCheckService getService()
        {
            return NewRequestsCheckService.this;
        }
    }

    private void syncOrderOnlineData()
    {
        // call your rest service here

        //Log.d(TAG, "syncOrderOnlineData");
        OrderListInput orderListInput = new OrderListInput();
        orderListInput.setRetailerId(retailerId);

        RestHelper restHelper = new RestHelper(BuildConfig.URL_BASE_PHP1);
        restHelper.sendPost("MerchantAppV2/getOrderDetailsForADay", orderListInput, OrderListOutput.class, (RestResultReceiver<OrderListOutput>) data ->
        {
            validateResult(Validator.isValid(data) ? data.getOrderListResult() : null, new Task<OrderListResult>()
            {
                @Override
                public void onSuccess(OrderListResult result)
                {
                    //Log.d(TAG, "syncOrderOnlineData-onSuccess");
                    //Log.d(TAG, "syncOrderOnlineData-onSuccess Size " + result.getOrderList().size());
                    if (lastSize != result.getOrderList().size())
                    {
                        lastSize = result.getOrderList().size();

                        if (cycleCount > 2)
                        {
                            sendMyBroadCast(result);
                            String messageBody = getString(R.string.lbl_new_order_online_message);
                            textToSpeech(messageBody);
                            sendNotification(messageBody);
                        }
                    }
                }

                @Override
                public void onError(String message)
                {
                    Log.d(TAG, "syncData-onSuccess Error-->> " + message);

                }
            });
        });
    }

    private void syncOrderAtTableData()
    {
        //Log.d(TAG, "syncOrderOnlineData");
        OrderListInput orderListInput = new OrderListInput();
        orderListInput.setRetailerId(retailerId);

        RestHelper restHelperTwo = new RestHelper(BuildConfig.URL_BASE_PHP1);
        restHelperTwo.sendPost("getTableOrderDetailsForADay", orderListInput, OrderListOutput.class, (RestResultReceiver<OrderListOutput>) data ->
        {
            validateResult(Validator.isValid(data) ? data.getOrderListResult() : null, new Task<OrderListResult>()
            {
                @Override
                public void onSuccess(OrderListResult result)
                {
                    //Log.d(TAG, "syncData-onSuccess");
                    //Log.d(TAG, "syncData-onSuccess Size " + result.getOrderList().size());
                    if (lastSizeOrderAtTable != result.getOrderList().size())
                    {
                        lastSizeOrderAtTable = result.getOrderList().size();
                        if (cycleCount > 2)
                        {
                            sendOrderAtBroadCast(result);
                            String messageBody = "";
                            if (isOrderAtTable())
                            {
                                messageBody = getString(R.string.lbl_new_order_at_table_message);
                            }
                            else
                            {
                                messageBody = getString(R.string.lbl_new_order_at_store_message);
                            }
                            textToSpeech(messageBody);
                            sendNotification(messageBody);
                        }
                    }
                }

                @Override
                public void onError(String message)
                {
                    Log.d(TAG, "syncData-onSuccess Error-->> " + message);

                }
            });
        });
    }

    private void syncTableBookingData()
    {
        //Log.d(TAG, "syncTableBookingData");
        TableBookingListInput tableBookingListInput = new TableBookingListInput();
        tableBookingListInput.setRetailerId(retailerId);


        RestHelper restHelper = new RestHelper(BuildConfig.URL_BASE_PHP1);
        restHelper.sendPost("MerchantApp/TableBookingList", tableBookingListInput, TableBookingListOutput.class, (RestResultReceiver<TableBookingListOutput>) data ->
        {
            validateResult(Validator.isValid(data) ? data.getTableBookingListResult() : null, new Task<TableBookingListResult>()
            {
                @Override
                public void onSuccess(TableBookingListResult result)
                {
                    //Log.d(TAG, "syncData-onSuccess");
                    //Log.d(TAG, "syncData-onSuccess Size " + result.getTableBookingList().size());
                    if (lastSizeTableBookingList < result.getTableBookingList().size())
                    {
                        lastSizeTableBookingList = result.getTableBookingList().size();
                        if(cycleCount>2)
                        {
                            sendTableBookingListBroadCast(result);
                            String messageBody=getString(R.string.lbl_new_table_booking_requests_message);
                            textToSpeech(messageBody);
                            sendNotification(messageBody);
                        }
                    }
                }

                @Override
                public void onError(String message)
                {
                    Log.d(TAG, "syncData-onSuccess Error-->> " + message);

                }
            });
        });
    }

    private boolean isOrderAtTable()
    {
        return bookingTypeId.contains(ORDER_AT_TABLE);
    }

    private boolean isOrderAtStore()
    {
        return bookingTypeId.contains(ORDER_AT_STORE);
    }

    private void validateResult(RestBasedOutput restBasedOutput, Task task)
    {
        if (Validator.isValid(restBasedOutput))
        {
            RestBasedErrorDetail errorDetail = restBasedOutput.getErrorDetail();

            if (Validator.isValid(errorDetail))
            {
                String errorMessage = errorDetail.getErrorMessage();

                if (errorDetail.getErrorCode() != 0)
                {
                    task.onError(Validator.isValid(errorMessage) ? errorMessage : "No error message");
                }
                else
                {
                    task.onSuccess(restBasedOutput);
                }
            }
            else
            {
                task.onSuccess(restBasedOutput);
            }
        }
        else
        {
            task.onError("Invalid response");
        }
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private void sendNotification(String messageBody)
    {
        //Uri soundUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://"+ getApplicationContext().getPackageName() + "/" + R.raw.mysound);
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Intent intent = new Intent(this, DashBoardActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        String channelId = getString(R.string.default_local_channel_id);
        //Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.drawable.ic_stat_name)
                        .setContentTitle(getString(R.string.app_name))
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        //.setSound(soundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            NotificationChannel channel = new NotificationChannel(channelId, getString(R.string.app_name),
                    NotificationManager.IMPORTANCE_DEFAULT);
            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .build();
            //channel.setSound(soundUri, audioAttributes);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }

    @Override
    public void onInit(int status)
    {
        if (status == TextToSpeech.SUCCESS)
        {
            int result = mTts.setLanguage(Locale.US);
            if (result != TextToSpeech.LANG_MISSING_DATA && result != TextToSpeech.LANG_NOT_SUPPORTED)
            {
                Log.d(TAG, "Language UK Supported");
                mTts.speak(spokenText, TextToSpeech.QUEUE_FLUSH, null);
            }
            else
            {
                Log.d(TAG, "Language UK Not Supported");
            }
        }
        else
        {
            Log.d(TAG, "Text-To-Speech is not Supported");
        }
    }

    private void textToSpeech(String messageBody)
    {
        mTts.speak(messageBody, TextToSpeech.QUEUE_FLUSH, null);
    }

    @Override
    public void onLowMemory()
    {
        super.onLowMemory();
        stopSelf();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        Log.d(TAG, "onDestroy: called.");
        if (mTts != null)
        {
            mTts.stop();
            mTts.shutdown();
        }
        mHandler.removeCallbacks(runnableService);
        mHandler = null;
        stopSelf();
    }

    /**
     * This method is responsible to send broadCast to specific Action
     */
    private void sendMyBroadCast(OrderListResult orderListResult)
    {
        try
        {
            Intent broadCastIntent = new Intent();
            broadCastIntent.setAction(BROADCAST_ACTION);
            broadCastIntent.putExtra(AppUtils.BUNDLE_ORDER_LIST_RESULT, orderListResult);
            sendBroadcast(broadCastIntent);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    /**
     * This method is responsible to send broadCast to specific Action
     */
    private void sendOrderAtBroadCast(OrderListResult orderListResult)
    {
        try
        {
            Intent broadCastIntent = new Intent();
            broadCastIntent.setAction(BROADCAST_ACTION_ORDER_AT_TABLE_LIST);
            broadCastIntent.putExtra(AppUtils.BUNDLE_ORDER_AT_TABLE_LIST_RESULT, orderListResult);
            sendBroadcast(broadCastIntent);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    /**
     * This method is responsible to send broadCast to specific Action
     */
    private void sendTableBookingListBroadCast(TableBookingListResult tableBookingListResult)
    {
        try
        {
            Intent broadCastIntent = new Intent();
            broadCastIntent.setAction(BROADCAST_ACTION_TABLE_BOOKING_LIST);
            broadCastIntent.putExtra(AppUtils.BUNDLE_BOOKING_TABLE_LIST_RESULT, tableBookingListResult);
            sendBroadcast(broadCastIntent);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

}
