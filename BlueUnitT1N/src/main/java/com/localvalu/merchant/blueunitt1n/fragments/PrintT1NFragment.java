package com.localvalu.merchant.blueunitt1n.fragments;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.localvalu.coremerchantapp.BuildConfig;
import com.localvalu.coremerchantapp.LocalValuFragment;
import com.localvalu.coremerchantapp.R;
import com.localvalu.coremerchantapp.data.output.FoodItem;
import com.localvalu.coremerchantapp.data.output.SignInResult;
import com.localvalu.coremerchantapp.data.output.order.OrderDetails;
import com.localvalu.coremerchantapp.fragment.adapter.order.OrderedFoodItemsPrintAdapter;
import com.localvalu.coremerchantapp.handler.DashBoardHandler;
import com.localvalu.coremerchantapp.utils.AppConstants;
import com.localvalu.coremerchantapp.utils.AppUtils;
import com.localvalu.coremerchantapp.utils.QRCodeUtil;
import com.topwise.cloudpos.aidl.AidlDeviceService;
import com.topwise.cloudpos.aidl.printer.AidlPrinter;
import com.topwise.cloudpos.aidl.printer.AidlPrinterListener;
import com.topwise.cloudpos.aidl.printer.Align;
import com.topwise.cloudpos.aidl.printer.ImageUnit;
import com.topwise.cloudpos.aidl.printer.PrintItemObj;
import com.topwise.cloudpos.aidl.printer.PrintTemplate;
import com.topwise.cloudpos.aidl.printer.TextUnit;
import com.utils.helper.preference.PreferenceFactory;
import com.utils.helper.recyclerview.RVItemAnimator;
import com.utils.util.DateTimeUtil;
import com.utils.validator.Validator;
import com.utils.view.button.CustomMaterialButton;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public class PrintT1NFragment extends LocalValuFragment
{

    private static final String TAG = PrintT1NFragment.class.getSimpleName();
    public static final String TOPWISE_SERVICE_ACTION = "topwise_cloudpos_device_service";
    private CustomMaterialButton btnPrint;
    private ConstraintLayout cnlContentView;
    private AppCompatTextView tvMerchantName, tvCustomerName, tvOrderId, tvOrderDate, tvOrderTime;
    private RecyclerView rvOrderedItems;
    private AppCompatTextView tvNoOfItems, tvSubTotal, tvTokenApplied, tvBalanceToPay, tvDeliveryFee, tvFinalPay;
    private DashBoardHandler dashBoardHandler;
    private OrderDetails orderDetails;
    private AppCompatImageView ivLogo, ivPrint;
    private OrderedFoodItemsPrintAdapter orderedFoodItemsPrintAdapter;
    private String strCurrencySymbol, strCurrencyLetter;
    private int marginSpace;
    private SignInResult signInResult;

    // android built in classes for bluetooth operations
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothSocket mmSocket;
    private BluetoothDevice mmDevice;

    // needed for communication to bluetooth device / network
    private OutputStream mmOutputStream;
    private InputStream mmInputStream;
    private Thread workerThread;

    private byte[] readBuffer;
    private int readBufferPosition;
    private volatile boolean stopWorker;

    private Typeface typeface;

    //TOPWise
    private AidlPrinter printerDev = null;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();

        if (bundle != null)
        {
            orderDetails = bundle.getParcelable(AppUtils.BUNDLE_ORDER_DETAILS);
        }

        strCurrencySymbol = getString(R.string.symbol_pound);
        strCurrencyLetter = getString(R.string.currency_letter_lt);
        marginSpace = (int) requireContext().getResources().getDimension(R.dimen.margin_15);
        typeface = ResourcesCompat.getFont(requireContext(), R.font.hwzs);
        setSignInResult();
    }

    private void setSignInResult()
    {
        try
        {
            String strSignInResult = PreferenceFactory.getInstance().getString(PREF_SIGN_IN_RESULT);
            if (strSignInResult != null)
            {
                if (strSignInResult != "")
                {
                    Gson gson = new Gson();
                    signInResult = gson.fromJson(strSignInResult, SignInResult.class);
                }
            }
            if (signInResult != null)
            {
                if (BuildConfig.DEBUG)
                {
                    Log.d(TAG, "signInResult->" + signInResult.getDriverType());
                }
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_print, container, false);
        setHasOptionsMenu(true);
        setRetainInstance(true);
        initView(view);
        return view;
    }

    private void initView(View view)
    {
        cnlContentView = (ConstraintLayout) view.findViewById(R.id.cnlContentView);
        btnPrint = (CustomMaterialButton) view.findViewById(R.id.btnPrint);
        ivLogo = (AppCompatImageView) view.findViewById(R.id.ivLogo);
        ivPrint = (AppCompatImageView) view.findViewById(R.id.ivPrint);
        tvMerchantName = view.findViewById(R.id.tvMerchantName);
        tvCustomerName = view.findViewById(R.id.tvCustomerName);
        tvOrderId = view.findViewById(R.id.tvOrderId);
        tvOrderDate = view.findViewById(R.id.tvOrderDate);
        tvOrderTime = view.findViewById(R.id.tvOrderTime);
        tvNoOfItems = view.findViewById(R.id.tvNoOfItems);
        rvOrderedItems = view.findViewById(R.id.rvOrderedItems);
        tvSubTotal = view.findViewById(R.id.tvSubTotal);
        tvTokenApplied = view.findViewById(R.id.tvTokenApplied);
        tvBalanceToPay = view.findViewById(R.id.tvBalanceToPay);
        tvDeliveryFee = view.findViewById(R.id.tvDeliveryFee);
        tvFinalPay = view.findViewById(R.id.tvFinalPay);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        btnPrint.setOnClickListener(_OnClickListener);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rvOrderedItems = view.findViewById(R.id.rvOrderedItems);
        rvOrderedItems.setLayoutManager(linearLayoutManager);
        rvOrderedItems.setItemAnimator(new RVItemAnimator());
        rvOrderedItems.setNestedScrollingEnabled(false);
        setUpPrinter();
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);

        dashBoardHandler = (DashBoardHandler) context;
    }

    @Override
    public void onDetach()
    {
        dashBoardHandler = null;
        super.onDetach();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        bindService();
    }

    View.OnClickListener _OnClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            if (v.getId() == R.id.btnPrint)
            {
                try
                {
                    doPrint();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        }
    };

    private void setUpPrinter()
    {
        if (Build.MODEL.equals("T1N"))
        {
            setData();
        }
    }

    private void setData()
    {
        tvMerchantName.setText(orderDetails.getBusinessName());
        tvCustomerName.setText(orderDetails.getCustomerName());
        tvOrderId.setText(orderDetails.getOrderId());
        setPaymentDetails();
        setStatus(orderDetails.getStatus());
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        try
        {
            Date orderDate = dateFormat.parse(orderDetails.getDateTime());
            String strDate = DateTimeUtil.getFormattedDate(orderDate.getTime(), DateTimeUtil.MM_dd_yyyy);
            String strTime = DateTimeUtil.getFormattedDate(orderDate.getTime(), DateTimeUtil.hh_mm_a);
            tvOrderDate.setText(strDate);
            tvOrderTime.setText(strTime);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        setAdapter();
    }

    private void setPaymentDetails()
    {
        try
        {
            double orderTotal = (Double.parseDouble(orderDetails.getOrderTotal()));
            String strOrderTotal = String.format("%,.2f", orderTotal);
            StringBuilder strSubTotal = new StringBuilder();
            strSubTotal.append(strCurrencySymbol).append(strOrderTotal);
            tvSubTotal.setText(strSubTotal.toString());

            double discountAmount = (Double.parseDouble(orderDetails.getDiscountLtAmount()));
            String strDiscountAmount = String.format("%,.2f", discountAmount);
            StringBuilder strTokenApply = new StringBuilder();
            strTokenApply.append("-").append(strCurrencyLetter).
                    append(strDiscountAmount);
            tvTokenApplied.setText(strTokenApply.toString());

            double deliveryCharges = (Double.parseDouble(orderDetails.getDeliveryRate()));
            String strDeliveryCharges = String.format("%,.2f", deliveryCharges);
            StringBuilder strDeliveryChargesTwo = new StringBuilder();
            strDeliveryChargesTwo.append("+").append(strCurrencySymbol).append(strDeliveryCharges);
            tvDeliveryFee.setText(strDeliveryChargesTwo.toString());

            double finalOrderTotal = (Double.parseDouble(orderDetails.getFinalOrderTotal()));
            String strFinalOrderTotal = String.format("%,.2f", finalOrderTotal);
            StringBuilder strFinalPay = new StringBuilder();
            strFinalPay.append(strCurrencySymbol).append(strFinalOrderTotal);
            tvFinalPay.setText(strFinalPay.toString());
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

    }

    private void setStatus(String status)
    {
        switch (status)
        {
            case AppConstants.ACCEPT:
            case AppConstants.PENDING:
            case AppConstants.DELIVERED:
                tvNoOfItems.setText(getString(R.string.lbl_ordered_items));
                break;
            case AppConstants.DECLINED:
                tvNoOfItems.setText(getString(R.string.lbl_declined_ordered_items));
                break;
        }
    }

    private void setAdapter()
    {
        if (Validator.isValid(orderDetails.getFoodItems()) && !orderDetails.getFoodItems().isEmpty())
        {
            if (Validator.isValid(orderedFoodItemsPrintAdapter))
            {
                orderedFoodItemsPrintAdapter.addItems(orderDetails.getFoodItems());
            }
            else
            {
                orderedFoodItemsPrintAdapter = new OrderedFoodItemsPrintAdapter(orderDetails.getFoodItems());
                rvOrderedItems.setAdapter(orderedFoodItemsPrintAdapter);
            }
        }
        else
        {
            if (Validator.isValid(orderedFoodItemsPrintAdapter))
            {
                orderedFoodItemsPrintAdapter.addItems(new ArrayList<>());
            }
            else
            {
                orderedFoodItemsPrintAdapter = new OrderedFoodItemsPrintAdapter(new ArrayList<>());
                rvOrderedItems.setAdapter(orderedFoodItemsPrintAdapter);
            }
        }
    }

    private void doPrint() throws IOException
    {
        if (Build.MODEL.equals("T1N"))
        {
            printReceipt();
            //printBitmap(getBitmapFromView(requireContext(), cnlContentView));
            //printText(); //Eg
            //printTemplate();//Eg
        }
    }

    private Bitmap getBitmapFromView(Context ctx, View view)
    {
        view.setLayoutParams(new
                ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT,
                ConstraintLayout.LayoutParams.MATCH_PARENT));
        DisplayMetrics dm = ctx.getResources().getDisplayMetrics();
        view.measure(View.MeasureSpec.makeMeasureSpec(dm.widthPixels,
                View.MeasureSpec.EXACTLY),
                View.MeasureSpec.makeMeasureSpec(dm.heightPixels,
                        View.MeasureSpec.EXACTLY));
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(),
                view.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        view.layout(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
        view.draw(canvas);
        return bitmap;
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        dashBoardHandler.setPreviousTitle();
        requireContext().unbindService(conn);
    }

    private String getFormattedString(String leftSide, String rightSide)
    {
        char[] chars = new char[32];

        if (rightSide.contains(getString(R.string.symbol_pound)))
        {
            chars = new char[31];
        }

        int charCount = 0;

        for (char rowChar : chars)
        {
            chars[charCount] = ' ';
            charCount++;
        }

        for (int k = 0; k < leftSide.length(); k++)
        {
            chars[k] = leftSide.charAt(k);
        }

        int newCharCount = chars.length - 1;
        int rightCharacterCount = rightSide.length() - 1;

        for (char rowChar : chars)
        {
            if (rightCharacterCount < 0)
            {
                break;
            }
            else
            {
                chars[newCharCount] = rightSide.charAt(rightCharacterCount);
                newCharCount--;
                rightCharacterCount--;
            }
        }

        String theText = new String(chars);
        Log.d(TAG, " Chars - " + theText);
        return theText;
    }

    private void getShortFoodSizeName(FoodItem foodItem)
    {
        StringBuilder strFoodSizeName = new StringBuilder();
        String[] strItemName = foodItem.getItemName().split(" ");

        if (strFoodSizeName != null)
        {
            if (strItemName.length > 0)
            {
                strFoodSizeName.append(strItemName[0].substring(0, 1)).append(".");
                strFoodSizeName.append(strItemName[1]).append("(").append(foodItem.getSizeName()
                        .substring(0, 3)).append(")");
            }
            else
            {
                strFoodSizeName.append(strItemName[1]).append("(").append(foodItem.getSizeName()
                        .substring(0, 3)).append(")");
            }
        }
    }

    private String getPrintString(String strLeft, String strRight)
    {
        Character[] chars = new Character[32];

        int maxValue = chars.length - strRight.length();

        for (int i = chars.length - 1; i > strRight.length(); i--)
        {
            chars[i] = strRight.charAt(i);
        }

        for (int i = 0; i < strLeft.length() - 1; i++)
        {
            chars[i] = strRight.charAt(i);
        }

        return chars.toString();
    }

    private String getSecondLineFoodName(FoodItem foodItem)
    {
        StringBuilder strFoodSizeName = new StringBuilder();
        String[] strItemName = foodItem.getItemName().split(" ");

        if (strItemName != null)
        {
            if (strItemName.length > 1)
            {
                strFoodSizeName.append(strItemName[1]).append(" ").append(strItemName[2]).append("(").append(foodItem.getSizeName()).append(")");
                return strFoodSizeName.toString();
            }
            else if (strItemName.length > 0)
            {
                strFoodSizeName.append(strItemName[1]).append(" ").append("(").append(foodItem.getSizeName()).append(")");
                return strFoodSizeName.toString();
            }
        }
        return foodItem.getItemName();
    }

    private String getFirstLineFoodName(FoodItem foodItem)
    {
        StringBuilder strFoodSizeName = new StringBuilder();
        String[] strItemName = foodItem.getItemName().split(" ");

        if (strItemName != null)
        {
            if (strItemName.length > 0)
            {
                return strFoodSizeName.append(strItemName[0]).toString();
            }
            else
            {
                return strFoodSizeName.append(strItemName[0]).toString();
            }
        }
        return foodItem.getItemName();
    }

    //TopWise Functions

    /**
     * 获取打印机状态
     *
     * @param v
     * @createtor：Administrator
     * @date:2015-8-4 下午2:18:47
     */
    public void getPrintState(View v)
    {
        try
        {
            int printState = printerDev.getPrinterState();
            Toast.makeText(requireContext(), getResources().getString(R.string.print_status) + printState, Toast.LENGTH_LONG).show();
        }
        catch (RemoteException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * 打印文本
     *
     * @param v
     * @createtor：Administrator
     * @date:2015-8-4 下午2:19:28
     */
    public void printText(View v)
    {
        try
        {
            String startTime = getCurTime();
            Toast.makeText(requireContext(), getResources().getString(R.string.print_start_time) + startTime, Toast.LENGTH_LONG).show();
            printerDev.printText(new ArrayList<PrintItemObj>()
            {
                {
                    add(new PrintItemObj("默认打印数据测试"));
                    add(new PrintItemObj("默认打印数据测试"));
                    add(new PrintItemObj("默认打印数据测试"));
                    add(new PrintItemObj("打印数据字体放大", 24));
                    add(new PrintItemObj("打印数据字体放大", 24));
                    add(new PrintItemObj("打印数据字体放大", 24));
                    add(new PrintItemObj("打印数据加粗", 8, true));
                    add(new PrintItemObj("打印数据加粗", 8, true));
                    add(new PrintItemObj("打印数据加粗", 8, true));
                    add(new PrintItemObj("打印数据左对齐测试", 8, false, PrintItemObj.ALIGN.LEFT));
                    add(new PrintItemObj("打印数据左对齐测试", 8, false, PrintItemObj.ALIGN.LEFT));
                    add(new PrintItemObj("打印数据左对齐测试", 8, false, PrintItemObj.ALIGN.LEFT));
                    add(new PrintItemObj("打印数据居中对齐测试", 8, false, PrintItemObj.ALIGN.CENTER));
                    add(new PrintItemObj("打印数据居中对齐测试", 8, false, PrintItemObj.ALIGN.CENTER));
                    add(new PrintItemObj("打印数据居中对齐测试", 8, false, PrintItemObj.ALIGN.CENTER));
                    add(new PrintItemObj("打印数据右对齐测试", 8, false, PrintItemObj.ALIGN.RIGHT));
                    add(new PrintItemObj("打印数据右对齐测试", 8, false, PrintItemObj.ALIGN.RIGHT));
                    add(new PrintItemObj("打印数据右对齐测试", 8, false, PrintItemObj.ALIGN.RIGHT));
                    add(new PrintItemObj("打印数据下划线", 8, false, PrintItemObj.ALIGN.LEFT, true));
                    add(new PrintItemObj("打印数据下划线", 8, false, PrintItemObj.ALIGN.LEFT, true));
                    add(new PrintItemObj("打印数据下划线", 8, false, PrintItemObj.ALIGN.LEFT, true));
                    add(new PrintItemObj("打印数据不换行测试打印数据不换行测试打印数据不换行测试", 8, false, PrintItemObj.ALIGN.LEFT, false, true));
                    add(new PrintItemObj("打印数据不换行测试打印数据不换行测试打印数据不换行测试", 8, false, PrintItemObj.ALIGN.LEFT, false, false));
                    add(new PrintItemObj("打印数据不换行测试", 8, false, PrintItemObj.ALIGN.LEFT, false, false));
                    add(new PrintItemObj("打印数据行间距测试", 8, false, PrintItemObj.ALIGN.LEFT, false, true, 40));
                    add(new PrintItemObj("打印数据行间距测试", 8, false, PrintItemObj.ALIGN.LEFT, false, true, 83));
                    add(new PrintItemObj("打印数据行间距测试", 8, false, PrintItemObj.ALIGN.LEFT, false, true, 40));
                    add(new PrintItemObj("打印数据字符间距测试", 8, false, PrintItemObj.ALIGN.LEFT, false, true, 29, 25));
                    add(new PrintItemObj("打印数据字符间距测试", 8, false, PrintItemObj.ALIGN.LEFT, false, true, 29, 25));
                    add(new PrintItemObj("打印数据字符间距测试", 8, false, PrintItemObj.ALIGN.LEFT, false, true, 29, 25));
                    add(new PrintItemObj("打印数据左边距测试", 8, false, PrintItemObj.ALIGN.LEFT, false, true, 29, 0, 40));
                    add(new PrintItemObj("打印数据左边距测试", 8, false, PrintItemObj.ALIGN.LEFT, false, true, 29, 0, 40));
                    add(new PrintItemObj("打印数据左边距测试", 8, false, PrintItemObj.ALIGN.LEFT, false, true, 29, 0, 40));
                    add(new PrintItemObj("\n\n\n"));
                }
            }, new AidlPrinterListener.Stub()
            {

                @Override
                public void onPrintFinish() throws RemoteException
                {
                    String endTime = getCurTime();
                    Toast.makeText(requireContext(), getResources().getString(R.string.print_end_time) + endTime, Toast.LENGTH_LONG).show();
                }

                @Override
                public void onError(int arg0) throws RemoteException
                {
                    Toast.makeText(requireContext(), getResources().getString(R.string.print_error_code) + arg0, Toast.LENGTH_LONG).show();
                }
            });
        }
        catch (RemoteException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * 打印位图
     *
     * @createtor：Administrator
     * @date:2015-8-4 下午2:39:33
     */
    public void printBitmap(Bitmap bitmap)
    {
        try
        {
            String startTime = getCurTime();
            Toast.makeText(requireContext(), getResources().getString(R.string.print_start_time) + startTime, Toast.LENGTH_LONG).show();
            this.printerDev.printBmp(100, bitmap.getWidth(), bitmap.getHeight(), bitmap, new AidlPrinterListener.Stub()
            {

                @Override
                public void onPrintFinish() throws RemoteException
                {
                    String endTime = getCurTime();
                    Toast.makeText(requireContext(), getResources().getString(R.string.print_end_time) + endTime, Toast.LENGTH_LONG).show();
                }

                @Override
                public void onError(int arg0) throws RemoteException
                {
                    Toast.makeText(requireContext(), getResources().getString(R.string.print_error_code) + arg0, Toast.LENGTH_LONG).show();
                }
            });

            this.printerDev.printText(new ArrayList<PrintItemObj>()
            {
                {
                    add(new PrintItemObj("\n"));
                }
            }, new PrintStateChangeListener());

        }
        catch (RemoteException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * Template打印
     *
     * @param
     * @createtor：Administrator
     * @date:2019-9-7
     */
    public void printTemplate()
    {
        try
        {
            Typeface typeface = Typeface.createFromFile("/system/fonts/NSimSun.ttf");

        }
        catch (Exception e)
        {
            Log.d("caixh", "font is not exist!");
            e.printStackTrace();
        }
        try
        {
            Typeface typeface = ResourcesCompat.getFont(requireContext(), R.font.hwzs);
            String startTime = getCurTime();
            Toast.makeText(requireContext(), getResources().getString(R.string.print_start_time) + startTime, Toast.LENGTH_LONG).show();
            //showMessage(new StringBuffer("默认打印数据").reverse().toString());
            PrintTemplate template = new PrintTemplate(requireContext(), typeface);
            //template.setStrokeWidth(0.1f);
            int textSize = TextUnit.TextSize.SMALL;
            template.add(new TextUnit("Привет приятно познакомиться", TextUnit.TextSize.NORMAL, Align.CENTER).setLineSpacing(10));
            template.add(new TextUnit("Пример печати", TextUnit.TextSize.NORMAL, Align.CENTER));
            template.add(new TextUnit("کد ترمینال ۷۱۲۶۷۸۳۲", TextUnit.TextSize.SMALL, Align.RIGHT));

            template.add(new TextUnit("中国银联签购单", textSize, Align.CENTER));
            template.add(new TextUnit("--------------------------------------------------------------------").setWordWrap(false));
            template.add(new TextUnit("商户名称：鼎智开发测试专用", textSize));
            template.add(new TextUnit("商户名称：鼎智开发测试专用", textSize));
            template.add(new TextUnit("商户号：123456789012345", textSize).setBold(true));
            template.add(new TextUnit("终端号：12345678", textSize));
            template.add(new TextUnit("操作员号：01", textSize));
            template.add(new TextUnit("发卡行：招商银行", textSize));
            template.add(new TextUnit("卡号：", textSize));
            template.add(new TextUnit("6214 61** **** 6526", textSize, Align.CENTER).setBold(true));
            template.add(1, new TextUnit("交易：", textSize),
                    2, new TextUnit("消费(SALE)", textSize, Align.CENTER).setBold(true));
            template.add(new TextUnit("批次号：000001", textSize));
            template.add(new TextUnit("凭证号：000008", textSize));
            template.add(new TextUnit("授权码：", textSize));
            template.add(new TextUnit("参考号：015555323233", textSize));
            template.add(new TextUnit("清算日期：0830", textSize));
            template.add(new TextUnit("日期/时间(DATE/TIME)：", textSize));
            template.add(new TextUnit("2019/08/30 16:16:18", textSize));
            template.add(1, new TextUnit("金额(AMOUNT):", textSize),
                    1, new TextUnit("RMB 8.88", textSize).setBold(true));
            template.add(new TextUnit("--------------------------------------------------------------------").setWordWrap(false));
            template.add(new TextUnit("备注(REFERENCE)：0830", textSize));
            template.add(new TextUnit("订单编号: 3465767899", textSize));
            template.add(new TextUnit("------------------------------------------").setWordWrap(false));
            template.add(new TextUnit("本人确认将以上交易，同意将其计入本卡账户\n\n\n", textSize));
            template.add(new TextUnit("打印数据字体放大", TextUnit.TextSize.NORMAL));
            template.add(new TextUnit("打印数据左对齐测试", TextUnit.TextSize.NORMAL));
            template.add(new TextUnit("打印数据居中对齐测试", TextUnit.TextSize.NORMAL, Align.CENTER));
            template.add(new TextUnit("打印数据右对齐测试", TextUnit.TextSize.NORMAL, Align.RIGHT));
            template.add(1, new TextUnit("金 额：", TextUnit.TextSize.NORMAL, Align.LEFT),
                    1, new TextUnit("1.00元", TextUnit.TextSize.NORMAL, Align.RIGHT));//.setBold(true)
            template.add(1, new TextUnit("金 额", TextUnit.TextSize.NORMAL, Align.LEFT),//.setBold(true)
                    1, new TextUnit("价 格", TextUnit.TextSize.NORMAL, Align.CENTER),//.setBold(true)
                    1, new TextUnit("数 量", TextUnit.TextSize.NORMAL, Align.RIGHT));//.setBold(true)
            template.add(1, new TextUnit("1.00", TextUnit.TextSize.NORMAL, Align.LEFT),
                    1, new TextUnit("2.00", TextUnit.TextSize.NORMAL, Align.CENTER),
                    1, new TextUnit("121", TextUnit.TextSize.NORMAL, Align.RIGHT));
            template.add(1, new TextUnit("2.00", TextUnit.TextSize.NORMAL, Align.LEFT),
                    1, new TextUnit("3.00", TextUnit.TextSize.NORMAL, Align.CENTER),
                    1, new TextUnit("111", TextUnit.TextSize.NORMAL, Align.RIGHT));
            template.add(new TextUnit("\n\n\n\n"));
            Bitmap bitmap = QRCodeUtil.createQRImage("asdfggfffffsshhheeed", 190, 190, null);
            List<TextUnit> list = new ArrayList<TextUnit>();
            list.add(new TextUnit("默认打印数据测试1"));
            list.add(new TextUnit("默认打印数据测试2", TextUnit.TextSize.NORMAL));
            template.add(new ImageUnit(bitmap, 190, 190), list);
            template.add(list, new ImageUnit(bitmap, 190, 190));
            printerDev.addRuiImage(template.getPrintBitmap(), 0);
            printerDev.printRuiQueue(mListen);
        }
        catch (RemoteException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private PrintTemplate getFoodItemTemplate(String itemName,String qty,String price,String amount)
    {
        StringBuilder strRightSideText = new StringBuilder();
        strRightSideText.append(qty).append(" x ").append(price).append("    ").append(amount);
        PrintTemplate printTemplate = new PrintTemplate(requireContext(),typeface);
        printTemplate.add(1,new TextUnit(itemName,TextUnit.TextSize.SMALL,Align.LEFT),
                          1,new TextUnit(strRightSideText.toString(),TextUnit.TextSize.SMALL,Align.RIGHT));
        return printTemplate;
    }

    private PrintTemplate getChargeTemplate(String chargeName,String amount)
    {
        PrintTemplate printTemplate = new PrintTemplate(requireContext(),typeface);
        printTemplate.add(1,new TextUnit(chargeName,TextUnit.TextSize.SMALL,Align.LEFT),
                1,new TextUnit(amount,TextUnit.TextSize.SMALL,Align.RIGHT));
        return printTemplate;
    }

    private String getSpecialInstructions()
    {
        StringBuilder strSpecialInstructions = new StringBuilder();
        strSpecialInstructions.append(getString(R.string.lbl_special_instructions_colon)).append("\n");
        if(orderDetails.getSpecialInstructions()!=null)
        {
            if(!orderDetails.getSpecialInstructions().isEmpty())
            {
                strSpecialInstructions.append(orderDetails.getAllergyInstructions());
                strSpecialInstructions.append("\n");
            }
            else
            {
                strSpecialInstructions.append("NA").append("\n");
            }
        }
        else
        {
            strSpecialInstructions.append("NA").append("\n");
        }
        return strSpecialInstructions.toString();
    }

    private String getAllergyInstructions()
    {
        StringBuilder strAllergyInstructions = new StringBuilder();
        strAllergyInstructions.append(getString(R.string.lbl_allergy_instructions_colon)).append("\n");
        if(orderDetails.getAllergyInstructions()!=null)
        {
            if(!orderDetails.getAllergyInstructions().isEmpty())
            {
                strAllergyInstructions.append(orderDetails.getAllergyInstructions());
                strAllergyInstructions.append("\n");
            }
            else
            {
                strAllergyInstructions.append("NA").append("\n");
            }
        }
        else
        {
            strAllergyInstructions.append("NA").append("\n");
        }
        return strAllergyInstructions.toString();

    }

    private void printReceipt()
    {
        try
        {
            Typeface typeface = ResourcesCompat.getFont(requireContext(), R.font.hwzs);
            String startTime = getCurTime();
            Toast.makeText(requireContext(), getResources().getString(R.string.print_start_time) + startTime, Toast.LENGTH_LONG).show();
            //showMessage(new StringBuffer("默认打印数据").reverse().toString());
            PrintTemplate template = new PrintTemplate(requireContext(), typeface);
            //template.setStrokeWidth(0.1f);
            int textSize = TextUnit.TextSize.NORMAL;

            template.add(new TextUnit(orderDetails.getBusinessName(), TextUnit.TextSize.LARGE, Align.CENTER).setLineSpacing(10));

            StringBuilder strOrderId = new StringBuilder();
            strOrderId.append(getString(R.string.lbl_order_no_colon)).append(orderDetails.getOrderId());
            template.add(new TextUnit(strOrderId.toString(), TextUnit.TextSize.LARGE, Align.LEFT).setLineSpacing(10));

            template.add(new TextUnit(getString(R.string.lbl_ordered_items), TextUnit.TextSize.LARGE, Align.LEFT).setLineSpacing(10));

            if(orderDetails.getFoodItems()!=null)
            {
                if(orderDetails.getFoodItems().size()>0)
                {
                    for(int i=0;i<orderDetails.getFoodItems().size();i++)
                    {
                        FoodItem foodItem = orderDetails.getFoodItems().get(i);

                        if(foodItem!=null)
                        {
                            double price = Double.parseDouble(foodItem.getSingleQuantityPrice());
                            String strPrice = (String.format("%,.2f", price));

                            double quantity = Double.parseDouble(foodItem.getQuantity());
                            double amount = quantity * price;
                            String strAmount = (String.format("%,.2f", amount));

                            if(foodItem.getItemName()!=null)
                            {
                                StringBuilder strItemRow = new StringBuilder();
                                String foodItemName = foodItem.getItemName();
                                String foodItemNameFirst,secondName="";
                                if(foodItemName.length()>15)
                                {
                                    foodItemNameFirst = foodItemName.substring(0,15);
                                    StringBuilder strSecondName = new StringBuilder();
                                    strSecondName.append("-").append(foodItemName.substring(16,foodItemName.length()));
                                    secondName=strSecondName.toString();
                                }
                                foodItemNameFirst=foodItemName;
                                StringBuilder strRightSideText = new StringBuilder();
                                strRightSideText.append((int)quantity).append(" x ").append(price)
                                        .append(" ").append(strCurrencySymbol).append(strAmount);

                                template.add(1,new TextUnit(foodItemName,TextUnit.TextSize.NORMAL,Align.LEFT),
                                        1,new TextUnit(strRightSideText.toString(),TextUnit.TextSize.NORMAL,Align.RIGHT));


                                /*if(!secondName.equals(""))
                                {
                                    template.add(new TextUnit(getFormattedString(secondName,""), TextUnit.TextSize.SMALL, Align.LEFT).setLineSpacing(10));
                                }*/
                            }
                        }
                    }
                }
            }

            double orderTotal = (Double.parseDouble(orderDetails.getOrderTotal()));
            String strOrderTotal = String.format("%,.2f", orderTotal);
            StringBuilder strSubTotal = new StringBuilder();
            strSubTotal.append(strCurrencySymbol).append(strOrderTotal);
            template.add(1,new TextUnit(getString(R.string.lbl_sub_total),TextUnit.TextSize.NORMAL,Align.LEFT),
                         1,new TextUnit(strSubTotal.toString(),TextUnit.TextSize.NORMAL,Align.RIGHT));


            double discountAmount = (Double.parseDouble(orderDetails.getDiscountLtAmount()));
            String strDiscountAmount = String.format("%,.2f", discountAmount);
            StringBuilder strTokenApply = new StringBuilder();
            strTokenApply.append("-").append(strCurrencyLetter).append(strDiscountAmount);
            template.add(1,new TextUnit(getString(R.string.lbl_token_applied),TextUnit.TextSize.NORMAL,Align.LEFT),
                         1,new TextUnit(strTokenApply.toString(),TextUnit.TextSize.NORMAL,Align.RIGHT));

            double deliveryCharges = (Double.parseDouble(orderDetails.getDeliveryRate()));
            String strDeliveryCharges = String.format("%,.2f", deliveryCharges);
            StringBuilder strDeliveryChargesTwo = new StringBuilder();
            strDeliveryChargesTwo.append("+").append(strCurrencySymbol).append(strDeliveryCharges);
            template.add(1,new TextUnit(getString(R.string.lbl_delivery_fee),TextUnit.TextSize.NORMAL,Align.LEFT),
                         1,new TextUnit(strDeliveryChargesTwo.toString(),TextUnit.TextSize.NORMAL,Align.RIGHT));

            double finalOrderTotal = (Double.parseDouble(orderDetails.getFinalOrderTotal()));
            String strFinalOrderTotal = String.format("%,.2f", finalOrderTotal);
            StringBuilder strFinalPay = new StringBuilder();
            strFinalPay.append(strCurrencySymbol).append(strFinalOrderTotal);
            template.add(1,new TextUnit(getString(R.string.lbl_final_payment),TextUnit.TextSize.NORMAL,Align.LEFT),
                         1,new TextUnit(strFinalPay.toString(),TextUnit.TextSize.NORMAL,Align.RIGHT));

            for(int i=0;i<3;i++)
            {
                template.add(1,new TextUnit("",TextUnit.TextSize.NORMAL,Align.LEFT),
                        1,new TextUnit("",TextUnit.TextSize.NORMAL,Align.RIGHT));

            }
            if(signInResult!=null)
            {
                switch (signInResult.getRetailerType())
                {
                    case AppConstants.RETAILER_TYPE_EATS:
                         template.add(new TextUnit(getAllergyInstructions(), TextUnit.TextSize.LARGE, Align.LEFT).setLineSpacing(10));
                         template.add(new TextUnit(getSpecialInstructions(), TextUnit.TextSize.LARGE, Align.LEFT).setLineSpacing(10));
                         break;
                    case AppConstants.RETAILER_TYPE_LOCAL:
                         template.add(new TextUnit(getSpecialInstructions(), TextUnit.TextSize.LARGE, Align.LEFT).setLineSpacing(10));
                         break;
                }
            }
            for(int i=0;i<8;i++)
            {
                template.add(1,new TextUnit("",TextUnit.TextSize.NORMAL,Align.LEFT),
                        1,new TextUnit("",TextUnit.TextSize.NORMAL,Align.RIGHT));

            }
            printerDev.addRuiImage(template.getPrintBitmap(), 0);
            printerDev.printRuiQueue(mListen);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    /*private void printReceipt()
    {
        try
        {
            Typeface typeface = ResourcesCompat.getFont(requireContext(), R.font.hwzs);
            String startTime = getCurTime();
            Toast.makeText(requireContext(), getResources().getString(R.string.print_start_time) + startTime, Toast.LENGTH_LONG).show();
            //showMessage(new StringBuffer("默认打印数据").reverse().toString());
            PrintTemplate template = new PrintTemplate(requireContext(), typeface);
            //template.setStrokeWidth(0.1f);
            int textSize = TextUnit.TextSize.SMALL;

            template.add(new TextUnit(orderDetails.getBusinessName(), TextUnit.TextSize.NORMAL, Align.CENTER).setLineSpacing(10));

            StringBuilder strOrderId = new StringBuilder();
            strOrderId.append(getString(R.string.lbl_order_no_colon)).append(orderDetails.getOrderId());
            template.add(new TextUnit(strOrderId.toString(), TextUnit.TextSize.NORMAL, Align.LEFT).setLineSpacing(10));

            template.add(new TextUnit(getString(R.string.lbl_ordered_items), TextUnit.TextSize.NORMAL, Align.LEFT).setLineSpacing(10));

            if(orderDetails.getFoodItems()!=null)
            {
                if(orderDetails.getFoodItems().size()>0)
                {
                    for(int i=0;i<orderDetails.getFoodItems().size();i++)
                    {
                        FoodItem foodItem = orderDetails.getFoodItems().get(i);

                        if(foodItem!=null)
                        {
                            double price = Double.parseDouble(foodItem.getSingleQuantityPrice());
                            String strPrice = (String.format("%,.2f", price));

                            double quantity = Double.parseDouble(foodItem.getQuantity());
                            double amount = quantity * price;
                            String strAmount = (String.format("%,.2f", amount));

                            if(foodItem.getItemName()!=null)
                            {
                                StringBuilder strItemRow = new StringBuilder();
                                String foodItemName = foodItem.getItemName();
                                String foodItemNameFirst,secondName="";
                                if(foodItemName.length()>15)
                                {
                                    foodItemNameFirst = foodItemName.substring(0,15);
                                    StringBuilder strSecondName = new StringBuilder();
                                    strSecondName.append("-").append(foodItemName.substring(16,foodItemName.length()));
                                    secondName=strSecondName.toString();
                                }
                                foodItemNameFirst=foodItemName;
                                strItemRow.append(foodItem.getQuantity()).append("x").append(strPrice).append(" ");
                                strItemRow.append(strAmount);

                                template.add(new TextUnit(getFormattedString(foodItemNameFirst,strItemRow.toString()), TextUnit.TextSize.SMALL, Align.LEFT).setLineSpacing(10));
                                if(!secondName.equals(""))
                                {
                                    template.add(new TextUnit(getFormattedString(secondName,""), TextUnit.TextSize.SMALL, Align.LEFT).setLineSpacing(10));
                                }
                            }
                        }
                    }
                }
            }

            double orderTotal = (Double.parseDouble(orderDetails.getOrderTotal()));
            String strOrderTotal = String.format("%,.2f", orderTotal);
            StringBuilder strSubTotal = new StringBuilder();
            strSubTotal.append(strCurrencySymbol).append(strOrderTotal);
            template.add(new TextUnit(getFormattedString(getString(R.string.lbl_sub_total),strSubTotal.toString()), TextUnit.TextSize.SMALL, Align.LEFT).setLineSpacing(10));

            double discountAmount = (Double.parseDouble(orderDetails.getDiscountLtAmount()));
            String strDiscountAmount = String.format("%,.2f", discountAmount);
            StringBuilder strTokenApply = new StringBuilder();
            strTokenApply.append("-").append(strCurrencyLetter).append(strDiscountAmount);
            template.add(new TextUnit(getFormattedString(getString(R.string.lbl_token_applied),strTokenApply.toString()), TextUnit.TextSize.SMALL, Align.LEFT).setLineSpacing(10));

            double deliveryCharges = (Double.parseDouble(orderDetails.getDeliveryRate()));
            String strDeliveryCharges = String.format("%,.2f", deliveryCharges);
            StringBuilder strDeliveryChargesTwo = new StringBuilder();
            strDeliveryChargesTwo.append("+").append(strCurrencySymbol).append(strDeliveryCharges);
            template.add(new TextUnit(getFormattedString(getString(R.string.lbl_delivery_fee),strDeliveryChargesTwo.toString()), TextUnit.TextSize.SMALL, Align.LEFT).setLineSpacing(10));

            double finalOrderTotal = (Double.parseDouble(orderDetails.getFinalOrderTotal()));
            String strFinalOrderTotal = String.format("%,.2f", finalOrderTotal);
            StringBuilder strFinalPay = new StringBuilder();
            strFinalPay.append(strCurrencySymbol).append(strFinalOrderTotal);
            template.add(new TextUnit(getFormattedString(getString(R.string.lbl_final_payment),strFinalPay.toString()), TextUnit.TextSize.SMALL, Align.LEFT).setLineSpacing(10));
            printerDev.addRuiImage(template.getPrintBitmap(), 0);
            printerDev.printRuiQueue(mListen);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }*/

    private String getCurTime()
    {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a");
        return simpleDateFormat.format(calendar.getTime());
    }

    private class PrintStateChangeListener extends AidlPrinterListener.Stub
    {

        @Override
        public void onError(int arg0) throws RemoteException
        {
            //Toast.makeText(requireContext(), getResources().getString(R.string.print_error_code) + arg0, Toast.LENGTH_LONG).show();
        }

        @Override
        public void onPrintFinish() throws RemoteException
        {
            //String endTime = getCurTime();
            //Toast.makeText(requireContext(), getResources().getString(R.string.print_end_time) + endTime, Toast.LENGTH_LONG).show();
        }

    }

    AidlPrinterListener mListen = new AidlPrinterListener.Stub()
    {
        @Override
        public void onError(int i) throws RemoteException
        {
            //Toast.makeText(requireContext(), getResources().getString(R.string.print_error_code) + i, Toast.LENGTH_LONG).show();
        }

        @Override
        public void onPrintFinish() throws RemoteException
        {
            //Toast.makeText(requireContext(), getResources().getString(R.string.print_success), Toast.LENGTH_LONG).show();
        }
    };

    //设别服务连接桥
    private ServiceConnection conn = new ServiceConnection()
    {

        @Override
        public void onServiceConnected(ComponentName name, IBinder serviceBinder)
        {
            Log.d(TAG, "aidlService服务连接成功");
            if (serviceBinder != null)
            {    //绑定成功
                AidlDeviceService serviceManager = AidlDeviceService.Stub.asInterface(serviceBinder);
                onDeviceConnected(serviceManager);
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name)
        {
            Log.d(TAG, "AidlService服务断开了");
        }
    };

    //绑定服务
    public void bindService()
    {
        Intent intent = new Intent();
        intent.setAction(TOPWISE_SERVICE_ACTION);
        final Intent eintent = new Intent(createExplicitFromImplicitIntent(requireContext(), intent));
        boolean flag = requireContext().bindService(eintent, conn, Context.BIND_AUTO_CREATE);
        if (flag)
        {
            Log.d(TAG, "服务绑定成功");
        }
        else
        {
            Log.d(TAG, "服务绑定失败");
        }
    }

    public void onDeviceConnected(AidlDeviceService serviceManager)
    {
        try
        {
            printerDev = AidlPrinter.Stub.asInterface(serviceManager.getPrinter());
        }
        catch (RemoteException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}