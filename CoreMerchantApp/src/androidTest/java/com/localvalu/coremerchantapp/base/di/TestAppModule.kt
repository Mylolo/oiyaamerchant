package com.localvalu.coremerchantapp.base.di

import android.content.Context
import com.google.gson.Gson
import com.localvalu.coremerchantapp.LocalValuApplication
import com.localvalu.coremerchantapp.network.RemoteDataSource
import com.localvalu.coremerchantapp.storage.UserPreferences
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object TestAppModule
{
    @Provides
    fun provideApplication(application: LocalValuApplication): LocalValuApplication = application

    @Provides
    @Singleton
    fun providesPreferences(@ApplicationContext context: Context) : UserPreferences
    {
        return UserPreferences(context)
    }

    @Provides
    @Singleton
    fun provideGson(): Gson
    {
        return Gson()
    }

    @Singleton
    @Provides
    fun providesRemoteDataSource(): RemoteDataSource
    {
        return RemoteDataSource()
    }
}