package com.localvalu.coremerchantapp.dashboard.ui

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.localvalu.coremerchantapp.R
import com.localvalu.coremerchantapp.launchFragmentInHiltContainer
import com.localvalu.coremerchantapp.merchantdashboard.api.MerchantDashboardApi
import com.localvalu.coremerchantapp.merchantdashboard.repository.MerchantDashboardRepository
import com.localvalu.coremerchantapp.merchantdashboard.ui.DashboardFragment
import com.localvalu.coremerchantapp.merchantdashboard.viewmodel.MerchantDashboardViewModel
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock

@ExperimentalCoroutinesApi
@HiltAndroidTest
class DashboardFragmentTest
{
    @get:Rule
    val hiltRule = HiltAndroidRule(this)

    @Before
    fun setUp()
    {
        hiltRule.inject()
    }

    @Test
    fun isAllDataVisible()
    {
        val scenario = launchFragmentInHiltContainer<DashboardFragment>()
        isVisible(R.id.clMerchantDashboardLayoutRoot)
        isVisible(R.id.cnlContent)
        isVisible(R.id.tvTextCurrentMonthOiyaaOffer)
        isVisible(R.id.tvTextSales)
        isVisible(R.id.tvTextTokensAwarded)
        isVisible(R.id.tvTextNumberOfTransactions)
        isVisible(R.id.tvTextOiyaaPromotionSummary)
        isVisible(R.id.tvTextSalesOiyaaPromotionSummary)
        isVisible(R.id.tvTextTokensAwardedPromotionSummary)
        isVisible(R.id.tvTextNumberOfTransactionsPromotionSummary)
        isVisible(R.id.tvTextTokenAccountDetails)
        isVisible(R.id.tvTextTokenBalance)
        isVisible(R.id.tvTextTokenOverDraft)
    }

    private fun isVisible(id:Int)
    {
        onView(withId(id)).check(ViewAssertions.matches(isDisplayed()))
    }

}