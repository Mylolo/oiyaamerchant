package com.localvalu.coremerchantapp;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.localvalu.coremerchantapp.data.base.RestBasedErrorDetail;
import com.localvalu.coremerchantapp.data.base.RestBasedOutput;
import com.localvalu.coremerchantapp.data.input.DriversListInput;
import com.localvalu.coremerchantapp.data.input.DriversProfileInput;
import com.localvalu.coremerchantapp.data.input.InsertVisitEntryInput;
import com.localvalu.coremerchantapp.data.input.MerchantSelfServeInput;
import com.localvalu.coremerchantapp.data.input.MyAppointmentListInput;
import com.localvalu.coremerchantapp.data.input.MyAppointmentStatusChangeInput;
import com.localvalu.coremerchantapp.data.input.MyTransactionInput;
import com.localvalu.coremerchantapp.data.input.NotifyToDriversInput;
import com.localvalu.coremerchantapp.data.input.OrderDetailsInput;
import com.localvalu.coremerchantapp.data.input.OrderListInput;
import com.localvalu.coremerchantapp.data.input.OrderStatusChangeInput;
import com.localvalu.coremerchantapp.data.input.SaleTransactionInput;
import com.localvalu.coremerchantapp.data.input.SignInInput;
import com.localvalu.coremerchantapp.data.input.TableBookingListInput;
import com.localvalu.coremerchantapp.data.input.TableBookingStatusChangeInput;
import com.localvalu.coremerchantapp.data.input.TokenBalanceInput;
import com.localvalu.coremerchantapp.data.input.TransferLoyaltyTokenInput;
import com.localvalu.coremerchantapp.data.input.VisitEntryHistoryInput;
import com.localvalu.coremerchantapp.data.output.MyTransactionOutput;
import com.localvalu.coremerchantapp.data.output.MyTransactionResult;
import com.localvalu.coremerchantapp.data.output.SaleTransactionOutput;
import com.localvalu.coremerchantapp.data.output.SaleTransactionResult;
import com.localvalu.coremerchantapp.data.output.SignInOutput;
import com.localvalu.coremerchantapp.data.output.SignInResult;
import com.localvalu.coremerchantapp.data.output.TokenBalanceOutput;
import com.localvalu.coremerchantapp.data.output.TokenBalanceResult;
import com.localvalu.coremerchantapp.data.output.TransferLoyaltyTokenOutput;
import com.localvalu.coremerchantapp.data.output.TransferLoyaltyTokenResult;
import com.localvalu.coremerchantapp.data.output.driverOld.DriverListResult;
import com.localvalu.coremerchantapp.data.output.driverOld.DriverList;
import com.localvalu.coremerchantapp.data.output.driverOld.DriverNotificationResult;
import com.localvalu.coremerchantapp.data.output.driverOld.DriverNotifictionResultOutput;
import com.localvalu.coremerchantapp.data.output.driverOld.DriverProfileOutput;
import com.localvalu.coremerchantapp.data.output.driverOld.DriverProfileResult;
import com.localvalu.coremerchantapp.data.output.myappointments.MyAppointmentListOutput;
import com.localvalu.coremerchantapp.data.output.myappointments.MyAppointmentListResult;
import com.localvalu.coremerchantapp.data.output.myappointments.MyAppointmentStatusChangeOutput;
import com.localvalu.coremerchantapp.data.output.myappointments.MyAppointmentStatusChangeResult;
import com.localvalu.coremerchantapp.data.output.order.OrderDetailsResult;
import com.localvalu.coremerchantapp.data.output.order.OrderDetailsResultOutput;
import com.localvalu.coremerchantapp.data.output.order.OrderListOutput;
import com.localvalu.coremerchantapp.data.output.order.OrderListResult;
import com.localvalu.coremerchantapp.data.output.order.OrderStatusChangeOutput;
import com.localvalu.coremerchantapp.data.output.order.OrderStatusChangeResult;
import com.localvalu.coremerchantapp.data.output.selfserve.MerchantSelfServeOutput;
import com.localvalu.coremerchantapp.data.output.selfserve.MerchantSelfServeResult;
import com.localvalu.coremerchantapp.data.output.tablebooking.TableBookingListOutput;
import com.localvalu.coremerchantapp.data.output.tablebooking.TableBookingListResult;
import com.localvalu.coremerchantapp.data.output.tablebooking.TableBookingStatusChangeOutput;
import com.localvalu.coremerchantapp.data.output.tablebooking.TableBookingStatusChangeResult;
import com.localvalu.coremerchantapp.data.output.trace.TraceHistoryReportResult;
import com.localvalu.coremerchantapp.data.output.trace.TraceHistoryReportResultOutput;
import com.localvalu.coremerchantapp.data.output.trace.VisitEntryResult;
import com.localvalu.coremerchantapp.data.output.trace.VisitEntryResultOutput;
import com.utils.base.AppBaseActivity;
import com.utils.base.AppBaseFragment;
import com.utils.base.Task;
import com.utils.helper.preference.PreferenceFactory;
import com.utils.helper.toast.ToastFactory;
import com.utils.rest.RestHelper;
import com.utils.rest.RestResultReceiver;
import com.utils.validator.Validator;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class LocalValuActivity extends AppBaseActivity implements LocalValuConstants, RestHandler
{

    private static final String TAG = LocalValuActivity.class.getSimpleName();

    private static final int GPS_REQUEST_CODE = 1000; // arbitrary code

    protected Fragment currentFragment = null;

    private boolean googlePlayerServiceSupport = false;

    public <T extends AppBaseFragment> T getFragment(String tag, Class<T> fragment)
    {
        AppBaseFragment appBaseFragment = super.getFragment((Class<AppBaseFragment>) fragment);
        return (T) appBaseFragment;
    }

    public void updateFragment(int containerViewId, Fragment fragment, boolean addToBackStack, boolean replace)
    {
        currentFragment = fragment;
        super.updateFragmentView(containerViewId, fragment, addToBackStack, replace);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void init()
    {

    }

    @Override
    protected void onResume()
    {
        super.onResume();
    }


    public void showToast(String message)
    {
        View view = LayoutInflater.from(this).inflate(R.layout.view_app_toast, null, false);
        TextView textView = view.findViewById(R.id.app_toast_textView);
        textView.setText(message);

        ToastFactory toastFactory = super.getToastFactory();
        toastFactory.setCustomView(true);
        toastFactory.setView(view);
        toastFactory.toast(message);
    }

    @Override
    public void callSignIn(String fcmId, String email, String password, Task<SignInResult> task)
    {
        if (!Validator.isValid(email))
        {
            //showToast("FCM ID Not Found");
            return;
        }
        if (!Validator.isValid(email))
        {
            showToast("Enter email");
            return;
        }

        if (!Validator.isValidEmail(email))
        {
            showToast("Enter valid email");
            return;
        }

        if (!Validator.isValid(password))
        {
            showToast("Enter password");
            return;
        }

        showProgressBar();

        SignInInput signInInput = new SignInInput();
        signInInput.setEmail(email);
        signInInput.setPassword(password);
        signInInput.setFcmId(fcmId);

        RestHelper restHelper = new RestHelper(BuildConfig.URL_BASE_DOTNET);
        restHelper.sendPost("BusinessMemberVerificationV1", signInInput, SignInOutput.class, (RestResultReceiver<SignInOutput>) data ->
        {
            dismissProgressBar();
            validateResult(Validator.isValid(data) ? data.getResult() : null, task);
        });
    }

    @Override
    public void callTokenBalance(Task<TokenBalanceResult> task)
    {
        showProgressBar();

        TokenBalanceInput tokenBalanceInput = new TokenBalanceInput();
        tokenBalanceInput.setRetailerId(Integer.parseInt(PreferenceFactory.getInstance().getString(PREF_RETAILER_ID)));
        tokenBalanceInput.setAccountId(Integer.parseInt(PreferenceFactory.getInstance().getString(PREF_ACCOUNT_ID)));

        RestHelper restHelper = new RestHelper(BuildConfig.URL_BASE_DOTNET);
        restHelper.sendPost("RetailerBalance", tokenBalanceInput, TokenBalanceOutput.class, (RestResultReceiver<TokenBalanceOutput>) data ->
        {
            dismissProgressBar();
            validateResult(Validator.isValid(data) ? data.getResult() : null, task);
        });
    }

    @Override
    public void callSaleTransaction(String qrCode, Double saleTotal, Task<SaleTransactionResult> task)
    {
        if (!Validator.isValid(qrCode))
        {
            showToast("Enter QR code");
            return;
        }

        showProgressBar();

        SaleTransactionInput saleTransactionInput = new SaleTransactionInput();
        saleTransactionInput.setAccountId(Integer.parseInt(PreferenceFactory.getInstance().getString(PREF_RETAILER_ID)));
        saleTransactionInput.setRetailerID(qrCode);
        saleTransactionInput.setSaleTotal(saleTotal);

        RestHelper restHelper = new RestHelper(BuildConfig.URL_BASE_DOTNET);
        restHelper.sendPost("MerchantAppSaleTransaction", saleTransactionInput, SaleTransactionOutput.class, (RestResultReceiver<SaleTransactionOutput>) data ->
        {
            dismissProgressBar();
            validateResult(Validator.isValid(data) ? data.getResult() : null, task);
        });
    }

    @Override
    public void callMyTransaction(String fromDate, String toDate, Task<MyTransactionResult> task)
    {
        showProgressBar();

        MyTransactionInput myTransactionInput = new MyTransactionInput();
        myTransactionInput.setMerchantId(Integer.parseInt(PreferenceFactory.getInstance().getString(PREF_RETAILER_ID)));
        myTransactionInput.setAccountId(Integer.parseInt(PreferenceFactory.getInstance().getString(PREF_ACCOUNT_ID)));
        myTransactionInput.setFromDate(fromDate);
        myTransactionInput.setToDate(toDate);

        RestHelper restHelper = new RestHelper(BuildConfig.URL_BASE_DOTNET);
        restHelper.sendPost("MerchantAppTransactionHistory", myTransactionInput, MyTransactionOutput.class, (RestResultReceiver<MyTransactionOutput>) data ->
        {
            dismissProgressBar();
            validateResult(Validator.isValid(data) ? data.getResult() : null, task);
        });
    }

    @Override
    public void callTransferLoyaltyToken(String currentBalance, String transferAmount, String balanceAfterTransfer, String toAccountId, Task<TransferLoyaltyTokenResult> task)
    {
        if (!Validator.isValid(toAccountId) || toAccountId.equals("localvalu"))
        {
            showToast("Enter customer unique code");
            return;
        }

        showProgressBar();

        TransferLoyaltyTokenInput transferLoyaltyTokenInput = new TransferLoyaltyTokenInput();
        transferLoyaltyTokenInput.setRetailerId(PreferenceFactory.getInstance().getString(PREF_RETAILER_ID));
        transferLoyaltyTokenInput.setCurrentBalance(currentBalance);
        transferLoyaltyTokenInput.setTransferAmount(transferAmount);
        transferLoyaltyTokenInput.setBalanceAfterTransfer(balanceAfterTransfer);
        transferLoyaltyTokenInput.setToAccountID(toAccountId);

        RestHelper restHelper = new RestHelper(BuildConfig.URL_BASE_DOTNET);
        restHelper.sendPost("MerchantTransferLT", transferLoyaltyTokenInput, TransferLoyaltyTokenOutput.class, (RestResultReceiver<TransferLoyaltyTokenOutput>) data ->
        {
            dismissProgressBar();
            validateResult(Validator.isValid(data) ? data.getResult() : null, task);
        });
    }

    @Override
    public void callOrderList(String retailerId, Task<OrderListResult> task)
    {
        if (!Validator.isValid(retailerId))
        {
            showToast("Enter Account Id");
            return;
        }

        showProgressBar();

        OrderListInput orderListInput = new OrderListInput();
        orderListInput.setRetailerId(retailerId);

        RestHelper restHelper = new RestHelper(BuildConfig.URL_BASE_PHP1);
        restHelper.sendPost("MerchantAppV2/getOrderDetailsForADay", orderListInput, OrderListOutput.class, (RestResultReceiver<OrderListOutput>) data ->
        {
            dismissProgressBar();
            validateResult(Validator.isValid(data) ? data.getOrderListResult() : null, task);
        });
    }

    @Override
    public void callOrderDetails(String orderId, Task<OrderDetailsResult> task)
    {
        if (!Validator.isValid(orderId))
        {
            showToast("Enter order Id");
            return;
        }

        showProgressBar();

        OrderDetailsInput orderDetailsResult = new OrderDetailsInput();
        orderDetailsResult.setOrderId(orderId);

        RestHelper restHelper = new RestHelper(BuildConfig.URL_BASE_PHP1);
        restHelper.sendPost("MerchantAppV2/getInduvidualOrderDetails", orderDetailsResult, OrderDetailsResultOutput.class, (RestResultReceiver<OrderDetailsResultOutput>) data ->
        {
            dismissProgressBar();
            validateResult(Validator.isValid(data) ? data.getOrderDetailsResult() : null, task);
        });
    }

    @Override
    public void callOrderAtTableList(String retailerId, Task<OrderListResult> task)
    {
        if (!Validator.isValid(retailerId))
        {
            showToast("Enter Account Id");
            return;
        }

        showProgressBar();

        OrderListInput orderListInput = new OrderListInput();
        orderListInput.setRetailerId(retailerId);

        RestHelper restHelper = new RestHelper(BuildConfig.URL_BASE_PHP1);
        restHelper.sendPost("MerchantAppV2/getTableOrderDetailsForADay", orderListInput, OrderListOutput.class, (RestResultReceiver<OrderListOutput>) data ->
        {
            dismissProgressBar();
            validateResult(Validator.isValid(data) ? data.getOrderListResult() : null, task);
        });
    }

    @Override
    public void callOrderAtTableDetails(String orderId, Task<OrderDetailsResult> task)
    {
        if (!Validator.isValid(orderId))
        {
            showToast("Enter order Id");
            return;
        }

        showProgressBar();

        OrderDetailsInput orderDetailsResult = new OrderDetailsInput();
        orderDetailsResult.setOrderId(orderId);

        RestHelper restHelper = new RestHelper(BuildConfig.URL_BASE_PHP1);
        restHelper.sendPost("MerchantAppV2/getInduvidualTableOrderDetails", orderDetailsResult, OrderDetailsResultOutput.class, (RestResultReceiver<OrderDetailsResultOutput>) data ->
        {
            dismissProgressBar();
            validateResult(Validator.isValid(data) ? data.getOrderDetailsResult() : null, task);
        });
    }

    @Override
    public void callOrderStatusChange(String orderId, String status, Task<OrderStatusChangeResult> task)
    {
        if (!Validator.isValid(orderId))
        {
            showToast("Order Id cannot be empty");
            return;
        }
        else if (!Validator.isValid(status))
        {
            showToast("Status cannot be emtpy");
            return;
        }

        showProgressBar();

        OrderStatusChangeInput orderStatusChangeInput = new OrderStatusChangeInput();
        orderStatusChangeInput.setOrderId(orderId);
        orderStatusChangeInput.setStatus(status);

        RestHelper restHelper = new RestHelper(BuildConfig.URL_BASE_PHP1);
        restHelper.sendPost("MerchantAppV2/ChangeOrderStatus", orderStatusChangeInput, OrderStatusChangeOutput.class, (RestResultReceiver<OrderStatusChangeOutput>) data ->
        {
            dismissProgressBar();
            validateResult(Validator.isValid(data) ? data.getOrderStatusChangeResult() : null, task);
        });
    }

    @Override
    public void callTableBookingList(String retailerId, Task<TableBookingListResult> task)
    {
        if (!Validator.isValid(retailerId))
        {
            showToast("callTableBookingList");
            return;
        }

        showProgressBar();

        TableBookingListInput tableBookingListInput = new TableBookingListInput();
        tableBookingListInput.setRetailerId(retailerId);


        RestHelper restHelper = new RestHelper(BuildConfig.URL_BASE_PHP1);
        restHelper.sendPost("MerchantApp/TableBookingList", tableBookingListInput, TableBookingListOutput.class, (RestResultReceiver<TableBookingListOutput>) data ->
        {
            dismissProgressBar();
            validateResult(Validator.isValid(data) ? data.getTableBookingListResult() : null, task);
        });
    }

    @Override
    public void callTableBookingStatusChange(String tableBookId, String status, Task<TableBookingStatusChangeResult> task)
    {
        if (!Validator.isValid(tableBookId))
        {
            showToast("Book Id cannot be empty");
            return;
        }
        else if (!Validator.isValid(status))
        {
            showToast("Status cannot be emtpy");
            return;
        }

        showProgressBar();

        TableBookingStatusChangeInput tableBookingStatusChangeInput = new TableBookingStatusChangeInput();
        tableBookingStatusChangeInput.setTableBookid(tableBookId);
        tableBookingStatusChangeInput.setStatus(status);
        Log.d(TAG, "callTableBookingStatusChange: " + tableBookingStatusChangeInput.toString());


        RestHelper restHelper = new RestHelper(BuildConfig.URL_BASE_PHP1);
        restHelper.sendPost("MerchantApp/ChangeTableBookStatus", tableBookingStatusChangeInput, TableBookingStatusChangeOutput.class, (RestResultReceiver<TableBookingStatusChangeOutput>) data ->
        {
            dismissProgressBar();
            validateResult(Validator.isValid(data) ? data.getTableBookingStatusChangeResult() : null, task);
        });
    }

    @Override
    public void callMyAppointmentList(String retailerId, Task<MyAppointmentListResult> task)
    {
        if (!Validator.isValid(retailerId))
        {
            showToast("callTableBookingList");
            return;
        }

        showProgressBar();

        MyAppointmentListInput MyAppointmentListInput = new MyAppointmentListInput();
        MyAppointmentListInput.setRetailerId(retailerId);


        RestHelper restHelper = new RestHelper(BuildConfig.URL_BASE_PHP1);
        restHelper.sendPost("UserRegistration/requestSlotMerchantreport", MyAppointmentListInput, MyAppointmentListOutput.class, (RestResultReceiver<MyAppointmentListOutput>) data ->
        {
            dismissProgressBar();
            validateResult(Validator.isValid(data) ? data.getMyAppointmentListResult() : null, task);
        });
    }

    @Override
    public void callMyAppointmentListStatusChange(String slotId, String status, Task<MyAppointmentStatusChangeResult> task)
    {
        if (!Validator.isValid(slotId))
        {
            showToast("Slot Id cannot be empty");
            return;
        }
        else if (!Validator.isValid(status))
        {
            showToast("Status cannot be emtpy");
            return;
        }

        showProgressBar();

        MyAppointmentStatusChangeInput myAppointmentStatusChangeInput = new MyAppointmentStatusChangeInput();
        myAppointmentStatusChangeInput.setSlotId(slotId);
        myAppointmentStatusChangeInput.setStatus(status);
        Log.d(TAG, "callMyAppointmentListStatusChange: " + myAppointmentStatusChangeInput.toString());

        String methodName = "";

        if(status.equals("0"))
        {
            methodName = "UserRegistration/slotBookingMerchantaccept";
        }
        else if(status.equals("2"))
        {
            methodName = "UserRegistration/slotBookingMerchantDecline";
        }
        RestHelper restHelper = new RestHelper(BuildConfig.URL_BASE_PHP1);
        restHelper.sendPost(methodName, myAppointmentStatusChangeInput, MyAppointmentStatusChangeOutput.class, (RestResultReceiver<MyAppointmentStatusChangeOutput>) data ->
        {
            dismissProgressBar();
            validateResult(Validator.isValid(data) ? data.getMyAppointmentStatusChangeResult() : null, task);
        });
    }

    @Override
    public void callInsertVisitEntry(int merchantId, String QRCode, String visitDate, Task<VisitEntryResult> task)
    {
        if (!Validator.isValid(merchantId))
        {
            showToast("Merchant Id cannot be empty");
            return;
        }
        else if (!Validator.isValid(QRCode))
        {
            showToast("QRCode cannot be empty");
            return;
        }
        else if (!Validator.isValid(visitDate))
        {
            showToast("Visit Date cannot be empty");
            return;
        }

        showProgressBar();

        InsertVisitEntryInput insertVisitEntryInput = new InsertVisitEntryInput();
        insertVisitEntryInput.setMerchantId(merchantId);
        insertVisitEntryInput.setQRCode(QRCode);
        insertVisitEntryInput.setVisitDate(visitDate);
        Log.d(TAG, "callInsertVisitEntry: " + insertVisitEntryInput.toString());


        RestHelper restHelper = new RestHelper(BuildConfig.URL_BASE_DOTNET);
        restHelper.sendPost("TraceHistory_Insert", insertVisitEntryInput, VisitEntryResultOutput.class, (RestResultReceiver<VisitEntryResultOutput>) data ->
        {
            dismissProgressBar();
            validateResult(Validator.isValid(data) ? data.getVisitEntryResult() : null, task);
        });
    }

    @Override
    public void callVisitEntryHistory(int merchantId, String fromDate, String toDate, Task<TraceHistoryReportResult> task)
    {

        if (!Validator.isValid(merchantId))
        {
            showToast("Merchant Id cannot be empty");
            return;
        }
        else if (!Validator.isValid(fromDate))
        {
            showToast("From date  cannot be empty");
            return;
        }
        else if (!Validator.isValid(toDate))
        {
            showToast("To Date cannot be empty");
            return;
        }

        showProgressBar();

        VisitEntryHistoryInput visitEntryHistoryInput = new VisitEntryHistoryInput();
        visitEntryHistoryInput.setMerchantId(merchantId);
        visitEntryHistoryInput.setFromDate(fromDate);
        visitEntryHistoryInput.setToDate(toDate);
        Log.d(TAG, "callVisitEntryHistory: " + visitEntryHistoryInput.toString());


        RestHelper restHelper = new RestHelper(BuildConfig.URL_BASE_DOTNET);
        restHelper.sendPost("TraceHistoryReport", visitEntryHistoryInput, TraceHistoryReportResultOutput.class,
                (RestResultReceiver<TraceHistoryReportResultOutput>) data ->
                {
                    dismissProgressBar();
                    validateResult(Validator.isValid(data) ? data.getTraceHistoryReportResult() : null, task);
                });

    }

    @Override
    public void callDriversList(String retailerId,Task<DriverList> task)
    {
        showProgressBar();

        DriversListInput driversListInput = new DriversListInput();
        driversListInput.setTypes("drivers");
        driversListInput.setRetailerId(retailerId);
        if (BuildConfig.DEBUG)
        {
            Log.d(TAG, "callDriversList: " + driversListInput.toString());
        }
        RestHelper restHelper = new RestHelper(BuildConfig.URL_BASE_PHP1);
        restHelper.sendPost("DriverApp/GetDriverList", driversListInput, DriverListResult.class,
                (RestResultReceiver<DriverListResult>) data ->
                {
                    dismissProgressBar();
                    validateResult(Validator.isValid(data) ? data.getDriverListResult() : null, task);
                });
    }

    @Override
    public void callNotifyToDrivers(int driverId, int orderId, int retailerId, Task<DriverNotificationResult> task)
    {
        showProgressBar();

        NotifyToDriversInput notifyToDriversInput = new NotifyToDriversInput();
        notifyToDriversInput.setDriverId(driverId);
        notifyToDriversInput.setOrderId(orderId);
        notifyToDriversInput.setRetailerId(retailerId);
        if (BuildConfig.DEBUG)
        {
            Log.d(TAG, "callNotifyToDrivers: " + notifyToDriversInput.toString());
        }
        RestHelper restHelper = new RestHelper(BuildConfig.URL_BASE_PHP1);
        restHelper.sendPost("MerchantAppV2/NotifyToDrivers", notifyToDriversInput, DriverNotifictionResultOutput.class,
                (RestResultReceiver<DriverNotifictionResultOutput>) data ->
                {
                    dismissProgressBar();
                    validateResult(Validator.isValid(data) ? data.getDriverNotificationResult() : null, task);
                });
    }

    @Override
    public void callDriverProfile(String driverId, Task<DriverProfileResult> task)
    {
        //showProgressBar();

        DriversProfileInput driversProfileInput = new DriversProfileInput();
        driversProfileInput.setDriverId(driverId);

        if (BuildConfig.DEBUG)
        {
            Log.d(TAG, "callDriverProfile: " + driversProfileInput.toString());
        }
        RestHelper restHelper = new RestHelper(BuildConfig.URL_BASE_DOTNET);
        restHelper.sendPost("DriverProfile", driversProfileInput, DriverProfileOutput.class,
                (RestResultReceiver<DriverProfileOutput>) data ->
                {
                    //dismissProgressBar();
                    validateResult(Validator.isValid(data) ? data.getDriverProfileResult() : null, task);
                });
    }

    @Override
    public void callInsertMerchantSelfServe(MerchantSelfServeInput merchantSelfServeInput, Task<MerchantSelfServeResult> task)
    {
        if (BuildConfig.DEBUG)
        {
            Log.d(TAG, "callDriverProfile: " + merchantSelfServeInput.toString());
        }
        RestHelper restHelper = new RestHelper(BuildConfig.URL_BASE_PHP1);
        restHelper.sendPost("MerchantAppV2/MerchantSelfServe", merchantSelfServeInput, MerchantSelfServeOutput.class,
                (RestResultReceiver<MerchantSelfServeOutput>) data ->
                {
                    //dismissProgressBar();
                    validateResult(Validator.isValid(data) ? data.getResult() : null, task);
                });
    }

    private void validateResult(RestBasedOutput restBasedOutput, Task task)
    {
        if (Validator.isValid(restBasedOutput))
        {
            RestBasedErrorDetail errorDetail = restBasedOutput.getErrorDetail();

            if (Validator.isValid(errorDetail))
            {
                String errorMessage = errorDetail.getErrorMessage();

                if (errorDetail.getErrorCode() != 0)
                {
                    task.onError(Validator.isValid(errorMessage) ? errorMessage : "No error message");
                }
                else
                {
                    task.onSuccess(restBasedOutput);
                }
            }
            else
            {
                task.onSuccess(restBasedOutput);
            }
        }
        else
        {
            task.onError("Invalid response");
        }
    }


}
