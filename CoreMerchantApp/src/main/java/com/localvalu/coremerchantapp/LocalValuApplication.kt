package com.localvalu.coremerchantapp

import android.app.Application
import android.content.Context
import androidx.multidex.MultiDex
import dagger.hilt.android.HiltAndroidApp
import com.utils.UtilFactory

@HiltAndroidApp
class LocalValuApplication : Application()
{
    override fun onCreate()
    {
        super.onCreate()
        UtilFactory.init(applicationContext,this.resources.getString(R.string.app_name))
    }

}