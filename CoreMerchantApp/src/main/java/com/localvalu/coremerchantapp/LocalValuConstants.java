package com.localvalu.coremerchantapp;

public interface LocalValuConstants
{
    String EXTRA_TITLE = "extraTitle";
    String EXTRA_URL = "extraUrl";

    String PREF_RETAILER_ID = "prefRetailerId";
    String PREF_ACCOUNT_ID = "prefAccountId";
    String PREF_EMAIL = "prefEmail";
    String PREF_FCM_ID = "prefFCMId";
    String PREF_PUSHY_ID = "prefPushyId";
    String PREF_BOOKING_TYPE_ID = "prefBookingTypeId";
    String PREF_RETAILER_TYPE = "prefRetailerType";
    String PREF_SIGN_IN_RESULT = "prefSignInResult";

    public static final String SP_KEY_LABEL_OFFSET = "labelOffset";
    public static final String SP_KEY_LABEL_SIZE = "labelSize";
    public static final String SP_KEY_LABEL_GAP = "labelGap";//For TSC
    public static final String SP_KEY_LABEL_SPEED = "labelSpeed";
    public static final String SP_KEY_LABEL_TYPE = "labelType";

    String ORDER_ONLINE="4";
    String ORDER_AT_TABLE="5";
    String ORDER_AT_STORE="6";

}
