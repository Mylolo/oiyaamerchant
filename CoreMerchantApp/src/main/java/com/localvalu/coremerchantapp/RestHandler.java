package com.localvalu.coremerchantapp;


import com.localvalu.coremerchantapp.data.input.MerchantSelfServeInput;
import com.localvalu.coremerchantapp.data.output.MyTransactionResult;
import com.localvalu.coremerchantapp.data.output.SaleTransactionResult;
import com.localvalu.coremerchantapp.data.output.SignInResult;
import com.localvalu.coremerchantapp.data.output.TokenBalanceResult;
import com.localvalu.coremerchantapp.data.output.TransferLoyaltyTokenResult;
import com.localvalu.coremerchantapp.data.output.driverOld.DriverList;
import com.localvalu.coremerchantapp.data.output.driverOld.DriverNotificationResult;
import com.localvalu.coremerchantapp.data.output.driverOld.DriverProfileResult;
import com.localvalu.coremerchantapp.data.output.myappointments.MyAppointmentListResult;
import com.localvalu.coremerchantapp.data.output.myappointments.MyAppointmentStatusChangeResult;
import com.localvalu.coremerchantapp.data.output.order.OrderDetailsResult;
import com.localvalu.coremerchantapp.data.output.order.OrderListResult;
import com.localvalu.coremerchantapp.data.output.order.OrderStatusChangeResult;
import com.localvalu.coremerchantapp.data.output.selfserve.MerchantSelfServeResult;
import com.localvalu.coremerchantapp.data.output.tablebooking.TableBookingListResult;
import com.localvalu.coremerchantapp.data.output.tablebooking.TableBookingStatusChangeResult;
import com.localvalu.coremerchantapp.data.output.trace.TraceHistoryReportResult;
import com.localvalu.coremerchantapp.data.output.trace.VisitEntryResult;
import com.utils.base.Task;

public interface RestHandler
{

    void callSignIn(String fcmId, String email, String password, Task<SignInResult> task);

    void callTokenBalance(Task<TokenBalanceResult> task);

    void callSaleTransaction(String qrCode, Double saleTotal, Task<SaleTransactionResult> task);

    void callMyTransaction(String fromDate, String toDate, Task<MyTransactionResult> task);

    void callTransferLoyaltyToken(String currentBalance, String transferAmount, String balanceAfterTransfer, String toAccountId, Task<TransferLoyaltyTokenResult> task);

    void callOrderList(String userId, Task<OrderListResult> task);

    void callOrderDetails(String orderId, Task<OrderDetailsResult> task);

    void callOrderAtTableList(String userId, Task<OrderListResult> task);

    void callOrderAtTableDetails(String orderId, Task<OrderDetailsResult> task);

    void callOrderStatusChange(String orderId, String status, Task<OrderStatusChangeResult> task);

    void callTableBookingList(String retailerId, Task<TableBookingListResult> task);

    void callTableBookingStatusChange(String tableBookId, String status, Task<TableBookingStatusChangeResult> task);

    void callMyAppointmentList(String retailerId, Task<MyAppointmentListResult> task);

    void callMyAppointmentListStatusChange(String tableBookId, String status, Task<MyAppointmentStatusChangeResult> task);

    void callInsertVisitEntry(int merchantId, String QRCode, String visitDate, Task<VisitEntryResult> task);

    void callVisitEntryHistory(int merchantId, String fromDate, String toDate, Task<TraceHistoryReportResult> task);

    void callDriversList(String strRetail,Task<DriverList> task);

    void callNotifyToDrivers(int driverId, int orderId, int retailerId, Task<DriverNotificationResult> task);

    void callDriverProfile(String driverId, Task<DriverProfileResult> task);

    void callInsertMerchantSelfServe(MerchantSelfServeInput merchantSelfServeInput, Task<MerchantSelfServeResult> task);
}
