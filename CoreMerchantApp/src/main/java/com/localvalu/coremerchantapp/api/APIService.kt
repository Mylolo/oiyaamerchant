package com.localvalu.coremerchantapp.api

import com.localvalu.coremerchantapp.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object APIService
{
    fun getPoolDriverApiService(): PoolDriverListAPIService
    {
        return Retrofit.Builder()
                .baseUrl(BuildConfig.URL_BASE_PHP1)
                .client(provideOkHttpClient(provideLoggingInterceptor()))
                .addConverterFactory(GsonConverterFactory.create())
                .build().create(PoolDriverListAPIService::class.java)
    }

    fun getNotifyDriversService():NotifyDriversService
    {
        return Retrofit.Builder()
                .baseUrl(BuildConfig.URL_BASE_PHP1)
                .client(provideOkHttpClient(provideLoggingInterceptor()))
                .addConverterFactory(GsonConverterFactory.create())
                .build().create(NotifyDriversService::class.java)
    }

    fun getAppVersionService():AppVersionAPIService
    {
        return Retrofit.Builder()
            .baseUrl(BuildConfig.URL_BASE_PHP1)
            .client(provideOkHttpClient(provideLoggingInterceptor()))
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(AppVersionAPIService::class.java)
    }

    private fun provideOkHttpClient(interceptor: HttpLoggingInterceptor): OkHttpClient
    {
        val b = OkHttpClient.Builder()
        b.addInterceptor(interceptor)
        return b.build()
    }

    private fun provideLoggingInterceptor(): HttpLoggingInterceptor
    {
        val interceptor = HttpLoggingInterceptor()
        if(BuildConfig.DEBUG)
        {
            interceptor.level = HttpLoggingInterceptor.Level.BODY
        }
        return interceptor
    }

}