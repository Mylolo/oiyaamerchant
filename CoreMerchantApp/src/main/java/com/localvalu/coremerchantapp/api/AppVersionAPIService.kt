package com.localvalu.coremerchantapp.api

import com.localvalu.coremerchantapp.data.input.AppVersionCheckInput
import com.localvalu.coremerchantapp.data.output.appversioncheck.AppVersionOutput
import retrofit2.http.Body
import retrofit2.http.POST

interface AppVersionAPIService
{
    @POST("Webadmin/AppVersion")
    suspend fun getAppVersion(@Body checkInput: AppVersionCheckInput) : AppVersionOutput
}