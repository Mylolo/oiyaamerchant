package com.localvalu.coremerchantapp.api

import com.localvalu.coremerchantapp.data.input.NotifyToDriversInput
import com.localvalu.coremerchantapp.data.input.NotifyToPoolDriversInput
import com.localvalu.coremerchantapp.data.output.driverOld.DriverNotifictionResultOutput
import retrofit2.http.Body
import retrofit2.http.POST

interface NotifyDriversService
{
    @POST("MerchantAppV2/NotifyToDrivers")
    suspend fun notifyToPoolDrivers(@Body notifyToPoolDriversInput: NotifyToPoolDriversInput) : DriverNotifictionResultOutput

    @POST("MerchantAppV2/NotifyToDrivers")
    suspend fun notifyToDrivers(@Body notifyToDriversInput: NotifyToDriversInput) : DriverNotifictionResultOutput

}