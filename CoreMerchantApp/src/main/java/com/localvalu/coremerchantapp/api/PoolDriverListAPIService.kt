package com.localvalu.coremerchantapp.api

import DriverAppBase
import com.localvalu.coremerchantapp.data.input.PoolDriverListInput
import com.localvalu.coremerchantapp.data.output.driver.OwnDriversListRequest
import com.localvalu.coremerchantapp.data.output.pooldrivers.GetPoolDriverListResultOutput
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface PoolDriverListAPIService
{
    @POST("DriverApp/GetDriverpoolList")
    suspend fun poolDriverList(@Body poolDriverListInput: PoolDriverListInput) : GetPoolDriverListResultOutput

    @POST("DriverApp/GetDriverList")
    suspend fun ownDriversList(@Body ownDriversListRequest: OwnDriversListRequest) : DriverAppBase
}