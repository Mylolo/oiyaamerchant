package com.localvalu.coremerchantapp.b2b.api

import com.localvalu.coremerchantapp.BaseApi
import com.localvalu.coremerchantapp.b2b.model.B2BTransferTokenResponse
import com.localvalu.coremerchantapp.b2c.model.B2BTransferTokenRequest
import retrofit2.http.Body
import retrofit2.http.POST

interface B2BTransferTokenApi : BaseApi
{
    @POST("webadmin/MerchantTransferMToken")
    suspend fun transferTokenToMerchant(@Body b2CTransferTokenRequest: B2BTransferTokenRequest): B2BTransferTokenResponse
}