package com.localvalu.coremerchantapp.b2b.di

import android.content.Context
import com.localvalu.coremerchantapp.b2b.api.B2BTransferTokenApi
import com.localvalu.coremerchantapp.b2b.repository.B2BTransferTokenRepository
import com.localvalu.coremerchantapp.b2c.api.B2CTransferTokenApi
import com.localvalu.coremerchantapp.b2c.repository.B2CTransferTokenRepository
import com.localvalu.coremerchantapp.network.RemoteDataSource
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class B2BTransferTokenModule
{
    @Provides
    fun providesB2BTransferTokenApi(remoteDataSource: RemoteDataSource,
                                     @ApplicationContext context: Context): B2BTransferTokenApi
    {
        return remoteDataSource.buildApiPhpOne(B2BTransferTokenApi::class.java,context)
    }

    @Provides
    fun providesB2BRepository(b2BTransferTokenApi: B2BTransferTokenApi): B2BTransferTokenRepository
    {
        return B2BTransferTokenRepository(b2BTransferTokenApi)
    }
}