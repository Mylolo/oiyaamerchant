package com.localvalu.coremerchantapp.b2c.model

import com.google.gson.annotations.SerializedName
import java.net.IDN

class B2BTransferTokenRequest(@SerializedName("Token") var token:String?,
                              @SerializedName("RetailerId") val retailerId: Int,
                              @SerializedName("CurrentBalance") val currentBalance: Double,
                              @SerializedName("TransferAmount") val transferAmount: Int,
                              @SerializedName("BalanceAfterTransfer") val balanceAfterTransfer: Double,
                              @SerializedName("RetailerQRcode") val retailerQRcode: String)
{
    init
    {
        token="/3+YFd5QZdSK9EKsB8+TlA=="
    }
}