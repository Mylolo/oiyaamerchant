package com.localvalu.coremerchantapp.b2b.model

import com.google.gson.annotations.SerializedName

data class B2BTransferTokenResponse(@SerializedName("MerchantTransferMToken")
                                   val merchantTransferMToken: MerchantTransferMToken,
                                    @SerializedName("msg") val msg:String)
