package com.localvalu.coremerchantapp.b2b.model

import ErrorDetails
import com.google.gson.annotations.SerializedName

data class MerchantTransferMToken(@SerializedName("status") val status:String,
                                  @SerializedName("MerchantTransferMerchantResult")
                                  val merchantTransferMTokenResult:MerchantTransferMTokenResult)
