package com.localvalu.coremerchantapp.b2b.model

import ErrorDetails
import com.google.gson.annotations.SerializedName

data class MerchantTransferMTokenResult(@SerializedName("ErrorDetails") val errorDetails: ErrorDetails,
                                        @SerializedName("NewBalance") val newBalance: String)
