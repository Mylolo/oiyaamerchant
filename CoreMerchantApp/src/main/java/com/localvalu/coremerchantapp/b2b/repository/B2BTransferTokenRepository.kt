package com.localvalu.coremerchantapp.b2b.repository

import com.localvalu.coremerchantapp.b2b.api.B2BTransferTokenApi
import com.localvalu.coremerchantapp.b2c.model.B2BTransferTokenRequest
import com.localvalu.coremerchantapp.base.BaseRepository
import com.localvalu.coremerchantapp.network.SafeApiCall

class B2BTransferTokenRepository (private val api: B2BTransferTokenApi): BaseRepository(api)
{
    suspend fun transferTokenToMerchant(b2BTransferTokenRequest: B2BTransferTokenRequest) = safeApiCall {
        api.transferTokenToMerchant(b2BTransferTokenRequest)
    }
}