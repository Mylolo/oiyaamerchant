package com.localvalu.coremerchantapp.b2b.ui

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.localvalu.coremerchantapp.base.Resource
import com.localvalu.coremerchantapp.R
import com.localvalu.coremerchantapp.b2b.viewmodel.B2BTransferTokenViewModel
import com.localvalu.coremerchantapp.b2c.model.B2BTransferTokenRequest
import com.localvalu.coremerchantapp.databinding.FragmentB2CTransferTokensBinding
import com.localvalu.coremerchantapp.extensions.closeKeyboard
import com.localvalu.coremerchantapp.extensions.handleAPIError
import com.localvalu.coremerchantapp.extensions.shorten
import com.localvalu.coremerchantapp.extensions.showMessage
import com.localvalu.coremerchantapp.token.model.MerchantTokenBalanceRequest
import com.localvalu.coremerchantapp.token.viewmodel.MerchantTokenBalanceViewModel
import com.localvalu.coremerchantapp.viewmodel.DashboardViewModel
import com.utils.base.AppBaseFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class B2BTransferTokensFragment : AppBaseFragment()
{

    var binding: FragmentB2CTransferTokensBinding? = null;
    val viewModel: B2BTransferTokenViewModel by viewModels()
    private val dashboardViewModel: DashboardViewModel by viewModels()
    private val merchantTokenBalanceViewModel: MerchantTokenBalanceViewModel by viewModels()
    val currency: String by lazy {
        requireContext().getString(R.string.symbol_pound)
    }
    var lastB2BTransferTokenRequest: B2BTransferTokenRequest? = null;
    var retailerId: String? = null

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?
    {
        binding = FragmentB2CTransferTokensBinding.inflate(inflater, container, false)
        return binding?.root;
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        observeSubscribers()
        setProperties()
    }

    private fun setProperties()
    {
        binding!!.transferTokenIndicator.visibility = View.GONE
        binding!!.btnSend.isEnabled = false
        binding!!.tilCustomerUniqueCode.setHint(getString(R.string.lbl_merchant_unique_code))
        binding!!.tvTokensToACustomer.setText(getString(R.string.lbl_tokens_to_a_merchant))
        binding!!.btnSend.isEnabled = false
        binding!!.tvPlus.setOnClickListener {
            viewModel?.addPlus()
        }
        binding!!.tvMinus.setOnClickListener {
            viewModel?.addMinus()
        }
        binding!!.etCustomerUniqueCode.doAfterTextChanged {
            viewModel?.setMerchantUniqueCode(it.toString())
        }
        binding!!.btnSend.setOnClickListener {
            transferTokens()
        }
        binding!!.etCustomerUniqueCode.setOnEditorActionListener{ v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE)
            {
                if(binding!!.btnSend.isEnabled)
                {
                    transferTokens()
                }
                true
            }
            else
            {
                false
            }
        }
    }

    private fun observeSubscribers()
    {
        viewModel?.b2BTransferTokenResponse?.observe(viewLifecycleOwner, Observer { event ->
            if (!event.hasBeenHandled)
            {
                when (val response = event.getContentIfNotHandled())
                {
                    is Resource.Loading ->
                    {
                        binding!!.transferTokenIndicator.visibility = View.VISIBLE
                        binding!!.btnSend.setText("")
                    }
                    is Resource.Success ->
                    {
                        when (response.value.merchantTransferMToken.merchantTransferMTokenResult.errorDetails.errorCode)
                        {
                            0 ->
                            {
                                Handler(Looper.myLooper()!!).postDelayed(Runnable {
                                    binding!!.transferTokenIndicator.visibility = View.GONE
                                    binding!!.btnSend.setText(getString(R.string.lbl_send))

                                    showMessage(response.value.msg)
                                    viewModel?.setMerchantUniqueCode("")
                                    viewModel?.setTransferTokens(1)
                                    lastB2BTransferTokenRequest = null
                                }, 2000)
                            }
                            else ->
                            {
                                binding!!.transferTokenIndicator.visibility = View.GONE
                                binding!!.btnSend.setText(getString(R.string.lbl_send))
                                showMessage(response.value.merchantTransferMToken.merchantTransferMTokenResult.errorDetails.errorMessage)
                            }
                        }
                    }
                    is Resource.Failure ->
                    {
                        binding!!.transferTokenIndicator.visibility = View.GONE
                        binding!!.btnSend.setText(getString(R.string.lbl_send))
                        handleAPIError(response) {
                            transferTokens()
                        }
                    }
                }
            }
        })
        viewModel?.balanceAfterTransfer?.observe(viewLifecycleOwner, Observer {
            binding?.tvBalanceAfterTransfer?.text = it.shorten()
        })
        viewModel?.transferTokens?.observe(viewLifecycleOwner, Observer {
            binding?.tvTransferAmount?.setText(it.toString())
        })
        viewModel?.enableSendNow?.observe(viewLifecycleOwner, Observer {
            binding?.btnSend?.isEnabled = it
        })
        dashboardViewModel?.user.observe(viewLifecycleOwner, Observer {
            it?.let {
                retailerId = it.retailerId.toString();
                getMerchantBalance()
            }
        })
        merchantTokenBalanceViewModel.merchantTokenBalanceResponse?.observe(
            viewLifecycleOwner,
            Observer { response ->
                when (response)
                {
                    is Resource.Loading ->
                    {
                        showProgress()
                    }
                    is Resource.Success ->
                    {
                        showContent()
                        when (response.value.result.errorDetail.errorCode)
                        {
                            0 ->
                            {
                                binding?.tvCurrentBalance?.text =
                                    response.value.result.currentTokenBalance.shorten()
                                viewModel?.setCurrentBalance(response.value.result.currentTokenBalance)
                            }
                            1 ->
                            {

                            }
                        }
                    }
                    is Resource.Failure ->
                    {
                        handleAPIError(response) {
                            getMerchantBalance()
                        }
                    }
                }
            })
        dashboardViewModel?.getUser()
    }

    private fun getMerchantBalance()
    {
        merchantTokenBalanceViewModel?.merchantTokenBalance(

            MerchantTokenBalanceRequest("", 40527018.toString(), retailerId.toString())
        )
    }

    private fun transferTokens()
    {
        closeKeyboard(requireContext(), binding?.root?.rootView)
        retailerId?.let {
            lastB2BTransferTokenRequest = B2BTransferTokenRequest(
                "",
                it.toString().toInt(),
                binding?.tvCurrentBalance?.text.toString().toDouble(),
                binding?.tvTransferAmount?.text.toString().toInt(),
                binding?.tvBalanceAfterTransfer?.text.toString().toDouble(),
                binding?.etCustomerUniqueCode?.text.toString()
            );
            viewModel?.transferTokensToMerchant(lastB2BTransferTokenRequest!!)
        }
    }

    private fun showProgress()
    {
        binding!!.layoutProgress.layoutRoot.visibility = View.VISIBLE
        binding!!.cnlContent.visibility = View.GONE
        binding!!.layoutError.layoutRoot.visibility = View.GONE
    }

    private fun showContent()
    {
        binding!!.layoutProgress.layoutRoot.visibility = View.GONE
        binding!!.cnlContent.visibility = View.VISIBLE
        binding!!.layoutError.layoutRoot.visibility = View.GONE
    }

    private fun showError(strMessage: String, retry: Boolean)
    {
        binding!!.layoutProgress.layoutRoot.visibility = View.GONE
        binding!!.cnlContent.visibility = View.GONE
        binding!!.layoutError.layoutRoot.visibility = View.VISIBLE
        binding!!.layoutError.tvError.text = strMessage
        if (retry) binding!!.layoutError.btnRetry.visibility = View.VISIBLE
        else binding!!.layoutError.btnRetry.visibility = View.GONE
    }

}