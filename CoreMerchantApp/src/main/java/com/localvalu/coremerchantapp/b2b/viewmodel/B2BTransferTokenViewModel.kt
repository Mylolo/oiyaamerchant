package com.localvalu.coremerchantapp.b2b.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.localvalu.coremerchantapp.base.Resource
import com.localvalu.coremerchantapp.b2b.model.B2BTransferTokenResponse
import com.localvalu.coremerchantapp.b2b.repository.B2BTransferTokenRepository
import com.localvalu.coremerchantapp.b2c.model.B2BTransferTokenRequest
import com.localvalu.coremerchantapp.base.Event

import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class B2BTransferTokenViewModel @Inject constructor(val repository: B2BTransferTokenRepository) : ViewModel()
{
    private val _b2BTransferTokenResponse: MutableLiveData<Event<Resource<B2BTransferTokenResponse>>> = MutableLiveData()

    val b2BTransferTokenResponse: LiveData<Event<Resource<B2BTransferTokenResponse>>>
        get() = _b2BTransferTokenResponse

    fun transferTokensToMerchant(b2BTransferTokenRequest: B2BTransferTokenRequest) = viewModelScope.launch {
        _b2BTransferTokenResponse.value = Event(Resource.Loading)
        _b2BTransferTokenResponse.value = Event(repository.transferTokenToMerchant(b2BTransferTokenRequest))
    }

    val balanceAfterTransfer:MutableLiveData<Double> = MutableLiveData()

    val transferTokens:MutableLiveData<Int> = MutableLiveData()

    val enableSendNow:MutableLiveData<Boolean> = MutableLiveData()

    private val merchantUniqueCode:MutableLiveData<String> = MutableLiveData()

    init
    {
        transferTokens.value = 1
    }

    fun setCurrentBalance(currentBalance:Double)
    {
        this.balanceAfterTransfer.value = currentBalance-1
    }

    fun addPlus()
    {
        transferTokens.value = transferTokens.value?.plus(1)
        balanceAfterTransfer.value = balanceAfterTransfer.value?.minus(1)
        this.merchantUniqueCode.value?.let { enableDisableSend(it) }
    }

    fun addMinus()
    {
        if((transferTokens.value)!! > 1)
        {
            transferTokens.value = transferTokens.value?.minus(1)
            balanceAfterTransfer.value = balanceAfterTransfer.value?.plus(1)
            this.merchantUniqueCode.value?.let { enableDisableSend(it) }
        }
        else enableSendNow.value = false
    }

    fun setMerchantUniqueCode(uniqueCode:String)
    {
        this.merchantUniqueCode.value = uniqueCode
        enableDisableSend(uniqueCode)
    }

    private fun enableDisableSend(uniqueCode: String)
    {
        enableSendNow.value = (uniqueCode.isNotEmpty() &&
                transferTokens.value!! > 0 &&
                uniqueCodeValidation(uniqueCode) &&
                uniqueCode.length==11)
    }

    private fun uniqueCodeValidation(uniqueCode:String):Boolean
    {
        if(uniqueCode.length>4)
        {
            if(uniqueCode.substring(0,3).equals("IDL"))
            {
                return true
            }
            return false
        }
        return false
    }

    fun setTransferTokens(transferTokens:Int)
    {
        this.transferTokens.value = transferTokens
    }


}