package com.localvalu.coremerchantapp.b2c.api

import com.localvalu.coremerchantapp.BaseApi
import com.localvalu.coremerchantapp.b2c.model.B2CTransferTokenRequest
import com.localvalu.coremerchantapp.b2c.model.B2CTransferTokenResponse
import retrofit2.http.Body
import retrofit2.http.POST

interface B2CTransferTokenApi : BaseApi
{
    @POST("webadmin/TransferMerchantToken")
    suspend fun transferTokenToConsumer(@Body b2CTransferTokenRequest: B2CTransferTokenRequest): B2CTransferTokenResponse
}