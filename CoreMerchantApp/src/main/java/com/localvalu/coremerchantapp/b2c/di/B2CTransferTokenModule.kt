package com.localvalu.coremerchantapp.b2c.di

import android.content.Context
import com.localvalu.coremerchantapp.b2c.api.B2CTransferTokenApi
import com.localvalu.coremerchantapp.b2c.repository.B2CTransferTokenRepository
import com.localvalu.coremerchantapp.network.RemoteDataSource
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class B2CTransferTokenModule
{
    @Provides
    fun providesB2CTransferTokenApi(remoteDataSource: RemoteDataSource,
                                     @ApplicationContext context: Context): B2CTransferTokenApi
    {
        return remoteDataSource.buildApiPhpOne(B2CTransferTokenApi::class.java,context)
    }

    @Provides
    fun providesB2CRepository(b2CTransferTokenApi: B2CTransferTokenApi):B2CTransferTokenRepository
    {
        return B2CTransferTokenRepository(b2CTransferTokenApi)
    }
}