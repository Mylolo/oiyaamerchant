package com.localvalu.coremerchantapp.b2c.model

import com.google.gson.annotations.SerializedName
import java.net.IDN

class B2CTransferTokenRequest(@SerializedName("Token") var token:String?,
                              @SerializedName("RetailerId") val retailerId: Int,
                              @SerializedName("CurrentBalance") val currentBalance: Double,
                              @SerializedName("TransferAmount") val transferAmount: Int,
                              @SerializedName("BalanceAfterTransfer") val balanceAfterTransfer: Double,
                              @SerializedName("ToAccountID") val toAccountID: String)
{
    init
    {
        token="/3+YFd5QZdSK9EKsB8+TlA=="
    }
}