package com.localvalu.coremerchantapp.b2c.model

import com.google.gson.annotations.SerializedName

data class B2CTransferTokenResponse(@SerializedName("TransferMerchantToken")
                                   val transferMerchantToken:TransferMerchantToken,
                                    @SerializedName("msg") val msg:String)
