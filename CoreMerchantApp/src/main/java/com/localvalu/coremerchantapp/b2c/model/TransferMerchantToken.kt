package com.localvalu.coremerchantapp.b2c.model

import ErrorDetails
import com.google.gson.annotations.SerializedName

data class TransferMerchantToken(@SerializedName("TransferMerchantTokenResult")
                                 val transferMerchantTokenResult:TransferMerchantTokenResult)