package com.localvalu.coremerchantapp.b2c.model

import ErrorDetails
import com.google.gson.annotations.SerializedName

data class TransferMerchantTokenResult(@SerializedName("ErrorDetails") val errorDetails: ErrorDetails,
                                       @SerializedName("status") val status:String,
                                       @SerializedName("msg") val msg:String,
                                       @SerializedName("NewBalance") val newBalance: String)
