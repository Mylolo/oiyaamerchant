package com.localvalu.coremerchantapp.b2c.repository

import com.localvalu.coremerchantapp.b2c.api.B2CTransferTokenApi
import com.localvalu.coremerchantapp.b2c.model.B2CTransferTokenRequest
import com.localvalu.coremerchantapp.base.BaseRepository

class B2CTransferTokenRepository (private val api: B2CTransferTokenApi): BaseRepository(api)
{
    suspend fun transferTokenToConsumer(b2CTransferTokenRequest: B2CTransferTokenRequest) = safeApiCall {
        api.transferTokenToConsumer(b2CTransferTokenRequest)
    }
}