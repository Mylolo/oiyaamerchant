package com.localvalu.coremerchantapp.b2c.viewmodel

import androidx.lifecycle.*
import com.localvalu.coremerchantapp.base.Resource
import com.localvalu.coremerchantapp.b2c.model.B2CTransferTokenRequest
import com.localvalu.coremerchantapp.b2c.model.B2CTransferTokenResponse
import com.localvalu.coremerchantapp.b2c.repository.B2CTransferTokenRepository
import com.localvalu.coremerchantapp.base.Event

import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class B2CTransferTokenViewModel @Inject constructor(val repository: B2CTransferTokenRepository) : ViewModel()
{
    private val _b2CTransferTokenResponse: MutableLiveData<Event<Resource<B2CTransferTokenResponse>>> = MutableLiveData()

    val b2CTransferTokenResponse: LiveData<Event<Resource<B2CTransferTokenResponse>>>
        get() = _b2CTransferTokenResponse

    fun transferTokensToConsumer(b2CTransferTokenRequest: B2CTransferTokenRequest) = viewModelScope.launch {
        _b2CTransferTokenResponse.value = Event(Resource.Loading)
        _b2CTransferTokenResponse.value = Event(repository.transferTokenToConsumer(b2CTransferTokenRequest))
    }

    val balanceAfterTransfer:MutableLiveData<Double> = MutableLiveData()

    val transferTokens:MutableLiveData<Int> = MutableLiveData()

    val enableSendNow:MutableLiveData<Boolean> = MutableLiveData()

    private val customerUniqueCode:MutableLiveData<String> = MutableLiveData()

    init
    {
        transferTokens.value = 1
    }

    fun setCurrentBalance(currentBalance:Double)
    {
        this.balanceAfterTransfer.value = currentBalance - 1
    }

    fun addPlus()
    {
        transferTokens.value = transferTokens.value?.plus(1)
        balanceAfterTransfer.value = balanceAfterTransfer.value?.minus(1)
        this.customerUniqueCode.value?.let { enableDisableSend(it) }
    }

    fun addMinus()
    {
        if((transferTokens.value)!! > 1)
        {
            transferTokens.value = transferTokens.value?.minus(1)
            balanceAfterTransfer.value = balanceAfterTransfer.value?.plus(1)
            this.customerUniqueCode.value?.let { enableDisableSend(it) }
        }
        else enableSendNow.value = false
    }

    fun setCustomerUniqueCode(uniqueCode:String)
    {
        this.customerUniqueCode.value = uniqueCode
        enableDisableSend(uniqueCode)
    }

    private fun enableDisableSend(uniqueCode: String)
    {
        enableSendNow.value = (uniqueCode.isNotEmpty() &&
                transferTokens.value!! > 0 &&
                uniqueCodeValidation(uniqueCode) &&
                uniqueCode.length==11)
    }

    private fun uniqueCodeValidation(uniqueCode:String):Boolean
    {
        if(uniqueCode.length>4)
        {
            if(uniqueCode.substring(0,3).equals("IDL"))
            {
                return true
            }
            return false
        }
        return false
    }

    fun setTransferTokens(transferTokens:Int)
    {
        this.transferTokens.value = transferTokens
    }

}