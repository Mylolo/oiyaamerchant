package com.localvalu.coremerchantapp.base

import com.localvalu.coremerchantapp.BaseApi
import com.localvalu.coremerchantapp.network.SafeApiCall


abstract class BaseRepository(private val api: BaseApi) : SafeApiCall
{

}