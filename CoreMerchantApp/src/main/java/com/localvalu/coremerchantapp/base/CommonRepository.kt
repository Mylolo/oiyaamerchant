package com.localvalu.coremerchantapp.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

object CommonRepository
{
    val fcmToken:MutableLiveData<String> = MutableLiveData()
}