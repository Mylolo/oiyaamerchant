package com.localvalu.coremerchantapp.components

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.media.AudioAttributes
import android.media.AudioManager
import android.media.MediaPlayer
import android.media.RingtoneManager
import android.net.Uri
import android.os.Binder
import android.os.Build
import android.os.Handler
import android.os.IBinder
import android.speech.tts.TextToSpeech
import android.util.Log
import androidx.core.app.NotificationCompat
import com.localvalu.coremerchantapp.BuildConfig
import com.localvalu.coremerchantapp.LocalValuActivity
import com.localvalu.coremerchantapp.LocalValuConstants
import com.localvalu.coremerchantapp.R
import com.localvalu.coremerchantapp.base.Resource
import dagger.hilt.android.AndroidEntryPoint
import com.localvalu.coremerchantapp.components.OiyaaBackEndService
import com.localvalu.coremerchantapp.utils.AppConstants
import com.localvalu.coremerchantapp.utils.AppUtils
import com.localvalu.coremerchantapp.data.input.OrderListInput
import com.utils.rest.RestHelper
import com.localvalu.coremerchantapp.data.output.order.OrderListOutput
import com.utils.rest.RestResultReceiver
import com.localvalu.coremerchantapp.data.output.order.OrderListResult
import com.localvalu.coremerchantapp.data.input.TableBookingListInput
import com.localvalu.coremerchantapp.data.output.tablebooking.TableBookingListOutput
import com.localvalu.coremerchantapp.data.output.tablebooking.TableBookingListResult
import com.localvalu.coremerchantapp.data.base.RestBasedOutput
import com.localvalu.coremerchantapp.data.base.RestBasedErrorDetail
import com.localvalu.coremerchantapp.extensions.handleAPIError
import com.localvalu.coremerchantapp.network.RemoteDataSource
import com.localvalu.coremerchantapp.orders.api.OnlineOrderListApi
import com.localvalu.coremerchantapp.orders.repository.OnlineOrderListRepository
import com.localvalu.coremerchantapp.orders.repository.TableOrderListRepository
import com.localvalu.coremerchantapp.tablebookings.repository.TableBookingRepository
import com.utils.base.Task
import com.utils.validator.Validator
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import java.lang.Exception
import java.util.*
import javax.inject.Inject

@AndroidEntryPoint
class OiyaaBackEndService : Service(), TextToSpeech.OnInitListener
{
    private val mBinder: IBinder = MyBinder()
    private var mHandler: Handler? = null
    private var retailerId: String? = ""
    private var bookingTypeId: String? = ""
    private var retailerType: String? = ""
    private var lastSize = 0
    private var lastSizeOrderAtTable = 0
    private var lastSizeTableBookingList = 0
    private var mTts: TextToSpeech? = null
    private var spokenText = ""
    private var cycleCount = 0

    @Inject
    lateinit var onlineOrderListRepository:OnlineOrderListRepository

    @Inject
    lateinit var tableOrderListRepository:TableOrderListRepository

    @Inject
    lateinit var tableBookingRepository:TableBookingRepository


    override fun onCreate()
    {
        super.onCreate()
        if (Build.VERSION.SDK_INT >= 26)
        {
            val CHANNEL_ID = getString(R.string.default_local_channel_id)
            val channel = NotificationChannel(
                CHANNEL_ID,
                "Channel human readable title",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            (getSystemService(NOTIFICATION_SERVICE) as NotificationManager).createNotificationChannel(
                channel
            )

            /*Intent stopSelf = new Intent(this, NewRequestsCheckService.class);
            stopSelf.setAction(ACTION_NOTIFICATION_END);
            PendingIntent pStopSelf = PendingIntent.getService(this, 0, stopSelf,0);*/
            val contentIntent = Intent(this, LocalValuActivity::class.java)
            val pendingIntent = PendingIntent.getActivity(this, 0, contentIntent, PendingIntent.FLAG_IMMUTABLE)
            val notification = NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("Running...")
                .setContentText("")
                .setSmallIcon(R.drawable.ic_stat_name)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent) //.addAction(R.drawable.ic_exit_to_app, getString(R.string.lbl_exit), pStopSelf)
                .build()
            startForeground(NOTIFICATION_ID, notification)
        }
        mTts = TextToSpeech(this, this)
        spokenText = "Your voice service started"
        OiyaaBackEndServiceManager.isMyServiceRunning = true
    }

    //Task to be run here
    private val runnableService: Runnable? = object : Runnable
    {
        override fun run()
        {
            cycleCount += 1
            CoroutineScope(Dispatchers.IO).launch {
                syncOnlineOrders()
                syncTableOrders()
            }
            if (retailerType != null)
            {
                when (retailerType)
                {
                    AppConstants.RETAILER_TYPE_EATS ->
                    {
                        CoroutineScope(Dispatchers.IO).launch{
                            syncTableBookings()
                        }
                    }
                    AppConstants.RETAILER_TYPE_LOCAL ->
                    {

                    }
                }
            }
            // Repeat this runnable code block again every ... min
            mHandler!!.postDelayed(this, DEFAULT_SYNC_INTERVAL)
        }
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int
    {
        Log.d(TAG, "onStartCommand")
        if (intent != null)
        {
            if (intent.action != null)
            {
                when (intent.action)
                {
                    AppUtils.ACTION_NOTIFICATION_START ->
                    {
                        retailerId = intent.extras!!.getString(AppUtils.BUNDLE_RETAILER_ID)
                        bookingTypeId = intent.extras!!.getString(AppUtils.BUNDLE_BOOKING_TYPE_ID)
                        retailerType = intent.extras!!.getString(AppUtils.BUNDLE_RETAILER_TYPE)
                        // Create the Handler object
                        mHandler = Handler()
                        // Execute a runnable task as soon as possible
                        mHandler!!.post(runnableService!!)
                    }
                    AppUtils.ACTION_NOTIFICATION_END -> stopSelf()
                }
            }
        }
        return START_STICKY_COMPATIBILITY
    }

    override fun onBind(intent: Intent): IBinder?
    {
        return mBinder
    }

    inner class MyBinder : Binder()
    {
        val service: OiyaaBackEndService
            get() = this@OiyaaBackEndService
    }

    private suspend fun syncOnlineOrders()
    {
        val orderListInput = OrderListInput()
        orderListInput.retailerId=retailerId
        when(val response = onlineOrderListRepository.onlineOrderList(orderListInput))
        {
            is Resource.Loading->
            {
            }
            is Resource.Success->
            {
                if(response.value.orderListResult.errorDetail.errorCode==0)
                {
                    Log.d(TAG,response.value.orderListResult.errorDetail.errorMessage)
                    if (lastSize != response.value.orderListResult.orderList.size)
                    {
                        lastSize = response.value.orderListResult.orderList.size
                        if (cycleCount > 2)
                        {
                            sendOnlineOrderBroadCast(response.value.orderListResult)
                            val messageBody =
                                getString(R.string.lbl_new_order_online_message)
                            textToSpeech(messageBody)
                            sendNotification(messageBody)
                        }
                    }

                }
                else
                {
                    if(BuildConfig.DEBUG)
                    {
                        Log.d(TAG,response.value.orderListResult.errorDetail.errorMessage)
                    }
                }
            }
            is Resource.Failure->
            {
                if(BuildConfig.DEBUG)
                {
                    Log.d(TAG,response.errorBody.toString())
                }
            }
        }
    }

    private suspend fun syncTableOrders()
    {
        val orderListInput = OrderListInput()
        orderListInput.retailerId=retailerId
        when(val response = tableOrderListRepository.tableOrderList(orderListInput))
        {
            is Resource.Loading->
            {
            }
            is Resource.Success->
            {
                if(response.value.orderListResult.errorDetail.errorCode==0)
                {
                    Log.d(TAG,response.value.orderListResult.errorDetail.errorMessage)
                    if (lastSizeOrderAtTable != response.value.orderListResult.orderList.size)
                    {
                        lastSizeOrderAtTable = response.value.orderListResult.orderList.size
                        if (cycleCount > 2)
                        {
                            sendTableOrderBroadCast(response.value.orderListResult)
                            var messageBody = ""
                            messageBody = if (isOrderAtTable)
                            {
                                getString(R.string.lbl_new_order_at_table_message)
                            }
                            else
                            {
                                getString(R.string.lbl_new_order_at_store_message)
                            }
                            textToSpeech(messageBody)
                            sendNotification(messageBody)
                        }
                    }

                }
                else
                {
                    if(BuildConfig.DEBUG)
                    {
                        Log.d(TAG,response.value.orderListResult.errorDetail.errorMessage)
                    }
                }
            }
            is Resource.Failure->
            {
                if(BuildConfig.DEBUG)
                {
                    Log.d(TAG,response.errorBody.toString())
                }
            }
        }
    }

    private suspend fun syncTableBookings()
    {
        val tableBookingListInput = TableBookingListInput()
        tableBookingListInput.retailerId = retailerId
        when(val response = tableBookingRepository.tableBookingList(tableBookingListInput))
        {
            is Resource.Loading->
            {
            }
            is Resource.Success->
            {
                if(response.value.tableBookingListResult.errorDetail.errorCode==0)
                {
                    Log.d(TAG,response.value.tableBookingListResult.errorDetail.errorMessage)
                    if (lastSizeTableBookingList != response.value.tableBookingListResult.tableBookingList.size)
                    {
                        lastSizeTableBookingList = response.value.tableBookingListResult.tableBookingList.size
                        if (cycleCount > 2)
                        {
                            sendTableBookingListBroadCast(response.value.tableBookingListResult)
                            val messageBody =
                                getString(R.string.lbl_new_table_booking_requests_message)
                            textToSpeech(messageBody)
                            sendNotification(messageBody)
                        }
                    }

                }
                else
                {
                    if(BuildConfig.DEBUG)
                    {
                        Log.d(TAG,response.value.tableBookingListResult.errorDetail.errorMessage)
                    }
                }
            }
            is Resource.Failure->
            {
                if(BuildConfig.DEBUG)
                {
                    Log.d(TAG,response.errorBody.toString())
                }
            }
        }
    }

    private val isOrderAtTable: Boolean
        private get() = bookingTypeId!!.contains(LocalValuConstants.ORDER_AT_TABLE)

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private fun sendNotification(messageBody: String)
    {
        //Uri soundUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://"+ getApplicationContext().getPackageName() + "/" + R.raw.mysound);
        val soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val intent = Intent(this, LocalValuActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(
            this, 0 /* Request code */, intent,
            PendingIntent.FLAG_IMMUTABLE
        )
        val channelId = getString(R.string.default_local_channel_id)
        //Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        val notificationBuilder = NotificationCompat.Builder(this, channelId)
            .setSmallIcon(R.drawable.ic_stat_name)
            .setContentTitle(getString(R.string.app_name))
            .setContentText(messageBody)
            .setAutoCancel(true) //.setSound(soundUri)
            .setContentIntent(pendingIntent)
        val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            val channel = NotificationChannel(
                channelId, getString(R.string.app_name),
                NotificationManager.IMPORTANCE_DEFAULT
            )
            val audioAttributes = AudioAttributes.Builder()
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                .build()
            //channel.setSound(soundUri, audioAttributes);
            notificationManager.createNotificationChannel(channel)
        }
        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build())
        /*try
        {
            val alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE)
            val mMediaPlayer = MediaPlayer()
            mMediaPlayer.setDataSource(this, alert)
            val audioManager: AudioManager =
                getSystemService(Context.AUDIO_SERVICE) as AudioManager
            if (audioManager.getStreamVolume(AudioManager.STREAM_RING) !== 0)
            {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                {
                    mMediaPlayer.setAudioAttributes(
                        AudioAttributes.Builder()
                            .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                            .build()
                    )
                }
                else mMediaPlayer.setAudioStreamType(AudioManager.STREAM_RING)
                mMediaPlayer.isLooping = true
                mMediaPlayer.prepare()
                mMediaPlayer.start()
            }
        }
        catch (e: Exception)
        {
            e.printStackTrace()
        }*/
        OiyaaPlayer.getInstance(applicationContext).also {
            try
            {
                it.prepareAsync()
                it.setOnPreparedListener {
                    it.start()
                }
            }
            catch (ex:Exception)
            {
                ex.printStackTrace()
            }
        }
    }

    override fun onInit(status: Int)
    {
        if (status == TextToSpeech.SUCCESS)
        {
            val result = mTts!!.setLanguage(Locale.US)
            if (result != TextToSpeech.LANG_MISSING_DATA && result != TextToSpeech.LANG_NOT_SUPPORTED)
            {
                Log.d(TAG, "Language UK Supported")
                mTts!!.speak(spokenText, TextToSpeech.QUEUE_FLUSH, null)
            } else
            {
                Log.d(TAG, "Language UK Not Supported")
            }
        } else
        {
            Log.d(TAG, "Text-To-Speech is not Supported")
        }
    }

    private fun textToSpeech(messageBody: String)
    {
        mTts!!.speak(messageBody, TextToSpeech.QUEUE_FLUSH, null)
    }

    override fun onLowMemory()
    {
        super.onLowMemory()
        stopSelf()
    }

    override fun onDestroy()
    {
        super.onDestroy()
        Log.d(TAG, "onDestroy: called.")
        OiyaaBackEndServiceManager.isMyServiceRunning=false
        if (mTts != null)
        {
            mTts!!.stop()
            mTts!!.shutdown()
        }
        if (runnableService != null && mHandler != null)
        {
            mHandler!!.removeCallbacks(runnableService)
            mHandler = null
            stopSelf()
        }
    }

    /**
     * This method is responsible to send broadCast to specific Action
     */
    private fun sendOnlineOrderBroadCast(orderListResult: OrderListResult)
    {
        try
        {
            val broadCastIntent = Intent()
            broadCastIntent.action = AppUtils.BROADCAST_ACTION
            broadCastIntent.putExtra(AppUtils.BUNDLE_ORDER_LIST_RESULT, orderListResult)
            sendBroadcast(broadCastIntent)
        } catch (ex: Exception)
        {
            ex.printStackTrace()
        }
    }

    /**
     * This method is responsible to send broadCast to specific Action
     */
    private fun sendTableOrderBroadCast(orderListResult: OrderListResult)
    {
        try
        {
            val broadCastIntent = Intent()
            broadCastIntent.action = AppUtils.BROADCAST_ACTION_ORDER_AT_TABLE_LIST
            broadCastIntent.putExtra(AppUtils.BUNDLE_ORDER_AT_TABLE_LIST_RESULT, orderListResult)
            sendBroadcast(broadCastIntent)
        } catch (ex: Exception)
        {
            ex.printStackTrace()
        }
    }

    /**
     * This method is responsible to send broadCast to specific Action
     */
    private fun sendTableBookingListBroadCast(tableBookingListResult: TableBookingListResult)
    {
        try
        {
            val broadCastIntent = Intent()
            broadCastIntent.action = AppUtils.BROADCAST_ACTION_TABLE_BOOKING_LIST
            broadCastIntent.putExtra(
                AppUtils.BUNDLE_BOOKING_TABLE_LIST_RESULT,
                tableBookingListResult
            )
            sendBroadcast(broadCastIntent)
        } catch (ex: Exception)
        {
            ex.printStackTrace()
        }
    }

    companion object
    {
        private const val TAG = "OiyaaBackEndService"

        //default interval for syncing data
        const val DEFAULT_SYNC_INTERVAL = (30 * 1000).toLong()
        const val NOTIFICATION_ID = 1
    }
}