package com.localvalu.coremerchantapp.components

import android.content.Context
import android.media.AudioAttributes
import android.media.AudioManager
import android.media.MediaPlayer
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import androidx.core.content.ContextCompat.getSystemService


class OiyaaPlayer private constructor(val context:Context) : MediaPlayer()
{
    companion object
    {
        var alert: Uri? = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE)

        @Volatile private var INSTANCE: MediaPlayer? = null

        fun getInstance(context: Context): MediaPlayer =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildMediaPlayer(context,alert!!).also { INSTANCE = it }
            }

        private fun buildMediaPlayer(context: Context,uri: Uri) : MediaPlayer
        {
            val alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE)
            val mMediaPlayer = MediaPlayer()
            mMediaPlayer.setDataSource(context, alert)
            val audioManager: AudioManager = context.
                getSystemService(Context.AUDIO_SERVICE) as AudioManager
            if (audioManager.getStreamVolume(AudioManager.STREAM_RING) !== 0)
            {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                {
                    mMediaPlayer.setAudioAttributes(
                        AudioAttributes.Builder()
                            .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                            .build()
                    )
                }
                else mMediaPlayer.setAudioStreamType(AudioManager.STREAM_RING)
                mMediaPlayer.isLooping = true
                mMediaPlayer.prepare()
                mMediaPlayer.start()
            }
            return mMediaPlayer
        }
    }
}