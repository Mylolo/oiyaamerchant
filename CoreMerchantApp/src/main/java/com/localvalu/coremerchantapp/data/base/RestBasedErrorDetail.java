package com.localvalu.coremerchantapp.data.base;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RestBasedErrorDetail implements Parcelable
{

    @SerializedName("ErrorCode")
    @Expose
    public int errorCode;

    @SerializedName("ErrorMessage")
    @Expose
    public String errorMessage;

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeInt(this.errorCode);
        dest.writeString(this.errorMessage);
    }

    public RestBasedErrorDetail()
    {
    }

    protected RestBasedErrorDetail(Parcel in)
    {
        this.errorCode = in.readInt();
        this.errorMessage = in.readString();
    }

    public static final Creator<RestBasedErrorDetail> CREATOR = new Creator<RestBasedErrorDetail>()
    {
        @Override
        public RestBasedErrorDetail createFromParcel(Parcel source)
        {
            return new RestBasedErrorDetail(source);
        }

        @Override
        public RestBasedErrorDetail[] newArray(int size)
        {
            return new RestBasedErrorDetail[size];
        }
    };
}
