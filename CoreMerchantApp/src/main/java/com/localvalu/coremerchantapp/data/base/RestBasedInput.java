package com.localvalu.coremerchantapp.data.base;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public abstract class RestBasedInput {

    @SerializedName("Token")
    @Expose
    private String token = "/3+YFd5QZdSK9EKsB8+TlA==";

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
