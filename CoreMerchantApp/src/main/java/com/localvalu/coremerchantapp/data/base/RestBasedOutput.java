package com.localvalu.coremerchantapp.data.base;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RestBasedOutput implements Parcelable
{

    @SerializedName("ErrorDetails")
    @Expose
    private RestBasedErrorDetail errorDetail;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("message")
    @Expose
    private String message;

    public static final Creator<RestBasedOutput> CREATOR = new Creator<RestBasedOutput>()
    {
        @Override
        public RestBasedOutput createFromParcel(Parcel in)
        {
            return new RestBasedOutput(in);
        }

        @Override
        public RestBasedOutput[] newArray(int size)
        {
            return new RestBasedOutput[size];
        }
    };

    public RestBasedErrorDetail getErrorDetail() {
        return errorDetail;
    }

    public void setErrorDetail(RestBasedErrorDetail errorDetail) {
        this.errorDetail = errorDetail;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public RestBasedOutput()
    {
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeParcelable(this.errorDetail, flags);
        dest.writeString(this.status);
        dest.writeString(this.message);
    }

    protected RestBasedOutput(Parcel in)
    {
        this.errorDetail = in.readParcelable(RestBasedErrorDetail.class.getClassLoader());
        this.status = in.readString();
        this.message = in.readString();
    }

}
