package com.localvalu.coremerchantapp.data.input

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.localvalu.coremerchantapp.data.base.RestBasedInput

class AppVersionCheckInput : RestBasedInput()
{
    @SerializedName("AppName")
    @Expose
    val appName = "1"

    @SerializedName("DeviceName")
    @Expose
    var deviceName ="1"
}