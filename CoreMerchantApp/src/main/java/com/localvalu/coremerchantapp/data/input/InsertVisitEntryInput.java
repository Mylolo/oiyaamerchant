package com.localvalu.coremerchantapp.data.input;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.coremerchantapp.data.base.RestBasedInput;

public class InsertVisitEntryInput extends RestBasedInput
{
    @SerializedName("MerchantId")
    @Expose
    private Integer merchantId;

    @SerializedName("sQRcode")
    @Expose
    private String QRCode;

    @SerializedName("dVisitDate")
    @Expose
    private String visitDate;

    public Integer getMerchantId()
    {
        return merchantId;
    }

    public void setMerchantId(Integer merchantId)
    {
        this.merchantId = merchantId;
    }

    public String getQRCode()
    {
        return QRCode;
    }

    public void setQRCode(String QRCode)
    {
        this.QRCode = QRCode;
    }

    public String getVisitDate()
    {
        return visitDate;
    }

    public void setVisitDate(String visitDate)
    {
        this.visitDate = visitDate;
    }
}
