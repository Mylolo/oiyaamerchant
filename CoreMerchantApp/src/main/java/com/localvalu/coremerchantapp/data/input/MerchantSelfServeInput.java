package com.localvalu.coremerchantapp.data.input;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.coremerchantapp.data.base.RestBasedInput;


public class MerchantSelfServeInput extends RestBasedInput
{
    @SerializedName("Name")
    @Expose
    private String name;

    @SerializedName("MobileNo")
    @Expose
    private String mobileNumber;

    @SerializedName("EmailId")
    @Expose
    private String emailId;

    @SerializedName("Postcode")
    @Expose
    private String postCode;

    @SerializedName("BusinessName")
    @Expose
    private String businessName;

    @SerializedName("BusinesNo")
    @Expose
    private String businessNumber;

    @SerializedName("providername")
    @Expose
    private String providerName;

    @SerializedName("comment")
    @Expose
    private String comment;

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getContactNumber()
    {
        return mobileNumber;
    }

    public void setContactNumber(String mobileNumber)
    {
        this.mobileNumber = mobileNumber;
    }

    public String getEmailId()
    {
        return emailId;
    }

    public void setEmailId(String emailId)
    {
        this.emailId = emailId;
    }

    public String getPostCode()
    {
        return postCode;
    }

    public void setPostCode(String postCode)
    {
        this.postCode = postCode;
    }

    public String getBusinessName()
    {
        return businessName;
    }

    public void setBusinessName(String businessName)
    {
        this.businessName = businessName;
    }

    public String getBusinessNumber()
    {
        return businessNumber;
    }

    public void setBusinessNumber(String businessNumber)
    {
        this.businessNumber = businessNumber;
    }

    public String getProviderName()
    {
        return providerName;
    }

    public void setProviderName(String providerName)
    {
        this.providerName = providerName;
    }

    public String getComment()
    {
        return comment;
    }

    public void setComment(String comment)
    {
        this.comment = comment;
    }

}
