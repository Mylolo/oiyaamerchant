package com.localvalu.coremerchantapp.data.input;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.coremerchantapp.data.base.RestBasedInput;

public class MyAppointmentStatusChangeInput extends RestBasedInput
{

    @SerializedName("slotId")
    @Expose
    private String slotId;

    @SerializedName("Status")
    @Expose
    private String status;

    public String getSlotId()
    {
        return slotId;
    }

    public void setSlotId(String slotId)
    {
        this.slotId = slotId;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }
}
