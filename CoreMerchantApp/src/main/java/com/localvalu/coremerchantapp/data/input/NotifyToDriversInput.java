package com.localvalu.coremerchantapp.data.input;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.coremerchantapp.data.base.RestBasedInput;

public class NotifyToDriversInput extends RestBasedInput
{
    @SerializedName("retailerId")
    @Expose
    private int retailerId;

    @SerializedName("orderId")
    @Expose
    private int orderId;

    @SerializedName("DriverId")
    @Expose
    private int DriverId;

    public int getRetailerId()
    {
        return retailerId;
    }

    public void setRetailerId(int retailerId)
    {
        this.retailerId = retailerId;
    }

    public int getOrderId()
    {
        return orderId;
    }

    public void setOrderId(int orderId)
    {
        this.orderId = orderId;
    }

    public int getDriverId()
    {
        return DriverId;
    }

    public void setDriverId(int driverId)
    {
        DriverId = driverId;
    }
}
