package com.localvalu.coremerchantapp.data.input;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.coremerchantapp.data.base.RestBasedInput;

import java.util.List;

public class NotifyToPoolDriversInput extends RestBasedInput
{
    @SerializedName("retailerId")
    @Expose
    private int retailerId;

    @SerializedName("orderId")
    @Expose
    private int orderId;

    @SerializedName("DriverId")
    @Expose
    private List<Integer> DriverId;

    public int getRetailerId()
    {
        return retailerId;
    }

    public void setRetailerId(int retailerId)
    {
        this.retailerId = retailerId;
    }

    public int getOrderId()
    {
        return orderId;
    }

    public void setOrderId(int orderId)
    {
        this.orderId = orderId;
    }

    public List<Integer> getDriverId()
    {
        return DriverId;
    }

    public void setDriverId(List<Integer> driverId)
    {
        DriverId = driverId;
    }

}
