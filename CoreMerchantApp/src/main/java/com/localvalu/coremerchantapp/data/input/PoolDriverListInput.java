package com.localvalu.coremerchantapp.data.input;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.coremerchantapp.data.base.RestBasedInput;


public class PoolDriverListInput extends RestBasedInput
{
    @SerializedName("poolname")
    @Expose
    private String poolName;

    public String getPoolName()
    {
        return poolName;
    }

    public void setPoolName(String types)
    {
        this.poolName = types;
    }
}
