package com.localvalu.coremerchantapp.data.input;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.coremerchantapp.data.base.RestBasedInput;


public class SaleTransactionInput extends RestBasedInput
{

    @SerializedName("AccountId")
    @Expose
    private Integer accountId;

    @SerializedName("RetailerID")
    @Expose
    private String retailerID;


    @SerializedName("ConsumerQRCode")
    @Expose
    private String consumerQRCode;

    @SerializedName("SaleTotal")
    @Expose
    private Double saleTotal;

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public String getRetailerID()
    {
        return retailerID;
    }

    public void setRetailerID(String retailerID)
    {
        this.retailerID = retailerID;
    }

    public String getConsumerQRCode()
    {
        return consumerQRCode;
    }

    public void setConsumerQRCode(String consumerQRCode)
    {
        this.consumerQRCode = consumerQRCode;
    }

    public Double getSaleTotal() {
        return saleTotal;
    }

    public void setSaleTotal(Double saleTotal) {
        this.saleTotal = saleTotal;
    }
}
