package com.localvalu.coremerchantapp.data.input;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.coremerchantapp.data.base.RestBasedInput;

public class SignInInput extends RestBasedInput
{

    @SerializedName("EmailId")
    @Expose
    private String email;

    @SerializedName("Password")
    @Expose
    private String password;

    @SerializedName("FCMid")
    @Expose
    private String fcmId;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFcmId()
    {
        return fcmId;
    }

    public void setFcmId(String fcmId)
    {
        this.fcmId = fcmId;
    }
}
