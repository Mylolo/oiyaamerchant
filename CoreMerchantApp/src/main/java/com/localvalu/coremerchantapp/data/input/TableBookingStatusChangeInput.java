package com.localvalu.coremerchantapp.data.input;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.coremerchantapp.data.base.RestBasedInput;

public class TableBookingStatusChangeInput extends RestBasedInput
{

    @SerializedName("TableBookid")
    @Expose
    private String tableBookId;

    @SerializedName("Status")
    @Expose
    private String status;

    public String getTableBookid()
    {
        return tableBookId;
    }

    public void setTableBookid(String tableBookid)
    {
        this.tableBookId = tableBookid;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }
}
