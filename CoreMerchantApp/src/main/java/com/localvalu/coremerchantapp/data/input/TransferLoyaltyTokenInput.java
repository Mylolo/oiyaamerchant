package com.localvalu.coremerchantapp.data.input;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.coremerchantapp.data.base.RestBasedInput;


public class TransferLoyaltyTokenInput extends RestBasedInput
{

    @SerializedName("RetailerId")
    @Expose
    private String retailerId;

    @SerializedName("CurrentBalance")
    @Expose
    private String currentBalance;

    @SerializedName("TransferAmount")
    @Expose
    private String transferAmount;

    @SerializedName("BalanceAfterTransfer")
    @Expose
    private String balanceAfterTransfer;

    @SerializedName("LTtoAccountID")
    @Expose
    private String toAccountID;

    public String getRetailerId()
    {
        return retailerId;
    }

    public void setRetailerId(String retailerId)
    {
        this.retailerId = retailerId;
    }

    public String getCurrentBalance()
    {
        return currentBalance;
    }

    public void setCurrentBalance(String currentBalance)
    {
        this.currentBalance = currentBalance;
    }

    public String getTransferAmount()
    {
        return transferAmount;
    }

    public void setTransferAmount(String transferAmount)
    {
        this.transferAmount = transferAmount;
    }

    public String getBalanceAfterTransfer()
    {
        return balanceAfterTransfer;
    }

    public void setBalanceAfterTransfer(String balanceAfterTransfer)
    {
        this.balanceAfterTransfer = balanceAfterTransfer;
    }

    public String getToAccountID()
    {
        return toAccountID;
    }

    public void setToAccountID(String toAccountID)
    {
        this.toAccountID = toAccountID;
    }
}
