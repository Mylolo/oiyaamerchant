package com.localvalu.coremerchantapp.data.output;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FoodItem implements Parcelable
{
    @SerializedName("item_name")
    @Expose
    private String itemName;

    @SerializedName("quantity")
    @Expose
    private String quantity;

    @SerializedName("single_quantity_price")
    @Expose
    private String singleQuantityPrice;

    @SerializedName("item_total_price")
    @Expose
    private String itemTotalPrice;

    @SerializedName("foodDesc")
    @Expose
    private String foodDesc;

    @SerializedName("foodType")
    @Expose
    private String foodType;

    @SerializedName("foodPic")
    @Expose
    private String foodPic;

    @SerializedName("SizeName")
    @Expose
    private String sizeName;

    @SerializedName("size_id")
    @Expose
    private String sizeId;

    public String getItemName()
    {
        return itemName;
    }

    public void setItemName(String itemName)
    {
        this.itemName = itemName;
    }

    public String getQuantity()
    {
        return quantity;
    }

    public void setQuantity(String quantity)
    {
        this.quantity = quantity;
    }

    public String getSingleQuantityPrice()
    {
        return singleQuantityPrice;
    }

    public void setSingleQuantityPrice(String singleQuantityPrice)
    {
        this.singleQuantityPrice = singleQuantityPrice;
    }

    public String getItemTotalPrice()
    {
        return itemTotalPrice;
    }

    public void setItemTotalPrice(String itemTotalPrice)
    {
        this.itemTotalPrice = itemTotalPrice;
    }

    public String getFoodDesc()
    {
        return foodDesc;
    }

    public void setFoodDesc(String foodDesc)
    {
        this.foodDesc = foodDesc;
    }

    public String getFoodType()
    {
        return foodType;
    }

    public void setFoodType(String foodType)
    {
        this.foodType = foodType;
    }

    public String getFoodPic()
    {
        return foodPic;
    }

    public void setFoodPic(String foodPic)
    {
        this.foodPic = foodPic;
    }

    public String getSizeName()
    {
        return sizeName;
    }

    public void setSizeName(String sizeName)
    {
        sizeName = sizeName;
    }

    public String getSizeId()
    {
        return sizeId;
    }

    public void setSizeId(String sizeId)
    {
        this.sizeId = sizeId;
    }


    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(this.itemName);
        dest.writeString(this.quantity);
        dest.writeString(this.singleQuantityPrice);
        dest.writeString(this.itemTotalPrice);
        dest.writeString(this.foodDesc);
        dest.writeString(this.foodType);
        dest.writeString(this.foodPic);
        dest.writeString(this.sizeName);
        dest.writeString(this.sizeId);
    }

    public FoodItem()
    {
    }

    protected FoodItem(Parcel in)
    {
        this.itemName = in.readString();
        this.quantity = in.readString();
        this.singleQuantityPrice = in.readString();
        this.itemTotalPrice = in.readString();
        this.foodDesc = in.readString();
        this.foodType = in.readString();
        this.foodPic = in.readString();
        this.sizeName = in.readString();
        this.sizeId = in.readString();
    }

    public static final Creator<FoodItem> CREATOR = new Creator<FoodItem>()
    {
        @Override
        public FoodItem createFromParcel(Parcel source)
        {
            return new FoodItem(source);
        }

        @Override
        public FoodItem[] newArray(int size)
        {
            return new FoodItem[size];
        }
    };
}
