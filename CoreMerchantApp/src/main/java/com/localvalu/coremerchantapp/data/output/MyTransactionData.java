package com.localvalu.coremerchantapp.data.output;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MyTransactionData {

    @SerializedName("RetailerId")
    @Expose
    private String retailerId;

    @SerializedName("RetailerName")
    @Expose
    private String retailerName;

    @SerializedName("SaleAmount")
    @Expose
    private Double saleAmount;

    @SerializedName("SIRTransID")
    @Expose
    private Integer transactionId;

    @SerializedName("TransactionDate")
    @Expose
    private String transactionDate;

    @SerializedName("TransType")
    @Expose
    private String transactionType;

    public String getRetailerId() {
        return retailerId;
    }

    public void setRetailerId(String retailerId) {
        this.retailerId = retailerId;
    }

    public String getRetailerName() {
        return retailerName;
    }

    public void setRetailerName(String retailerName) {
        this.retailerName = retailerName;
    }

    public Double getSaleAmount() {
        return saleAmount;
    }

    public void setSaleAmount(Double saleAmount) {
        this.saleAmount = saleAmount;
    }

    public Integer getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Integer transactionId) {
        this.transactionId = transactionId;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }
}
