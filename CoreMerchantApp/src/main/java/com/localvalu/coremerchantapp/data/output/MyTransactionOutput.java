package com.localvalu.coremerchantapp.data.output;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MyTransactionOutput
{

    @SerializedName("MerchantApp_TransactionHistoryResult")
    @Expose
    private MyTransactionResult result;

    public MyTransactionResult getResult()
    {
        return result;
    }

    public void setResult(MyTransactionResult result)
    {
        this.result = result;
    }
}
