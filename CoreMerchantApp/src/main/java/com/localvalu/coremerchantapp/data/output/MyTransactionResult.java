package com.localvalu.coremerchantapp.data.output;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.coremerchantapp.data.base.RestBasedOutput;

import java.util.List;

public class MyTransactionResult extends RestBasedOutput
{

    @SerializedName("MerchantApp_TransactionHistoryList")
    @Expose
    private List<MyTransactionData> datas;

    public List<MyTransactionData> getDatas()
    {
        return datas;
    }

    public void setDatas(List<MyTransactionData> datas)
    {
        this.datas = datas;
    }
}
