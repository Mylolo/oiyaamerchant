package com.localvalu.coremerchantapp.data.output;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SaleTransactionOutput
{

    @SerializedName("MerchantSaleTransactionResult")
    @Expose
    private SaleTransactionResult result;

    public SaleTransactionResult getResult()
    {
        return result;
    }

    public void setResult(SaleTransactionResult result)
    {
        this.result = result;
    }
}
