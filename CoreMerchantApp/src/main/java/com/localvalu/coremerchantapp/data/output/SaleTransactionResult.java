package com.localvalu.coremerchantapp.data.output;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.coremerchantapp.data.base.RestBasedOutput;

public class SaleTransactionResult extends RestBasedOutput
{

    @SerializedName("Token_Applied")
    @Expose
    private Double tokenApplied;

    @SerializedName("Bonus_Tokens")
    @Expose
    private Double bonusTokens;

    @SerializedName("Balance_ToPay")
    @Expose
    private String balanceToPay;

    @SerializedName("Total")
    @Expose
    private String total;

    @SerializedName("Tokens_earned")
    @Expose
    private Double tokensEarned;

    @SerializedName("NewToken_Balance")
    @Expose
    private Double newTokenBalance;

    @SerializedName("MerchantName")
    @Expose
    private String merchantName;

    @SerializedName("CustomerName")
    @Expose
    private String customerName;

    @SerializedName("PromotionPercentage")
    @Expose
    private String promotion;

    public Double getTokenApplied() {
        return tokenApplied;
    }

    public void setTokenApplied(Double tokenApplied) {
        this.tokenApplied = tokenApplied;
    }

    public Double getBonusTokens() {
        return bonusTokens;
    }

    public void setBonusTokens(Double bonusTokens) {
        this.bonusTokens = bonusTokens;
    }

    public String getBalanceToPay() {
        return balanceToPay;
    }

    public void setBalanceToPay(String balanceToPay) {
        this.balanceToPay = balanceToPay;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public Double getTokensEarned() {
        return tokensEarned;
    }

    public void setTokensEarned(Double tokensEarned) {
        this.tokensEarned = tokensEarned;
    }

    public Double getNewTokenBalance() {
        return newTokenBalance;
    }

    public void setNewTokenBalance(Double newTokenBalance) {
        this.newTokenBalance = newTokenBalance;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getPromotion() {
        return promotion;
    }

    public void setPromotion(String promotion) {
        this.promotion = promotion;
    }
}
