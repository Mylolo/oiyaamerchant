package com.localvalu.coremerchantapp.data.output;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SignInOutput {

    @SerializedName("MemberVerificationV1Result")
    @Expose
    private SignInResult result;

    public SignInResult getResult() {
        return result;
    }

    public void setResult(SignInResult result) {
        this.result = result;
    }
}
