package com.localvalu.coremerchantapp.data.output;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.coremerchantapp.data.base.RestBasedOutput;

public class SignInResult extends RestBasedOutput implements Parcelable
{

    @SerializedName("RetailerId")
    @Expose
    private Integer retailerId;

    @SerializedName("AccountId")
    @Expose
    private Integer accountId;

    @SerializedName("ispax")
    @Expose
    private Integer isPax;

    @SerializedName("paxSerailNo")
    @Expose
    private Integer paxSerialNumber;

    @SerializedName("bookingid")
    @Expose
    private String bookingTypeId;

    @SerializedName("retailerType")
    @Expose
    private String retailerType;

    @SerializedName("driverType")
    @Expose
    private String driverType;

    @SerializedName("poolname")
    @Expose
    private String poolName;

    private String email;

    public Integer getRetailerId()
    {
        return retailerId;
    }

    public void setRetailerId(Integer retailerId)
    {
        this.retailerId = retailerId;
    }

    public Integer getAccountId()
    {
        return accountId;
    }

    public void setAccountId(Integer accountId)
    {
        this.accountId = accountId;
    }

    public Integer IsPax()
    {
        return isPax;
    }

    public void setPax(Integer isPax)
    {
        this.isPax = isPax;
    }

    public Integer getPaxSerialNumber()
    {
        return paxSerialNumber;
    }

    public void setPaxSerialNumber(Integer paxSerialNumber)
    {
        this.paxSerialNumber = paxSerialNumber;
    }

    public Integer getIsPax()
    {
        return isPax;
    }

    public void setIsPax(Integer isPax)
    {
        this.isPax = isPax;
    }

    public String getBookingTypeId()
    {
        return bookingTypeId;
    }

    public void setBookingTypeId(String bookingTypeId)
    {
        this.bookingTypeId = bookingTypeId;
    }

    public String getRetailerType()
    {
        return retailerType;
    }

    public void setRetailerType(String retailerType)
    {
        this.retailerType = retailerType;
    }

    public String getDriverType() {
        return driverType;
    }

    public void setDriverType(String driverType) {
        this.driverType = driverType;
    }

    public String getPoolName() {
        return poolName;
    }

    public void setPoolName(String poolName) {
        this.poolName = poolName;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        super.writeToParcel(dest, flags);
        dest.writeValue(this.retailerId);
        dest.writeValue(this.accountId);
        dest.writeValue(this.isPax);
        dest.writeValue(this.paxSerialNumber);
        dest.writeString(this.bookingTypeId);
        dest.writeString(this.retailerType);
    }

    public SignInResult()
    {
    }

    protected SignInResult(Parcel in)
    {
        super(in);
        this.retailerId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.accountId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.isPax = (Integer) in.readValue(Integer.class.getClassLoader());
        this.paxSerialNumber = (Integer) in.readValue(Integer.class.getClassLoader());
        this.bookingTypeId = in.readString();
        this.retailerType = in.readString();
    }

    public static final Creator<SignInResult> CREATOR = new Creator<SignInResult>()
    {
        @Override
        public SignInResult createFromParcel(Parcel source)
        {
            return new SignInResult(source);
        }

        @Override
        public SignInResult[] newArray(int size)
        {
            return new SignInResult[size];
        }
    };

}
