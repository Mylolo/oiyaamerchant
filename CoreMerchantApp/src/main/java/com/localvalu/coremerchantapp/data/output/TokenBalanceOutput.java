package com.localvalu.coremerchantapp.data.output;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TokenBalanceOutput
{

    @SerializedName("RetailerBalanceEnquiryResult")
    @Expose
    private TokenBalanceResult result;

    public TokenBalanceResult getResult() {
        return result;
    }

    public void setResult(TokenBalanceResult result) {
        this.result = result;
    }
}
