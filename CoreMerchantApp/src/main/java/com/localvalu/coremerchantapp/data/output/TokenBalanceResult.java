package com.localvalu.coremerchantapp.data.output;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.coremerchantapp.data.base.RestBasedOutput;

public class TokenBalanceResult extends RestBasedOutput
{

    @SerializedName("RetailerId")
    @Expose
    private Integer retailerId;

    @SerializedName("CurrentTokenBalance")
    @Expose
    private Double currentTokenBalance;

    @SerializedName("TotalToken_earned")
    @Expose
    private Double totalTokenEarned;

    @SerializedName("TotalToken_Redeemed")
    @Expose
    private Double totalTokenRedeemed;

    @SerializedName("QRCode")
    @Expose
    private String qrCode;

    public Integer getRetailerId() {
        return retailerId;
    }

    public void setRetailerId(Integer retailerId) {
        this.retailerId = retailerId;
    }

    public Double getCurrentTokenBalance() {
        return currentTokenBalance;
    }

    public void setCurrentTokenBalance(Double currentTokenBalance) {
        this.currentTokenBalance = currentTokenBalance;
    }

    public Double getTotalTokenEarned() {
        return totalTokenEarned;
    }

    public void setTotalTokenEarned(Double totalTokenEarned) {
        this.totalTokenEarned = totalTokenEarned;
    }

    public Double getTotalTokenRedeemed() {
        return totalTokenRedeemed;
    }

    public void setTotalTokenRedeemed(Double totalTokenRedeemed) {
        this.totalTokenRedeemed = totalTokenRedeemed;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }
}
