package com.localvalu.coremerchantapp.data.output;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TransferLoyaltyTokenOutput
{

    @SerializedName("Merchant_TransferLTFriendResult")
    @Expose
    private TransferLoyaltyTokenResult result;

    public TransferLoyaltyTokenResult getResult()
    {
        return result;
    }

    public void setResult(TransferLoyaltyTokenResult result)
    {
        this.result = result;
    }
}
