package com.localvalu.coremerchantapp.data.output.appversioncheck

import com.google.gson.annotations.SerializedName

class AppVersion(@SerializedName("AppVersionResult") val appVersionResult:AppVersionResult)
{

}