package com.localvalu.coremerchantapp.data.output.appversioncheck

import com.google.gson.annotations.SerializedName

class AppVersionOutput(@SerializedName("Appversion") val appVersionCheckResult:AppVersion)
{

}