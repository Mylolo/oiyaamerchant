package com.localvalu.coremerchantapp.data.output.appversioncheck

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.localvalu.coremerchantapp.data.base.RestBasedInput
import com.localvalu.coremerchantapp.data.base.RestBasedOutput

class AppVersionResult : RestBasedOutput()
{
    @SerializedName("versionNo")
    @Expose
    val versionNo = ""

    @SerializedName("DeviceName")
    @Expose
    var versioncode ="1"
}