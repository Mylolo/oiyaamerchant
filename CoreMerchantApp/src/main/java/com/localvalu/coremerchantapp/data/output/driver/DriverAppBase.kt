import com.google.gson.annotations.SerializedName

data class DriverAppBase(
	@SerializedName("DriverAppResult") val driverAppResult: DriverAppResult,
	@SerializedName("msg") val msg: String
)