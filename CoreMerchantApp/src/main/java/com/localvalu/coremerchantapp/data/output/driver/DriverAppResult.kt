import com.google.gson.annotations.SerializedName

data class DriverAppResult (
	@SerializedName("GetDriverListResult") val getDriverListResult : GetDriverListResult
)