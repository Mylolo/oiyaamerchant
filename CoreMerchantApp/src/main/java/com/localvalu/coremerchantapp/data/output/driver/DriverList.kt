package com.localvalu.coremerchantapp.data.output.driver

import com.google.gson.annotations.SerializedName

data class DriverList (

	@SerializedName("Driverid") val driverid : Int,
	@SerializedName("Drivername") val drivername : String,
	@SerializedName("Retailerid") val retailerid : Int
)