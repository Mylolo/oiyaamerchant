import com.google.gson.annotations.SerializedName

data class ErrorDetails (

	@SerializedName("ErrorCode") val errorCode : Int,
	@SerializedName("ErrorMessage") val errorMessage : String
)