import com.google.gson.annotations.SerializedName
import com.localvalu.coremerchantapp.data.output.driver.DriverList

data class GetDriverListResult (
	@SerializedName("DriverList") val driverList : List<DriverList>,
	@SerializedName("ErrorCode") val errorCode : Int,
	@SerializedName("errorMessage") val errorMessage : String,
	@SerializedName("ErrorDetails") val errorDetails : ErrorDetails,
	@SerializedName("status") val status : Boolean
)