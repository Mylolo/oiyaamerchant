package com.localvalu.coremerchantapp.data.output.driver

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.localvalu.coremerchantapp.data.base.RestBasedInput

class OwnDriversListRequest : RestBasedInput()
{
    @SerializedName("types")
    @Expose
    var types: String? = null

    @SerializedName("retailerid")
    @Expose
    var retailerId: String? = null
}