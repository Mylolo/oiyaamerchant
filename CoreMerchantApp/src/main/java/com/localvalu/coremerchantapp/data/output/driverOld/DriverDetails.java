package com.localvalu.coremerchantapp.data.output.driverOld;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DriverDetails implements Parcelable
{
    @SerializedName("Driverid")
    @Expose
    private String driverid;

    @SerializedName("Drivername")
    @Expose
    private String Drivername;

    public String getDriverid()
    {
        return driverid;
    }

    public void setDriverid(String driverid)
    {
        this.driverid = driverid;
    }

    public String getDrivername()
    {
        return Drivername;
    }

    public void setDrivername(String drivername)
    {
        Drivername = drivername;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(this.driverid);
        dest.writeString(this.Drivername);
    }

    public DriverDetails()
    {
    }

    protected DriverDetails(Parcel in)
    {
        this.driverid = in.readString();
        this.Drivername = in.readString();
    }

    public static final Creator<DriverDetails> CREATOR = new Creator<DriverDetails>()
    {
        @Override
        public DriverDetails createFromParcel(Parcel source)
        {
            return new DriverDetails(source);
        }

        @Override
        public DriverDetails[] newArray(int size)
        {
            return new DriverDetails[size];
        }
    };
}
