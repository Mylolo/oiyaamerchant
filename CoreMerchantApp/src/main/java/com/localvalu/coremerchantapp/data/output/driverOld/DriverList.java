package com.localvalu.coremerchantapp.data.output.driverOld;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.coremerchantapp.data.base.RestBasedOutput;

import java.util.List;

public class DriverList extends RestBasedOutput implements  Parcelable
{
    @SerializedName("DriverList")
    @Expose
    private List<DriverDetails> driverDetailsList;

    public List<DriverDetails> getDriverDetailsList()
    {
        return driverDetailsList;
    }

    public void setDriverDetailsList(List<DriverDetails> driverDetailsList)
    {
        this.driverDetailsList = driverDetailsList;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeTypedList(this.driverDetailsList);
    }

    public DriverList()
    {
    }

    protected DriverList(Parcel in)
    {
        this.driverDetailsList = in.createTypedArrayList(DriverDetails.CREATOR);
    }

    public static final Creator<DriverList> CREATOR = new Creator<DriverList>()
    {
        @Override
        public DriverList createFromParcel(Parcel source)
        {
            return new DriverList(source);
        }

        @Override
        public DriverList[] newArray(int size)
        {
            return new DriverList[size];
        }
    };

}
