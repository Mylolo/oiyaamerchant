package com.localvalu.coremerchantapp.data.output.driverOld;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class DriverListOutput
{
    @SerializedName("DriverAppResult")
    @Expose
    private DriverListResult driverListResult;

    public DriverListResult getDriverListResult()
    {
        return driverListResult;
    }

    public void setDriverListResult(DriverListResult driverListResult)
    {
        this.driverListResult = driverListResult;
    }
}
