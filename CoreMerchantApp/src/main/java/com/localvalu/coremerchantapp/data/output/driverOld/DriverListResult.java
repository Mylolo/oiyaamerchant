package com.localvalu.coremerchantapp.data.output.driverOld;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class DriverListResult
{
    @SerializedName("/GetDriverListResult")
    @Expose
    private DriverList driverListResult;

    public DriverList getDriverListResult()
    {
        return driverListResult;
    }

    public void setDriverListResult(DriverList driverListResult)
    {
        this.driverListResult = driverListResult;
    }
    
}
