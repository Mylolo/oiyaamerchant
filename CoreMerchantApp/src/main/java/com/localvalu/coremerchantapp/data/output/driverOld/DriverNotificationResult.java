package com.localvalu.coremerchantapp.data.output.driverOld;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.coremerchantapp.data.base.RestBasedOutput;

public class DriverNotificationResult extends RestBasedOutput implements Parcelable
{
    @SerializedName("DriverStatus")
    @Expose
    private DriverStatus driverStatus;

    public DriverStatus getDriverStatus()
    {
        return driverStatus;
    }

    public void setDriverStatus(DriverStatus driverStatus)
    {
        this.driverStatus = driverStatus;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        super.writeToParcel(dest, flags);
        dest.writeParcelable(this.driverStatus, flags);
    }

    public DriverNotificationResult()
    {
    }

    protected DriverNotificationResult(Parcel in)
    {
        super(in);
        this.driverStatus = in.readParcelable(DriverStatus.class.getClassLoader());
    }

    public static final Creator<DriverNotificationResult> CREATOR = new Creator<DriverNotificationResult>()
    {
        @Override
        public DriverNotificationResult createFromParcel(Parcel source)
        {
            return new DriverNotificationResult(source);
        }

        @Override
        public DriverNotificationResult[] newArray(int size)
        {
            return new DriverNotificationResult[size];
        }
    };
}
