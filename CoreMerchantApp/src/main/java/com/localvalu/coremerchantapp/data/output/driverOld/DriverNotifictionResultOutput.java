package com.localvalu.coremerchantapp.data.output.driverOld;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class DriverNotifictionResultOutput
{
    @SerializedName("DriverNotificationResult")
    @Expose
    private DriverNotificationResult driverNotificationResult;

    public DriverNotificationResult getDriverNotificationResult()
    {
        return driverNotificationResult;
    }

    public void setDriverNotificationResult(DriverNotificationResult driverNotificationResult)
    {
        this.driverNotificationResult = driverNotificationResult;
    }
}
