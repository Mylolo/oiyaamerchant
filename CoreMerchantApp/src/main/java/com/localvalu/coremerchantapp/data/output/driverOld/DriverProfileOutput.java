package com.localvalu.coremerchantapp.data.output.driverOld;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class DriverProfileOutput
{
    @SerializedName("GetDriverprofileResult")
    @Expose
    private DriverProfileResult driverProfileResult;

    public DriverProfileResult getDriverProfileResult()
    {
        return driverProfileResult;
    }

    public void setDriverProfileResult(DriverProfileResult driverProfileResult)
    {
        this.driverProfileResult = driverProfileResult;
    }
}
