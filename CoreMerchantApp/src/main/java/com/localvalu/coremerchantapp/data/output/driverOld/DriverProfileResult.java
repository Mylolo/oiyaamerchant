package com.localvalu.coremerchantapp.data.output.driverOld;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.coremerchantapp.data.base.RestBasedOutput;

public class DriverProfileResult extends RestBasedOutput implements Parcelable
{
    @SerializedName("Name")
    @Expose
    private String name;

    @SerializedName("Email")
    @Expose
    private String email;

    @SerializedName("Mobileno")
    @Expose
    private String mobileNumber;

    @SerializedName("Gender")
    @Expose
    private String gender;

    @SerializedName("Dob")
    @Expose
    private String dob;

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getMobileNumber()
    {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber)
    {
        this.mobileNumber = mobileNumber;
    }

    public String getGender()
    {
        return gender;
    }

    public void setGender(String gender)
    {
        this.gender = gender;
    }

    public String getDob()
    {
        return dob;
    }

    public void setDob(String dob)
    {
        this.dob = dob;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        super.writeToParcel(dest, flags);
        dest.writeString(this.name);
        dest.writeString(this.email);
        dest.writeString(this.mobileNumber);
        dest.writeString(this.gender);
        dest.writeString(this.dob);
    }

    public DriverProfileResult()
    {
    }

    protected DriverProfileResult(Parcel in)
    {
        super(in);
        this.name = in.readString();
        this.email = in.readString();
        this.mobileNumber = in.readString();
        this.gender = in.readString();
        this.dob = in.readString();
    }

    public static final Creator<DriverProfileResult> CREATOR = new Creator<DriverProfileResult>()
    {
        @Override
        public DriverProfileResult createFromParcel(Parcel source)
        {
            return new DriverProfileResult(source);
        }

        @Override
        public DriverProfileResult[] newArray(int size)
        {
            return new DriverProfileResult[size];
        }
    };
}
