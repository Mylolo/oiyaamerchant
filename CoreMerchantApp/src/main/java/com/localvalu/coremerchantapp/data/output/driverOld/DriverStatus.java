package com.localvalu.coremerchantapp.data.output.driverOld;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DriverStatus implements Parcelable
{
    @SerializedName("Driverid")
    @Expose
    private String driverId;

    @SerializedName("retailerId")
    @Expose
    private String retailerId;

    @SerializedName("orderId")
    @Expose
    private String orderId;

    @SerializedName("driverNotifyStatus")
    @Expose
    private String driverNotifyStatus;

    public String getDriverId()
    {
        return driverId;
    }

    public void setDriverId(String driverId)
    {
        this.driverId = driverId;
    }

    public String getRetailerId()
    {
        return retailerId;
    }

    public void setRetailerId(String retailerId)
    {
        this.retailerId = retailerId;
    }

    public String getOrderId()
    {
        return orderId;
    }

    public void setOrderId(String orderId)
    {
        this.orderId = orderId;
    }

    public String getDriverNotifyStatus()
    {
        return driverNotifyStatus;
    }

    public void setDriverNotifyStatus(String driverNotifyStatus)
    {
        this.driverNotifyStatus = driverNotifyStatus;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(this.driverId);
        dest.writeString(this.retailerId);
        dest.writeString(this.orderId);
        dest.writeString(this.driverNotifyStatus);
    }

    public DriverStatus()
    {
    }

    protected DriverStatus(Parcel in)
    {
        this.driverId = in.readString();
        this.retailerId = in.readString();
        this.orderId = in.readString();
        this.driverNotifyStatus = in.readString();
    }

    public static final Creator<DriverStatus> CREATOR = new Creator<DriverStatus>()
    {
        @Override
        public DriverStatus createFromParcel(Parcel source)
        {
            return new DriverStatus(source);
        }

        @Override
        public DriverStatus[] newArray(int size)
        {
            return new DriverStatus[size];
        }
    };
}
