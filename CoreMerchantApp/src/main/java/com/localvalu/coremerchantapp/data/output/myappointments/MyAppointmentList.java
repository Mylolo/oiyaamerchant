package com.localvalu.coremerchantapp.data.output.myappointments;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MyAppointmentList implements Parcelable
{
    @SerializedName("slotid")
    @Expose
    private String slotId;

    @SerializedName("Userid")
    @Expose
    private String userId;

    @SerializedName("BusinessId")
    @Expose
    private String businessId;

    @SerializedName("PreferredDate")
    @Expose
    private String preferredDate;

    @SerializedName("PreferredTime")
    @Expose
    private String preferredTime;

    @SerializedName("NoOfGuest")
    @Expose
    private String NnOfGuests;

    @SerializedName("SpecialRequest")
    @Expose
    private String specialRequest;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("userName")
    @Expose
    private String userName;

    @SerializedName("tradingname")
    @Expose
    private String tradingName;

    @SerializedName("RetailerType")
    @Expose
    private String retailerType;

    private boolean updating=false;

    public String getSlotId()
    {
        return slotId;
    }

    public void setSlotId(String slotId)
    {
        this.slotId = slotId;
    }

    public String getUserId()
    {
        return userId;
    }

    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    public String getBusinessId()
    {
        return businessId;
    }

    public void setBusinessId(String businessId)
    {
        this.businessId = businessId;
    }

    public String getPreferredDate()
    {
        return preferredDate;
    }

    public void setPreferredDate(String preferredDate)
    {
        this.preferredDate = preferredDate;
    }

    public String getPreferredTime()
    {
        return preferredTime;
    }

    public void setPreferredTime(String preferredTime)
    {
        this.preferredTime = preferredTime;
    }

    public String getNnOfGuests()
    {
        return NnOfGuests;
    }

    public void setNnOfGuests(String nnOfGuests)
    {
        NnOfGuests = nnOfGuests;
    }

    public String getSpecialRequest()
    {
        return specialRequest;
    }

    public void setSpecialRequest(String specialRequest)
    {
        this.specialRequest = specialRequest;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getTradingName()
    {
        return tradingName;
    }

    public void setTradingName(String tradingName)
    {
        this.tradingName = tradingName;
    }

    public String getRetailerType()
    {
        return retailerType;
    }

    public void setRetailerType(String retailerType)
    {
        this.retailerType = retailerType;
    }

    public boolean isUpdating()
    {
        return updating;
    }

    public void setUpdating(boolean updating)
    {
        this.updating = updating;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(this.userId);
        dest.writeString(this.businessId);
        dest.writeString(this.preferredDate);
        dest.writeString(this.preferredTime);
        dest.writeString(this.NnOfGuests);
        dest.writeString(this.specialRequest);
        dest.writeString(this.status);
        dest.writeString(this.userName);
        dest.writeString(this.tradingName);
        dest.writeString(this.retailerType);
        dest.writeByte((byte) (updating ? 1 : 0));
    }

    public MyAppointmentList()
    {
    }

    protected MyAppointmentList(Parcel in)
    {
        this.userId = in.readString();
        this.businessId = in.readString();
        this.preferredDate = in.readString();
        this.preferredTime = in.readString();
        this.NnOfGuests = in.readString();
        this.specialRequest = in.readString();
        this.status = in.readString();
        this.userName = in.readString();
        this.tradingName = in.readString();
        this.retailerType = in.readString();
        this.updating = in.readByte() != 0;
    }

    public static final Creator<MyAppointmentList> CREATOR = new Creator<MyAppointmentList>()
    {
        @Override
        public MyAppointmentList createFromParcel(Parcel source)
        {
            return new MyAppointmentList(source);
        }

        @Override
        public MyAppointmentList[] newArray(int size)
        {
            return new MyAppointmentList[size];
        }
    };
}
