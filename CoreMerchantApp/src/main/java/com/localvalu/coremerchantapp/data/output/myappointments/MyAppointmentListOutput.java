package com.localvalu.coremerchantapp.data.output.myappointments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.coremerchantapp.data.output.tablebooking.TableBookingListResult;

public class MyAppointmentListOutput
{
    @SerializedName("RequestslotResults")
    @Expose
    private MyAppointmentListResult myAppointmentListResult;

    public MyAppointmentListResult getMyAppointmentListResult()
    {
        return myAppointmentListResult;
    }

    public void setMyAppointmentListResult(MyAppointmentListResult myAppointmentListResult)
    {
        this.myAppointmentListResult = myAppointmentListResult;
    }
}
