package com.localvalu.coremerchantapp.data.output.myappointments;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.coremerchantapp.data.base.RestBasedOutput;

import java.util.ArrayList;
import java.util.List;

public class MyAppointmentListResult extends RestBasedOutput implements Parcelable
{
    @SerializedName("RequestslotResults")
    @Expose
    private List<MyAppointmentList> myAppointmentList;

    public List<MyAppointmentList> getMyAppointmentList()
    {
        return myAppointmentList;
    }

    public void setMyAppointmentList(List<MyAppointmentList> myAppointmentList)
    {
        this.myAppointmentList = myAppointmentList;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        super.writeToParcel(dest, flags);
        dest.writeList(this.myAppointmentList);
    }

    public MyAppointmentListResult()
    {
    }

    protected MyAppointmentListResult(Parcel in)
    {
        super(in);
        this.myAppointmentList = new ArrayList<MyAppointmentList>();
        in.readList(this.myAppointmentList, MyAppointmentList.class.getClassLoader());
    }

    public static final Creator<MyAppointmentListResult> CREATOR = new Creator<MyAppointmentListResult>()
    {
        @Override
        public MyAppointmentListResult createFromParcel(Parcel source)
        {
            return new MyAppointmentListResult(source);
        }

        @Override
        public MyAppointmentListResult[] newArray(int size)
        {
            return new MyAppointmentListResult[size];
        }
    };
}
