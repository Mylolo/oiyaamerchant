package com.localvalu.coremerchantapp.data.output.myappointments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.coremerchantapp.data.output.tablebooking.TableBookingStatusChangeResult;

public class MyAppointmentStatusChangeOutput
{
    @SerializedName(value = "slotBookingAcceptedResults", alternate = {"slotBookingDeclineResults"})
    @Expose
    private MyAppointmentStatusChangeResult myAppointmentStatusChangeResult;

    public MyAppointmentStatusChangeResult getMyAppointmentStatusChangeResult()
    {
        return myAppointmentStatusChangeResult;
    }

    public void setMyAppointmentStatusChangeResult(MyAppointmentStatusChangeResult myAppointmentStatusChangeResult)
    {
        this.myAppointmentStatusChangeResult = myAppointmentStatusChangeResult;
    }
}
