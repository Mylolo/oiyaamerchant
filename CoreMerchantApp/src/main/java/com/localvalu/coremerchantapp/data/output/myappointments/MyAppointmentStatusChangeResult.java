package com.localvalu.coremerchantapp.data.output.myappointments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.coremerchantapp.data.base.RestBasedOutput;
import com.localvalu.coremerchantapp.data.output.tablebooking.ChangeStatusResult;

public class MyAppointmentStatusChangeResult extends RestBasedOutput
{
    @SerializedName(value = "slotBookingAcceptedResults", alternate = {"slotBookingDeclineResults"})
    @Expose
    private MyAppointmentChangeStatusResult myAppointmentChangeStatusResult;

    public MyAppointmentChangeStatusResult getMyAppointmentChangeStatusResult()
    {
        return myAppointmentChangeStatusResult;
    }

    public void setMyAppointmentChangeStatusResult(MyAppointmentChangeStatusResult myAppointmentChangeStatusResult)
    {
        this.myAppointmentChangeStatusResult = myAppointmentChangeStatusResult;
    }
}
