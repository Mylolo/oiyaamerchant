package com.localvalu.coremerchantapp.data.output.order;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.coremerchantapp.data.output.FoodItem;

import java.util.ArrayList;
import java.util.List;

public class OrderDetails implements Parcelable
{

    @SerializedName("order_id")
    @Expose
    private String orderId;

    @SerializedName("business_id")
    @Expose
    private String businessId;

    @SerializedName("orderTotal")
    @Expose
    private String orderTotal;

    @SerializedName("is_order_placed")
    @Expose
    private String isOrderPlaced;

    @SerializedName("is_order_confirmed_by_rest")
    @Expose
    private String isOrderConfirmedByRest;

    @SerializedName("special_ins")
    @Expose
    private String specialInstructions;

    @SerializedName("allergy_ins")
    @Expose
    private String allergyInstructions;

    @SerializedName("createdDate")
    @Expose
    private String createdDate;

    @SerializedName("customer_name")
    @Expose
    private String customerName;

    @SerializedName("customer_mobile")
    @Expose
    private String customerMobile;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("Currentstatus")
    @Expose
    private String currentStatus;

    @SerializedName("orderType")
    @Expose
    private String orderType;

    @SerializedName("finalOrderTotal")
    @Expose
    private String finalOrderTotal;

    @SerializedName("OrderOption")
    @Expose
    private String orderOption;

    @SerializedName("tableid")
    @Expose
    private String tableId;

    @SerializedName(value = "StoreId",alternate={"storeid"})
    @Expose
    private String storeId;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("address1")
    @Expose
    private String addressOne;

    @SerializedName("City")
    @Expose
    private String city;

    @SerializedName("Country")
    @Expose
    private String country;

    @SerializedName("PostCode")
    @Expose
    private String PostCode;

    @SerializedName("user_id")
    @Expose
    private String userId;

    @SerializedName("deliveryRate")
    @Expose
    private String deliveryRate;

    @SerializedName("tax")
    @Expose
    private String tax;

    @SerializedName("datetime")
    @Expose
    private String dateTime;

    @SerializedName("account_id")
    @Expose
    private String accountId;

    @SerializedName("business_name")
    @Expose
    private String businessName;

    @SerializedName("business_logo")
    @Expose
    private String businessLogo;

    @SerializedName("business_address")
    @Expose
    private String businessAddress;

    @SerializedName("business_contact_phone")
    @Expose
    private String businessContactPhone;

    @SerializedName("business_contact_name")
    @Expose
    private String businessContactName;

    @SerializedName("Paymentstatus")
    @Expose
    private String paymentStatus;

    @SerializedName("DiscountLtAmount")
    @Expose
    private String DiscountLtAmount;

    @SerializedName("business_logo_path")
    @Expose
    private String businessLogoPath;

    @SerializedName("category_logo_upload_path")
    @Expose
    private String categoryLogoUploadedPath;

    @SerializedName("food_item_logo_upload_path")
    @Expose
    private String foodItemLogoUploadPath;

    @SerializedName("items")
    @Expose
    private List<FoodItem> foodItems;

    @SerializedName("isdriverselected")
    @Expose
    private String isDriverSelected;

    @SerializedName("driverId")
    @Expose
    private String driverId;

    public String getOrderId()
    {
        return orderId;
    }

    public void setOrderId(String orderId)
    {
        this.orderId = orderId;
    }

    public String getBusinessId()
    {
        return businessId;
    }

    public void setBusinessId(String businessId)
    {
        this.businessId = businessId;
    }

    public String getOrderTotal()
    {
        return orderTotal;
    }

    public void setOrderTotal(String orderTotal)
    {
        this.orderTotal = orderTotal;
    }

    public String getIsOrderPlaced()
    {
        return isOrderPlaced;
    }

    public void setIsOrderPlaced(String isOrderPlaced)
    {
        this.isOrderPlaced = isOrderPlaced;
    }

    public String getIsOrderConfirmedByRest()
    {
        return isOrderConfirmedByRest;
    }

    public void setIsOrderConfirmedByRest(String isOrderConfirmedByRest)
    {
        this.isOrderConfirmedByRest = isOrderConfirmedByRest;
    }

    public String getSpecialInstructions()
    {
        return specialInstructions;
    }

    public void setSpecialInstructions(String specialInstructions)
    {
        this.specialInstructions = specialInstructions;
    }

    public String getAllergyInstructions()
    {
        return allergyInstructions;
    }

    public void setAllergyInstructions(String allergyInstructions)
    {
        this.allergyInstructions = allergyInstructions;
    }

    public String getCreatedDate()
    {
        return createdDate;
    }

    public void setCreatedDate(String createdDate)
    {
        this.createdDate = createdDate;
    }

    public String getCustomerName()
    {
        return customerName;
    }

    public void setCustomerName(String customerName)
    {
        this.customerName = customerName;
    }

    public String getCustomerMobile()
    {
        return customerMobile;
    }

    public void setCustomerMobile(String customerMobile)
    {
        this.customerMobile = customerMobile;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getCurrentStatus()
    {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus)
    {
        this.currentStatus = currentStatus;
    }

    public String getAddressOne()
    {
        return addressOne;
    }

    public void setAddressOne(String addressOne)
    {
        this.addressOne = addressOne;
    }

    public String getCity()
    {
        return city;
    }

    public void setCity(String city)
    {
        this.city = city;
    }

    public String getCountry()
    {
        return country;
    }

    public void setCountry(String country)
    {
        this.country = country;
    }

    public String getPostCode()
    {
        return PostCode;
    }

    public void setPostCode(String postCode)
    {
        PostCode = postCode;
    }

    public String getUserId()
    {
        return userId;
    }

    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    public String getDeliveryRate()
    {
        return deliveryRate;
    }

    public void setDeliveryRate(String deliveryRate)
    {
        this.deliveryRate = deliveryRate;
    }

    public String getTax()
    {
        return tax;
    }

    public void setTax(String tax)
    {
        this.tax = tax;
    }

    public String getDateTime()
    {
        return dateTime;
    }

    public void setDateTime(String dateTime)
    {
        this.dateTime = dateTime;
    }

    public String getAccountId()
    {
        return accountId;
    }

    public void setAccountId(String accountId)
    {
        this.accountId = accountId;
    }

    public String getBusinessName()
    {
        return businessName;
    }

    public void setBusinessName(String businessName)
    {
        this.businessName = businessName;
    }

    public String getBusinessLogo()
    {
        return businessLogo;
    }

    public void setBusinessLogo(String businessLogo)
    {
        this.businessLogo = businessLogo;
    }

    public String getBusinessAddress()
    {
        return businessAddress;
    }

    public void setBusinessAddress(String businessAddress)
    {
        this.businessAddress = businessAddress;
    }

    public String getBusinessContactPhone()
    {
        return businessContactPhone;
    }

    public void setBusinessContactPhone(String businessContactPhone)
    {
        this.businessContactPhone = businessContactPhone;
    }

    public String getBusinessContactName()
    {
        return businessContactName;
    }

    public void setBusinessContactName(String businessContactName)
    {
        this.businessContactName = businessContactName;
    }

    public String getOrderType()
    {
        return orderType;
    }

    public void setOrderType(String orderType)
    {
        this.orderType = orderType;
    }

    public String getOrderOption()
    {
        return orderOption;
    }

    public void setOrderOption(String orderOption)
    {
        this.orderOption = orderOption;
    }

    public String getPaymentStatus()
    {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus)
    {
        this.paymentStatus = paymentStatus;
    }

    public String getFoodItemLogoUploadPath()
    {
        return foodItemLogoUploadPath;
    }

    public void setFoodItemLogoUploadPath(String foodItemLogoUploadPath)
    {
        this.foodItemLogoUploadPath = foodItemLogoUploadPath;
    }

    public String getDiscountLtAmount()
    {
        return DiscountLtAmount;
    }

    public void setDiscountLtAmount(String discountLtAmount)
    {
        DiscountLtAmount = discountLtAmount;
    }

    public String getFinalOrderTotal()
    {
        return finalOrderTotal;
    }

    public void setFinalOrderTotal(String finalOrderTotal)
    {
        this.finalOrderTotal = finalOrderTotal;
    }

    public String getBusinessLogoPath()
    {
        return businessLogoPath;
    }

    public void setBusinessLogoPath(String businessLogoPath)
    {
        this.businessLogoPath = businessLogoPath;
    }

    public String getCategoryLogoUploadedPath()
    {
        return categoryLogoUploadedPath;
    }

    public void setCategoryLogoUploadedPath(String categoryLogoUploadedPath)
    {
        this.categoryLogoUploadedPath = categoryLogoUploadedPath;
    }

    public String getFoodItemLogoUpploadPath()
    {
        return foodItemLogoUploadPath;
    }

    public void setFoodItemLogoUpploadPath(String foodItemLogoUpploadPath)
    {
        this.foodItemLogoUploadPath = foodItemLogoUpploadPath;
    }

    public List<FoodItem> getFoodItems()
    {
        return foodItems;
    }

    public void setFoodItems(List<FoodItem> foodItems)
    {
        this.foodItems = foodItems;
    }

    public String getTableId()
    {
        return tableId;
    }

    public void setTableId(String tableId)
    {
        this.tableId = tableId;
    }

    public String getStoreId()
    {
        return storeId;
    }

    public void setStoreId(String storeId)
    {
        this.storeId = storeId;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getIsDriverSelected()
    {
        return isDriverSelected;
    }

    public void setIsDriverSelected(String isDriverSelected)
    {
        this.isDriverSelected = isDriverSelected;
    }

    public String getDriverId()
    {
        return driverId;
    }

    public void setDriverId(String driverId)
    {
        this.driverId = driverId;
    }

    public OrderDetails()
    {
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(this.orderId);
        dest.writeString(this.businessId);
        dest.writeString(this.orderTotal);
        dest.writeString(this.isOrderPlaced);
        dest.writeString(this.isOrderConfirmedByRest);
        dest.writeString(this.specialInstructions);
        dest.writeString(this.allergyInstructions);
        dest.writeString(this.createdDate);
        dest.writeString(this.customerName);
        dest.writeString(this.customerMobile);
        dest.writeString(this.status);
        dest.writeString(this.currentStatus);
        dest.writeString(this.orderType);
        dest.writeString(this.finalOrderTotal);
        dest.writeString(this.orderOption);
        dest.writeString(this.tableId);
        dest.writeString(this.storeId);
        dest.writeString(this.description);
        dest.writeString(this.addressOne);
        dest.writeString(this.city);
        dest.writeString(this.country);
        dest.writeString(this.PostCode);
        dest.writeString(this.userId);
        dest.writeString(this.deliveryRate);
        dest.writeString(this.tax);
        dest.writeString(this.dateTime);
        dest.writeString(this.accountId);
        dest.writeString(this.businessName);
        dest.writeString(this.businessLogo);
        dest.writeString(this.businessAddress);
        dest.writeString(this.businessContactPhone);
        dest.writeString(this.businessContactName);
        dest.writeString(this.paymentStatus);
        dest.writeString(this.DiscountLtAmount);
        dest.writeString(this.businessLogoPath);
        dest.writeString(this.categoryLogoUploadedPath);
        dest.writeString(this.foodItemLogoUploadPath);
        dest.writeList(this.foodItems);
        dest.writeString(this.isDriverSelected);
        dest.writeString(this.driverId);
    }

    protected OrderDetails(Parcel in)
    {
        this.orderId = in.readString();
        this.businessId = in.readString();
        this.orderTotal = in.readString();
        this.isOrderPlaced = in.readString();
        this.isOrderConfirmedByRest = in.readString();
        this.specialInstructions = in.readString();
        this.allergyInstructions = in.readString();
        this.createdDate = in.readString();
        this.customerName = in.readString();
        this.customerMobile = in.readString();
        this.status = in.readString();
        this.currentStatus = in.readString();
        this.orderType = in.readString();
        this.finalOrderTotal = in.readString();
        this.orderOption = in.readString();
        this.tableId = in.readString();
        this.storeId = in.readString();
        this.description = in.readString();
        this.addressOne = in.readString();
        this.city = in.readString();
        this.country = in.readString();
        this.PostCode = in.readString();
        this.userId = in.readString();
        this.deliveryRate = in.readString();
        this.tax = in.readString();
        this.dateTime = in.readString();
        this.accountId = in.readString();
        this.businessName = in.readString();
        this.businessLogo = in.readString();
        this.businessAddress = in.readString();
        this.businessContactPhone = in.readString();
        this.businessContactName = in.readString();
        this.paymentStatus = in.readString();
        this.DiscountLtAmount = in.readString();
        this.businessLogoPath = in.readString();
        this.categoryLogoUploadedPath = in.readString();
        this.foodItemLogoUploadPath = in.readString();
        this.foodItems = new ArrayList<FoodItem>();
        in.readList(this.foodItems, FoodItem.class.getClassLoader());
        this.isDriverSelected = in.readString();
        this.driverId = in.readString();
    }

    public static final Creator<OrderDetails> CREATOR = new Creator<OrderDetails>()
    {
        @Override
        public OrderDetails createFromParcel(Parcel source)
        {
            return new OrderDetails(source);
        }

        @Override
        public OrderDetails[] newArray(int size)
        {
            return new OrderDetails[size];
        }
    };
}
