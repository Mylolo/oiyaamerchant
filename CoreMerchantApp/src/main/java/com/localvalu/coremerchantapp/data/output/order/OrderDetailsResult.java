package com.localvalu.coremerchantapp.data.output.order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.coremerchantapp.data.base.RestBasedOutput;

import java.util.List;

public class OrderDetailsResult extends RestBasedOutput
{
    @SerializedName("InduvidualOrder")
    @Expose
    private List<OrderDetails> orderDetails;

    public List<OrderDetails> getOrderDetails()
    {
        return orderDetails;
    }

    public void setOrderDetails(List<OrderDetails> orderDetails)
    {
        this.orderDetails = orderDetails;
    }
}
