package com.localvalu.coremerchantapp.data.output.order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderDetailsResultOutput
{
    @SerializedName("InduvidualOrderResult")
    @Expose
    private OrderDetailsResult orderDetailsResult;

    public OrderDetailsResult getOrderDetailsResult()
    {
        return orderDetailsResult;
    }

    public void setOrderDetailsResult(OrderDetailsResult orderDetailsResult)
    {
        this.orderDetailsResult = orderDetailsResult;
    }
}
