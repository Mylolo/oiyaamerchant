package com.localvalu.coremerchantapp.data.output.order;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderListData implements Parcelable
{

    @SerializedName("order_id")
    @Expose
    private String orderId;

    @SerializedName("business_id")
    @Expose
    private String businessId;

    @SerializedName("orderTotal")
    @Expose
    private String orderTotal;

    @SerializedName("is_order_placed")
    @Expose
    private String isOrderPlaced;

    @SerializedName("DiscountLtAmount")
    @Expose
    private String discountLtAmount;

    @SerializedName("orderType")
    @Expose
    private String orderType;

    @SerializedName("finalOrderTotal")
    @Expose
    private String finalOrderTotal;

    @SerializedName("OrderOption")
    @Expose
    private String orderOption;

    @SerializedName("is_order_confirmed_by_rest")
    @Expose
    private String isOrderConfirmedByRest;

    @SerializedName("special_ins")
    @Expose
    private String specialInstructions;

    @SerializedName("allergy_ins")
    @Expose
    private String allergyInstructions;

    @SerializedName("createdDate")
    @Expose
    private String createdDate;

    @SerializedName("customer_name")
    @Expose
    private String customerName;

    @SerializedName("customer_mobile")
    @Expose
    private String customerMobile;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("Currentstatus")
    @Expose
    private String currentStatus;

    public String getOrderId()
    {
        return orderId;
    }

    public void setOrderId(String orderId)
    {
        this.orderId = orderId;
    }

    public String getBusinessId()
    {
        return businessId;
    }

    public void setBusinessId(String businessId)
    {
        this.businessId = businessId;
    }

    public String getOrderTotal()
    {
        return orderTotal;
    }

    public void setOrderTotal(String orderTotal)
    {
        this.orderTotal = orderTotal;
    }

    public String getIsOrderPlaced()
    {
        return isOrderPlaced;
    }

    public void setIsOrderPlaced(String isOrderPlaced)
    {
        this.isOrderPlaced = isOrderPlaced;
    }

    public String getIsOrderConfirmedByRest()
    {
        return isOrderConfirmedByRest;
    }

    public void setIsOrderConfirmedByRest(String isOrderConfirmedByRest)
    {
        this.isOrderConfirmedByRest = isOrderConfirmedByRest;
    }

    public String getSpecialInstructions()
    {
        return specialInstructions;
    }

    public void setSpecialInstructions(String specialInstructions)
    {
        this.specialInstructions = specialInstructions;
    }

    public String getAllergyInstructions()
    {
        return allergyInstructions;
    }

    public void setAllergyInstructions(String allergyInstructions)
    {
        this.allergyInstructions = allergyInstructions;
    }

    public String getCreatedDate()
    {
        return createdDate;
    }

    public void setCreatedDate(String createdDate)
    {
        this.createdDate = createdDate;
    }

    public String getCustomerName()
    {
        return customerName;
    }

    public void setCustomerName(String customerName)
    {
        this.customerName = customerName;
    }

    public String getCustomerMobile()
    {
        return customerMobile;
    }

    public void setCustomerMobile(String customerMobile)
    {
        this.customerMobile = customerMobile;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getCurrentStatus()
    {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus)
    {
        this.currentStatus = currentStatus;
    }

    public String getDiscountLtAmount()
    {
        return discountLtAmount;
    }

    public void setDiscountLtAmount(String discountLtAmount)
    {
        this.discountLtAmount = discountLtAmount;
    }

    public String getOrderType()
    {
        return orderType;
    }

    public void setOrderType(String orderType)
    {
        this.orderType = orderType;
    }

    public String getFinalOrderTotal()
    {
        return finalOrderTotal;
    }

    public void setFinalOrderTotal(String finalOrderTotal)
    {
        this.finalOrderTotal = finalOrderTotal;
    }

    public String getOrderOption()
    {
        return orderOption;
    }

    public void setOrderOption(String orderOption)
    {
        this.orderOption = orderOption;
    }

    public OrderListData()
    {
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(this.orderId);
        dest.writeString(this.businessId);
        dest.writeString(this.orderTotal);
        dest.writeString(this.isOrderPlaced);
        dest.writeString(this.discountLtAmount);
        dest.writeString(this.orderType);
        dest.writeString(this.finalOrderTotal);
        dest.writeString(this.orderOption);
        dest.writeString(this.isOrderConfirmedByRest);
        dest.writeString(this.specialInstructions);
        dest.writeString(this.allergyInstructions);
        dest.writeString(this.createdDate);
        dest.writeString(this.customerName);
        dest.writeString(this.customerMobile);
        dest.writeString(this.status);
        dest.writeString(this.currentStatus);
    }

    protected OrderListData(Parcel in)
    {
        this.orderId = in.readString();
        this.businessId = in.readString();
        this.orderTotal = in.readString();
        this.isOrderPlaced = in.readString();
        this.discountLtAmount = in.readString();
        this.orderType = in.readString();
        this.finalOrderTotal = in.readString();
        this.orderOption = in.readString();
        this.isOrderConfirmedByRest = in.readString();
        this.specialInstructions = in.readString();
        this.allergyInstructions = in.readString();
        this.createdDate = in.readString();
        this.customerName = in.readString();
        this.customerMobile = in.readString();
        this.status = in.readString();
        this.currentStatus = in.readString();
    }

    public static final Creator<OrderListData> CREATOR = new Creator<OrderListData>()
    {
        @Override
        public OrderListData createFromParcel(Parcel source)
        {
            return new OrderListData(source);
        }

        @Override
        public OrderListData[] newArray(int size)
        {
            return new OrderListData[size];
        }
    };
}
