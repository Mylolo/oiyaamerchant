package com.localvalu.coremerchantapp.data.output.order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderListOutput
{
    @SerializedName("OrderListForADayResult")
    @Expose
    private OrderListResult orderListResult;

    public OrderListResult getOrderListResult()
    {
        return orderListResult;
    }

    public void setOrderListResult(OrderListResult orderListResult)
    {
        this.orderListResult = orderListResult;
    }
}
