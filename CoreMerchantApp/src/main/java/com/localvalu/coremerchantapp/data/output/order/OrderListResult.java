package com.localvalu.coremerchantapp.data.output.order;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.coremerchantapp.data.base.RestBasedOutput;

import java.util.ArrayList;
import java.util.List;

public class OrderListResult extends RestBasedOutput implements Parcelable
{
    @SerializedName("OrderListForADay")
    @Expose
    private List<OrderListData> orderList;

    public List<OrderListData> getOrderList()
    {
        return orderList;
    }

    public void setOrderList(ArrayList<OrderListData> orderList)
    {
        this.orderList = orderList;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeTypedList(this.orderList);

    }

    public OrderListResult()
    {
    }

    protected OrderListResult(Parcel in)
    {
        this.orderList = in.createTypedArrayList(OrderListData.CREATOR);

    }

    public static final Creator<OrderListResult> CREATOR = new Creator<OrderListResult>()
    {
        @Override
        public OrderListResult createFromParcel(Parcel source)
        {
            return new OrderListResult(source);
        }

        @Override
        public OrderListResult[] newArray(int size)
        {
            return new OrderListResult[size];
        }
    };
}
