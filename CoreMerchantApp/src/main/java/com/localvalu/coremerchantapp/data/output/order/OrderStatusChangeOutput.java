package com.localvalu.coremerchantapp.data.output.order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderStatusChangeOutput
{
    @SerializedName("OrderStatusResult")
    @Expose
    private OrderStatusChangeResult orderStatusChangeResult;

    public OrderStatusChangeResult getOrderStatusChangeResult()
    {
        return orderStatusChangeResult;
    }

    public void setOrderStatusChangeResult(OrderStatusChangeResult orderStatusChangeResult)
    {
        this.orderStatusChangeResult = orderStatusChangeResult;
    }
}
