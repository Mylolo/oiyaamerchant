
package com.localvalu.coremerchantapp.data.output.pooldrivers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DriverAppResult
{
    @SerializedName("GetpooldriverlistResult")
    @Expose
    private GetpooldriverlistResult getpooldriverlistResult;

    public GetpooldriverlistResult getGetpooldriverlistResult()
    {
        return getpooldriverlistResult;
    }

    public void setGetpooldriverlistResult(GetpooldriverlistResult getpooldriverlistResult)
    {
        this.getpooldriverlistResult = getpooldriverlistResult;
    }

    public DriverAppResult withGetpooldriverlistResult(GetpooldriverlistResult getpooldriverlistResult)
    {
        this.getpooldriverlistResult = getpooldriverlistResult;
        return this;
    }

}
