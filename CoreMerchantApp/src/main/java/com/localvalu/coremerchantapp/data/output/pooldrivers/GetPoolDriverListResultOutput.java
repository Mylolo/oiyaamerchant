package com.localvalu.coremerchantapp.data.output.pooldrivers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.coremerchantapp.data.output.order.OrderDetailsResult;

public class GetPoolDriverListResultOutput
{
    @SerializedName("DriverAppResult")
    @Expose
    private DriverAppResult driverAppResult;

    public DriverAppResult getDriverAppResult() {
        return driverAppResult;
    }

    public void setDriverAppResult(DriverAppResult driverAppResult) {
        this.driverAppResult = driverAppResult;
    }
}
