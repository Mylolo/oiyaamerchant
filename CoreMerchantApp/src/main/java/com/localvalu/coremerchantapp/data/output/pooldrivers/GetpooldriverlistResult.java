
package com.localvalu.coremerchantapp.data.output.pooldrivers;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.coremerchantapp.data.base.RestBasedOutput;

public class GetpooldriverlistResult extends RestBasedOutput
{
    @SerializedName("poolDriverlist")
    @Expose
    private List<PoolDriver> poolDriverlist = null;

    public List<PoolDriver> getPoolDriverlist()
    {
        return poolDriverlist;
    }

    public void setPoolDriverlist(List<PoolDriver> poolDriverlist)
    {
        this.poolDriverlist = poolDriverlist;
    }

    public GetpooldriverlistResult withPoolDriverlist(List<PoolDriver> poolDriverlist)
    {
        this.poolDriverlist = poolDriverlist;
        return this;
    }

}
