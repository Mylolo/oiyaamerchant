
package com.localvalu.coremerchantapp.data.output.pooldrivers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PoolDriver
{
    @SerializedName("bankAccountdetails")
    @Expose
    private String bankAccountdetails;
    @SerializedName("deliveryfee")
    @Expose
    private String deliveryfee;
    @SerializedName("driverId")
    @Expose
    private String driverId;
    @SerializedName("driverName")
    @Expose
    private String driverName;
    @SerializedName("poolName")
    @Expose
    private String poolName;
    @SerializedName("postCode")
    @Expose
    private String postCode;

    public String getBankAccountdetails() {
        return bankAccountdetails;
    }

    public void setBankAccountdetails(String bankAccountdetails) {
        this.bankAccountdetails = bankAccountdetails;
    }

    public PoolDriver withBankAccountdetails(String bankAccountdetails) {
        this.bankAccountdetails = bankAccountdetails;
        return this;
    }

    public String getDeliveryfee() {
        return deliveryfee;
    }

    public void setDeliveryfee(String deliveryfee) {
        this.deliveryfee = deliveryfee;
    }

    public PoolDriver withDeliveryfee(String deliveryfee) {
        this.deliveryfee = deliveryfee;
        return this;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public PoolDriver withDriverId(String driverId) {
        this.driverId = driverId;
        return this;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public PoolDriver withDriverName(String driverName) {
        this.driverName = driverName;
        return this;
    }

    public String getPoolName() {
        return poolName;
    }

    public void setPoolName(String poolName) {
        this.poolName = poolName;
    }

    public PoolDriver withPoolName(String poolName) {
        this.poolName = poolName;
        return this;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public PoolDriver withPostCode(String postCode) {
        this.postCode = postCode;
        return this;
    }

}
