package com.localvalu.coremerchantapp.data.output.selfserve;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.coremerchantapp.data.base.RestBasedOutput;
import com.localvalu.coremerchantapp.data.output.SaleTransactionResult;

public class MerchantSelfServeOutput extends RestBasedOutput
{
    @SerializedName("MerchantSelfServe")
    @Expose
    private MerchantSelfServeResult result;

    public MerchantSelfServeResult getResult()
    {
        return result;
    }

    public void setResult(MerchantSelfServeResult result)
    {
        this.result = result;
    }
}
