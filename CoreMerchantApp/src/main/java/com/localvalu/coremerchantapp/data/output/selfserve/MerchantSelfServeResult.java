package com.localvalu.coremerchantapp.data.output.selfserve;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.coremerchantapp.data.base.RestBasedOutput;

public class MerchantSelfServeResult extends RestBasedOutput
{
    @SerializedName("insertMerchantselfserveResult")
    @Expose
    private InsertMerchantSelfServeResult insertMerchantSelfServeResult;

    public InsertMerchantSelfServeResult getInsertMerchantSelfServeResult()
    {
        return insertMerchantSelfServeResult;
    }

    public void setInsertMerchantSelfServeResult(InsertMerchantSelfServeResult insertMerchantSelfServeResult)
    {
        this.insertMerchantSelfServeResult = insertMerchantSelfServeResult;
    }
}
