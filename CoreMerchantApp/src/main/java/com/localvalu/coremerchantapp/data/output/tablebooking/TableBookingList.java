package com.localvalu.coremerchantapp.data.output.tablebooking;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TableBookingList implements Parcelable
{
    @SerializedName("TableBookid")
    @Expose
    private String tableBookId;

    @SerializedName("UserId")
    @Expose
    private String userId;

    @SerializedName("BusinessId")
    @Expose
    private String businessId;

    @SerializedName("PreferredDate")
    @Expose
    private String preferredDate;

    @SerializedName("PreferredTime")
    @Expose
    private String preferredTime;

    @SerializedName("NoOfGuest")
    @Expose
    private String NnOfGuests;

    @SerializedName("SpecialRequest")
    @Expose
    private String specialRequest;

    @SerializedName("IsTableBooking")
    @Expose
    private String isTableBooking;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("userName")
    @Expose
    private String userName;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("Mobile")
    @Expose
    private String mobile;

    @SerializedName("Placedatetime")
    @Expose
    private String placeDateTime;

    @SerializedName("is_order_placed")
    @Expose
    private String isOrderPlaced;

    @SerializedName("is_order_confirmed_by_rest")
    @Expose
    private String isOrderConfirmedByRest;

    @SerializedName("special_ins")
    @Expose
    private String specialInstructions;

    @SerializedName("allergy_ins")
    @Expose
    private String allergyInstructions;

    @SerializedName("createdDate")
    @Expose
    private String createdDate;

    @SerializedName("isActive")
    @Expose
    private String isActive;

    @SerializedName("orderType")
    @Expose
    private String orderType;

    @SerializedName("tradingname")
    @Expose
    private String tradingName;

    @SerializedName("RetailerType")
    @Expose
    private String retailerType;

    private boolean updating=false;

    public String getTableBookId()
    {
        return tableBookId;
    }

    public void setTableBookId(String tableBookId)
    {
        this.tableBookId = tableBookId;
    }

    public String getUserId()
    {
        return userId;
    }

    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    public String getBusinessId()
    {
        return businessId;
    }

    public void setBusinessId(String businessId)
    {
        this.businessId = businessId;
    }

    public String getPreferredDate()
    {
        return preferredDate;
    }

    public void setPreferredDate(String preferredDate)
    {
        this.preferredDate = preferredDate;
    }

    public String getPreferredTime()
    {
        return preferredTime;
    }

    public void setPreferredTime(String preferredTime)
    {
        this.preferredTime = preferredTime;
    }

    public String getNnOfGuests()
    {
        return NnOfGuests;
    }

    public void setNnOfGuests(String nnOfGuests)
    {
        NnOfGuests = nnOfGuests;
    }

    public String getSpecialRequest()
    {
        return specialRequest;
    }

    public void setSpecialRequest(String specialRequest)
    {
        this.specialRequest = specialRequest;
    }

    public String getIsTableBooking()
    {
        return isTableBooking;
    }

    public void setIsTableBooking(String isTableBooking)
    {
        this.isTableBooking = isTableBooking;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getMobile()
    {
        return mobile;
    }

    public void setMobile(String mobile)
    {
        this.mobile = mobile;
    }

    public String getPlaceDateTime()
    {
        return placeDateTime;
    }

    public void setPlaceDateTime(String placeDateTime)
    {
        this.placeDateTime = placeDateTime;
    }

    public String getIsOrderPlaced()
    {
        return isOrderPlaced;
    }

    public void setIsOrderPlaced(String isOrderPlaced)
    {
        this.isOrderPlaced = isOrderPlaced;
    }

    public String getIsOrderConfirmedByRest()
    {
        return isOrderConfirmedByRest;
    }

    public void setIsOrderConfirmedByRest(String isOrderConfirmedByRest)
    {
        this.isOrderConfirmedByRest = isOrderConfirmedByRest;
    }

    public String getSpecialInstructions()
    {
        return specialInstructions;
    }

    public void setSpecialInstructions(String specialInstructions)
    {
        this.specialInstructions = specialInstructions;
    }

    public String getAllergyInstructions()
    {
        return allergyInstructions;
    }

    public void setAllergyInstructions(String allergyInstructions)
    {
        this.allergyInstructions = allergyInstructions;
    }

    public String getCreatedDate()
    {
        return createdDate;
    }

    public void setCreatedDate(String createdDate)
    {
        this.createdDate = createdDate;
    }

    public String getIsActive()
    {
        return isActive;
    }

    public void setIsActive(String isActive)
    {
        this.isActive = isActive;
    }

    public String getOrderType()
    {
        return orderType;
    }

    public void setOrderType(String orderType)
    {
        this.orderType = orderType;
    }

    public String getTradingName()
    {
        return tradingName;
    }

    public void setTradingName(String tradingName)
    {
        this.tradingName = tradingName;
    }

    public String getRetailerType()
    {
        return retailerType;
    }

    public void setRetailerType(String retailerType)
    {
        this.retailerType = retailerType;
    }

    public boolean isUpdating()
    {
        return updating;
    }

    public void setUpdating(boolean updating)
    {
        this.updating = updating;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(this.tableBookId);
        dest.writeString(this.userId);
        dest.writeString(this.businessId);
        dest.writeString(this.preferredDate);
        dest.writeString(this.preferredTime);
        dest.writeString(this.NnOfGuests);
        dest.writeString(this.specialRequest);
        dest.writeString(this.isTableBooking);
        dest.writeString(this.status);
        dest.writeString(this.userName);
        dest.writeString(this.email);
        dest.writeString(this.mobile);
        dest.writeString(this.placeDateTime);
        dest.writeString(this.isOrderPlaced);
        dest.writeString(this.isOrderConfirmedByRest);
        dest.writeString(this.specialInstructions);
        dest.writeString(this.allergyInstructions);
        dest.writeString(this.createdDate);
        dest.writeString(this.isActive);
        dest.writeString(this.orderType);
        dest.writeString(this.tradingName);
        dest.writeString(this.retailerType);
    }

    public TableBookingList()
    {
    }

    protected TableBookingList(Parcel in)
    {
        this.tableBookId = in.readString();
        this.userId = in.readString();
        this.businessId = in.readString();
        this.preferredDate = in.readString();
        this.preferredTime = in.readString();
        this.NnOfGuests = in.readString();
        this.specialRequest = in.readString();
        this.isTableBooking = in.readString();
        this.status = in.readString();
        this.userName = in.readString();
        this.email = in.readString();
        this.mobile = in.readString();
        this.placeDateTime = in.readString();
        this.isOrderPlaced = in.readString();
        this.isOrderConfirmedByRest = in.readString();
        this.specialInstructions = in.readString();
        this.allergyInstructions = in.readString();
        this.createdDate = in.readString();
        this.isActive = in.readString();
        this.orderType = in.readString();
        this.tradingName = in.readString();
        this.retailerType = in.readString();
    }

    public static final Creator<TableBookingList> CREATOR = new Creator<TableBookingList>()
    {
        @Override
        public TableBookingList createFromParcel(Parcel source)
        {
            return new TableBookingList(source);
        }

        @Override
        public TableBookingList[] newArray(int size)
        {
            return new TableBookingList[size];
        }
    };
}
