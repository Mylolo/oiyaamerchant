package com.localvalu.coremerchantapp.data.output.tablebooking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TableBookingListOutput
{
    @SerializedName("TableBookingListResult")
    @Expose
    private TableBookingListResult tableBookingListResult;

    public TableBookingListResult getTableBookingListResult()
    {
        return tableBookingListResult;
    }

    public void setTableBookingListResult(TableBookingListResult tableBookingListResult)
    {
        this.tableBookingListResult = tableBookingListResult;
    }
}
