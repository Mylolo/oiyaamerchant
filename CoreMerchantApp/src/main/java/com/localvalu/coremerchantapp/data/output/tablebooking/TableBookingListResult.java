package com.localvalu.coremerchantapp.data.output.tablebooking;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.coremerchantapp.data.base.RestBasedOutput;

import java.util.ArrayList;
import java.util.List;

public class TableBookingListResult extends RestBasedOutput implements Parcelable
{
    @SerializedName("TableBookingList")
    @Expose
    private List<TableBookingList> tableBookingList;

    public List<TableBookingList> getTableBookingList()
    {
        return tableBookingList;
    }

    public void setTableBookingList(List<TableBookingList> tableBookingList)
    {
        this.tableBookingList = tableBookingList;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        super.writeToParcel(dest, flags);
        dest.writeList(this.tableBookingList);
    }

    public TableBookingListResult()
    {
    }

    protected TableBookingListResult(Parcel in)
    {
        super(in);
        this.tableBookingList = new ArrayList<TableBookingList>();
        in.readList(this.tableBookingList, TableBookingList.class.getClassLoader());
    }

    public static final Creator<TableBookingListResult> CREATOR = new Creator<TableBookingListResult>()
    {
        @Override
        public TableBookingListResult createFromParcel(Parcel source)
        {
            return new TableBookingListResult(source);
        }

        @Override
        public TableBookingListResult[] newArray(int size)
        {
            return new TableBookingListResult[size];
        }
    };
}
