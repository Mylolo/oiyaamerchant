package com.localvalu.coremerchantapp.data.output.tablebooking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TableBookingStatusChangeOutput
{
    @SerializedName("TableBookstatusResult")
    @Expose
    private TableBookingStatusChangeResult tableBookingStatusChangeResult;

    public TableBookingStatusChangeResult getTableBookingStatusChangeResult()
    {
        return tableBookingStatusChangeResult;
    }

    public void setTableBookingStatusChangeResult(TableBookingStatusChangeResult tableBookingStatusChangeResult)
    {
        this.tableBookingStatusChangeResult = tableBookingStatusChangeResult;
    }
}
