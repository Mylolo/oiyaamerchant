package com.localvalu.coremerchantapp.data.output.tablebooking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.coremerchantapp.data.base.RestBasedOutput;

public class TableBookingStatusChangeResult extends RestBasedOutput
{
    @SerializedName("ChangeCancelStatustableDetailsResult")
    @Expose
    private ChangeStatusResult changeStatusResult;

    public ChangeStatusResult getChangeStatusResult()
    {
        return changeStatusResult;
    }

    public void setChangeStatusResult(ChangeStatusResult changeStatusResult)
    {
        this.changeStatusResult = changeStatusResult;
    }


}
