package com.localvalu.coremerchantapp.data.output.trace;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TraceHistoryReport
{
    @SerializedName("AgeGroup")
    @Expose
    private String ageGroup;

    @SerializedName("Email")
    @Expose
    private String email;

    @SerializedName("Gender")
    @Expose
    private String gender;

    @SerializedName("Home_PostCode")
    @Expose
    private String homePostCode;

    @SerializedName("MobileNo")
    @Expose
    private String mobileNumber;

    @SerializedName("Name")
    @Expose
    private String name;

    @SerializedName("VisitTime")
    @Expose
    private String visitTime;

    @SerializedName("Work_PostCode")
    @Expose
    private String work_PostCode;

    public String getAgeGroup()
    {
        return ageGroup;
    }

    public void setAgeGroup(String ageGroup)
    {
        this.ageGroup = ageGroup;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getGender()
    {
        return gender;
    }

    public void setGender(String gender)
    {
        this.gender = gender;
    }

    public String getHomePostCode()
    {
        return homePostCode;
    }

    public void setHomePostCode(String homePostCode)
    {
        this.homePostCode = homePostCode;
    }

    public String getMobileNumber()
    {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber)
    {
        this.mobileNumber = mobileNumber;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getVisitTime()
    {
        return visitTime;
    }

    public void setVisitTime(String visitTime)
    {
        this.visitTime = visitTime;
    }

    public String getWork_PostCode()
    {
        return work_PostCode;
    }

    public void setWork_PostCode(String work_PostCode)
    {
        this.work_PostCode = work_PostCode;
    }

}
