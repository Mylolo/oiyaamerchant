package com.localvalu.coremerchantapp.data.output.trace;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.coremerchantapp.data.base.RestBasedOutput;

import java.util.List;

public class TraceHistoryReportResult extends RestBasedOutput
{
    @SerializedName("TraceHistoryReport")
    @Expose
    private List<TraceHistoryReport> traceHistoryReportList;

    public List<TraceHistoryReport> getTraceHistoryReportList()
    {
        return traceHistoryReportList;
    }

    public void setTraceHistoryReportList(List<TraceHistoryReport> traceHistoryReportList)
    {
        this.traceHistoryReportList = traceHistoryReportList;
    }
}
