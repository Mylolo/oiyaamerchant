package com.localvalu.coremerchantapp.data.output.trace;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TraceHistoryReportResultOutput
{
    @SerializedName("TraceHistoryReportResult")
    @Expose
    private TraceHistoryReportResult traceHistoryReportResult;

    public TraceHistoryReportResult getTraceHistoryReportResult()
    {
        return traceHistoryReportResult;
    }

    public void setTraceHistoryReportResult(TraceHistoryReportResult traceHistoryReportResult)
    {
        this.traceHistoryReportResult = traceHistoryReportResult;
    }
}
