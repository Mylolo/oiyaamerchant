package com.localvalu.coremerchantapp.data.output.trace;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VisitEntryResultOutput
{
    @SerializedName("TraceHistory_InsertResult")
    @Expose
    private VisitEntryResult visitEntryResult;

    public VisitEntryResult getVisitEntryResult()
    {
        return visitEntryResult;
    }

    public void setVisitEntryResult(VisitEntryResult visitEntryResult)
    {
        this.visitEntryResult = visitEntryResult;
    }
}
