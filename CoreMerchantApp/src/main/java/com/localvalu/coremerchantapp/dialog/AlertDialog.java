package com.localvalu.coremerchantapp.dialog;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

import androidx.appcompat.widget.AppCompatTextView;

import com.google.android.material.button.MaterialButton;
import com.localvalu.coremerchantapp.R;
import com.utils.log.Logger;
import com.utils.util.Util;
import com.utils.validator.Validator;

public class AlertDialog extends DialogFragment
{

    private static final String TAG = AlertDialog.class.getSimpleName();

    private static Context context;
    private String title;
    private String message;
    private String positiveButtonText;
    private String negativeButtonText;
    private AlertDialogClickListener alertDialogClickListener;

    private boolean isDismissible = true;

    public static AlertDialog newInstance(Context context, String title, String message, String positiveButtonText, String negativeButtonText, AlertDialogClickListener alertDialogClickListener)
    {
        AlertDialog.context = context;

        try
        {
            Bundle bundle = new Bundle();
            bundle.putString("title", title);
            bundle.putString("message", message);
            bundle.putString("positiveButtonText", positiveButtonText);
            bundle.putString("negativeButtonText", negativeButtonText);

            AlertDialog alertDialog = new AlertDialog();
            alertDialog.setArguments(bundle);

            alertDialog.alertDialogClickListener = alertDialogClickListener;
            return alertDialog;
        }
        catch (IllegalStateException e)
        {
            Logger.logWarn(TAG, e.getLocalizedMessage());
            return null;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        title = getArguments().getString("title");
        message = getArguments().getString("message");
        positiveButtonText = getArguments().getString("positiveButtonText");
        negativeButtonText = getArguments().getString("negativeButtonText");
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        View view = getActivity().getLayoutInflater().inflate(R.layout.alert_dialog, null);

        AppCompatTextView textViewTitle = view.findViewById(R.id.alert_textView_title);
        textViewTitle.setText(title);
        if (Validator.isValid(title))
            textViewTitle.setVisibility(View.VISIBLE);
        else
            textViewTitle.setVisibility(View.GONE);

        AppCompatTextView textViewMessage = view.findViewById(R.id.alert_textView_message);
        textViewMessage.setText(message);

        MaterialButton buttonPositive = view.findViewById(R.id.alert_button_positive);
        buttonPositive.setText(positiveButtonText);
        if (Validator.isValid(positiveButtonText))
            buttonPositive.setVisibility(View.VISIBLE);
        else
            buttonPositive.setVisibility(View.GONE);

        buttonPositive.setOnClickListener(v ->
        {
            if (Validator.isValid(alertDialogClickListener))
                alertDialogClickListener.alertDialogPositiveButtonClicked();

            dismissDialog();
        });

        MaterialButton buttonNegative = view.findViewById(R.id.alert_button_negative);
        buttonNegative.setText(negativeButtonText);
        if (Validator.isValid(negativeButtonText))
            buttonNegative.setVisibility(View.VISIBLE);
        else
            buttonNegative.setVisibility(View.GONE);

        buttonNegative.setOnClickListener(v ->
        {
            if (Validator.isValid(alertDialogClickListener))
                alertDialogClickListener.alertDialogNegativeButtonClicked();

            dismissDialog();
        });

        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(getActivity());

        androidx.appcompat.app.AlertDialog dialog = builder.setView(view).create();
        dialog.getWindow().getDecorView().getBackground().setColorFilter(Util.getColor(getActivity(), R.color.lightWindowBackground), PorterDuff.Mode.MULTIPLY);

        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(dialog.getWindow().getAttributes());
        layoutParams.width = WindowManager.LayoutParams.WRAP_CONTENT;
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;

        dialog.show();
        dialog.getWindow().setAttributes(layoutParams);

        return dialog;
    }

    @Override
    public void onCancel(DialogInterface dialog)
    {
        super.onCancel(dialog);

        if (Validator.isValid(alertDialogClickListener))
            alertDialogClickListener.alertDialogOutsideClicked();

        dismissDialog();
    }

    public void isDismissible(boolean isDismissible)
    {
        this.isDismissible = isDismissible;
    }

    public void dismissDialog()
    {
        if (isDismissible)
            dismiss();
    }

    public interface AlertDialogClickListener
    {
        void alertDialogPositiveButtonClicked();

        void alertDialogNegativeButtonClicked();

        void alertDialogOutsideClicked();
    }
}
