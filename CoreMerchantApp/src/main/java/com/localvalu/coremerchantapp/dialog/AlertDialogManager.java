package com.localvalu.coremerchantapp.dialog;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import androidx.annotation.NonNull;

import com.utils.log.Logger;
import com.utils.validator.Validator;

public class AlertDialogManager {

    public static final int ALERT_ERROR = 1;
    private static final String TAG = AlertDialogManager.class.getSimpleName();

    public static AlertDialog show(@NonNull Context context, @NonNull int dialogType, String messageData, AlertDialog.AlertDialogClickListener alertDialogClickListener) {
        dismissDialogIfShown(context);

        int icon;
        String title;
        String message;
        String positiveButtonText;
        String negativeButtonText;
        AlertDialog alertDialog;

        switch (dialogType) {
            case ALERT_ERROR:
                title = "Error";
                message = messageData;
                positiveButtonText = "Ok";
                negativeButtonText = "Cancel";

                alertDialog = builtDialog(context, title, message, positiveButtonText, negativeButtonText, alertDialogClickListener);
                alertDialog.isDismissible(true);
                alertDialog.setCancelable(true);
                return alertDialog;

            default:
                return null;
        }
    }

    public static AlertDialog customDialog(Context context, String title, String message, String positiveButtonText, String negativeButtonText, AlertDialog.AlertDialogClickListener alertDialogClickListener) {
        dismissDialogIfShown(context);
        return builtDialog(context, title, message, positiveButtonText, negativeButtonText, alertDialogClickListener);
    }

    private static void dismissDialogIfShown(Context context) {
        try {
            FragmentManager fragmentManager = ((Activity) context).getFragmentManager();
            Fragment fragmentPreview = fragmentManager.findFragmentByTag("AlertDialog");
            if (Validator.isValid(fragmentPreview)) {
                try {
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.remove(fragmentPreview);
                    fragmentTransaction.commit();
                } catch (Exception e) {
                    Logger.logWarn(TAG, e.getLocalizedMessage());
                }
            }

            fragmentManager.executePendingTransactions();
        } catch (Exception e) {

        }
    }

    private static AlertDialog builtDialog(Context context, String title, String message, String positiveButtonText, String negativeButtonText, AlertDialog.AlertDialogClickListener alertDialogClickListener) {
        AlertDialog alertDialog = AlertDialog.newInstance(context, title, message, positiveButtonText, negativeButtonText, alertDialogClickListener);
        try {
            FragmentTransaction fragmentTransaction = ((Activity) context).getFragmentManager().beginTransaction();
            fragmentTransaction.add(alertDialog, "AlertDialog");
            fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commitAllowingStateLoss();
        } catch (Exception e) {
            Logger.logWarn(TAG, e.getLocalizedMessage());
        }

        return alertDialog;
    }
}
