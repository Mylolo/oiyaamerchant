package com.localvalu.coremerchantapp.dialog

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.localvalu.coremerchantapp.R
import com.localvalu.coremerchantapp.base.Resource
import com.localvalu.coremerchantapp.data.input.SaleTransactionInput
import com.localvalu.coremerchantapp.databinding.FragmentSendAmountBinding
import com.localvalu.coremerchantapp.extensions.closeKeyboard
import com.localvalu.coremerchantapp.extensions.handleAPIError
import com.localvalu.coremerchantapp.extensions.showMessage
import com.localvalu.coremerchantapp.handler.DashBoardHandler
import com.localvalu.coremerchantapp.scan.viewmodel.MerchantSaleTransactionViewModel
import com.localvalu.coremerchantapp.utils.AppUtils.BUNDLE_CUSTOMER_QR_CODE
import com.localvalu.coremerchantapp.viewmodel.DashboardViewModel
import com.utils.base.AppBaseFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_send_amount.*

@AndroidEntryPoint
class SendAmountFragment : AppBaseFragment()
{
    private var binding: FragmentSendAmountBinding? = null
    private var retailerId: String? = null
    private var customerQRCode: String? = null
    private val dashboardViewModel: DashboardViewModel by viewModels()
    private val viewModel: MerchantSaleTransactionViewModel by viewModels()
    private var dashboardHandler: DashBoardHandler? = null
    private var sendAmount: Double = 0.00

    override fun onAttach(context: Context)
    {
        super.onAttach(context)
        dashboardHandler = context as DashBoardHandler
    }

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        customerQRCode = arguments?.getString(BUNDLE_CUSTOMER_QR_CODE, "")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?
    {
        binding = FragmentSendAmountBinding.inflate(inflater, container, false)
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        setProperties()
    }

    private fun setProperties()
    {
        hideProgress()
        clearAmountText()
        binding!!.etSendAmount.doAfterTextChanged {
            viewModel?.setMerchantSaleTransactionAmount(binding!!.etSendAmount.text.toString())
        }
        binding!!.btnSend.setOnClickListener {
            if (binding!!.btnSend.text.toString().isNotEmpty())
            {
                sendAmount()
            }
        }
        binding!!.btnCancel.setOnClickListener {
            requireActivity().supportFragmentManager.popBackStack()
        }
        binding!!.etSendAmount.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE)
            {
                if(binding!!.btnSend.isEnabled)
                {
                    sendAmount()
                }
                true
            }
            else
            {
                false
            }
        }
        observeSubscribers()
        dashboardViewModel?.getUser()
    }

    private fun observeSubscribers()
    {
        dashboardViewModel?.user.observe(viewLifecycleOwner, Observer { signInResult ->
            this.retailerId = signInResult?.retailerId.toString()
        })
        viewModel?.merchantSaleTransactionResponse.observe(
            viewLifecycleOwner,
            Observer { response ->
                when (response)
                {
                    is Resource.Loading ->
                    {
                        showProgress()
                    }
                    is Resource.Success ->
                    {
                        hideProgress()
                        if (response.value.result.errorDetail.errorCode == 0)
                        {
                            clearAmountText()
                            dashboardHandler?.scanSuccess(sendAmount, response.value.result)
                        }
                        else
                        {
                            showMessage(response.value.result.errorDetail.errorMessage)
                        }
                    }
                    is Resource.Failure ->
                    {
                        hideProgress()
                        handleAPIError(response) {
                            sendAmount()
                        }
                    }
                }
            })
        viewModel?.enableSendNow.observe(viewLifecycleOwner, Observer {
            binding!!.btnSend.isEnabled = it
        })
    }

    private fun sendAmount()
    {
        closeKeyboard(requireContext(), binding!!.etSendAmount)
        retailerId?.let {
            val saleTransactionInput: SaleTransactionInput = SaleTransactionInput()
            saleTransactionInput.accountId = (40527018)
            saleTransactionInput.retailerID = retailerId
            saleTransactionInput.consumerQRCode = customerQRCode
            sendAmount = (binding!!.etSendAmount.text.toString()).toDouble()
            saleTransactionInput.saleTotal = sendAmount;
            viewModel?.merchantSaleTransaction(saleTransactionInput)
        }
    }

    private fun showProgress()
    {
        binding!!.transferTokenIndicator.visibility = View.VISIBLE
        binding!!.btnSend.setText("")
    }

    private fun hideProgress()
    {
        binding!!.transferTokenIndicator.visibility = View.GONE
        binding!!.btnSend.setText(getString(R.string.lbl_send))
    }

    private fun clearAmountText()
    {
        binding!!.etSendAmount.setText("")
        binding!!.btnSend.isEnabled = false
    }

    override fun onDestroy()
    {
        clearAmountText()
        super.onDestroy()
    }

}