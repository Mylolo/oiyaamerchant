package com.localvalu.coremerchantapp.drivers.api

import com.localvalu.coremerchantapp.BaseApi
import com.localvalu.coremerchantapp.data.input.DriversProfileInput
import com.localvalu.coremerchantapp.data.output.driverOld.DriverProfileOutput
import retrofit2.http.Body
import retrofit2.http.POST

interface DriverProfileApi : BaseApi
{
    @POST("DriverProfile")
    suspend fun driverProfileInfo(@Body driversProfileInput: DriversProfileInput): DriverProfileOutput
}