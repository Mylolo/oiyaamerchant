package com.localvalu.coremerchantapp.drivers.di

import android.content.Context
import com.localvalu.coremerchantapp.drivers.api.DriverProfileApi
import com.localvalu.coremerchantapp.drivers.repsitory.DriverProfileRepository
import com.localvalu.coremerchantapp.network.RemoteDataSource
import com.localvalu.coremerchantapp.orders.api.OnlineOrderStatusChangeApi
import com.localvalu.coremerchantapp.orders.api.TableOrderListApi
import com.localvalu.coremerchantapp.orders.repository.OnlineOrderChangeStatusRepository
import com.localvalu.coremerchantapp.orders.repository.TableOrderListRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class DriverProfileModule
{
    @Provides
    fun providesDriverProfileApi(
        remoteDataSource: RemoteDataSource, @ApplicationContext
        context: Context
    ): DriverProfileApi
    {
        return remoteDataSource.buildApiDotNet(DriverProfileApi::class.java, context);
    }

    @Provides
    fun providesDriverProfileRepository(api: DriverProfileApi): DriverProfileRepository
    {
        return DriverProfileRepository(api);
    }

}