package com.localvalu.coremerchantapp.drivers.repsitory

import com.localvalu.coremerchantapp.base.BaseRepository
import com.localvalu.coremerchantapp.data.input.DriversProfileInput
import com.localvalu.coremerchantapp.data.input.OrderStatusChangeInput
import com.localvalu.coremerchantapp.drivers.api.DriverProfileApi

class DriverProfileRepository (private val api: DriverProfileApi) :
    BaseRepository(api)
{
    suspend fun driverProfile(driversProfileInput: DriversProfileInput) = safeApiCall {
        api.driverProfileInfo(driversProfileInput)
    }
}