package com.localvalu.coremerchantapp.drivers.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.localvalu.coremerchantapp.base.Event
import com.localvalu.coremerchantapp.base.Resource
import com.localvalu.coremerchantapp.data.input.DriversProfileInput
import com.localvalu.coremerchantapp.data.input.OrderDetailsInput
import com.localvalu.coremerchantapp.data.input.OrderListInput
import com.localvalu.coremerchantapp.data.input.OrderStatusChangeInput
import com.localvalu.coremerchantapp.data.output.driverOld.DriverProfileOutput
import com.localvalu.coremerchantapp.data.output.order.OrderDetailsResultOutput
import com.localvalu.coremerchantapp.data.output.order.OrderListOutput
import com.localvalu.coremerchantapp.data.output.order.OrderStatusChangeOutput
import com.localvalu.coremerchantapp.drivers.repsitory.DriverProfileRepository
import com.localvalu.coremerchantapp.orders.api.OnlineOrderStatusChangeApi
import com.localvalu.coremerchantapp.orders.repository.OnlineOrderChangeStatusRepository
import com.localvalu.coremerchantapp.orders.repository.OnlineOrderDetailsRepository
import com.localvalu.coremerchantapp.orders.repository.TableOrderListRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DriverProfileInfoViewModel @Inject constructor(val repository: DriverProfileRepository):ViewModel()
{
    private val _driverProfileResponse:MutableLiveData<Event<Resource<DriverProfileOutput>>> = MutableLiveData()

    val driverProfileResponse: LiveData<Event<Resource<DriverProfileOutput>>>
        get() = _driverProfileResponse

    fun driverProfile(driverProfileInput:DriversProfileInput) = viewModelScope.launch{
        _driverProfileResponse.value = Event(Resource.Loading)
        _driverProfileResponse.value = Event(repository.driverProfile(driverProfileInput))
    }
}