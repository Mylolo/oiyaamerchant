package com.localvalu.coremerchantapp.extensions

import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.localvalu.coremerchantapp.base.Resource
import com.localvalu.coremerchantapp.R

fun Fragment.handleAPIError(failure: Resource.Failure, retry: (()->Unit)?=null)
{
    when
    {
        failure.isNetworkError->
        {
            requireView().snackbar(requireContext().getString(R.string.lbl_could_not_process_your_request),retry)
        }
    }
}

fun Fragment.showMessage(message: String, retry: (()->Unit)?=null)
{
    requireView().snackbar(message)
}

fun View.snackbar(message: String, action: (()-> Unit)?=null)
{
    val snackbar = Snackbar.make(this,message, Snackbar.LENGTH_INDEFINITE)
    action?.let { invokeFun->
        snackbar.setAction(this.context.getString(R.string.lbl_retry))
        {
            invokeFun.invoke()
        }
    }
    snackbar.show()
}

fun Fragment.closeKeyboard(context: Context,view:View?)
{
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
    imm?.hideSoftInputFromWindow(view?.windowToken, 0)
}
