package com.localvalu.coremerchantapp.extensions

//Or add extension
fun Double.shorten(): String{
    return String.format("%.2f", this)
}