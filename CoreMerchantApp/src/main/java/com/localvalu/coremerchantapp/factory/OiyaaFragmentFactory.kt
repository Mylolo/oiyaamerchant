package com.localvalu.coremerchantapp.factory

import androidx.fragment.app.FragmentFactory
import com.localvalu.coremerchantapp.merchantdashboard.ui.DashboardFragment

class OiyaaFragmentFactory : FragmentFactory()
{
    override fun instantiate(classLoader: ClassLoader, className: String) =

        when(className)
        {
            DashboardFragment::javaClass.name->
            {
               DashboardFragment()
            }
            else->
            {
                super.instantiate(classLoader, className)
            }
        }


}