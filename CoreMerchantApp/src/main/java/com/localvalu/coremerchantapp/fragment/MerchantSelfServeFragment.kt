package com.localvalu.coremerchantapp.fragment

import android.content.Context
import android.content.Intent
import android.graphics.PorterDuff
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.core.content.ContextCompat
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.localvalu.coremerchantapp.BuildConfig
import com.localvalu.coremerchantapp.R
import com.localvalu.coremerchantapp.base.Resource
import com.localvalu.coremerchantapp.handler.DashBoardHandler
import com.localvalu.coremerchantapp.data.input.MerchantSelfServeInput
import com.localvalu.coremerchantapp.databinding.FragmentMerchantSelfServeBinding
import com.localvalu.coremerchantapp.extensions.closeKeyboard
import com.localvalu.coremerchantapp.extensions.handleAPIError
import com.localvalu.coremerchantapp.extensions.showMessage
import com.localvalu.coremerchantapp.merchantselfserve.viewmodel.MerchantSelfServeViewModel
import com.localvalu.coremerchantapp.viewmodel.DashboardViewModel
import com.utils.base.AppBaseFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MerchantSelfServeFragment : AppBaseFragment()
{
    private var binding: FragmentMerchantSelfServeBinding? = null
    private val dashboardViewModel:DashboardViewModel by viewModels()
    private val viewModel:MerchantSelfServeViewModel by viewModels()
    private var dashBoardHandler: DashBoardHandler? = null
    private var colorWhite = 0
    private var retailerId:String=""

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?
    {
        binding = FragmentMerchantSelfServeBinding.inflate(inflater, container, false)
        setHasOptionsMenu(true)
        retainInstance = true
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        super.onViewCreated(view, savedInstanceState)
        setProperties()
    }

    private fun setProperties()
    {
        binding!!.btnSubmit.isEnabled=false
        hideProgress()
        binding!!.btnSubmit.setOnClickListener(onClickListener)
        binding!!.imgBack.setOnClickListener(onClickListener)
        binding!!.tvCheckListPrep.setOnClickListener(onClickListener)
        if (binding!!.imgBack.drawable != null)
        {
            binding!!.imgBack.setColorFilter(colorWhite, PorterDuff.Mode.SRC_ATOP)
        }
        if (BuildConfig.DEBUG)
        {
        }
        binding!!.etName.doAfterTextChanged {
            viewModel?.name.value=binding!!.etName.text.toString()
            viewModel?.isValidName()
        }
        binding!!.etEmail.doAfterTextChanged {
            viewModel?.email.value=binding!!.etEmail.text.toString()
            viewModel?.isValidEmail()
        }
        binding!!.etContactNumber.doAfterTextChanged {
            viewModel?.contactNumber.value=binding!!.etContactNumber.text.toString()
            viewModel?.isValidContactNumber()
        }
        binding!!.etBusinessName.doAfterTextChanged {
            viewModel?.businessName.value=binding!!.etBusinessName.text.toString()
            viewModel?.isValidBusinessName()
        }
        binding!!.etProviderName.doAfterTextChanged {
            viewModel?.providerName.value=binding!!.etProviderName.text.toString()
            viewModel?.isValidProviderName()
        }
        binding!!.etPostcode.doAfterTextChanged {
            viewModel?.postCode.value=binding!!.etPostcode.text.toString()
            viewModel?.isValidPostCode()
        }
        binding!!.etProviderName.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE)
            {
                doRegisterMerchantSelfServe()
                true
            }
            else
            {
                false
            }
        }
        observeSubscribers()
    }

    private fun observeSubscribers()
    {
        dashboardViewModel.user.observe(viewLifecycleOwner, Observer {
            retailerId = it.retailerId.toString()
        })
        viewModel.enableSendNow.observe(viewLifecycleOwner, Observer {
            binding!!.btnSubmit.isEnabled=it
        })
        viewModel.validName.observe(viewLifecycleOwner, Observer {
            if(!it)
            {
                binding!!.etName.error = getString(R.string.msg_enter_your_name)
                binding!!.etName.requestFocus()
            }
            else
            {
                binding!!.etName.error = null
            }

        })
        viewModel.validContactNumber.observe(viewLifecycleOwner, Observer {
            if(!it)
            {
                binding!!.etContactNumber.error =
                    getString(R.string.msg_enter_valid_contact_no)
                binding!!.etContactNumber.requestFocus()

            }
            else
            {
                binding!!.etContactNumber.error = null
            }

        })
        viewModel.validEmail.observe(viewLifecycleOwner, Observer {
            if(!it)
            {
                binding!!.etEmail.error =
                    getString(R.string.msg_enter_valid_email_id)
                binding!!.etEmail.requestFocus()

            }
            else
            {
                binding!!.etEmail.error = null
            }

        })
        viewModel.validPostCode.observe(viewLifecycleOwner, Observer {
            if(!it)
            {
                binding!!.etPostcode.error = getString(R.string.msg_enter_valid_post_code)
                binding!!.etPostcode.requestFocus()
            }
            else
            {
                binding!!.etPostcode.error = null
            }

        })
        viewModel.validBusinessName.observe(viewLifecycleOwner, Observer {
            if(!it)
            {
                binding!!.etBusinessName.error = getString(R.string.msg_enter_your_business_name)
                binding!!.etBusinessName.requestFocus()
            }
            else
            {
                binding!!.etBusinessName.error = null
            }

        })
        viewModel.validProviderName.observe(viewLifecycleOwner, Observer {
            if(!it)
            {
                binding!!.etProviderName.error = getString(R.string.msg_enter_your_provider_name)
                binding!!.etProviderName.requestFocus()
            }
            else
            {
                binding!!.etProviderName.error = null
            }

        })
        viewModel.merchantSelfServeResponse.observe(viewLifecycleOwner, Observer { response->
            when(response)
            {
                is Resource.Loading->
                {
                    showProgress()
                }
                is Resource.Success->
                {
                    hideProgress()
                    if(response.value.merchantSelfServe.errorDetails.errorCode==0)
                    {
                        showMessage(getString(R.string.lbl_successfully_registered))
                    }
                    else
                    {
                        showMessage(response.value.merchantSelfServe.errorDetails.errorMessage)
                    }
                }
                is Resource.Failure->
                {
                    hideProgress()
                    handleAPIError(response){
                        doRegisterMerchantSelfServe()
                    }
                }
            }
        })
    }

    var onClickListener = View.OnClickListener { view -> onButtonClick(view) }

    override fun onAttach(context: Context)
    {
        super.onAttach(context)
        dashBoardHandler = context as DashBoardHandler
        colorWhite = ContextCompat.getColor(context, R.color.colorWhite)
    }

    override fun onDetach()
    {
        super.onDetach()
        dashBoardHandler = null
    }

    private fun onButtonClick(view: View)
    {
        when(view.id)
        {
            R.id.btnSubmit-> doRegisterMerchantSelfServe()
            R.id.imgBack-> dashBoardHandler!!.onBackButtonPressed()
            R.id.tvCheckListPrep->  showCheckList(
                "https://www.oiyaa.com//Documents/sales_notes.pdf",
                getString(R.string.lbl_checklist)
            )
        }
    }

    private fun showCheckList(strUri: String, title: String)
    {
        val browserIntent = Intent(Intent.ACTION_VIEW)
        browserIntent.setDataAndType(Uri.parse(strUri), "application/pdf")
        val chooser = Intent.createChooser(browserIntent, title)
        chooser.flags = Intent.FLAG_ACTIVITY_NEW_TASK // optional
        startActivity(chooser)
    }

    private fun hideProgress()
    {
        binding!!.progressIndicator.visibility = View.GONE
        binding!!.btnSubmit.visibility=View.VISIBLE
    }

    private fun showProgress()
    {
        binding!!.progressIndicator.visibility = View.VISIBLE
        binding!!.btnSubmit.visibility=View.GONE
    }

    private fun doRegisterMerchantSelfServe()
    {
        closeKeyboard(requireContext(),binding!!.etProviderName)
        val merchantSelfServeInput = MerchantSelfServeInput()
        merchantSelfServeInput.name = binding!!.etName.text.toString()
        merchantSelfServeInput.contactNumber = binding!!.etContactNumber.text.toString()
        merchantSelfServeInput.emailId = binding!!.etEmail.text.toString()
        merchantSelfServeInput.postCode = binding!!.etPostcode.text.toString()
        merchantSelfServeInput.businessName = binding!!.etBusinessName.text.toString()
        merchantSelfServeInput.businessNumber = ""
        merchantSelfServeInput.providerName = binding!!.etProviderName.text.toString()
        merchantSelfServeInput.comment = ""
        viewModel?.merchantSelfServe(merchantSelfServeInput);
    }

    companion object
    {
        private val TAG = MerchantSelfServeFragment::class.java.simpleName
    }
}