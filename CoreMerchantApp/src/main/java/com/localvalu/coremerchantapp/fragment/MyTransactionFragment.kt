package com.localvalu.coremerchantapp.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.datepicker.CalendarConstraints
import com.google.android.material.datepicker.MaterialDatePicker
import com.localvalu.coremerchantapp.LocalValuFragment
import com.localvalu.coremerchantapp.R
import com.localvalu.coremerchantapp.fragment.adapter.MyTransactionAdapter
import com.localvalu.coremerchantapp.handler.DashBoardHandler
import com.utils.util.DateTimeUtil
import com.localvalu.coremerchantapp.databinding.FragmentMyTransactionBinding
import com.localvalu.coremerchantapp.viewmodel.DashboardViewModel
import com.localvalu.coremerchantapp.base.Resource
import com.localvalu.coremerchantapp.data.input.MyTransactionInput
import com.localvalu.coremerchantapp.extensions.handleAPIError
import com.localvalu.coremerchantapp.extensions.showMessage
import com.localvalu.coremerchantapp.merchantselfserve.viewmodel.MerchantSelfServeViewModel
import com.localvalu.coremerchantapp.transaction.viewmodel.TransactionsViewModel
import com.localvalu.coremerchantapp.utils.RangeValidator
import com.localvalu.coremerchantapp.utils.getCalenderEndDate
import com.localvalu.coremerchantapp.utils.getCalenderStartDate
import com.utils.helper.preference.PreferenceFactory
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_my_transaction.*
import java.util.*

@AndroidEntryPoint
class MyTransactionFragment : LocalValuFragment(), View.OnClickListener
{
    private var binding:FragmentMyTransactionBinding?=null
    private val dashboardViewModel: DashboardViewModel by viewModels()
    private val viewModel: TransactionsViewModel by viewModels()
    private var myTransactionAdapter: MyTransactionAdapter? = null
    private var dashBoardHandler: DashBoardHandler? = null
    private val MATERIAL_DIALOG_DATE_PICKER = "MaterialDialogPicker"
    private var retailerId:String?=null

    override fun onAttach(context: Context)
    {
        super.onAttach(context)
        dashBoardHandler = context as DashBoardHandler
    }

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?
    {
        binding = FragmentMyTransactionBinding.inflate(inflater, container, false)
        setHasOptionsMenu(true)
        retainInstance = true
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        super.onViewCreated(view, savedInstanceState)
        setProperties()
        observeSubscribers()
    }

    private fun setProperties()
    {
        binding!!.btnView.isEnabled = false
        showListView()
        binding!!.etStartDate.setOnClickListener{
            showDatePicker(true)
        }
        binding!!.etEndDate.setOnClickListener{
            showDatePicker(false)
        }
        binding!!.btnView.setOnClickListener{
            viewTransactions()
        }
        binding!!.etStartDate.doAfterTextChanged {
            viewModel?.enableDisableSend(binding!!.etStartDate.text.toString(),
                binding!!.etEndDate.text.toString())
        }
        binding!!.etEndDate.doAfterTextChanged {
            viewModel?.enableDisableSend(binding!!.etStartDate.text.toString(),
                binding!!.etEndDate.text.toString())
        }
        binding!!.layoutError.btnRetry.setOnClickListener{
            viewTransactions()
        }
        binding!!.rvTransactions.layoutManager = LinearLayoutManager(requireContext())
        myTransactionAdapter = MyTransactionAdapter()
        binding!!.rvTransactions.adapter = myTransactionAdapter
    }

    private fun observeSubscribers()
    {
        dashboardViewModel.user.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            this.retailerId = it.retailerId.toString()
        })
        viewModel.enableSendNow.observe(viewLifecycleOwner, Observer {
            binding!!.btnView.isEnabled=it
            viewModel.initialCall.observe(viewLifecycleOwner, Observer {
                viewTransactions()
            })
        })
        viewModel.strStartDate.observe(viewLifecycleOwner, Observer {
            etStartDate.setText(it)
        })
        viewModel.strEndDate.observe(viewLifecycleOwner, Observer {
            etEndDate.setText(it)
        })
        viewModel.transactionsResponse.observe(viewLifecycleOwner, Observer { response->
            when(response)
            {
                is Resource.Loading->
                {
                    showProgress()
                }
                is Resource.Success->
                {
                    if(response.value.result.errorDetail.errorCode==0)
                    {
                        showListView()
                        if(response.value.result.datas.size>0)
                        {
                            myTransactionAdapter?.submitList(response.value.result.datas)
                        }
                        else
                        {
                            showErrorView(getString(R.string.lbl_no_transactions_found),true)
                        }
                    }
                    else
                    {
                        showErrorView(response.value.result.errorDetail.errorMessage,true)
                    }
                }
                is Resource.Failure->
                {
                    handleAPIError(response){
                        viewTransactions()
                    }
                    showErrorView(getString(R.string.lbl_could_not_process_your_request),true)
                }
            }
        })
        viewModel.initialCall.observe(viewLifecycleOwner, Observer {
            if(it){viewTransactions()}
        })
        dashboardViewModel?.getUser()
    }

    override fun onClick(view: View)
    {
        val id = view.id
        when(id)
        {
            R.id.etStartDate->showDatePicker(true)
            R.id.etEndDate->showDatePicker(false)
            R.id.btnView->viewTransactions()
        }
    }

    private fun viewTransactions()
    {
        retailerId?.let {
            val myTransactionInput = MyTransactionInput()
            myTransactionInput.accountId =
                PreferenceFactory.getInstance().getString(PREF_ACCOUNT_ID).toInt()
            myTransactionInput.merchantId = it.toInt()
            myTransactionInput.fromDate = binding!!.etStartDate.text.toString()
            myTransactionInput.toDate = binding!!.etEndDate.text.toString()
            viewModel.transactions(myTransactionInput)
        }
    }

    private fun setFromDate(date: Long)
    {
        viewModel.setStartDate(Date(date))
    }

    private fun setToDate(date: Long)
    {
        viewModel.setEndDate(Date(date))
    }

    private fun showDatePicker(isStartDate: Boolean)
    {
        val materialDateBuilder: MaterialDatePicker.Builder<*> = MaterialDatePicker.Builder.datePicker()
        // Build constraints.
        materialDateBuilder.setCalendarConstraints(timeLimitRange().build())
        if(isStartDate)materialDateBuilder.setTitleText(getString(R.string.lbl_select_start_date))
        else materialDateBuilder.setTitleText(getString(R.string.lbl_select_end_date))
        val materialDatePicker = materialDateBuilder.build()
        materialDatePicker.addOnPositiveButtonClickListener {
            if(isStartDate)setFromDate(it.toString().toLong())
            else setToDate(it.toString().toLong())
        }
        materialDatePicker.show(requireActivity().supportFragmentManager,MATERIAL_DIALOG_DATE_PICKER)
    }

    /*
     Limit selectable Date range
   */
    private fun timeLimitRange(): CalendarConstraints.Builder
    {
        val constraintsBuilderRange = CalendarConstraints.Builder()
        constraintsBuilderRange.setStart(getCalenderStartDate().time)
        constraintsBuilderRange.setEnd(getCalenderEndDate().time)
        constraintsBuilderRange.setValidator( RangeValidator(getCalenderStartDate().time,getCalenderEndDate().time))
        return constraintsBuilderRange
    }

    private fun showProgress()
    {
        binding!!.rvTransactions.visibility = View.GONE
        binding!!.layoutProgress.layoutRoot.visibility = View.VISIBLE
        binding!!.layoutError.layoutRoot.visibility = View.GONE
    }

    private fun showListView()
    {
        binding!!.rvTransactions.visibility = View.VISIBLE
        binding!!.layoutProgress.layoutRoot.visibility = View.GONE
        binding!!.layoutError.layoutRoot.visibility = View.GONE
    }

    private fun showErrorView(message: String,showRetry:Boolean)
    {
        binding!!.rvTransactions.visibility = View.GONE
        binding!!.layoutProgress.layoutRoot.visibility = View.GONE
        binding!!.layoutError.layoutRoot.visibility = View.VISIBLE
        binding!!.layoutError.tvError.setText(message)
        if(showRetry) binding!!.layoutError.btnRetry.visibility=View.VISIBLE
        else binding!!.layoutError.btnRetry.visibility=View.GONE
    }

    override fun onDetach()
    {
        super.onDetach()
        dashBoardHandler = null
    }

    companion object
    {
        private val TAG = MyTransactionFragment::class.java.simpleName
    }
}