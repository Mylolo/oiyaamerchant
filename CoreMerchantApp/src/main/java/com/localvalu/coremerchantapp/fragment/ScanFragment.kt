package com.localvalu.coremerchantapp.fragment

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.localvalu.coremerchantapp.handler.DashBoardHandler
import com.google.zxing.client.android.BeepManager
import com.localvalu.coremerchantapp.utils.FontUtilis
import com.journeyapps.barcodescanner.DefaultDecoderFactory
import com.google.zxing.BarcodeFormat
import com.journeyapps.barcodescanner.BarcodeCallback
import com.google.zxing.ResultPoint
import com.journeyapps.barcodescanner.BarcodeResult
import com.localvalu.coremerchantapp.R
import com.localvalu.coremerchantapp.base.Resource
import com.localvalu.coremerchantapp.databinding.FragmentScanBinding
import com.localvalu.coremerchantapp.dialog.SendAmountFragment
import com.localvalu.coremerchantapp.extensions.closeKeyboard
import com.localvalu.coremerchantapp.handler.TempHandler
import com.localvalu.coremerchantapp.scan.viewmodel.ScanViewModel
import com.localvalu.coremerchantapp.token.model.MerchantTokenBalanceRequest
import com.localvalu.coremerchantapp.token.viewmodel.MerchantTokenBalanceViewModel
import com.localvalu.coremerchantapp.utils.AppUtils
import com.localvalu.coremerchantapp.utils.Device
import com.localvalu.coremerchantapp.viewmodel.DashboardViewModel
import com.utils.base.AppBaseFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_send_amount.*

@AndroidEntryPoint
class ScanFragment : AppBaseFragment(), View.OnClickListener
{
    private var binding:FragmentScanBinding?=null;
    private var dashBoardHandler: DashBoardHandler? = null
    private var tempHandler: TempHandler? = null
    private var beepManager: BeepManager? = null
    private var lastText:String?=null;
    private val dashboardViewModel:DashboardViewModel by viewModels()
    private val viewModel: ScanViewModel by viewModels()
    private val merchantTokenBalanceViewModel: MerchantTokenBalanceViewModel by viewModels()
    private var retailerId:String?=null
    private var qrCode:String?=null
    private var counter:Int=0;

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?
    {
        binding = FragmentScanBinding.inflate(inflater,container,false)
        setHasOptionsMenu(true)
        retainInstance = true
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        super.onViewCreated(view, savedInstanceState)
        setProperties()
    }

    private fun setProperties()
    {
        beepManager = BeepManager(requireActivity())
        binding!!.scanner.decodeContinuous(object : BarcodeCallback
        {
            override fun barcodeResult(result: BarcodeResult)
            {
                Log.d("barcode result ", "Beep count - $counter")
                counter++
                if(counter==1)
                {
                    beepManager!!.playBeepSoundAndVibrate()
                    lastText = result.text
                    binding!!.etCustomerUniqueCode.setText(lastText)
                    moveToSendAmountFragment()
                    counter==0
                }               ;

            }
            override fun possibleResultPoints(resultPoints: List<ResultPoint>)
            {
            }
        })
        binding!!.btnSubmit.isEnabled = false
        binding!!.tilCustomerUniqueCode.isEnabled = false
        binding!!.scanner.setStatusText("Scan Your QR Code")
        binding!!.scanner.statusView.typeface = FontUtilis.getInstance().comfortaaRegular(requireContext())
        binding!!.scanner.barcodeView.decoderFactory = DefaultDecoderFactory(listOf(BarcodeFormat.QR_CODE))
        binding!!.scanner.initializeFromIntent(requireActivity().intent)
        binding!!.etCustomerUniqueCode.doAfterTextChanged {
            qrCode?.let { strQRCode ->
                viewModel?.setCustomerUniqueCode(binding!!.etCustomerUniqueCode.text.toString(),strQRCode )
            }
        }
        binding!!.etCustomerUniqueCode.setText("IDL")
        binding!!.etCustomerUniqueCode.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE)
            {
                if(binding!!.btnSubmit.isEnabled)
                {
                    moveToSendAmountFragment()
                }
                true
            }
            else
            {
                false
            }
        }
        binding!!.btnSubmit.setOnClickListener{
            moveToSendAmountFragment()
        }
        subscribeObservers()
        dashboardViewModel.getUser()
    }

    private fun moveToSendAmountFragment()
    {
        closeKeyboard(requireContext(),binding!!.etCustomerUniqueCode)
        val sendAmountFragment:SendAmountFragment = SendAmountFragment()
        val bundle:Bundle = Bundle()
        bundle.putString(AppUtils.BUNDLE_CUSTOMER_QR_CODE,binding!!.etCustomerUniqueCode.text.toString() )
        sendAmountFragment.arguments = bundle
        tempHandler?.updateFragment(sendAmountFragment,getString(R.string.lbl_enter_amount),true,false)
    }

    private fun subscribeObservers()
    {
        dashboardViewModel.user.observe(viewLifecycleOwner, androidx.lifecycle.Observer { signInResult->
            this.retailerId = signInResult.retailerId.toString()
            merchantTokenBalance()
        })
        merchantTokenBalanceViewModel.merchantTokenBalanceResponse.observe(viewLifecycleOwner,Observer { response->
                when(response)
                {
                    is Resource.Success->
                    {
                        if(response.value.result.errorDetail.errorCode==0)
                        {
                            qrCode = response.value.result.qrCode
                            binding?.tilCustomerUniqueCode?.isEnabled = true
                        }
                        else binding?.tilCustomerUniqueCode?.isEnabled = false
                    }
                    else ->
                    {
                        binding?.tilCustomerUniqueCode?.isEnabled=false
                    }
                }
            })
        viewModel.displayNotAllowedMessage.observe(viewLifecycleOwner, Observer {
            if(it)
            {
                binding!!.tilCustomerUniqueCode?.error = getString(R.string.lbl_not_allowed_to_send_this_account)
            }
            else binding!!.tilCustomerUniqueCode?.error = null
        })
        viewModel.enableSendNow.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            binding!!.btnSubmit.isEnabled = it
        })
    }

    private fun merchantTokenBalance()
    {
        retailerId?.let {
            merchantTokenBalanceViewModel?.
            merchantTokenBalance(
                MerchantTokenBalanceRequest("",
                40527018.toString(),retailerId.toString())
            )
        }
    }

    override fun onAttach(context: Context)
    {
        super.onAttach(context)
        dashBoardHandler = context as DashBoardHandler
        if(dashBoardHandler!!.deviceType()==Device.devicePlayStore || dashBoardHandler!!.deviceType() == Device.deviceT1N)
        {
            tempHandler = context as TempHandler
        }
    }

    override fun onDetach()
    {
        super.onDetach()
        dashBoardHandler = null
    }

    override fun onResume()
    {
        super.onResume()
        binding!!.scanner!!.resume()
    }

    override fun onPause()
    {
        super.onPause()
        binding!!.scanner!!.pause()
    }

    /*private fun showAmountDialog()
    {
        AlertAmountDialog.newInstance(
            localValuActivity,
            "Enter Total Sale Amount",
            object : AlertAmountDialogClickListener
            {
                override fun alertAmountDialogOkClicked(amount: Double)
                {
                    dashBoardHandler!!.doSaleTransaction(
                        lastText,
                        amount,
                        object : Task<SaleTransactionResult?>
                        {
                            override fun onSuccess(result: SaleTransactionResult)
                            {
                                if (Validator.isValid(result)) dashBoardHandler!!.scanSuccess(
                                    amount,
                                    result
                                )
                            }

                            override fun onError(message: String)
                            {
                                lastText = null
                            }
                        })
                }

                override fun alertAmountDialogCancelClicked()
                {
                    lastText = null
                }

                override fun alertAmountDialogOutsideClicked()
                {
                    lastText = null
                }
            })
    }*/

    companion object
    {
        private val TAG = ScanFragment::class.java.simpleName
    }

    override fun onClick(v: View?)
    {
    }



}