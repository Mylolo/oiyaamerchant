package com.localvalu.coremerchantapp.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import com.localvalu.coremerchantapp.LocalValuFragment
import com.localvalu.coremerchantapp.R
import com.localvalu.coremerchantapp.handler.DashBoardHandler
import com.localvalu.coremerchantapp.data.output.SaleTransactionResult
import com.localvalu.coremerchantapp.fragment.ScanSuccessFragment
import com.localvalu.coremerchantapp.utils.Device
import java.lang.StringBuilder

class ScanSuccessFragment : LocalValuFragment(), View.OnClickListener
{
    private var textViewMerchant: AppCompatTextView? = null
    private var textViewCustomerName: AppCompatTextView? = null
    private var textViewUnPaid: AppCompatTextView? = null
    private var textViewPromotion: AppCompatTextView? = null
    private var textViewToPay: AppCompatTextView? = null
    private var dashBoardHandler: DashBoardHandler? = null
    var amount: Double? = null
    var saleTransactionResult: SaleTransactionResult? = null

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?
    {
        val view = inflater.inflate(R.layout.fragment_scan_success, container, false)
        setHasOptionsMenu(true)
        retainInstance = true
        textViewMerchant = view.findViewById(R.id.scanSuccess_textView_merchant)
        textViewCustomerName = view.findViewById(R.id.scanSuccess_textView_customerName)
        textViewUnPaid = view.findViewById(R.id.scanSuccess_textView_unPaid)
        textViewPromotion = view.findViewById(R.id.scanSuccess_textView_promotion)
        textViewToPay = view.findViewById(R.id.scanSuccess_textView_toPay)
        view.findViewById<View>(R.id.scanSuccess_button_done)
            .setOnClickListener { view: View -> onClick(view) }
        populateData()
        return view
    }

    override fun onAttach(context: Context)
    {
        super.onAttach(context)
        dashBoardHandler = context as DashBoardHandler
    }

    override fun onDetach()
    {
        super.onDetach()
        dashBoardHandler = null
    }

    override fun onClick(view: View)
    {
        val id = view.id
        if (id == R.id.scanSuccess_button_done)
        {
            when(dashBoardHandler?.deviceType())
            {
                Device.devicePlayStore->
                {
                    dashBoardHandler!!.scan()
                }
                Device.deviceT1N->
                {
                    dashBoardHandler!!.scan()
                }
                else ->
                {
                    dashBoardHandler!!.doMoreOnScanTransactionSuccess(saleTransactionResult!!.balanceToPay)
                }
            }
        }
    }

    private fun populateData()
    {
        textViewMerchant!!.text = saleTransactionResult!!.merchantName
        textViewCustomerName!!.text = saleTransactionResult!!.customerName
        textViewUnPaid!!.text =
            context!!.getString(R.string.symbol_pound) + " " + saleTransactionResult!!.total
        val promotions = StringBuilder()
        promotions.append(context!!.getString(R.string.currency_letter_lt))
        promotions.append(" ").append(saleTransactionResult!!.tokenApplied)
        promotions.append(" (").append(saleTransactionResult!!.promotion)
        promotions.append("%)")
        textViewPromotion!!.text = promotions.toString()
        textViewToPay!!.text =
            context!!.getString(R.string.symbol_pound) + " " + saleTransactionResult!!.balanceToPay
    }

    companion object
    {
        private val TAG = ScanSuccessFragment::class.java.simpleName
    }
}