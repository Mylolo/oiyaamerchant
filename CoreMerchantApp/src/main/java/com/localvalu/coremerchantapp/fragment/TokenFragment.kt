package com.localvalu.coremerchantapp.fragment

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Color
import android.os.Bundle
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import dagger.hilt.android.AndroidEntryPoint
import com.localvalu.coremerchantapp.handler.DashBoardHandler
import com.localvalu.coremerchantapp.data.output.TokenBalanceResult
import com.google.zxing.common.BitMatrix
import com.google.zxing.MultiFormatWriter
import com.google.zxing.BarcodeFormat
import com.localvalu.coremerchantapp.LocalValuFragment
import com.localvalu.coremerchantapp.R
import com.localvalu.coremerchantapp.base.Resource
import com.localvalu.coremerchantapp.databinding.FragmentTokenBinding
import com.localvalu.coremerchantapp.extensions.handleAPIError
import com.localvalu.coremerchantapp.extensions.shorten
import com.localvalu.coremerchantapp.extensions.showMessage
import com.localvalu.coremerchantapp.fragment.TokenFragment
import com.localvalu.coremerchantapp.token.model.MerchantTokenBalanceRequest
import com.localvalu.coremerchantapp.token.viewmodel.MerchantTokenBalanceViewModel
import com.localvalu.coremerchantapp.viewmodel.DashboardViewModel
import com.utils.base.Task
import com.utils.validator.Validator
import java.lang.Exception
import java.lang.StringBuilder

@AndroidEntryPoint
class TokenFragment : LocalValuFragment()
{
    private var dashBoardHandler: DashBoardHandler? = null
    private var binding: FragmentTokenBinding? = null
    private var colorWhite = 0
    private var colorBlack = 0
    private val dashboardViewModel:DashboardViewModel by activityViewModels()
    private val viewModel: MerchantTokenBalanceViewModel by viewModels()
    private var retailerId:String? = null

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?
    {
        binding = FragmentTokenBinding.inflate(inflater, container, false)
        setHasOptionsMenu(true)
        retainInstance = true
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        setProperties()
    }

    override fun onAttach(context: Context)
    {
        var context: Context? = context
        super.onAttach(context!!)
        context = context
        dashBoardHandler = context as DashBoardHandler?
        colorBlack = ContextCompat.getColor(context, R.color.colorBlack)
        colorWhite = ContextCompat.getColor(context, R.color.colorWhite)
        //dashBoardHandler!!.startService()
    }

    private fun setProperties()
    {
        setSupportingText()
        binding!!.btnContinue.setOnClickListener(onClickListener)
        observeSubscribers()
        dashboardViewModel?.getUser()
    }

    private fun observeSubscribers()
    {
        dashboardViewModel?.user.observe(viewLifecycleOwner, Observer { signInResult->
            this.retailerId = signInResult?.retailerId?.toString()
            merchantTokenBalance()
        })
        viewModel.merchantTokenBalanceResponse?.observe(viewLifecycleOwner,Observer {  response->
            when(response)
            {
                is Resource.Loading ->
                {
                    showProgress()
                }
                is Resource.Success ->
                {
                    showContent()
                    when(response.value.result.errorDetail.errorCode)
                    {
                        0->
                        {
                            setData(response?.value.result);
                        }
                        else->
                        {
                            setTokenWhenError()
                            showMessage(response.value.result.errorDetail.errorMessage)
                        }
                    }
                }
                is Resource.Failure ->
                {
                    showContent()
                    setTokenWhenError()
                    handleAPIError(response){
                        merchantTokenBalance()
                    }
                }
            }
        })
    }

    private fun merchantTokenBalance()
    {
        retailerId?.let {
            viewModel?.
            merchantTokenBalance(MerchantTokenBalanceRequest("",
                40527018.toString(),retailerId.toString()))
        }
    }

    var onClickListener = View.OnClickListener { view -> onButtonClick(view) }

    private fun setSupportingText()
    {
        val builder = SpannableStringBuilder()
        val str1 = SpannableString(getString(R.string.lbl_splash_info_text_one))
        str1.setSpan(ForegroundColorSpan(colorBlack), 0, str1.length, 0)
        builder.append(str1).append(" ")
        val str2 = SpannableString(getString(R.string.lbl_splash_info_text_one_a_white))
        str2.setSpan(ForegroundColorSpan(colorWhite), 0, str2.length, 0)
        builder.append(str2).append(" ")
        val str3 = SpannableString(getString(R.string.lbl_splash_info_text_one_a_black))
        str3.setSpan(ForegroundColorSpan(colorBlack), 0, str3.length, 0)
        builder.append(str3).append(" ")
        binding!!.tvSplashTextOne.setText(builder, TextView.BufferType.SPANNABLE)
        val builder2 = SpannableStringBuilder()
        val str4 = SpannableString(getString(R.string.lbl_splash_info_text_two))
        str4.setSpan(ForegroundColorSpan(colorWhite), 0, str4.length, 0)
        builder2.append(str4).append(" ")
        val str5 = SpannableString(getString(R.string.lbl_splash_info_text_two_a))
        str5.setSpan(ForegroundColorSpan(colorBlack), 0, str5.length, 0)
        builder2.append(str5).append(" ")
        binding!!.tvSplashTextTwo.setText(builder2, TextView.BufferType.SPANNABLE)
    }

    override fun onDetach()
    {
        super.onDetach()
        dashBoardHandler = null
    }

    private fun onButtonClick(view: View)
    {
        val id = view.id
        if (id == R.id.btnContinue)
        {
            dashBoardHandler!!.scan()
        }
    }

    private fun setData(result: TokenBalanceResult)
    {

        val strTokenBalance = StringBuilder()
        strTokenBalance.append(getContext()!!.getString(R.string.currency_letter_lt))
            .append(" ")
            .append(result.currentTokenBalance.shorten())
        binding!!.tvMyLoyaltyBalance.text = strTokenBalance.toString()
        binding!!.tvCustomerCode.text = result.qrCode
        val qrCodeImage = generateQRCode(result.qrCode)
        if (Validator.isValid(qrCodeImage)) binding!!.ivQRCode.setImageBitmap(qrCodeImage)
    }

    private fun setTokenWhenError()
    {

        binding!!.tvMyLoyaltyBalance.text =
            getContext()!!.getString(R.string.symbol_pound) + " 0.00"
        binding!!.tvMyLoyaltyBalance.text =
            getContext()!!.getString(R.string.symbol_pound) + "0"

    }

    private fun generateQRCode(data: String): Bitmap?
    {
        val retVal: Bitmap
        try
        {
            val size = 420
            val bitMatrix =
                MultiFormatWriter().encode(data, BarcodeFormat.QR_CODE, size, size, null)
            val width = bitMatrix.width
            val height = bitMatrix.height
            val pixels = IntArray(width * height)
            for (i in 0 until height)
            {
                val offset = i * width
                for (j in 0 until width)
                {
                    pixels[offset + j] = if (bitMatrix[j, i]) Color.BLACK else Color.WHITE
                }
            }
            retVal = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
            retVal.setPixels(pixels, 0, size, 0, 0, width, height)
        } catch (e: Exception)
        {
            return null
        }
        return retVal
    }

    private fun showProgress()
    {
        binding!!.layoutProgress.layoutRoot.visibility = View.VISIBLE
        binding!!.cnlContent.visibility = View.GONE
        binding!!.layoutError.layoutRoot.visibility = View.GONE
    }

    private fun showContent()
    {
        binding!!.layoutProgress.layoutRoot.visibility = View.GONE
        binding!!.cnlContent.visibility = View.VISIBLE
        binding!!.layoutError.layoutRoot.visibility = View.GONE
    }

    private fun showError(strMessage: String, retry: Boolean)
    {
        binding!!.layoutProgress.layoutRoot.visibility = View.GONE
        binding!!.cnlContent.visibility = View.GONE
        binding!!.layoutError.layoutRoot.visibility = View.VISIBLE
        binding!!.layoutError.tvError.text = strMessage
        if (retry) binding!!.layoutError.btnRetry.visibility = View.VISIBLE
        else binding!!.layoutError.btnRetry.visibility = View.GONE
    }

    companion object
    {
        private val TAG = TokenFragment::class.java.simpleName
    }
}