package com.localvalu.coremerchantapp.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;

import com.localvalu.coremerchantapp.LocalValuFragment;
import com.localvalu.coremerchantapp.R;
import com.localvalu.coremerchantapp.data.output.TokenBalanceResult;
import com.localvalu.coremerchantapp.data.output.TransferLoyaltyTokenResult;
import com.localvalu.coremerchantapp.handler.DashBoardHandler;

import com.utils.base.Task;
import com.utils.validator.Validator;

public class TransferLoyaltyTokenFragment extends LocalValuFragment implements View.OnClickListener
{

    private static final String TAG = TransferLoyaltyTokenFragment.class.getSimpleName();

    private AppCompatTextView textViewCurrentBalance, textViewBalanceAfterTransfer;
    private AppCompatEditText editTextTransferAmount, editTextSendLoyaltyTokenTo;

    private DashBoardHandler dashBoardHandler;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_transfer_loyalty_token, container, false);
        setHasOptionsMenu(true);
        setRetainInstance(true);

        textViewCurrentBalance = view.findViewById(R.id.transferLoyaltyToken_textView_currentBalance);
        view.findViewById(R.id.transferLoyaltyToken_button_subtract).setOnClickListener(this::onClick);
        editTextTransferAmount = view.findViewById(R.id.transferLoyaltyToken_editText_transferAmount);
        view.findViewById(R.id.transferLoyaltyToken_button_add).setOnClickListener(this::onClick);
        textViewBalanceAfterTransfer = view.findViewById(R.id.transferLoyaltyToken_textView_balanceAfterTransfer);
        editTextSendLoyaltyTokenTo = view.findViewById(R.id.transferLoyaltyToken_editText_sendLoyaltyTokenTo);
        view.findViewById(R.id.transferLoyaltyToken_button_sendNow).setOnClickListener(this::onClick);

        populateData();
        return view;
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);

        dashBoardHandler = (DashBoardHandler) context;
    }

    @Override
    public void onDetach()
    {
        super.onDetach();

        dashBoardHandler = null;
    }

    @Override
    public void onClick(View view)
    {
        int id = view.getId();

        if (id == R.id.transferLoyaltyToken_button_subtract)
        {
            populateBalanceAfterTransfer(false);
        }
        else if (id == R.id.transferLoyaltyToken_button_add)
        {
            populateBalanceAfterTransfer(true);
        }
        else if (id == R.id.transferLoyaltyToken_button_sendNow)
        {
            doSendLoyaltyToken();
        }
    }

    private void populateData()
    {
        resetData();

        dashBoardHandler.doGetMyTokenBalance(new Task<TokenBalanceResult>()
        {
            @Override
            public void onSuccess(TokenBalanceResult result)
            {
                if (Validator.isValid(result))
                {
                    Double currentTokenBalance = result.getCurrentTokenBalance();

                    textViewCurrentBalance.setText(String.valueOf(currentTokenBalance));
                    editTextTransferAmount.setText("0");
                    textViewBalanceAfterTransfer.setText(String.valueOf(currentTokenBalance));

                    populateBalanceAfterTransfer(true);
                }
                else
                {
                    resetData();
                }
            }

            @Override
            public void onError(String message)
            {
                resetData();
            }
        });
    }

    private void doSendLoyaltyToken()
    {
        String currentBalance = textViewCurrentBalance.getText().toString();
        String transferAmount = editTextTransferAmount.getText().toString();
        String balanceAfterTransfer = textViewBalanceAfterTransfer.getText().toString();
        String sendLoyaltyTokenTo = editTextSendLoyaltyTokenTo.getText().toString();

        dashBoardHandler.doTransferLoyaltyToken(currentBalance, transferAmount, balanceAfterTransfer, sendLoyaltyTokenTo, new Task<TransferLoyaltyTokenResult>()
        {
            @Override
            public void onSuccess(TransferLoyaltyTokenResult result)
            {
                populateData();
            }

            @Override
            public void onError(String message)
            {

            }
        });
    }

    private void populateBalanceAfterTransfer(boolean add)
    {
        int currentBalance = Integer.parseInt(textViewCurrentBalance.getText().toString());

        int transferAmount = Integer.parseInt(editTextTransferAmount.getText().toString());
        transferAmount = add ? (transferAmount + 1) : (transferAmount - 1);
        if (transferAmount >= 0 && transferAmount <= currentBalance)
        {
            editTextTransferAmount.setText(String.valueOf(transferAmount));

            int currentBalanceAfterTransfer = Integer.parseInt(textViewBalanceAfterTransfer.getText().toString());
            int balanceAfterTransfer = add ? (currentBalanceAfterTransfer - 1) : (currentBalanceAfterTransfer + 1);
            textViewBalanceAfterTransfer.setText(String.valueOf(balanceAfterTransfer));
        }
    }

    private void resetData()
    {
        textViewCurrentBalance.setText("0.00");
        editTextTransferAmount.setText("0");
        textViewBalanceAfterTransfer.setText("0.00");
        editTextSendLoyaltyTokenTo.setText("localvalu");
    }
}
