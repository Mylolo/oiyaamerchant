package com.localvalu.coremerchantapp.fragment.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.localvalu.coremerchantapp.R
import com.localvalu.coremerchantapp.data.output.MyTransactionData
import com.localvalu.coremerchantapp.data.output.myappointments.MyAppointmentList
import com.localvalu.coremerchantapp.listeners.MyAppointmentItemClickListener
import com.localvalu.coremerchantapp.fragment.adapter.viewholder.MyAppointmentListVH
import java.util.ArrayList


class MyAppointmentListAdapter(private val myAppointmentItemClickListener:MyAppointmentItemClickListener):
    ListAdapter<MyAppointmentList, MyAppointmentListVH>(DIFF_CALLBACK),MyAppointmentItemClickListener
{
    private var context: Context? = null
    private val data: ArrayList<MyTransactionData>? = null

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView)
        context = recyclerView.context
    }

    override fun onCreateViewHolder(
        viewGroup: ViewGroup,
        position: Int
    ): MyAppointmentListVH
    {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.view_item_my_appointment_list, viewGroup, false)
        return MyAppointmentListVH(viewGroup.context,view,position,myAppointmentItemClickListener)
    }

    override fun onBindViewHolder(holder: MyAppointmentListVH, position: Int)
    {
        if (holder is MyAppointmentListVH) holder.populateData(getItem(position))
    }

    override fun getItemId(position: Int): Long
    {
        return position.toLong()
    }

    companion object
    {
        private const val TAG = "MyAppointmentListAdapter"
        private val DIFF_CALLBACK: DiffUtil.ItemCallback<MyAppointmentList> =
            object : DiffUtil.ItemCallback<MyAppointmentList>()
            {
                override fun areItemsTheSame(
                    oldItem: MyAppointmentList,
                    newItem: MyAppointmentList
                ): Boolean
                {
                    return oldItem.slotId == newItem.slotId
                }

                override fun areContentsTheSame(
                    oldItem: MyAppointmentList,
                    newItem: MyAppointmentList
                ): Boolean
                {
                    return Integer.toString(oldItem.slotId.toInt()) == Integer.toString(newItem.slotId.toInt())
                }
            }
    }

    override fun onMyAppointmentItemClicked(position: Int, myAppointmentList: MyAppointmentList?)
    {
        myAppointmentItemClickListener.onMyAppointmentItemClicked(position,myAppointmentList)
    }

    override fun onMyAppointmentAccepted(position: Int, myAppointmentList: MyAppointmentList?)
    {
        myAppointmentItemClickListener.onMyAppointmentAccepted(position,myAppointmentList)
    }

    override fun onMyAppointmentDeclined(position: Int, myAppointmentList: MyAppointmentList?)
    {
        myAppointmentItemClickListener.onMyAppointmentDeclined(position,myAppointmentList)
    }

}