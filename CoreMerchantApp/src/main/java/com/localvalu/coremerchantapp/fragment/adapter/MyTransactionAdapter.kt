package com.localvalu.coremerchantapp.fragment.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.*
import com.localvalu.coremerchantapp.R
import com.localvalu.coremerchantapp.data.input.MyTransactionInput
import com.localvalu.coremerchantapp.data.output.MyTransactionData
import com.localvalu.coremerchantapp.handler.DashBoardHandler
import com.localvalu.coremerchantapp.fragment.adapter.viewholder.MyTransactionVH
import java.util.ArrayList

class MyTransactionAdapter():ListAdapter<MyTransactionData, MyTransactionVH>(DIFF_CALLBACK)
{
    private var context: Context? = null
    private val data: ArrayList<MyTransactionData>? = null

    override fun onCreateViewHolder(
        viewGroup: ViewGroup,
        position: Int
    ): MyTransactionVH
    {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.fragment_my_transaction_item, viewGroup, false)
        return MyTransactionVH(viewGroup.context,view)
    }

    override fun onBindViewHolder(holder: MyTransactionVH, position: Int)
    {
        if (holder is MyTransactionVH) holder.populateData(getItem(position),null)
    }

    override fun getItemId(position: Int): Long
    {
        return position.toLong()
    }

    companion object
    {
        private const val TAG = "MyTransactionAdapter"
        private val DIFF_CALLBACK: DiffUtil.ItemCallback<MyTransactionData> =
            object : DiffUtil.ItemCallback<MyTransactionData>()
            {
                override fun areItemsTheSame(
                    oldItem: MyTransactionData,
                    newItem: MyTransactionData
                ): Boolean
                {
                    return oldItem.transactionId == newItem.transactionId
                }

                override fun areContentsTheSame(
                    oldItem: MyTransactionData,
                    newItem: MyTransactionData
                ): Boolean
                {
                    return Integer.toString(oldItem.transactionId) == Integer.toString(newItem.transactionId)
                }
            }
    }
}