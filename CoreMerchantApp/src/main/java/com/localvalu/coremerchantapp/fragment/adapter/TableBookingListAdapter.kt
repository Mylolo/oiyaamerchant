package com.localvalu.coremerchantapp.fragment.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.localvalu.coremerchantapp.R
import com.localvalu.coremerchantapp.data.output.tablebooking.TableBookingList
import com.localvalu.coremerchantapp.fragment.adapter.viewholder.TableBookingListVH
import com.localvalu.coremerchantapp.listeners.TableBookingItemClickListener

class TableBookingListAdapter(private val tableBookingItemClickListener: TableBookingItemClickListener):
    ListAdapter<TableBookingList, TableBookingListVH>(DIFF_CALLBACK),
    TableBookingItemClickListener
{
    private var context: Context? = null


    override fun onAttachedToRecyclerView(recyclerView: RecyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView)
        context = recyclerView.context
    }

    override fun onCreateViewHolder(
        viewGroup: ViewGroup,
        position: Int
    ): TableBookingListVH
    {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.view_item_booking_list, viewGroup, false)
        return TableBookingListVH(viewGroup.context,view,position,tableBookingItemClickListener)
    }

    override fun onBindViewHolder(holder: TableBookingListVH, position: Int)
    {
        if (holder is TableBookingListVH) holder.populateData(getItem(position))
    }

    override fun getItemId(position: Int): Long
    {
        return position.toLong()
    }

    companion object
    {
        private const val TAG = "TableBookingListAdapter"
        private val DIFF_CALLBACK: DiffUtil.ItemCallback<TableBookingList> =
            object : DiffUtil.ItemCallback<TableBookingList>()
            {
                override fun areItemsTheSame(
                    oldItem: TableBookingList,
                    newItem: TableBookingList
                ): Boolean
                {
                    return oldItem.tableBookId == newItem.tableBookId
                }

                override fun areContentsTheSame(
                    oldItem: TableBookingList,
                    newItem: TableBookingList
                ): Boolean
                {
                    return Integer.toString(oldItem.tableBookId.toInt()) == Integer.toString(newItem.tableBookId.toInt())
                }
            }
    }

    override fun onTableBookingItemClicked(position: Int, tableBookingList: TableBookingList?)
    {
        tableBookingItemClickListener.onTableBookingItemClicked(position,tableBookingList)
    }

    override fun onAcceptClicked(position: Int, tableBookingList: TableBookingList?)
    {
        tableBookingItemClickListener.onAcceptClicked(position,tableBookingList)
    }

    override fun onDeclineClicked(position: Int, tableBookingList: TableBookingList?)
    {
        tableBookingItemClickListener.onDeclineClicked(position,tableBookingList)
    }

}