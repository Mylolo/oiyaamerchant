package com.localvalu.coremerchantapp.fragment.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.localvalu.coremerchantapp.R;
import com.localvalu.coremerchantapp.data.output.trace.TraceHistoryReport;
import com.localvalu.coremerchantapp.fragment.adapter.viewholder.trace.VisitHistoryVH;
import com.localvalu.coremerchantapp.handler.DashBoardHandler;

import java.util.ArrayList;
import java.util.List;

public class VisitHistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{

    private final List<TraceHistoryReport> items;
    private final DashBoardHandler dashBoardHandler;
    private Context context;

    public VisitHistoryAdapter(List<TraceHistoryReport> items, DashBoardHandler dashBoardHandler)
    {
        this.items = new ArrayList<>(items);
        this.dashBoardHandler = dashBoardHandler;
    }

    @Override
    public int getItemViewType(int position)
    {
        TraceHistoryReport traceHistoryReport = items.get(position);
        return 0;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        this.context = parent.getContext();

        switch (viewType)
        {
            case 0:
            {
                View view = LayoutInflater.from(context).inflate(R.layout.view_item_visit_history, parent, false);
                return new VisitHistoryVH(context, view);
            }

            default:
            {
                return null;
            }
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position)
    {
        holder.setIsRecyclable(false);

        if (holder instanceof VisitHistoryVH)
            ((VisitHistoryVH) holder).populateData(items.get(position), dashBoardHandler);
    }

    @Override
    public int getItemCount()
    {
        return this.items.size();
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    public TraceHistoryReport removeItem(int position)
    {
        TraceHistoryReport traceHistoryReport = this.items.remove(position);
        notifyItemRemoved(position);

        return traceHistoryReport;
    }

    public void addItem(int position, TraceHistoryReport traceHistoryReport)
    {
        this.items.add(position, traceHistoryReport);
        notifyItemInserted(position);
    }

    public void addItem(TraceHistoryReport traceHistoryReport)
    {
        this.items.add(traceHistoryReport);
        notifyItemInserted(this.items.size());
    }

    public void addItems(List<TraceHistoryReport> items)
    {
        this.items.clear();
        this.items.addAll(items);
        notifyDataSetChanged();
    }

    public void moveItem(int fromPosition, int toPosition)
    {
        final TraceHistoryReport traceHistoryReport = this.items.remove(fromPosition);

        this.items.add(toPosition, traceHistoryReport);
        notifyItemMoved(fromPosition, toPosition);
    }
}
