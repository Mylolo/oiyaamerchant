package com.localvalu.coremerchantapp.fragment.adapter.driver

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.localvalu.coremerchantapp.R
import com.localvalu.coremerchantapp.data.output.driver.DriverList
import com.localvalu.coremerchantapp.databinding.ViewItemDriverListBinding
import java.lang.StringBuilder
import java.util.*

class DriverListAdapter : ListAdapter<DriverList, DriverListAdapter.DriverListViewHolder>(DriverListDC())
{
    var driverListItemClickListener: DriverListItemClickListener? = null

    class DriverListViewHolder(val binding:ViewItemDriverListBinding) : RecyclerView.ViewHolder(binding.root)

    private lateinit var context: Context;

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView)
        context = recyclerView.context;
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) : DriverListViewHolder
    {
        val binding:ViewItemDriverListBinding =
            ViewItemDriverListBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return DriverListViewHolder(binding)
    }

    override fun onBindViewHolder(holder: DriverListViewHolder, position: Int)
    {
        val driverList = getItem(position)
        holder.binding.driverList = driverList
        holder.binding.driverListItemClickListener = driverListItemClickListener
        holder.binding.position = position
        holder.binding.executePendingBindings()
    }

    fun swapData(data: List<DriverList>)
    {
        submitList(data.toMutableList())
    }

    interface DriverListItemClickListener
    {
        fun driverListItemClicked(driverList: DriverList, position: Int)
    }

    private class DriverListDC : DiffUtil.ItemCallback<DriverList>()
    {
        override fun areItemsTheSame(oldItem: DriverList, newItem: DriverList) =
            oldItem.driverid == newItem.driverid

        override fun areContentsTheSame(
            oldItem: DriverList,
            newItem: DriverList
        ) = oldItem == newItem
    }
}
