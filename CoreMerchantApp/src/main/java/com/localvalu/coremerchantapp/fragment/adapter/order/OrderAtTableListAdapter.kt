package com.localvalu.coremerchantapp.fragment.adapter.order

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.localvalu.coremerchantapp.R
import com.localvalu.coremerchantapp.data.output.myappointments.MyAppointmentList
import com.localvalu.coremerchantapp.data.output.order.OrderListData
import com.localvalu.coremerchantapp.fragment.adapter.viewholder.MyAppointmentListVH
import com.localvalu.coremerchantapp.handler.DashBoardHandler
import com.localvalu.coremerchantapp.listeners.OrderItemClickListener
import com.localvalu.coremerchantapp.fragment.adapter.viewholder.orders.OrderAtTableListVH
import java.util.ArrayList

class OrderAtTableListAdapter(private val orderItemClickListener: OrderItemClickListener) :
    ListAdapter<OrderListData, OrderAtTableListVH>(DIFF_CALLBACK), OrderItemClickListener
{
    private var context: Context? = null
    private val TAG = "OrderAtTableListAdapter"

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView)
        context = recyclerView.context
    }

    override fun onCreateViewHolder(
        viewGroup: ViewGroup,
        position: Int
    ): OrderAtTableListVH
    {
        Log.d(TAG, "onCreateViewHolder")
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.view_item_order_list, viewGroup, false)
        return OrderAtTableListVH(viewGroup.context, view, position, orderItemClickListener)
    }

    override fun onBindViewHolder(holder: OrderAtTableListVH, position: Int)
    {
        Log.d(TAG, "onBindViewHolder")
        if (holder is OrderAtTableListVH) holder.populateData(getItem(position))
    }

    override fun getItemId(position: Int): Long
    {
        return position.toLong()
    }

    override fun getItemCount(): Int
    {
        return super.getItemCount()
    }

    companion object
    {
        private const val TAG = "OrderAtTableListAdapter"
        private val DIFF_CALLBACK: DiffUtil.ItemCallback<OrderListData> =
            object : DiffUtil.ItemCallback<OrderListData>()
            {
                override fun areItemsTheSame(
                    oldItem: OrderListData,
                    newItem: OrderListData
                ): Boolean
                {
                    return oldItem.orderId == newItem.orderId
                }

                override fun areContentsTheSame(
                    oldItem: OrderListData,
                    newItem: OrderListData
                ): Boolean
                {
                    return Integer.toString(oldItem.orderId.toInt()) == Integer.toString(newItem.orderId.toInt())
                }
            }
    }

    override fun onOrderItemClicked(position: Int, orderListData: OrderListData?)
    {
        orderItemClickListener.onOrderItemClicked(position, orderListData)
    }

}