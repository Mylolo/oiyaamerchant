package com.localvalu.coremerchantapp.fragment.adapter.order

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.localvalu.coremerchantapp.R
import com.localvalu.coremerchantapp.data.output.myappointments.MyAppointmentList
import com.localvalu.coremerchantapp.data.output.order.OrderListData
import com.localvalu.coremerchantapp.fragment.adapter.viewholder.MyAppointmentListVH
import com.localvalu.coremerchantapp.handler.DashBoardHandler
import com.localvalu.coremerchantapp.listeners.OrderItemClickListener
import com.localvalu.coremerchantapp.fragment.adapter.viewholder.orders.OrderListVH
import java.util.ArrayList

class OrderListAdapter(private val orderItemClickListener: OrderItemClickListener):
    ListAdapter<OrderListData, OrderListVH>(DIFF_CALLBACK),OrderItemClickListener
{
    private var context: Context? = null
    private val TAG = "OrderListAdapter"

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView)
        context = recyclerView.context
    }

    override fun onCreateViewHolder(
        viewGroup: ViewGroup,
        position: Int
    ): OrderListVH
    {
        Log.d(TAG,"onCreateViewHolder")
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.view_item_order_list, viewGroup, false)
        return OrderListVH(viewGroup.context,view,position,orderItemClickListener)
    }

    override fun onBindViewHolder(holder: OrderListVH, position: Int)
    {
        Log.d(TAG,"onBindViewHolder")
        if (holder is OrderListVH) holder.populateData(getItem(position))
    }

    override fun getItemId(position: Int): Long
    {
        return position.toLong()
    }

    override fun getItemCount(): Int
    {
        return super.getItemCount()
    }

    companion object
    {
        private const val TAG = "OrderListAdapter"
        private val DIFF_CALLBACK: DiffUtil.ItemCallback<OrderListData> =
            object : DiffUtil.ItemCallback<OrderListData>()
            {
                override fun areItemsTheSame(
                    oldItem: OrderListData,
                    newItem: OrderListData
                ): Boolean
                {
                    return oldItem.orderId == newItem.orderId
                }

                override fun areContentsTheSame(
                    oldItem: OrderListData,
                    newItem: OrderListData
                ): Boolean
                {
                    return oldItem.equals(newItem) && oldItem.status.equals(newItem.status)
                }
            }
    }

    override fun onOrderItemClicked(position: Int, orderListData: OrderListData?)
    {
        orderItemClickListener.onOrderItemClicked(position,orderListData)
    }

}