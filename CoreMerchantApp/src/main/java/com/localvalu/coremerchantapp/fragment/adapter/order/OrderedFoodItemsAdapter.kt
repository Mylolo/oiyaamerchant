package com.localvalu.coremerchantapp.fragment.adapter.order

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.localvalu.coremerchantapp.R
import com.localvalu.coremerchantapp.data.output.FoodItem
import com.localvalu.coremerchantapp.fragment.adapter.viewholder.orders.OrderFoodItemsVH

class OrderedFoodItemsAdapter() :
    ListAdapter<FoodItem, OrderFoodItemsVH>(OrderedFoodItemsAdapter.DIFF_CALLBACK)
{
    private var context: Context? = null


    override fun onCreateViewHolder(
        viewGroup: ViewGroup,
        position: Int
    ): OrderFoodItemsVH
    {
        Log.d(TAG, "onCreateViewHolder")
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.view_ordered_items, viewGroup, false)
        return OrderFoodItemsVH(viewGroup.context, view)
    }

    override fun onBindViewHolder(holder: OrderFoodItemsVH, position: Int)
    {
        Log.d(TAG, "onBindViewHolder")
        if (holder is OrderFoodItemsVH) holder.populateData(getItem(position))
    }

    override fun getItemId(position: Int): Long
    {
        return position.toLong()
    }

    companion object
    {
        private const val TAG = "OrderedFoodItemsAdapter"
        private val DIFF_CALLBACK: DiffUtil.ItemCallback<FoodItem> =
            object : DiffUtil.ItemCallback<FoodItem>()
            {
                override fun areItemsTheSame(
                    oldItem: FoodItem,
                    newItem: FoodItem
                ): Boolean
                {
                    return oldItem.itemName == newItem.itemName &&
                            oldItem.foodDesc == newItem.itemName &&
                            oldItem.quantity == newItem.quantity &&
                            oldItem.singleQuantityPrice == newItem.singleQuantityPrice &&
                            oldItem.itemTotalPrice == newItem.itemTotalPrice &&
                            oldItem.foodDesc == newItem.foodDesc &&
                            oldItem.foodType == newItem.foodType &&
                            oldItem.foodPic == newItem.foodPic &&
                            oldItem.sizeName == newItem.sizeName &&
                            oldItem.sizeId == newItem.sizeId

                }

                override fun areContentsTheSame(
                    oldItem: FoodItem,
                    newItem: FoodItem
                ): Boolean
                {
                    return oldItem.itemName == newItem.itemName &&
                            oldItem.foodDesc == newItem.itemName &&
                            oldItem.quantity == newItem.quantity &&
                            oldItem.singleQuantityPrice == newItem.singleQuantityPrice &&
                            oldItem.itemTotalPrice == newItem.itemTotalPrice &&
                            oldItem.foodDesc == newItem.foodDesc &&
                            oldItem.foodType == newItem.foodType &&
                            oldItem.foodPic == newItem.foodPic &&
                            oldItem.sizeName == newItem.sizeName &&
                            oldItem.sizeId == newItem.sizeId
                }
            }
    }

}