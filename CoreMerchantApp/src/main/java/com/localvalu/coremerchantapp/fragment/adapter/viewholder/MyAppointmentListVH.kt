package com.localvalu.coremerchantapp.fragment.adapter.viewholder

import android.content.Context
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.button.MaterialButton
import com.localvalu.coremerchantapp.R
import com.localvalu.coremerchantapp.data.output.myappointments.MyAppointmentList
import com.localvalu.coremerchantapp.handler.DashBoardHandler
import com.localvalu.coremerchantapp.data.output.myappointments.MyAppointmentStatusChangeResult
import com.utils.util.DateTimeUtil
import com.localvalu.coremerchantapp.fragment.adapter.viewholder.MyAppointmentListVH
import com.localvalu.coremerchantapp.listeners.MyAppointmentItemClickListener
import com.utils.base.Task
import java.lang.Exception
import java.lang.StringBuilder
import java.text.SimpleDateFormat

/**
 * Status 3 -- Waiting for confirm
 * Status 2 -- Declined
 * Status 1 -- Delivered
 * Status 0 -- Accept
 * Status 6 -- Order Cancelled
 * Status 5 -- Booking Table Cancelled
 * Status 20 -- Print
 */
class MyAppointmentListVH(private val context: Context,
                          view: View,
                          position:Int,
                          myAppointmentItemClickListener: MyAppointmentItemClickListener) : RecyclerView.ViewHolder(view)
{
    private val tvCustomerName: AppCompatTextView = view.findViewById(R.id.tvCustomerName)
    private val tvSlotId: AppCompatTextView = view.findViewById(R.id.tvSlotId)
    private val tvPreferredDate: AppCompatTextView = view.findViewById(R.id.tvPreferredDate)
    private val tvStatus: AppCompatTextView = view.findViewById(R.id.tvStatus)
    private val btnAccept: MaterialButton = view.findViewById(R.id.btnAccept)
    private val btnReject: MaterialButton = view.findViewById(R.id.btnReject)
    private val progressBar: ProgressBar = view.findViewById(R.id.progressBar)
    private var myAppointmentList: MyAppointmentList? = null

    var _OnClickListener = View.OnClickListener { v ->

        val id = v.id

        if (id == R.id.btnAccept)
        {
            myAppointmentItemClickListener.onMyAppointmentAccepted(position,this.myAppointmentList)
        }
        else if (id == R.id.btnReject)
        {
            myAppointmentItemClickListener.onMyAppointmentDeclined(position,this.myAppointmentList)
        }
    }

    fun populateData(myAppointmentList: MyAppointmentList)
    {
        this.myAppointmentList = myAppointmentList
        btnAccept.setOnClickListener(_OnClickListener)
        btnReject.setOnClickListener(_OnClickListener)
        setData()
    }

    private fun setData()
    {
        val strSlotId = StringBuilder()
        strSlotId.append(context.getString(R.string.lbl_slot_id_colon))
        strSlotId.append(" ").append(myAppointmentList!!.slotId)
        tvSlotId.text = strSlotId.toString()
        tvCustomerName.text = myAppointmentList!!.userName + ""
        if(myAppointmentList?.isUpdating == true)
        {
            progressBar.visibility = View.VISIBLE
            btnAccept.isEnabled = false
            btnReject.isEnabled = false
        }
        else
        {
            progressBar.visibility=View.GONE
            btnReject.isEnabled=true
            btnAccept.isEnabled=true
        }
        val strPreferredDate = StringBuilder()
        strPreferredDate.append(context.getString(R.string.lbl_preferred_date)).append(" : ")
        val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        try
        {
            val strDateTime = StringBuilder()
            strDateTime.append(myAppointmentList!!.preferredDate).append(" ")
            strDateTime.append(myAppointmentList!!.preferredTime)
            val orderDate = dateFormat.parse(strDateTime.toString())
            val strDate = DateTimeUtil.getFormattedDate(
                orderDate.time,
                DateTimeUtil.EEE_dd_MMM_yyyy_at_hh_mm_a
            )
            strPreferredDate.append(strDate)
        }
        catch (ex: Exception)
        {
            ex.printStackTrace()
        }
        tvPreferredDate.text = strPreferredDate.toString()
        if (myAppointmentList!!.status != null)
        {
            if (myAppointmentList!!.status.length > 0)
            {
                try
                {
                    val status = myAppointmentList!!.status.toInt()
                    setStatus(status)
                } catch (ex: Exception)
                {
                }
            }
        }
    }

    private fun setStatus(status: Int)
    {
        when (status)
        {
            0 ->
            {
                tvStatus.text = context.getString(R.string.lbl_accepted)
                tvStatus.setTextColor(context.resources.getColor(R.color.greenDark))
                showHideControls(false)
            }
            2 ->
            {
                tvStatus.setText(R.string.lbl_declined)
                tvStatus.setTextColor(context.resources.getColor(R.color.redDark))
                showHideControls(false)
            }
            3 ->
            {
                tvStatus.setText(R.string.lbl_pending)
                tvStatus.setTextColor(context.resources.getColor(R.color.orangeDark))
                showHideControls(true)
            }
            5 ->
            {
                tvStatus.setText(R.string.lbl_cancelled)
                tvStatus.setTextColor(context.resources.getColor(R.color.redDark))
                showHideControls(false)
            }
        }
    }

    private fun showHideControls(show: Boolean)
    {
        if (show)
        {
            btnAccept.visibility = View.VISIBLE
            btnReject.visibility = View.VISIBLE
        } else
        {
            btnAccept.visibility = View.GONE
            btnReject.visibility = View.GONE
        }
    }

    companion object
    {
        private val TAG = MyAppointmentListVH::class.java.simpleName
    }

}