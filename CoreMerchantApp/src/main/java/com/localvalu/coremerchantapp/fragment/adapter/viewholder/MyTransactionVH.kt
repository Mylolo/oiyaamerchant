package com.localvalu.coremerchantapp.fragment.adapter.viewholder

import android.content.Context
import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.localvalu.coremerchantapp.R
import com.localvalu.coremerchantapp.data.output.MyTransactionData
import com.localvalu.coremerchantapp.handler.DashBoardHandler
import com.utils.util.DateTimeUtil
import com.localvalu.coremerchantapp.fragment.adapter.viewholder.MyTransactionVH
import java.lang.Exception
import java.text.SimpleDateFormat

class MyTransactionVH(private val context: Context?, view: View) : RecyclerView.ViewHolder(view),
    View.OnClickListener
{
    private val tvDate: TextView
    private val tvAmount: TextView
    private var myTransactionData: MyTransactionData? = null

    override fun onClick(view: View)
    {
        val id = view.id
        when (id)
        {
        }
    }

    fun populateData(myTransactionData: MyTransactionData, dashBoardHandler: DashBoardHandler?)
    {
        this.myTransactionData = myTransactionData
        val dateFormat = SimpleDateFormat("yyyy-MM-dd")
        try
        {
            val orderDate = dateFormat.parse(myTransactionData.transactionDate)
            val strDate = DateTimeUtil.getFormattedDate(orderDate.time, DateTimeUtil.dd_MM_yyyy)
            tvDate.text = strDate
        } catch (ex: Exception)
        {
            ex.printStackTrace()
        }
        tvAmount.text = context?.getString(R.string.symbol_pound) + " " + String.format(
            "%,.2f",
            myTransactionData.saleAmount
        )
    }

    companion object
    {
        private val TAG = MyTransactionVH::class.java.simpleName
    }

    init
    {
        tvAmount = view.findViewById(R.id.tvAmount)
        tvDate = view.findViewById(R.id.tvDate)
    }
}