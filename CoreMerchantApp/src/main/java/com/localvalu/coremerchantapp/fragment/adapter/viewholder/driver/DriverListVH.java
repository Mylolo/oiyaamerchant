package com.localvalu.coremerchantapp.fragment.adapter.viewholder.driver;

import android.content.Context;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.localvalu.coremerchantapp.R;
import com.localvalu.coremerchantapp.data.output.driverOld.DriverDetails;
import com.localvalu.coremerchantapp.data.output.driverOld.DriverNotificationResult;
import com.localvalu.coremerchantapp.data.output.order.OrderDetails;
import com.localvalu.coremerchantapp.handler.DashBoardHandler;
import com.localvalu.coremerchantapp.utils.AppConstants;
import com.utils.base.Task;
import com.utils.helper.preference.PreferenceFactory;
import com.utils.validator.Validator;

import static com.localvalu.coremerchantapp.LocalValuConstants.PREF_RETAILER_ID;

public class DriverListVH extends RecyclerView.ViewHolder implements View.OnClickListener
{
    private static final String TAG = DriverListVH.class.getSimpleName();
    private Context context;
    private AppCompatTextView tvDriverName;
    private DriverDetails driverDetails;
    private DashBoardHandler dashBoardHandler;
    private OrderDetails orderDetails;

    public DriverListVH(Context context, View view)
    {
        super(view);
        this.context = context;
        tvDriverName = view.findViewById(R.id.tvDriverName);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {

        }
    }

    public void populateData(DriverDetails driverDetails, final DashBoardHandler dashBoardHandler, OrderDetails orderDetails)
    {
        this.driverDetails = driverDetails;
        this.dashBoardHandler = dashBoardHandler;
        this.orderDetails = orderDetails;
        tvDriverName.setText(driverDetails.getDrivername());
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doNotifyToUsers(Integer.parseInt(driverDetails.getDriverid()));
            }
        });
    }

    private void doNotifyToUsers(int driverId)
    {
        String strRetailerId = PreferenceFactory.getInstance().getString(PREF_RETAILER_ID);
        int retailerId = Integer.parseInt(strRetailerId);
        int orderId = Integer.parseInt(orderDetails.getOrderId());
        dashBoardHandler.doNotifyToDrivers(driverId, orderId, retailerId, new Task<DriverNotificationResult>()
        {
            @Override
            public void onSuccess(DriverNotificationResult result)
            {
                if (Validator.isValid(result.getDriverStatus()))
                {
                    Toast.makeText(context, context.getString(R.string.msg_driver_has_been_assigned), Toast.LENGTH_LONG).show();
                    switch (orderDetails.getOrderOption()) {
                        case AppConstants.ORDER_OPTION_DELIVERY:
                            dashBoardHandler.onBackButtonPressed();
                            dashBoardHandler.orderDetails(orderDetails.getOrderId());
                            break;
                    }
                    //showTimer();
                }
                else
                {
                }
            }

            @Override
            public void onError(String message) {

            }
        });
    }

}


