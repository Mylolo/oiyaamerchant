package com.localvalu.coremerchantapp.fragment.adapter.viewholder.orders;

import android.content.Context;
import android.view.View;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.localvalu.coremerchantapp.R;
import com.localvalu.coremerchantapp.data.output.FoodItem;
import com.localvalu.coremerchantapp.utils.AppUtils;

public class OrderFoodItemsPrintVH extends RecyclerView.ViewHolder implements View.OnClickListener
{

    private static final String TAG = OrderFoodItemsPrintVH.class.getSimpleName();

    private Context context;
    private AppCompatTextView tvAmount,tvPrice, tvQuantity, tvFoodName;

    public OrderFoodItemsPrintVH(Context context, View view)
    {
        super(view);
        this.context = context;

        tvPrice = view.findViewById(R.id.tvPrice);
        tvQuantity = view.findViewById(R.id.tvQuantity);
        tvFoodName = view.findViewById(R.id.tvFoodName);
        tvAmount = view.findViewById(R.id.tvAmount);
    }

    @Override
    public void onClick(View view)
    {
        int id = view.getId();

        switch (id)
        {

        }
    }

    public void populateData(FoodItem foodItem)
    {
        StringBuilder strFoodItem = new StringBuilder();
        strFoodItem.append(AppUtils.capitalizeFirstLetterInEachWord(foodItem.getItemName()));
        strFoodItem.append("(").append(foodItem.getSizeName()).append(")");
        tvFoodName.setText(strFoodItem.toString());
        tvQuantity.setText(foodItem.getQuantity());

        try
        {
            double price = Double.parseDouble(foodItem.getSingleQuantityPrice());
            tvPrice.setText(context.getString(R.string.symbol_pound) + " " + String.format("%,.2f", price));

            double quantity = Double.parseDouble(foodItem.getQuantity());
            double amount = quantity * price;
            tvAmount.setText(context.getString(R.string.symbol_pound) + " " + String.format("%,.2f", amount));
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

}
