package com.localvalu.coremerchantapp.fragment.adapter.viewholder.orders

import android.content.Context
import android.view.View
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.localvalu.coremerchantapp.R
import com.utils.view.rippleview.CustomRippleView
import com.localvalu.coremerchantapp.data.output.FoodItem
import com.localvalu.coremerchantapp.handler.DashBoardHandler
import com.localvalu.coremerchantapp.utils.AppUtils
import com.localvalu.coremerchantapp.fragment.adapter.viewholder.orders.OrderFoodItemsVH
import java.lang.Exception
import java.lang.StringBuilder

class OrderFoodItemsVH(private val context: Context, view: View) : RecyclerView.ViewHolder(view),
    View.OnClickListener
{
    private val tvAmount: AppCompatTextView = view.findViewById(R.id.tvAmount)
    private val tvPrice: AppCompatTextView = view.findViewById(R.id.tvPrice)
    private val tvQuantity: AppCompatTextView = view.findViewById(R.id.tvQuantity)
    private val tvFoodName: AppCompatTextView = view.findViewById(R.id.tvFoodName)
    //private val rippleContent: CustomRippleView = view.findViewById(R.id.rippleContent)

    //private AppCompatImageView ivBusinessLogo;
    override fun onClick(view: View)
    {
        val id = view.id
        when (id)
        {
        }
    }

    fun populateData(foodItem: FoodItem)
    {
        val strFoodItem = StringBuilder()
        strFoodItem.append(AppUtils.capitalizeFirstLetterInEachWord(foodItem.itemName))
        strFoodItem.append("(").append(foodItem.sizeName).append(")")
        tvFoodName.text = strFoodItem.toString()
        tvQuantity.text = foodItem.quantity
        try
        {
            val price = foodItem.singleQuantityPrice.toDouble()
            tvPrice.text =
                context.getString(R.string.symbol_pound) + " " + String.format("%,.2f", price)
            val quantity = foodItem.quantity.toDouble()
            val amount = quantity * price
            tvAmount.text =
                context.getString(R.string.symbol_pound) + " " + String.format("%,.2f", amount)
        }
        catch (ex: Exception)
        {
            ex.printStackTrace()
        }
    }

    companion object
    {
        private val TAG = OrderFoodItemsVH::class.java.simpleName
    }

    init
    {
        //ivBusinessLogo = view.findViewById(R.id.ivBusinessLogo);
    }
}