package com.localvalu.coremerchantapp.fragment.adapter.viewholder.orders

import android.content.Context
import android.view.View
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.localvalu.coremerchantapp.R
import com.localvalu.coremerchantapp.data.output.order.OrderListData
import com.localvalu.coremerchantapp.handler.DashBoardHandler
import com.utils.util.DateTimeUtil
import com.localvalu.coremerchantapp.utils.AppConstants
import com.localvalu.coremerchantapp.fragment.adapter.viewholder.orders.OrderListVH
import com.localvalu.coremerchantapp.listeners.MyAppointmentItemClickListener
import com.localvalu.coremerchantapp.listeners.OrderItemClickListener
import com.utils.view.button.Button
import java.lang.Exception
import java.lang.StringBuilder
import java.text.SimpleDateFormat

/**
 * Status 3 -- Waiting for confirm
 * Status 2 -- Declined
 * Status 1 -- Delivered
 * Status 0 -- Accept
 * Status 6 -- Order Cancelled
 * Status 5 -- Booking Table Cancelled
 * Status 20 -- Print
 */
class OrderListVH(
    private val context: Context,
    view: View,
    position: Int, orderItemClickListener: OrderItemClickListener
) : RecyclerView.ViewHolder(view)
{
    private val tvOrderId: AppCompatTextView = view.findViewById(R.id.tvOrderId)
    private val tvCustomerName: AppCompatTextView = view.findViewById(R.id.tvCustomerName)
    private val tvOrderDate: AppCompatTextView = view.findViewById(R.id.tvOrderDate)
    private val tvFinalAmount: AppCompatTextView = view.findViewById(R.id.tvFinalAmount)
    private val tvStatus: AppCompatTextView = view.findViewById(R.id.tvStatus)
    private var orderListData: OrderListData? = null

    init
    {
        view.setOnClickListener{
            orderItemClickListener.onOrderItemClicked(position,orderListData)
        }
    }

    val onClickListener = View.OnClickListener { v ->
        val id = v.id
    }

    fun populateData(orderListData: OrderListData)
    {
        this.orderListData = orderListData
        val strOrderId = StringBuilder()
        strOrderId.append(context.getString(R.string.lbl_order_no_colon)).append(" ")
        strOrderId.append(orderListData.orderId)
        tvOrderId.text = strOrderId.toString()
        tvCustomerName.text = orderListData.customerName
        val dateFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
        try
        {
            val orderDate = dateFormat.parse(orderListData.createdDate)
            val strDate = DateTimeUtil.getFormattedDate(orderDate.time, DateTimeUtil.dd_MM_yyyy)
            val strTime = DateTimeUtil.getFormattedDate(orderDate.time, DateTimeUtil.hh_mm_a)
            val strDateTime = StringBuilder()
            strDateTime.append(strDate).append(" ").append(strTime)
            tvOrderDate.text = strDateTime.toString()
        } catch (ex: Exception)
        {
            ex.printStackTrace()
        }
        tvFinalAmount.text = context.getString(R.string.symbol_pound) + " " + String.format(
            "%,.2f",
            orderListData.finalOrderTotal.toFloat()
        )
        if (orderListData.status != null)
        {
            if (orderListData.status.isNotEmpty())
            {
                try
                {
                    setStatus(orderListData.status)
                }
                catch (ex: Exception)
                {
                    ex.printStackTrace()
                }
            }
        }
    }

    private fun setStatus(status: String)
    {
        when (status)
        {
            AppConstants.ACCEPT ->
            {
                tvStatus.text = context.getString(R.string.lbl_accepted)
                tvStatus.setTextColor(context.resources.getColor(R.color.greenDark))
            }
            AppConstants.DELIVERED ->
            {
                tvStatus.text = context.getString(R.string.lbl_delivered)
                tvStatus.setTextColor(context.resources.getColor(R.color.greenDark))
            }
            AppConstants.DECLINED ->
            {
                tvStatus.setText(R.string.lbl_declined)
                tvStatus.setTextColor(context.resources.getColor(R.color.redDark))
            }
            AppConstants.ORDER_CANCELLED ->
            {
                tvStatus.setText(R.string.lbl_cancelled)
                tvStatus.setTextColor(context.resources.getColor(R.color.redDark))
            }
            AppConstants.PENDING ->
            {
                tvStatus.setText(R.string.lbl_pending)
                tvStatus.setTextColor(context.resources.getColor(R.color.orangeDark))
            }
        }
    }

    companion object
    {
        private val TAG = OrderListVH::class.java.simpleName
    }

}