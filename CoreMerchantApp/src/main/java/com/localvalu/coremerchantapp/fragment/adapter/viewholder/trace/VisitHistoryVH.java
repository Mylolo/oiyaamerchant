package com.localvalu.coremerchantapp.fragment.adapter.viewholder.trace;

import android.content.Context;
import android.view.View;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.localvalu.coremerchantapp.R;
import com.localvalu.coremerchantapp.data.output.trace.TraceHistoryReport;
import com.localvalu.coremerchantapp.handler.DashBoardHandler;

import java.text.SimpleDateFormat;
import java.util.Date;

public class VisitHistoryVH extends RecyclerView.ViewHolder implements View.OnClickListener
{
    private static final String TAG = VisitHistoryVH.class.getSimpleName();

    private Context context;
    private AppCompatTextView tvCustomerName,tvEmail,tvMobile,tvVisitDate;
    private TraceHistoryReport traceHistoryReport;
    private DashBoardHandler dashBoardHandler;

    public VisitHistoryVH(Context context, View view)
    {
        super(view);
        this.context = context;

        tvCustomerName = view.findViewById(R.id.tvCustomerName);
        tvEmail = view.findViewById(R.id.tvEmail);
        tvMobile = view.findViewById(R.id.tvMobile);
        tvVisitDate = view.findViewById(R.id.tvVisitDate);
    }

    @Override
    public void onClick(View view)
    {
        int id = view.getId();

        switch (id)
        {

        }
    }

    public void populateData(TraceHistoryReport traceHistoryReport, final DashBoardHandler dashBoardHandler)
    {
        this.traceHistoryReport = traceHistoryReport;
        this.dashBoardHandler = dashBoardHandler;

        tvCustomerName.setText(traceHistoryReport.getName());
        tvEmail.setText(traceHistoryReport.getEmail());
        tvMobile.setText(traceHistoryReport.getMobileNumber());
        SimpleDateFormat formatInput = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
        try
        {
            Date visitDate = formatInput.parse(traceHistoryReport.getVisitTime());
            SimpleDateFormat formatOutput = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
            String strDateTime = formatOutput.format(visitDate);
            tvVisitDate.setText(strDateTime);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

    }

}
