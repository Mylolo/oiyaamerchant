package com.localvalu.coremerchantapp.fragment.driver

import android.content.Context
import android.os.Bundle
import android.os.Parcelable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.localvalu.coremerchantapp.BuildConfig
import com.localvalu.coremerchantapp.LocalValuFragment
import com.localvalu.coremerchantapp.R
import com.localvalu.coremerchantapp.data.input.NotifyToDriversInput
import com.localvalu.coremerchantapp.data.input.PoolDriverListInput
import com.localvalu.coremerchantapp.data.output.SignInResult
import com.localvalu.coremerchantapp.data.output.driver.DriverList
import com.localvalu.coremerchantapp.data.output.driver.OwnDriversListRequest
import com.localvalu.coremerchantapp.data.output.order.OrderDetails
import com.localvalu.coremerchantapp.databinding.FragmentDriverSelectionBinding
import com.localvalu.coremerchantapp.fragment.adapter.driver.DriverListAdapter
import com.localvalu.coremerchantapp.generic.Resource
import com.localvalu.coremerchantapp.handler.DashBoardHandler
import com.localvalu.coremerchantapp.utils.AppConstants
import com.localvalu.coremerchantapp.utils.AppUtils
import com.localvalu.coremerchantapp.viewmodel.DriverListViewModel
import com.utils.base.Task
import com.utils.helper.preference.PreferenceFactory
import com.utils.helper.recyclerview.RVItemAnimator
import com.utils.validator.Validator
import java.util.*

class DriverListFragment : LocalValuFragment(), View.OnClickListener,DriverListAdapter.DriverListItemClickListener
{
    private val TAG = "DriverListFragment"
    private var driverListAdapter: DriverListAdapter? = null
    private var dashBoardHandler: DashBoardHandler? = null
    private val selectedPosition = -1
    private val selectedItem: DriverList? = null
    private var ownDriverList: List<DriverList>? = null
    private var binding: FragmentDriverSelectionBinding? = null
    private var orderDetails: OrderDetails? = null
    private var signInResult: SignInResult? = null
    private val driverListViewModel by viewModels<DriverListViewModel>()
    private var poolDriversListObserved=false
    private var ownDriversListObserved=false
    private var notifyDriversObserved=false
    private var responseCount:Int = 0;
    private var requestCount = 0;
    //Fetch driver ids and notify
    private val driverIds:MutableList<Int> = mutableListOf()


    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        if (arguments != null)
        {
            orderDetails = arguments?.getParcelable<Parcelable>(AppUtils.BUNDLE_ORDER_DETAILS) as OrderDetails?
        }
        try
        {
            val strSignInResult = PreferenceFactory.getInstance().getString(PREF_SIGN_IN_RESULT)
            if (strSignInResult != null)
            {
                if (strSignInResult != "")
                {
                    val gson = Gson()
                    signInResult = gson.fromJson(strSignInResult, SignInResult::class.java)
                }
            }
            if (signInResult != null)
            {
                if (BuildConfig.DEBUG)
                {
                    Log.d(TAG, "signInResult->" + signInResult!!.driverType)
                }
            }
        }
        catch (ex: Exception)
        {
            ex.printStackTrace()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        binding = FragmentDriverSelectionBinding.inflate(inflater, container, false)
        setHasOptionsMenu(true)
        retainInstance = true
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        setProperties()
    }

    private fun setProperties()
    {
        setUI()
        val linearLayoutManager = LinearLayoutManager(context)
        binding!!.rvDriverList.layoutManager = linearLayoutManager
        binding!!.rvDriverList.itemAnimator = RVItemAnimator()
        binding!!.rvDriverList.addItemDecoration(DividerItemDecoration(context, linearLayoutManager.orientation))
        binding!!.rvDriverList.setHasFixedSize(true)
        driverListAdapter = DriverListAdapter()
        driverListAdapter!!.driverListItemClickListener = this
        binding!!.rvDriverList.adapter = driverListAdapter
        binding!!.btnRedirectToPoolDriver.setOnClickListener {
            if (signInResult != null)
            {
                if (signInResult!!.driverType != null)
                {
                    if (signInResult!!.driverType?.toInt() == AppConstants.POOL_DRIVERS)
                    {
                        //Call api
                        val poolDriverListInput   = PoolDriverListInput()
                        poolDriverListInput.poolName = signInResult?.poolName
                        driverListViewModel.setPoolDriverRequest(poolDriverListInput)
                    }
                }
            }
        }
        subscribeObservers()
    }

    private fun setUI()
    {
        if(signInResult?.driverType!=null)
        {
            when(signInResult?.driverType?.toInt())
            {
                AppConstants.OWN_DRIVERS->
                {
                    binding!!.btnRedirectToPoolDriver.visibility = View.GONE
                    binding!!.progressBar.visibility = View.GONE
                    binding!!.cnlNotifyProgressBar.visibility = View.GONE
                    binding!!.cnlErrorEmptyView.visibility = View.GONE
                    doGetOwnDrivers()
                }
                AppConstants.POOL_DRIVERS ->
                {
                    binding!!.rvDriverList.visibility = View.GONE
                    binding!!.cnlErrorEmptyView.visibility = View.GONE
                    binding!!.progressBar.visibility = View.GONE
                    binding!!.cnlNotifyProgressBar.visibility=View.GONE
                    binding!!.btnRedirectToPoolDriver.visibility=View.VISIBLE
                }
                AppConstants.NO_DRIVERS ->
                {
                    showEmptyView(getString(R.string.lbl_no_drivers_found))
                }
            }
        }
    }

    private fun subscribeObservers()
    {
        observeOwnDriversList()
        observePoolDriversList()
        observeNotifyDrivers()
        observeSingleNotifyDrivers()
    }

    private fun doGetOwnDrivers()
    {
        //Call api
        val ownDriversListRequest   = OwnDriversListRequest()
        ownDriversListRequest.retailerId = signInResult?.retailerId.toString()
        ownDriversListRequest.types = "drivers"
        driverListViewModel.setOwnDriverRequest(ownDriversListRequest)
    }

    private fun notifyPoolDriver(driverID:Int)
    {
        val notifyToDriversInput:NotifyToDriversInput = NotifyToDriversInput()
        notifyToDriversInput.driverId = driverID
        notifyToDriversInput.orderId = Integer.parseInt(orderDetails!!.orderId)
        notifyToDriversInput.retailerId = signInResult?.retailerId!!
        driverListViewModel.setNotifySingleDriverRequest(notifyToDriversInput)
    }

    private fun observeOwnDriversList()
    {
        driverListViewModel.ownDriversListOutput.observe(viewLifecycleOwner, androidx.lifecycle.Observer { result->
            when(result)
            {
                is Resource.Loading ->
                {
                    showProgress()
                    ownDriversListObserved = true
                }

                is Resource.Success ->
                {
                    if(ownDriversListObserved)
                    {
                        if(result.data.driverAppResult.getDriverListResult.errorCode==0)
                        {
                            ownDriverList = result.data.driverAppResult.getDriverListResult.driverList

                            if (ownDriverList?.isNotEmpty() == true)
                            {
                                driverListAdapter?.swapData(ownDriverList!!)
                                showListView()
                            }
                            else
                            {
                                showEmptyView(getString(R.string.lbl_no_own_drivers_found))
                            }
                        }
                        else
                        {
                            showError(result.data.driverAppResult.getDriverListResult.errorMessage,false)
                        }
                    }
                }

                is Resource.Failure ->
                {
                    ownDriversListObserved=false
                    showError(getString(R.string.lbl_could_not_process_your_request),true)
                }
            }
        })
    }

    private fun observePoolDriversList()
    {
        driverListViewModel.poolDriverListOutput.observe(viewLifecycleOwner, androidx.lifecycle.Observer { result->
            when(result)
            {
                is Resource.Loading ->
                {
                    poolDriversListObserved = true
                    binding!!.btnRedirectToPoolDriver.isEnabled = false
                    binding!!.btnRedirectToPoolDriver.text = getString(R.string.lbl_fetching_pool_drivers)
                    showProgress()
                }

                is Resource.Success ->
                {
                    if(poolDriversListObserved)
                    {
                        driverIds.clear()
                        showListView()
                        if(result.data.driverAppResult.getpooldriverlistResult.errorDetail.errorCode==0)
                        {
                            for(drivers in result.data.driverAppResult.getpooldriverlistResult.poolDriverlist)
                            {
                                driverIds.add(Integer.parseInt(drivers.driverId))
                            }
                            if(driverIds.isNotEmpty())
                            {
                                requestCount = driverIds.size
                                responseCount = 0;
                                notifyPoolDriver(driverIds[responseCount])
                                binding?.progressBarNotify?.max = 100
                                showNotifyDrivers()
                            }
                            else
                            {
                                binding!!.btnRedirectToPoolDriver.isEnabled = true
                                binding!!.btnRedirectToPoolDriver.text = getString(R.string.lbl_notifying_pool_drivers)
                                showEmptyView(getString(R.string.lbl_no_pool_drivers_found))
                            }
                        }
                        else
                        {
                            binding!!.btnRedirectToPoolDriver.isEnabled = true
                            binding!!.btnRedirectToPoolDriver.text = getString(R.string.lbl_notifying_pool_drivers)
                            showError(result.data.driverAppResult.getpooldriverlistResult.errorDetail.errorMessage,false)
                        }
                    }
                }

                is Resource.Failure ->
                {
                    binding!!.btnRedirectToPoolDriver.isEnabled = true
                    binding!!.btnRedirectToPoolDriver.text = getString(R.string.lbl_redirect_to_pool_driver)
                    showError(getString(R.string.lbl_could_not_process_your_request),true)
                }
            }
        })
    }

    private fun observeNotifyDrivers()
    {
        driverListViewModel.notifyDriversOutput.observe(viewLifecycleOwner, androidx.lifecycle.Observer { result->
            when(result)
            {
                is Resource.Loading ->
                {
                    notifyDriversObserved= true
                    binding!!.btnRedirectToPoolDriver.isEnabled = false
                    binding!!.btnRedirectToPoolDriver.text = getString(R.string.lbl_notifying_pool_drivers)
                }

                is Resource.Success ->
                {
                    if(notifyDriversObserved)
                    {
                        if(result.data.driverNotificationResult.errorDetail.errorCode==0)
                        {
                            requireFragmentManager().popBackStackImmediate()
                        }
                        else
                        {
                            binding!!.btnRedirectToPoolDriver.isEnabled = true
                            binding!!.btnRedirectToPoolDriver.text = getString(R.string.lbl_notifying_pool_drivers)
                            Toast.makeText(requireContext(),result.data.driverNotificationResult.errorDetail.errorMessage,Toast.LENGTH_LONG).show()
                        }

                    }
                }

                is Resource.Failure ->
                {

                }
            }
        })
    }

    private fun observeSingleNotifyDrivers()
    {
        driverListViewModel.notifySingleDriverOutput.observe(viewLifecycleOwner, androidx.lifecycle.Observer { result->
            when(result)
            {
                is Resource.Loading ->
                {
                    notifyDriversObserved= true
                    binding!!.btnRedirectToPoolDriver.isEnabled = false
                    binding!!.btnRedirectToPoolDriver.text = getString(R.string.lbl_notifying_pool_drivers)
                }

                is Resource.Success ->
                {
                    if(notifyDriversObserved)
                    {
                        if(result.data.driverNotificationResult.errorDetail.errorCode==0)
                        {
                            when(signInResult?.driverType?.toInt())
                            {
                               AppConstants.OWN_DRIVERS->
                               {
                                   requireFragmentManager().popBackStackImmediate()
                               }
                               AppConstants.POOL_DRIVERS->
                               {
                                   responseCount++
                                   if(responseCount == requestCount)
                                   {
                                       Toast.makeText(requireContext(),
                                           getString(R.string.lbl_all_pool_drivers_notified),
                                           Toast.LENGTH_LONG).show()
                                       requireFragmentManager().popBackStackImmediate()
                                   }
                                   else
                                   {
                                       notifyProgress()
                                       notifyPoolDriver(driverIds[responseCount])
                                   }
                               }
                            }
                        }
                        else
                        {
                            binding!!.btnRedirectToPoolDriver.isEnabled = true
                            binding!!.btnRedirectToPoolDriver.text = getString(R.string.lbl_notifying_pool_drivers)
                            Toast.makeText(requireContext(),result.data.driverNotificationResult.errorDetail.errorMessage,Toast.LENGTH_LONG).show()
                        }
                    }
                }

                is Resource.Failure ->
                {
                    showError(getString(R.string.lbl_could_not_process_your_request),false)
                }
            }
        })
    }

    override fun onAttach(context: Context)
    {
        super.onAttach(context)
        dashBoardHandler = context as DashBoardHandler
    }

    override fun onDetach()
    {
        super.onDetach()
        dashBoardHandler = null
    }

    override fun onClick(view: View)
    {
        val id = view.id
        when (id)
        {
        }
    }

    private fun notifyProgress()
    {
        val percentage = (responseCount / requestCount * 100).toInt()
        binding?.progressBarNotify?.progress = percentage
        val strDriversNotified = StringBuilder()
        strDriversNotified.append(responseCount).append(" ").append(getString(R.string.lbl_pool_drivers_notified))
        binding?.tvProgressBar?.text = strDriversNotified.toString()
    }

    private fun showProgress()
    {
        binding?.progressBar?.visibility = View.VISIBLE
        binding?.rvDriverList?.visibility = View.GONE
        binding?.cnlErrorEmptyView?.visibility = View.GONE
        binding?.cnlNotifyProgressBar?.visibility = View.GONE
    }

    private fun showEmptyView(message: String)
    {
        binding?.progressBar?.visibility = View.GONE
        binding?.rvDriverList?.visibility = View.GONE
        binding?.cnlErrorEmptyView?.visibility = View.VISIBLE
        binding?.tvEmpty?.visibility=View.VISIBLE
        binding?.tvEmpty?.text = message
        binding?.btnRetry?.visibility=View.GONE
        binding?.cnlNotifyProgressBar?.visibility = View.GONE
    }

    private fun showListView()
    {
        binding?.progressBar?.visibility = View.GONE
        binding?.rvDriverList?.visibility = View.VISIBLE
        binding?.cnlErrorEmptyView?.visibility = View.GONE
        binding?.cnlNotifyProgressBar?.visibility = View.GONE
    }

    private fun showError(message: String,showRetry:Boolean)
    {
        binding?.progressBar?.visibility = View.GONE
        binding?.rvDriverList?.visibility = View.GONE
        binding?.cnlErrorEmptyView?.visibility = View.VISIBLE
        binding?.tvEmpty?.visibility=View.VISIBLE
        binding?.tvEmpty?.text = message
        if(showRetry) binding?.btnRetry?.visibility=View.VISIBLE
        else binding?.btnRetry?.visibility = View.GONE
        binding?.cnlNotifyProgressBar?.visibility=View.GONE

    }

    private fun showNotifyDrivers()
    {
        binding?.progressBar?.visibility = View.GONE
        binding?.rvDriverList?.visibility = View.GONE
        binding?.cnlErrorEmptyView?.visibility=View.GONE
        binding?.cnlNotifyProgressBar?.visibility=View.VISIBLE
    }

    companion object
    {
        private val TAG = DriverListFragment::class.java.simpleName
    }

    override fun driverListItemClicked(
        driverList:DriverList,
        position: Int
    )
    {
        val notifyToDriversInput:NotifyToDriversInput = NotifyToDriversInput()
        notifyToDriversInput.driverId = driverList.driverid
        notifyToDriversInput.orderId = Integer.parseInt(orderDetails!!.orderId)
        notifyToDriversInput.retailerId = signInResult?.retailerId!!
        driverListViewModel.setNotifySingleDriverRequest(notifyToDriversInput)
    }

}