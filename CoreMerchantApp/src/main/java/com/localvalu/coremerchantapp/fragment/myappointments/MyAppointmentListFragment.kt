package com.localvalu.coremerchantapp.fragment.myappointments

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.localvalu.coremerchantapp.LocalValuFragment
import com.localvalu.coremerchantapp.R
import com.localvalu.coremerchantapp.base.Resource
import com.localvalu.coremerchantapp.data.input.MyAppointmentListInput
import com.localvalu.coremerchantapp.data.input.MyAppointmentStatusChangeInput
import com.localvalu.coremerchantapp.data.output.myappointments.MyAppointmentList
import com.localvalu.coremerchantapp.fragment.adapter.MyAppointmentListAdapter
import com.localvalu.coremerchantapp.handler.DashBoardHandler
import com.utils.helper.recyclerview.RVItemAnimator
import com.utils.helper.preference.PreferenceFactory
import com.localvalu.coremerchantapp.fragment.myappointments.MyAppointmentListFragment
import com.localvalu.coremerchantapp.data.output.myappointments.MyAppointmentListResult
import com.localvalu.coremerchantapp.databinding.FragmentTableBookingListBinding
import com.localvalu.coremerchantapp.extensions.handleAPIError
import com.localvalu.coremerchantapp.extensions.showMessage
import com.localvalu.coremerchantapp.listeners.MyAppointmentItemClickListener
import com.localvalu.coremerchantapp.myappointments.viewmodel.MyAppointmentsViewModel
import com.localvalu.coremerchantapp.transaction.viewmodel.TransactionsViewModel
import com.localvalu.coremerchantapp.viewmodel.DashboardViewModel
import com.utils.base.Task
import com.utils.validator.Validator
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_my_transaction.*

@AndroidEntryPoint
class MyAppointmentListFragment : LocalValuFragment(), View.OnClickListener,MyAppointmentItemClickListener
{
    private var binding:FragmentTableBookingListBinding?=null;
    private val dashboardViewModel: DashboardViewModel by viewModels()
    private val viewModel: MyAppointmentsViewModel by viewModels()
    private var myAppointmentListAdapter: MyAppointmentListAdapter? = null
    private var dashBoardHandler: DashBoardHandler? = null
    private var retailerId:String="";
    private var position = -1;
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?
    {
        binding = FragmentTableBookingListBinding.inflate(inflater,container,false)
        setHasOptionsMenu(true)
        retainInstance = true
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        super.onViewCreated(view, savedInstanceState)
        setProperties()
    }

    private fun setProperties()
    {
        val linearLayoutManager = LinearLayoutManager(context)
        binding!!.recyclerView.layoutManager = linearLayoutManager
        binding!!.recyclerView.itemAnimator = RVItemAnimator()
        binding!!.recyclerView.addItemDecoration(
            DividerItemDecoration(
                context,
                linearLayoutManager.orientation
            )
        )
        binding!!.recyclerView.setHasFixedSize(true)
        myAppointmentListAdapter = MyAppointmentListAdapter(this)
        binding!!.recyclerView.adapter = myAppointmentListAdapter
        binding!!.layoutError.btnRetry.setOnClickListener{
            doGetMyAppointmentList()
        }
        observeSubscribers()
    }

    private fun observeSubscribers()
    {
        dashboardViewModel.user.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            this.retailerId = it.retailerId.toString()
            doGetMyAppointmentList()
        })
        viewModel.myAppointmentListResponse.observe(viewLifecycleOwner, Observer { response->
            when(response)
            {
                is Resource.Loading->
                {
                    showProgress()
                }
                is Resource.Success->
                {
                    if(response.value.myAppointmentListResult.errorDetail.errorCode==0)
                    {
                       setRecyclerview(response.value.myAppointmentListResult.myAppointmentList)
                    }
                    else
                    {
                        showErrorView(response.value.myAppointmentListResult.errorDetail.errorMessage,true)
                    }
                }
                is Resource.Failure->
                {
                    handleAPIError(response){
                        doGetMyAppointmentList()
                    }
                    showErrorView(getString(R.string.lbl_could_not_process_your_request),true)
                }
            }
        })
        viewModel.myAppointmentAcceptResponse.observe(viewLifecycleOwner, Observer { response->
            when(response)
            {
                is Resource.Loading->
                {
                    myAppointmentListAdapter?.currentList?.toMutableList()?.get(position)?.isUpdating=true
                    myAppointmentListAdapter?.notifyItemChanged(position)
                }
                is Resource.Success->
                {
                    if(response.value.myAppointmentStatusChangeResult.errorDetail.errorCode==0)
                    {
                        val cList = myAppointmentListAdapter?.currentList?.toMutableList()
                        cList?.removeAt(position)
                        myAppointmentListAdapter?.submitList(cList)

                        if (cList != null)
                        {
                            setRecyclerview(cList)
                        }

                        showMessage(response.value.myAppointmentStatusChangeResult.myAppointmentChangeStatusResult.message)
                    }
                    else
                    {
                        showErrorView(response.value.myAppointmentStatusChangeResult.errorDetail.errorMessage,true)

                        val cList = myAppointmentListAdapter?.currentList?.toMutableList()
                        cList?.get(position)?.isUpdating=false
                        myAppointmentListAdapter?.submitList(cList)
                    }
                }
                is Resource.Failure->
                {
                    handleAPIError(response){
                    }
                    showErrorView(getString(R.string.lbl_could_not_process_your_request),true)
                    val cList = myAppointmentListAdapter?.currentList?.toMutableList()
                    cList?.get(position)?.isUpdating=false
                    myAppointmentListAdapter?.submitList(cList)
                }
            }
        })
        viewModel.myAppointmentDeclineResponse.observe(viewLifecycleOwner, Observer { response->
            when(response)
            {
                is Resource.Loading->
                {
                    myAppointmentListAdapter?.currentList?.toMutableList()?.get(position)?.isUpdating=true
                    myAppointmentListAdapter?.notifyItemChanged(position)
                }
                is Resource.Success->
                {
                    if(response.value.myAppointmentStatusChangeResult.errorDetail.errorCode==0)
                    {
                        val cList = myAppointmentListAdapter?.currentList?.toMutableList()
                        cList?.removeAt(position)
                        myAppointmentListAdapter?.submitList(cList)
                        if (cList != null)
                        {
                            setRecyclerview(cList)
                        }
                        showMessage(response.value.myAppointmentStatusChangeResult.myAppointmentChangeStatusResult.message)
                    }
                    else
                    {
                        myAppointmentListAdapter?.currentList?.get(position)?.isUpdating=false
                        showErrorView(response.value.myAppointmentStatusChangeResult.errorDetail.errorMessage,true)

                        val cList = myAppointmentListAdapter?.currentList?.toMutableList()
                        cList?.get(position)?.isUpdating=false
                        myAppointmentListAdapter?.submitList(cList)
                    }
                }
                is Resource.Failure->
                {
                    handleAPIError(response){
                    }
                    showErrorView(getString(R.string.lbl_could_not_process_your_request),true)
                    val cList = myAppointmentListAdapter?.currentList?.toMutableList()
                    cList?.get(position)?.isUpdating=false
                    myAppointmentListAdapter?.submitList(cList)
                }
            }
        })
        dashboardViewModel?.getUser()
    }

    private fun setRecyclerview(items:List<MyAppointmentList>)
    {
        if(items.size>0) showListView()
        else showErrorView(getString(R.string.lbl_no_appointments_found),false)
        myAppointmentListAdapter?.submitList(items)

    }

    override fun onAttach(context: Context)
    {
        super.onAttach(context)
        dashBoardHandler = context as DashBoardHandler
    }

    override fun onDetach()
    {
        super.onDetach()
        dashBoardHandler = null
    }

    override fun onClick(view: View)
    {
        val id = view.id
        when (id)
        {
        }
    }

    private fun doGetMyAppointmentList()
    {
        retailerId?.let {
            val myAppointmentListInput:MyAppointmentListInput= MyAppointmentListInput()
            myAppointmentListInput.retailerId=it
            viewModel?.myAppointments(myAppointmentListInput)
        }
    }

    private fun doAcceptAppointment(slotId:String)
    {
        retailerId?.let {
            val myappointmentStatusChangeInput= MyAppointmentStatusChangeInput()
            myappointmentStatusChangeInput.slotId = slotId
            myappointmentStatusChangeInput.status="0"
            viewModel?.acceptAppointment(myappointmentStatusChangeInput)
        }
    }

    private fun doDeclineAppointment(slotId:String)
    {
        retailerId?.let {
            val myappointmentStatusChangeInput= MyAppointmentStatusChangeInput()
            myappointmentStatusChangeInput.slotId = slotId
            myappointmentStatusChangeInput.status="2"
            viewModel?.declineAppointment(myappointmentStatusChangeInput)
        }
    }

    private fun showProgress()
    {
        binding!!.recyclerView.visibility = View.GONE
        binding!!.layoutProgress.layoutRoot.visibility = View.VISIBLE
        binding!!.layoutError.layoutRoot.visibility = View.GONE
    }

    private fun showListView()
    {
        binding!!.recyclerView.visibility = View.VISIBLE
        binding!!.layoutProgress.layoutRoot.visibility = View.GONE
        binding!!.layoutError.layoutRoot.visibility = View.GONE
    }

    private fun showErrorView(message: String,showRetry:Boolean)
    {
        binding!!.recyclerView.visibility = View.GONE
        binding!!.layoutProgress.layoutRoot.visibility = View.GONE
        binding!!.layoutError.layoutRoot.visibility = View.VISIBLE
        binding!!.layoutError.tvError.setText(message)
        if(showRetry) binding!!.layoutError.btnRetry.visibility=View.VISIBLE
        else binding!!.layoutError.btnRetry.visibility=View.GONE
    }

    override fun onDestroy()
    {
        super.onDestroy()
    }

    companion object
    {
        private val TAG = MyAppointmentListFragment::class.java.simpleName
    }

    override fun onMyAppointmentItemClicked(position: Int, myAppointmentList: MyAppointmentList?)
    {
        TODO("Not yet implemented")
    }

    override fun onMyAppointmentAccepted(position: Int, myAppointmentList: MyAppointmentList?)
    {
        this.position = position;
        myAppointmentList?.slotId?.let {
            doAcceptAppointment(it)
        }
    }

    override fun onMyAppointmentDeclined(position: Int, myAppointmentList: MyAppointmentList?)
    {
        this.position = position;
        myAppointmentList?.slotId?.let {
            myAppointmentListAdapter?.currentList?.get(position)?.isUpdating=true
            doDeclineAppointment(it)
        }
    }
}