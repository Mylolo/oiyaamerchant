package com.localvalu.coremerchantapp.fragment.orders

import android.content.Context
import com.localvalu.coremerchantapp.handler.DashBoardHandler
import com.localvalu.coremerchantapp.fragment.adapter.order.MyOrdersFragmentAdapter
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import com.localvalu.coremerchantapp.BuildConfig
import com.localvalu.coremerchantapp.LocalValuConstants
import com.localvalu.coremerchantapp.LocalValuFragment
import com.utils.helper.preference.PreferenceFactory
import com.localvalu.coremerchantapp.R
import com.localvalu.coremerchantapp.data.output.SignInResult
import com.localvalu.coremerchantapp.databinding.FragmentMyOrdersBinding
import com.localvalu.coremerchantapp.fragment.orders.OrderListFragment
import com.localvalu.coremerchantapp.fragment.orders.OrderAtTableListFragment
import com.localvalu.coremerchantapp.fragment.orders.MyOrdersFragment

class MyOrdersFragment : LocalValuFragment(), LocalValuConstants
{
    private var dashBoardHandler: DashBoardHandler? = null
    private var myOrdersFragmentAdapter: MyOrdersFragmentAdapter? = null
    private var binding: FragmentMyOrdersBinding? = null
    private var signInResult: SignInResult? = null

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        try
        {
            val strSignInResult = PreferenceFactory.getInstance().getString(PREF_SIGN_IN_RESULT)
            if (strSignInResult != null)
            {
                if (strSignInResult != "")
                {
                    val gson = Gson()
                    signInResult = gson.fromJson(strSignInResult, SignInResult::class.java)
                }
            }
            if (signInResult != null)
            {
                if (BuildConfig.DEBUG)
                {
                    Log.d(TAG, "signInResult->" + signInResult!!.driverType)
                }
            }
        }
        catch (ex: Exception)
        {
            ex.printStackTrace()
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?
    {
        binding = FragmentMyOrdersBinding.inflate(inflater, container, false)
        setHasOptionsMenu(true)
        setProperties()
        return binding!!.root
    }

    override fun onAttach(context: Context)
    {
        super.onAttach(context)
        dashBoardHandler = context as DashBoardHandler
    }

    override fun onDetach()
    {
        super.onDetach()
        dashBoardHandler = null
    }

    private fun setProperties()
    {
        val titles = arrayOfNulls<String>(2)
        titles[0] = getString(R.string.lbl_online_order)
        if(signInResult?.bookingTypeId?.contains(ORDER_AT_TABLE)!!)
        {
            titles[1] = getString(R.string.lbl_order_at_table)
        }
        else if(signInResult?.bookingTypeId?.contains(ORDER_AT_STORE)!!)
        {
            titles[1] = getString(R.string.lbl_order_at_store)
        }

        myOrdersFragmentAdapter = MyOrdersFragmentAdapter(childFragmentManager)
        myOrdersFragmentAdapter!!.addFragment(OrderListFragment(), titles[0])
        myOrdersFragmentAdapter!!.addFragment(OrderAtTableListFragment(), titles[1])
        binding!!.viewPagerMyOrders.adapter = myOrdersFragmentAdapter
        binding!!.tabs.setupWithViewPager(binding!!.viewPagerMyOrders)
    }

    fun updateStatus(status: String?)
    {
        val orderListFragment = myOrdersFragmentAdapter!!.getItem(0) as OrderListFragment
        orderListFragment?.updateStatus(status)
    }

    fun updateStatusInOrderAtTable(status: String?)
    {
        val orderAtTableListFragment =
            myOrdersFragmentAdapter!!.getItem(1) as OrderAtTableListFragment
        orderAtTableListFragment?.updateStatus(status)
    }

    override fun onDestroy()
    {
        dashBoardHandler!!.setPreviousTitle()
        super.onDestroy()
    }

    companion object
    {
        private val TAG = MyOrdersFragment::class.java.simpleName
    }
}