package com.localvalu.coremerchantapp.fragment.orders

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.localvalu.coremerchantapp.LocalValuFragment
import com.localvalu.coremerchantapp.R
import com.localvalu.coremerchantapp.base.Resource
import com.localvalu.coremerchantapp.data.input.OrderListInput
import com.localvalu.coremerchantapp.utils.Device.Companion.devicePlayStore
import com.localvalu.coremerchantapp.listeners.OrderItemClickListener
import com.localvalu.coremerchantapp.handler.DashBoardHandler
import com.localvalu.coremerchantapp.data.output.order.OrderListData
import com.utils.helper.recyclerview.RVItemAnimator
import com.localvalu.coremerchantapp.utils.AppUtils
import com.localvalu.coremerchantapp.data.output.order.OrderListResult
import com.localvalu.coremerchantapp.databinding.FragmentOrderListBinding
import com.localvalu.coremerchantapp.extensions.handleAPIError
import com.localvalu.coremerchantapp.fragment.adapter.order.OrderAtTableListAdapter
import com.localvalu.coremerchantapp.orders.viewmodel.TableOrderListViewModel
import com.localvalu.coremerchantapp.viewmodel.DashboardViewModel
import dagger.hilt.android.AndroidEntryPoint
import java.lang.Exception
import java.util.ArrayList

@AndroidEntryPoint
class OrderAtTableListFragment : LocalValuFragment(), OrderItemClickListener
{
    private val dashboardViewModel: DashboardViewModel by viewModels()
    private val viewModel: TableOrderListViewModel by activityViewModels()
    private var orderAtTableListAdapter: OrderAtTableListAdapter? = null
    private var dashBoardHandler: DashBoardHandler? = null
    private var orderAtTableListBroadcastReceiver: OrderAtTableListBroadCastReceiver? = null
    private var selectedPosition = -1
    private var selectedItem: OrderListData? = null
    private var binding: FragmentOrderListBinding? = null
    private var retailerId:String="";

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        orderAtTableListBroadcastReceiver = OrderAtTableListBroadCastReceiver()
        registerMyReceiver()
    }

    override fun onAttach(context: Context)
    {
        super.onAttach(context)
        dashBoardHandler = context as DashBoardHandler
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?
    {
        binding = FragmentOrderListBinding.inflate(inflater, container, false)
        setHasOptionsMenu(true)
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        setProperties()
    }

    private fun setProperties()
    {
        val linearLayoutManager = LinearLayoutManager(context)
        linearLayoutManager.orientation = RecyclerView.VERTICAL
        binding!!.rvOrderList.layoutManager = linearLayoutManager
        binding!!.rvOrderList.itemAnimator = RVItemAnimator()
        binding!!.rvOrderList.addItemDecoration(
            DividerItemDecoration(
                context, linearLayoutManager.orientation
            )
        )
        binding!!.rvOrderList.setHasFixedSize(true)
        orderAtTableListAdapter = OrderAtTableListAdapter(this)
        binding!!.rvOrderList.adapter = orderAtTableListAdapter
        binding!!.layoutError.btnRetry.setOnClickListener{
            doGetOrderList()
        }
        observeSubscribers()
    }

    private fun observeSubscribers()
    {
        dashboardViewModel.user.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            this.retailerId = it.retailerId.toString()
            doGetOrderList()
        })
        viewModel.tableOrderListResponse.observe(viewLifecycleOwner, Observer { event->
            if(!event.hasBeenHandled)
            {
                when(val response = event.getContentIfNotHandled())
                {
                    is Resource.Loading->
                    {
                        showProgress()
                    }
                    is Resource.Success->
                    {
                        if(response.value.orderListResult.errorDetail.errorCode==0)
                        {
                            showListView()
                            setRecyclerview(response.value.orderListResult.orderList)
                        }
                        else
                        {
                            showErrorView(response.value.orderListResult.errorDetail.errorMessage,true)
                        }
                    }
                    is Resource.Failure->
                    {
                        handleAPIError(response){
                            doGetOrderList()
                        }
                        showErrorView(getString(R.string.lbl_could_not_process_your_request),true)
                    }
                }
            }
        })
        dashboardViewModel?.getUser()
    }

    override fun onDetach()
    {
        super.onDetach()
        dashBoardHandler = null
    }

    /**
     * This method is responsible to register an action to BroadCastReceiver
     */
    private fun registerMyReceiver()
    {
        try
        {
            val intentFilter = IntentFilter()
            intentFilter.addAction(AppUtils.BROADCAST_ACTION_ORDER_AT_TABLE_LIST)
            requireContext().registerReceiver(orderAtTableListBroadcastReceiver, intentFilter)
        } catch (ex: Exception)
        {
            ex.printStackTrace()
        }
    }

    private fun doGetOrderList()
    {
        retailerId?.let {
            val orderListnput: OrderListInput = OrderListInput()
            orderListnput.retailerId=it
            viewModel?.tableOrderList(orderListnput)
        }
    }

    private fun setRecyclerview(items:List<OrderListData>)
    {
        Log.d(TAG,"setRecyclerview" + orderAtTableListAdapter)
        orderAtTableListAdapter?.submitList(null)
        orderAtTableListAdapter?.submitList(items)
    }

    private fun showProgress()
    {
        binding!!.rvOrderList.visibility = View.GONE
        binding!!.layoutProgress.layoutRoot.visibility = View.VISIBLE
        binding!!.layoutError.layoutRoot.visibility = View.GONE
    }

    private fun showListView()
    {

        binding!!.rvOrderList.visibility = View.VISIBLE
        binding!!.layoutProgress.layoutRoot.visibility = View.GONE
        binding!!.layoutError.layoutRoot.visibility = View.GONE
    }

    private fun showErrorView(message: String,showRetry:Boolean)
    {
        binding!!.rvOrderList.visibility = View.GONE
        binding!!.layoutProgress.layoutRoot.visibility = View.GONE
        binding!!.layoutError.layoutRoot.visibility = View.VISIBLE
        binding!!.layoutError.tvError.setText(message)
        if(showRetry) binding!!.layoutError.btnRetry.visibility=View.VISIBLE
        else binding!!.layoutError.btnRetry.visibility=View.GONE
    }

    private fun dummyData(): List<OrderListData>
    {
        val orderListDataList: MutableList<OrderListData> = ArrayList()
        val orderListData = OrderListData()
        orderListData.orderId = "1"
        orderListData.businessId = "2"
        orderListData.orderTotal = "300"
        orderListData.isOrderConfirmedByRest = "1"
        orderListData.customerName = "Test"
        orderListData.currentStatus = "0"
        orderListData.allergyInstructions = "None"
        orderListData.specialInstructions = "None"
        orderListData.status = "1"
        orderListData.createdDate = "2020-02-22 14:09:12"
        orderListData.customerMobile = "88999000033"
        orderListData.isOrderPlaced = "1"
        orderListDataList.add(orderListData)
        return orderListDataList
    }

    override fun onOrderItemClicked(position: Int, orderListData: OrderListData)
    {
        selectedPosition = position
        selectedItem = orderListData
        if (dashBoardHandler!!.deviceType() == devicePlayStore)
        {
            dashBoardHandler!!.orderAtTableDetails(orderListData.orderId)
        } else
        {
            dashBoardHandler!!.showDeviceOrderAtTableDetails(orderListData.orderId)
        }
    }

    fun updateStatus(status: String?)
    {
        if (selectedItem != null)
        {
            orderAtTableListAdapter?.currentList?.toMutableList()?.removeAt(selectedPosition)
            selectedItem!!.status = status
            orderAtTableListAdapter?.currentList?.toMutableList()?.add(selectedPosition, selectedItem)
            orderAtTableListAdapter?.notifyItemChanged(selectedPosition)
        }
    }

    internal inner class OrderAtTableListBroadCastReceiver : BroadcastReceiver()
    {
        private val TAG = "OrderAtTableListBroadCastReceiver"

        override fun onReceive(context: Context, intent: Intent)
        {
            try
            {
                Log.d(Companion.TAG, "onReceive() called")
                if (intent.action == AppUtils.BROADCAST_ACTION_ORDER_AT_TABLE_LIST)
                {
                    val orderListResult = intent
                        .getParcelableExtra<OrderListResult>(AppUtils.BUNDLE_ORDER_AT_TABLE_LIST_RESULT)!!
                    setRecyclerview(orderListResult.orderList)
                }
            } catch (ex: Exception)
            {
                ex.printStackTrace()
            }
        }
    }

    override fun onDestroy()
    {
        super.onDestroy()
        requireContext().unregisterReceiver(orderAtTableListBroadcastReceiver)
    }

    companion object
    {
        private val TAG = OrderListFragment::class.java.simpleName
    }
}