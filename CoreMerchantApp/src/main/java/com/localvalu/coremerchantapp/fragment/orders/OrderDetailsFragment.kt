package com.localvalu.coremerchantapp.fragment.orders

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.localvalu.coremerchantapp.handler.DashBoardHandler
import com.localvalu.coremerchantapp.data.output.order.OrderDetails
import com.localvalu.coremerchantapp.fragment.adapter.order.OrderedFoodItemsAdapter
import com.localvalu.coremerchantapp.data.output.SignInResult
import com.localvalu.coremerchantapp.utils.AppUtils
import com.utils.helper.preference.PreferenceFactory
import com.google.gson.Gson
import com.localvalu.coremerchantapp.BuildConfig
import com.localvalu.coremerchantapp.LocalValuFragment
import com.localvalu.coremerchantapp.R
import com.localvalu.coremerchantapp.base.Resource
import com.localvalu.coremerchantapp.components.OiyaaPlayer
import com.localvalu.coremerchantapp.data.input.DriversProfileInput
import com.localvalu.coremerchantapp.data.input.OrderDetailsInput
import com.localvalu.coremerchantapp.data.input.OrderStatusChangeInput
import com.utils.helper.recyclerview.RVItemAnimator
import com.utils.helper.recyclerview.RVItemDecorator
import com.localvalu.coremerchantapp.utils.AppConstants
import com.utils.util.DateTimeUtil
import com.localvalu.coremerchantapp.databinding.FragmentOrderDetailsBinding
import com.localvalu.coremerchantapp.drivers.viewmodel.DriverProfileInfoViewModel
import com.localvalu.coremerchantapp.extensions.handleAPIError
import com.localvalu.coremerchantapp.extensions.showMessage
import com.localvalu.coremerchantapp.orders.viewmodel.OnlineOrderDetailsViewModel
import com.localvalu.coremerchantapp.orders.viewmodel.OnlineOrderListViewModel
import com.localvalu.coremerchantapp.orders.viewmodel.OnlineOrderStatusChangeViewModel
import com.localvalu.coremerchantapp.utils.Device
import com.localvalu.coremerchantapp.viewmodel.DashboardViewModel
import com.utils.base.AppBaseFragment
import dagger.hilt.android.AndroidEntryPoint
import java.lang.Exception
import java.lang.StringBuilder
import java.text.SimpleDateFormat

/**
 * Status 3 -- Waiting for confirm
 * Status 2 -- Declined
 * Status 1 -- Delivered
 * Status 0 -- Accept
 * Status 6 -- Order Cancelled
 * Status 5 -- Booking Table Cancelled
 * Status 20 -- Print
 */
@AndroidEntryPoint
class OrderDetailsFragment : AppBaseFragment()
{
    private val dashboardViewModel: DashboardViewModel by viewModels()
    private val onlineOrderDetailsViewModel: OnlineOrderDetailsViewModel by viewModels()
    private val onlineOrderStatusChangeViewModel: OnlineOrderStatusChangeViewModel by viewModels()
    private val onlineOrderListViewModel: OnlineOrderListViewModel by activityViewModels()
    private val driverProfileViewModel: DriverProfileInfoViewModel by viewModels()
    private lateinit var binding:FragmentOrderDetailsBinding
    private var dashBoardHandler: DashBoardHandler? = null
    private var orderId: String? = null
    private var orderedFoodItemsAdapter: OrderedFoodItemsAdapter? = null
    private var strCurrencySymbol: String? = null
    private var strCurrencyLetter: String? = null
    private val previousTitle: String? = null
    private var statusUpdated = false
    private var retailerId:String="";
    private var orderDetails:OrderDetails? = null
    private var user:SignInResult?=null

    override fun onCreate(savedInstanceState: Bundle?)
    {
        Log.d(TAG,"onCreate")
        super.onCreate(savedInstanceState)
        val bundle = arguments
        if (bundle != null)
        {
            orderId = bundle.getString(AppUtils.BUNDLE_ORDER_ID)
        }
        strCurrencySymbol = getString(R.string.symbol_pound)
        strCurrencyLetter = getString(R.string.currency_letter_lt)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?
    {
        Log.d(TAG,"onCreateView")
        binding = FragmentOrderDetailsBinding.inflate(inflater,container,false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        Log.d(TAG,"onViewCreated")
        setProperties()
    }

    private fun setProperties()
    {
        Log.d(TAG,"setProperties")

        val linearLayoutManager = LinearLayoutManager(context)
        binding!!.rvOrderedItems.layoutManager = linearLayoutManager
        binding!!.rvOrderedItems.itemAnimator = RVItemAnimator()
        binding!!.rvOrderedItems.setHasFixedSize(true)
        val drawable = resources.getDrawable(R.drawable.list_divider)
        if (drawable != null)
        {
            binding!!.rvOrderedItems.addItemDecoration(RVItemDecorator(drawable))
        }
        binding!!.btnAssignDriver.setOnClickListener(onClickListener)
        binding!!.btnAccept!!.setOnClickListener(onClickListener)
        binding!!.btnReject!!.setOnClickListener(onClickListener)
        binding!!.btnPrint!!.setOnClickListener(onClickListener)
        binding!!.btnCompleted!!.setOnClickListener(onClickListener)
        binding!!.cnlAssignDriver!!.visibility = View.GONE
        binding!!.tvTextDriverAssigned!!.visibility = View.GONE
        binding!!.tvDriverAssigned!!.visibility = View.GONE
        binding!!.tvTextDriverName!!.visibility = View.GONE
        binding!!.tvDriverName!!!!.visibility = View.GONE
        orderedFoodItemsAdapter = OrderedFoodItemsAdapter()
        binding!!.rvOrderedItems.adapter = orderedFoodItemsAdapter
        binding!!.layoutError.btnRetry.setOnClickListener{
            doGetOrderDetails()
        }
        observeSubscribers()
        dashboardViewModel.getUser()
    }

    private fun observeSubscribers()
    {
        dashboardViewModel.user.observe(viewLifecycleOwner, Observer {
            this.retailerId = it.retailerId.toString()
            this.user = it
            doGetOrderDetails()
        })
        onlineOrderDetailsViewModel.onlineOrderDetailsApiResponse.observe(viewLifecycleOwner, Observer { event->
            if(!event.hasBeenHandled)
            {
                when(val response = event.getContentIfNotHandled())
                {
                    is Resource.Loading->
                    {
                        showProgress()
                    }
                    is Resource.Success->
                    {
                        if(response.value.orderDetailsResult.errorDetail.errorCode==0)
                        {
                            showContent()
                            orderDetails = response.value.orderDetailsResult.orderDetails.get(0)
                            setData()
                        }
                        else
                        {
                            showErrorView(response.value.orderDetailsResult.errorDetail.errorMessage,true)
                        }
                    }
                    is Resource.Failure->
                    {
                        handleAPIError(response){
                        }
                        showErrorView(getString(R.string.lbl_could_not_process_your_request),true)
                    }
                }
            }
        })
        onlineOrderStatusChangeViewModel.onlineOrderChangeStatusResponse.observe(viewLifecycleOwner, Observer { event->
            if(!event.hasBeenHandled)
            {
                when(val response = event.getContentIfNotHandled())
                {
                    is Resource.Loading->
                    {
                        binding!!.progressBarOrderDetails.visibility=View.VISIBLE
                        binding!!.cnlOrderDetailsBottom.visibility=View.GONE
                    }
                    is Resource.Success->
                    {
                        binding!!.progressBarOrderDetails.visibility=View.GONE
                        binding!!.cnlOrderDetailsBottom.visibility=View.VISIBLE
                        if(response.value.orderStatusChangeResult.errorDetail.errorCode==0)
                        {
                            doGetOrderDetails()
                        }
                        else
                        {
                            showMessage(response.value.orderStatusChangeResult.errorDetail.errorMessage)
                        }
                    }
                    is Resource.Failure->
                    {
                        binding!!.progressBarOrderDetails.visibility=View.GONE
                        binding!!.cnlOrderDetailsBottom.visibility=View.VISIBLE
                        handleAPIError(response){
                        }
                    }
                }
            }
        })
        driverProfileViewModel.driverProfileResponse.observe(viewLifecycleOwner, Observer { event->
            if(!event.hasBeenHandled)
            {
                when(val response = event.getContentIfNotHandled())
                {
                    is Resource.Loading->
                    {
                    }
                    is Resource.Success->
                    {
                        if(response.value.driverProfileResult.errorDetail.errorCode==0)
                        {
                            binding!!.tvDriverName.text = response.value.driverProfileResult.name
                        }
                        else
                        {
                            showMessage(response.value.driverProfileResult.errorDetail.errorMessage)
                        }
                    }
                    is Resource.Failure->
                    {
                        handleAPIError(response){
                        }
                    }
                }
            }
        })
        dashboardViewModel?.getUser()
    }

    override fun onAttach(context: Context)
    {
        super.onAttach(context)
        dashBoardHandler = context as DashBoardHandler
    }

    override fun onDetach()
    {
        super.onDetach()
        dashBoardHandler = null
    }

    private fun showProgress()
    {
        binding!!.nsvOrderDetails.visibility = View.GONE
        binding!!.layoutProgress.layoutRoot.visibility = View.VISIBLE
        binding!!.layoutError.layoutRoot.visibility = View.GONE
        binding!!.cnlOrderDetailsBottom.visibility = View.GONE
    }

    private fun showContent()
    {
        binding!!.nsvOrderDetails.visibility = View.VISIBLE
        binding!!.layoutProgress.layoutRoot.visibility = View.GONE
        binding!!.layoutError.layoutRoot.visibility = View.GONE
        binding!!.cnlOrderDetailsBottom.visibility = View.VISIBLE
    }

    private fun showErrorView(message: String,showRetry:Boolean)
    {
        binding!!.nsvOrderDetails.visibility = View.VISIBLE
        binding!!.cnlOrderDetailsBottom.visibility = View.VISIBLE
        binding!!.layoutProgress.layoutRoot.visibility = View.GONE
        binding!!.layoutError.layoutRoot.visibility = View.VISIBLE
        binding!!.layoutError.tvError.setText(message)
        if(showRetry) binding!!.layoutError.btnRetry.visibility=View.VISIBLE
        else binding!!.layoutError.btnRetry.visibility=View.GONE
    }

    var onClickListener = View.OnClickListener { v ->
        val id = v.id
        if (id == R.id.btnAccept)
        {
            doAccept()
        } else if (id == R.id.btnReject)
        {
            doReject()
        } else if (id == R.id.btnCompleted)
        {
            doComplete()
        } else if (id == R.id.btnPrint)
        {
            dashBoardHandler!!.showPrintScreen(orderDetails)
        } else if (id == R.id.btnAssignDriver)
        {
            dashBoardHandler!!.driverListFragment(orderDetails)
        }
    }

    private fun dummyData(): OrderDetails
    {
        val orderListData = OrderDetails()
        orderListData.orderId = "1"
        orderListData.businessId = "2"
        orderListData.orderTotal = "125"
        orderListData.isOrderConfirmedByRest = "1"
        orderListData.customerName = "Test"
        orderListData.currentStatus = "0"
        orderListData.allergyInstructions = "None"
        orderListData.specialInstructions = "None"
        orderListData.status = "1"
        orderListData.createdDate = "2020-02-25 14:09:12"
        orderListData.dateTime = "2020-02-22 14:09:12"
        orderListData.customerMobile = "88999000033"
        orderListData.isOrderPlaced = "1"
        orderListData.finalOrderTotal = "12"
        return orderListData
    }

    private fun doGetOrderDetails()
    {
        //Log.d(TAG, "doGetOrderDetails: order Id - " + orderId);
        //orderDetails=dummyData();
        //setData()
        val orderDetailsInput = OrderDetailsInput()
        orderDetailsInput.orderId = orderId
        onlineOrderDetailsViewModel.onlineOrderDetails(orderDetailsInput)
    }

    private fun doGetDriverProfile(driverId: String)
    {
        val driverProfileInput:DriversProfileInput = DriversProfileInput()
        driverProfileInput.driverId = driverId
        driverProfileViewModel.driverProfile(driverProfileInput)
    }

    private fun setData()
    {
        if (orderDetails != null)
        {
            binding!!.tvCustomerName!!.text = orderDetails!!.customerName
            binding!!.tvOrderId!!.text = orderDetails!!.orderId
            if(user?.retailerType==AppConstants.RETAILER_TYPE_EATS)
            {
                binding!!.tvAllergyInstructions!!.text = orderDetails!!.allergyInstructions
                binding!!.tvTextAllergyInstructions.visibility = View.VISIBLE
                binding!!.tvAllergyInstructions.visibility = View.VISIBLE
            }
            else
            {
                binding!!.tvTextAllergyInstructions.visibility = View.GONE
                binding!!.tvAllergyInstructions.visibility = View.GONE
            }
            binding!!.tvSpecialInstructions!!.text = orderDetails!!.specialInstructions
            binding!!.tvPaymentStatus!!.text = orderDetails!!.paymentStatus
            when (orderDetails!!.orderOption)
            {
                AppConstants.ORDER_OPTION_DELIVERY -> if (orderDetails!!.isDriverSelected == AppConstants.ORDER_DRIVER_SELECTED)
                {
                    binding!!.tvTextDriverAssigned!!.visibility = View.VISIBLE
                    binding!!.tvDriverAssigned!!.visibility = View.VISIBLE
                    binding!!.tvDriverAssigned!!.text = getString(R.string.lbl_yes)
                    binding!!.tvDriverAssigned!!.setTextColor(resources.getColor(R.color.greenDark))

                    binding!!.tvTextDriverName!!!!.visibility = View.VISIBLE
                    binding!!.tvDriverName!!!!.visibility = View.VISIBLE

                    doGetDriverProfile(orderDetails!!.driverId)
                }
                else
                {
                    binding!!.tvTextDriverAssigned!!.visibility=View.VISIBLE
                    binding!!.tvDriverAssigned!!.visibility=View.VISIBLE
                    binding!!.tvDriverAssigned!!.text = getString(R.string.lbl_no)
                    binding!!.tvDriverAssigned!!.setTextColor(resources.getColor(R.color.redDark))

                    binding!!.tvTextDriverName!!!!.visibility = View.GONE
                    binding!!.tvDriverName!!!!.visibility = View.GONE
                }
                else ->
                {
                    binding!!.tvTextDriverAssigned!!.visibility = View.GONE
                    binding!!.tvDriverAssigned!!.visibility = View.GONE
                    binding!!.tvTextDriverName!!!!.visibility = View.GONE
                    binding!!.tvDriverName!!!!.visibility = View.GONE
                }
            }
            setPaymentDetails()
            val dateFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
            try
            {
                val orderDate = dateFormat.parse(orderDetails!!.dateTime)
                val strDate = DateTimeUtil.getFormattedDate(orderDate.time, DateTimeUtil.dd_MM_yyyy)
                val strTime = DateTimeUtil.getFormattedDate(orderDate.time, DateTimeUtil.hh_mm_a)
                val strDateTime = StringBuilder()
                strDateTime.append(strDate).append(" ").append(strTime)
                binding!!.tvOrderDate!!.text = strDateTime.toString()
            } catch (ex: Exception)
            {
                ex.printStackTrace()
            }
            if (orderDetails!!.status != null)
            {
                if (orderDetails!!.status.length > 0)
                {
                    try
                    {
                        setOrderStatus(orderDetails!!.status)
                        onlineOrderListViewModel.updateList(orderId.toString(),orderDetails!!.status)
                    }
                    catch (ex: Exception)
                    {
                        ex.printStackTrace()
                    }
                }
            }
            orderedFoodItemsAdapter?.submitList(orderDetails?.foodItems)
            showHideBottom()
        }
    }

    private fun setPaymentDetails()
    {
        try
        {
            val orderTotal = orderDetails!!.orderTotal.toDouble()
            val strOrderTotal = String.format("%,.2f", orderTotal)
            val strSubTotal = StringBuilder()
            strSubTotal.append(strCurrencySymbol).append(strOrderTotal)
            binding!!.tvSubTotal!!.text = strSubTotal.toString()
            val discountAmount = orderDetails!!.discountLtAmount.toDouble()
            val strDiscountAmount = String.format("%,.2f", discountAmount)
            val strTokenApply = StringBuilder()
            strTokenApply.append("-").append(strCurrencyLetter).append(strDiscountAmount)
            binding!!.tvTokenApplied!!.text = strTokenApply.toString()
            val deliveryCharges = orderDetails!!.deliveryRate.toDouble()
            val strDeliveryCharges = String.format("%,.2f", deliveryCharges)
            val strDeliveryChargesTwo = StringBuilder()
            strDeliveryChargesTwo.append("+").append(strCurrencySymbol).append(strDeliveryCharges)
            binding!!.tvDeliveryFee!!.text = strDeliveryChargesTwo.toString()
            val finalOrderTotal = orderDetails!!.finalOrderTotal.toDouble()
            val strFinalOrderTotal = String.format("%,.2f", finalOrderTotal)
            val strFinalPay = StringBuilder()
            strFinalPay.append(strCurrencySymbol).append(strFinalOrderTotal)
            binding!!.tvFinalPay!!.text = strFinalPay.toString()
        }
        catch (ex: Exception)
        {
            ex.printStackTrace()
        }
    }

    private fun showHideBottom()
    {
        when (orderDetails!!.status)
        {
            AppConstants.PENDING ->
            {
                binding!!.cnlOrderEntryProcess!!.visibility = View.VISIBLE
                binding!!.btnCompleted!!.visibility = View.GONE
                binding!!.cnlAssignDriver!!.visibility = View.GONE
            }
            AppConstants.DECLINED, AppConstants.ORDER_CANCELLED ->
            {
                binding!!.cnlOrderEntryProcess!!.visibility = View.GONE
                binding!!.btnCompleted!!.visibility = View.GONE
                binding!!.cnlAssignDriver!!.visibility = View.GONE
            }
            AppConstants.DELIVERED ->
            {
                binding!!.btnCompleted!!.visibility = View.GONE
                binding!!.cnlOrderEntryProcess!!.visibility = View.GONE
                binding!!.cnlAssignDriver!!.visibility = View.GONE
            }
            AppConstants.ACCEPT ->
            {
                //binding!!.btnCompleted.setVisibility(View.VISIBLE);
                binding!!.cnlOrderEntryProcess!!.visibility = View.GONE
                binding!!.cnlAssignDriver!!.visibility = View.GONE
                when (orderDetails!!.orderOption)
                {
                    AppConstants.ORDER_OPTION_DELIVERY ->
                    {
                        if (orderDetails!!.isDriverSelected == AppConstants.ORDER_DRIVER_SELECTED)
                        {
                            //binding!!.btnCompleted.setVisibility(View.VISIBLE);
                            binding!!.cnlAssignDriver!!.visibility = View.GONE
                        }
                        else
                        {
                            if (user != null)
                            {
                                if (user!!.driverType != null)
                                {
                                    if (user!!.driverType.toInt() == AppConstants.NO_DRIVERS)
                                    {
                                        binding!!.cnlAssignDriver!!.visibility = View.GONE
                                    } else binding!!.cnlAssignDriver!!.visibility = View.VISIBLE
                                } else binding!!.cnlAssignDriver!!.visibility = View.VISIBLE
                            } else binding!!.cnlAssignDriver!!.visibility = View.VISIBLE
                            binding!!.btnCompleted!!.visibility = View.GONE
                        }
                        binding!!.cnlOrderEntryProcess!!.visibility = View.GONE
                    }
                    else ->
                    {
                        binding!!.cnlAssignDriver!!.visibility = View.GONE
                        binding!!.cnlOrderEntryProcess!!.visibility = View.GONE
                        binding!!.btnCompleted!!.visibility = View.VISIBLE
                    }
                }
            }
        }
    }

    private fun setOrderStatus(status: String)
    {
        when (status)
        {
            AppConstants.ACCEPT ->
            {
                binding!!.tvOrderStatus!!.text = getString(R.string.lbl_accepted)
                binding!!.tvOrderStatus!!.setTextColor(resources.getColor(R.color.greenDark))
                binding!!.tvNoOfItems!!.text =
                    getString(R.string.lbl_ordered_items).toUpperCase()
            }
            AppConstants.DELIVERED ->
            {
                binding!!.tvOrderStatus!!.text = getString(R.string.lbl_delivered)
                binding!!.tvOrderStatus!!.setTextColor(resources.getColor(R.color.greenDark))
                binding!!.tvNoOfItems!!.text = getString(R.string.lbl_ordered_items).toUpperCase()
            }
            AppConstants.DECLINED ->
            {
                binding!!.tvOrderStatus!!.setText(R.string.lbl_declined)
                binding!!.tvOrderStatus!!.setTextColor(resources.getColor(R.color.redDark))
                binding!!.tvNoOfItems!!.text =
                    getString(R.string.lbl_declined_ordered_items).toUpperCase()
            }
            AppConstants.ORDER_CANCELLED ->
            {
                binding!!.tvOrderStatus!!.setText(R.string.lbl_cancelled)
                binding!!.tvOrderStatus!!.setTextColor(resources.getColor(R.color.redDark))
                binding!!.tvNoOfItems!!.text =
                    getString(R.string.lbl_cancelled_ordered_items).toUpperCase()
            }
            AppConstants.PENDING ->
            {
                binding!!.tvOrderStatus!!.setText(R.string.lbl_pending)
                binding!!.tvOrderStatus!!.setTextColor(resources.getColor(R.color.orangeDark))
                binding!!.tvNoOfItems!!.text = getString(R.string.lbl_ordered_items).toUpperCase()
            }
        }
    }

    private fun doAccept()
    {
        val orderStatusInput = OrderStatusChangeInput()
        orderStatusInput.orderId = orderId
        orderStatusInput.status = AppConstants.ACCEPT
        onlineOrderStatusChangeViewModel.changeOrderStatus(orderStatusInput)
        OiyaaPlayer.getInstance(requireContext()).stop()
    }

    private fun doReject()
    {
        val orderStatusInput = OrderStatusChangeInput()
        orderStatusInput.orderId = orderId
        orderStatusInput.status = AppConstants.DECLINED
        onlineOrderStatusChangeViewModel.changeOrderStatus(orderStatusInput)
    }

    private fun doComplete()
    {
        val orderStatusInput = OrderStatusChangeInput()
        orderStatusInput.orderId = orderId
        orderStatusInput.status = AppConstants.DELIVERED
        onlineOrderStatusChangeViewModel.changeOrderStatus(orderStatusInput)
    }

    override fun onDestroy()
    {
        super.onDestroy()
        //getLocalValuActivity().unregisterReceiver(br);
        dashBoardHandler!!.setPreviousTitle()
    }

    companion object
    {
        private val TAG = OrderDetailsFragment::class.java.simpleName
    }
}