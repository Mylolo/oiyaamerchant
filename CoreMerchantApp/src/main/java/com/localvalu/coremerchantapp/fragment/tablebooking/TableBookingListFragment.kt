package com.localvalu.coremerchantapp.fragment.tablebooking

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.localvalu.coremerchantapp.LocalValuFragment
import com.localvalu.coremerchantapp.R
import com.localvalu.coremerchantapp.base.Resource
import com.localvalu.coremerchantapp.data.input.TableBookingListInput
import com.localvalu.coremerchantapp.data.input.TableBookingStatusChangeInput
import com.localvalu.coremerchantapp.data.output.tablebooking.TableBookingList
import com.localvalu.coremerchantapp.handler.DashBoardHandler
import com.utils.helper.recyclerview.RVItemAnimator
import com.localvalu.coremerchantapp.databinding.FragmentTableBookingListBinding
import com.localvalu.coremerchantapp.extensions.handleAPIError
import com.localvalu.coremerchantapp.extensions.showMessage
import com.localvalu.coremerchantapp.fragment.adapter.TableBookingListAdapter
import com.localvalu.coremerchantapp.fragment.myappointments.MyAppointmentListFragment
import com.localvalu.coremerchantapp.listeners.TableBookingItemClickListener
import com.localvalu.coremerchantapp.tablebookings.viewmodel.TableBookingViewModel
import com.localvalu.coremerchantapp.utils.AppUtils
import com.localvalu.coremerchantapp.viewmodel.DashboardViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TableBookingListFragment : LocalValuFragment(), View.OnClickListener,TableBookingItemClickListener
{
    private var binding: FragmentTableBookingListBinding?=null;
    private val dashboardViewModel: DashboardViewModel by viewModels()
    private val viewModel: TableBookingViewModel by viewModels()
    private var tableBookingListAdapter: TableBookingListAdapter? = null
    private var dashBoardHandler: DashBoardHandler? = null
    private var retailerId:String="";
    private var tableBookingListBroadCastReceiver: TableBookingListBroadCastReceiver? = null
    private var position = -1;

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        registerMyReceiver()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?
    {
        binding = FragmentTableBookingListBinding.inflate(inflater,container,false)
        setHasOptionsMenu(true)
        retainInstance = true
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        super.onViewCreated(view, savedInstanceState)
        setProperties()
    }

    private fun setProperties()
    {
        val linearLayoutManager = LinearLayoutManager(context)
        binding!!.recyclerView.layoutManager = linearLayoutManager
        binding!!.recyclerView.itemAnimator = RVItemAnimator()
        binding!!.recyclerView.addItemDecoration(
            DividerItemDecoration(
                context,
                linearLayoutManager.orientation
            )
        )
        binding!!.recyclerView.setHasFixedSize(true)
        tableBookingListAdapter = TableBookingListAdapter(this)
        binding!!.recyclerView.adapter = tableBookingListAdapter
        binding!!.layoutError.btnRetry.setOnClickListener{
            doGetTableBookingsList()
        }
        observeSubscribers()
    }

    /**
     * This method is responsible to register an action to BroadCastReceiver
        */
    private fun registerMyReceiver()
    {
        try
        {
            val intentFilter = IntentFilter()
            intentFilter.addAction(AppUtils.BROADCAST_ACTION_TABLE_BOOKING_LIST)
            requireContext().registerReceiver(tableBookingListBroadCastReceiver, intentFilter)
        } catch (ex: Exception)
        {
            ex.printStackTrace()
        }
    }

    private fun observeSubscribers()
    {
        dashboardViewModel.user.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            this.retailerId = it.retailerId.toString()
            doGetTableBookingsList()
        })
        viewModel.tableBookingResponse.observe(viewLifecycleOwner, Observer { response->
            when(response)
            {
                is Resource.Loading->
                {
                    showProgress()
                }
                is Resource.Success->
                {
                    if(response.value.tableBookingListResult.errorDetail.errorCode==0)
                    {
                        setRecyclerview(response.value.tableBookingListResult.tableBookingList)
                    }
                    else
                    {
                        showErrorView(response.value.tableBookingListResult.errorDetail.errorMessage,true)
                    }
                }
                is Resource.Failure->
                {
                    handleAPIError(response){
                        doGetTableBookingsList()
                    }
                    showErrorView(getString(R.string.lbl_could_not_process_your_request),true)
                }
            }
        })
        viewModel.tableBookingAcceptDeclineResponse.observe(viewLifecycleOwner, Observer { response->
            when(response)
            {
                is Resource.Loading->
                {
                    tableBookingListAdapter?.currentList?.toMutableList()?.get(position)?.isUpdating=true
                    tableBookingListAdapter?.notifyItemChanged(position)
                }
                is Resource.Success->
                {
                    if(response.value.tableBookingStatusChangeResult.errorDetail.errorCode==0)
                    {
                        val cList = tableBookingListAdapter?.currentList?.toMutableList()
                        cList?.removeAt(position)
                        tableBookingListAdapter?.submitList(cList)

                        if (cList != null)
                        {
                            setRecyclerview(cList)
                        }

                        showMessage(response.value.tableBookingStatusChangeResult.changeStatusResult.status)
                    }
                    else
                    {
                        showErrorView(response.value.tableBookingStatusChangeResult.errorDetail.errorMessage,true)

                        val cList = tableBookingListAdapter?.currentList?.toMutableList()
                        cList?.get(position)?.isUpdating=false
                        tableBookingListAdapter?.submitList(cList)
                    }
                }
                is Resource.Failure->
                {
                    handleAPIError(response){
                    }
                    showErrorView(getString(R.string.lbl_could_not_process_your_request),true)
                    val cList = tableBookingListAdapter?.currentList?.toMutableList()
                    cList?.get(position)?.isUpdating=false
                    tableBookingListAdapter?.submitList(cList)
                }
            }
        })
        dashboardViewModel?.getUser()
    }

    private fun setRecyclerview(items:List<TableBookingList>)
    {
        if(items.size>0) showListView()
        else showErrorView(getString(R.string.lbl_no_booking_requests),false)
        tableBookingListAdapter?.submitList(items)

    }

    override fun onAttach(context: Context)
    {
        super.onAttach(context)
        dashBoardHandler = context as DashBoardHandler
    }

    override fun onDetach()
    {
        super.onDetach()
        dashBoardHandler = null
    }

    override fun onClick(view: View)
    {
        val id = view.id
        when (id)
        {
        }
    }

    private fun doGetTableBookingsList()
    {
        retailerId?.let {
            val tableBookingListInput:TableBookingListInput = TableBookingListInput()
            tableBookingListInput.retailerId=it
            viewModel?.tableBookings(tableBookingListInput)
        }
    }

    private fun doAcceptOrDecline(tableBookId:String,status:String)
    {
        retailerId?.let {
            val tableBookingStatusChangeInput= TableBookingStatusChangeInput()
            tableBookingStatusChangeInput.tableBookid = tableBookId
            tableBookingStatusChangeInput.status=status
            viewModel?.acceptOrDecline(tableBookingStatusChangeInput)
        }
    }

    private fun showProgress()
    {
        binding!!.recyclerView.visibility = View.GONE
        binding!!.layoutProgress.layoutRoot.visibility = View.VISIBLE
        binding!!.layoutError.layoutRoot.visibility = View.GONE
    }

    private fun showListView()
    {
        binding!!.recyclerView.visibility = View.VISIBLE
        binding!!.layoutProgress.layoutRoot.visibility = View.GONE
        binding!!.layoutError.layoutRoot.visibility = View.GONE
    }

    private fun showErrorView(message: String,showRetry:Boolean)
    {
        binding!!.recyclerView.visibility = View.GONE
        binding!!.layoutProgress.layoutRoot.visibility = View.GONE
        binding!!.layoutError.layoutRoot.visibility = View.VISIBLE
        binding!!.layoutError.tvError.setText(message)
        if(showRetry) binding!!.layoutError.btnRetry.visibility=View.VISIBLE
        else binding!!.layoutError.btnRetry.visibility=View.GONE
    }

    override fun onDestroy()
    {
        super.onDestroy()
        requireContext().unregisterReceiver(tableBookingListBroadCastReceiver)
    }

    companion object
    {
        private val TAG = MyAppointmentListFragment::class.java.simpleName
    }

    override fun onTableBookingItemClicked(position: Int, tableBookingList: TableBookingList?)
    {

    }

    override fun onAcceptClicked(position: Int, tableBookingList: TableBookingList?)
    {
        this.position = position
        doAcceptOrDecline(tableBookingList?.tableBookId.toString(),"0")
    }

    override fun onDeclineClicked(position: Int, tableBookingList: TableBookingList?)
    {
        this.position = position
        doAcceptOrDecline(tableBookingList?.tableBookId.toString(),"2")
    }

    internal inner class TableBookingListBroadCastReceiver : BroadcastReceiver()
    {
        private val TAG = TableBookingListBroadCastReceiver::class.java.name
        override fun onReceive(context: Context, intent: Intent)
        {
            try
            {
                Log.d(TAG, "onReceive() called")
                if (intent.action == AppUtils.BROADCAST_ACTION_TABLE_BOOKING_LIST)
                {
                    tableBookingListAdapter?.submitList(intent.getParcelableExtra(AppUtils.BUNDLE_BOOKING_TABLE_LIST_RESULT))
                }
            } catch (ex: Exception)
            {
                ex.printStackTrace()
            }
        }
    }

}