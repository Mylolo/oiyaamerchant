package com.localvalu.coremerchantapp.fragment.trackandtrace;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.ResultPoint;
import com.google.zxing.client.android.BeepManager;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;
import com.journeyapps.barcodescanner.DefaultDecoderFactory;
import com.localvalu.coremerchantapp.LocalValuFragment;
import com.localvalu.coremerchantapp.R;
import com.localvalu.coremerchantapp.data.output.trace.VisitEntryResult;
import com.localvalu.coremerchantapp.handler.DashBoardHandler;
import com.localvalu.coremerchantapp.utils.FontUtilis;
import com.utils.base.Task;
import com.utils.validator.Validator;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class ScanVisitFragment extends LocalValuFragment implements View.OnClickListener
{

    private static final String TAG = ScanVisitFragment.class.getSimpleName();

    private DecoratedBarcodeView decoratedBarcodeView;

    private DashBoardHandler dashBoardHandler;
    private AppCompatEditText etQRCode;
    private AppCompatTextView tvSuccess;
    private AppCompatImageView ivSuccess;
    private String lastText;
    private BeepManager beepManager;
    private Drawable drawableSuccess;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_visit_scan, container, false);
        setHasOptionsMenu(true);
        setRetainInstance(true);

        beepManager = new BeepManager(getActivity());

        decoratedBarcodeView = view.findViewById(R.id.scanViewVisit);
        decoratedBarcodeView.setStatusText("Scan Your QR Code");
        decoratedBarcodeView.getStatusView().setTypeface(FontUtilis.getInstance().comfortaaRegular(getContext()));
        decoratedBarcodeView.getBarcodeView().setDecoderFactory(new DefaultDecoderFactory(Arrays.asList(BarcodeFormat.QR_CODE)));
        decoratedBarcodeView.initializeFromIntent(getLocalValuActivity().getIntent());
        decoratedBarcodeView.decodeContinuous(new BarcodeCallback()
        {
            @Override
            public void barcodeResult(BarcodeResult result)
            {
                beepManager.playBeepSoundAndVibrate();

                if (!Validator.isValid(result.getText()) || result.getText().equals(lastText))
                    return;

                lastText = result.getText();

                setTimeDoVisitEntry();
            }

            @Override
            public void possibleResultPoints(List<ResultPoint> resultPoints)
            {

            }
        });

        etQRCode = view.findViewById(R.id.etQRCode);
        etQRCode.setText("IDL");
        view.findViewById(R.id.btnSubmit).setOnClickListener(this::onClick);

        drawableSuccess = getResources().getDrawable(R.drawable.ic_baseline_check_circle_outline_24);
        tvSuccess = view.findViewById(R.id.tvSuccess);
        ivSuccess  = view.findViewById(R.id.ivSuccess);
        drawableSuccess.setColorFilter(ContextCompat.getColor(requireContext(),R.color.greenDark), PorterDuff.Mode.SRC_ATOP);
        tvSuccess.setVisibility(View.GONE);
        ivSuccess.setVisibility(View.GONE);
        return view;
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);

        dashBoardHandler = (DashBoardHandler) context;
    }

    @Override
    public void onDetach()
    {
        super.onDetach();

        dashBoardHandler = null;
    }

    @Override
    public void onResume()
    {
        super.onResume();

        decoratedBarcodeView.resume();
    }

    @Override
    public void onPause()
    {
        super.onPause();

        decoratedBarcodeView.pause();
    }

    private void setTimeDoVisitEntry()
    {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm a");
        TimeZone obj = TimeZone.getTimeZone("Europe/London");
        simpleDateFormat.setTimeZone(obj);
        String strDateTime = simpleDateFormat.format(new Date());
        doVisitEntry(lastText,strDateTime);
    }

    private void doVisitEntry(String qrCode,String visitDate)
    {
        tvSuccess.setVisibility(View.GONE);
        ivSuccess.setVisibility(View.GONE);
        decoratedBarcodeView.pause();
        //setDummyData();
        //showListView();
        dashBoardHandler.doInsertVisitEntry(qrCode, visitDate, new Task<VisitEntryResult>()
        {
            @Override
            public void onSuccess(VisitEntryResult result)
            {
                if(result!=null)
                {
                    showTVSuccess("");
                    lastText="";
                }
                else countDownTimer.start();

            }

            @Override
            public void onError(String message)
            {
                 countDownTimer.start();
            }
        });
    }

    private void showTVSuccess(String userName)
    {
        StringBuilder strWelcome = new StringBuilder();
        strWelcome.append(getString(R.string.lbl_welcome)).append(" ");
        strWelcome.append(userName);
        tvSuccess.setText(strWelcome.toString().toUpperCase());
        countDownTimer.start();
        tvSuccess.setVisibility(View.VISIBLE);
        ivSuccess.setVisibility(View.VISIBLE);
    }

    CountDownTimer countDownTimer = new CountDownTimer(5000,1000)
    {

        @Override
        public void onTick(long l)
        {

        }

        @Override
        public void onFinish()
        {
            tvSuccess.setVisibility(View.GONE);
            ivSuccess.setVisibility(View.GONE);
            decoratedBarcodeView.resume();
        }
    };

    @Override
    public void onClick(View view) {
        int id = view.getId();

        if (id == R.id.btnSubmit)
        {
            String qrCode = etQRCode.getText().toString();

            if (!Validator.isValid(qrCode) || qrCode.equals("IDL"))
            {
                getLocalValuActivity().showToast("Enter QR code");
                return;
            }

            lastText = qrCode;
            setTimeDoVisitEntry();
        }
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        if(countDownTimer!=null) countDownTimer.cancel();
    }
}
