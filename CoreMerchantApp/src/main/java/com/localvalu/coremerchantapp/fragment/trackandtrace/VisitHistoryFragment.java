package com.localvalu.coremerchantapp.fragment.trackandtrace;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.localvalu.coremerchantapp.LocalValuFragment;
import com.localvalu.coremerchantapp.R;
import com.localvalu.coremerchantapp.data.output.trace.TraceHistoryReportResult;
import com.localvalu.coremerchantapp.fragment.adapter.VisitHistoryAdapter;
import com.localvalu.coremerchantapp.handler.DashBoardHandler;
import com.utils.base.Task;
import com.utils.helper.recyclerview.RVItemAnimator;
import com.utils.util.DateTimeUtil;
import com.utils.validator.Validator;

import java.util.Calendar;

public class VisitHistoryFragment extends LocalValuFragment implements View.OnClickListener
{

    private static final String TAG = VisitHistoryFragment.class.getSimpleName();

    private AppCompatTextView tvFromDate, tvToDate, tvEmpty;
    private RecyclerView rvVisitHistory;
    private VisitHistoryAdapter visitHistoryAdapter;

    private DashBoardHandler dashBoardHandler;
    private long fromDate;
    private long toDate;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_visit_scan_view_history, container, false);
        setHasOptionsMenu(true);
        setRetainInstance(true);

        tvFromDate = view.findViewById(R.id.tvFromDate);
        tvFromDate.setOnClickListener(this::onClick);
        fromDate = DateTimeUtil.getCalendar().getTimeInMillis();
        setFromDate(DateTimeUtil.getCalendar().getTimeInMillis());

        tvToDate = view.findViewById(R.id.tvToDate);
        tvToDate.setOnClickListener(this::onClick);
        toDate = DateTimeUtil.getCalendar().getTimeInMillis();
        setToDate(DateTimeUtil.getCalendar().getTimeInMillis());

        view.findViewById(R.id.btnViewHistory).setOnClickListener(this::onClick);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rvVisitHistory = view.findViewById(R.id.rvVisitHistory);
        rvVisitHistory.setLayoutManager(linearLayoutManager);
        rvVisitHistory.setItemAnimator(new RVItemAnimator());
        rvVisitHistory.addItemDecoration(new DividerItemDecoration(getContext(), linearLayoutManager.getOrientation()));
        rvVisitHistory.setHasFixedSize(true);

        tvEmpty = view.findViewById(R.id.tvEmpty);

        if (Validator.isValid(visitHistoryAdapter))
            rvVisitHistory.setAdapter(visitHistoryAdapter);

        populateData();
        return view;
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);

        dashBoardHandler = (DashBoardHandler) context;
    }

    @Override
    public void onDetach()
    {
        super.onDetach();

        dashBoardHandler = null;
    }

    @Override
    public void onClick(View view)
    {
        int id = view.getId();

        if (id == R.id.tvFromDate)
        {
            showDatePicker(true);
        }
        else if (id == R.id.tvToDate)
        {
            showDatePicker(false);
        }
        else if (id == R.id.btnViewHistory)
        {
            populateData();
        }
    }

    private void setFromDate(long date)
    {
        String formattedDate = DateTimeUtil.getFormattedDate(date, DateTimeUtil.dd_MM_yyyy);
        tvFromDate.setText(formattedDate);
    }

    private void setToDate(long date)
    {
        String formattedDate = DateTimeUtil.getFormattedDate(date, DateTimeUtil.dd_MM_yyyy);
        tvToDate.setText(formattedDate);
    }

    private void populateData()
    {
        String strFromDate = DateTimeUtil.getFormattedDate(fromDate, DateTimeUtil.yyyy__MM__dd);
        String strToDate = DateTimeUtil.getFormattedDate(toDate, DateTimeUtil.yyyy__MM__dd);

        dashBoardHandler.doGetVisitHistory(strFromDate, strToDate, new Task<TraceHistoryReportResult>()
        {
            @Override
            public void onSuccess(TraceHistoryReportResult result)
            {
                if (Validator.isValid(result.getTraceHistoryReportList()) && !result.getTraceHistoryReportList().isEmpty())
                {
                    if (Validator.isValid(visitHistoryAdapter))
                    {
                        visitHistoryAdapter.addItems(result.getTraceHistoryReportList());
                    }
                    else
                    {
                        visitHistoryAdapter = new VisitHistoryAdapter(result.getTraceHistoryReportList(), dashBoardHandler);
                        rvVisitHistory.setAdapter(visitHistoryAdapter);
                    }
                    showListView();
                }
                else
                {
                    showEmptyView(getString(R.string.lbl_no_visitors_found));
                }
            }

            @Override
            public void onError(String message)
            {
                showEmptyView(getString(R.string.lbl_no_transactions_found));
            }
        });
    }

    private void showDatePicker(Boolean isStartDate)
    {
        Calendar calendarToday = DateTimeUtil.getCalendar();
        int day = calendarToday.get(Calendar.DAY_OF_MONTH);
        int month = calendarToday.get(Calendar.MONTH);
        int year = calendarToday.get(Calendar.YEAR);

        long today = calendarToday.getTimeInMillis();

        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), R.style.DatePickerDialogTheme,(view1, year1, month1, dayOfMonth) ->
        {
            Calendar selectedDate = DateTimeUtil.getCalendar();
            selectedDate.set(year1, month1, dayOfMonth);

            if (isStartDate)
            {
                fromDate = selectedDate.getTimeInMillis();
                setFromDate(fromDate);
            }
            else
            {
                toDate = selectedDate.getTimeInMillis();
                setToDate(toDate);
            }
        }, year, month, day);

        if (isStartDate)
        {
            //datePickerDialog.getDatePicker().setMinDate(today);
            datePickerDialog.getDatePicker().setMaxDate(today);
        }
        else
        {
            datePickerDialog.getDatePicker().setMinDate(fromDate);
            datePickerDialog.getDatePicker().setMaxDate(today);
        }

        datePickerDialog.show();
    }

    private void showListView()
    {
        rvVisitHistory.setVisibility(View.VISIBLE);
        tvEmpty.setVisibility(View.GONE);
    }

    private void showEmptyView(String message)
    {
        tvEmpty.setText(message);
        rvVisitHistory.setVisibility(View.GONE);
        tvEmpty.setVisibility(View.VISIBLE);
    }

}
