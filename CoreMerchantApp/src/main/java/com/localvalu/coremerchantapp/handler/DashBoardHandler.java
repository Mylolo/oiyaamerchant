package com.localvalu.coremerchantapp.handler;

import com.localvalu.coremerchantapp.RestHandler;
import com.localvalu.coremerchantapp.b2b.ui.B2BTransferTokensFragment;
import com.localvalu.coremerchantapp.b2c.ui.B2CTransferTokensFragment;
import com.localvalu.coremerchantapp.data.input.MerchantSelfServeInput;
import com.localvalu.coremerchantapp.data.output.MyTransactionResult;
import com.localvalu.coremerchantapp.data.output.SaleTransactionResult;
import com.localvalu.coremerchantapp.data.output.TokenBalanceResult;
import com.localvalu.coremerchantapp.data.output.TransferLoyaltyTokenResult;
import com.localvalu.coremerchantapp.data.output.driverOld.DriverList;
import com.localvalu.coremerchantapp.data.output.driverOld.DriverNotificationResult;
import com.localvalu.coremerchantapp.data.output.driverOld.DriverProfileResult;
import com.localvalu.coremerchantapp.data.output.myappointments.MyAppointmentListResult;
import com.localvalu.coremerchantapp.data.output.myappointments.MyAppointmentStatusChangeResult;
import com.localvalu.coremerchantapp.data.output.order.OrderDetails;
import com.localvalu.coremerchantapp.data.output.order.OrderDetailsResult;
import com.localvalu.coremerchantapp.data.output.order.OrderListResult;
import com.localvalu.coremerchantapp.data.output.order.OrderStatusChangeResult;
import com.localvalu.coremerchantapp.data.output.selfserve.MerchantSelfServeResult;
import com.localvalu.coremerchantapp.data.output.tablebooking.TableBookingListResult;
import com.localvalu.coremerchantapp.data.output.tablebooking.TableBookingStatusChangeResult;
import com.localvalu.coremerchantapp.data.output.trace.TraceHistoryReportResult;
import com.localvalu.coremerchantapp.data.output.trace.VisitEntryResult;
import com.localvalu.coremerchantapp.fragment.MerchantSelfServeFragment;
import com.localvalu.coremerchantapp.fragment.MyTransactionFragment;
import com.localvalu.coremerchantapp.fragment.ScanFragment;
import com.localvalu.coremerchantapp.fragment.ScanSuccessFragment;
import com.localvalu.coremerchantapp.fragment.SignInFragment;
import com.localvalu.coremerchantapp.fragment.TokenFragment;
import com.localvalu.coremerchantapp.fragment.TransferLoyaltyTokenFragment;
import com.localvalu.coremerchantapp.fragment.driver.DriverListFragment;
import com.localvalu.coremerchantapp.fragment.myappointments.MyAppointmentListFragment;
import com.localvalu.coremerchantapp.fragment.orders.MyOrdersFragment;
import com.localvalu.coremerchantapp.fragment.orders.OrderAtTableDetailsFragment;
import com.localvalu.coremerchantapp.fragment.orders.OrderDetailsFragment;
import com.localvalu.coremerchantapp.fragment.tablebooking.TableBookingListFragment;
import com.localvalu.coremerchantapp.fragment.trackandtrace.ScanVisitFragment;
import com.localvalu.coremerchantapp.fragment.trackandtrace.VisitHistoryFragment;
import com.localvalu.coremerchantapp.merchantdashboard.ui.DashboardFragment;
import com.utils.base.Task;

public interface DashBoardHandler extends RestHandler
{

    SignInFragment signIn();

    MerchantSelfServeFragment register();

    TokenFragment token();

    ScanFragment scan();

    DashboardFragment dashboardFragment();

    B2CTransferTokensFragment b2CTransferTokensFragment();

    B2BTransferTokensFragment b2BTransferTokensFragment();

    ScanSuccessFragment scanSuccess(Double amount, SaleTransactionResult saleTransactionResult);

    MyTransactionFragment myTransaction();

    TransferLoyaltyTokenFragment transferLoyaltyToken();

    MyOrdersFragment myOrdersFragment();

    OrderDetailsFragment orderDetails(String orderId);

    OrderAtTableDetailsFragment orderAtTableDetails(String orderId);

    TableBookingListFragment tableBookingList();

    MyAppointmentListFragment myAppointmentList();

    ScanVisitFragment scanVisitFragment();

    VisitHistoryFragment visitHistoryFragment();

    DriverListFragment driverListFragment(OrderDetails orderDetails);

    void signOut();

    void doGetMyTokenBalance(Task<TokenBalanceResult> task);

    void doTransferLoyaltyToken(String currentBalance, String transferAmount, String balanceAfterTransfer, String toAccountId, Task<TransferLoyaltyTokenResult> task);

    void doGetOrderList(String retailerId, Task<OrderListResult> task);

    void doGetOrderDetails(String orderId, Task<OrderDetailsResult> task);

    void doChangeOrderStatus(String orderId, String status, Task<OrderStatusChangeResult> task);

    void doGetOrderAtTableList(String retailerId, Task<OrderListResult> task);

    void doGetOrderAtTableDetails(String orderId, Task<OrderDetailsResult> task);

    void doInsertVisitEntry(String QRCode, String visitDate, Task<VisitEntryResult> task);

    void doGetVisitHistory(String fromDate, String toDate, Task<TraceHistoryReportResult> task);

    void doGetDriversList(String strRetailerId,Task<DriverList> task);

    void doNotifyToDrivers(int driverId, int orderId, int retailerId, Task<DriverNotificationResult> task);

    void doGetDriverProfile(String driverId, Task<DriverProfileResult> task);

    void setPreviousTitle();

    void startService();

    void stopService();

    int deviceType();

    void showDeviceOrderDetails(String orderId);

    void showDeviceOrderAtTableDetails(String orderId);

    void doMoreOnScanTransactionSuccess(String balanceToPay);

    void showPrintScreen(OrderDetails orderDetails);

    void onBackButtonPressed();

}
