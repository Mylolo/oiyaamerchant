package com.localvalu.coremerchantapp.handler

import androidx.fragment.app.Fragment
import com.localvalu.coremerchantapp.R

interface TempHandler
{
    fun updateFragment(fragment: Fragment,title:String, addToBackStack: Boolean, replace: Boolean)
}