package com.localvalu.coremerchantapp.listeners;

import android.os.Message;

public interface AppReceiver
{
    void onReceiveResult(Message message);
}
