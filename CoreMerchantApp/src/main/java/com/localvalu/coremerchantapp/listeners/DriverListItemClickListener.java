package com.localvalu.coremerchantapp.listeners;

import com.localvalu.coremerchantapp.data.output.driverOld.DriverDetails;

public interface DriverListItemClickListener
{
    void onDriverItemClicked(int position, DriverDetails driverDetails);
}
