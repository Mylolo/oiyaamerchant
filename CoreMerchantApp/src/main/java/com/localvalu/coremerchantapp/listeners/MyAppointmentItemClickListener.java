package com.localvalu.coremerchantapp.listeners;


import com.localvalu.coremerchantapp.data.output.myappointments.MyAppointmentList;

public interface MyAppointmentItemClickListener
{
    void onMyAppointmentItemClicked(int position, MyAppointmentList myAppointmentList);
    void onMyAppointmentAccepted(int position, MyAppointmentList myAppointmentList);
    void onMyAppointmentDeclined(int position, MyAppointmentList myAppointmentList);
}
