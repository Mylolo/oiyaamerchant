package com.localvalu.coremerchantapp.listeners;


import com.localvalu.coremerchantapp.data.output.order.OrderListData;

public interface OrderItemClickListener
{
    void onOrderItemClicked(int position, OrderListData orderListData);
}
