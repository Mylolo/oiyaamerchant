package com.localvalu.coremerchantapp.listeners;


import com.localvalu.coremerchantapp.data.output.tablebooking.TableBookingList;

public interface TableBookingItemClickListener
{
    void onTableBookingItemClicked(int position, TableBookingList tableBookingList);
    void onAcceptClicked(int position, TableBookingList tableBookingList);
    void onDeclineClicked(int position, TableBookingList tableBookingList);
}
