package com.localvalu.coremerchantapp.merchantdashboard.api

import com.localvalu.coremerchantapp.BaseApi
import com.localvalu.coremerchantapp.merchantdashboard.model.MerchantDashboardRequest
import com.localvalu.coremerchantapp.merchantdashboard.model.MerchantDashboardResponse
import retrofit2.http.Body
import retrofit2.http.POST

interface MerchantDashboardApi : BaseApi
{
    @POST("Webadmin/MerchantTransactiondetails")
    suspend fun merchantDashboard(@Body merchantDashboardRequest: MerchantDashboardRequest): MerchantDashboardResponse
}