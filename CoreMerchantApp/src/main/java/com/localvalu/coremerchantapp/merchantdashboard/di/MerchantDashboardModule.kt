package com.localvalu.coremerchantapp.merchantdashboard.di

import android.content.Context
import com.localvalu.coremerchantapp.merchantdashboard.api.MerchantDashboardApi
import com.localvalu.coremerchantapp.merchantdashboard.repository.MerchantDashboardRepository
import com.localvalu.coremerchantapp.network.RemoteDataSource
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class MerchantDashboardModule
{
    @Provides
    fun providesMerchantDashboardApi(remoteDataSource: RemoteDataSource,
                                     @ApplicationContext context: Context): MerchantDashboardApi
    {
        return remoteDataSource.buildApiPhpOne(MerchantDashboardApi::class.java,context)
    }

    @Provides
    fun providesPassionListRepository(merchantDashboardApi: MerchantDashboardApi):MerchantDashboardRepository
    {
        return MerchantDashboardRepository(merchantDashboardApi)
    }
}