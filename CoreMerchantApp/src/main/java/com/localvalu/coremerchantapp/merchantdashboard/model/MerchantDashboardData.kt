package com.localvalu.coremerchantapp.merchantdashboard.model

import ErrorDetails
import com.google.gson.annotations.SerializedName

data class MerchantDashboardData(@SerializedName("ErrorDetails") val errorDetails: ErrorDetails,
                                 @SerializedName("currentMonthTransaction") val currentMonthTransaction:String,
                                 @SerializedName("currentmonthTokensawarded") val currentmonthTokensawarded:String,
                                 @SerializedName("currentmonthsales") val currentmonthsales:String,
                                 @SerializedName("OverallTransaction") val overAllTransactions:String,
                                 @SerializedName("OverallTokensawarded") val overallTokensawarded:String,
                                 @SerializedName("Overallsales") val overallSales:String)
