package com.localvalu.coremerchantapp.merchantdashboard.model

import com.google.gson.annotations.SerializedName

data class MerchantDashboardResponse(@SerializedName("MerchantTransactiondetails")
                                   val merchantDashboardResult:MerchantDashboardResult)
