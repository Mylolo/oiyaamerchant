package com.localvalu.coremerchantapp.merchantdashboard.model

import com.google.gson.annotations.SerializedName

data class MerchantDashboardResult(@SerializedName("MerchantTransactiondetailsResult")
                                   val merchantDashboardData:MerchantDashboardData)
