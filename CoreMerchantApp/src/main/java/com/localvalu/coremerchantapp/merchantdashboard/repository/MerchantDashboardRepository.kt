package com.localvalu.coremerchantapp.merchantdashboard.repository

import com.localvalu.coremerchantapp.base.BaseRepository
import com.localvalu.coremerchantapp.merchantdashboard.api.MerchantDashboardApi
import com.localvalu.coremerchantapp.merchantdashboard.model.MerchantDashboardRequest

class MerchantDashboardRepository (private val api: MerchantDashboardApi): BaseRepository(api)
{
    suspend fun merchantDashboard(merchantDashboardRequest: MerchantDashboardRequest) = safeApiCall {
        api.merchantDashboard(merchantDashboardRequest)
    }
}