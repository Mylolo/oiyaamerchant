package com.localvalu.coremerchantapp.merchantdashboard.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.localvalu.coremerchantapp.base.Resource
import com.localvalu.coremerchantapp.R
import com.localvalu.coremerchantapp.databinding.FragmentMerchantDashboardBinding
import com.localvalu.coremerchantapp.extensions.handleAPIError
import com.localvalu.coremerchantapp.extensions.shorten
import com.localvalu.coremerchantapp.extensions.showMessage
import com.localvalu.coremerchantapp.merchantdashboard.model.MerchantDashboardData
import com.localvalu.coremerchantapp.merchantdashboard.model.MerchantDashboardRequest
import com.localvalu.coremerchantapp.merchantdashboard.viewmodel.MerchantDashboardViewModel
import com.localvalu.coremerchantapp.token.model.MerchantTokenBalanceRequest
import com.localvalu.coremerchantapp.token.viewmodel.MerchantTokenBalanceViewModel
import com.localvalu.coremerchantapp.viewmodel.DashboardViewModel
import com.utils.base.AppBaseFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DashboardFragment : AppBaseFragment()
{
    val TAG = "DashboardFragment"
    var binding:FragmentMerchantDashboardBinding? = null;
    val viewModel:MerchantDashboardViewModel by viewModels()
    val dashboardViewModel:DashboardViewModel by viewModels()
    val merchantTokenBalanceViewModel:MerchantTokenBalanceViewModel by viewModels()
    val currency:String by lazy {
        requireContext().getString(R.string.symbol_pound)
    }
    var lastMerchantDashboardRequest:MerchantDashboardRequest? = null;

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?
    {
        binding = FragmentMerchantDashboardBinding.inflate(inflater,container,false)
        return binding?.root;
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        observeSubscribers()
        setProperties()
        getDashboard()
    }

    private fun setProperties()
    {
        binding!!.layoutError.btnRetry.setOnClickListener{
            getDashboard()
        }
    }

    private fun observeSubscribers()
    {
        viewModel?.merchantDashboardResponse?.observe(viewLifecycleOwner,Observer { response->
            when(response)
            {

                is Resource.Loading ->
                {
                    showProgress()
                }
                is Resource.Success ->
                {
                    showContent()
                    when(response.value.merchantDashboardResult.merchantDashboardData.errorDetails.errorCode)
                    {
                        0->
                        {
                            setData(response.value.merchantDashboardResult.merchantDashboardData)
                        }
                        1->
                        {
                            showMessage(response.value.merchantDashboardResult.merchantDashboardData.errorDetails.errorMessage)
                        }
                    }
                }
                is Resource.Failure ->
                {
                    showError(getString(R.string.lbl_could_not_process_your_request),false)
                    handleAPIError(response) { getDashboard() }
                }

            }
        })
        dashboardViewModel?.user.observe(viewLifecycleOwner, Observer {
            it?.let {
                lastMerchantDashboardRequest = MerchantDashboardRequest("",it.retailerId.toString())
                viewModel?.merchantDashboard(lastMerchantDashboardRequest!!)
                merchantTokenBalanceViewModel?.
                merchantTokenBalance(MerchantTokenBalanceRequest("",40527018.toString(),
                    it.retailerId.toString()))
            }
        })
        merchantTokenBalanceViewModel.merchantTokenBalanceResponse?.observe(viewLifecycleOwner,Observer {  response->
            when(response)
            {
                is Resource.Loading ->
                {

                }
                is Resource.Success ->
                {
                    when(response.value.result.errorDetail.errorCode)
                    {
                        0->
                        {
                            binding?.tvTokenBalance?.text = response.value.result.currentTokenBalance.shorten()
                        }
                        1->
                        {

                        }
                    }

                }
                is Resource.Failure ->
                {

                }
            }
            })
    }

    private fun getDashboard()
    {
        Log.d(TAG,"getDashboard")
        dashboardViewModel?.getUser()
    }

    private fun setData(merchantDashboardData: MerchantDashboardData)
    {
        binding?.tvSales?.text = currency + " " + merchantDashboardData.currentmonthsales
        binding?.tvTokensAwarded?.text = merchantDashboardData.currentmonthTokensawarded
        binding?.tvNumberOfTransactions?.text = merchantDashboardData.currentMonthTransaction
        binding?.tvSalesPromotionSummary?.text = currency + " " +merchantDashboardData.overallSales
        binding?.tvTokensAwardedPromotionSummary?.text = merchantDashboardData.overallTokensawarded
        binding?.tvNumberOfTransactionsPromotionSummary?.text = merchantDashboardData.overAllTransactions
        binding?.tvTokensOverDraft?.text = "0"
    }

    private fun showProgress()
    {
        binding!!.layoutProgress.layoutRoot.visibility = View.VISIBLE
        binding!!.cnlContent.visibility = View.GONE
        binding!!.layoutError.layoutRoot.visibility = View.GONE
    }

    private fun showContent()
    {
        binding!!.layoutProgress.layoutRoot.visibility = View.GONE
        binding!!.cnlContent.visibility = View.VISIBLE
        binding!!.layoutError.layoutRoot.visibility = View.GONE
    }

    private fun showError(strMessage: String, retry: Boolean)
    {
        binding!!.layoutProgress.layoutRoot.visibility = View.GONE
        binding!!.cnlContent.visibility = View.GONE
        binding!!.layoutError.layoutRoot.visibility = View.VISIBLE
        binding!!.layoutError.tvError.text = strMessage
        if (retry) binding!!.layoutError.btnRetry.visibility = View.VISIBLE
        else binding!!.layoutError.btnRetry.visibility = View.GONE
    }



}
