package com.localvalu.coremerchantapp.merchantdashboard.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.localvalu.coremerchantapp.base.Resource
import com.localvalu.coremerchantapp.merchantdashboard.model.MerchantDashboardRequest
import com.localvalu.coremerchantapp.merchantdashboard.model.MerchantDashboardResponse
import com.localvalu.coremerchantapp.merchantdashboard.repository.MerchantDashboardRepository

import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MerchantDashboardViewModel @Inject constructor(val repository:MerchantDashboardRepository) : ViewModel()
{
    private val _merchantDashboardResponse: MutableLiveData<Resource<MerchantDashboardResponse>> = MutableLiveData()
    val merchantDashboardResponse: LiveData<Resource<MerchantDashboardResponse>>
        get() = _merchantDashboardResponse

    fun merchantDashboard(merchantDashboardRequest: MerchantDashboardRequest) = viewModelScope.launch {
        _merchantDashboardResponse.value = Resource.Loading
        _merchantDashboardResponse.value = repository.merchantDashboard(merchantDashboardRequest)
    }

}