package com.localvalu.coremerchantapp.merchantselfserve.api

import com.localvalu.coremerchantapp.BaseApi
import com.localvalu.coremerchantapp.data.input.MerchantSelfServeInput
import com.localvalu.coremerchantapp.data.output.selfserve.MerchantSelfServeOutput
import com.localvalu.coremerchantapp.merchantselfserve.model.MerchantSelfServeResponse
import retrofit2.http.Body
import retrofit2.http.POST

interface MerchantSelfServeApi : BaseApi
{
    @POST("MerchantAppV2/MerchantSelfServe")
    suspend fun merchantSelfServe(@Body merchantSelfServeInput: MerchantSelfServeInput):MerchantSelfServeResponse
}