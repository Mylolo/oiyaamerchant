package com.localvalu.coremerchantapp.merchantselfserve.di

import android.content.Context
import com.localvalu.coremerchantapp.merchantselfserve.api.MerchantSelfServeApi
import com.localvalu.coremerchantapp.merchantselfserve.repository.MerchantSelfServerRepository
import com.localvalu.coremerchantapp.network.RemoteDataSource
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class MerchantSelfServeModule
{
    @Provides
    fun providesMerchantSelfServeApi(remoteDataSource: RemoteDataSource,@ApplicationContext context: Context ) :MerchantSelfServeApi
    {
        return remoteDataSource.buildApi(MerchantSelfServeApi::class.java,context)
    }

    @Provides
    fun providesMerchantSelfServeRepository(api:MerchantSelfServeApi) : MerchantSelfServerRepository
    {
        return MerchantSelfServerRepository(api);
    }
}