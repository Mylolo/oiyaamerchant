package com.localvalu.coremerchantapp.merchantselfserve.model


import com.google.gson.annotations.SerializedName
import com.google.gson.annotations.Expose
import com.localvalu.coremerchantapp.data.base.RestBasedErrorDetail

data class InsertMerchantselfserveResult(
    @SerializedName("ErrorDetails")
    @Expose
    val errorDetails: RestBasedErrorDetail
)