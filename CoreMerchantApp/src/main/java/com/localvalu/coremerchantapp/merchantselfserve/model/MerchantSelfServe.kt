package com.localvalu.coremerchantapp.merchantselfserve.model


import com.google.gson.annotations.SerializedName
import com.google.gson.annotations.Expose
import com.localvalu.coremerchantapp.data.base.RestBasedErrorDetail

data class MerchantSelfServe(
    @SerializedName("ErrorDetails")
    @Expose
    val errorDetails: RestBasedErrorDetail,
    @SerializedName("MerchantSelfServeResult")
    @Expose
    val merchantSelfServeResult: MerchantSelfServeResult,
    @SerializedName("status")
    @Expose
    val status: String
)