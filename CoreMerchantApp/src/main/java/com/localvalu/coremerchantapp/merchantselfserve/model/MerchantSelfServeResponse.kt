package com.localvalu.coremerchantapp.merchantselfserve.model


import com.google.gson.annotations.SerializedName
import com.google.gson.annotations.Expose

data class MerchantSelfServeResponse(
    @SerializedName("MerchantSelfServe")
    @Expose
    val merchantSelfServe: MerchantSelfServe
)