package com.localvalu.coremerchantapp.merchantselfserve.repository

import com.localvalu.coremerchantapp.base.BaseRepository
import com.localvalu.coremerchantapp.data.input.MerchantSelfServeInput
import com.localvalu.coremerchantapp.merchantselfserve.api.MerchantSelfServeApi

class MerchantSelfServerRepository(private val api: MerchantSelfServeApi):BaseRepository(api)
{
    suspend fun merchantSelfServe(merchantSelfServeInput: MerchantSelfServeInput) = safeApiCall {
        api.merchantSelfServe(merchantSelfServeInput)
    }
}