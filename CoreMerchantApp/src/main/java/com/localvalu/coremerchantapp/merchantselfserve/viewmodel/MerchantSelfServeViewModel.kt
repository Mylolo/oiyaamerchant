package com.localvalu.coremerchantapp.merchantselfserve.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.localvalu.coremerchantapp.base.Resource
import com.localvalu.coremerchantapp.data.input.MerchantSelfServeInput
import com.localvalu.coremerchantapp.data.output.selfserve.MerchantSelfServeOutput
import com.localvalu.coremerchantapp.merchantselfserve.model.MerchantSelfServeResponse
import com.localvalu.coremerchantapp.merchantselfserve.repository.MerchantSelfServerRepository
import com.localvalu.coremerchantapp.utils.ValidationUtils
import com.utils.validator.Validator
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MerchantSelfServeViewModel @Inject constructor(
    private val repository: MerchantSelfServerRepository):ViewModel()
{

    private val _merchantSelfServeResponse: MutableLiveData<Resource<MerchantSelfServeResponse>> = MutableLiveData()

    val merchantSelfServeResponse: LiveData<Resource<MerchantSelfServeResponse>>
        get() = _merchantSelfServeResponse

    fun merchantSelfServe(merchantSelfServerInput: MerchantSelfServeInput) = viewModelScope.launch {
        _merchantSelfServeResponse.value = Resource.Loading
        _merchantSelfServeResponse.value = repository.merchantSelfServe(merchantSelfServerInput)
    }

    val name: MutableLiveData<String> = MutableLiveData()

    val validName: MutableLiveData<Boolean> = MutableLiveData()

    val contactNumber: MutableLiveData<String> = MutableLiveData()

    val validContactNumber: MutableLiveData<Boolean> = MutableLiveData()

    val email: MutableLiveData<String> = MutableLiveData()

    val validEmail: MutableLiveData<Boolean> = MutableLiveData()

    val postCode: MutableLiveData<String> = MutableLiveData()

    val validPostCode: MutableLiveData<Boolean> = MutableLiveData()

    val businessName: MutableLiveData<String> = MutableLiveData()

    val validBusinessName: MutableLiveData<Boolean> = MutableLiveData()

    val providerName: MutableLiveData<String> = MutableLiveData()

    val validProviderName: MutableLiveData<Boolean> = MutableLiveData()

    val enableSendNow: MutableLiveData<Boolean> = MutableLiveData()

    private fun enableDisableSend()
    {
        enableSendNow.value = validName.value == true &&
                validEmail.value == true && validContactNumber.value == true && validPostCode.value == true
                &&  validBusinessName.value == true && validProviderName.value == true
    }

    fun isValidName()
    {
        validName.value = (name.value?.length!! >0)
        enableDisableSend()
    }

    fun isValidEmail()
    {
        validEmail.value = Validator.isValidEmail(email.value)
        enableDisableSend()
    }

    fun isValidPostCode()
    {
        validPostCode.value = ValidationUtils.isValidPostCode(postCode.value)
        enableDisableSend()
    }

    fun isValidContactNumber()
    {
        validContactNumber.value = ValidationUtils.isValidMobileNo(contactNumber.value)
        enableDisableSend()
    }

    fun isValidBusinessName()
    {
        validBusinessName.value = (businessName.value?.length!! >0)
        enableDisableSend()
    }

    fun isValidProviderName()
    {
        validProviderName.value = (providerName.value?.length!! >0)
        enableDisableSend()
    }

}