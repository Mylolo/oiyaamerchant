package com.localvalu.coremerchantapp.myappointments.api

import com.localvalu.coremerchantapp.BaseApi
import com.localvalu.coremerchantapp.data.input.MyAppointmentListInput
import com.localvalu.coremerchantapp.data.input.MyAppointmentStatusChangeInput
import com.localvalu.coremerchantapp.data.output.myappointments.MyAppointmentListOutput
import com.localvalu.coremerchantapp.data.output.myappointments.MyAppointmentStatusChangeOutput
import retrofit2.http.Body
import retrofit2.http.POST

interface MyAppointmentsApi : BaseApi
{
    @POST("UserRegistration/requestSlotMerchantreport")
    suspend fun myAppointments(@Body myAppointmentListInput: MyAppointmentListInput):MyAppointmentListOutput

    @POST("UserRegistration/slotBookingMerchantaccept")
    suspend fun acceptAppointment(@Body myAppointmentStatusChangeInput: MyAppointmentStatusChangeInput):MyAppointmentStatusChangeOutput

    @POST("UserRegistration/slotBookingMerchantDecline")
    suspend fun declineAppointment(@Body myAppointmentStatusChangeInput: MyAppointmentStatusChangeInput): MyAppointmentStatusChangeOutput

}