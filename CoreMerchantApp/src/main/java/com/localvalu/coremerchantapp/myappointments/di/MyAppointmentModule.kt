package com.localvalu.coremerchantapp.myappointments.di

import android.content.Context
import com.localvalu.coremerchantapp.myappointments.api.MyAppointmentsApi
import com.localvalu.coremerchantapp.myappointments.repository.MyAppointmentsRepository
import com.localvalu.coremerchantapp.network.RemoteDataSource
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class MyAppointmentModule
{
    @Provides
    fun providesMyAppointmentApi(remoteDataSource: RemoteDataSource,
                                 @ApplicationContext context:Context):MyAppointmentsApi
    {
        return (remoteDataSource.buildApi(MyAppointmentsApi::class.java,context))
    }

    @Provides
    fun providesMyAppointmentRepository(myAppointmentsApi: MyAppointmentsApi):MyAppointmentsRepository
    {
        return MyAppointmentsRepository(myAppointmentsApi)
    }

}