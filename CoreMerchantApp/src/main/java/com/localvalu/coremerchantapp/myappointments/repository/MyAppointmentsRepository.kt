package com.localvalu.coremerchantapp.myappointments.repository

import com.localvalu.coremerchantapp.base.BaseRepository
import com.localvalu.coremerchantapp.data.input.MyAppointmentListInput
import com.localvalu.coremerchantapp.data.input.MyAppointmentStatusChangeInput
import com.localvalu.coremerchantapp.myappointments.api.MyAppointmentsApi

class MyAppointmentsRepository(val api: MyAppointmentsApi) : BaseRepository(api)
{
    suspend fun myAppointments(myAppointmentListInput: MyAppointmentListInput) = safeApiCall {
        api.myAppointments(myAppointmentListInput)
    }

    suspend fun acceptAppointment(myAppointmentStatusChangeInput: MyAppointmentStatusChangeInput) = safeApiCall {
        api.acceptAppointment(myAppointmentStatusChangeInput)
    }

    suspend fun declineAppointment(myAppointmentStatusChangeInput: MyAppointmentStatusChangeInput) = safeApiCall {
        api.declineAppointment(myAppointmentStatusChangeInput)
    }

}