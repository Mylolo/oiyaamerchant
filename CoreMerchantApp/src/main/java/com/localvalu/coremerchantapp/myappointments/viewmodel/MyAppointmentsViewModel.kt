package com.localvalu.coremerchantapp.myappointments.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.localvalu.coremerchantapp.base.Resource
import com.localvalu.coremerchantapp.data.input.MyAppointmentListInput
import com.localvalu.coremerchantapp.data.input.MyAppointmentStatusChangeInput
import com.localvalu.coremerchantapp.data.output.myappointments.MyAppointmentChangeStatusResult
import com.localvalu.coremerchantapp.data.output.myappointments.MyAppointmentListOutput
import com.localvalu.coremerchantapp.data.output.myappointments.MyAppointmentStatusChangeOutput
import com.localvalu.coremerchantapp.merchantdashboard.model.MerchantDashboardResponse
import com.localvalu.coremerchantapp.myappointments.repository.MyAppointmentsRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MyAppointmentsViewModel @Inject constructor(val repository: MyAppointmentsRepository):ViewModel()
{
    private val _myAppointmentListResponse:MutableLiveData<Resource<MyAppointmentListOutput>> = MutableLiveData()

    val myAppointmentListResponse: LiveData<Resource<MyAppointmentListOutput>>
        get() = _myAppointmentListResponse

    fun myAppointments(myAppointmentListInput: MyAppointmentListInput) = viewModelScope.launch{
        _myAppointmentListResponse.value = Resource.Loading
        _myAppointmentListResponse.value = repository.myAppointments(myAppointmentListInput)
    }

    private val _myAppointmentAcceptResponse:MutableLiveData<Resource<MyAppointmentStatusChangeOutput>> = MutableLiveData()

    val myAppointmentAcceptResponse:LiveData<Resource<MyAppointmentStatusChangeOutput>>
        get() = _myAppointmentAcceptResponse

    fun acceptAppointment(myAppointmentStatusChangeInput: MyAppointmentStatusChangeInput) = viewModelScope.launch{
        _myAppointmentAcceptResponse.value = Resource.Loading
        _myAppointmentAcceptResponse.value = repository.acceptAppointment(myAppointmentStatusChangeInput)
    }

    private val _myAppointmentDeclineResponse:MutableLiveData<Resource<MyAppointmentStatusChangeOutput>> = MutableLiveData()

    val myAppointmentDeclineResponse:LiveData<Resource<MyAppointmentStatusChangeOutput>>
        get() = _myAppointmentDeclineResponse

    fun declineAppointment(myAppointmentStatusChangeInput: MyAppointmentStatusChangeInput) = viewModelScope.launch{
        _myAppointmentDeclineResponse.value = Resource.Loading
        _myAppointmentDeclineResponse.value = repository.declineAppointment(myAppointmentStatusChangeInput)
    }

}