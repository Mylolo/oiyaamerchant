package com.localvalu.coremerchantapp.network

import android.content.Context
import com.localvalu.base.TokenRefreshApi
import com.localvalu.coremerchantapp.BuildConfig
import okhttp3.Authenticator
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named

class RemoteDataSource
{
    fun <Api> buildApiDotNet(api: Class<Api>,context: Context):Api
    {
        return Retrofit.Builder().baseUrl(BuildConfig.URL_BASE_DOTNET)
            .client(getRetrofitClient(null))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(api);
    }

    fun <Api> buildApi(api: Class<Api>,context: Context):Api
    {
        return Retrofit.Builder().baseUrl(BuildConfig.URL_BASE_PHP1)
            .client(getRetrofitClient(null))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(api);
    }

    fun <Api> buildApiPhpOne(api: Class<Api>,context: Context):Api
    {
        return Retrofit.Builder().baseUrl(BuildConfig.URL_BASE_PHP)
            .client(getRetrofitClient(null))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(api);
    }

    private fun buildTokenApi(): TokenRefreshApi
    {
        return Retrofit.Builder()
            .baseUrl(BuildConfig.URL_BASE_PHP1)
            .client(getRetrofitClient())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(TokenRefreshApi::class.java)
    }

    private fun getRetrofitClient(authenticator: Authenticator? = null):OkHttpClient
    {
        return OkHttpClient.Builder()
            .addInterceptor { chain ->
                chain.proceed(chain.request().newBuilder().also {
                    it.addHeader("Accept", "application/json")
                }.build())
            }.also { client ->
                authenticator?.let { client.authenticator(it) }
                if (BuildConfig.DEBUG) {
                    val logging = HttpLoggingInterceptor()
                    logging.setLevel(HttpLoggingInterceptor.Level.BODY)
                    client.addInterceptor(logging)
                }
            }.build()
    }
}