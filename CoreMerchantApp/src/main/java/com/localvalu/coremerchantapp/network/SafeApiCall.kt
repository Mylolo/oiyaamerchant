package com.localvalu.coremerchantapp.network

import com.localvalu.coremerchantapp.base.Resource
import com.localvalu.coremerchantapp.BuildConfig
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.HttpException

interface SafeApiCall
{
    suspend fun <T> safeApiCall(apiCall: suspend () -> T): Resource<T>
    {
        return withContext(Dispatchers.IO)
        {
            try
            {
                Resource.Success(apiCall.invoke())
            }
            catch (throwable: Throwable)
            {
                if(BuildConfig.DEBUG)
                {
                    throwable.printStackTrace()
                }

                when (throwable)
                {
                    is HttpException ->
                    {
                        Resource.Failure(false, throwable.code(), throwable.response()?.errorBody())
                    }
                    else ->
                    {
                        Resource.Failure(true, null, null)
                    }
                }
            }
        }
    }
}