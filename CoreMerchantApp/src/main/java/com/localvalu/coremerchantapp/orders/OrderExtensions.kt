package com.localvalu.coremerchantapp.orders

import com.localvalu.coremerchantapp.base.Event
import com.localvalu.coremerchantapp.base.Resource
import com.localvalu.coremerchantapp.data.base.RestBasedErrorDetail
import com.localvalu.coremerchantapp.data.output.order.OrderListData
import com.localvalu.coremerchantapp.data.output.order.OrderListOutput
import com.localvalu.coremerchantapp.data.output.order.OrderListResult
import java.util.ArrayList

fun fetchUpdatedList(orderListOutput:Resource<OrderListOutput>,orderId:String,status:String):Event<Resource<OrderListOutput>>
{
    when(orderListOutput)
    {
        is Resource.Success->
        {
            if(orderListOutput.value.orderListResult.errorDetail.errorCode==0)
            {

                val orderList = orderListOutput.value.orderListResult.orderList
                val index = orderList?.indexOfFirst{ it->
                    it.orderId==orderId
                }
                index?.let {
                    if (index>-1)
                    {
                        orderList?.get(it).status=status

                        val orderListOutputTemp = OrderListOutput()
                        val orderListResultTemp = OrderListResult()
                        orderListOutputTemp.orderListResult= orderListResultTemp
                        val restBasedErrorDetail = RestBasedErrorDetail()
                        restBasedErrorDetail.errorCode=0
                        orderListResultTemp.errorDetail= restBasedErrorDetail
                        orderListResultTemp.setOrderList(orderList as ArrayList<OrderListData>?)
                        orderListOutputTemp.orderListResult=orderListResultTemp

                        return Event(Resource.Success(orderListOutputTemp))
                    }
                }
            }
        }
    }
    return Event(orderListOutput)
}