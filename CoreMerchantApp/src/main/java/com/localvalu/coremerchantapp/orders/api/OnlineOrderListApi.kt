package com.localvalu.coremerchantapp.orders.api

import com.localvalu.coremerchantapp.BaseApi
import com.localvalu.coremerchantapp.data.input.OrderListInput
import com.localvalu.coremerchantapp.data.output.order.OrderListOutput
import retrofit2.http.Body
import retrofit2.http.POST

interface OnlineOrderListApi : BaseApi
{
    @POST("MerchantAppV2/getOrderDetailsForADay")
    suspend fun onlineOrderList(@Body ordersListInput: OrderListInput): OrderListOutput
}