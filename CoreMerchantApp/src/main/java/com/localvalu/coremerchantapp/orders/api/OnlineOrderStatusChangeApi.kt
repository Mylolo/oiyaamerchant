package com.localvalu.coremerchantapp.orders.api

import com.localvalu.coremerchantapp.BaseApi
import com.localvalu.coremerchantapp.data.input.OrderStatusChangeInput
import com.localvalu.coremerchantapp.data.output.order.OrderStatusChangeOutput
import retrofit2.http.Body
import retrofit2.http.POST

interface OnlineOrderStatusChangeApi : BaseApi
{
    @POST("MerchantAppV2/ChangeOrderStatus")
    suspend fun changeOrderStatus(@Body orderStatusChangeInput: OrderStatusChangeInput): OrderStatusChangeOutput
}