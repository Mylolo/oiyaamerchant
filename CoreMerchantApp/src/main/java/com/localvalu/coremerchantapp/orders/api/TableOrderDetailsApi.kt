package com.localvalu.coremerchantapp.orders.api

import com.localvalu.coremerchantapp.BaseApi
import com.localvalu.coremerchantapp.data.input.OrderDetailsInput
import com.localvalu.coremerchantapp.data.input.OrderListInput
import com.localvalu.coremerchantapp.data.output.order.OrderDetailsResultOutput
import com.localvalu.coremerchantapp.data.output.order.OrderListOutput
import retrofit2.http.Body
import retrofit2.http.POST

interface TableOrderDetailsApi : BaseApi
{
    @POST("MerchantAppV2/getInduvidualTableOrderDetails")
    suspend fun onlineOrderDetails(@Body orderDetailsInput: OrderDetailsInput): OrderDetailsResultOutput
}