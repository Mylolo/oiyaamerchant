package com.localvalu.coremerchantapp.orders.api

import com.localvalu.coremerchantapp.BaseApi
import com.localvalu.coremerchantapp.data.input.OrderListInput
import com.localvalu.coremerchantapp.data.output.order.OrderListOutput
import retrofit2.http.Body
import retrofit2.http.POST

interface TableOrderListApi : BaseApi
{
    @POST("MerchantAppV2/getTableOrderDetailsForADay")
    suspend fun tableOrdersList(@Body ordersListInput: OrderListInput): OrderListOutput
}