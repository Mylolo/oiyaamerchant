package com.localvalu.coremerchantapp.orders.di

import android.content.Context
import com.localvalu.coremerchantapp.network.RemoteDataSource
import com.localvalu.coremerchantapp.orders.api.OnlineOrderStatusChangeApi
import com.localvalu.coremerchantapp.orders.api.TableOrderListApi
import com.localvalu.coremerchantapp.orders.repository.OnlineOrderChangeStatusRepository
import com.localvalu.coremerchantapp.orders.repository.TableOrderListRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class OnlineOrderChangeStatusModule
{
    @Provides
    fun providesOnlineOrderStatusChangeApi(
        remoteDataSource: RemoteDataSource, @ApplicationContext
        context: Context
    ): OnlineOrderStatusChangeApi
    {
        return remoteDataSource.buildApiPhpOne(OnlineOrderStatusChangeApi::class.java, context);
    }

    @Provides
    fun providesOnlineOrderStatusChangeRepository(api: OnlineOrderStatusChangeApi): OnlineOrderChangeStatusRepository
    {
        return OnlineOrderChangeStatusRepository(api);
    }

}