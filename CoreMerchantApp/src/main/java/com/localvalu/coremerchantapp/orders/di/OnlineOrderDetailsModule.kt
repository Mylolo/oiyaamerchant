package com.localvalu.coremerchantapp.orders.di

import android.content.Context
import com.localvalu.coremerchantapp.network.RemoteDataSource
import com.localvalu.coremerchantapp.orders.api.OnlineOrderDetailsApi
import com.localvalu.coremerchantapp.orders.api.OnlineOrderStatusChangeApi
import com.localvalu.coremerchantapp.orders.api.TableOrderListApi
import com.localvalu.coremerchantapp.orders.repository.OnlineOrderChangeStatusRepository
import com.localvalu.coremerchantapp.orders.repository.OnlineOrderDetailsRepository
import com.localvalu.coremerchantapp.orders.repository.TableOrderListRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class OnlineOrderDetailsModule
{
    @Provides
    fun providesOnlineOrderDetailsApi(
        remoteDataSource: RemoteDataSource, @ApplicationContext
        context: Context
    ): OnlineOrderDetailsApi
    {
        return remoteDataSource.buildApiPhpOne(OnlineOrderDetailsApi::class.java, context);
    }

    @Provides
    fun providesOnlineOrderDetailsRepository(api: OnlineOrderDetailsApi): OnlineOrderDetailsRepository
    {
        return OnlineOrderDetailsRepository(api);
    }

}