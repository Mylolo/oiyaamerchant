package com.localvalu.coremerchantapp.orders.di

import android.content.Context
import com.localvalu.coremerchantapp.network.RemoteDataSource
import com.localvalu.coremerchantapp.orders.api.OnlineOrderListApi
import com.localvalu.coremerchantapp.orders.repository.OnlineOrderListRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class OnlineOrderListModule
{
    @Provides
    fun providesOnlineOrderListApi(
        remoteDataSource: RemoteDataSource, @ApplicationContext
        context: Context
    ): OnlineOrderListApi
    {
        return remoteDataSource.buildApiPhpOne(OnlineOrderListApi::class.java, context);
    }

    @Provides
    fun providesOnlineOrderListRepository(api: OnlineOrderListApi): OnlineOrderListRepository
    {
        return OnlineOrderListRepository(api);
    }

}