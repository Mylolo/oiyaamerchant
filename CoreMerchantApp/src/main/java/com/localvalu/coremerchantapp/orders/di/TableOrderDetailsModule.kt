package com.localvalu.coremerchantapp.orders.di

import android.content.Context
import com.localvalu.coremerchantapp.network.RemoteDataSource
import com.localvalu.coremerchantapp.orders.api.OnlineOrderDetailsApi
import com.localvalu.coremerchantapp.orders.api.OnlineOrderStatusChangeApi
import com.localvalu.coremerchantapp.orders.api.TableOrderDetailsApi
import com.localvalu.coremerchantapp.orders.api.TableOrderListApi
import com.localvalu.coremerchantapp.orders.repository.OnlineOrderChangeStatusRepository
import com.localvalu.coremerchantapp.orders.repository.OnlineOrderDetailsRepository
import com.localvalu.coremerchantapp.orders.repository.TableOrderDetailsRepository
import com.localvalu.coremerchantapp.orders.repository.TableOrderListRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class TableOrderDetailsModule
{
    @Provides
    fun providesTableOrderDetailsApi(
        remoteDataSource: RemoteDataSource, @ApplicationContext
        context: Context
    ): TableOrderDetailsApi
    {
        return remoteDataSource.buildApiPhpOne(TableOrderDetailsApi::class.java, context);
    }

    @Provides
    fun providesTableOrderDetailsRepository(api: TableOrderDetailsApi): TableOrderDetailsRepository
    {
        return TableOrderDetailsRepository(api);
    }

}