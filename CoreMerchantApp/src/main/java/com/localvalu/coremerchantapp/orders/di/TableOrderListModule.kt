package com.localvalu.coremerchantapp.orders.di

import android.content.Context
import com.localvalu.coremerchantapp.network.RemoteDataSource
import com.localvalu.coremerchantapp.orders.api.TableOrderListApi
import com.localvalu.coremerchantapp.orders.repository.TableOrderListRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class TableOrderListModule
{
    @Provides
    fun providesTableOrderListApi(
        remoteDataSource: RemoteDataSource, @ApplicationContext
        context: Context
    ): TableOrderListApi
    {
        return remoteDataSource.buildApiPhpOne(TableOrderListApi::class.java, context);
    }

    @Provides
    fun providesOnlineOrderListRepository(api: TableOrderListApi): TableOrderListRepository
    {
        return TableOrderListRepository(api);
    }

}