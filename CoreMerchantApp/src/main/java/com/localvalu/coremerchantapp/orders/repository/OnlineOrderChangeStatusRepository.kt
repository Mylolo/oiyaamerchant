package com.localvalu.coremerchantapp.orders.repository

import com.localvalu.coremerchantapp.base.BaseRepository
import com.localvalu.coremerchantapp.data.input.OrderListInput
import com.localvalu.coremerchantapp.data.input.OrderStatusChangeInput
import com.localvalu.coremerchantapp.orders.api.OnlineOrderListApi
import com.localvalu.coremerchantapp.orders.api.OnlineOrderStatusChangeApi

class OnlineOrderChangeStatusRepository (private val api: OnlineOrderStatusChangeApi) :
    BaseRepository(api)
{
    suspend fun changeOrderStatus(orderStatusChangeInput: OrderStatusChangeInput) = safeApiCall {
        api.changeOrderStatus(orderStatusChangeInput)
    }
}