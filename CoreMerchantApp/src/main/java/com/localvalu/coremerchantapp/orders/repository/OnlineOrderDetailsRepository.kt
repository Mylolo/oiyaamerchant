package com.localvalu.coremerchantapp.orders.repository

import com.localvalu.coremerchantapp.base.BaseRepository
import com.localvalu.coremerchantapp.data.input.OrderDetailsInput
import com.localvalu.coremerchantapp.data.input.OrderListInput
import com.localvalu.coremerchantapp.data.input.OrderStatusChangeInput
import com.localvalu.coremerchantapp.orders.api.OnlineOrderDetailsApi
import com.localvalu.coremerchantapp.orders.api.OnlineOrderListApi
import com.localvalu.coremerchantapp.orders.api.OnlineOrderStatusChangeApi

class OnlineOrderDetailsRepository (private val api: OnlineOrderDetailsApi) :
    BaseRepository(api)
{
    suspend fun orderDetails(orderDetailsInput: OrderDetailsInput) = safeApiCall {
        api.onlineOrderDetails(orderDetailsInput)
    }
}