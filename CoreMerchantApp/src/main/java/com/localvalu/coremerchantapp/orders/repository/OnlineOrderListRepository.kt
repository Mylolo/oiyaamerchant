package com.localvalu.coremerchantapp.orders.repository

import com.localvalu.coremerchantapp.base.BaseRepository
import com.localvalu.coremerchantapp.data.input.OrderListInput
import com.localvalu.coremerchantapp.orders.api.OnlineOrderListApi

class OnlineOrderListRepository (private val api: OnlineOrderListApi) :
    BaseRepository(api)
{
    suspend fun onlineOrderList(ordersListInput: OrderListInput) = safeApiCall {
        api.onlineOrderList(ordersListInput)
    }
}