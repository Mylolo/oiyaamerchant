package com.localvalu.coremerchantapp.orders.repository

import com.localvalu.coremerchantapp.base.BaseRepository
import com.localvalu.coremerchantapp.data.input.OrderListInput
import com.localvalu.coremerchantapp.orders.api.TableOrderListApi

class TableOrderListRepository (private val api: TableOrderListApi) :
    BaseRepository(api)
{
    suspend fun tableOrderList(ordersListInput: OrderListInput) = safeApiCall {
        api.tableOrdersList(ordersListInput)
    }
}