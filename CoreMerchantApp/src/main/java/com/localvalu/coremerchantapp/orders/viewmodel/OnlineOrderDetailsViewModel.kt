package com.localvalu.coremerchantapp.orders.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.localvalu.coremerchantapp.base.Event
import com.localvalu.coremerchantapp.base.Resource
import com.localvalu.coremerchantapp.data.input.OrderDetailsInput
import com.localvalu.coremerchantapp.data.input.OrderListInput
import com.localvalu.coremerchantapp.data.input.OrderStatusChangeInput
import com.localvalu.coremerchantapp.data.output.order.OrderDetailsResultOutput
import com.localvalu.coremerchantapp.data.output.order.OrderListOutput
import com.localvalu.coremerchantapp.data.output.order.OrderStatusChangeOutput
import com.localvalu.coremerchantapp.orders.api.OnlineOrderStatusChangeApi
import com.localvalu.coremerchantapp.orders.repository.OnlineOrderChangeStatusRepository
import com.localvalu.coremerchantapp.orders.repository.OnlineOrderDetailsRepository
import com.localvalu.coremerchantapp.orders.repository.TableOrderListRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class OnlineOrderDetailsViewModel @Inject constructor(val repository: OnlineOrderDetailsRepository):ViewModel()
{
    private val _onlineOrderDetailsApiResponse:MutableLiveData<Event<Resource<OrderDetailsResultOutput>>> = MutableLiveData()

    val onlineOrderDetailsApiResponse: LiveData<Event<Resource<OrderDetailsResultOutput>>>
        get() = _onlineOrderDetailsApiResponse

    fun onlineOrderDetails(orderDetailsInput: OrderDetailsInput) = viewModelScope.launch{
        _onlineOrderDetailsApiResponse.value = Event(Resource.Loading)
        _onlineOrderDetailsApiResponse.value = Event(repository.orderDetails(orderDetailsInput))
    }
}