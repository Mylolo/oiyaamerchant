package com.localvalu.coremerchantapp.orders.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.localvalu.coremerchantapp.base.Event
import com.localvalu.coremerchantapp.base.Resource
import com.localvalu.coremerchantapp.data.base.RestBasedErrorDetail
import com.localvalu.coremerchantapp.data.input.OrderListInput
import com.localvalu.coremerchantapp.data.output.order.OrderListData
import com.localvalu.coremerchantapp.data.output.order.OrderListOutput
import com.localvalu.coremerchantapp.data.output.order.OrderListResult
import com.localvalu.coremerchantapp.orders.repository.OnlineOrderListRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.util.ArrayList
import javax.inject.Inject

@HiltViewModel
class OnlineOrderListViewModel @Inject constructor(val repository: OnlineOrderListRepository):ViewModel()
{
    private val TAG = "OOLVM"
    private val _onlineOrderListResponse:MutableLiveData<Event<Resource<OrderListOutput>>> = MutableLiveData()

    val onlineOrderListResponse: LiveData<Event<Resource<OrderListOutput>>>
        get() = _onlineOrderListResponse

    fun onlineOrderList(orderListInput: OrderListInput) = viewModelScope.launch{
        _onlineOrderListResponse.value = Event(Resource.Loading)
        _onlineOrderListResponse.value = Event(repository.onlineOrderList(orderListInput))
    }

    fun updateList(orderId:String,status:String)
    {
        Log.d(TAG,"UpdateList")
        var orderListOutput : Resource<OrderListOutput>? = _onlineOrderListResponse?.value?.peekContent()
        when(orderListOutput)
        {
            is Resource.Success->
            {
                Log.d(TAG,"Resource.Success")
                if(orderListOutput.value.orderListResult.errorDetail.errorCode==0)
                {
                    Log.d(TAG,"orderListOutput")

                    val orderList = orderListOutput.value.orderListResult.orderList
                    val index = orderList?.indexOfFirst{ it->
                        it.orderId==orderId
                    }
                    index?.let {
                        if (index>-1)
                        {
                            Log.d(TAG,"orderListOutput-index " + index)
                            orderList?.get(it).status=status
                            val orderListOutputTemp = OrderListOutput()
                            val orderListResultTemp = OrderListResult()
                            orderListOutputTemp.orderListResult= orderListResultTemp
                            val restBasedErrorDetail = RestBasedErrorDetail()
                            restBasedErrorDetail.errorCode=0
                            orderListResultTemp.errorDetail= restBasedErrorDetail
                            orderListResultTemp.setOrderList(orderList as ArrayList<OrderListData>?)
                            orderListOutputTemp.orderListResult=orderListResultTemp
                            _onlineOrderListResponse.value=Event(Resource.Success(orderListOutputTemp))
                        }
                    }
                }
            }
        }
    }
}