package com.localvalu.coremerchantapp.orders.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.localvalu.coremerchantapp.base.Event
import com.localvalu.coremerchantapp.base.Resource
import com.localvalu.coremerchantapp.data.input.OrderListInput
import com.localvalu.coremerchantapp.data.input.OrderStatusChangeInput
import com.localvalu.coremerchantapp.data.output.order.OrderListOutput
import com.localvalu.coremerchantapp.data.output.order.OrderStatusChangeOutput
import com.localvalu.coremerchantapp.orders.api.OnlineOrderStatusChangeApi
import com.localvalu.coremerchantapp.orders.repository.OnlineOrderChangeStatusRepository
import com.localvalu.coremerchantapp.orders.repository.TableOrderListRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class OnlineOrderStatusChangeViewModel @Inject constructor(val repository: OnlineOrderChangeStatusRepository):ViewModel()
{
    private val _onlineOrderChangeStatusResponse:MutableLiveData<Event<Resource<OrderStatusChangeOutput>>> = MutableLiveData()

    val onlineOrderChangeStatusResponse: LiveData<Event<Resource<OrderStatusChangeOutput>>>
        get() = _onlineOrderChangeStatusResponse

    fun changeOrderStatus(orderStatusChangeInput: OrderStatusChangeInput) = viewModelScope.launch{
        _onlineOrderChangeStatusResponse.value = Event(Resource.Loading)
        _onlineOrderChangeStatusResponse.value = Event(repository.changeOrderStatus(orderStatusChangeInput))
    }
}