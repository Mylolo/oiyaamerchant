package com.localvalu.coremerchantapp.orders.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.localvalu.coremerchantapp.base.Event
import com.localvalu.coremerchantapp.base.Resource
import com.localvalu.coremerchantapp.data.input.OrderDetailsInput
import com.localvalu.coremerchantapp.data.input.OrderListInput
import com.localvalu.coremerchantapp.data.input.OrderStatusChangeInput
import com.localvalu.coremerchantapp.data.output.order.OrderDetailsResultOutput
import com.localvalu.coremerchantapp.data.output.order.OrderListOutput
import com.localvalu.coremerchantapp.data.output.order.OrderStatusChangeOutput
import com.localvalu.coremerchantapp.orders.api.OnlineOrderStatusChangeApi
import com.localvalu.coremerchantapp.orders.repository.OnlineOrderChangeStatusRepository
import com.localvalu.coremerchantapp.orders.repository.OnlineOrderDetailsRepository
import com.localvalu.coremerchantapp.orders.repository.TableOrderDetailsRepository
import com.localvalu.coremerchantapp.orders.repository.TableOrderListRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class TableOrderDetailsViewModel @Inject constructor(val repository:TableOrderDetailsRepository):ViewModel()
{
    private val _tableOrderDetailsApiResponse:MutableLiveData<Event<Resource<OrderDetailsResultOutput>>> = MutableLiveData()

    val onlineOrderDetailsApiResponse: LiveData<Event<Resource<OrderDetailsResultOutput>>>
        get() = _tableOrderDetailsApiResponse

    fun tableOrderDetails(orderDetailsInput: OrderDetailsInput) = viewModelScope.launch{
        _tableOrderDetailsApiResponse.value = Event(Resource.Loading)
        _tableOrderDetailsApiResponse.value = Event(repository.orderDetails(orderDetailsInput))
    }
}