package com.localvalu.coremerchantapp.orders.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.localvalu.coremerchantapp.base.Event
import com.localvalu.coremerchantapp.base.Resource
import com.localvalu.coremerchantapp.data.base.RestBasedErrorDetail
import com.localvalu.coremerchantapp.data.input.OrderListInput
import com.localvalu.coremerchantapp.data.output.order.OrderListData
import com.localvalu.coremerchantapp.data.output.order.OrderListOutput
import com.localvalu.coremerchantapp.data.output.order.OrderListResult
import com.localvalu.coremerchantapp.orders.fetchUpdatedList
import com.localvalu.coremerchantapp.orders.repository.TableOrderListRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.util.ArrayList
import javax.inject.Inject

@HiltViewModel
class TableOrderListViewModel @Inject constructor(val repository: TableOrderListRepository):ViewModel()
{
    private val TAG = "TOLVM"

    private val _tableOrderListResponse:MutableLiveData<Event<Resource<OrderListOutput>>> = MutableLiveData()

    val tableOrderListResponse: LiveData<Event<Resource<OrderListOutput>>>
        get() = _tableOrderListResponse

    fun tableOrderList(orderListInput: OrderListInput) = viewModelScope.launch{
        _tableOrderListResponse.value = Event(Resource.Loading)
        _tableOrderListResponse.value = Event(repository.tableOrderList(orderListInput))
    }

    fun updateList(orderId:String,status:String)
    {
        Log.d(TAG,"UpdateList")
        val orderListOutput : Resource<OrderListOutput>? = tableOrderListResponse?.value?.peekContent()
        orderListOutput?.let {
            _tableOrderListResponse.value = fetchUpdatedList(orderListOutput!!,orderId,status)
        }
    }
}