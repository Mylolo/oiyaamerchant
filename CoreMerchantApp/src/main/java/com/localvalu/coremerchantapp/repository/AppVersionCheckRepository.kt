package com.localvalu.coremerchantapp.repository

import DriverAppBase
import androidx.lifecycle.MediatorLiveData
import com.localvalu.coremerchantapp.BuildConfig
import com.localvalu.coremerchantapp.api.APIService
import com.localvalu.coremerchantapp.data.input.AppVersionCheckInput
import com.localvalu.coremerchantapp.data.input.PoolDriverListInput
import com.localvalu.coremerchantapp.data.output.appversioncheck.AppVersionOutput
import com.localvalu.coremerchantapp.data.output.driver.OwnDriversListRequest
import com.localvalu.coremerchantapp.data.output.pooldrivers.GetPoolDriverListResultOutput
import com.localvalu.coremerchantapp.generic.Resource
import kotlinx.coroutines.*
import java.lang.Exception

object AppVersionCheckRepository
{
    var appVersionCheckJob: CompletableJob?=null

    fun cancelJobs()
    {
        appVersionCheckJob?.cancel()
    }

    fun getAppVersion(appVersionCheckInput: AppVersionCheckInput): MediatorLiveData<Resource<AppVersionOutput>>
    {
        appVersionCheckJob = Job()
        return object : MediatorLiveData<Resource<AppVersionOutput>>()
        {
            override fun onActive()
            {
                super.onActive()
                appVersionCheckJob?.let { theJob ->
                    CoroutineScope(Dispatchers.IO + theJob).launch {
                        try
                        {
                            withContext(Dispatchers.Main)
                            {
                                value = Resource.Loading()
                            }
                            val appVersionOutput = APIService.getAppVersionService().getAppVersion(appVersionCheckInput)
                            withContext(Dispatchers.Main)
                            {
                                value = Resource.Success(appVersionOutput)
                                theJob.complete()
                            }
                        }
                        catch (ex: Exception)
                        {
                            if(BuildConfig.DEBUG)
                            {
                                ex.printStackTrace()
                            }
                            withContext(Dispatchers.Main)
                            {
                                value = Resource.Failure(ex)
                                theJob.complete()
                            }
                        }
                    }
                }
            }
        }
    }
}