package com.localvalu.coremerchantapp.repository

import androidx.lifecycle.MediatorLiveData
import com.localvalu.coremerchantapp.BuildConfig
import com.localvalu.coremerchantapp.api.APIService
import com.localvalu.coremerchantapp.data.input.NotifyToDriversInput
import com.localvalu.coremerchantapp.data.input.NotifyToPoolDriversInput
import com.localvalu.coremerchantapp.data.output.driverOld.DriverNotifictionResultOutput
import com.localvalu.coremerchantapp.generic.Resource
import kotlinx.coroutines.*
import java.lang.Exception

object NotifyDriversRepository
{
    var notifyDriversJob:CompletableJob?=null

    fun cancelJobs()
    {
        notifyDriversJob?.cancel()
    }

    fun notifyDrivers(notifyToPoolDriversInput: NotifyToPoolDriversInput):MediatorLiveData<Resource<DriverNotifictionResultOutput>>
    {
        notifyDriversJob = Job()
        return object : MediatorLiveData<Resource<DriverNotifictionResultOutput>>()
        {
            override fun onActive()
            {
                super.onActive()
                notifyDriversJob?.let { theJob ->
                    CoroutineScope(Dispatchers.IO + theJob).launch {
                        try
                        {
                            withContext(Dispatchers.Main)
                            {
                                value = Resource.Loading()
                            }
                            val driverNotifictionResultOutput = APIService.getNotifyDriversService().notifyToPoolDrivers(notifyToPoolDriversInput)
                            withContext(Dispatchers.Main)
                            {
                                value = Resource.Success(driverNotifictionResultOutput)
                                theJob.complete()
                            }
                        }
                        catch (ex:Exception)
                        {
                            if(BuildConfig.DEBUG)
                            {
                                ex.printStackTrace()
                            }
                            withContext(Dispatchers.Main)
                            {
                                value = Resource.Failure(ex)
                                theJob.complete()
                            }
                        }
                    }
                }
            }
        }
    }

    fun notifySingleDriver(notifyDriversInput: NotifyToDriversInput):MediatorLiveData<Resource<DriverNotifictionResultOutput>>
    {
        notifyDriversJob = Job()
        return object : MediatorLiveData<Resource<DriverNotifictionResultOutput>>()
        {
            override fun onActive()
            {
                super.onActive()
                notifyDriversJob?.let { theJob ->
                    CoroutineScope(Dispatchers.IO + theJob).launch {
                        try
                        {
                            withContext(Dispatchers.Main)
                            {
                                value = Resource.Loading()
                            }
                            val driverNotifictionResultOutput = APIService.getNotifyDriversService().notifyToDrivers(notifyDriversInput)
                            withContext(Dispatchers.Main)
                            {
                                value = Resource.Success(driverNotifictionResultOutput)
                                theJob.complete()
                            }
                        }
                        catch (ex:Exception)
                        {
                            if(BuildConfig.DEBUG)
                            {
                                ex.printStackTrace()
                            }
                            withContext(Dispatchers.Main)
                            {
                                value = Resource.Failure(ex)
                                theJob.complete()
                            }
                        }
                    }
                }
            }
        }
    }

}