package com.localvalu.coremerchantapp.repository

import DriverAppBase
import androidx.lifecycle.MediatorLiveData
import com.localvalu.coremerchantapp.BuildConfig
import com.localvalu.coremerchantapp.api.APIService
import com.localvalu.coremerchantapp.data.input.PoolDriverListInput
import com.localvalu.coremerchantapp.data.output.driver.OwnDriversListRequest
import com.localvalu.coremerchantapp.data.output.pooldrivers.GetPoolDriverListResultOutput
import com.localvalu.coremerchantapp.generic.Resource
import kotlinx.coroutines.*
import java.lang.Exception

object OwnDriverRepository
{
    var ownDriverListJob: CompletableJob?=null

    fun cancelJobs()
    {
        ownDriverListJob?.cancel()
    }

    fun getOwnDriverList(ownDriversListRequest: OwnDriversListRequest): MediatorLiveData<Resource<DriverAppBase>>
    {
        ownDriverListJob = Job()
        return object : MediatorLiveData<Resource<DriverAppBase>>()
        {
            override fun onActive()
            {
                super.onActive()
                ownDriverListJob?.let { theJob ->
                    CoroutineScope(Dispatchers.IO + theJob).launch {
                        try
                        {
                            withContext(Dispatchers.Main)
                            {
                                value = Resource.Loading()
                            }
                            val getPoolDriverListResultOutput = APIService.getPoolDriverApiService().ownDriversList(ownDriversListRequest)
                            withContext(Dispatchers.Main)
                            {
                                value = Resource.Success(getPoolDriverListResultOutput)
                                theJob.complete()
                            }
                        }
                        catch (ex: Exception)
                        {
                            if(BuildConfig.DEBUG)
                            {
                                ex.printStackTrace()
                            }
                            withContext(Dispatchers.Main)
                            {
                                value = Resource.Failure(ex)
                                theJob.complete()
                            }
                        }
                    }
                }
            }
        }
    }
}