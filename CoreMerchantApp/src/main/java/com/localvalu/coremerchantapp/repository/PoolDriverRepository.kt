package com.localvalu.coremerchantapp.repository

import androidx.lifecycle.MediatorLiveData
import com.localvalu.coremerchantapp.BuildConfig
import com.localvalu.coremerchantapp.api.APIService
import com.localvalu.coremerchantapp.data.input.PoolDriverListInput
import com.localvalu.coremerchantapp.data.output.pooldrivers.GetPoolDriverListResultOutput
import com.localvalu.coremerchantapp.generic.Resource
import kotlinx.coroutines.*
import java.lang.Exception

object PoolDriverRepository
{
    var poolDriverListJob:CompletableJob?=null

    fun cancelJobs()
    {
        poolDriverListJob?.cancel()
    }

    fun getPoolDriverList(poolDriverListInput: PoolDriverListInput):MediatorLiveData<Resource<GetPoolDriverListResultOutput>>
    {
        poolDriverListJob = Job()
        return object : MediatorLiveData<Resource<GetPoolDriverListResultOutput>>()
        {
            override fun onActive()
            {
                super.onActive()
                poolDriverListJob?.let { theJob ->
                    CoroutineScope(Dispatchers.IO + theJob).launch {
                        try
                        {
                            withContext(Dispatchers.Main)
                            {
                                value = Resource.Loading()
                            }
                            val getPoolDriverListResultOutput = APIService.getPoolDriverApiService().poolDriverList(poolDriverListInput)
                            withContext(Dispatchers.Main)
                            {
                                value = Resource.Success(getPoolDriverListResultOutput)
                                theJob.complete()
                            }
                        }
                        catch (ex:Exception)
                        {
                            if(BuildConfig.DEBUG)
                            {
                                ex.printStackTrace()
                            }
                            withContext(Dispatchers.Main)
                            {
                                value = Resource.Failure(ex)
                                theJob.complete()
                            }
                        }
                    }
                }
            }
        }
    }
}