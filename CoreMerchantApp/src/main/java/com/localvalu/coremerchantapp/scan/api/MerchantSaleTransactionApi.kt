package com.localvalu.coremerchantapp.scan.api

import com.localvalu.coremerchantapp.BaseApi
import com.localvalu.coremerchantapp.data.input.SaleTransactionInput
import com.localvalu.coremerchantapp.data.output.SaleTransactionOutput
import retrofit2.http.Body
import retrofit2.http.POST

interface MerchantSaleTransactionApi:BaseApi
{
    @POST("MerchantAppSaleTransaction")
    suspend fun merchantSaleTransaction(@Body merchantSaleTransactionInput: SaleTransactionInput):SaleTransactionOutput
}