package com.localvalu.coremerchantapp.scan.di

import android.content.Context
import com.localvalu.coremerchantapp.network.RemoteDataSource
import com.localvalu.coremerchantapp.scan.api.MerchantSaleTransactionApi
import com.localvalu.coremerchantapp.scan.repository.MerchantSaleTransactionRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class SaleTransactionModule
{
    @Provides
    fun providesSaleTransactionApi(
        remoteDataSource: RemoteDataSource, @ApplicationContext
        context: Context
    ): MerchantSaleTransactionApi
    {
        return remoteDataSource.buildApiDotNet(MerchantSaleTransactionApi::class.java, context);
    }

    @Provides
    fun providesSaleTransactionRepository(api: MerchantSaleTransactionApi): MerchantSaleTransactionRepository
    {
        return MerchantSaleTransactionRepository(api);
    }

}