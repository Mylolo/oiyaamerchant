package com.localvalu.coremerchantapp.scan.repository

import com.localvalu.coremerchantapp.BaseApi
import com.localvalu.coremerchantapp.base.BaseRepository
import com.localvalu.coremerchantapp.data.input.SaleTransactionInput
import com.localvalu.coremerchantapp.scan.api.MerchantSaleTransactionApi

class MerchantSaleTransactionRepository (private val api: MerchantSaleTransactionApi): BaseRepository(api)
{
    suspend fun merchantSaleTransaction(saleTransactionInput: SaleTransactionInput) = safeApiCall{
        api.merchantSaleTransaction(saleTransactionInput)
    }
}