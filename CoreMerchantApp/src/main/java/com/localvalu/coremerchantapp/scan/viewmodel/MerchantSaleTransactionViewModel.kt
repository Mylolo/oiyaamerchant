package com.localvalu.coremerchantapp.scan.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.localvalu.coremerchantapp.base.Resource
import com.localvalu.coremerchantapp.data.input.SaleTransactionInput
import com.localvalu.coremerchantapp.data.output.SaleTransactionOutput
import com.localvalu.coremerchantapp.scan.repository.MerchantSaleTransactionRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MerchantSaleTransactionViewModel @Inject constructor(
    val repository: MerchantSaleTransactionRepository):ViewModel()
{
    private val _merchantSaleTransactionResponse: MutableLiveData<Resource<SaleTransactionOutput>> = MutableLiveData()

    val merchantSaleTransactionResponse: LiveData<Resource<SaleTransactionOutput>>
        get() = _merchantSaleTransactionResponse

    private var merchantSaleTransactionAmount:MutableLiveData<String> = MutableLiveData()

    fun merchantSaleTransaction(saleTransactionInput: SaleTransactionInput) = viewModelScope.launch {
        _merchantSaleTransactionResponse.value = Resource.Loading
        _merchantSaleTransactionResponse.value = repository.merchantSaleTransaction(saleTransactionInput)
    }

    val enableSendNow: MutableLiveData<Boolean> = MutableLiveData()

    private val amount: MutableLiveData<String> = MutableLiveData()

    fun setMerchantSaleTransactionAmount(saleAmount:String)
    {
        this.merchantSaleTransactionAmount.value = saleAmount
        enableDisableSend()
    }

    private fun enableDisableSend()
    {
        enableSendNow.value = (merchantSaleTransactionAmount.value?.isNotEmpty())
    }

}