package com.localvalu.coremerchantapp.scan.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ScanViewModel : ViewModel()
{

    val enableSendNow: MutableLiveData<Boolean> = MutableLiveData()

    val displayNotAllowedMessage: MutableLiveData<Boolean> = MutableLiveData()

    fun setCustomerUniqueCode(uniqueCode:String,ownCode:String)
    {
        enableDisableSend(uniqueCode,ownCode)
    }

    private fun enableDisableSend(uniqueCode: String,ownCode: String)
    {
        enableSendNow.value = (uniqueCodeValidation(uniqueCode,ownCode) &&
                uniqueCode.length==11)
    }

    private fun uniqueCodeValidation(uniqueCode:String,ownCode: String):Boolean
    {
        if(uniqueCode.length>4)
        {
            if(uniqueCode.substring(0,3).equals("IDL"))
            {
                if(uniqueCode.equals(ownCode))
                {
                    displayNotAllowedMessage.value=true
                    return false
                }
                displayNotAllowedMessage.value=false
                return true
            }
            displayNotAllowedMessage.value=false
            return false
        }
        displayNotAllowedMessage.value=false
        return false
    }


}