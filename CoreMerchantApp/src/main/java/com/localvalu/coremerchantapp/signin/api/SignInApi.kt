package com.localvalu.coremerchantapp.signin.api

import com.localvalu.coremerchantapp.BaseApi
import com.localvalu.coremerchantapp.data.input.SignInInput
import com.localvalu.coremerchantapp.data.output.SignInOutput
import retrofit2.http.Body
import retrofit2.http.POST

interface SignInApi : BaseApi
{
    @POST("BusinessMemberVerificationV1")
    suspend fun signIn(@Body signInInput: SignInInput) : SignInOutput
}