package com.localvalu.coremerchantapp.signin.di

import android.content.Context
import com.localvalu.coremerchantapp.network.RemoteDataSource
import com.localvalu.coremerchantapp.signin.api.SignInApi
import com.localvalu.coremerchantapp.signin.repository.SignInRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class SignInModule
{
    @Provides
    fun providesLoginApi(remoteDataSource: RemoteDataSource,
                         @ApplicationContext context: Context):SignInApi
    {
        return remoteDataSource.buildApiDotNet(SignInApi::class.java,context)
    }

    @Provides
    fun providesSignInRepository(loginApi: SignInApi):SignInRepository
    {
        return SignInRepository(loginApi)
    }

}