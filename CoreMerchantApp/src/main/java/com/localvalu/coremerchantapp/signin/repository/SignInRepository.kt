package com.localvalu.coremerchantapp.signin.repository

import com.localvalu.coremerchantapp.base.BaseRepository
import com.localvalu.coremerchantapp.data.input.SignInInput
import com.localvalu.coremerchantapp.signin.api.SignInApi

class SignInRepository(private val api:SignInApi) : BaseRepository(api)
{
    suspend fun signIn(signInInput: SignInInput) = safeApiCall{
        api.signIn(signInInput)
    }
}