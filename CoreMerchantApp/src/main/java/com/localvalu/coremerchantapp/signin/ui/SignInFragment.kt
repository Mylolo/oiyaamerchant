package com.localvalu.coremerchantapp.signin.ui

import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.net.Uri
import android.os.Bundle
import android.text.InputType
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.localvalu.coremerchantapp.BuildConfig
import com.localvalu.coremerchantapp.LocalValuConstants
import com.localvalu.coremerchantapp.R
import com.localvalu.coremerchantapp.base.Resource
import com.localvalu.coremerchantapp.data.input.SignInInput
import com.localvalu.coremerchantapp.databinding.FragmentSignInBinding
import com.localvalu.coremerchantapp.extensions.handleAPIError
import com.localvalu.coremerchantapp.fragment.SignInFragment
import com.localvalu.coremerchantapp.handler.DashBoardHandler
import com.localvalu.coremerchantapp.signin.viewmodel.SignInViewModel
import com.localvalu.coremerchantapp.utils.FontUtilis
import com.localvalu.coremerchantapp.viewmodel.DashboardViewModel
import com.utils.base.AppBaseFragment
import com.utils.helper.preference.PreferenceFactory
import com.utils.util.Util
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_sign_in.*
import java.lang.StringBuilder

@AndroidEntryPoint
class SignInFragment: AppBaseFragment()
{
    private val TAG ="SignInFragment"
    private var binding:FragmentSignInBinding?=null;
    private var colorWhite = 0
    private var colorBlack = 0
    private var dashBoardHandler: DashBoardHandler? = null
    private val dashboardViewModel:DashboardViewModel by viewModels()
    private val viewModel:SignInViewModel by viewModels()
    private var fcmToken:String?=""

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        colorBlack = ContextCompat.getColor(requireContext(), R.color.colorBlack)
        colorWhite = ContextCompat.getColor(requireContext(), R.color.colorWhite)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?
    {
        binding = FragmentSignInBinding.inflate(inflater,container,false)
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        super.onViewCreated(view, savedInstanceState)
        setProperties()
    }

    private fun setProperties()
    {
        setSupportingText()
        binding!!.btnLogin.isEnabled=false
        binding!!.progressIndicator.visibility=View.GONE
        binding!!.etPassword.typeface = FontUtilis.getInstance().comfortaaRegular(context)
        binding!!.etEmail.doAfterTextChanged {
            viewModel?.setEmail(it.toString())
        }
        binding!!.etPassword.doAfterTextChanged {
            viewModel?.setPassword(it.toString())
        }
        binding!!.btnLogin.setOnClickListener(_OnClickListener)
        binding!!.btnEnquiry.setOnClickListener(_OnClickListener)
        binding!!.tvLoginTermsAndConditions.setOnClickListener(_OnClickListener)
        binding!!.tvLoginPrivacyPolicy.setOnClickListener(_OnClickListener)
        val type = Typeface.createFromAsset(activity!!.assets, "fonts/comfortaa_bold.ttf")
        binding!!.etPassword.inputType =
            InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
        binding!!.etPassword.typeface = type
        observeSubscribers()
        if (BuildConfig.DEBUG)
        {
            binding!!.etEmail.setText("oyapp@gmail.com")
            binding!!.etPassword.setText("Lvpw@1234")
        }
    }

    private fun setSupportingText()
    {
        val builder = SpannableStringBuilder()
        val str1 = SpannableString(getString(R.string.lbl_login_bottom_text_one))
        str1.setSpan(ForegroundColorSpan(colorBlack), 0, str1.length, 0)
        builder.append(str1).append(" ")
        val str2 = SpannableString(getString(R.string.lbl_login_bottom_text_one_a))
        str2.setSpan(ForegroundColorSpan(colorWhite), 0, str2.length, 0)
        builder.append(str2).append(" ")
        binding!!.tvLoginTermsAndConditions.setText(builder, TextView.BufferType.SPANNABLE)
        val builder2 = SpannableStringBuilder()
        val str4 = SpannableString(getString(R.string.lbl_login_bottom_text_two))
        str4.setSpan(ForegroundColorSpan(colorBlack), 0, str4.length, 0)
        builder2.append(str4).append(" ")
        val str5 = SpannableString(getString(R.string.lbl_login_bottom_text_two_a))
        str5.setSpan(ForegroundColorSpan(colorWhite), 0, str5.length, 0)
        builder2.append(str5).append(" ")
        binding!!.tvLoginPrivacyPolicy.setText(builder2, TextView.BufferType.SPANNABLE)
    }

    private fun observeSubscribers()
    {
        dashboardViewModel.fcmToken.observe(viewLifecycleOwner, Observer {
            fcmToken = it
        })
        viewModel.signInResponse.observe(viewLifecycleOwner, Observer { response->
            when(response)
            {
                is Resource.Loading->
                {
                    showProgress()
                }
                is Resource.Success->
                {
                   hideProgress()
                }
                is Resource.Failure->
                {
                    hideProgress()
                   handleAPIError(response){
                       doSignIn()
                   }
                }
            }
        })
        viewModel.enableDisableSignIn.observe(viewLifecycleOwner, Observer {
            binding!!.btnLogin.isEnabled = it
        })
        viewModel.validEmail.observe(viewLifecycleOwner, Observer {
            if(!it) binding!!.etEmail.setError(getString(R.string.lbl_enter_valid_email))
            else binding!!.etEmail.setError(null)
        })
        viewModel.validPassword.observe(viewLifecycleOwner, Observer {
            if(!it) binding!!.etEmail.setError(getString(R.string.lbl_password_empty))
            else binding!!.etEmail.setError(null)
        })
    }

    var _OnClickListener =
        View.OnClickListener { view -> onButtonClick(view) }

    override fun onAttach(context: Context)
    {
        super.onAttach(context)
        dashBoardHandler = context as DashBoardHandler
    }

    override fun onDetach()
    {
        super.onDetach()
        dashBoardHandler = null
    }

    private fun onButtonClick(view: View)
    {
        val id = view.id
        if (id == R.id.btnLogin)
        {
            doSignIn()
        } else if (id == R.id.btnEnquiry)
        {
            gotoEnquiryFragment()
        } else if (id == R.id.tvLoginTermsAndConditions)
        {
            val strTACUri = StringBuilder()
            strTACUri.append("https://www.oiyaa.com//terms_conditions/WebsiteTermsandConditions.pdf")
            showTermsAndConditionsOrPrivacyPolicy(
                strTACUri.toString(),
                getString(R.string.lbl_open_terms_and_conditions_file)
            )
        } else if (id == R.id.tvLoginPrivacyPolicy)
        {
            val strUri = StringBuilder()
            strUri.append("https://www.oiyaa.com//terms_conditions/App%20Privacy%20Policy.pdf")
            showTermsAndConditionsOrPrivacyPolicy(
                strUri.toString(),
                getString(R.string.lbl_open_terms_and_conditions_file)
            )
        }
    }

    private fun doSignIn()
    {
        Util.hideSoftKeyboard(requireContext(), binding!!.etEmail)
        Util.hideSoftKeyboard(requireContext(), binding!!.etPassword)
        fcmToken?.let {
            val signInInput = SignInInput()
            signInInput.email=binding!!.etEmail.text.toString()
            signInInput.password=binding!!.etPassword.text.toString()
            signInInput.fcmId=fcmToken
            viewModel.signIn(signInInput)
        }
    }

    private fun showProgress()
    {
        binding!!.btnLogin.visibility = View.GONE
        binding!!.progressIndicator.visibility=View.VISIBLE
    }

    private fun hideProgress()
    {
        binding!!.btnLogin.visibility = View.VISIBLE
        binding!!.progressIndicator.visibility=View.GONE
    }

    private fun gotoEnquiryFragment()
    {
        dashBoardHandler?.register()
    }

    private fun showTermsAndConditionsOrPrivacyPolicy(strUri: String, title: String)
    {
        val browserIntent = Intent(Intent.ACTION_VIEW)
        browserIntent.setDataAndType(Uri.parse(strUri), "application/pdf")
        val chooser = Intent.createChooser(browserIntent, title)
        chooser.flags = Intent.FLAG_ACTIVITY_NEW_TASK // optional
        startActivity(chooser)
    }

}