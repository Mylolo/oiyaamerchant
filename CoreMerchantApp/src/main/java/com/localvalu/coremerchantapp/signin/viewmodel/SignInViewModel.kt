package com.localvalu.coremerchantapp.signin.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.localvalu.coremerchantapp.base.Resource
import com.localvalu.coremerchantapp.data.input.SignInInput
import com.localvalu.coremerchantapp.data.output.SignInOutput
import com.localvalu.coremerchantapp.signin.repository.SignInRepository
import com.utils.validator.Validator
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SignInViewModel @Inject constructor(private val repository: SignInRepository) : ViewModel()
{
    private val _signInResponse: MutableLiveData<Resource<SignInOutput>> = MutableLiveData()

    val signInResponse: LiveData<Resource<SignInOutput>>
        get() = _signInResponse

    val email:MutableLiveData<String> = MutableLiveData()

    val password:MutableLiveData<String> = MutableLiveData()

    val validEmail:MutableLiveData<Boolean> = MutableLiveData()

    val validPassword:MutableLiveData<Boolean> = MutableLiveData()

    val enableDisableSignIn:MutableLiveData<Boolean> = MutableLiveData()

    fun signIn(signInInput: SignInInput) = viewModelScope.launch {
        _signInResponse.value = Resource.Loading
        _signInResponse.value = repository.signIn(signInInput)
    }

    fun setEmail(email:String)
    {
        this.email.value = email
        validateEmail()
        enableDisableSignIn()
    }

    fun setPassword(password:String)
    {
        this.password.value = password
        validatePassword()
        enableDisableSignIn()
    }

    fun validateEmail()
    {
        validEmail.value = Validator.isValidEmail(email.value)
    }

    fun validatePassword()
    {
        validPassword.value = Validator.isValid(password.value)
    }

    fun enableDisableSignIn()
    {
        enableDisableSignIn.value = (validEmail.value == true && validPassword.value == true)
    }

}