package com.localvalu.coremerchantapp.tablebookings.api

import com.localvalu.coremerchantapp.BaseApi
import com.localvalu.coremerchantapp.data.input.TableBookingListInput
import com.localvalu.coremerchantapp.data.input.TableBookingStatusChangeInput
import com.localvalu.coremerchantapp.data.output.tablebooking.TableBookingListOutput
import com.localvalu.coremerchantapp.data.output.tablebooking.TableBookingStatusChangeOutput
import retrofit2.http.Body
import retrofit2.http.POST

interface TableBookingsApi : BaseApi
{
    @POST("MerchantApp/TableBookingList")
    suspend fun bookingList(@Body tableBookingListInput: TableBookingListInput):TableBookingListOutput

    @POST("MerchantApp/ChangeTableBookStatus")
    suspend fun acceptDeclineBooking(@Body tableBookingStatusChangeInput: TableBookingStatusChangeInput):TableBookingStatusChangeOutput
}