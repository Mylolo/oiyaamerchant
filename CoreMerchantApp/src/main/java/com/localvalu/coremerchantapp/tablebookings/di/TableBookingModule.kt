package com.localvalu.coremerchantapp.tablebookings.di

import android.content.Context
import com.localvalu.coremerchantapp.myappointments.api.MyAppointmentsApi
import com.localvalu.coremerchantapp.myappointments.repository.MyAppointmentsRepository
import com.localvalu.coremerchantapp.network.RemoteDataSource
import com.localvalu.coremerchantapp.tablebookings.api.TableBookingsApi
import com.localvalu.coremerchantapp.tablebookings.repository.TableBookingRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class TableBookingModule
{
    @Provides
    fun providesTableBookingApi(remoteDataSource: RemoteDataSource,
                                 @ApplicationContext context:Context):TableBookingsApi
    {
        return (remoteDataSource.buildApi(TableBookingsApi::class.java,context))
    }

    @Provides
    fun providesTableBookingRepository(tableBookingsApi: TableBookingsApi):TableBookingRepository
    {
        return TableBookingRepository(tableBookingsApi)
    }

}