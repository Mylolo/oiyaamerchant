package com.localvalu.coremerchantapp.tablebookings.repository

import com.localvalu.coremerchantapp.base.BaseRepository
import com.localvalu.coremerchantapp.data.input.MyAppointmentListInput
import com.localvalu.coremerchantapp.data.input.MyAppointmentStatusChangeInput
import com.localvalu.coremerchantapp.data.input.TableBookingListInput
import com.localvalu.coremerchantapp.data.input.TableBookingStatusChangeInput
import com.localvalu.coremerchantapp.myappointments.api.MyAppointmentsApi
import com.localvalu.coremerchantapp.tablebookings.api.TableBookingsApi

class TableBookingRepository(val api: TableBookingsApi) : BaseRepository(api)
{
    suspend fun tableBookingList(tableBookingListInput: TableBookingListInput) = safeApiCall {
        api.bookingList(tableBookingListInput)
    }

    suspend fun acceptDeclineBooking(tableBookingStatusChangeInput: TableBookingStatusChangeInput) = safeApiCall {
        api.acceptDeclineBooking(tableBookingStatusChangeInput)
    }
}