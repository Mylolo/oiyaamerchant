package com.localvalu.coremerchantapp.tablebookings.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.localvalu.coremerchantapp.base.Resource
import com.localvalu.coremerchantapp.data.input.MyAppointmentListInput
import com.localvalu.coremerchantapp.data.input.MyAppointmentStatusChangeInput
import com.localvalu.coremerchantapp.data.input.TableBookingListInput
import com.localvalu.coremerchantapp.data.input.TableBookingStatusChangeInput
import com.localvalu.coremerchantapp.data.output.myappointments.MyAppointmentChangeStatusResult
import com.localvalu.coremerchantapp.data.output.myappointments.MyAppointmentListOutput
import com.localvalu.coremerchantapp.data.output.myappointments.MyAppointmentStatusChangeOutput
import com.localvalu.coremerchantapp.data.output.tablebooking.TableBookingListOutput
import com.localvalu.coremerchantapp.data.output.tablebooking.TableBookingStatusChangeOutput
import com.localvalu.coremerchantapp.merchantdashboard.model.MerchantDashboardResponse
import com.localvalu.coremerchantapp.myappointments.repository.MyAppointmentsRepository
import com.localvalu.coremerchantapp.tablebookings.repository.TableBookingRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class TableBookingViewModel @Inject constructor(val repository: TableBookingRepository):ViewModel()
{
    private val _tableBookingResponse:MutableLiveData<Resource<TableBookingListOutput>> = MutableLiveData()

    val tableBookingResponse: LiveData<Resource<TableBookingListOutput>>
        get() = _tableBookingResponse

    fun tableBookings(tableBookingListInput: TableBookingListInput) = viewModelScope.launch{
        _tableBookingResponse.value = Resource.Loading
        _tableBookingResponse.value = repository.tableBookingList(tableBookingListInput)
    }

    private val _tableBookingAcceptDeclineResponse:MutableLiveData<Resource<TableBookingStatusChangeOutput>> = MutableLiveData()

    val tableBookingAcceptDeclineResponse:LiveData<Resource<TableBookingStatusChangeOutput>>
        get() = _tableBookingAcceptDeclineResponse

    fun acceptOrDecline(tableBookingStatusChangeInput: TableBookingStatusChangeInput) = viewModelScope.launch{
        _tableBookingAcceptDeclineResponse.value = Resource.Loading
        _tableBookingAcceptDeclineResponse.value = repository.acceptDeclineBooking(tableBookingStatusChangeInput)
    }

}