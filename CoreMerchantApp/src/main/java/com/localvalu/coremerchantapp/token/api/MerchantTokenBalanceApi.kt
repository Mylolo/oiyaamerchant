package com.localvalu.coremerchantapp.token.api

import com.localvalu.coremerchantapp.BaseApi
import com.localvalu.coremerchantapp.data.input.TokenBalanceInput
import com.localvalu.coremerchantapp.data.output.TokenBalanceOutput
import com.localvalu.coremerchantapp.merchantdashboard.model.MerchantDashboardRequest
import com.localvalu.coremerchantapp.merchantdashboard.model.MerchantDashboardResponse
import com.localvalu.coremerchantapp.token.model.MerchantTokenBalanceRequest
import retrofit2.http.Body
import retrofit2.http.POST

interface MerchantTokenBalanceApi : BaseApi
{
    @POST("RetailerBalance")
    suspend fun merchantTokenBalance(@Body merchantTokenBalanceRequest: MerchantTokenBalanceRequest): TokenBalanceOutput
}