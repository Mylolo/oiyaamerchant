package com.localvalu.coremerchantapp.token.di

import android.content.Context
import com.localvalu.coremerchantapp.merchantdashboard.api.MerchantDashboardApi
import com.localvalu.coremerchantapp.merchantdashboard.repository.MerchantDashboardRepository
import com.localvalu.coremerchantapp.network.RemoteDataSource
import com.localvalu.coremerchantapp.token.api.MerchantTokenBalanceApi
import com.localvalu.coremerchantapp.token.repository.MerchantTokenBalanceRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class MerchantTokenBalanceModule
{
    @Provides
    fun providesMerchantTokenBalanceRepository(remoteDataSource: RemoteDataSource,
                                     @ApplicationContext context: Context): MerchantTokenBalanceApi
    {
        return remoteDataSource.buildApiDotNet(MerchantTokenBalanceApi::class.java,context)
    }

    @Provides
    fun providesPassionListRepository(merchantTokenBalanceApi: MerchantTokenBalanceApi):MerchantTokenBalanceRepository
    {
        return MerchantTokenBalanceRepository(merchantTokenBalanceApi)
    }
}