package com.localvalu.coremerchantapp.token.repository

import com.localvalu.coremerchantapp.base.BaseRepository
import com.localvalu.coremerchantapp.token.api.MerchantTokenBalanceApi
import com.localvalu.coremerchantapp.token.model.MerchantTokenBalanceRequest

class MerchantTokenBalanceRepository (private val api: MerchantTokenBalanceApi): BaseRepository(api)
{
    suspend fun merchantTokenBalance(merchantTokenBalanceRequest: MerchantTokenBalanceRequest) = safeApiCall {
        api.merchantTokenBalance(merchantTokenBalanceRequest)
    }
}