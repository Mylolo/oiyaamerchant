package com.localvalu.coremerchantapp.token.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.localvalu.coremerchantapp.base.Resource
import com.localvalu.coremerchantapp.data.output.TokenBalanceOutput
import com.localvalu.coremerchantapp.token.model.MerchantTokenBalanceRequest
import com.localvalu.coremerchantapp.token.repository.MerchantTokenBalanceRepository

import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MerchantTokenBalanceViewModel @Inject constructor(val repository:MerchantTokenBalanceRepository) : ViewModel()
{
    private val _merchantTokenBalanceResponse: MutableLiveData<Resource<TokenBalanceOutput>> = MutableLiveData()
    val merchantTokenBalanceResponse: LiveData<Resource<TokenBalanceOutput>>
        get() = _merchantTokenBalanceResponse

    fun merchantTokenBalance(merchantTokenBalanceRequest: MerchantTokenBalanceRequest) = viewModelScope.launch {
        _merchantTokenBalanceResponse.value = Resource.Loading
        _merchantTokenBalanceResponse.value = repository.merchantTokenBalance(merchantTokenBalanceRequest)
    }

}