package com.localvalu.coremerchantapp.transaction.api

import com.localvalu.coremerchantapp.BaseApi
import com.localvalu.coremerchantapp.data.input.MyTransactionInput
import com.localvalu.coremerchantapp.data.output.MyTransactionOutput
import retrofit2.http.Body
import retrofit2.http.POST

interface TransactionsApi : BaseApi
{
    @POST("MerchantAppTransactionHistory")
    suspend fun transactions(@Body transactionInput: MyTransactionInput) : MyTransactionOutput
}