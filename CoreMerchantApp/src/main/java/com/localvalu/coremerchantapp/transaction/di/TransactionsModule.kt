package com.localvalu.coremerchantapp.transaction.di

import android.content.Context
import com.localvalu.coremerchantapp.network.RemoteDataSource
import com.localvalu.coremerchantapp.transaction.api.TransactionsApi
import com.localvalu.coremerchantapp.transaction.repository.TransactionsRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class TransactionsModule
{
    @Provides
    fun providesTransactionsApi(remoteDataSource: RemoteDataSource,@ApplicationContext context: Context) : TransactionsApi
    {
        return remoteDataSource.buildApiDotNet(TransactionsApi::class.java,context)
    }

    @Provides
    fun providesTransactionsRepository(api: TransactionsApi) : TransactionsRepository
    {
        return TransactionsRepository(api)
    }

}