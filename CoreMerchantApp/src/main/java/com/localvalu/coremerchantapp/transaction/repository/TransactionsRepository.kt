package com.localvalu.coremerchantapp.transaction.repository

import com.localvalu.coremerchantapp.base.BaseRepository
import com.localvalu.coremerchantapp.data.input.MyTransactionInput
import com.localvalu.coremerchantapp.transaction.api.TransactionsApi

class TransactionsRepository(val api:TransactionsApi):BaseRepository(api)
{
    suspend fun transactions(myTransactionInput: MyTransactionInput) = safeApiCall{
        api.transactions(myTransactionInput)
    }
}