package com.localvalu.coremerchantapp.transaction.viewmodel

import androidx.lifecycle.*
import com.localvalu.coremerchantapp.base.Resource
import com.localvalu.coremerchantapp.data.input.MyTransactionInput
import com.localvalu.coremerchantapp.data.output.MyTransactionOutput
import com.localvalu.coremerchantapp.transaction.repository.TransactionsRepository
import com.localvalu.coremerchantapp.utils.*
import com.utils.validator.Validator
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

@HiltViewModel
class TransactionsViewModel @Inject constructor(private val repository: TransactionsRepository) : ViewModel()
{
    private val _transactionsResponse: MutableLiveData<Resource<MyTransactionOutput>> = MutableLiveData()

    private val startDate:MutableLiveData<Date> = MutableLiveData()

    private val endDate:MutableLiveData<Date> = MutableLiveData()

    val strStartDate:LiveData<String> = Transformations.map(startDate){ sDate->
        sDate?.let {
            getDisplayFormat(sDate.time)
        }
    }

    val strEndDate:LiveData<String> = Transformations.map(endDate){ eDate->
        eDate?.let {
            getDisplayFormat(eDate.time)
        }
    }

    private val strServerStartDate:LiveData<String> = Transformations.map(startDate){ sDate->
        sDate?.let {
            getServerFormat(sDate.time)
        }
    }

    private val strServerEndDate:LiveData<String> = Transformations.map(endDate){ eDate->
        eDate?.let {
            getServerFormat(eDate.time)
        }
    }

    val initialCall:MutableLiveData<Boolean> = MutableLiveData()

    init
    {
        initialCall.value=false
        startDate.value = getStartDateOfCurrentMonth()
        endDate.value = getCalenderEndDate()
    }

    val transactionsResponse: LiveData<Resource<MyTransactionOutput>>
        get() = _transactionsResponse

    fun transactions(myTransactionInput: MyTransactionInput) = viewModelScope.launch {
        myTransactionInput.fromDate = getServerFormat(startDate?.value?.time)
        myTransactionInput.toDate = getServerFormat(endDate?.value?.time)
        _transactionsResponse.value = Resource.Loading
        _transactionsResponse.value = repository.transactions(myTransactionInput)
    }

    val enableSendNow:MutableLiveData<Boolean> = MutableLiveData()

    fun setStartDate(startDate:Date)
    {
        if(this.startDate.equals(startDate)) return
        this.startDate.value = startDate
    }

    fun setEndDate(endDate:Date)
    {
        if(this.endDate.equals(endDate)) return
        this.endDate.value = endDate
    }

    fun enableDisableSend(sDate:String,eDate:String)
    {
        enableSendNow.value = sDate.isNotEmpty() && eDate.isNotEmpty()
    }

}