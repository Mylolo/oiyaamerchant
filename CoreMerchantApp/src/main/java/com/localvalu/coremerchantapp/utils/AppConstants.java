package com.localvalu.coremerchantapp.utils;

public class AppConstants
{
    /**
     * Status 3 -- Waiting for confirm
     * Status 2 -- Declined
     * Status 1 -- Delivered
     * Status 0 -- Accept
     * Status 6 -- Order Cancelled
     * Status 5 -- Booking Table Cancelled
     * Status 20 -- Print
     */
    public static final String PENDING = "3";
    public static final String DECLINED = "2";
    public static final String DELIVERED= "1";
    public static final String ACCEPT = "0";
    public static final String ORDER_CANCELLED = "6";
    public static final String BOOKING_CANCELLED = "5";
    public static final String PRINT = "20";

    public static final String PAID = "Paid";
    public static final String NOT_PAID = "Not Paid";

    public static final String RETAILER_TYPE_EATS="Eats";
    public static final String RETAILER_TYPE_LOCAL="Local";

    //Delivery or Collection
    public static final String ORDER_OPTION_DELIVERY="Delivery";

    //Driver selected is true
    public static final String ORDER_DRIVER_SELECTED="1";

    //Driver type check -
    public static final int OWN_DRIVER = 1;
    public static final int POOL_DRIVER = 2;

    public static final int POOL_DRIVERS = 1;
    public static final int OWN_DRIVERS = 2;
    public static final int NO_DRIVERS=0;

    public static final String AL_PUSH_NOTIFICATION = "AL_PUSH_NOTIFICATION";

    // id to handle the notification in the notification tray
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;

}
