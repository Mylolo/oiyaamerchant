package com.localvalu.coremerchantapp.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AppUtils
{
    public static String APP_PREFERENCES = "App_Preferences";
    public static String BUNDLE_CUSTOMER_QR_CODE = "Bundle_CustomerQR_Code";
    public static String BUNDLE_ORDER_ID = "Bundle_OrderId";
    public static String BUNDLE_ORDER_DETAILS = "Bundle_OrderDetails";
    public static String BUNDLE_ORDER_LIST_RESULT = "Bundle_OrderListResult";
    public static String BUNDLE_ORDER_AT_TABLE_LIST_RESULT = "Bundle_OrderAtTableListResult";
    public static String BUNDLE_BOOKING_TABLE_LIST_RESULT = "Bundle_BookingTableListResult";
    public static final String BUNDLE_DRIVER_LIST = "Bundle_DriverList";

    public static String WORKER_RESULT_ORDER_LIST_KEY = "Result_Order_List_Key";
    public static final String BUNDLE_RETAILER_ID = "Bundle_RetailerId";
    public static final String BUNDLE_BOOKING_TYPE_ID = "Bundle_BookingTypeId";
    public static final String BUNDLE_RETAILER_TYPE = "Bundle_RetailerType";


    //PAX
    public static final String BUNDLE_DISPLAY_PAYABLE_AMOUNT = "Bundle_Display_Payable_Amount";
    public static final String BUNDLE_DISPLAY_CASHBACK_AMOUNT = "Bundle_Display_Cashback_Amount";
    public static final String BUNDLE_PAYABLE_AMOUNT = "Bundle_Payable_Amount";
    public static final String BUNDLE_CASHBACK_AMOUNT = "Bundle_Cashback_Amount";
    public static final String BUNDLE_LAST_RECEIVED_UTI = "Bundle_Last_Received_UTI";

    public static final String BUNDLE_TRANSACTION_RESPONSE_LIST = "Bundle_Transaction_Response_List";
    public static final String BUNDLE_TRANSACTION_STATUS_RESPONSE_LIST = "Bundle_Transaction__Status_Response_List";
    public static final String BUNDLE_TRANSACTION_RESPONSE_TITLE = "Bundle_Transaction_Response_Title";
    public static final String BUNDLE_RESPONSE_LIST = "Bundle_Response_List";
    public static final String BUNDLE_TRANSACTION_ERROR_CODE = "Bundle_Transaction_Error_Code";
    public static final String BUNDLE_TRANSACTION_ERROR_MESSAGE = "Bundle_Transaction_Error_Message";
    public static final String BUNDLE_RESPONSE_LIST_COMMON = "Bundle_Response_List_Common";
    public static final String INTENT_FILTER_PAX_TRANSACTION_RECEIVER="PAX_TRANSACTION_RECEIVER";

    //Notification and Broadcast Receivers
    public static final String ORDER_LIST_SERVICE = "com.localvalu.merchant.components.NewRequestsCheckService";
    public static final String BROADCAST_ACTION = "com.localvalu.merchant.orderlist";
    public static final String BROADCAST_ACTION_ORDER_AT_TABLE_LIST = "com.localvalu.merchant.orderAtTableList";
    public static final String BROADCAST_ACTION_TABLE_BOOKING_LIST = "com.localvalu.merchant.tableBookingList";
    public static final String ACTION_NOTIFICATION_START = "com.localvalu.merchant.Start";
    public static final String ACTION_NOTIFICATION_END = "com.localvalu.merchant.Exit";

    public static final String BUNDLE_TRANSACTION_SUCCESS_FAILURE_DETAILS = "Bundle_Transaction_Success_Failure_Details";

    public static final String DEVICE_MODEL_T1N="T1N";

    public static String capitalizeFirstLetterInEachWord(String capString)
    {
        StringBuffer capBuffer = new StringBuffer();
        Matcher capMatcher = Pattern.compile("([a-z])([a-z]*)", Pattern.CASE_INSENSITIVE).matcher(capString);
        while (capMatcher.find())
        {
            capMatcher.appendReplacement(capBuffer, capMatcher.group(1).toUpperCase() + capMatcher.group(2).toLowerCase());
        }

        return capMatcher.appendTail(capBuffer).toString();
    }
}
