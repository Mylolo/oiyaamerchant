package com.localvalu.coremerchantapp.utils

import com.utils.util.DateTimeUtil
import java.text.SimpleDateFormat
import java.util.*

fun getStartDateOfCurrentMonth():Date
{
    val calendar = DateTimeUtil.getCalendar()
    calendar.set(Calendar.DAY_OF_MONTH,1)
    return calendar.time
}

fun getEndDateOfCurrentMonth():Date
{
    val calendar = DateTimeUtil.getCalendar()
    calendar.set(Calendar.DAY_OF_MONTH,1)
    val noOfDays = calendar.getActualMaximum(Calendar.DAY_OF_MONTH)
    calendar.set(Calendar.DAY_OF_MONTH,noOfDays)
    return calendar.time
}

fun getServerFormat(time:Long?):String
{
    return SimpleDateFormat("yyyy-MM-dd").format(time)
}

fun getDisplayFormat(time:Long?):String
{
    return SimpleDateFormat("dd/MM/yyyy").format(time)
}

fun getCalenderStartDate():Date
{
    val calendar:Calendar = Calendar.getInstance()
    calendar.set(2015,1,1)
    return calendar.time
}

fun getCalenderEndDate():Date
{
    val calendar:Calendar = Calendar.getInstance()
    return calendar.time
}