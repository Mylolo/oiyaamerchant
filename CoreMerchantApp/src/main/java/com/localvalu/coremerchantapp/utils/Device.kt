package com.localvalu.coremerchantapp.utils

class Device
{
    companion object
    {
        val devicePlayStore = 1
        val deviceT1N = 2
        val deviceClover = 3
        val devicePOS = 4
        val deviceOptomany=5
        val deviceIngenico=6

    }

}