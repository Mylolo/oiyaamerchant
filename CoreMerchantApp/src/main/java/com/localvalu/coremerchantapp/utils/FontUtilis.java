package com.localvalu.coremerchantapp.utils;

import android.content.Context;
import android.graphics.Typeface;

import androidx.core.content.res.ResourcesCompat;

import com.localvalu.coremerchantapp.R;


public class FontUtilis
{
    // static variable single_instance of type Singleton
    private static FontUtilis fontInstance = null;

    // variable of type Instace
    Typeface typeface;

    // private constructor restricted to this class itself
    private FontUtilis()
    {

    }

    // static method to create instance of Singleton class
    public static FontUtilis getInstance()
    {
        if (fontInstance == null)
            fontInstance = new FontUtilis();

        return fontInstance;
    }

    public Typeface comfortaaRegular(Context context)
    {
        typeface = ResourcesCompat.getFont(context, R.font.comfortaa_regular);
        return typeface;
    }

    public Typeface comfortaaBold(Context context)
    {
        typeface = ResourcesCompat.getFont(context, R.font.comfortaa_bold);
        return typeface;
    }

}
