package com.localvalu.coremerchantapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.localvalu.coremerchantapp.base.CommonRepository
import com.localvalu.coremerchantapp.data.output.SignInResult
import com.localvalu.coremerchantapp.storage.UserPreferences
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class DashboardViewModel @Inject constructor(
    private val userPreferences: UserPreferences,
    val gson: Gson
): ViewModel()
{

    private val _user: MutableLiveData<SignInResult> = MutableLiveData()
    val user: LiveData<SignInResult>
        get() = _user

    fun getUser() = viewModelScope.launch {
        userPreferences.userString?.collect{
            _user.value = gson.fromJson(it, SignInResult::class.java)
        }
    }

    fun saveUser(user:SignInResult)
    {
        viewModelScope.launch {
            userPreferences.saveUserObject(gson.toJson(user))
        }
    }

    private val _fcmToken: MutableLiveData<String> = MutableLiveData()
    val fcmToken: LiveData<String>
        get() = _fcmToken

    fun getFCMToken() = viewModelScope.launch {
        userPreferences.fcmTokeString?.collect{
            _fcmToken.value = it
        }
    }

    fun saveFCM(fcmToken:String)
    {
        viewModelScope.launch {
            userPreferences.saveFCMObject(fcmToken)
        }
    }

}