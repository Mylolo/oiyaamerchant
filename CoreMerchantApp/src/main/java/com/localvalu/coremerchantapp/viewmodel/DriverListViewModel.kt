package com.localvalu.coremerchantapp.viewmodel

import DriverAppBase
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.localvalu.coremerchantapp.data.input.NotifyToDriversInput
import com.localvalu.coremerchantapp.data.input.NotifyToPoolDriversInput
import com.localvalu.coremerchantapp.data.input.PoolDriverListInput
import com.localvalu.coremerchantapp.data.output.driver.OwnDriversListRequest
import com.localvalu.coremerchantapp.data.output.driverOld.DriverNotifictionResultOutput
import com.localvalu.coremerchantapp.data.output.pooldrivers.GetPoolDriverListResultOutput
import com.localvalu.coremerchantapp.generic.Resource
import com.localvalu.coremerchantapp.repository.NotifyDriversRepository
import com.localvalu.coremerchantapp.repository.OwnDriverRepository
import com.localvalu.coremerchantapp.repository.PoolDriverRepository

class DriverListViewModel : ViewModel()
{
    /**
     * Getting own drivers list starts
     */
    private val _ownDriverListRequest:MutableLiveData<OwnDriversListRequest> = MutableLiveData()

    val ownDriversListOutput:LiveData<Resource<DriverAppBase>> = Transformations.
    switchMap(_ownDriverListRequest)
    {
        OwnDriverRepository.getOwnDriverList(it)
    }

    fun setOwnDriverRequest(ownDriversListRequest: OwnDriversListRequest)
    {
        _ownDriverListRequest.value = ownDriversListRequest
    }

    /**
     * Getting Pool drivers list starts
     */
    private val _poolDriverListRequest:MutableLiveData<PoolDriverListInput> = MutableLiveData()

    val poolDriverListOutput:LiveData<Resource<GetPoolDriverListResultOutput>> = Transformations.
        switchMap(_poolDriverListRequest)
        {
            PoolDriverRepository.getPoolDriverList(it)
        }

    fun setPoolDriverRequest(poolDriverListInput: PoolDriverListInput)
    {
        _poolDriverListRequest.value = poolDriverListInput
    }

    /**
     * Getting Pool drivers list starts
     */

    /**
     * Notify multiple drivers starts
     */

    private val _notifyDriverRequest:MutableLiveData<NotifyToPoolDriversInput> = MutableLiveData()

    val notifyDriversOutput:LiveData<Resource<DriverNotifictionResultOutput>> = Transformations.
    switchMap(_notifyDriverRequest)
    {
        NotifyDriversRepository.notifyDrivers(it)
    }

    fun setNotifyPoolDriversRequest(notifyToPoolDriversInput: NotifyToPoolDriversInput)
    {
        _notifyDriverRequest.value = notifyToPoolDriversInput
    }

    /**
     * Notify multiple drivers ends
     */

    /**
     * Notify single driver starts
     */

    private val _notifySingleDriverRequest:MutableLiveData<NotifyToDriversInput> = MutableLiveData()

    val notifySingleDriverOutput:LiveData<Resource<DriverNotifictionResultOutput>> = Transformations.
    switchMap(_notifySingleDriverRequest)
    {
        NotifyDriversRepository.notifySingleDriver(it)
    }

    fun setNotifySingleDriverRequest(notifyToDriversInput: NotifyToDriversInput)
    {
        _notifySingleDriverRequest.value = notifyToDriversInput
    }

    /**
     * Notify single driver ends
     */


    fun cancelJobs()
    {
        PoolDriverRepository.cancelJobs()
        NotifyDriversRepository.cancelJobs()
    }

}