package com.localvalu.coremerchantapp.merchantdashboard.fakeapi

import ErrorDetails
import com.localvalu.coremerchantapp.merchantdashboard.api.MerchantDashboardApi
import com.localvalu.coremerchantapp.merchantdashboard.model.MerchantDashboardData
import com.localvalu.coremerchantapp.merchantdashboard.model.MerchantDashboardRequest
import com.localvalu.coremerchantapp.merchantdashboard.model.MerchantDashboardResponse
import com.localvalu.coremerchantapp.merchantdashboard.model.MerchantDashboardResult

class FakeMerchantDashboardApi : MerchantDashboardApi
{
    private var success:Boolean = false;

    private fun getMerchantDataResponse() : MerchantDashboardResponse
    {
        val merchantDashboardData: MerchantDashboardData =
            MerchantDashboardData(getSuccessOrError(success),"200",
                "200","200","200",
                "200","200")
        val merchantDashboardResult: MerchantDashboardResult =
            MerchantDashboardResult(merchantDashboardData)
        return MerchantDashboardResponse(merchantDashboardResult)
    }

    private fun getSuccessOrError(success:Boolean) : ErrorDetails
    {
        return if(success) ErrorDetails(0,"SuccessMessage")
        else ErrorDetails(1,"ErrorMessage")
    }

    override suspend fun merchantDashboard(merchantDashboardRequest: MerchantDashboardRequest): MerchantDashboardResponse
    {
        return getMerchantDataResponse()
    }

    fun setSuccess(value: Boolean)
    {
        this.success = value
    }

}