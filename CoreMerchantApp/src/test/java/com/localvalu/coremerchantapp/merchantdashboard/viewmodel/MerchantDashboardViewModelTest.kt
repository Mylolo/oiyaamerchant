package com.localvalu.coremerchantapp.merchantdashboard.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.localvalu.coremerchantapp.base.Resource
import com.localvalu.coremerchantapp.MainCoroutineRule
import com.localvalu.coremerchantapp.getOrAwaitValueTest
import com.localvalu.coremerchantapp.merchantdashboard.model.MerchantDashboardRequest
import com.localvalu.coremerchantapp.merchantdashboard.model.MerchantDashboardResponse
import com.localvalu.coremerchantapp.merchantdashboard.repository.MerchantDashboardRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import com.localvalu.coremerchantapp.merchantdashboard.fakeapi.FakeMerchantDashboardApi
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import org.hamcrest.CoreMatchers.`is`
import okhttp3.ResponseBody

@ExperimentalCoroutinesApi
class MerchantDashboardViewModelTest
{
    @get:Rule
    var instantTaskExecutor = InstantTaskExecutorRule()

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private lateinit var  merchantDashboardViewModel:MerchantDashboardViewModel

    private lateinit var fakeMerchantDashboardApi: FakeMerchantDashboardApi

    @Before
    public fun setUp()
    {
        fakeMerchantDashboardApi = FakeMerchantDashboardApi()
        fakeMerchantDashboardApi.setSuccess(true)
        merchantDashboardViewModel = MerchantDashboardViewModel(MerchantDashboardRepository(fakeMerchantDashboardApi))
    }

    @Test
    fun testMerchantDashboardReceivesCorrectData()
    {
        val merchantDashboardRequest = MerchantDashboardRequest("","")
        merchantDashboardViewModel.merchantDashboard(merchantDashboardRequest)

        when(val response: Resource<MerchantDashboardResponse> = merchantDashboardViewModel.merchantDashboardResponse.getOrAwaitValueTest())
        {
            is Resource.Success ->
            {
                assertThat(response.value.merchantDashboardResult.
                merchantDashboardData.errorDetails.errorCode.toString(),`is`("0"))
            }
            else->
            {

            }
        }
    }

    @Test
    fun testMerchantDashboardReceivesInCorrectData()
    {
        fakeMerchantDashboardApi.setSuccess(false)

        val merchantDashboardRequest = MerchantDashboardRequest("","")
        merchantDashboardViewModel.merchantDashboard(merchantDashboardRequest)

        when(val response: Resource<MerchantDashboardResponse> = merchantDashboardViewModel.merchantDashboardResponse.getOrAwaitValueTest())
        {
            is Resource.Success ->
            {
                assertThat(response.value.merchantDashboardResult.
                merchantDashboardData.errorDetails.errorCode.toString(),`is`("1"))
            }
            else->
            {

            }
        }
    }

    @Test
    fun testMerchantDashboardReceivesErrorData()
    {
        val merchantDashboardRequest = MerchantDashboardRequest("*","*")
        merchantDashboardViewModel.merchantDashboard(merchantDashboardRequest)

        val JSON = "application/json; charset=utf-8".toMediaTypeOrNull()
        val body: ResponseBody = ResponseBody.create(JSON, "json")
        val response: Resource<MerchantDashboardResponse> = Resource.Failure(true,400,body)

        when(response)
        {
            is Resource.Success ->
            {
                assertThat(response.value.merchantDashboardResult.
                merchantDashboardData.errorDetails.errorCode.toString(),`is`("1"))
            }
            is Resource.Failure->
            {
                assertThat(response.isNetworkError,`is`(true))
            }
        }
    }
}