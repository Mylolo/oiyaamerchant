package com.oiyaa.merchant.ingenico.utils;

/**
 * Helper class.
 */
public final class ByteUtils {
    private static final char[] HEX_ARRAY = "0123456789abcdef".toCharArray();

    private ByteUtils() {
    }

    /**
     * Fastest solution as seen here:
     * https://stackoverflow.com/questions/9655181/how-to-convert-a-byte-array-to-a-hex-string-in-java
     */
    public static String bytesToHexString(byte[] bytes) {
        if (bytes == null) {
            return "";
        }

        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars);
    }

    /**
     * Convert an int array to a byte array, to avoid having to cast each value when declaring an
     * array with hex values.
     * Used only for tests.
     * @param input
     * @return
     */
    public static byte[] intToByteArray(int[] input) {
        byte[] byteArray = new byte[input.length];
        for (int i = 0; i < input.length; i++) {
            byteArray[i] = (byte) input[i];
        }
        return byteArray;
    }
}
