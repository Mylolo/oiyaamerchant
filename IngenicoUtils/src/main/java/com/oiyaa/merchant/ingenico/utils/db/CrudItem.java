package com.oiyaa.merchant.ingenico.utils.db;

/**
 * To be usable with the default implementation of {@link com.ingenico.utils.ui.ViewBinder}, a
 * model object must implement this interface to ensure that a setValue mathod is defined.
 */
public interface CrudItem {
    /**
     * This method will be called on creation/update of an item, to update the desired property
     * with the value provided by the user.
     * @param value
     */
    void setValue(String value);
}
