package com.oiyaa.merchant.ingenico.utils.db;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;

import java.util.List;

/**
 * Implement this interface to declare a provider for your data source.
 * @param <T>
 */
public interface CrudProvider<T> {
    LiveData<List<T>> readAll();
    void create(@NonNull T key);
    void update(@NonNull T key);
    void delete(@NonNull T key);
}
