package com.oiyaa.merchant.ingenico.utils.db;

/**
 * Implement this interface to provide a custom inputType value for the edit field.
 * @see android.text.InputType
 */
public interface CustomInputType {
    int getInputType();
}
