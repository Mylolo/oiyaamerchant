package com.oiyaa.merchant.ingenico.utils.db;

import android.os.AsyncTask;

import com.oiyaa.merchant.ingenico.utils.function.BiConsumer;
import com.oiyaa.merchant.ingenico.utils.function.Consumer;

/**
 * An AsyncTask to execute a single action on the specified DAO.
 * @param <U>
 * @param <V>
 */
public class DbActionTask<U, V> extends AsyncTask<U, Void, Void> {
    private V dao;
    private BiConsumer<V, U> biFunction = null;
    private Consumer<V> function = null;

    /**
     * Constructor for methods with U as an argument.
     * @param dao
     * @param function
     */
    public DbActionTask(V dao, BiConsumer<V, U> function) {
        this.dao = dao;
        this.biFunction = function;
    }

    /**
     * Constructor for non-arguments methods. U should be void in this case.
     * @param dao
     * @param function
     */
    public DbActionTask(V dao, Consumer<V> function) {
        this.dao = dao;
        this.function = function;
    }

    @Override
    protected Void doInBackground(U... ts) {
        if (biFunction != null) {
            biFunction.accept(dao, ts[0]);
        } else if (function != null) {
            function.accept(dao);
        }
        return null;
    }
}
