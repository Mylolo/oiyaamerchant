package com.oiyaa.merchant.ingenico.utils.db;

import android.os.AsyncTask;

import androidx.arch.core.util.Function;

/**
 * An AsyncTask to request a single object on the specified table.
 * @param <U>
 * @param <V>
 */
public class DbRequestTask<U, V> extends AsyncTask<Void, Void, U> {
    private V dao;
    private Function<V, U> function;

    public DbRequestTask(V dao, Function<V, U> function) {
        this.dao = dao;
        this.function = function;
    }

    @Override
    protected U doInBackground(Void... ts) {
        return function.apply(dao);
    }
}
