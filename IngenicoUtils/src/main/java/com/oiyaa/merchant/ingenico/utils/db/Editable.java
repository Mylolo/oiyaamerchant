package com.oiyaa.merchant.ingenico.utils.db;

/**
 * Implement this interface on your data objects to be able to have some non-editable items.
 */
public interface Editable {
    boolean isEditable();
}
