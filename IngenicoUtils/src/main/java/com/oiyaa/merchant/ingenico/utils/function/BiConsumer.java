package com.oiyaa.merchant.ingenico.utils.function;

public interface BiConsumer<T, U> {
    void accept(T var1, U var2);
}
