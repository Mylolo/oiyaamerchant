package com.oiyaa.merchant.ingenico.utils.function;

public interface BiFunction<T, U, R> {
    R apply(T var1, U var2);
}
