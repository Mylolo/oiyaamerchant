package com.oiyaa.merchant.ingenico.utils.function;

public interface Consumer<T> {
    void accept(T var1);
}
