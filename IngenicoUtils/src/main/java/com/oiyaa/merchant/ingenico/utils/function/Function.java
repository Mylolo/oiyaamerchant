package com.oiyaa.merchant.ingenico.utils.function;

public interface Function<T, R> {
    R apply(T var1);
}
