package com.oiyaa.merchant.ingenico.utils.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.oiyaa.merchant.ingenico.utils.R;
import com.oiyaa.merchant.ingenico.utils.db.CrudItem;
import com.oiyaa.merchant.ingenico.utils.db.CustomInputType;

/**
 * Override this class to create a custom CRUD (Create Read Update Delete) activity on an item
 * table. Constructor must provide the T Class object. You must also provide a proper implementation
 * of {@link CrudViewModel} to link the activity with a {@link com.ingenico.utils.db.CrudProvider}
 * instance.
 * @param <T>
 */
public abstract class CrudActivity<T> extends AppCompatActivity {

    private final Class<T> tClass;
    private CrudViewModel<T> viewModel;
    private CrudListFragment<T> crudListFragment;
    private CrudEditFragment<T> crudEditFragment;

    private CrudListAdapter<T> defaultAdapter;

    private ViewBinder<T> defaultViewBinder = new ViewBinder<T>() {
        @Override
        public View getLayout() {
            EditText editText = new EditText(getBaseContext());

            T item = createNew();
            if (item instanceof CustomInputType) {
                editText.setInputType(((CustomInputType) item).getInputType());
            }
            return editText;
        }

        @Override
        public void bindToView(View view, T item) {
            ((EditText) view).setText(item.toString());
        }

        @Override
        public void updateFromView(View view, T item) {
            if (item instanceof CrudItem) {
                ((CrudItem) item).setValue(((TextView) view).getText().toString());
            } else {
                throw new IllegalArgumentException(item.getClass().getSimpleName()
                        + " must implement the " + CrudItem.class.getSimpleName()
                        + " interface to use the default " + ViewBinder.class.getSimpleName()
                        + ". Implement it or use a custom " + ViewBinder.class.getSimpleName()
                        + " instance to avoid this exception.");
            }
        }

        @Override
        public T createNew() {
            try {
                return tClass.newInstance();
            } catch (InstantiationException | IllegalAccessException e) {
                throw new IllegalArgumentException(
                        tClass.toString() + " must have a default public constructor. Create it"
                                + " or use a custom " + ViewBinder.class.getSimpleName()
                                + " instance to avoid this exception.", e);
            }
        }
    };

    /**
     * Subclasses of CrudActivity must have a default constructor without parameter, which call
     * this one with the T Class.
     * @param tClass
     */
    protected CrudActivity(Class<T> tClass) {
        this.tClass = tClass;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crud);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        crudListFragment = new CrudListFragment<>();
        crudEditFragment = new CrudEditFragment<>();

        viewModel = new ViewModelProvider(this).get(getViewModelClass());

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.crud_content, crudListFragment);
        transaction.commit();
    }

    public CrudViewModel<T> getViewModel() {
        return viewModel;
    }

    /**
     * Override this method to use your own custom adapter for more complex views.
     * Default adapter use simple_list_item_1 from Android SDK, which is a simple TextView, and
     * write the result of {@link T#toString()} method into it.
     *
     * @return
     */
    public CrudListAdapter<T> getAdapter() {
        // Adapters can't be created before onCreate so instantiate it here
        if (defaultAdapter == null) {
            defaultAdapter =
                    new CrudListAdapter<T>(this, android.R.layout.simple_list_item_1) {
                        @Override
                        public RecyclerView.ViewHolder createViewHolder(View view) {
                            return new RecyclerView.ViewHolder(view) {
                            };
                        }

                        @Override
                        public void bindItem(RecyclerView.ViewHolder holder, T item) {
                            ((TextView) holder.itemView).setText(item.toString());
                        }
                    };
        }

        return defaultAdapter;
    }

    /**
     * Override this method to provide a custom ViewBinder implementation to display a more complex
     * creation/edition dialog and bind views with item properties.
     *
     * @return
     */
    public ViewBinder<T> getViewBinder() {
        return defaultViewBinder;
    }

    /**
     * Override this method to return your own {@link CrudViewModel} implementation.
     *
     * @return
     */
    public abstract Class<CrudViewModel> getViewModelClass();

    /**
     * Return the current item selected by the most recent long click.
     * @return
     */
    public T getCurrentItem() {
        return crudListFragment.getCurrentItem();
    }

    void showAddItemDialog() {
        crudEditFragment.show(getSupportFragmentManager());
    }

    void showEditItemDialog(T item) {
        crudEditFragment.show(getSupportFragmentManager(), item);
    }

    @CallSuper
    public void createItem(@NonNull T item) {
        viewModel.create(item);
    }

    @CallSuper
    public void updateItem(@NonNull T item) {
        viewModel.update(item);
    }

    @CallSuper
    public void deleteItem(@NonNull T item) {
        viewModel.delete(item);
    }
}
