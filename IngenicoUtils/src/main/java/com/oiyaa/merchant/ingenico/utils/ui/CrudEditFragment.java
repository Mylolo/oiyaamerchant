package com.oiyaa.merchant.ingenico.utils.ui;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

public class CrudEditFragment<T> extends DialogFragment {

    private static final String TAG = "CrudEditFragment";

    private CrudActivity<T> activity;
    private ViewBinder<T> viewBinder;
    private View content;
    private T item;

    public void show(FragmentManager manager) {
        show(manager, (T) null);
    }

    public void show(FragmentManager manager, T item) {
        this.item = item;
        show(manager, TAG);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof CrudActivity) {
            activity = (CrudActivity<T>) context;
            viewBinder = activity.getViewBinder();
        } else {
            throw new IllegalStateException("CrudListFragment can only be attached to CrudActivity");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        content = viewBinder.getLayout();
        if (item != null) {
            viewBinder.bindToView(content, item);
        }

        return new AlertDialog.Builder(getActivity())
                .setIcon(android.R.drawable.ic_menu_edit)
                .setTitle(item != null ? "Edit item" : "Add item")
                .setView(content)
                .setPositiveButton(
                        "Save",
                        (dialog, whichButton) -> {
                            if (item == null) {
                                item = viewBinder.createNew();
                                viewBinder.updateFromView(content, item);
                                activity.createItem(item);
                            } else {
                                viewBinder.updateFromView(content, item);
                                activity.updateItem(item);
                            }
                            dismiss();
                        }
                )
                .setNegativeButton(
                        "Cancel",
                        (dialog, whichButton) -> dismiss()
                )
                .create();
    }
}
