package com.oiyaa.merchant.ingenico.utils.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

/**
 * Implement this class to provide a custom adapter to the {@link RecyclerView} displayed in the
 * list fragment from the CRUD activity.
 * @param <T>
 */
public abstract class CrudListAdapter<T> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private LayoutInflater inflater;
    private List<T> itemList;
    private final int layoutId;
    private ItemSelectedListener<T> itemSelectedListener;

    public CrudListAdapter(Context context, int layoutId) {
        inflater = LayoutInflater.from(context);
        this.layoutId = layoutId;
    }

    public void setItemSelectedListener(ItemSelectedListener<T> itemSelectedListener) {
        this.itemSelectedListener = itemSelectedListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(layoutId, parent, false);

        return createViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (itemList != null) {
            final T item = itemList.get(position);
            bindItem(holder, item);

            holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (itemSelectedListener != null) {
                        itemSelectedListener.onItemSelected(item);
                    }
                    return false;
                }
            });
        }
    }

    @Override
    public void onViewRecycled(RecyclerView.ViewHolder holder) {
        holder.itemView.setOnLongClickListener(null);
        super.onViewRecycled(holder);
    }

    @Override
    public int getItemCount() {
        if (itemList != null) {
            return itemList.size();
        }
        return 0;
    }

    public abstract RecyclerView.ViewHolder createViewHolder(View view);
    public abstract void bindItem(RecyclerView.ViewHolder holder, T item);

    public void setItemList(List<T> itemList) {
        this.itemList = itemList;
        notifyDataSetChanged();
    }

    interface ItemSelectedListener<T> {
        void onItemSelected(T item);
    }
}
