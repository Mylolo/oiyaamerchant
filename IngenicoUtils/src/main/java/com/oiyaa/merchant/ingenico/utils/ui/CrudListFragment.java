package com.oiyaa.merchant.ingenico.utils.ui;

import android.content.Context;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.oiyaa.merchant.ingenico.utils.R;
import com.oiyaa.merchant.ingenico.utils.db.Editable;

/**
 * Main fragment from {@link CrudActivity}, to display the list of elements in the associated table.
 * This fragment is used internally by {@link CrudActivity} and should not be used with other
 * activities.
 * @param <T>
 */
public class CrudListFragment<T> extends Fragment implements
        CrudListAdapter.ItemSelectedListener<T> {
    private RecyclerView recyclerView;
    private FloatingActionButton actionButton;
    private CrudListAdapter<T> adapter;
    private CrudActivity<T> activity;
    private T currentItem = null;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof CrudActivity) {
            activity = (CrudActivity<T>) context;
        } else {
            throw new IllegalStateException(
                    "CrudListFragment can only be attached to CrudActivity");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_crudlist, container, false);

        recyclerView = root.findViewById(R.id.item_list);
        actionButton = root.findViewById(R.id.add_button);

        DividerItemDecoration dividerItemDecoration
                = new DividerItemDecoration(recyclerView.getContext(),
                DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(dividerItemDecoration);

        adapter = activity.getAdapter();
        adapter.setItemSelectedListener(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        registerForContextMenu(recyclerView);

        activity.getViewModel().readAll().observe(getViewLifecycleOwner(),
                ts -> adapter.setItemList(ts));

        actionButton.setOnClickListener(v -> activity.showAddItemDialog());

        return root;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if (checkEditable(currentItem)) {
            activity.getMenuInflater().inflate(R.menu.crud_item, menu);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_edit && checkEditable(currentItem)) {
            activity.showEditItemDialog(currentItem);
        } else if (item.getItemId() == R.id.menu_delete && checkEditable(currentItem)) {
            activity.deleteItem(currentItem);
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public void onItemSelected(T item) {
        currentItem = item;
    }

    T getCurrentItem() {
        return currentItem;
    }

    private boolean checkEditable(T item) {
        return item != null && (!(item instanceof Editable) || ((Editable) item).isEditable());
    }
}
