package com.oiyaa.merchant.ingenico.utils.ui;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.oiyaa.merchant.ingenico.utils.db.CrudProvider;

import java.util.List;

/**
 * A subclass from CrudViewModel with the proper T must be declared in order to be usable.
 * <p>
 * Like other implementations of {@link AndroidViewModel}, subclasses must have a constructor which
 * accepts {@link Application} as the only parameter.
 * <p>
 * Instances of this classes should be generated using {@link ViewModelProviders#of} instead of
 * being instantiated with new.
 * @param <T>
 */
public abstract class CrudViewModel<T> extends AndroidViewModel {
    private CrudProvider<T> provider;

    public CrudViewModel(@NonNull Application application) {
        super(application);
        this.provider = getProvider(application);
    }

    public LiveData<List<T>> readAll() {
        return provider.readAll();
    }

    void create(@NonNull T key) {
        provider.create(key);
    }

    void update(@NonNull T key) {
        provider.update(key);
    }

    void delete(@NonNull T key) {
        provider.delete(key);
    }

    /**
     * Override this method to provide the desired {@link CrudProvider} instance.
     * @return
     */
    protected abstract CrudProvider<T> getProvider(Context context);
}
