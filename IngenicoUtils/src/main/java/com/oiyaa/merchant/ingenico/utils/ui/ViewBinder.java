package com.oiyaa.merchant.ingenico.utils.ui;

import android.view.View;

/**
 * Implement this interface to use a custom layout for the create/edit dialog.
 * @param <T>
 */
public interface ViewBinder<T> {
    /**
     * This method should generate the view to use as content for the dialog.
     * @return
     */
    View getLayout();

    /**
     * This method must update the view content with values from the T item.
     * @param view
     * @param item
     */
    void bindToView(View view, T item);

    /**
     * This method must update the T item based on the content of the view.
     * @param view
     * @param item
     */
    void updateFromView(View view, T item);

    /**
     * This method should create a new instance of T. Most of the time it will just call the
     * default constructor.
     * @return
     */
    T createNew();
}
