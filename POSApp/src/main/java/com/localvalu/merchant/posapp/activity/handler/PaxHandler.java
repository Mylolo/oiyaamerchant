package com.localvalu.merchant.posapp.activity.handler;

import android.os.Bundle;

import com.localvalu.coremerchantapp.data.output.order.OrderDetails;
import com.localvalu.merchant.posapp.fragment.orders.OrderAtTableDetailsPaxFragment;
import com.localvalu.merchant.posapp.fragment.orders.OrderDetailsPaxFragment;
import com.localvalu.merchant.posapp.fragment.pax.PaxTransactionsDetailsFragment;
import com.localvalu.merchant.posapp.fragment.pax.PaxTransactionsFragment;
import com.localvalu.merchant.posapp.fragment.pax.PrintFragment;

public interface PaxHandler
{
    OrderDetailsPaxFragment orderDetailsPaxFragment(String orderId);

    OrderAtTableDetailsPaxFragment orderAtTableDetailsPaxFragment(String orderId);

    PrintFragment printFragment(OrderDetails orderDetails);

    PaxTransactionsFragment paxTransactionFragment(Bundle bundle);

    PaxTransactionsDetailsFragment paxTransactionDetailsFragment(Bundle bundle);

}
