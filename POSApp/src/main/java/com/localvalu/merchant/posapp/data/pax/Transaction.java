package com.localvalu.merchant.posapp.data.pax;


public class Transaction
{
    private int mTransDrawable;

    private String mTransName;

    public Transaction(int mTransDrawable, String mTransName) {
        this.mTransDrawable = mTransDrawable;
        this.mTransName = mTransName;
    }

    public int getTransDrawable()
    {
        return mTransDrawable;
    }

    public void setTransDrawable(int mTransDrawable)
    {
        this.mTransDrawable = mTransDrawable;
    }

    public String getTransName()
    {
        return mTransName;
    }

    public void setTransName(String mTransName)
    {
        this.mTransName = mTransName;
    }
}
