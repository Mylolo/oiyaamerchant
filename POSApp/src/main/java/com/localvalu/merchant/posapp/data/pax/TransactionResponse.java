package com.localvalu.merchant.posapp.data.pax;

import android.os.Parcel;
import android.os.Parcelable;

import lombok.Getter;

public class TransactionResponse implements Parcelable
{

    private String transResponseName;
    private String transResponseValue;


    protected TransactionResponse(Parcel in)
    {
        transResponseName = in.readString();
        transResponseValue = in.readString();
    }

    public String getTransResponseName()
    {
        return transResponseName;
    }

    public void setTransResponseName(String transResponseName)
    {
        this.transResponseName = transResponseName;
    }

    public String getTransResponseValue()
    {
        return transResponseValue;
    }

    public void setTransResponseValue(String transResponseValue)
    {
        this.transResponseValue = transResponseValue;
    }

    public static final Creator<TransactionResponse> CREATOR = new Creator<TransactionResponse>()
    {
        @Override
        public TransactionResponse createFromParcel(Parcel in)
        {
            return new TransactionResponse(in);
        }

        @Override
        public TransactionResponse[] newArray(int size)
        {
            return new TransactionResponse[size];
        }
    };

    public TransactionResponse(String transResponseName, String transResponseValue)
    {
        this.transResponseName = transResponseName;
        this.transResponseValue = transResponseValue;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(transResponseName);
        dest.writeString(transResponseValue);
    }
}
