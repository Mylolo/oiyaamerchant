package com.localvalu.merchant.posapp.fragment.adapter.pax;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;


import com.localvalu.coremerchantapp.utils.AppUtils;
import com.localvalu.merchant.posapp.fragment.pax.PaxTransactionResponseFragment;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class TransactionDetailAdapter extends FragmentStateAdapter
{
    private Bundle bundle;
    private String[] titles;

    public TransactionDetailAdapter(FragmentManager fm, Lifecycle lifecycle, Bundle bundle, String[] titles)
    {
        super(fm,lifecycle);
        this.bundle = bundle;
        this.titles =titles;
    }

    @Override
    public int getItemCount()
    {
        return 2;
    }

    private Fragment getPaxTransactionResponseFragment(int position)
    {
        Bundle bundleTemp = new Bundle();
        switch (position)
        {
            case 0:
                 bundleTemp.putParcelableArrayList(AppUtils.BUNDLE_RESPONSE_LIST_COMMON,
                         bundle.getParcelableArrayList(AppUtils.BUNDLE_TRANSACTION_RESPONSE_LIST));
                 break;
            case 1:
                 bundleTemp.putParcelableArrayList(AppUtils.BUNDLE_RESPONSE_LIST_COMMON,
                        bundle.getParcelableArrayList(AppUtils.BUNDLE_TRANSACTION_STATUS_RESPONSE_LIST));
                 break;
        }
        PaxTransactionResponseFragment fragment = new PaxTransactionResponseFragment();
        fragment.setArguments(bundleTemp);
        return fragment;
    }

    @NonNull
    @NotNull
    @Override
    public Fragment createFragment(int position)
    {
        return getPaxTransactionResponseFragment(position);
    }
}
