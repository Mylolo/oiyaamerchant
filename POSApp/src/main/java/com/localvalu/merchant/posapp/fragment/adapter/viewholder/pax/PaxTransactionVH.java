package com.localvalu.merchant.posapp.fragment.pax.viewholder.pax;

import android.content.Context;
import android.view.View;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.localvalu.merchant.posapp.R;
import com.localvalu.merchant.posapp.data.pax.Transaction;
import com.utils.view.rippleview.CustomRippleView;
import com.utils.view.textview.TextView;


public class PaxTransactionVH extends RecyclerView.ViewHolder implements View.OnClickListener
{
    private static final String TAG = PaxTransactionVH.class.getSimpleName();
    private Context context;
    private AppCompatTextView tvTransactionType;
    private AppCompatImageView ivTransactionType;

    public PaxTransactionVH(Context context, View view)
    {
        super(view);
        this.context = context;

        ivTransactionType = view.findViewById(R.id.ivTransactionType);
        tvTransactionType = view.findViewById(R.id.tvTransactionType);
    }

    @Override
    public void onClick(View view)
    {
        int id = view.getId();

        switch (id)
        {

        }
    }

    public void populateData(Transaction transaction)
    {
        ivTransactionType.setImageResource(transaction.getTransDrawable());
        tvTransactionType.setText(transaction.getTransName());
    }
}
