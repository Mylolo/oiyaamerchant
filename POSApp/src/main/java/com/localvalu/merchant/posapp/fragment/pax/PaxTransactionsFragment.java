package com.localvalu.merchant.posapp.fragment.pax;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.eft.libpositive.PosIntegrate;
import com.localvalu.coremerchantapp.LocalValuFragment;
import com.localvalu.coremerchantapp.handler.DashBoardHandler;
import com.localvalu.coremerchantapp.utils.AppUtils;
import com.localvalu.merchant.posapp.R;
import com.localvalu.merchant.posapp.activity.handler.PaxHandler;
import com.localvalu.merchant.posapp.components.LocalPOSReceiver;
import com.localvalu.merchant.posapp.components.PosPaymentReceiver;
import com.localvalu.merchant.posapp.data.pax.Transaction;
import com.localvalu.merchant.posapp.fragment.adapter.pax.PaxTransactionAdapter;
import com.localvalu.merchant.posapp.listeners.PaxTransactionItemClickListener;
import com.localvalu.merchant.posapp.utils.PaxConstants;
import com.utils.helper.recyclerview.RVItemAnimator;
import com.utils.validator.Validator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.eft.libpositive.PosIntegrate.CONFIG_TYPE.CT_AMOUNT;
import static com.eft.libpositive.PosIntegrate.CONFIG_TYPE.CT_CANCELLED_TIMEOUT;
import static com.eft.libpositive.PosIntegrate.CONFIG_TYPE.CT_HISTORYREPORT;
import static com.eft.libpositive.PosIntegrate.CONFIG_TYPE.CT_RRN;
import static com.eft.libpositive.PosIntegrate.CONFIG_TYPE.CT_UTI;
import static com.eft.libpositive.PosIntegrate.CONFIG_TYPE.CT_XREPORT;
import static com.eft.libpositive.PosIntegrate.CONFIG_TYPE.CT_ZREPORT;
import static com.eft.libpositive.PosIntegrate.TRANSACTION_TYPE.TRANSACTION_TYPE_COMPLETION;
import static com.eft.libpositive.PosIntegrate.TRANSACTION_TYPE.TRANSACTION_TYPE_PREAUTH;
import static com.eft.libpositive.PosIntegrate.TRANSACTION_TYPE.TRANSACTION_TYPE_RECONCILIATION;
import static com.eft.libpositive.PosIntegrate.TRANSACTION_TYPE.TRANSACTION_TYPE_REFUND;
import static com.eft.libpositive.PosIntegrate.TRANSACTION_TYPE.TRANSACTION_TYPE_SALE;

public class PaxTransactionsFragment extends LocalValuFragment implements PaxTransactionItemClickListener
{

    private static final String TAG = PaxTransactionsFragment.class.getSimpleName();

    private RecyclerView rvPAXList;
    private AppCompatTextView tvLblAmount,tvAmount,tvEmpty;
    private PaxTransactionAdapter paxTransactionAdapter;
    private List<Transaction> transactionList;
    private String payableAmount,cashBackAmount;
    private String displayPayableAmount,displayCashbackAmount,lastReceivedUTI;
    private DashBoardHandler dashBoardHandler;
    private PaxHandler paxHandler;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();

        if(bundle!=null)
        {
            payableAmount = bundle.getString(AppUtils.BUNDLE_PAYABLE_AMOUNT);
            cashBackAmount = bundle.getString(AppUtils.BUNDLE_CASHBACK_AMOUNT);
            displayPayableAmount = bundle.getString(AppUtils.BUNDLE_DISPLAY_PAYABLE_AMOUNT);
            Log.d(TAG,"onCreate : " + bundle.getString(AppUtils.BUNDLE_DISPLAY_PAYABLE_AMOUNT));
            displayCashbackAmount = bundle.getString(AppUtils.BUNDLE_DISPLAY_CASHBACK_AMOUNT);
            lastReceivedUTI = bundle.getString(AppUtils.BUNDLE_LAST_RECEIVED_UTI);
        }
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(posUIReceiver,
                new IntentFilter(AppUtils.INTENT_FILTER_PAX_TRANSACTION_RECEIVER));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_pax_transactions, container, false);
        setHasOptionsMenu(true);
        //setRetainInstance(true);

        tvEmpty = view.findViewById(R.id.tvEmpty);
        tvAmount = view.findViewById(R.id.tvAmount);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rvPAXList = view.findViewById(R.id.rvPAXList);
        rvPAXList.setLayoutManager(linearLayoutManager);
        rvPAXList.setItemAnimator(new RVItemAnimator());
        rvPAXList.addItemDecoration(new DividerItemDecoration(getContext(), linearLayoutManager.getOrientation()));
        rvPAXList.setHasFixedSize(true);

        if (Validator.isValid(paxTransactionAdapter))
            rvPAXList.setAdapter(paxTransactionAdapter);

        doGetPaxTransactionList();

        setAmount();

        return view;
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);

        dashBoardHandler = (DashBoardHandler) context;

        paxHandler  = (PaxHandler) context;
    }

    @Override
    public void onDetach()
    {
        dashBoardHandler = null;
        paxHandler = null;
        super.onDetach();
    }

    private void setAmount()
    {
        tvAmount.setText(displayPayableAmount);
    }

    private void doGetPaxTransactionList()
    {
        setPaxTransactionAdapter();
        showListView();
    }

    private void showEmptyView(String message)
    {
        tvEmpty.setText(message);
        rvPAXList.setVisibility(View.GONE);
        tvEmpty.setVisibility(View.VISIBLE);
    }

    private void showListView()
    {
        rvPAXList.setVisibility(View.VISIBLE);
        tvEmpty.setVisibility(View.GONE);
    }

    private void setPaxTransactionAdapter()
    {
        populatePaxTransactionList();
        paxTransactionAdapter = new PaxTransactionAdapter(transactionList);
        paxTransactionAdapter.setPaxTransactionItemClickListener(this);
        rvPAXList.setAdapter(paxTransactionAdapter);
    }

    private void populatePaxTransactionList() {
        transactionList = new ArrayList<>();
        transactionList.add(new Transaction(R.drawable.msale,getResources().getString(R.string.run_sale)));
        transactionList.add(new Transaction(R.drawable.mrefund,getResources().getString(R.string.run_refund)));
        transactionList.add(new Transaction(R.drawable.mreversal,getResources().getString(R.string.reverse_last)));
        transactionList.add(new Transaction(R.drawable.mreversal,getResources().getString(R.string.reverse_uti)));
        transactionList.add(new Transaction(R.drawable.mpreauth,getResources().getString(R.string.preauth)));
        transactionList.add(new Transaction(R.drawable.mcompletion,getResources().getString(R.string.completion)));
        transactionList.add(new Transaction(R.drawable.mquerytrans,getResources().getString(R.string.query_transaction)));
        transactionList.add(new Transaction(R.drawable.mcancel,getResources().getString(R.string.cancel_trans)));
        transactionList.add(new Transaction(R.drawable.mdailybatch,getResources().getString(R.string.x_report)));
        transactionList.add(new Transaction(R.drawable.mreconciliation,getResources().getString(R.string.z_report)));
        transactionList.add(new Transaction(R.drawable.mhistoryreport,getResources().getString(R.string.history_report)));
        transactionList.add(new Transaction(R.drawable.mexit,getResources().getString(R.string.exit)));
    }

    @Override
    public void onPaxTransactionItemClicked(int paxTransactionOption)
    {
        PosPaymentReceiver.statusEventsList.clear();

        HashMap<PosIntegrate.CONFIG_TYPE, String> args = new HashMap<PosIntegrate.CONFIG_TYPE, String>();
        args.put(CT_AMOUNT, payableAmount);
        
        switch (paxTransactionOption)
        {
            case PaxConstants.SALE:
                 PosIntegrate.executeTransaction(getActivity(), TRANSACTION_TYPE_SALE, args);
                 break;
            case PaxConstants.REFUND:
                 PosIntegrate.executeTransaction(getActivity(), TRANSACTION_TYPE_REFUND, args);
                 break;
            case PaxConstants.REVERSE_LAST:
                 PosIntegrate.executeReversal(getActivity(), args);
                 break;
            case PaxConstants.REVERSE_BY_UTI:
                 if(lastReceivedUTI!=null)
                 {
                     if(lastReceivedUTI.length()>0)
                     {
                         args.put(CT_UTI, lastReceivedUTI);
                         PosIntegrate.executeReversal(getActivity(), args);
                     }
                     else
                     {
                         Log.i(TAG, "NO UTI TO Run Reversal on");
                         Toast.makeText(getActivity(), "No UTI TO Run Reversal on", Toast.LENGTH_SHORT).show();
                     }
                 }
                 break;
            case PaxConstants.PREAUTH:
                 PosIntegrate.executeTransaction(getActivity(), TRANSACTION_TYPE_PREAUTH, args);
                 break;
            case PaxConstants.COMPLETION:
                 args.put(CT_RRN, "RRN");
                 PosIntegrate.executeTransaction(getActivity(), TRANSACTION_TYPE_COMPLETION, args);
                 break;
            case PaxConstants.QUERY_TRANSACTION:
                 if(lastReceivedUTI!=null)
                 {
                    if (lastReceivedUTI.length() > 0)
                    {
                        Log.i(TAG, "Run Query TransRec on:" + lastReceivedUTI);
                        args.put(CT_UTI, lastReceivedUTI);
                        PosIntegrate.queryTransaction(getActivity(), args);
                    }
                    else
                    {
                        Log.i(TAG, "NO UTI TO Run Query on");
                        Toast.makeText(getActivity(), "No UTI TO Run Query on", Toast.LENGTH_SHORT).show();
                    }
                 }
                 break;
            case PaxConstants.CANCEL_TRANSACTION:
                 args.put(CT_CANCELLED_TIMEOUT, "30");
                 PosIntegrate.executeTransaction(getActivity(), TRANSACTION_TYPE_SALE, args);
                 break;
            case PaxConstants.PRINT_X:
                 args.put(CT_XREPORT, "true");
                 PosIntegrate.executeReport(getActivity(), TRANSACTION_TYPE_RECONCILIATION, args);
                 break;
            case PaxConstants.PRINT_Z:
                 args.put(CT_ZREPORT, "true");
                 PosIntegrate.executeReport(getActivity(),TRANSACTION_TYPE_RECONCILIATION, args);
                 break;
            case PaxConstants.PRINT_HISTORY:
                 args.put(CT_HISTORYREPORT, "true");
                 PosIntegrate.executeReport(getActivity(), TRANSACTION_TYPE_RECONCILIATION, args);
                 break;
            case PaxConstants.EXIT:
                 getActivity().getSupportFragmentManager().popBackStackImmediate();
                 break;
        }
    }

    private BroadcastReceiver posUIReceiver = new LocalPOSReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            super.onReceive(context, intent);
            if(intent.getExtras()!=null)
            {
                Bundle bundle = intent.getExtras();

                if(bundle!=null)
                {
                    int errorCode = bundle.getInt(AppUtils.BUNDLE_TRANSACTION_ERROR_CODE);
                    switch (errorCode)
                    {
                        case 0:
                            //Do Nothing
                            break;
                        case 2:
                            // Show Details
                            paxHandler.paxTransactionDetailsFragment(bundle);
                            break;
                        case -1:
                            //Show Error
                            break;
                    }
                /*payableAmount = bundle.getString(AppUtils.BUNDLE_PAYABLE_AMOUNT);
                cashBackAmount = bundle.getString(AppUtils.BUNDLE_CASHBACK_AMOUNT);
                displayPayableAmount = bundle.getString(AppUtils.BUNDLE_DISPLAY_PAYABLE_AMOUNT);
                displayCashbackAmount = bundle.getString(AppUtils.BUNDLE_DISPLAY_CASHBACK_AMOUNT);
                lastReceivedUTI = bundle.getString(AppUtils.BUNDLE_LAST_RECEIVED_UTI);*/
                }
            }
        }
    };

    @Override
    public void onDestroy()
    {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(posUIReceiver);
        dashBoardHandler.setPreviousTitle();
        super.onDestroy();
    }
}
