package com.localvalu.merchant.posapp.fragment.pax.viewholder.pax;

import android.content.Context;
import android.view.View;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.localvalu.merchant.posapp.R;
import com.localvalu.merchant.posapp.data.pax.TransactionResponse;
import com.utils.view.textview.TextView;


public class PaxTransactionResponseVH extends RecyclerView.ViewHolder implements View.OnClickListener
{

    private static final String TAG = PaxTransactionResponseVH.class.getSimpleName();

    private Context context;
    private AppCompatTextView tvTransResponseName,tvTransResponseValue;


    public PaxTransactionResponseVH(Context context, View view)
    {
        super(view);
        this.context = context;

        tvTransResponseName = view.findViewById(R.id.tvTransResponseName);
        tvTransResponseValue = view.findViewById(R.id.tvTransResponseValue);
    }

    @Override
    public void onClick(View view)
    {
        int id = view.getId();

        switch (id)
        {

        }
    }

    public void populateData(TransactionResponse transactionResponse)
    {
        tvTransResponseName.setText(transactionResponse.getTransResponseName());
        tvTransResponseValue.setText(transactionResponse.getTransResponseValue());
    }

}
