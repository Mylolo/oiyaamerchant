package com.localvalu.merchant.posapp.listeners;

public interface PaxTransactionItemClickListener
{
    void onPaxTransactionItemClicked(int paxTransactionOption);
}
