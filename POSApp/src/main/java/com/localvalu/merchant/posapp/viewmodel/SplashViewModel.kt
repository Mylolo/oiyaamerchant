package com.localvalu.merchant.posapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.localvalu.coremerchantapp.data.input.AppVersionCheckInput
import com.localvalu.coremerchantapp.data.output.appversioncheck.AppVersionOutput
import com.localvalu.coremerchantapp.generic.Resource
import com.localvalu.coremerchantapp.repository.AppVersionCheckRepository

class SplashViewModel:ViewModel()
{
    private val _appVersionCheckInput:MutableLiveData<AppVersionCheckInput> = MutableLiveData()

    val appVersionCheckOutput: LiveData<Resource<AppVersionOutput>> = Transformations.switchMap(_appVersionCheckInput)
    {
        AppVersionCheckRepository.getAppVersion(it)
    }

    fun setAppVersionCheckInput(appVersionCheckInput:AppVersionCheckInput)
    {
        _appVersionCheckInput.value = appVersionCheckInput
    }
}