package com.localvalu.merchant;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.localvalu.coremerchantapp.LocalValuApplication;
import com.localvalu.merchant.base.BaseEnum;
import com.rt.printerlibrary.printer.RTPrinter;
import com.utils.UtilFactory;
import com.utils.helper.preference.PreferenceFactory;

import dagger.hilt.android.AndroidEntryPoint;
import dagger.hilt.android.HiltAndroidApp;

@HiltAndroidApp
public class OiyaaApplication extends Application
{
    private static final String SP_KEY_LABEL_SIZE = "";
    public static OiyaaApplication instance = null;
    private RTPrinter rtPrinter;
    private static OiyaaApplication oiyaaApplication;

    @BaseEnum.CmdType
    private int currentCmdType = BaseEnum.CMD_PIN;//默认为针打

    @BaseEnum.ConnectType
    private int currentConnectType = BaseEnum.NONE;//默认未连接

    public static final String SP_NAME_SETTING = "setting";

    public static String labelSizeStr = "80*40", labelWidth = "80", labelHeight = "40", labelSpeed = "2", labelType = "CPCL", labelOffset = "0";

    public OiyaaApplication()
    {

    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        oiyaaApplication = this;
        UtilFactory.init(this, this.getResources().getString(com.localvalu.coremerchantapp.R.string.app_name));
        String strLabelSize = PreferenceFactory.getInstance().getString(SP_KEY_LABEL_SIZE);
        if(strLabelSize==null || strLabelSize.isEmpty())
        {
            strLabelSize =  getResources().getStringArray(R.array.label_setting_size)[0];
        }
        String[] temp = strLabelSize.split("\\*");
        labelWidth = temp[0];
        labelHeight = temp[1];
        instance = this;
    }

    public static OiyaaApplication getInstance()
    {
        return instance;
    }


    public RTPrinter getRtPrinter()
    {
        return rtPrinter;
    }

    public void setRtPrinter(RTPrinter rtPrinter)
    {
        this.rtPrinter = rtPrinter;
    }

    @BaseEnum.CmdType
    public int getCurrentCmdType()
    {
        return currentCmdType;
    }

    public void setCurrentCmdType(@BaseEnum.CmdType int currentCmdType)
    {
        this.currentCmdType = currentCmdType;
    }

    @BaseEnum.ConnectType
    public int getCurrentConnectType()
    {
        return currentConnectType;
    }

    public void setCurrentConnectType(@BaseEnum.ConnectType int currentConnectType)
    {
        this.currentConnectType = currentConnectType;
    }
}
