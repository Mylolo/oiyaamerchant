package com.localvalu.merchant.activity

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.WindowManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.localvalu.coremerchantapp.LocalValuActivity
import com.localvalu.coremerchantapp.data.input.AppVersionCheckInput
import com.localvalu.coremerchantapp.dialog.AlertDialog
import com.localvalu.coremerchantapp.generic.Resource
import com.localvalu.merchant.ActivityConfig
import com.localvalu.merchant.BuildConfig
import com.localvalu.merchant.R
import com.localvalu.merchant.viewmodel.SplashViewModel
import com.utils.base.Task
import com.utils.data.DangerousPermission
import com.utils.helper.permission.PermissionHandler
import com.utils.util.Util
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SplashActivity : LocalValuActivity(), View.OnClickListener
{
    val TAG = "SplashActivity"
    var model: SplashViewModel? = null;
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        Log.d(TAG,"SplashActivity")
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_splash)
        Util.setStatusBarColor(this, R.color.white)
        init()
    }

    override fun init()
    {
        Log.d(TAG,"init")
        checkPermissionAccess(
            Util.asList(
                DangerousPermission.CAMERA,
                DangerousPermission.WRITE_EXTERNAL_STORAGE
            )
        )
    }

    override fun onClick(view: View)
    {
        val id = view.id
    }

    private fun checkPermissionAccess(dangerousPermissions: List<DangerousPermission>)
    {
        permissionFactory.requestPermission(this, dangerousPermissions, object : PermissionHandler
        {
            override fun result(
                granted: List<DangerousPermission>,
                denied: List<DangerousPermission>
            )
            {
                model = ViewModelProvider(this@SplashActivity).get(SplashViewModel::class.java)
                checkAppUpdate()
            }

            override fun permissionRationale(): Boolean
            {
                return false
            }

            override fun permissionRationaleFor(dangerousPermissions: List<DangerousPermission>)
            {
                checkPermissionAccess(dangerousPermissions)
            }

            override fun info(message: String)
            {
                processApp()
            }
        })
    }

    private fun processApp()
    {
        Util.makeDelay(1500, object : Task<Boolean?>
        {
            override fun onSuccess(result: Boolean?)
            {
                ActivityConfig.startDashBoardActivity(this@SplashActivity)
            }

            override fun onError(message: String?)
            {
            }
        })
    }

    private fun checkAppUpdate()
    {
        model?.let {
            observeSubscribers()
            it.setAppVersionCheckInput(AppVersionCheckInput())
        }

    }

    private fun observeSubscribers()
    {
        model?.let {
            it.appVersionCheckOutput.observe(this, Observer { result->
                when(result)
                {
                    is Resource.Loading->
                    {

                    }
                    is Resource.Success->
                    {
                        if(result?.data?.appVersionCheckResult?.appVersionResult?.errorDetail.errorCode==0)
                        {
                            val versionCode = result?.data?.appVersionCheckResult?.appVersionResult?.versioncode?.toInt()
                            if(versionCode>BuildConfig.VERSION_CODE)
                            {
                                showMandateUpdate()
                            }
                            else processApp()
                        }
                        else processApp()
                    }
                    is Resource.Failure->
                    {
                        processApp()
                    }
                }
            })
        }
    }

    private fun showMandateUpdate()
    {
        AlertDialog.newInstance(this.applicationContext,
            getString(R.string.dialog_title),
            getString(R.string.lbl_mandate_update_message),getString(R.string.lbl_ok),
            "",object:AlertDialog.AlertDialogClickListener{
                override fun alertDialogPositiveButtonClicked()
                {
                    redirectToPlayStore()
                }

                override fun alertDialogNegativeButtonClicked()
                {
                    processApp()
                }

                override fun alertDialogOutsideClicked()
                {
                    processApp()
                }
            })

    }

    private fun redirectToPlayStore()
    {
        val appPackageName = packageName // package name of the app
        try
        {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW, Uri.parse(
                        "market://details?id=$appPackageName"
                    )
                )
            )
        } catch (anfe: ActivityNotFoundException)
        {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW, Uri.parse(
                        "https://play.google.com/store/apps/details?id=$appPackageName"
                    )
                )
            )
        }
    }

    companion object
    {
        private val TAG = SplashActivity::class.java.simpleName
    }
}