package com.localvalu.merchant.adapter;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.localvalu.coremerchantapp.data.output.order.OrderListData;
import com.localvalu.coremerchantapp.fragment.adapter.viewholder.orders.OrderAtTableListVH;
import com.localvalu.coremerchantapp.generated.callback.OnClickListener;
import com.localvalu.merchant.R;

import java.util.List;

/**
 * Created by Administrator on 2015/6/9.
 */
public class BluetoothDeviceAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{

    private Context context;
    private List<BluetoothDevice> mList;
    private BluetoothItemClickListener bluetoothItemClickListener;
    private boolean isPairedDeviceList;

    public BluetoothDeviceAdapter(List<BluetoothDevice> list)
    {
        this.mList = list;
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    public void setBluetoothItemClickListener(BluetoothItemClickListener bluetoothItemClickListener)
    {
        this.bluetoothItemClickListener = bluetoothItemClickListener;
    }

    class BluetoothDeviceViewHolder extends RecyclerView.ViewHolder
    {
        TextView tvBluetoothDevice;

        public BluetoothDeviceViewHolder(@NonNull View itemView)
        {
            super(itemView);
            tvBluetoothDevice = itemView.findViewById(R.id.tvBluetoothDevice);
        }
    }

    @Override
    public int getItemViewType(int position)
    {
        BluetoothDevice bluetoothDevice = mList.get(position);
        return 0;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        this.context = parent.getContext();
        if(parent.getId() == R.id.rvPairedDevices)
        {
            isPairedDeviceList = true;
        }
        else
        {
            isPairedDeviceList = false;
        }
        switch (viewType)
        {
            case 0:
            {
                View view = LayoutInflater.from(context).inflate(R.layout.view_item_bluetooth_device, parent, false);
                return new BluetoothDeviceViewHolder(view);
            }

            default:
            {
                return null;
            }
        }
    }

    @Override
    public int getItemCount()
    {
        return this.mList.size();
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position)
    {
        if(holder instanceof BluetoothDeviceViewHolder)
        {
            BluetoothDevice bluetoothDevice = mList.get(holder.getBindingAdapterPosition());

            if (TextUtils.isEmpty(bluetoothDevice.getName()))
            {
                ((BluetoothDeviceViewHolder) holder).tvBluetoothDevice.setText(bluetoothDevice.getAddress());
            }
            else
            {
                ((BluetoothDeviceViewHolder) holder).tvBluetoothDevice.setText(mList.get(position).getName() + " [" + bluetoothDevice.getAddress() + "]");
            }
            holder.itemView.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    if(bluetoothItemClickListener!=null)
                    {
                        if(isPairedDeviceList)
                        {
                            bluetoothItemClickListener.onPairedDeviceClicked(bluetoothDevice,holder.getBindingAdapterPosition());
                        }
                        else
                        {
                             bluetoothItemClickListener.onSearchedDeviceClicked(bluetoothDevice,holder.getBindingAdapterPosition());
                        }
                    }
                }
            });
        }
    }

    public interface BluetoothItemClickListener
    {
        void onPairedDeviceClicked(BluetoothDevice bluetoothDevice,int position);
        void onSearchedDeviceClicked(BluetoothDevice bluetoothDevice,int position);
    }

}
