package com.localvalu.merchant.components

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import javax.inject.Inject
import com.localvalu.coremerchantapp.storage.UserPreferences
import com.google.firebase.messaging.RemoteMessage
import com.localvalu.coremerchantapp.LocalValuConstants
import com.localvalu.coremerchantapp.base.CommonRepository
import com.localvalu.coremerchantapp.components.OiyaaPlayer
import com.localvalu.merchant.activity.DashBoardActivity
import com.utils.helper.preference.PreferenceFactory
import kotlinx.coroutines.*
import org.json.JSONException
import org.json.JSONObject
import java.lang.Exception

/**
 * NOTE: There can only be one service in each app that receives FCM messages. If multiple
 * are declared in the Manifest then the first one will be chosen.
 *
 *
 * In order to make this Java sample functional, you must remove the following from the Kotlin messaging
 * service in the AndroidManifest.xml:
 *
 *
 * <intent-filter>
 * <action android:name="com.google.firebase.MESSAGING_EVENT"></action>
</intent-filter> *
 */
class MyFirebaseMessagingService : FirebaseMessagingService(), LocalValuConstants
{
    private var notificationUtils: NotificationUtils? = null

    @Inject
    private val userPreferences: UserPreferences? = null

    private val scope = CoroutineScope(newSingleThreadContext("name"))

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    override fun onMessageReceived(remoteMessage: RemoteMessage)
    {
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.from)

        // Check if message contains a notification payload.
        if (remoteMessage.notification != null)
        {
            Log.e(TAG, "Notification Body: " + remoteMessage.notification!!.body)
            //handleNotification(remoteMessage.getNotification().getBody());
        }

        // Check if message contains a data payload.
        if (remoteMessage.data.isNotEmpty())
        {
            Log.d(TAG, "Data Payload: " + remoteMessage.data.toString())
            try
            {
                val json = JSONObject(remoteMessage.data as Map<*, *>)
                handleDataMessage(json)
            }
            catch (e: Exception)
            {
                Log.e(TAG, "Exception: " + e.message)
            }
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
    // [END receive_message]
    // [START on_new_token]
    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    override fun onNewToken(token: String)
    {
        Log.d(TAG, "Refreshed token: $token")
        PreferenceFactory.getInstance().setString(LocalValuConstants.PREF_FCM_ID, token)
        scope.launch {
            userPreferences?.saveFCMObject(token)
        }
        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(token)
    }
    // [END on_new_token]


    /**
     * Persist token to third-party servers.
     *
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private fun sendRegistrationToServer(token: String)
    {
        // TODO: Implement this method to send token to your app server.
        Log.e(TAG, "sendRegistrationToServer: $token")
    }

    @Throws(JSONException::class)
    private fun handleDataMessage(json: JSONObject)
    {
        Log.e(TAG, "push json: $json")
        try
        {
            val title = json.optString("title")
            val message = json.optString("body")
            val imageUrl = json.optString("image")
            val timestamp = json.optString("timestamp")
            Log.e(TAG, "title: $title")
            Log.e(TAG, "message: $message")
            Log.e(TAG, "imageUrl: $imageUrl")
            Log.e(TAG, "timestamp: $timestamp")
            val newBundle = Bundle()
            try
            {
            }
            catch (e: Exception)
            {
            }
            if (!NotificationUtils.isAppIsInBackground(applicationContext)
            )
            {
                println("app is here")
                val resultIntent = Intent(applicationContext, DashBoardActivity::class.java)
                resultIntent.putExtra("message", message)
                resultIntent.putExtras(newBundle)
                showNotificationMessage(applicationContext, title, message, timestamp, resultIntent)
            }
            else
            {
                // app is in background, show the notification in notification tray
                val resultIntent = Intent(applicationContext, DashBoardActivity::class.java)
                resultIntent.putExtra("message", message)
                resultIntent.putExtras(newBundle)

                // check for image attachment
                if (TextUtils.isEmpty(imageUrl))
                {
                    showNotificationMessage(
                        applicationContext,
                        title,
                        message,
                        timestamp,
                        resultIntent
                    )
                } else
                {
                    // image is present, show notification with image
                    showNotificationMessageWithBigImage(
                        applicationContext,
                        title,
                        message,
                        timestamp,
                        resultIntent,
                        imageUrl
                    )
                }
            }
        } catch (e: Exception)
        {
            Log.e(TAG, "Exception: " + e.message)
        }
    }

    /**
     * Showing notification with text only
     */
    private fun showNotificationMessage(
        context: Context,
        title: String,
        message: String,
        timeStamp: String,
        intent: Intent
    )
    {
        println("showNotificationMessage")
        notificationUtils = NotificationUtils(context)
        intent.flags =
            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        notificationUtils!!.showNotificationMessage(title, message, timeStamp, intent)
        OiyaaPlayer.getInstance(applicationContext).also {
            try
            {
                it.prepareAsync()
                it.setOnPreparedListener { mediaPlayer->
                    mediaPlayer.start()
                }
            }
            catch (ex:Exception)
            {
                ex.printStackTrace()
            }
        }
    }

    /**
     * Showing notification with text and image
     */
    private fun showNotificationMessageWithBigImage(
        context: Context,
        title: String,
        message: String,
        timeStamp: String,
        intent: Intent,
        imageUrl: String
    )
    {
        notificationUtils = NotificationUtils(context)
        intent.flags =
            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        notificationUtils!!.showNotificationMessage(title, message, timeStamp, intent, imageUrl)
    }

    companion object
    {
        private const val TAG = "MyFirebaseMsgService"
    }
}