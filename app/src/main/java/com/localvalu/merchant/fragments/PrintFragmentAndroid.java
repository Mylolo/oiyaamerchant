package com.localvalu.merchant.fragments;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.localvalu.coremerchantapp.LocalValuFragment;
import com.localvalu.coremerchantapp.R;
import com.localvalu.coremerchantapp.data.output.FoodItem;
import com.localvalu.coremerchantapp.data.output.order.OrderDetails;;
import com.localvalu.coremerchantapp.fragment.adapter.order.OrderedFoodItemsPrintAdapter;
import com.localvalu.coremerchantapp.handler.DashBoardHandler;
import com.localvalu.coremerchantapp.utils.AppConstants;
import com.localvalu.coremerchantapp.utils.AppUtils;
import com.localvalu.merchant.OiyaaApplication;
import com.localvalu.merchant.adapter.BluetoothDeviceAdapter;
import com.localvalu.merchant.base.BaseEnum;
import com.localvalu.merchant.base.TimeRecordUtils;

import com.localvalu.merchant.databinding.FragmentPrintAndroidBinding;
import com.localvalu.merchant.printer.PrinterComponent;
import com.rt.printerlibrary.bean.BluetoothEdrConfigBean;
import com.rt.printerlibrary.bean.Position;
import com.rt.printerlibrary.cmd.EscCmd;
import com.rt.printerlibrary.cmd.EscFactory;
import com.rt.printerlibrary.connect.PrinterInterface;
import com.rt.printerlibrary.enumerate.BmpPrintMode;
import com.rt.printerlibrary.enumerate.CommonEnum;
import com.rt.printerlibrary.enumerate.ConnectStateEnum;
import com.rt.printerlibrary.enumerate.ESCFontTypeEnum;
import com.rt.printerlibrary.enumerate.SettingEnum;
import com.rt.printerlibrary.enumerate.WiFiModeEnum;
import com.rt.printerlibrary.exception.SdkException;
import com.rt.printerlibrary.factory.connect.BluetoothFactory;
import com.rt.printerlibrary.factory.connect.PIFactory;
import com.rt.printerlibrary.factory.printer.PrinterFactory;
import com.rt.printerlibrary.factory.printer.UniversalPrinterFactory;
import com.rt.printerlibrary.observer.PrinterObserver;
import com.rt.printerlibrary.observer.PrinterObserverManager;
import com.rt.printerlibrary.printer.RTPrinter;
import com.rt.printerlibrary.setting.BitmapSetting;
import com.rt.printerlibrary.setting.CommonSetting;
import com.rt.printerlibrary.setting.TextSetting;
import com.rt.printerlibrary.utils.BitmapConvertUtil;
import com.rt.printerlibrary.utils.FuncUtils;
import com.rt.printerlibrary.utils.WiFiSettingUtil;
import com.utils.data.DangerousPermission;
import com.utils.util.DateTimeUtil;
import com.utils.validator.Validator;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class PrintFragmentAndroid extends LocalValuFragment implements PrinterObserver,
        BluetoothDeviceAdapter.BluetoothItemClickListener
{

    private static final String TAG = PrintFragmentAndroid.class.getSimpleName();

    private FragmentPrintAndroidBinding binding;
    private DashBoardHandler dashBoardHandler;
    private OrderDetails orderDetails;
    private OrderedFoodItemsPrintAdapter orderedFoodItemsPrintAdapter;
    private String strCurrencySymbol, strCurrencyLetter;
    private int marginSpace;

    private static final int BLUETOOTH_PERMISSIONS = 43;

    private RTPrinter rtPrinter;
    private PrinterFactory printerFactory;
    private ComparatoLevel comparatoLevel = new ComparatoLevel();
    private int iprintTimes = 0;
    private Object configObj;
    private final int REQUEST_ENABLE_BT = 101;
    private ArrayList<PrinterInterface> printerInterfaceArrayList = new ArrayList<>();
    private PrinterInterface curPrinterInterface = null;
    private TextSetting textSetting;
    private ESCFontTypeEnum curESCFontType = null;
    private Bitmap mBitmap;
    private String mChartsetName = "UTF-8";

    //WIFI
    private WifiManager wifiManager;
    private static final String[] WIFI_MMODE = {"STA", "AP"};//spinner的资源
    private WiFiModeEnum wifi_mode = WiFiModeEnum.STA;//wifi模式，0--STA,1--AP
    private List<ScanResult> scanResults;// 拿到扫描周围wifi结果

    //Bluetooth
    private BroadcastReceiver mBluetoothReceiver;
    private IntentFilter mBluetoothIntentFilter;
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothDeviceAdapter pairedDeviceAdapter, foundDeviceAdapter;
    private List<BluetoothDevice> pairedDeviceList, foundDeviceList;
    private boolean mSearchInitiated = false;// 若为true表示搜索设备按钮已按下过，数据已初始化
    private boolean mRegistered = false;// 若为true表示接收器已注册
    private boolean isHidePairedDevlist = false;//是否隐藏配对列表
    private PrinterComponent printerComponent;

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);

        dashBoardHandler = (DashBoardHandler) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();

        if (bundle != null)
        {
            orderDetails = bundle.getParcelable(AppUtils.BUNDLE_ORDER_DETAILS);
        }

        strCurrencySymbol = getString(R.string.symbol_pound);
        strCurrencyLetter = getString(R.string.currency_letter_lt);
        marginSpace = (int) requireContext().getResources().getDimension(R.dimen.margin_15);
        initPrinterComponents();
        initBluetoothPrinter();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        binding = FragmentPrintAndroidBinding.inflate(inflater, container, false);
        setHasOptionsMenu(true);
        setRetainInstance(true);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        checkPermissions();
    }

    @Override
    public void onDetach()
    {
        dashBoardHandler = null;
        super.onDetach();
    }

    private void checkPermissions()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            int permissionCoarseLocation = ContextCompat.checkSelfPermission(requireContext(), ACCESS_COARSE_LOCATION);
            int permissionFineLocation = ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION);
            int permissionBluetooth = ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.BLUETOOTH);
            int permissionBluetoothAdmin = ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.BLUETOOTH_ADMIN);
            int permissionBluetoothScan = ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.BLUETOOTH_SCAN);
            int permissionBluetoothConnect = ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.BLUETOOTH_CONNECT);
            int permissionForReadFile = ContextCompat.checkSelfPermission(requireContext(), READ_EXTERNAL_STORAGE);
            int permissionForwriteFile = ContextCompat.checkSelfPermission(requireContext(), WRITE_EXTERNAL_STORAGE);

            if (permissionCoarseLocation != PackageManager.PERMISSION_GRANTED
                    || permissionFineLocation != PackageManager.PERMISSION_GRANTED
                    || permissionBluetooth != PackageManager.PERMISSION_GRANTED
                    || permissionBluetoothAdmin != PackageManager.PERMISSION_GRANTED
                    || permissionBluetoothScan != PackageManager.PERMISSION_GRANTED
                    || permissionBluetoothConnect != PackageManager.PERMISSION_GRANTED
                    || permissionForReadFile != PackageManager.PERMISSION_GRANTED
                    || permissionForwriteFile != PackageManager.PERMISSION_GRANTED)
            {
                ActivityCompat.requestPermissions(requireActivity(), new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.BLUETOOTH,
                        Manifest.permission.BLUETOOTH_ADMIN,
                        Manifest.permission.BLUETOOTH_SCAN,
                        Manifest.permission.BLUETOOTH_CONNECT}, BLUETOOTH_PERMISSIONS);
                /*requestPermissionsLauncher.launch(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.BLUETOOTH,
                        Manifest.permission.BLUETOOTH_ADMIN,
                        Manifest.permission.BLUETOOTH_SCAN,
                        Manifest.permission.BLUETOOTH_CONNECT});*/
            }
            else
            {
                setProperties();
            }

        }
    }

    private void setProperties()
    {
        binding.btnPrint.setOnClickListener(_OnClickListener);
        binding.btnSearch.setOnClickListener(_OnClickListener);
        binding.btnHide.setOnClickListener(_OnClickListener);
        binding.tvNoPairedDevices.setVisibility(View.GONE);
        binding.rvPairedDevices.setLayoutManager(new LinearLayoutManager(requireContext()));
        binding.rvPairedDevices.setNestedScrollingEnabled(false);
        binding.rvSearchedDevices.setLayoutManager(new LinearLayoutManager(requireContext()));
        binding.rvSearchedDevices.setNestedScrollingEnabled(false);
        setPrintEnable(false);
        //binding.groupSearch.setVisibility(View.GONE);
        registerTheReceiver();
        setData();
        initBluetooth();
        initBluetoothPrinter();
        initLogoBitmap();
        searchBluetoothDevices();
    }

    View.OnClickListener _OnClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            if (v.getId() == com.localvalu.merchant.R.id.btnHide)
            {
                try
                {
                    //setUpPrinterAndPrintMessage(printReceipt());
                    showAndHidePairedDevices();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
            else if (v.getId() == com.localvalu.merchant.R.id.btnPrint)
            {
                try
                {
                    //setUpPrinterAndPrintMessage(printReceipt()); //wifi print
                    printUsingBluetooth();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
            else if (v.getId() == com.localvalu.merchant.R.id.btnSearch)
            {
                searchBluetoothDevices();
            }
        }
    };

    // Register the permissions callback, which handles the user's response to the
    // system permissions dialog. Save the return value, an instance of
    // ActivityResultLauncher, as an instance variable.
    private ActivityResultLauncher<String[]> requestPermissionsLauncher =
            registerForActivityResult(new ActivityResultContracts.RequestMultiplePermissions(), permissions ->
            {
                if (permissions.size() > 0)
                {
                    setProperties();
                }
                else
                {
                    //Permission denied
                }
            });

    public void onPermissionsResult(int requestCode, String[] permissions,
                                    int[] grantResults)
    {
        switch (requestCode)
        {
            case BLUETOOTH_PERMISSIONS:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    // Permission is granted. Continue the action or workflow
                    // in your app.
                    setProperties();
                }
                else
                {
                    // Explain to the user that the feature is unavailable because
                    // the features requires a permission that the user has denied.
                    // At the same time, respect the user's decision. Don't link to
                    // system settings in an effort to convince the user to change
                    // their decision.
                }
                return;
        }
        // Other 'case' lines to check for other
        // permissions this app might request.
    }

    private void setData()
    {
        binding.tvMerchantName.setText(orderDetails.getBusinessName());
        binding.tvOrderId.setText(orderDetails.getOrderId());
        setPaymentDetails();
        setStatus(orderDetails.getStatus());
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        try
        {
            Date orderDate = dateFormat.parse(orderDetails.getDateTime());
            String strDate = DateTimeUtil.getFormattedDate(orderDate.getTime(), DateTimeUtil.MM_dd_yyyy);
            String strTime = DateTimeUtil.getFormattedDate(orderDate.getTime(), DateTimeUtil.hh_mm_a);
            binding.tvOrderDate.setText(strDate);
            binding.tvOrderTime.setText(strTime);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        setFoodItemAdapter();
    }

    private void setPaymentDetails()
    {
        try
        {
            double orderTotal = (Double.parseDouble(orderDetails.getOrderTotal()));
            String strOrderTotal = String.format("%,.2f", orderTotal);
            StringBuilder strSubTotal = new StringBuilder();
            strSubTotal.append(strCurrencySymbol).append(strOrderTotal);

            double discountAmount = (Double.parseDouble(orderDetails.getDiscountLtAmount()));
            String strDiscountAmount = String.format("%,.2f", discountAmount);
            StringBuilder strTokenApply = new StringBuilder();
            strTokenApply.append("-").append(strCurrencyLetter).
                    append(strDiscountAmount);

            double deliveryCharges = (Double.parseDouble(orderDetails.getDeliveryRate()));
            String strDeliveryCharges = String.format("%,.2f", deliveryCharges);
            StringBuilder strDeliveryChargesTwo = new StringBuilder();
            strDeliveryChargesTwo.append("+").append(strCurrencySymbol).append(strDeliveryCharges);

            double finalOrderTotal = (Double.parseDouble(orderDetails.getFinalOrderTotal()));
            String strFinalOrderTotal = String.format("%,.2f", finalOrderTotal);
            StringBuilder strFinalPay = new StringBuilder();
            strFinalPay.append(strCurrencySymbol).append(strFinalOrderTotal);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

    }

    private void setStatus(String status)
    {
        switch (status)
        {
            case AppConstants.ACCEPT:
            case AppConstants.PENDING:
            case AppConstants.DELIVERED:
                //binding.tvNoOfItems.setText(getString(R.string.lbl_ordered_items));
                break;
            case AppConstants.DECLINED:
                //binding.tvNoOfItems.setText(getString(R.string.lbl_declined_ordered_items));
                break;
        }
    }

    private void setFoodItemAdapter()
    {
        if (Validator.isValid(orderDetails.getFoodItems()) && !orderDetails.getFoodItems().isEmpty())
        {
            if (Validator.isValid(orderedFoodItemsPrintAdapter))
            {
                orderedFoodItemsPrintAdapter.addItems(orderDetails.getFoodItems());
            }
            else
            {
                orderedFoodItemsPrintAdapter = new OrderedFoodItemsPrintAdapter(orderDetails.getFoodItems());
            }
        }
        else
        {
            if (Validator.isValid(orderedFoodItemsPrintAdapter))
            {
                orderedFoodItemsPrintAdapter.addItems(new ArrayList<>());
            }
            else
            {
                orderedFoodItemsPrintAdapter = new OrderedFoodItemsPrintAdapter(new ArrayList<>());
            }
        }
    }

    /**
     * 整理扫描wifi获得的list
     */
    private List<ScanResult> sortScanWifi()
    {
        List<ScanResult> scanR = wifiManager.getScanResults();
        int size = scanR.size();
        for (int i = 0; i < size; i++)
        {
            if (TextUtils.isEmpty(scanR.get(i).SSID))
            {
                scanR.remove(i);
                size--;//因为集合删除元素, 长度减了
            }
        }
        Collections.sort(scanR, comparatoLevel);
        return scanR;
    }

    private class ComparatoLevel implements Comparator
    {
        public int compare(Object arg0, Object arg1)
        {
            ScanResult user0 = (ScanResult) arg0;
            ScanResult user1 = (ScanResult) arg1;
            int flag = user1.level - (user0.level);
            if (flag == 0)
            {
                return user1.SSID.compareTo(user0.SSID);
            }
            else
            {
                return flag;
            }
        }
    }

    private Bitmap getBitmapFromView(Context ctx, View view)
    {
        view.setLayoutParams(new
                ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT,
                ConstraintLayout.LayoutParams.MATCH_PARENT));
        DisplayMetrics dm = ctx.getResources().getDisplayMetrics();
        view.measure(View.MeasureSpec.makeMeasureSpec(dm.widthPixels,
                View.MeasureSpec.EXACTLY),
                View.MeasureSpec.makeMeasureSpec(dm.heightPixels,
                        View.MeasureSpec.EXACTLY));
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(),
                view.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        view.layout(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
        view.draw(canvas);
        return bitmap;
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();

        dashBoardHandler.setPreviousTitle();
    }

    private String getOrderIdPrintString()
    {
        StringBuilder strOrderId = new StringBuilder();
        strOrderId.append(getString(R.string.lbl_order_no_colon)).append(orderDetails.getOrderId()).append("\n").append("\n");
        return strOrderId.toString();
    }

    private String getFoodItemsAndBillPrintString()
    {
        StringBuilder strFoodItems = new StringBuilder();

        if (orderDetails.getFoodItems() != null)
        {
            if (orderDetails.getFoodItems().size() > 0)
            {
                for(FoodItem foodItem:orderDetails.getFoodItems())
                {
                    if (foodItem != null)
                    {
                        double price = Double.parseDouble(foodItem.getSingleQuantityPrice());
                        String strPrice = (String.format("%,.2f", price));

                        double quantity = Double.parseDouble(foodItem.getQuantity());
                        double amount = quantity * price;
                        String strAmount = (String.format("%,.2f", amount));

                        if (foodItem.getItemName() != null)
                        {
                            StringBuilder strItemRow = new StringBuilder();
                            String foodItemName = foodItem.getItemName();
                            String foodItemNameFirst, secondName = "";
                            if (foodItemName.length() > 15)
                            {
                                foodItemNameFirst = foodItemName.substring(0, 15);
                                StringBuilder strSecondName = new StringBuilder();
                                strSecondName.append("-").append(foodItemName.substring(16, foodItemName.length()));
                                secondName = strSecondName.toString();
                            }
                            foodItemNameFirst = foodItemName;
                            StringBuilder strPriceQty = new StringBuilder();
                            strPriceQty.append(foodItem.getQuantity()).append("x").append(strPrice).append(" ").append(strAmount);
                            strItemRow.append(getFormattedString(foodItemNameFirst, strPriceQty.toString()));
                            strFoodItems.append(strItemRow.toString()).append("\n");
                        }
                    }

                }
            }
        }

        double orderTotal = (Double.parseDouble(orderDetails.getOrderTotal()));
        String strOrderTotal = String.format("%,.2f", orderTotal);
        StringBuilder strSubTotal = new StringBuilder();
        strSubTotal.append(strCurrencySymbol).append(strOrderTotal);

        double discountAmount = (Double.parseDouble(orderDetails.getDiscountLtAmount()));
        String strDiscountAmount = String.format("%,.2f", discountAmount);
        StringBuilder strTokenApply = new StringBuilder();
        strTokenApply.append("-").append(strCurrencyLetter).append(strDiscountAmount);

        double deliveryCharges = (Double.parseDouble(orderDetails.getDeliveryRate()));
        String strDeliveryCharges = String.format("%,.2f", deliveryCharges);
        StringBuilder strDeliveryChargesTwo = new StringBuilder();
        strDeliveryChargesTwo.append("+").append(strCurrencySymbol).append(strDeliveryCharges);

        StringBuilder strReceipt = new StringBuilder();
        strReceipt.append(strFoodItems).append(strFoodItems.toString());
        strReceipt.append("\n");
        strReceipt.append(getFormattedString(getString(R.string.lbl_sub_total), strSubTotal.toString())).append("\n");
        strReceipt.append(getFormattedString(getString(R.string.lbl_token_applied), strTokenApply.toString())).append("\n");
        strReceipt.append(getFormattedString(getString(R.string.lbl_delivery_fee), strDeliveryChargesTwo.toString())).append("\n");
        return strReceipt.toString();
    }

    private String getUnderlinedChars()
    {
        StringBuilder strUnderlinedChars = new StringBuilder();
        for(int i=0;i<38;i++)
        {
            strUnderlinedChars.append(" ");
        }
        return strUnderlinedChars.toString();
    }

    private String getFinalPayPrintString()
    {
        double finalOrderTotal = (Double.parseDouble(orderDetails.getFinalOrderTotal()));
        String strFinalOrderTotal = String.format("%,.2f", finalOrderTotal);
        StringBuilder strFinalPay = new StringBuilder();
        strFinalPay.append(strCurrencySymbol).append(strFinalOrderTotal);

        StringBuilder strFinalPayString = new StringBuilder();
        strFinalPayString.append("\n");
        strFinalPayString.append(getFormattedString(getString(R.string.lbl_final_payment), strFinalPay.toString()));
        return strFinalPayString.toString();
    }

    private String getSpecialInstructions()
    {
        StringBuilder strSpecialInstructions = new StringBuilder();
        strSpecialInstructions.append(getString(R.string.lbl_special_instructions_colon)).append(" ");
        if(orderDetails.getSpecialInstructions()!=null)
        {
            if(!orderDetails.getSpecialInstructions().isEmpty())
            {
                strSpecialInstructions.append(orderDetails.getAllergyInstructions());
                strSpecialInstructions.append("\n");
            }
            else
            {
                strSpecialInstructions.append("NA").append("\n");
            }
        }
        else
        {
            strSpecialInstructions.append("NA").append("\n");
        }
        return strSpecialInstructions.toString();
    }

    private String getAllergyInstructions()
    {
        StringBuilder strAllergyInstructions = new StringBuilder();
        strAllergyInstructions.append(getString(R.string.lbl_allergy_instructions_colon)).append(" ");
        if(orderDetails.getSpecialInstructions()!=null)
        {
            if(!orderDetails.getSpecialInstructions().isEmpty())
            {
                strAllergyInstructions.append(orderDetails.getAllergyInstructions());
                strAllergyInstructions.append("\n");
            }
            else
            {
                strAllergyInstructions.append("NA").append("\n");
            }
        }
        else
        {
            strAllergyInstructions.append("NA").append("\n");
        }
        return strAllergyInstructions.toString();

    }

    private String getFormattedString(String leftSide, String rightSide)
    {
        char[] chars = new char[37];

        /*if (rightSide.contains(getString(R.string.symbol_pound)))
        {
            chars = new char[37];
        }*/
        if (!rightSide.contains(strCurrencySymbol))
        {
            chars = new char[38];
        }
        int charCount = 0;

        for (char rowChar : chars)
        {
            chars[charCount] = ' ';
            charCount++;
        }

        for (int k = 0; k < leftSide.length(); k++)
        {
            chars[k] = leftSide.charAt(k);
        }

        int newCharCount = chars.length - 1;
        int rightCharacterCount = rightSide.length() - 1;

        for (char rowChar : chars)
        {
            if (rightCharacterCount < 0)
            {
                break;
            }
            else
            {
                chars[newCharCount] = rightSide.charAt(rightCharacterCount);
                newCharCount--;
                rightCharacterCount--;
            }
        }

        String theText = new String(chars);
        Log.d(TAG, " Chars - " + theText);
        return theText;
    }

    private void getShortFoodSizeName(FoodItem foodItem)
    {
        StringBuilder strFoodSizeName = new StringBuilder();
        String[] strItemName = foodItem.getItemName().split(" ");

        if (strFoodSizeName != null)
        {
            if (strItemName.length > 0)
            {
                strFoodSizeName.append(strItemName[0].substring(0, 1)).append(".");
                strFoodSizeName.append(strItemName[1]).append("(").append(foodItem.getSizeName()
                        .substring(0, 3)).append(")");
            }
            else
            {
                strFoodSizeName.append(strItemName[1]).append("(").append(foodItem.getSizeName()
                        .substring(0, 3)).append(")");
            }
        }
    }

    private String getPrintString(String strLeft, String strRight)
    {
        Character[] chars = new Character[37];

        int maxValue = chars.length - strRight.length();

        for (int i = chars.length - 1; i > strRight.length(); i--)
        {
            chars[i] = strRight.charAt(i);
        }

        for (int i = 0; i < strLeft.length() - 1; i++)
        {
            chars[i] = strLeft.charAt(i);
        }

        return chars.toString();
    }

    private String getSecondLineFoodName(FoodItem foodItem)
    {
        StringBuilder strFoodSizeName = new StringBuilder();
        String[] strItemName = foodItem.getItemName().split(" ");

        if (strItemName != null)
        {
            if (strItemName.length > 1)
            {
                strFoodSizeName.append(strItemName[1]).append(" ").append(strItemName[2]).append("(").append(foodItem.getSizeName()).append(")");
                return strFoodSizeName.toString();
            }
            else if (strItemName.length > 0)
            {
                strFoodSizeName.append(strItemName[1]).append(" ").append("(").append(foodItem.getSizeName()).append(")");
                return strFoodSizeName.toString();
            }
        }
        return foodItem.getItemName();
    }

    private String getFirstLineFoodName(FoodItem foodItem)
    {
        StringBuilder strFoodSizeName = new StringBuilder();
        String[] strItemName = foodItem.getItemName().split(" ");

        if (strItemName != null)
        {
            if (strItemName.length > 0)
            {
                return strFoodSizeName.append(strItemName[0]).toString();
            }
            else
            {
                return strFoodSizeName.append(strItemName[0]).toString();
            }
        }
        return foodItem.getItemName();
    }


    /**
     * Wifi Print Code Starts
     **/

    public void setUpPrinterAndPrintMessage(String strMessage)
    {
        // TODO Auto-generated method stub
        String currentSSID = null;
        ConnectivityManager cm = (ConnectivityManager) requireContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo == null)
        {
            Toast.makeText(requireContext(), getString(R.string.lbl_connect_to_wifi), Toast.LENGTH_LONG).show();
        }
        if (networkInfo.isConnected())
        {
            wifiManager = (WifiManager) requireActivity().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            if (!wifiManager.isWifiEnabled())
            {
                wifiManager.setWifiEnabled(true);
            }
            final WifiInfo connectionInfo = wifiManager.getConnectionInfo();
            if (connectionInfo != null && !TextUtils.isEmpty(connectionInfo.getSSID()))
            {
                currentSSID = connectionInfo.getSSID();
                wifiManager.startScan();
                scanResults = sortScanWifi();
                initPrinterComponents();
                for (ScanResult scanResult : scanResults)
                {
                    if (currentSSID.equals(scanResult.SSID))
                    {
                        initSetWiFi(scanResult, strMessage);
                        return;
                    }
                }
            }
            else
            {
                Toast.makeText(requireContext(), getString(R.string.lbl_connect_to_wifi), Toast.LENGTH_LONG).show();
            }
        }
        else
        {
            Toast.makeText(requireContext(), getString(R.string.lbl_connect_to_wifi), Toast.LENGTH_LONG).show();
        }


    }

    /**
     * .拿到扫描的到的指定wifi信息,进行分析
     *
     * @param scanResult
     */
    private void initSetWiFi(ScanResult scanResult, String strMessage)
    {
        doPrint(scanResult, strMessage);
    }

    /**
     * 弹出确认连接wifi的dialog
     *
     * @param scanResult
     */
    private void doPrint(final ScanResult scanResult, String strMessage)
    {
        int WIFIType = 0;
        if (scanResult.capabilities.contains("WPA2-PSK"))
        {//加密模式WPA-PSK
            WIFIType = 1;
        }
        else if (scanResult.capabilities.contains("WPA-PSK"))
        {//加密模式WPA2-PSK
            WIFIType = 1;
        }
        else if (scanResult.capabilities.contains("WEP"))
        {//加密模式WEP
            WIFIType = 2;
        }
        else
        {//开放无密码
            WIFIType = 0;
        }

        if (rtPrinter == null || rtPrinter.getConnectState() != ConnectStateEnum.Connected)
        {
            Toast.makeText(requireContext(), getString(com.localvalu.merchant.R.string.lbl_please_connect_printer_first), Toast.LENGTH_LONG).show();
            return;
        }
        //write the wifi infos to the printer.
        rtPrinter.writeMsgAsync(WiFiSettingUtil.getInstance().setWiFiParam(scanResult, "dummy text", wifi_mode));

    }

    /** Wifi Print Code Ends **/


    /**
     * Bluetooth Print Code Starts
     **/

    private void connectBluetooth(BluetoothEdrConfigBean bluetoothEdrConfigBean)
    {
        PIFactory piFactory = new BluetoothFactory();
        PrinterInterface printerInterface = piFactory.create();
        printerInterface.setConfigObject(bluetoothEdrConfigBean);
        rtPrinter.setPrinterInterface(printerInterface);
        try
        {
            rtPrinter.connect(bluetoothEdrConfigBean);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            // Log.e("mao",e.getMessage());
        }
        finally
        {

        }
    }

    public void showAndHidePairedDevices()
    {
        if (isHidePairedDevlist)
        {
            //当前是隐藏
            isHidePairedDevlist = false;
            binding.rvPairedDevices.setVisibility(View.VISIBLE);
            binding.btnHide.setText(getString(com.localvalu.merchant.R.string.lbl_hide));
        }
        else
        {
            //当前是可见
            isHidePairedDevlist = true;
            binding.rvPairedDevices.setVisibility(View.GONE);
            binding.btnHide.setText(getString(com.localvalu.merchant.R.string.lbl_show));
        }

    }

    private void isConfigPrintEnable(Object configObj)
    {
        if (isInConnectList(configObj))
        {
            setPrintEnable(true);
        }
        else
        {
            setPrintEnable(false);
        }
    }

    private boolean isInConnectList(Object configObj)
    {
        boolean isInList = false;
        for (int i = 0; i < printerInterfaceArrayList.size(); i++)
        {
            PrinterInterface printerInterface = printerInterfaceArrayList.get(i);
            if (configObj.toString().equals(printerInterface.getConfigObject().toString()))
            {
                if (printerInterface.getConnectState() == ConnectStateEnum.Connected)
                {
                    isInList = true;
                    break;
                }
            }
        }
        return isInList;
    }

    @Override
    public void printerObserverCallback(final PrinterInterface printerInterface, final int state)
    {
        if (this.isAdded())
        {
            requireActivity().runOnUiThread(new Runnable()
            {
                @Override
                public void run()
                {
                    switch (state)
                    {
                        case CommonEnum.CONNECT_STATE_SUCCESS:
                            TimeRecordUtils.record("RT连接end：", System.currentTimeMillis());
                            Toast.makeText(requireContext(), printerInterface.getConfigObject().toString() +
                                            getString(com.localvalu.coremerchantapp.R.string.lbl_main_connected),
                                    Toast.LENGTH_SHORT).show();

                            binding.tvConnectedDevice.setTag(BaseEnum.HAS_DEVICE);
                            curPrinterInterface = printerInterface;//设置为当前连接， set current Printer Interface
                            printerInterfaceArrayList.add(printerInterface);//多连接-添加到已连接列表
                            rtPrinter.setPrinterInterface(printerInterface);
                            //BaseApplication.getInstance().setRtPrinter(rtPrinter);
                            setPrintEnable(true);
                            break;
                        case CommonEnum.CONNECT_STATE_INTERRUPTED:
                            if (printerInterface != null && printerInterface.getConfigObject() != null)
                            {
                                Toast.makeText(requireContext(), printerInterface.getConfigObject().toString()
                                        + getString(R.string.lbl_main_connected), Toast.LENGTH_SHORT).show();
                            }
                            else
                            {
                                Toast.makeText(requireContext(),
                                        getString(R.string.lbl_main_connected), Toast.LENGTH_LONG).show();
                            }
                            TimeRecordUtils.record("RT连接断开：", System.currentTimeMillis());
                            binding.tvConnectedDevice.setTag(BaseEnum.NO_DEVICE);
                            curPrinterInterface = null;
                            printerInterfaceArrayList.remove(printerInterface);//多连接-从已连接列表中移除
                            //  BaseApplication.getInstance().setRtPrinter(null);
                            setPrintEnable(false);
                            break;
                        default:
                            break;
                    }
                }
            });
        }
    }

    @Override
    public void printerReadMsgCallback(PrinterInterface printerInterface, final byte[] bytes)
    {
        Log.i(TAG, "printerReadMsgCallback: " + FuncUtils.ByteArrToHex(bytes));
        requireActivity().runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {
//                PrinterStatusBean StatusBean = PrinterStatusPareseUtils.parsePrinterStatusResult(bytes);
//                Log.i(TAG, "run: "+FuncUtils.ByteArrToHex(bytes));
//                String statusStr = PrinterStatusPareseUtils.getPrinterStatusStr(StatusBean);
//                Log.i(TAG, "run: "+statusStr);
//                if (!TextUtils.isEmpty(statusStr))
//                    ToastUtil.show(MainActivity.this, statusStr);
                /*if (StatusBean.printStatusCmd == PrintStatusCmd.cmd_PrintFinish) {
                    if (StatusBean.blPrintSucc) {
                        //  Log.e("mydebug","print ok");
                        ToastUtil.show(MainActivity.this, "print ok");
                    } else {
                        ToastUtil.show(MainActivity.this, PrinterStatusPareseUtils.getPrinterStatusStr(StatusBean));

                    }


                } else if (StatusBean.printStatusCmd == PrintStatusCmd.cmd_Normal) {
                    ToastUtil.show(MainActivity.this, "print status：" + PrinterStatusPareseUtils.getPrinterStatusStr(StatusBean));

                } else if (StatusBean.printStatusCmd == PrintStatusCmd.cmd_Print_RPP02N) {
                    String msg = FuncUtils.PrinterStatusToStr(StatusBean);
                    if (!msg.isEmpty())
                        ToastUtil.show(MainActivity.this, "print status：" + msg);
                }*/
            }
        });
    }

    /**
     * 设置是否可进行打印操作
     *
     * @param isEnable
     */
    private void setPrintEnable(boolean isEnable)
    {
        if (isEnable)
        {
            binding.btnPrint.setEnabled(true);
        }
        else
        {
            binding.btnPrint.setEnabled(false);
        }

    }

    private void initBluetoothPrinter()
    {

        printerFactory = new UniversalPrinterFactory();
        rtPrinter = printerFactory.create();
        textSetting = new TextSetting();
        curESCFontType = ESCFontTypeEnum.FONT_B_9x24;
        PrinterObserverManager.getInstance().add(this);//添加连接状态监听
    }

    private void printUsingBluetooth()
    {
        /*CmdFactory cmdFactory = new EscFactory();
        Cmd cmd = cmdFactory.create();
        cmd.append(cmd.getHeaderCmd());
        cmd.append(cmd.getLFCRCmd());
        cmd.append(cmd.getSelfTestCmd());
        cmd.append(cmd.getLFCRCmd());
        rtPrinter.writeMsgAsync(cmd.getAppendCmds());*/
        try
        {
            EscFactory escFac = new EscFactory();
            EscCmd escCmd = escFac.create();
            escCmd.append(escCmd.getHeaderCmd());//初始化, Initial
            escCmd.setChartsetName(mChartsetName);

            BitmapSetting bitmapSetting = new BitmapSetting();
            bitmapSetting.setBmpPrintMode(BmpPrintMode.MODE_SINGLE_COLOR);
            bitmapSetting.setBimtapLimitWidth(37 * 8);
            bitmapSetting.setPrintPostion(new Position(50, 0));
            try
            {
                escCmd.append(escCmd.getBitmapCmd(bitmapSetting, mBitmap));
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            CommonSetting commonSetting = new CommonSetting();
            Position txtposition = new Position(0, 0);
            textSetting.setTxtPrintPosition(txtposition);//如果没设置X值的偏移，就不要调用了
            textSetting.setEscFontType(curESCFontType);
            textSetting.setBold(SettingEnum.Enable);
            textSetting.setUnderline(SettingEnum.Disable);

            commonSetting.setEscLineSpacing(getInputLineSpacing());
            escCmd.append(escCmd.getCommonSettingCmd(commonSetting));
            //byte[] btContent = getFoodItemsAndBillPrintString().getBytes(StandardCharsets.UTF_8);
            escCmd.append(escCmd.getTextCmd(textSetting, getOrderIdPrintString(), mChartsetName));
            textSetting.setBold(SettingEnum.Disable);
            textSetting.setUnderline(SettingEnum.Disable);
            escCmd.append(escCmd.getTextCmd(textSetting, getFoodItemsAndBillPrintString(), mChartsetName));
            textSetting.setUnderline(SettingEnum.Enable);
            textSetting.setBold(SettingEnum.Disable);
            escCmd.append(escCmd.getTextCmd(textSetting, getUnderlinedChars(), mChartsetName));
            textSetting.setUnderline(SettingEnum.Disable);
            textSetting.setBold(SettingEnum.Enable);
            escCmd.append(escCmd.getTextCmd(textSetting, getUnderlinedChars(), mChartsetName));
            escCmd.append(escCmd.getTextCmd(textSetting, getFinalPayPrintString(), mChartsetName));
            escCmd.append(escCmd.getTextCmd(textSetting, getUnderlinedChars(), mChartsetName));
            if(getSpecialInstructions()!="")
            {
                escCmd.append(escCmd.getTextCmd(textSetting, getSpecialInstructions(), mChartsetName));
            }
            if(getAllergyInstructions()!="")
            {
                escCmd.append(escCmd.getTextCmd(textSetting, getAllergyInstructions(), mChartsetName));
            }
            escCmd.append(escCmd.getLFCRCmd());
            escCmd.append(escCmd.getCmdCutNew());//切刀指令
            rtPrinter.writeMsgAsync(escCmd.getAppendCmds());
            /*try
            {
                Thread.sleep(200);
            }
            catch (InterruptedException ex)
            {
                ex.printStackTrace();
            }
            requireActivity().runOnUiThread(new Runnable()
            {
                @Override
                public void run()
                {
                    escCmd.append(escCmd.getLFCRCmd());
                    escCmd.append(escCmd.getCmdCutNew());//切刀指令
                    rtPrinter.writeMsgAsync(escCmd.getAppendCmds());
                    rtPrinter.writeMsgAsync(btContent);
                }
            });*/

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    /**
     * line spacing setting
     */
    private int getInputLineSpacing()
    {
        int n = 30;
        if (n > 255)
        {
            n = 255;
        }
        return n;
    }

    /** Bluetooth Print Code Ends **/

    /**
     * Bluetooth Devices find code Starts
     */

    private final BroadcastReceiver receiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            Log.d(TAG, "onReceive");
            String action = intent.getAction();

            // When discovery finds a device
            if (BluetoothDevice.ACTION_FOUND.equals(action))
            {
                // Get the BluetoothDevice object from the Intent
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                int devType = device.getBluetoothClass().getMajorDeviceClass();
                if (devType != BluetoothClass.Device.Major.IMAGING)
                {
                    return;
                }

                if (!foundDeviceList.contains(device))
                {
                    foundDeviceList.add(device);
                    foundDeviceAdapter.notifyDataSetChanged();
                }
            }
            else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action))
            {
                mBluetoothAdapter.cancelDiscovery();
                requireContext().unregisterReceiver(receiver);
                mRegistered = false;
                binding.btnSearch.setEnabled(true);
                binding.pbSearch.setVisibility(View.GONE);
                if (foundDeviceList != null && foundDeviceList.size() == 0)
                {
                    binding.tvNoDevices.setVisibility(View.VISIBLE);
                }
            }
        }
    };

    private void initBluetooth()
    {
        Log.d(TAG, "initBluetooth");
        try
        {
            if (!BluetoothAdapter.getDefaultAdapter().isEnabled())
            {   //蓝牙未开启，则开启蓝牙
                Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
            }
            else
            {
                mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                pairedDeviceList = new ArrayList<>(mBluetoothAdapter.getBondedDevices());
                if (pairedDeviceList.size() == 0)
                {
                    binding.tvNoPairedDevices.setVisibility(View.VISIBLE);
                    binding.rvPairedDevices.setVisibility(View.GONE);
                }
                else if (pairedDeviceList.size() > 0)
                {
                    pairedDeviceAdapter = new BluetoothDeviceAdapter(pairedDeviceList);
                    binding.rvPairedDevices.setAdapter(pairedDeviceAdapter);
                    pairedDeviceAdapter.setBluetoothItemClickListener(this);
                    binding.tvNoPairedDevices.setVisibility(View.GONE);
                    binding.rvPairedDevices.setVisibility(View.VISIBLE);
                }
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    private void searchBluetoothDevices()
    {
        /*Intent bluetoothPicker = new Intent("android.bluetooth.devicepicker.action.LAUNCH");
        bluetoothPicker.putExtra("android.bluetooth.devicepicker.extra.FILTER_TYPE", 2);
        bluetoothPicker.putExtra("android.bluetooth.devicepicker.extra.NEED_AUTH", false);
        startActivity(bluetoothPicker);*/

        binding.btnSearch.setEnabled(false);
        binding.pbSearch.setVisibility(View.VISIBLE);
        binding.tvNoDevices.setVisibility(View.GONE);
        if (mSearchInitiated)
        {
            foundDeviceList.clear();
            foundDeviceAdapter.notifyDataSetChanged();
        }
        else
        {
            foundDeviceList = new ArrayList<>();
            foundDeviceAdapter = new BluetoothDeviceAdapter(foundDeviceList);
            binding.rvSearchedDevices.setAdapter(foundDeviceAdapter);
            foundDeviceAdapter.setBluetoothItemClickListener(this);
            //mBluetoothReceiver = new BluetoothDeviceReceiver();
            mSearchInitiated = true;
        }
        mRegistered = true;
        mBluetoothAdapter.startDiscovery();
    }

    private void registerTheReceiver()
    {
        mBluetoothIntentFilter = new IntentFilter();
        mBluetoothIntentFilter.addAction(BluetoothDevice.ACTION_FOUND);
        mBluetoothIntentFilter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        requireContext().registerReceiver(receiver, mBluetoothIntentFilter);
    }

    private void initLogoBitmap()
    {
        Uri uri = Uri.parse("android.resource://com.localvalu.merchant/drawable/app_logo_transparent_small");
        if (uri != null)
        {
            if (mBitmap != null)
            {
                mBitmap.recycle();
                mBitmap = null;
                System.gc();
            }
            try
            {
                mBitmap = MediaStore.Images.Media.getBitmap(requireContext().getContentResolver(), uri);
                if(mBitmap!=null)
                {
                    if (mBitmap.getWidth() > 48 * 8)
                    {
                        Log.d(TAG, "mBitmap getWidth = " + mBitmap.getWidth());
                        Log.d(TAG, "mBitmap getHeight = " + mBitmap.getHeight());
                        //mBitmap = BitmapConvertUtil.decodeSampledBitmapFromUri(requireContext(), uri, 48 * 8, 4000);
                        //Log.d(TAG, "mBitmap getWidth = " + mBitmap.getWidth());
                        //Log.d(TAG, "mBitmap getHeight = " + mBitmap.getHeight());
                    }
                }

            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

        }
    }

    private void initPrinterComponents()
    {
        printerComponent = PrinterComponent.getInstance();
        printerComponent.setCurrentCmdType(BaseEnum.CMD_ESC);
        printerComponent.setPrinterSettings(requireContext());
        rtPrinter = printerComponent.getRtPrinter();
    }

    @Override
    public void onPairedDeviceClicked(BluetoothDevice bluetoothDevice, int position)
    {
        mBluetoothAdapter.cancelDiscovery();
        if (mRegistered)
        {
            requireContext().unregisterReceiver(receiver);
            mRegistered = false;
        }
        if (TextUtils.isEmpty(bluetoothDevice.getName()))
        {
            binding.tvConnectedDevice.setText(bluetoothDevice.getAddress());
        }
        else
        {
            binding.tvConnectedDevice.setText(bluetoothDevice.getName() + " [" + bluetoothDevice.getAddress() + "]");
        }
        configObj = new BluetoothEdrConfigBean(bluetoothDevice);
        binding.tvConnectedDevice.setTag(BaseEnum.HAS_DEVICE);
        isConfigPrintEnable(configObj);
        connectBluetooth((BluetoothEdrConfigBean) configObj);
    }

    @Override
    public void onSearchedDeviceClicked(BluetoothDevice bluetoothDevice, int position)
    {
        mBluetoothAdapter.cancelDiscovery();
        if (mRegistered)
        {
            requireContext().unregisterReceiver(receiver);
            mRegistered = false;
        }
        if (TextUtils.isEmpty(bluetoothDevice.getName()))
        {
            binding.tvConnectedDevice.setText(bluetoothDevice.getAddress());
        }
        else
        {
            binding.tvConnectedDevice.setText(bluetoothDevice.getName() + " [" + bluetoothDevice.getAddress() + "]");
        }
        configObj = new BluetoothEdrConfigBean(bluetoothDevice);
        binding.tvConnectedDevice.setTag(BaseEnum.HAS_DEVICE);
        isConfigPrintEnable(configObj);
        connectBluetooth((BluetoothEdrConfigBean) configObj);
    }

    /**
     * Bluetooth Devices find code Ends
     */
}
