package com.localvalu.merchant.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.Toast
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputLayout
import com.localvalu.coremerchantapp.LocalValuFragment
import com.localvalu.coremerchantapp.R
import com.localvalu.coremerchantapp.handler.DashBoardHandler
import com.localvalu.merchant.OiyaaApplication
import com.utils.helper.preference.PreferenceFactory
import java.lang.Exception

class PrintSettings : LocalValuFragment()
{
    private var dashBoardHandler: DashBoardHandler? = null
    private var strCurrencySymbol: String? = null
    private var strCurrencyLetter: String? = null
    private var marginSpace = 0
    private var labelSize: String? = null
    private var tilLabelType: TextInputLayout? = null
    private var tilLabelSize: TextInputLayout? = null
    private var tilPrintOffset: TextInputLayout? = null
    private var tilPrintSpeed: TextInputLayout? = null
    private var etLabelType: AutoCompleteTextView? = null
    private var etLabelSize: AutoCompleteTextView? = null
    private var etPrintOffset: AutoCompleteTextView? = null
    private var etPrintSpeed: AutoCompleteTextView? = null
    private var btnSave:MaterialButton?=null
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        val bundle = arguments
        if (bundle != null)
        {
        }
        strCurrencySymbol = getString(R.string.symbol_pound)
        strCurrencyLetter = getString(R.string.currency_letter_lt)
        marginSpace = requireContext().resources.getDimension(R.dimen.margin_15)
            .toInt()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?
    {
        val view = inflater.inflate(
            com.localvalu.merchant.R.layout.fragment_print_settings,
            container,
            false
        )
        setHasOptionsMenu(true)
        retainInstance = true
        initView(view)
        return view
    }

    private fun initView(view: View)
    {
        tilLabelType = view.findViewById(com.localvalu.merchant.R.id.tilLabelType)
        tilLabelSize = view.findViewById(com.localvalu.merchant.R.id.tilLabelSize)
        tilPrintOffset = view.findViewById(com.localvalu.merchant.R.id.tilPrintOffset)
        tilPrintSpeed = view.findViewById(com.localvalu.merchant.R.id.tilPrintSpeed)
        etLabelType = view.findViewById(com.localvalu.merchant.R.id.etLabelType)
        etLabelSize = view.findViewById(com.localvalu.merchant.R.id.etLabelSize)
        etPrintOffset = view.findViewById(com.localvalu.merchant.R.id.etPrintOffset)
        etPrintSpeed = view.findViewById(com.localvalu.merchant.R.id.etPrintSpeed)
        btnSave = view.findViewById(com.localvalu.merchant.R.id.btnSave)
    }

    private fun setProperties()
    {
        val printSize = arrayOf(resources.getStringArray(com.localvalu.merchant.R.array.label_setting_size))
        val printSizeAdapter = ArrayAdapter(requireContext(), com.localvalu.merchant.R.layout.view_item_spinner, printSize)
        (tilLabelSize?.editText as? AutoCompleteTextView)?.setAdapter(printSizeAdapter)

        val sd = arrayOf(resources.getStringArray(com.localvalu.merchant.R.array.label_setting_sd))
        val sdSizeAdapter = ArrayAdapter(requireContext(), com.localvalu.merchant.R.layout.view_item_spinner, sd)
        (tilLabelSize?.editText as? AutoCompleteTextView)?.setAdapter(sdSizeAdapter)

        btnSave?.setOnClickListener(_OnClickListener)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        setProperties()
    }

    override fun onAttach(context: Context)
    {
        super.onAttach(context)
        dashBoardHandler = context as DashBoardHandler
    }

    override fun onDetach()
    {
        dashBoardHandler = null
        super.onDetach()
    }

    var _OnClickListener = View.OnClickListener { v ->
        when(v.id)
        {
            com.localvalu.merchant.R.id.btnSave->
            {
                try
                {
                    saveSetting()
                }
                catch (e: Exception)
                {
                    e.printStackTrace()
                }
            }

        }
    }

    private fun saveSetting()
    {
        labelSize = etLabelSize?.getText().toString()
        println("labelSize$labelSize")
        OiyaaApplication.labelSizeStr = labelSize!!.replace("(\\d+)".toRegex(), "$1mm")
        val temp = labelSize!!.split("\\*".toRegex()).toTypedArray()
        OiyaaApplication.labelWidth = temp[0]
        OiyaaApplication.labelHeight = temp[1]
        OiyaaApplication.labelSpeed = etPrintSpeed?.getText().toString()
        OiyaaApplication.labelType = etLabelType?.getText().toString()
        OiyaaApplication.labelOffset = etPrintOffset?.getText().toString()
        PreferenceFactory.getInstance().setString(SP_KEY_LABEL_SIZE,labelSize)
        PreferenceFactory.getInstance().setString(SP_KEY_LABEL_SPEED,labelSize)
        PreferenceFactory.getInstance().setString(SP_KEY_LABEL_TYPE,labelSize)
        PreferenceFactory.getInstance().setString(SP_KEY_LABEL_OFFSET,labelSize)
        Toast.makeText(requireContext(),getString(R.string.lbl_settings_saved),Toast.LENGTH_LONG).show()
    }

    override fun onDestroy()
    {
        super.onDestroy()
        dashBoardHandler!!.setPreviousTitle()
    }

    companion object
    {
        private val TAG = PrintSettings::class.java.simpleName
    }
}