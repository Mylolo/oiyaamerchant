package com.localvalu.merchant.printer;

import android.content.Context;

import com.localvalu.merchant.R;
import com.localvalu.merchant.base.BaseEnum;
import com.rt.printerlibrary.printer.RTPrinter;
import com.utils.helper.preference.PreferenceFactory;

public class PrinterComponent
{
    private static final String SP_KEY_LABEL_SIZE = "";
    private RTPrinter rtPrinter;

    private static PrinterComponent printerComponent = null;

    private PrinterComponent()
    {

    }

    public static PrinterComponent getInstance()
    {
        if (printerComponent == null)
            printerComponent = new PrinterComponent();
        return printerComponent;
    }

    @BaseEnum.CmdType
    private int currentCmdType = BaseEnum.CMD_PIN;//默认为针打

    @BaseEnum.ConnectType
    private int currentConnectType = BaseEnum.NONE;//默认未连接

    public static final String SP_NAME_SETTING = "setting";

    public static String labelSizeStr = "80*40", labelWidth = "80", labelHeight = "40", labelSpeed = "2", labelType = "CPCL", labelOffset = "0";

    public RTPrinter getRtPrinter()
    {
        return rtPrinter;
    }

    public void setRtPrinter(RTPrinter rtPrinter)
    {
        this.rtPrinter = rtPrinter;
    }

    @BaseEnum.CmdType
    public int getCurrentCmdType()
    {
        return currentCmdType;
    }

    public void setCurrentCmdType(@BaseEnum.CmdType int currentCmdType)
    {
        this.currentCmdType = currentCmdType;
    }

    @BaseEnum.ConnectType
    public int getCurrentConnectType()
    {
        return currentConnectType;
    }

    public void setCurrentConnectType(@BaseEnum.ConnectType int currentConnectType)
    {
        this.currentConnectType = currentConnectType;
    }

    public void setPrinterSettings(Context context)
    {
        String strLabelSize = PreferenceFactory.getInstance().getString(SP_KEY_LABEL_SIZE);
        if (strLabelSize == null || strLabelSize.isEmpty())
        {
            strLabelSize = context.getResources().getStringArray(R.array.label_setting_size)[0];
        }
        String[] temp = strLabelSize.split("\\*");
        labelWidth = temp[0];
        labelHeight = temp[1];

    }
}
