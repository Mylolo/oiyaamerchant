package com.localvalu.merchant.optomany.activity.handler;

import android.os.Bundle;

import com.localvalu.coremerchantapp.data.output.order.OrderDetails;
import com.localvalu.merchant.optomany.fragment.orders.OrderAtTableDetailsPaxFragment;
import com.localvalu.merchant.optomany.fragment.orders.OrderDetailsPaxFragment;
import com.localvalu.merchant.optomany.fragment.pax.PrintFragment;
import com.localvalu.merchant.optomany.fragment.pax.PaxTransactionsDetailsFragment;
import com.localvalu.merchant.optomany.fragment.pax.PaxTransactionsFragment;

public interface PaxHandler
{
    OrderDetailsPaxFragment orderDetailsPaxFragment(String orderId);

    OrderAtTableDetailsPaxFragment orderAtTableDetailsPaxFragment(String orderId);

    PrintFragment printFragment(OrderDetails orderDetails);

    PaxTransactionsFragment paxTransactionFragment(Bundle bundle);

    PaxTransactionsDetailsFragment paxTransactionDetailsFragment(Bundle bundle);

}
