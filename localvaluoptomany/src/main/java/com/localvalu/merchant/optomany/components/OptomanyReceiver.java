package com.localvalu.merchant.optomany.components;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.gson.Gson;
import com.localvalu.coremerchantapp.utils.AppUtils;
import com.localvalu.merchant.optomany.activity.DashboardActivity;

import org.json.JSONObject;

import eft.com.eftservicelib.EFTServiceLib;
import eft.com.eftservicelib.EFTServiceTransResult;

import static com.localvalu.coremerchantapp.utils.AppUtils.BROADCAST_ACTION;

public class OptomanyReceiver extends BroadcastReceiver
{
    public static final String TRANSACTION_RESULT_EVENT = "eft.com.TRANSACTION_RESULT";
    public static final String TRANSACTION_NOT_FOUND = "com.localvalu.merchant.optomany.TRANSACTION_NOT_FOUND";

    /* These need to match up with the manifest for app and the service (DOP NOT CHANGE) */
    private static final String TAG = "OptomanyReceiver";

    @Override
    public void onReceive(Context context, Intent intent)
    {
        if (TRANSACTION_RESULT_EVENT.equals(intent.getAction()))
        {
            /*boolean transFound = intent.getBooleanExtra("TransResponse", false);

            if(transFound)
            {
                EFTServiceTransResult result = EFTServiceLib.unpackResult(context, intent);
                Gson gson = new Gson();
                String transResult = gson.toJson(result);
                Intent broadCastIntent = new Intent();
                broadCastIntent.setAction(TRANSACTION_RESULT_EVENT);
                broadCastIntent.putExtra(AppUtils.BUNDLE_TRANSACTION_SUCCESS_FAILURE_DETAILS, transResult);
                if (intent.hasExtra("UTI"))
                    broadCastIntent.putExtra(AppUtils.BUNDLE_LAST_RECEIVED_UTI,intent.getStringExtra("UTI"));
                context.sendBroadcast(broadCastIntent);
            }
            else
            {
                Intent broadCastIntent = new Intent();
                broadCastIntent.setAction(TRANSACTION_NOT_FOUND);
                context.sendBroadcast(broadCastIntent);
            }*/

            /* unpack the transaction result */
            /* this will return null of the event was not for us, or if there was an error */
            EFTServiceTransResult result = EFTServiceLib.unpackResult(context, intent);

            if (result != null)
            {

                Log.i(TAG, "Using EFTServiceLib version:" + EFTServiceLib.getVersion());

                boolean transFound = intent.getBooleanExtra("TransResponse", false);

                if (transFound)
                {
                    //  Unpack the transaction results
                    String rrn = result.getRrn();
                    boolean approved = result.isApproved();
                    Integer receiptNumber = result.getReceiptNumber();


                    // Debug the result
                    Log.i(TAG, "Transaction Type = " + result.getTransType());
                    if (intent.hasExtra("UTI"))
                        DashboardActivity.Companion.setLastReceivedUTI(intent.getStringExtra("UTI"));


                    Log.i(TAG, "Amount = " + result.getAmount());
                    Log.i(TAG, "MsgStatus= " + intent.getStringExtra("MsgStatus"));

                    // all the additional extra data you can get
                    Log.i(TAG, "Approved = " + intent.getBooleanExtra("Approved", false));
                    Log.i(TAG, "Cancelled = " + intent.getBooleanExtra("Cancelled", false));
                    Log.i(TAG, "LastStatus(FOR ADDITIONAL INFO ONLY) = " + intent.getStringExtra("LastStatus"));
                    Log.i(TAG, "SigRequired = " + intent.getBooleanExtra("SigRequired", false));
                    Log.i(TAG, "PINVerified = " + intent.getBooleanExtra("PINVerified", false));
                    Log.i(TAG, "AuthMode = " + intent.getStringExtra("AuthMode"));
                    Log.i(TAG, "Currency = " + intent.getStringExtra("Currency"));
                    Log.i(TAG, "Tid = " + intent.getStringExtra("Tid"));
                    Log.i(TAG, "Mid = " + intent.getStringExtra("Mid"));
                    Log.i(TAG, "Version = " + intent.getStringExtra("Version"));

                    if (intent.getBooleanExtra("TransactionDetails", false))
                    {
                        Log.i(TAG, "Transaction Details:");
                        Log.i(TAG, "FlightReference = " + intent.getStringExtra("FlightReference"));
                        Log.i(TAG, "ReceiptNumber = " + intent.getIntExtra("ReceiptNumber", 0));
                        Log.i(TAG, "RRN = " + intent.getStringExtra("RRN"));
                        Log.i(TAG, "ResponseCode = " + intent.getStringExtra("ResponseCode"));
                        Log.i(TAG, "Stan = " + intent.getIntExtra("Stan", 0));
                        Log.i(TAG, "AuthCode = " + intent.getStringExtra("AuthCode"));
                        Log.i(TAG, "MerchantTokenId = " + intent.getStringExtra("MerchantTokenId"));

                        if (intent.hasExtra("CardType"))
                        {

                            String cardType = intent.getStringExtra("CardType");
                            Log.i(TAG, "CardType = " + cardType);

                            if (cardType.compareTo("EMV") == 0 || cardType.compareTo("CTLS") == 0)
                            {

                                Log.i(TAG, "AID = " + intent.getStringExtra("AID"));
                                Log.i(TAG, "TSI = " + intent.getStringExtra("TSI"));
                                Log.i(TAG, "TVR = " + intent.getStringExtra("TVR"));
                                Log.i(TAG, "CardHolder = " + intent.getStringExtra("CardHolder"));
                                Log.i(TAG, "Cryptogram = " + byteArrayToHexString(intent.getByteArrayExtra("Cryptogram")));
                                Log.i(TAG, "CryptogramType = " + intent.getStringExtra("CryptogramType"));

                            }
                            Log.i(TAG, "PAN = " + intent.getStringExtra("PAN"));
                            Log.i(TAG, "ExpiryDate = " + intent.getStringExtra("ExpiryDate"));
                            Log.i(TAG, "StartDate = " + intent.getStringExtra("StartDate"));
                            Log.i(TAG, "Scheme = " + intent.getStringExtra("Scheme"));
                            Log.i(TAG, "PSN = " + intent.getIntExtra("PSN", 0));
                        }
                    }
                }
                else
                {
                    Log.i(TAG, "Transaction not found");
                }

                bringToFront(context,result);
            }
        }
    }

    public void bringToFront(Context context,EFTServiceTransResult result)
    {
        /* Bring this application back into the foreground */
        final Intent notificationIntent = new Intent(context, DashboardActivity.class);
        if(result!=null)
        {
            Gson gson = new Gson();
            String transResult = gson.toJson(result);
            notificationIntent.putExtra(AppUtils.BUNDLE_TRANSACTION_SUCCESS_FAILURE_DETAILS,transResult);
        }
        //notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(notificationIntent);
        Log.i(TAG, "Bring MainActivity to the front");
    }

    public String byteArrayToHexString(final byte[] byteArray)
    {
        if (byteArray == null)
        {
            return "";
            //throw new IllegalArgumentException("Argument 'byteArray' cannot be null");
        }
        int readBytes = byteArray.length;
        StringBuilder hexData = new StringBuilder();
        int onebyte;
        for (int i = 0; i < readBytes; i++)
        {
            onebyte = (0x000000ff & byteArray[i]) | 0xffffff00;
            hexData.append(Integer.toHexString(onebyte).substring(6));
        }
        return hexData.toString().toUpperCase();
    }

}
