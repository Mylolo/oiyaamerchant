package com.localvalu.merchant.optomany.fragment.adapter.pax;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.localvalu.coremerchantapp.data.output.FoodItem;
import com.localvalu.merchant.optomany.R;
import com.localvalu.merchant.optomany.fragment.adapter.viewholder.pax.OrderFoodItemsPrintVH;

import java.util.ArrayList;
import java.util.List;

public class OrderedFoodItemsPrintAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    private final List<FoodItem> orderedFoodItems;
    private Context context;

    public OrderedFoodItemsPrintAdapter(List<FoodItem> orderedFoodItems)
    {
        this.orderedFoodItems = new ArrayList<>(orderedFoodItems);
    }

    @Override
    public int getItemViewType(int position)
    {
        FoodItem foodItem = orderedFoodItems.get(position);
        return 0;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        this.context = parent.getContext();

        switch (viewType)
        {
            case 0:
            {
                View view = LayoutInflater.from(context).inflate(R.layout.view_ordered_items_print, parent, false);
                return new OrderFoodItemsPrintVH(context, view);
            }

            default:
            {
                return null;
            }
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position)
    {
        holder.setIsRecyclable(false);

        if (holder instanceof OrderFoodItemsPrintVH)
        {
            OrderFoodItemsPrintVH viewHolder = (OrderFoodItemsPrintVH) holder;
            viewHolder.populateData(orderedFoodItems.get(position));
        }
    }

    @Override
    public int getItemCount()
    {
        return this.orderedFoodItems.size();
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    public FoodItem removeItem(int position)
    {
        FoodItem foodItem = this.orderedFoodItems.remove(position);
        notifyItemRemoved(position);

        return foodItem;
    }

    public void addItem(int position, FoodItem foodItem)
    {
        this.orderedFoodItems.add(position, foodItem);
        notifyItemInserted(position);
    }

    public void addItem(FoodItem foodItem)
    {
        this.orderedFoodItems.add(foodItem);
        notifyItemInserted(this.orderedFoodItems.size());
    }

    public void addItems(List<FoodItem> orderedFoodItems)
    {
        this.orderedFoodItems.clear();
        this.orderedFoodItems.addAll(orderedFoodItems);
        notifyDataSetChanged();
    }

    public void moveItem(int fromPosition, int toPosition)
    {
        final FoodItem foodItem = this.orderedFoodItems.remove(fromPosition);

        this.orderedFoodItems.add(toPosition, foodItem);
        notifyItemMoved(fromPosition, toPosition);
    }
}
