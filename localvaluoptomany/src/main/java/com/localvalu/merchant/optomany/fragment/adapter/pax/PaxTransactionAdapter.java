package com.localvalu.merchant.optomany.fragment.adapter.pax;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.localvalu.merchant.optomany.R;
import com.localvalu.merchant.optomany.data.pax.Transaction;
import com.localvalu.merchant.optomany.fragment.adapter.viewholder.pax.PaxTransactionVH;
import com.localvalu.merchant.optomany.listeners.PaxTransactionItemClickListener;


import java.util.ArrayList;
import java.util.List;

public class PaxTransactionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    private List<Transaction> paxTransactions;

    private Context context;

    private PaxTransactionItemClickListener paxTransactionItemClickListener;

    public PaxTransactionAdapter(List<Transaction> paxTransactions)
    {
        this.paxTransactions = new ArrayList<>(paxTransactions);
    }

    @Override
    public int getItemViewType(int position)
    {
        Transaction Transaction = paxTransactions.get(position);
        return 0;
    }

    public void setPaxTransactionItemClickListener(PaxTransactionItemClickListener paxTransactionItemClickListener)
    {
        this.paxTransactionItemClickListener = paxTransactionItemClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        this.context = parent.getContext();

        switch (viewType)
        {
            case 0:
            {
                View view = LayoutInflater.from(context).inflate(R.layout.view_item_pax_transaction_options, parent, false);
                return new PaxTransactionVH(context, view);
            }

            default:
            {
                return null;
            }
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position)
    {
        holder.setIsRecyclable(false);

        if (holder instanceof PaxTransactionVH)
        {
            PaxTransactionVH viewHolder = (PaxTransactionVH) holder;
            viewHolder.populateData(paxTransactions.get(position));
            viewHolder.itemView.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if(paxTransactionItemClickListener!=null)
                    {
                        paxTransactionItemClickListener.onPaxTransactionItemClicked(position);
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount()
    {
        return this.paxTransactions.size();
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    public Transaction removeItem(int position)
    {
        Transaction Transaction = this.paxTransactions.remove(position);
        notifyItemRemoved(position);

        return Transaction;
    }

    public void addItem(int position, Transaction transaction)
    {
        this.paxTransactions.add(position, transaction);
        notifyItemInserted(position);
    }

    public void addItem(Transaction transaction)
    {
        this.paxTransactions.add(transaction);
        notifyItemInserted(this.paxTransactions.size());
    }

    public void addItems(List<Transaction> paxTransactions)
    {
        this.paxTransactions.clear();
        this.paxTransactions.addAll(paxTransactions);
        notifyDataSetChanged();
    }

    public void moveItem(int fromPosition, int toPosition)
    {
        final Transaction transaction = this.paxTransactions.remove(fromPosition);

        this.paxTransactions.add(toPosition, transaction);
        notifyItemMoved(fromPosition, toPosition);
    }
}
