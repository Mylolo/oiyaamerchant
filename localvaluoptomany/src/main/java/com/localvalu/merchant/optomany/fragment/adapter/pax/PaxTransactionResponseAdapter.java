package com.localvalu.merchant.optomany.fragment.adapter.pax;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.localvalu.merchant.optomany.R;
import com.localvalu.merchant.optomany.data.pax.Transaction;
import com.localvalu.merchant.optomany.data.pax.TransactionResponse;
import com.localvalu.merchant.optomany.fragment.adapter.viewholder.pax.PaxTransactionResponseVH;

import java.util.ArrayList;
import java.util.List;

public class PaxTransactionResponseAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    private List<TransactionResponse> paxTransactionResponseList;

    private Context context;

    public PaxTransactionResponseAdapter(List<TransactionResponse> paxTransactionResponseList)
    {
        this.paxTransactionResponseList = paxTransactionResponseList;
    }

    @Override
    public int getItemViewType(int position)
    {
        return 0;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        this.context = parent.getContext();

        switch (viewType)
        {
            case 0:
            {
                View view = LayoutInflater.from(context).inflate(R.layout.view_item_transaction_and_status_response, parent, false);
                return new PaxTransactionResponseVH(context, view);
            }

            default:
            {
                return null;
            }
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position)
    {
        holder.setIsRecyclable(false);

        if (holder instanceof PaxTransactionResponseVH)
        {
            PaxTransactionResponseVH viewHolder = (PaxTransactionResponseVH) holder;
            viewHolder.populateData(paxTransactionResponseList.get(position));
        }
    }

    @Override
    public int getItemCount()
    {
        if(paxTransactionResponseList!=null)
        {
            if(paxTransactionResponseList.size()>0)
            {
                return this.paxTransactionResponseList.size();
            }
            else return 0;
        }
        else return 0;
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    public TransactionResponse removeItem(int position)
    {
        TransactionResponse transactionResponse = this.paxTransactionResponseList.remove(position);
        notifyItemRemoved(position);

        return transactionResponse;
    }

    public void addItem(int position, TransactionResponse transactionResponse)
    {
        this.paxTransactionResponseList.add(position, transactionResponse);
        notifyItemInserted(position);
    }

    public void addItem(TransactionResponse transactionResponse)
    {
        this.paxTransactionResponseList.add(transactionResponse);
        notifyItemInserted(this.paxTransactionResponseList.size());
    }

    public void addItems(List<TransactionResponse> paxTransactionResponse)
    {
        this.paxTransactionResponseList.clear();
        this.paxTransactionResponseList.addAll(paxTransactionResponse);
        notifyDataSetChanged();
    }

    public void moveItem(int fromPosition, int toPosition)
    {
        final TransactionResponse Transaction = this.paxTransactionResponseList.remove(fromPosition);

        this.paxTransactionResponseList.add(toPosition, Transaction);
        notifyItemMoved(fromPosition, toPosition);
    }
}
