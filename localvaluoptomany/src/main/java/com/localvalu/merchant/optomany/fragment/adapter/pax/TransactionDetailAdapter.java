package com.localvalu.merchant.optomany.fragment.adapter.pax;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.localvalu.coremerchantapp.utils.AppUtils;
import com.localvalu.merchant.optomany.fragment.pax.PaxTransactionResponseFragment;

import org.jetbrains.annotations.NotNull;

public class TransactionDetailAdapter extends FragmentStateAdapter
{
    private Bundle bundle;
    private String[] titles;

    public TransactionDetailAdapter(FragmentManager fm, Lifecycle lifecycle, Bundle bundle, String[] titles)
    {
        super(fm,lifecycle);
        this.bundle = bundle;
        this.titles =titles;
    }

    @NonNull
    @NotNull
    @Override
    public Fragment createFragment(int position)
    {
        Bundle bundleTemp = new Bundle();
        switch (position)
        {
            case 0:
                bundleTemp.putParcelableArrayList(AppUtils.BUNDLE_RESPONSE_LIST_COMMON,
                        bundle.getParcelableArrayList(AppUtils.BUNDLE_TRANSACTION_RESPONSE_LIST));
                break;
            case 1:
                bundleTemp.putParcelableArrayList(AppUtils.BUNDLE_RESPONSE_LIST_COMMON,
                        bundle.getParcelableArrayList(AppUtils.BUNDLE_TRANSACTION_STATUS_RESPONSE_LIST));
                break;
        }
        PaxTransactionResponseFragment fragment = new PaxTransactionResponseFragment();
        fragment.setArguments(bundleTemp);
        return fragment;
    }

    @Override
    public int getItemCount()
    {
        return 2;
    }
}
