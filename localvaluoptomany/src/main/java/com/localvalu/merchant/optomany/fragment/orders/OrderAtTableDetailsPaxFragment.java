package com.localvalu.merchant.optomany.fragment.orders;

import static com.localvalu.coremerchantapp.LocalValuConstants.PREF_SIGN_IN_RESULT;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.google.gson.Gson;
import com.localvalu.coremerchantapp.BuildConfig;
import com.localvalu.coremerchantapp.LocalValuFragment;
import com.localvalu.coremerchantapp.data.output.SignInResult;
import com.localvalu.coremerchantapp.data.output.order.OrderDetails;
import com.localvalu.coremerchantapp.data.output.order.OrderDetailsResult;
import com.localvalu.coremerchantapp.data.output.order.OrderStatusChangeResult;
import com.localvalu.coremerchantapp.fragment.adapter.order.OrderedFoodItemsAdapter;
import com.localvalu.coremerchantapp.fragment.orders.MyOrdersFragment;
import com.localvalu.coremerchantapp.handler.DashBoardHandler;
import com.localvalu.coremerchantapp.utils.AppConstants;
import com.localvalu.coremerchantapp.utils.AppUtils;
import com.localvalu.merchant.optomany.R;
import com.localvalu.merchant.optomany.activity.handler.PaxHandler;
import com.utils.base.Task;
import com.utils.helper.preference.PreferenceFactory;
import com.utils.helper.recyclerview.RVItemAnimator;
import com.utils.helper.recyclerview.RVItemDecorator;
import com.utils.util.DateTimeUtil;
import com.utils.validator.Validator;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Status 3 -- Waiting for confirm
 * Status 2 -- Declined
 * Status 1 -- Delivered
 * Status 0 -- Accept
 * Status 20 -- Print
 */
public class OrderAtTableDetailsPaxFragment extends LocalValuFragment
{
    private static final String TAG = OrderAtTableDetailsPaxFragment.class.getSimpleName();

    private DashBoardHandler dashBoardHandler;
    private PaxHandler paxHandler;
    private String orderId;
    private AppCompatTextView tvCustomerName,tvOrderId,tvOrderDate,tvOrderStatus,tvTextPaymentStatus,
            tvPaymentStatus,tvSpecialInstructions,tvAllergyInstructions,tvNoOfItems;
    private AppCompatTextView tvSubTotal,tvTokenApplied,tvBalanceToPay,tvDeliveryFee,tvFinalPay,tvTableId,tvDescription,tvTextTableId;
    private RecyclerView rvOrderedItems;
    private ConstraintLayout cnlOrderEntryProcess;
    private MaterialButton btnAccept,btnReject,btnCompleted,btnPrint,btnMore;
    private OrderDetails orderDetails;
    private OrderedFoodItemsAdapter orderedFoodItemsAdapter;
    private String strCurrencySymbol,strCurrencyLetter;
    private BroadcastReceiver br;
    private String previousTitle;
    private boolean statusUpdated=false;
    private SignInResult signInResult;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();

        if(bundle!=null)
        {
            orderId = bundle.getString(AppUtils.BUNDLE_ORDER_ID);
        }

        strCurrencySymbol=getString(R.string.symbol_pound);
        strCurrencyLetter=getString(R.string.currency_letter_lt);
        setSignInResult();
    }

    private void setSignInResult()
    {
        try
        {
            String strSignInResult = PreferenceFactory.getInstance().getString(PREF_SIGN_IN_RESULT);
            if (strSignInResult != null)
            {
                if (strSignInResult != "")
                {
                    Gson gson = new Gson();
                    signInResult = gson.fromJson(strSignInResult, SignInResult.class);
                }
            }
            if (signInResult != null)
            {
                if (BuildConfig.DEBUG)
                {
                    Log.d(TAG, "signInResult->" + signInResult.getDriverType());
                }
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_order_at_table_details, container, false);
        setHasOptionsMenu(true);
        setRetainInstance(true);
        initView(view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        setProperties();
        doGetOrderDetails();
    }

    private void initView(View view)
    {
        tvCustomerName = view.findViewById(R.id.tvCustomerName);
        tvOrderId = view.findViewById(R.id.tvOrderId);
        tvOrderDate = view.findViewById(R.id.tvOrderDate);
        tvOrderStatus = view.findViewById(R.id.tvOrderStatus);
        tvTextPaymentStatus = view.findViewById(R.id.tvTextPaymentStatus);
        tvPaymentStatus = view.findViewById(R.id.tvPaymentStatus);
        tvSpecialInstructions = view.findViewById(R.id.tvSpecialInstructions);
        tvAllergyInstructions = view.findViewById(R.id.tvAllergyInstructions);
        tvNoOfItems = view.findViewById(R.id.tvNoOfItems);
        rvOrderedItems = view.findViewById(R.id.rvOrderedItems);
        tvAllergyInstructions = view.findViewById(R.id.tvAllergyInstructions);
        cnlOrderEntryProcess = view.findViewById(R.id.cnlOrderEntryProcess);
        tvSubTotal = view.findViewById(R.id.tvSubTotal);
        tvTokenApplied = view.findViewById(R.id.tvTokenApplied);
        tvBalanceToPay = view.findViewById(R.id.tvBalanceToPay);
        tvDeliveryFee = view.findViewById(R.id.tvDeliveryFee);
        tvFinalPay = view.findViewById(R.id.tvFinalPay);
        btnAccept = view.findViewById(R.id.btnAccept);
        btnReject = view.findViewById(R.id.btnReject);
        btnCompleted = view.findViewById(R.id.btnCompleted);
        btnPrint = view.findViewById(R.id.btnPrint);
        btnMore  = view.findViewById(R.id.btnMore);
        tvTextTableId = view.findViewById(R.id.tvTextTableId);
        tvTableId = view.findViewById(R.id.tvTableId);
        tvDescription = view.findViewById(R.id.tvDescription);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rvOrderedItems = view.findViewById(R.id.rvOrderedItems);
        rvOrderedItems.setLayoutManager(linearLayoutManager);
        rvOrderedItems.setItemAnimator(new RVItemAnimator());

        Drawable drawable = getResources().getDrawable(R.drawable.list_divider);

        if(drawable!=null)
        {
            rvOrderedItems.addItemDecoration(new RVItemDecorator(drawable));
        }

        btnPrint.setVisibility(View.VISIBLE);
        btnMore.setVisibility(View.VISIBLE);
    }

    private void setProperties()
    {
        if(Validator.isValid(orderedFoodItemsAdapter))
            rvOrderedItems.setAdapter(orderedFoodItemsAdapter);
        btnAccept.setOnClickListener(_OnClickListener);
        btnReject.setOnClickListener(_OnClickListener);
        btnCompleted.setOnClickListener(_OnClickListener);
        btnPrint.setOnClickListener(_OnClickListener);
        btnMore.setOnClickListener(_OnClickListener);

        tvTextPaymentStatus.setVisibility(View.GONE);
        tvPaymentStatus.setVisibility(View.GONE);
        if(signInResult.getRetailerType().equals(AppConstants.RETAILER_TYPE_EATS))
            tvTextTableId.setText(getString(com.localvalu.coremerchantapp.R.string.lbl_table_id));
        else tvTextTableId.setText(getString(com.localvalu.coremerchantapp.R.string.lbl_store_id));
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);

        dashBoardHandler = (DashBoardHandler) context;

        paxHandler = (PaxHandler) context;

    }

    @Override
    public void onDetach()
    {
        paxHandler = null;
        dashBoardHandler = null;
        super.onDetach();
    }

    View.OnClickListener _OnClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            switch (v.getId())
            {
                case R.id.btnAccept:
                     doAccept();
                     break;
                case R.id.btnReject:
                     doReject();
                     break;
                case R.id.btnCompleted:
                     doComplete();
                     break;
                case R.id.btnPrint:
                     doPrint();
                     break;
                case R.id.btnMore:
                     doMore();
                     break;
            }
        }
    };

    private OrderDetails dummyData()
    {

        OrderDetails orderListData = new OrderDetails();
        orderListData.setOrderId("1");
        orderListData.setBusinessId("2");
        orderListData.setOrderTotal("125");
        orderListData.setIsOrderConfirmedByRest("1");
        orderListData.setCustomerName("Test");
        orderListData.setCurrentStatus("0");
        orderListData.setAllergyInstructions("None");
        orderListData.setSpecialInstructions("None");
        orderListData.setStatus("1");
        orderListData.setCreatedDate("2020-02-25 14:09:12");
        orderListData.setDateTime("2020-02-22 14:09:12");
        orderListData.setCustomerMobile("88999000033");
        orderListData.setIsOrderPlaced("1");
        orderListData.setFinalOrderTotal("12");

        return orderListData;
    }

    private void doGetOrderDetails()
    {
        //Log.d(TAG, "doGetOrderDetails: order Id - " + orderId);

        //orderDetails=dummyData();
        setData();
        dashBoardHandler.doGetOrderAtTableDetails(orderId, new Task<OrderDetailsResult>()
        {
            @Override
            public void onSuccess(OrderDetailsResult result)
            {
                if (Validator.isValid(result.getOrderDetails()))
                {
                    orderDetails=result.getOrderDetails().get(0);
                    setData();
                    if(statusUpdated)
                    {
                        FragmentManager fragmentManager = requireActivity().getSupportFragmentManager();

                        if(fragmentManager!=null)
                        {

                            for(Fragment fragment : fragmentManager.getFragments())
                            {
                                if(fragment instanceof MyOrdersFragment)
                                {
                                    MyOrdersFragment myOrdersFragment = (MyOrdersFragment) fragment;

                                    if(myOrdersFragment!=null)
                                    {
                                        myOrdersFragment.updateStatusInOrderAtTable(result.getOrderDetails().get(0).getStatus());
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {

                }
            }

            @Override
            public void onError(String message)
            {

            }
        });
    }

    private void setData()
    {
        if(orderDetails!=null)
        {
            tvCustomerName.setText(orderDetails.getCustomerName());
            tvOrderId.setText(orderDetails.getOrderId());
            tvAllergyInstructions.setText(orderDetails.getAllergyInstructions());
            tvSpecialInstructions.setText(orderDetails.getSpecialInstructions());
            tvPaymentStatus.setText(orderDetails.getPaymentStatus());
            tvTableId.setText(orderDetails.getTableId());
            tvDescription.setText(orderDetails.getDescription());

            if(signInResult.getRetailerType().equals(AppConstants.RETAILER_TYPE_EATS))
                tvTableId.setText(orderDetails.getTableId());
            else tvTableId.setText(orderDetails.getStoreId());

            setPaymentDetails();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            try
            {
                Date orderDate = dateFormat.parse(orderDetails.getDateTime());
                String strDate = DateTimeUtil.getFormattedDate(orderDate.getTime(), DateTimeUtil.dd_MM_yyyy);
                String strTime = DateTimeUtil.getFormattedDate(orderDate.getTime(), DateTimeUtil.hh_mm_a);
                StringBuilder strDateTime = new StringBuilder();
                strDateTime.append(strDate).append(" ").append(strTime);
                tvOrderDate.setText(strDateTime.toString());
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
            if(orderDetails.getStatus()!=null)
            {
                if(orderDetails.getStatus().length()>0)
                {
                    try
                    {
                        setOrderStatus(orderDetails.getStatus());
                    }
                    catch (Exception ex)
                    {
                        ex.printStackTrace();
                    }
                }
            }
            setAdapter();
            showHideBottom();
        }
    }

    private void setPaymentDetails()
    {
        try
        {
            double orderTotal = (Double.parseDouble(orderDetails.getOrderTotal()));
            String strOrderTotal = String.format("%,.2f",orderTotal);
            StringBuilder strSubTotal = new StringBuilder();
            strSubTotal.append(strCurrencySymbol).append(strOrderTotal);
            tvSubTotal.setText(strSubTotal.toString());

            double discountAmount = (Double.parseDouble(orderDetails.getDiscountLtAmount()));
            String strDiscountAmount = String.format("%,.2f",discountAmount);
            StringBuilder strTokenApply = new StringBuilder();
            strTokenApply.append("-").append(strCurrencyLetter).
                    append(strDiscountAmount);
            tvTokenApplied.setText(strTokenApply.toString());

            double deliveryCharges = (Double.parseDouble(orderDetails.getDeliveryRate()));
            String strDeliveryCharges = String.format("%,.2f",deliveryCharges);
            StringBuilder strDeliveryChargesTwo= new StringBuilder();
            strDeliveryChargesTwo.append("+").append(strCurrencySymbol).append(strDeliveryCharges);
            tvDeliveryFee.setText(strDeliveryChargesTwo.toString());

            double finalOrderTotal = (Double.parseDouble(orderDetails.getFinalOrderTotal()));
            String strFinalOrderTotal = String.format("%,.2f",finalOrderTotal);
            StringBuilder strFinalPay= new StringBuilder();
            strFinalPay.append(strCurrencySymbol).append(strFinalOrderTotal);
            tvFinalPay.setText(strFinalPay.toString());
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

    }

    private void showHideBottom()
    {
        switch (orderDetails.getStatus())
        {
            case AppConstants.PENDING:
                 cnlOrderEntryProcess.setVisibility(View.VISIBLE);
                 btnCompleted.setVisibility(View.GONE);
                 showHidePrintButton(false);
                 showHideMoreButton();
                 break;
            case AppConstants.DECLINED:
                 cnlOrderEntryProcess.setVisibility(View.GONE);
                 btnCompleted.setVisibility(View.GONE);
                 showHidePrintButton(false);
                 showHideMoreButton();
                 break;
            case AppConstants.ORDER_CANCELLED:
                 cnlOrderEntryProcess.setVisibility(View.GONE);
                 btnCompleted.setVisibility(View.GONE);
                 showHidePrintButton(false);
                 btnMore.setVisibility(View.GONE);
                 break;
            case AppConstants.DELIVERED:
                 btnCompleted.setVisibility(View.GONE);
                 cnlOrderEntryProcess.setVisibility(View.GONE);
                 showHidePrintButton(true);
                 showHideMoreButton();
                 break;
            case AppConstants.ACCEPT:
                 btnCompleted.setVisibility(View.VISIBLE);
                 cnlOrderEntryProcess.setVisibility(View.GONE);
                 showHidePrintButton(true);
                 showHideMoreButton();
                 break;
        }
    }

    private void showHidePrintButton(boolean show)
    {
        if(show) btnPrint.setVisibility(View.VISIBLE);
        else btnPrint.setVisibility(View.GONE);
    }

    private void showHideMoreButton()
    {
        if(orderDetails.getPaymentStatus()!=null)
        {
            switch (orderDetails.getPaymentStatus())
            {
                case AppConstants.PAID:
                     btnMore.setVisibility(View.GONE);
                     break;
                case AppConstants.NOT_PAID:
                     btnMore.setVisibility(View.VISIBLE);
                     break;
            }
        }

    }

    private void setAdapter()
    {
        if (Validator.isValid(orderDetails.getFoodItems()) && !orderDetails.getFoodItems().isEmpty())
        {
            if (Validator.isValid(orderedFoodItemsAdapter))
            {
                orderedFoodItemsAdapter.submitList(orderDetails.getFoodItems());
            }
            else
            {
                orderedFoodItemsAdapter = new OrderedFoodItemsAdapter();
                rvOrderedItems.setAdapter(orderedFoodItemsAdapter);
            }
        }
        else
        {
            if (Validator.isValid(orderedFoodItemsAdapter))
            {
                orderedFoodItemsAdapter.submitList(new ArrayList<>());
            }
            else
            {
                orderedFoodItemsAdapter = new OrderedFoodItemsAdapter();
                rvOrderedItems.setAdapter(orderedFoodItemsAdapter);
            }
        }
    }

    private void setOrderStatus(String status)
    {
        switch (status)
        {
            case AppConstants.ACCEPT:
                 tvOrderStatus.setText(getString(R.string.lbl_accepted));
                 tvOrderStatus.setTextColor(getResources().getColor(R.color.greenDark));
                 tvNoOfItems.setText(getString(R.string.lbl_ordered_items).toUpperCase());
                 break;
            case AppConstants.DELIVERED:
                 tvOrderStatus.setText(getString(R.string.lbl_delivered));
                 tvOrderStatus.setTextColor(getResources().getColor(R.color.greenDark));
                 tvNoOfItems.setText(getString(R.string.lbl_ordered_items).toUpperCase());
                 break;
            case AppConstants.DECLINED:
                 tvOrderStatus.setText(R.string.lbl_declined);
                 tvOrderStatus.setTextColor(getResources().getColor(R.color.redDark));
                 tvNoOfItems.setText(getString(R.string.lbl_declined_ordered_items).toUpperCase());
                 break;
            case AppConstants.ORDER_CANCELLED:
                 tvOrderStatus.setText(R.string.lbl_cancelled);
                 tvOrderStatus.setTextColor(getResources().getColor(R.color.redDark));
                 tvNoOfItems.setText(getString(R.string.lbl_declined_ordered_items).toUpperCase());
                 break;
            case AppConstants.PENDING:
                 tvOrderStatus.setText(R.string.lbl_pending);
                 tvOrderStatus.setTextColor(getResources().getColor(R.color.orangeDark));
                 tvNoOfItems.setText(getString(R.string.lbl_ordered_items).toUpperCase());
                 break;
        }
    }

    private void doAccept()
    {
        dashBoardHandler.doChangeOrderStatus(orderId, AppConstants.ACCEPT, new Task<OrderStatusChangeResult>()
        {
            @Override
            public void onSuccess(OrderStatusChangeResult result)
            {
                statusUpdated=true;
                doGetOrderDetails();
            }

            @Override
            public void onError(String message)
            {

            }
        });
    }

    private void doReject()
    {
        dashBoardHandler.doChangeOrderStatus(orderId, AppConstants.DECLINED, new Task<OrderStatusChangeResult>()
        {
            @Override
            public void onSuccess(OrderStatusChangeResult result)
            {
                statusUpdated=true;
                doGetOrderDetails();
            }

            @Override
            public void onError(String message)
            {

            }
        });
    }

    private void doComplete()
    {
        dashBoardHandler.doChangeOrderStatus(orderId, AppConstants.DELIVERED, new Task<OrderStatusChangeResult>()
        {
            @Override
            public void onSuccess(OrderStatusChangeResult result)
            {
                statusUpdated=true;
                doGetOrderDetails();
            }

            @Override
            public void onError(String message)
            {

            }
        });
    }

    private void doPrint()
    {
        paxHandler.printFragment(orderDetails);
    }

    private void doMore()
    {
        double payableAmount = Double.parseDouble(orderDetails.getFinalOrderTotal());
        payableAmount = payableAmount * 100 / 100;
        payableAmount = payableAmount * 100;
        int payableAmountInMinorUnits = (int) payableAmount;
        Bundle bundle = new Bundle();
        bundle.putString(AppUtils.BUNDLE_PAYABLE_AMOUNT,Integer.toString(payableAmountInMinorUnits));
        bundle.putString(AppUtils.BUNDLE_CASHBACK_AMOUNT,"0");
        bundle.putString(AppUtils.BUNDLE_DISPLAY_PAYABLE_AMOUNT,orderDetails.getFinalOrderTotal());
        bundle.putString(AppUtils.BUNDLE_DISPLAY_CASHBACK_AMOUNT,"0");
        bundle.putString(AppUtils.BUNDLE_LAST_RECEIVED_UTI,"0");

        paxHandler.paxTransactionFragment(bundle);

    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        //getLocalValuActivity().unregisterReceiver(br);
        dashBoardHandler.setPreviousTitle();
    }

}
