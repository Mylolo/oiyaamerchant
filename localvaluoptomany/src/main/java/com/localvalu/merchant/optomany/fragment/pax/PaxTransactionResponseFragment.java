package com.localvalu.merchant.optomany.fragment.pax;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.localvalu.coremerchantapp.LocalValuFragment;
import com.localvalu.coremerchantapp.handler.DashBoardHandler;
import com.localvalu.coremerchantapp.utils.AppUtils;
import com.localvalu.merchant.optomany.R;
import com.localvalu.merchant.optomany.data.pax.TransactionResponse;
import com.localvalu.merchant.optomany.fragment.adapter.pax.PaxTransactionResponseAdapter;
import com.utils.helper.recyclerview.RVItemAnimator;

import java.util.List;


public class PaxTransactionResponseFragment extends LocalValuFragment
{

    private static final String TAG = PaxTransactionResponseFragment.class.getSimpleName();

    private RecyclerView rvTransactionResponse;
    private AppCompatTextView tvEmpty;
    private PaxTransactionResponseAdapter paxTransactionResponseAdapter;
    private List<TransactionResponse> responseList;
    private DashBoardHandler dashBoardHandler;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();

        if(bundle!=null)
        {
            responseList = bundle.getParcelableArrayList(AppUtils.BUNDLE_RESPONSE_LIST_COMMON);
            Log.d(TAG, "onCreate: "+responseList.size());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_pax_transaction_details_sub, container, false);
        setHasOptionsMenu(true);
        setRetainInstance(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rvTransactionResponse = view.findViewById(R.id.rvTransactionResponse);
        tvEmpty = view.findViewById(R.id.tvEmpty);
        rvTransactionResponse.setLayoutManager(linearLayoutManager);
        rvTransactionResponse.setItemAnimator(new RVItemAnimator());
        rvTransactionResponse.addItemDecoration(new DividerItemDecoration(getContext(), linearLayoutManager.getOrientation()));
        rvTransactionResponse.setHasFixedSize(true);

        //if (Validator.isValid(rvTransactionResponse))
            //rvTransactionResponse.setAdapter(paxTransactionResponseAdapter);

        doGetPaxTransactionResponseList();

        return view;
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);

        dashBoardHandler = (DashBoardHandler) context;

    }

    @Override
    public void onDetach()
    {
        super.onDetach();

        dashBoardHandler = null;
    }

    private void doGetPaxTransactionResponseList()
    {
        if(responseList!=null)
        {
            setPaxTransactionResponseAdapter();
            showListView();
        }

        else
        {
            showEmptyView(getString(R.string.lbl_no_details));
        }
    }

    private void showEmptyView(String message)
    {
        tvEmpty.setText(message);
        rvTransactionResponse.setVisibility(View.GONE);
        tvEmpty.setVisibility(View.VISIBLE);
    }

    private void showListView()
    {
        rvTransactionResponse.setVisibility(View.VISIBLE);
        tvEmpty.setVisibility(View.GONE);
    }

    private void setPaxTransactionResponseAdapter()
    {

        paxTransactionResponseAdapter = new PaxTransactionResponseAdapter(responseList);
        rvTransactionResponse.setAdapter(paxTransactionResponseAdapter);
    }

    @Override
    public void onDestroy()
    {
        dashBoardHandler.setPreviousTitle();
        super.onDestroy();
    }

}
