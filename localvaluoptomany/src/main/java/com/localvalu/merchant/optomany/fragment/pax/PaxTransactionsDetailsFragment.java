package com.localvalu.merchant.optomany.fragment.pax;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import com.localvalu.coremerchantapp.LocalValuFragment;
import com.localvalu.coremerchantapp.handler.DashBoardHandler;
import com.localvalu.merchant.optomany.R;
import com.localvalu.merchant.optomany.activity.handler.PaxHandler;
import com.localvalu.merchant.optomany.fragment.adapter.pax.TransactionDetailAdapter;


public class PaxTransactionsDetailsFragment extends LocalValuFragment
{

    private static final String TAG = PaxTransactionsDetailsFragment.class.getSimpleName();

    private DashBoardHandler dashBoardHandler;

    private PaxHandler paxHandler;

    private Bundle bundleTransactionDetails;

    private TabLayout tabs;

    private ViewPager2 vpPaxTransactionDetails;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();

        if (bundle != null)
        {
            bundleTransactionDetails = bundle;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_pax_transaction_details_main, container, false);
        setHasOptionsMenu(true);

        tabs = view.findViewById(R.id.tabs);
        vpPaxTransactionDetails = view.findViewById(R.id.vpPaxTransactionDetails);

        setProperties();

        return view;
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);

        dashBoardHandler = (DashBoardHandler) context;

        paxHandler = (PaxHandler) context;
    }

    @Override
    public void onDetach()
    {
        dashBoardHandler =null;
        paxHandler = null;
        super.onDetach();
    }

    private void setProperties()
    {
        String[] titles = new String[2];
        titles[0] = getString(R.string.lbl_view_reports);
        titles[1] = getString(R.string.lbl_view_status);
        vpPaxTransactionDetails.setAdapter(new TransactionDetailAdapter(getChildFragmentManager(),getLifecycle(),
                bundleTransactionDetails,titles));
        TabLayoutMediator tabLayoutMediator = new TabLayoutMediator(tabs, vpPaxTransactionDetails, true, new TabLayoutMediator.TabConfigurationStrategy() {
            @Override
            public void onConfigureTab(TabLayout.Tab tab, int position) {
                // position of the current tab and that tab
            }
        });
        tabLayoutMediator.attach();
    }

    @Override
    public void onDestroy()
    {
        dashBoardHandler.setPreviousTitle();
        super.onDestroy();
    }

}
