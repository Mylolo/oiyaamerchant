package com.localvalu.merchant.optomany.fragment.pax;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.localvalu.coremerchantapp.LocalValuFragment;
import com.localvalu.coremerchantapp.handler.DashBoardHandler;
import com.localvalu.coremerchantapp.utils.AppUtils;
import com.localvalu.merchant.optomany.R;
import com.localvalu.merchant.optomany.activity.DashboardActivity;
import com.localvalu.merchant.optomany.activity.handler.PaxHandler;
import com.localvalu.merchant.optomany.data.pax.Transaction;
import com.localvalu.merchant.optomany.fragment.adapter.pax.PaxTransactionAdapter;
import com.localvalu.merchant.optomany.listeners.PaxTransactionItemClickListener;
import com.localvalu.merchant.optomany.utils.PaxConstants;
import com.pax.dal.IDAL;
import com.pax.neptunelite.api.NeptuneLiteUser;
import com.utils.helper.recyclerview.RVItemAnimator;
import com.utils.validator.Validator;

import java.util.ArrayList;
import java.util.List;

import eft.com.eftservicelib.EFTServiceLib;
import eft.com.eftservicelib.EFTServiceTransResult;

import static com.localvalu.coremerchantapp.utils.AppUtils.BROADCAST_ACTION;
import static com.localvalu.coremerchantapp.utils.AppUtils.BUNDLE_TRANSACTION_SUCCESS_FAILURE_DETAILS;
import static com.localvalu.merchant.optomany.components.OptomanyReceiver.TRANSACTION_NOT_FOUND;
import static com.localvalu.merchant.optomany.components.OptomanyReceiver.TRANSACTION_RESULT_EVENT;
import static eft.com.eftservicelib.EFTServiceLib.TRANSACTION_TYPE_REFUND;
import static eft.com.eftservicelib.EFTServiceLib.TRANSACTION_TYPE_SALE;


public class PaxTransactionsFragment extends LocalValuFragment implements PaxTransactionItemClickListener
{

    private static final String TAG = PaxTransactionsFragment.class.getSimpleName();

    private RecyclerView rvPAXList;
    private AppCompatTextView tvLblAmount, tvAmount, tvEmpty;
    private PaxTransactionAdapter paxTransactionAdapter;
    private List<Transaction> transactionList;
    private String payableAmount, cashBackAmount;
    private String displayPayableAmount, displayCashbackAmount, lastReceivedUTI;
    private DashBoardHandler dashBoardHandler;
    private PaxHandler paxHandler;
    private IDAL iPaxDal;
    private OiyaaOptomanyReceiver oiyaaOptomanyReceiver;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();

        if (bundle != null)
        {
            payableAmount = bundle.getString(AppUtils.BUNDLE_PAYABLE_AMOUNT);
            cashBackAmount = bundle.getString(AppUtils.BUNDLE_CASHBACK_AMOUNT);
            displayPayableAmount = bundle.getString(AppUtils.BUNDLE_DISPLAY_PAYABLE_AMOUNT);
            displayCashbackAmount = bundle.getString(AppUtils.BUNDLE_DISPLAY_CASHBACK_AMOUNT);
            lastReceivedUTI = bundle.getString(AppUtils.BUNDLE_LAST_RECEIVED_UTI);
        }

        try
        {
            iPaxDal = NeptuneLiteUser.getInstance().getDal(requireContext());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_pax_transactions, container, false);
        setHasOptionsMenu(true);
        setRetainInstance(true);

        tvEmpty = view.findViewById(R.id.tvEmpty);
        tvAmount = view.findViewById(R.id.tvAmount);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rvPAXList = view.findViewById(R.id.rvPAXList);
        rvPAXList.setLayoutManager(linearLayoutManager);
        rvPAXList.setItemAnimator(new RVItemAnimator());
        rvPAXList.addItemDecoration(new DividerItemDecoration(getContext(), linearLayoutManager.getOrientation()));
        rvPAXList.setHasFixedSize(true);

        if (Validator.isValid(paxTransactionAdapter))
            rvPAXList.setAdapter(paxTransactionAdapter);

        doGetPaxTransactionList();

        setAmount();

        return view;
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);
        dashBoardHandler = (DashBoardHandler) context;
        paxHandler = (PaxHandler) context;
    }

    @Override
    public void onDetach()
    {
        dashBoardHandler=null;
        paxHandler = null;
        super.onDetach();
    }

    /**
     * This method is responsible to register an action to BroadCastReceiver
     * */
    private void registerMyReceiver()
    {
        try
        {
            oiyaaOptomanyReceiver = new OiyaaOptomanyReceiver();
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(BROADCAST_ACTION);
            requireContext().registerReceiver(oiyaaOptomanyReceiver, intentFilter);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    private void setAmount()
    {
        tvAmount.setText(displayPayableAmount);
    }

    private void doGetPaxTransactionList()
    {
        setPaxTransactionAdapter();
        showListView();
    }

    private void showEmptyView(String message)
    {
        tvEmpty.setText(message);
        rvPAXList.setVisibility(View.GONE);
        tvEmpty.setVisibility(View.VISIBLE);
    }

    private void showListView()
    {
        rvPAXList.setVisibility(View.VISIBLE);
        tvEmpty.setVisibility(View.GONE);
    }

    private void setPaxTransactionAdapter()
    {
        populatePaxTransactionList();
        paxTransactionAdapter = new PaxTransactionAdapter(transactionList);
        paxTransactionAdapter.setPaxTransactionItemClickListener(this);
        rvPAXList.setAdapter(paxTransactionAdapter);
    }

    private void populatePaxTransactionList()
    {
        transactionList = new ArrayList<>();
        transactionList.add(new Transaction(R.drawable.msale, getResources().getString(R.string.run_sale)));
        transactionList.add(new Transaction(R.drawable.mrefund, getResources().getString(R.string.run_refund)));
        transactionList.add(new Transaction(R.drawable.mreversal, getResources().getString(R.string.reverse_last)));
        transactionList.add(new Transaction(R.drawable.mreversal, getResources().getString(R.string.reverse_uti)));
        transactionList.add(new Transaction(R.drawable.mpreauth, getResources().getString(R.string.preauth)));
        transactionList.add(new Transaction(R.drawable.mcompletion, getResources().getString(R.string.completion)));
        transactionList.add(new Transaction(R.drawable.mquerytrans, getResources().getString(R.string.query_transaction)));
        transactionList.add(new Transaction(R.drawable.mcancel, getResources().getString(R.string.cancel_trans)));
        transactionList.add(new Transaction(R.drawable.mdailybatch, getResources().getString(R.string.x_report)));
        transactionList.add(new Transaction(R.drawable.mreconciliation, getResources().getString(R.string.z_report)));
        transactionList.add(new Transaction(R.drawable.mhistoryreport, getResources().getString(R.string.history_report)));
        transactionList.add(new Transaction(R.drawable.mexit, getResources().getString(R.string.exit)));
    }

    @Override
    public void onPaxTransactionItemClicked(int paxTransactionOption)
    {
        switch (paxTransactionOption)
        {
            case PaxConstants.SALE:
            {
                /* Send a request to run a transaction for 100 pence (in the configured app currency */
                Log.i(TAG, "Run Auto Sale");
                //EFTServiceLib.runTrans(this, 100, TRANSACTION_TYPE_SALE, "", "", "");
                /* to run a transaction with a specific user ID or password */
                EFTServiceLib.runTrans(requireContext(), Integer.parseInt(payableAmount), TRANSACTION_TYPE_SALE, "", "", "", "5678");
                break;
            }

            case PaxConstants.REFUND:
            {
                /* Send a request to run a transaction for 100 pence (in the configured app currency */
                Log.i(TAG, "Run Auto Refund");
                EFTServiceLib.runTrans(requireContext(), Integer.parseInt(payableAmount), TRANSACTION_TYPE_REFUND, "", "", "");
                break;
            }

            case PaxConstants.REVERSE_LAST:
            {
                Log.i(TAG, "Run Auto Reverse Last");
                /* attempt to reverse the last transaction */
                EFTServiceLib.runReversal(requireContext(), Integer.parseInt(payableAmount), 0);
                break;
            }

            case PaxConstants.REVERSE_BY_UTI:
            {
                Log.i(TAG, "Run Uti Reversal");
                /* attempt to reverse the last transaction */
                if (lastReceivedUTI != null && lastReceivedUTI.length() > 0)
                {
                    EFTServiceLib.runReversal(requireContext(), Integer.parseInt(payableAmount), lastReceivedUTI, true, true);
                }
                else
                {
                    Log.i(TAG, "NO UTI TO Run Reversal on");
                }
                break;
            }

            case PaxConstants.QUERY_TRANSACTION:
            {
                /* Send a request to reverse a transaction by UTI */
                if (lastReceivedUTI != null && lastReceivedUTI.length() > 0)
                {
                    Log.i(TAG, "Run Query Trans on:" + lastReceivedUTI);
                    EFTServiceLib.queryTrans(requireContext(), lastReceivedUTI);
                }
                else
                {
                    Log.i(TAG, "NO UTI TO Run Query on");
                }
                break;
            }
        }
    }

    @Override
    public void onDestroy()
    {
        dashBoardHandler.setPreviousTitle();
        super.onDestroy();
    }

    class OiyaaOptomanyReceiver extends BroadcastReceiver
    {
        private static final String TAG = "OiyaaOptomanyReceiver";

        @Override
        public void onReceive(Context context, Intent intent)
        {
            try
            {
                Log.d(TAG, "onReceive() called");
                if(intent.getAction().equals(TRANSACTION_RESULT_EVENT))
                {
                    String transResult = intent.getExtras().getString(BUNDLE_TRANSACTION_SUCCESS_FAILURE_DETAILS);
                    if(transResult!=null)
                    {
                        if(!transResult.isEmpty())
                        {
                            Gson gson = new Gson();

                            EFTServiceTransResult result = gson.fromJson(transResult,EFTServiceTransResult.class);

                            if(result!=null)
                            {
                                String lastReceivedUTI = "";
                                if (intent.hasExtra("UTI"))
                                {
                                    lastReceivedUTI = intent.getExtras().getString("UTI");
                                }
                                setTransactionResult(result,lastReceivedUTI);
                            }
                            else
                            {
                                Log.d(TAG,"result is empty");
                            }
                        }
                        else
                        {
                            Log.d(TAG,"transaction result is empty");
                        }
                    }
                    else
                    {
                        Log.d(TAG,"transaction result is null");
                    }
                }
                else if(intent.getAction().equals(TRANSACTION_NOT_FOUND))
                {
                    Toast.makeText(requireContext(),getString(R.string.lbl_transaction_failed),Toast.LENGTH_LONG).show();
                }
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
        }
    }

    public void setTransactionList(String transResult)
    {
        Log.d(TAG,"setTransactionList");

        if(transResult!=null)
        {
            if(!transResult.isEmpty())
            {
                Gson gson = new Gson();

                EFTServiceTransResult result = gson.fromJson(transResult,EFTServiceTransResult.class);

                if(result!=null)
                {
                    String lastReceivedUTI = "";
                    setTransactionResult(result,lastReceivedUTI);
                }
                else
                {
                    Log.d(TAG,"result is empty");
                }
            }
            else
            {
                Log.d(TAG,"transaction result is empty");
            }
        }
        else
        {
            Log.d(TAG,"transaction result is null");
        }
    }

    private void setTransactionResult(EFTServiceTransResult result,String lastReceivedUTI)
    {
        Log.d(TAG,"setTransactionResult");

        if (result != null)
        {
            if(result.isApproved())
            {
                Toast.makeText(requireContext(),getString(R.string.lbl_transaction_success),Toast.LENGTH_LONG).show();
                requireActivity().getSupportFragmentManager().popBackStack();
            }
            else
            {
                Toast.makeText(requireContext(),getString(R.string.lbl_transaction_failed),Toast.LENGTH_LONG).show();
            }
        }
        else
        {
            Toast.makeText(requireContext(),getString(R.string.lbl_transaction_failed),Toast.LENGTH_LONG).show();
        }
        if(!lastReceivedUTI.isEmpty())
        {
            DashboardActivity.Companion.setLastReceivedUTI(lastReceivedUTI);
        }
    }

}
