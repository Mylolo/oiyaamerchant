package com.localvalu.merchant.optomany.listeners;

public interface PaxTransactionItemClickListener
{
    void onPaxTransactionItemClicked(int paxTransactionOption);
}
