package com.localvalu.merchant.optomany.utils;

public class PaxConstants
{
    public static final int SALE = 0;
    public static final int REFUND = 1;
    public static final int REVERSE_LAST = 2;
    public static final int REVERSE_BY_UTI = 3;
    public static final int PREAUTH = 4;
    public static final int COMPLETION = 5;
    public static final int QUERY_TRANSACTION = 6;
    public static final int CANCEL_TRANSACTION = 7;
    public static final int PRINT_X = 8;
    public static final int PRINT_Z = 9;
    public static final int PRINT_HISTORY = 10;
    public static final int EXIT = 11;

    public static String lastReceivedUTI="";
}
