package com.oiyaa.test

import android.os.Bundle
import android.util.Log
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import com.oiyaa.test.databinding.ActivityTestBinding
import javax.inject.Inject

class TestActivity : AppCompatActivity()
{

    private lateinit var binding: ActivityTestBinding
    private val TAG = "TestActivity"

    @Inject
    lateinit var injectedString:String
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)

        binding = ActivityTestBinding.inflate(layoutInflater)
        setContentView(binding.root)

        Log.d(TAG,"Injected String : " + injectedString)

    }


}