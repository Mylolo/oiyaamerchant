package com.oiyaa.clover;

import android.app.Application;
import android.content.Context;

import androidx.multidex.MultiDex;

import com.utils.UtilFactory;

public class LocalValuApplication extends Application {

    private static LocalValuApplication LoLoApplication;

    public LocalValuApplication() {

    }

    public static LocalValuApplication getInstance() {
        return LoLoApplication;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        LoLoApplication = this;
        UtilFactory.init(this, this.getResources().getString(R.string.app_name));
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);

        MultiDex.install(base);
    }
}
