package com.oiyaa.clover.activity;

import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

import com.oiyaa.clover.ActivityConfig;
import com.oiyaa.clover.LocalValuActivity;
import com.oiyaa.clover.R;
import com.utils.base.Task;
import com.utils.data.DangerousPermission;
import com.utils.helper.permission.PermissionHandler;
import com.utils.util.Util;

import java.util.List;

public class SplashActivity extends LocalValuActivity implements View.OnClickListener
{
    private static final String TAG = SplashActivity.class.getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        Util.setStatusBarColor(this, R.color.white);

        init();
    }

    @Override
    public void init()
    {
        checkPermissionAccess(Util.asList(DangerousPermission.CAMERA, DangerousPermission.WRITE_EXTERNAL_STORAGE));
    }

    @Override
    public void onClick(View view)
    {
        int id = view.getId();
    }

    private void checkPermissionAccess(List<DangerousPermission> dangerousPermissions)
    {
        getPermissionFactory().requestPermission(this, dangerousPermissions, new PermissionHandler()
        {
            @Override
            public void result(List<DangerousPermission> granted, List<DangerousPermission> denied)
            {
                processApp();
            }

            @Override
            public boolean permissionRationale()
            {
                return false;
            }

            @Override
            public void permissionRationaleFor(List<DangerousPermission> dangerousPermissions)
            {
                checkPermissionAccess(dangerousPermissions);
            }

            @Override
            public void info(String message)
            {
                processApp();
            }
        });
    }

    private void processApp()
    {
        Util.makeDelay(1500, new Task<Boolean>()
        {
            @Override
            public void onSuccess(Boolean result)
            {
                ActivityConfig.startDashBoardActivity(SplashActivity.this);
            }

            @Override
            public void onError(String message)
            {

            }
        });
    }


}
