package com.oiyaa.clover.activity.handler;

import android.os.Bundle;

import com.localvalu.coremerchantapp.data.output.order.OrderDetails;
import com.oiyaa.clover.fragment.orders.OrderAtTableDetailsCloverFragment;
import com.oiyaa.clover.fragment.orders.OrderDetailsCloverFragment;
import com.oiyaa.clover.fragment.clover.CloverTransactionFragment;
import com.oiyaa.clover.fragment.clover.PrintFragment;

public interface CloverHandler
{

    OrderDetailsCloverFragment orderDetailsPaxFragment(String orderId);

    OrderAtTableDetailsCloverFragment orderAtTableDetailsPaxFragment(String orderId);

    PrintFragment printFragment(OrderDetails orderDetails);

    CloverTransactionFragment cloverTransactionFragment(Bundle bundle);

}
