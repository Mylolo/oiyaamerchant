package com.oiyaa.clover.fragment.clover;

import android.accounts.Account;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.clover.sdk.util.CloverAccount;
import com.clover.sdk.v1.Intents;
import com.clover.sdk.v3.payments.Payment;
import com.localvalu.coremerchantapp.handler.DashBoardHandler;
import com.localvalu.coremerchantapp.utils.AppUtils;
import com.oiyaa.clover.LocalValuFragment;
import com.oiyaa.clover.R;
import com.oiyaa.clover.activity.handler.CloverHandler;
import com.oiyaa.clover.databinding.FragmentCloverTransactionBinding;

import static android.app.Activity.RESULT_OK;


public class CloverTransactionFragment extends LocalValuFragment
{
    private static final String TAG = CloverTransactionFragment.class.getSimpleName();

    private DashBoardHandler dashBoardHandler;
    private CloverHandler cloverHandler;
    private FragmentCloverTransactionBinding binding;
    private Account account;
    private static final int SECURE_PAY_REQUEST_CODE = 1;
    private int cardEntryMethodsAllowed = Intents.CARD_ENTRY_METHOD_MAG_STRIPE | Intents.CARD_ENTRY_METHOD_ICC_CONTACT | Intents.CARD_ENTRY_METHOD_NFC_CONTACTLESS | Intents.CARD_ENTRY_METHOD_MANUAL;
    private String payableAmount;
    private String displayPayableAmount;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();

        if (bundle != null)
        {
            payableAmount = bundle.getString(AppUtils.BUNDLE_PAYABLE_AMOUNT);
            displayPayableAmount = bundle.getString(AppUtils.BUNDLE_DISPLAY_PAYABLE_AMOUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        binding = FragmentCloverTransactionBinding.inflate(inflater, container, false);
        setHasOptionsMenu(true);
        setRetainInstance(true);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        setCheckBoxProperties();
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);
        dashBoardHandler = (DashBoardHandler) context;
        cloverHandler = (CloverHandler) context;
    }

    @Override
    public void onResume()
    {
        super.onResume();
        // Retrieve the Clover account
        if (account == null)
        {
            account = CloverAccount.getAccount(requireContext());

            // If an account can't be acquired, exit the app
            if (account == null)
            {
                Toast.makeText(requireContext(), getString(R.string.no_account), Toast.LENGTH_SHORT).show();
                requireActivity().getSupportFragmentManager().popBackStackImmediate();
                return;
            }
        }
    }

    private void setCheckBoxProperties()
    {
        //These methods toggle the bitvalue storing which card entry methods are allowed
        binding.cbMagStripe.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                cardEntryMethodsAllowed = cardEntryMethodsAllowed ^ Intents.CARD_ENTRY_METHOD_MAG_STRIPE;
            }
        });
        binding.cbChipCard.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                cardEntryMethodsAllowed = cardEntryMethodsAllowed ^ Intents.CARD_ENTRY_METHOD_ICC_CONTACT;
            }
        });
        binding.cbNfc.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                cardEntryMethodsAllowed = cardEntryMethodsAllowed ^ Intents.CARD_ENTRY_METHOD_NFC_CONTACTLESS;
            }
        });
        binding.payButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                startSecurePaymentIntent();
            }
        });
        binding.tvAmount.setText(displayPayableAmount.toString());
    }

    @Override
    public void onDetach()
    {
        dashBoardHandler = null;
        cloverHandler = null;
        super.onDetach();
    }

    // Start intent to launch Clover's secure payment activity
    //NOTE: ACTION_SECURE_PAY requires that your app has "clover.permission.ACTION_PAY" in it's AndroidManifest.xml file
    private void startSecurePaymentIntent()
    {
        Intent intent = new Intent(Intents.ACTION_SECURE_PAY);
        Long amount = Long.parseLong(payableAmount, 10);

        //EXTRA_AMOUNT is required for secure payment
        intent.putExtra(Intents.EXTRA_AMOUNT, amount);

        //Allow only selected card entry methods
        intent.putExtra(Intents.EXTRA_CARD_ENTRY_METHODS, cardEntryMethodsAllowed);

        //Because no order id is passed to EXTRA_ORDER_ID a new empty order will be generated for the payment
        startActivityForResult(intent, SECURE_PAY_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == SECURE_PAY_REQUEST_CODE)
        {
            if (resultCode == RESULT_OK)
            {
                //Once the secure payment activity completes the result and its extras can be worked with
                Payment payment = data.getParcelableExtra(Intents.EXTRA_PAYMENT);
                String amountString = String.format("%.2f", ((Double) (0.01 * payment.getAmount())));
                Toast.makeText(requireContext(), getString(R.string.payment_successful, amountString), Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(requireContext(), getString(R.string.payment_failed), Toast.LENGTH_SHORT).show();
            }
        }
    }

}
