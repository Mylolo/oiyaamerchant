package com.oiyaa.clover.fragment.clover;

import android.accounts.Account;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.clover.sdk.util.CloverAccount;
import com.clover.sdk.v1.printer.job.PrintJob;
import com.clover.sdk.v1.printer.job.StaticReceiptPrintJob;
import com.clover.sdk.v3.order.LineItem;
import com.clover.sdk.v3.order.Order;
import com.localvalu.coremerchantapp.data.output.FoodItem;
import com.localvalu.coremerchantapp.data.output.order.OrderDetails;
import com.localvalu.coremerchantapp.fragment.adapter.order.OrderedFoodItemsPrintAdapter;
import com.localvalu.coremerchantapp.handler.DashBoardHandler;
import com.localvalu.coremerchantapp.utils.AppConstants;
import com.localvalu.coremerchantapp.utils.AppUtils;
import com.oiyaa.clover.LocalValuFragment;
import com.oiyaa.clover.R;
import com.oiyaa.clover.activity.handler.CloverHandler;
import com.utils.helper.recyclerview.RVItemAnimator;
import com.utils.util.DateTimeUtil;
import com.utils.validator.Validator;
import com.utils.view.button.CustomMaterialButton;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PrintFragment extends LocalValuFragment
{

    private static final String TAG = PrintFragment.class.getSimpleName();

    private CustomMaterialButton btnPrint;

    private ConstraintLayout cnlContentView;

    private AppCompatTextView tvMerchantName, tvCustomerName, tvOrderId, tvOrderDate, tvOrderTime;

    private RecyclerView rvOrderedItems;

    private AppCompatTextView tvNoOfItems, tvSubTotal, tvTokenApplied, tvBalanceToPay, tvDeliveryFee, tvFinalPay;

    private DashBoardHandler dashBoardHandler;

    private CloverHandler cloverHandler;


    private OrderDetails orderDetails;

    private AppCompatImageView ivLogo, ivPrint;

    private OrderedFoodItemsPrintAdapter orderedFoodItemsPrintAdapter;

    private String strCurrencySymbol, strCurrencyLetter;

    private int marginSpace;

    private Account account;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();

        if (bundle != null)
        {
            orderDetails = bundle.getParcelable(AppUtils.BUNDLE_ORDER_DETAILS);
        }

        strCurrencySymbol = getString(R.string.symbol_pound);
        strCurrencyLetter = getString(R.string.currency_letter_lt);
        marginSpace = (int) getResources().getDimension(R.dimen.margin_15);

        account = CloverAccount.getAccount(requireContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_print, container, false);
        setHasOptionsMenu(true);
        setRetainInstance(true);
        initView(view);
        return view;
    }

    private void initView(View view)
    {
        cnlContentView = (ConstraintLayout) view.findViewById(R.id.cnlContentView);
        btnPrint = (CustomMaterialButton) view.findViewById(R.id.btnPrint);
        ivLogo = (AppCompatImageView) view.findViewById(R.id.ivLogo);
        ivPrint = (AppCompatImageView) view.findViewById(R.id.ivPrint);
        tvMerchantName = view.findViewById(R.id.tvMerchantName);
        tvCustomerName = view.findViewById(R.id.tvCustomerName);
        tvOrderId = view.findViewById(R.id.tvOrderId);
        tvOrderDate = view.findViewById(R.id.tvOrderDate);
        tvOrderTime = view.findViewById(R.id.tvOrderTime);
        tvNoOfItems = view.findViewById(R.id.tvNoOfItems);
        rvOrderedItems = view.findViewById(R.id.rvOrderedItems);
        tvSubTotal = view.findViewById(R.id.tvSubTotal);
        tvTokenApplied = view.findViewById(R.id.tvTokenApplied);
        tvBalanceToPay = view.findViewById(R.id.tvBalanceToPay);
        tvDeliveryFee = view.findViewById(R.id.tvDeliveryFee);
        tvFinalPay = view.findViewById(R.id.tvFinalPay);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        btnPrint.setOnClickListener(_OnClickListener);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rvOrderedItems = view.findViewById(R.id.rvOrderedItems);
        rvOrderedItems.setLayoutManager(linearLayoutManager);
        rvOrderedItems.setItemAnimator(new RVItemAnimator());
        rvOrderedItems.setNestedScrollingEnabled(false);
        setData();
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);
        cloverHandler = (CloverHandler) context;
        dashBoardHandler  = (DashBoardHandler) context;
    }

    @Override
    public void onDetach()
    {
        cloverHandler = null;
        dashBoardHandler = null;
        super.onDetach();

    }

    View.OnClickListener _OnClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            switch (v.getId())
            {
                case R.id.btnPrint:
                    doPrint();
                    break;
            }
        }
    };

    private void setData()
    {
        tvMerchantName.setText(orderDetails.getBusinessName());
        tvCustomerName.setText(orderDetails.getCustomerName());
        tvOrderId.setText(orderDetails.getOrderId());
        setPaymentDetails();
        setStatus(orderDetails.getStatus());
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        try
        {
            Date orderDate = dateFormat.parse(orderDetails.getDateTime());
            String strDate = DateTimeUtil.getFormattedDate(orderDate.getTime(), DateTimeUtil.MM_dd_yyyy);
            String strTime = DateTimeUtil.getFormattedDate(orderDate.getTime(), DateTimeUtil.hh_mm_a);
            tvOrderDate.setText(strDate);
            tvOrderTime.setText(strTime);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        setAdapter();
    }

    private void setPaymentDetails()
    {
        try
        {
            double orderTotal = (Double.parseDouble(orderDetails.getOrderTotal()));
            String strOrderTotal = String.format("%,.2f", orderTotal);
            StringBuilder strSubTotal = new StringBuilder();
            strSubTotal.append(strCurrencySymbol).append(strOrderTotal);
            tvSubTotal.setText(strSubTotal.toString());

            double discountAmount = (Double.parseDouble(orderDetails.getDiscountLtAmount()));
            String strDiscountAmount = String.format("%,.2f", discountAmount);
            StringBuilder strTokenApply = new StringBuilder();
            strTokenApply.append("-").append(strCurrencyLetter).
                    append(strDiscountAmount);
            tvTokenApplied.setText(strTokenApply.toString());

            double deliveryCharges = (Double.parseDouble(orderDetails.getDeliveryRate()));
            String strDeliveryCharges = String.format("%,.2f", deliveryCharges);
            StringBuilder strDeliveryChargesTwo = new StringBuilder();
            strDeliveryChargesTwo.append("+").append(strCurrencySymbol).append(strDeliveryCharges);
            tvDeliveryFee.setText(strDeliveryChargesTwo.toString());

            double finalOrderTotal = (Double.parseDouble(orderDetails.getFinalOrderTotal()));
            String strFinalOrderTotal = String.format("%,.2f", finalOrderTotal);
            StringBuilder strFinalPay = new StringBuilder();
            strFinalPay.append(strCurrencySymbol).append(strFinalOrderTotal);
            tvFinalPay.setText(strFinalPay.toString());
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

    }

    private void setStatus(String status)
    {
        switch (status)
        {
            case AppConstants.ACCEPT:
            case AppConstants.PENDING:
            case AppConstants.DELIVERED:
                tvNoOfItems.setText(getString(R.string.lbl_ordered_items));
                break;
            case AppConstants.DECLINED:
                tvNoOfItems.setText(getString(R.string.lbl_declined_ordered_items));
                break;
        }
    }

    private void setAdapter()
    {
        if (Validator.isValid(orderDetails.getFoodItems()) && !orderDetails.getFoodItems().isEmpty())
        {
            if (Validator.isValid(orderedFoodItemsPrintAdapter))
            {
                orderedFoodItemsPrintAdapter.addItems(orderDetails.getFoodItems());
            }
            else
            {
                orderedFoodItemsPrintAdapter = new OrderedFoodItemsPrintAdapter(orderDetails.getFoodItems());
                rvOrderedItems.setAdapter(orderedFoodItemsPrintAdapter);
            }
        }
        else
        {
            if (Validator.isValid(orderedFoodItemsPrintAdapter))
            {
                orderedFoodItemsPrintAdapter.addItems(new ArrayList<>());
            }
            else
            {
                orderedFoodItemsPrintAdapter = new OrderedFoodItemsPrintAdapter(new ArrayList<>());
                rvOrderedItems.setAdapter(orderedFoodItemsPrintAdapter);
            }
        }
    }

    private void doPrint()
    {
        printReceipt();
        /*try
        {
            IDAL paxIdal = NeptuneLiteUser.getInstance().getDal(requireContext());

            if (paxIdal != null)
            {
                //cnlContentView.invalidate();
                cnlContentView.post(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        //height is ready
                        System.out.println("view width:" + cnlContentView.getWidth() + " height:" + cnlContentView.getHeight());
                        try
                        {
                            Bitmap bitmap = getBitmapFromView(getContext(), cnlContentView);
                            com.pax.dal.IPrinter prn = paxIdal.getPrinter();
                            prn.init();
                            prn.setGray(15);
                            prn.print(bitmap, new IPrinter.IPinterListener()
                            {
                                @Override
                                public void onSucc()
                                {
                                    Toast.makeText(getActivity(), "SuccessFully Printed", Toast.LENGTH_LONG).show();
                                }

                                @Override
                                public void onError(int i)
                                {

                                }
                            });
                        }
                        catch (Exception ex)
                        {
                            ex.printStackTrace();
                        }

                    }
                });
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }*/
    }

    private Bitmap getBitmapFromView(Context ctx, View view)
    {
        view.setLayoutParams(new
                ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT,
                ConstraintLayout.LayoutParams.MATCH_PARENT));
        DisplayMetrics dm = ctx.getResources().getDisplayMetrics();
        view.measure(View.MeasureSpec.makeMeasureSpec(dm.widthPixels,
                View.MeasureSpec.EXACTLY),
                View.MeasureSpec.makeMeasureSpec(dm.heightPixels,
                        View.MeasureSpec.EXACTLY));
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(),
                view.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        view.layout(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
        view.draw(canvas);
        return bitmap;
    }

    private void printReceipt()
    {
        Order tempOrder = createAnOrder();
        int printerFlag =  PrintJob.FLAG_NONE;
        PrintJob pj = new StaticReceiptPrintJob.Builder().order(tempOrder).footerUris(Uri.parse("https://www.oiyaa.com/")).flag(printerFlag).build();
        pj.print(requireContext(), account);
    }

    private Order createAnOrder()
    {
        Order tempOrder = new Order();
        tempOrder.setCurrency("USD");
        tempOrder.setTestMode(false);
        List<LineItem> lineItemList  = new ArrayList<>();
        LineItem item = new LineItem();
        item.setBinName("Test Order");
        item.setName("Test Item");
        item.setPrice(10000L);
        item.setPrinted(false);
        lineItemList.add(item);
        tempOrder.setLineItems(lineItemList);

        return tempOrder;
    }

    private String getFormattedString(String leftSide,String rightSide)
    {
        char[] chars = new char[32];

        if(rightSide.contains(getString(R.string.symbol_pound)))
        {
            chars = new char[31];
        }

        int charCount=0;

        for(char rowChar : chars)
        {
            chars[charCount] = ' ';
            charCount++;
        }

        for(int k=0;k<leftSide.length();k++)
        {
            chars[k] = leftSide.charAt(k);
        }

        int newCharCount=chars.length-1;
        int rightCharacterCount = rightSide.length()-1;

        for(char rowChar : chars)
        {
            if(rightCharacterCount<0)
            {
                break;
            }
            else
            {
                chars[newCharCount] = rightSide.charAt(rightCharacterCount);
                newCharCount--;
                rightCharacterCount--;
            }
        }

        String theText = new String(chars);
        Log.d(TAG," Chars - " + theText);
        return theText;
    }

    private void getShortFoodSizeName(FoodItem foodItem)
    {
        StringBuilder strFoodSizeName=new StringBuilder();
        String[] strItemName = foodItem.getItemName().split(" ");

        if(strFoodSizeName!=null)
        {
            if(strItemName.length>0)
            {
                strFoodSizeName.append(strItemName[0].substring(0,1)).append(".");
                strFoodSizeName.append(strItemName[1]).append("(").append(foodItem.getSizeName()
                        .substring(0,3)).append(")");
            }
            else
            {
                strFoodSizeName.append(strItemName[1]).append("(").append(foodItem.getSizeName()
                        .substring(0,3)).append(")");
            }
        }
    }

    private String getPrintString(String strLeft,String strRight)
    {
        Character[] chars = new Character[32];

        int maxValue = chars.length - strRight.length();

        for(int i=chars.length-1;i>strRight.length();i--)
        {
            chars[i] = strRight.charAt(i);
        }

        for(int i=0;i<strLeft.length()-1;i++)
        {
            chars[i] = strRight.charAt(i);
        }

        return chars.toString();
    }

    private String getSecondLineFoodName(FoodItem foodItem)
    {
        StringBuilder strFoodSizeName=new StringBuilder();
        String[] strItemName = foodItem.getItemName().split(" ");

        if(strItemName!=null)
        {
            if(strItemName.length>1)
            {
                strFoodSizeName.append(strItemName[1]).append(" ").append(strItemName[2]).append("(").append(foodItem.getSizeName()).append(")");
                return strFoodSizeName.toString();
            }
            else if(strItemName.length>0)
            {
                strFoodSizeName.append(strItemName[1]).append(" ").append("(").append(foodItem.getSizeName()).append(")");
                return strFoodSizeName.toString();
            }
        }
        return foodItem.getItemName();
    }

    private String getFirstLineFoodName(FoodItem foodItem)
    {
        StringBuilder strFoodSizeName=new StringBuilder();
        String[] strItemName = foodItem.getItemName().split(" ");

        if(strItemName!=null)
        {
            if(strItemName.length>0)
            {
                return strFoodSizeName.append(strItemName[0]).toString();
            }
            else
            {
                return strFoodSizeName.append(strItemName[0]).toString();
            }
        }
        return foodItem.getItemName();
    }

    @Override
    public void onDestroy()
    {
        dashBoardHandler.setPreviousTitle();
        super.onDestroy();
    }
}
