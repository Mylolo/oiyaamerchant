package com.oiyaa.clover.listeners;

public interface PaxTransactionItemClickListener
{
    void onPaxTransactionItemClicked(int paxTransactionOption);
}
