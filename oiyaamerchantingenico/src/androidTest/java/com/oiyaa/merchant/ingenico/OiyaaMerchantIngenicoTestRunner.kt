package com.oiyaa.merchant.ingenico

import android.app.Application
import android.content.Context
import androidx.test.runner.AndroidJUnitRunner
import dagger.hilt.android.testing.HiltTestApplication

class OiyaaMerchantIngenicoTestRunner : AndroidJUnitRunner()
{

    override fun newApplication(
        cl: ClassLoader?,
        className: String?,
        context: Context?
    ): Application
    {
        return super.newApplication(cl, HiltTestApplication::class.java.canonicalName, context)
    }
}