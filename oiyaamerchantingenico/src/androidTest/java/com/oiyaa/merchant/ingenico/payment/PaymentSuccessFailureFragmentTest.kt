package com.oiyaa.merchant.ingenico.payment

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import com.oiyaa.merchant.ingenico.R
import com.oiyaa.merchant.ingenico.launchFragmentInHiltContainer
import com.oiyaa.merchant.ingenico.payment.viewmodel.PaymentViewModel
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito


@ExperimentalCoroutinesApi
@HiltAndroidTest
class PaymentSuccessFailureFragmentTest
{
    @get:Rule
    val hiltRule = HiltAndroidRule(this)

    private lateinit var viewModel:PaymentViewModel;

    @Before
    fun setUp()
    {
        hiltRule.inject()
        viewModel = PaymentViewModel()
        viewModel.setPaymentState(false)
    }

    @Test
    fun testPaymentStatusSuccess() {
        // GIVEN - Payment status has been set to true
        viewModel.setPaymentState(true)
        // WHEN - Payment Success fragment launched to display the payment status
        val scenario = launchFragmentInHiltContainer<PaymentSuccessFailureFragment>()

        // THEN - display payment success image and text
        // make sure that the image/text are both shown and correct
        onView(withId(R.id.tvPaymentSuccess)).check(matches(withText(R.string.lbl_transaction_success)))
    }

    @Test
    fun testPaymentStatusFailure() {
        // WHEN - Payment Success fragment launched to display the payment status
        val scenario = launchFragmentInHiltContainer<PaymentSuccessFailureFragment>()
        onView(withId(R.id.ivPaymentSuccess)).perform(click())
        // THEN - display payment failure image and text
        // make sure that the image/text are both shown and correct
        onView(withId(R.id.tvPaymentSuccess)).check(matches(withText(R.string.lbl_transaction_failed)))
    }

}