package com.oiyaa.merchant.ingenico

interface LocalValuConstants
{
    companion object
    {
        const val EXTRA_TITLE = "extraTitle"
        const val EXTRA_URL = "extraUrl"
        const val PREF_RETAILER_ID = "prefRetailerId"
        const val PREF_ACCOUNT_ID = "prefAccountId"
        const val PREF_EMAIL = "prefEmail"
        const val PREF_FCM_ID = "prefFCMId"
        const val PREF_PUSHY_ID = "prefPushyId"
        const val PREF_BOOKING_TYPE_ID = "prefBookingTypeId"
        const val PREF_RETAILER_TYPE = "prefRetailerType"
        const val ORDER_ONLINE = "4"
        const val ORDER_AT_TABLE = "5"
        const val ORDER_AT_STORE = "6"
    }
}