package com.oiyaa.merchant.ingenico.activity

import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.graphics.PorterDuff
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import androidx.activity.viewModels
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.google.android.material.navigation.NavigationView
import com.google.zxing.integration.android.IntentIntegrator
import com.google.zxing.integration.android.IntentResult
import com.ingenico.sdk.apimanagement.ApiManagement
import com.localvalu.coremerchantapp.LocalValuActivity
import com.localvalu.coremerchantapp.b2b.ui.B2BTransferTokensFragment
import com.localvalu.coremerchantapp.b2c.ui.B2CTransferTokensFragment
import com.localvalu.coremerchantapp.components.OiyaaBackEndService
import com.localvalu.coremerchantapp.components.OiyaaBackEndServiceManager
import com.localvalu.coremerchantapp.data.output.*
import com.localvalu.coremerchantapp.data.output.driverOld.DriverList
import com.localvalu.coremerchantapp.data.output.driverOld.DriverNotificationResult
import com.localvalu.coremerchantapp.data.output.driverOld.DriverProfileResult
import com.localvalu.coremerchantapp.data.output.order.OrderDetails
import com.localvalu.coremerchantapp.data.output.order.OrderDetailsResult
import com.localvalu.coremerchantapp.data.output.order.OrderListResult
import com.localvalu.coremerchantapp.data.output.order.OrderStatusChangeResult
import com.localvalu.coremerchantapp.data.output.trace.TraceHistoryReportResult
import com.localvalu.coremerchantapp.data.output.trace.VisitEntryResult
import com.localvalu.coremerchantapp.databinding.ActivityDashboardBinding
import com.localvalu.coremerchantapp.databinding.NavigationHeaderBinding
import com.localvalu.coremerchantapp.fragment.*
import com.localvalu.coremerchantapp.fragment.driver.DriverListFragment
import com.localvalu.coremerchantapp.fragment.myappointments.MyAppointmentListFragment
import com.localvalu.coremerchantapp.fragment.orders.MyOrdersFragment
import com.localvalu.coremerchantapp.fragment.orders.OrderAtTableDetailsFragment
import com.localvalu.coremerchantapp.fragment.orders.OrderDetailsFragment
import com.localvalu.coremerchantapp.fragment.tablebooking.TableBookingListFragment
import com.localvalu.coremerchantapp.fragment.trackandtrace.ScanVisitFragment
import com.localvalu.coremerchantapp.fragment.trackandtrace.VisitHistoryFragment
import com.localvalu.coremerchantapp.handler.DashBoardHandler
import com.localvalu.coremerchantapp.handler.TempHandler
import com.localvalu.coremerchantapp.merchantdashboard.ui.DashboardFragment
import com.localvalu.coremerchantapp.utils.AppUtils
import com.localvalu.coremerchantapp.utils.Device.Companion.deviceIngenico
import com.localvalu.coremerchantapp.viewmodel.DashboardViewModel
import com.oiyaa.merchant.ingenico.R
import com.utils.base.Task
import com.utils.helper.preference.PreferenceFactory
import com.utils.util.Util
import com.utils.validator.Validator
import java.util.ArrayList
import com.oiyaa.merchant.ingenico.handler.IngenicoHandler
import com.oiyaa.merchant.ingenico.order.OrderAtTableDetailsIngenicoFragment
import com.oiyaa.merchant.ingenico.order.OrderDetailsIngenicoFragment
import com.oiyaa.merchant.ingenico.payment.PaymentFragment
import com.oiyaa.merchant.ingenico.payment.viewmodel.IngenicoAPIModel
import com.utils.helper.imagecache.ImageCacheFactory
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@AndroidEntryPoint
class DashBoardActivity : LocalValuActivity(), NavigationView.OnNavigationItemSelectedListener,
    View.OnClickListener, DashBoardHandler,TempHandler,IngenicoHandler
{
    private val dashboardViewModel: DashboardViewModel by viewModels()
    private val ingenicoAPIModel: IngenicoAPIModel by viewModels()
    private var actionBarDrawerToggle: ActionBarDrawerToggle? = null
    private var titleList: MutableList<String>? = null
    private var binding: ActivityDashboardBinding? = null
    private var navigationHeaderBinding: NavigationHeaderBinding? = null
    private var backPressedTime: Long = 0
    private var colorPrimary = 0
    private var colorOnPrimary = 0
    private var user:SignInResult?=null;
    private lateinit var apiManagment:ApiManagement
    private val API_KEY=""

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        Util.setStatusBarColor(this, R.color.backgroundDark)
        titleList = ArrayList()
        init()
        observeSubscribers()
        dashboardViewModel.getUser()
    }

    override fun onNewIntent(intent: Intent)
    {
        super.onNewIntent(intent)
    }

    override fun init()
    {
        colorPrimary = ContextCompat.getColor(this, R.color.colorPrimary)
        colorOnPrimary = ContextCompat.getColor(this, R.color.app_bg_color)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_dashboard)
        setSupportActionBar(binding?.toolbarLayout?.toolbar)
        actionBarDrawerToggle = ActionBarDrawerToggle(
            this,
            binding?.drawerLayout,
            binding?.toolbarLayout?.toolbar,
            R.string.dashboard_navigation_drawer_open,
            R.string.dashboard_navigation_drawer_close
        )
        actionBarDrawerToggle!!.syncState()
        actionBarDrawerToggle!!.isDrawerSlideAnimationEnabled = true
        actionBarDrawerToggle!!.drawerArrowDrawable.color = resources.getColor(R.color.white)
        actionBarDrawerToggle!!.toolbarNavigationClickListener =
            View.OnClickListener { v: View? -> onBackPressed() }
        binding?.drawerLayout?.addDrawerListener(actionBarDrawerToggle!!)
        binding?.navigationView?.setNavigationItemSelectedListener(this)
        binding?.navigationView?.let { populateNavigationViewMenu(it) }
        binding?.toolbarLayout?.toolbar?.getNavigationIcon()?.setColorFilter(colorOnPrimary, PorterDuff.Mode.SRC_ATOP)
        val headerView: View? = binding?.navigationView?.getHeaderView(0)
        navigationHeaderBinding = headerView?.let { NavigationHeaderBinding.bind(it) }
    }

    private fun observeSubscribers()
    {
        dashboardViewModel?.user.observe(this, Observer { result->
            result?.let {
                result?.email?.let {
                    user = result
                    populateData(result?.email)
                    startService()
                    token()
                    return@Observer
                }
            }
            stopService()
            signIn()
        })
        ingenicoAPIModel?.serviceReady.observe(this, Observer { result->
            if(result)
            {
                Log.d(TAG,"Ingenico service is ready")
                return@Observer
            }
            Log.d(TAG,"Ingenico service is not ready ")
        })
        CoroutineScope(Dispatchers.IO).launch {
            apiManagment = ApiManagement.getInstance(applicationContext!!) as ApiManagement
            apiManagment.subscribe(API_KEY)
            ingenicoAPIModel.startIngenicoService(true)
        }
    }

    override fun onBackPressed()
    {
        if (Validator.isValid(currentFragment))
        {
            if (currentFragment is ScanFragment)
            {
                if (backPressedTime + BACK_PRESSED_TIME_INTERVAL > System.currentTimeMillis()) finish() else showToast(
                    "Tap Again To Exit"
                )
                backPressedTime = System.currentTimeMillis()
            }
            else
            {
                if (isHomeFragment) scan() else if (supportFragmentManager.fragments.size > 2)
                {
                    supportFragmentManager.popBackStackImmediate()
                    if (titleList != null)
                    {
                        if (titleList!!.size > 0)
                        {
                            titleList!!.removeAt(titleList!!.size - 1)
                        }
                    }
                } else finish()
            }
        }
    }

    private val isHomeFragment: Boolean
        private get()
        {
            val fragment = currentFragment
            return if (fragment != null)
            {
                !(fragment is OrderDetailsFragment ||
                        fragment is OrderAtTableDetailsFragment ||
                        fragment is MerchantSelfServeFragment ||
                        fragment is SignInFragment)
            } else false
        }

    override fun onCreateOptionsMenu(menu: Menu): Boolean
    {
        menu.clear()
        menuInflater.inflate(R.menu.menu_dashboard, menu)
        val menuItem = menu.findItem(R.id.action_exit)
        menuItem?.icon?.setColorFilter(
            ContextCompat.getColor(this, R.color.app_bg_color),
            PorterDuff.Mode.SRC_ATOP
        )
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean
    {
        val id = item.itemId
        when (id)
        {
            R.id.home -> onBackPressed()
            R.id.action_exit -> finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean
    {
        binding?.drawerLayout?.closeDrawer(GravityCompat.START)
        val itemName = item.title.toString().trim { it <= ' ' }
        when (itemName)
        {
            DASHBOARD -> dashboardFragment()
            SCAN -> scan()
            B2CTRANSFER_TOKENS -> b2CTransferTokensFragment()
            B2BTRANSFER_TOKENS -> b2BTransferTokensFragment()
            MY_TRANSACTION -> myTransaction()
            MY_ORDERS -> myOrdersFragment()
            TABLE_BOOKING_LIST -> tableBookingList()
            MY_APPOINTMENT_LIST -> myAppointmentList()
            TRANSFER_LOYALTY_TOKEN -> transferLoyaltyToken()
            SCAN_TRACK_AND_TRACE -> scanVisitFragment()
            SCAN_VISITOR_HISTORY -> visitHistoryFragment()
            SIGN_OUT -> signOut()
        }
        return true
    }

    override fun onClick(view: View)
    {
        val id = view.id
        when (id)
        {
        }
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?)
    {
        val intentResult: IntentResult =
            IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (Validator.isValid(intentResult))
        {
            val contents: String = intentResult.getContents()
            if (!Validator.isValid(contents))
            {
                showToast("Invalid code")
                return
            }
        } else
        {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun signIn(): SignInFragment
    {
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        binding?.drawerLayout?.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
        binding?.toolbarLayout?.toolbar?.setVisibility(View.GONE)
        binding?.toolbarLayout?.tvTitleBar?.setText(SIGN_IN)
        val signInFragment = getFragment(SCAN, SignInFragment::class.java)
        updateFragment(signInFragment, false, true)
        return signInFragment
    }

    override fun dashboardFragment(): DashboardFragment
    {
        window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        binding?.drawerLayout?.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
        binding?.toolbarLayout?.toolbar?.setVisibility(View.VISIBLE)
        binding?.toolbarLayout?.tvTitleBar?.setText(DASHBOARD)
        binding?.toolbarLayout?.toolbar?.getNavigationIcon()?.setColorFilter(colorOnPrimary, PorterDuff.Mode.SRC_ATOP)
        val dashboardFragment: DashboardFragment =
            getFragment<DashboardFragment>(DASHBOARD, DashboardFragment::class.java)
        updateFragment(dashboardFragment, false, true)
        return dashboardFragment
    }

    override fun b2CTransferTokensFragment(): B2CTransferTokensFragment
    {
        window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        binding?.drawerLayout?.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
        binding?.toolbarLayout?.toolbar?.setVisibility(View.VISIBLE)
        binding?.toolbarLayout?.tvTitleBar?.setText(B2CTRANSFER_TOKENS)
        binding?.toolbarLayout?.toolbar?.getNavigationIcon()?.setColorFilter(colorOnPrimary, PorterDuff.Mode.SRC_ATOP)
        val b2CTransferTokensFragment: B2CTransferTokensFragment =
            getFragment<B2CTransferTokensFragment>(
                B2CTRANSFER_TOKENS, B2CTransferTokensFragment::class.java
            )
        updateFragment(b2CTransferTokensFragment, false, true)
        return b2CTransferTokensFragment
    }

    override fun b2BTransferTokensFragment(): B2BTransferTokensFragment
    {
        window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        binding?.drawerLayout?.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
        binding?.toolbarLayout?.toolbar?.setVisibility(View.VISIBLE)
        binding?.toolbarLayout?.tvTitleBar?.setText(B2BTRANSFER_TOKENS)
        binding?.toolbarLayout?.toolbar?.getNavigationIcon()?.setColorFilter(colorOnPrimary, PorterDuff.Mode.SRC_ATOP)
        val b2BTransferTokensFragment: B2BTransferTokensFragment =
            getFragment<B2BTransferTokensFragment>(
                B2BTRANSFER_TOKENS, B2BTransferTokensFragment::class.java
            )
        updateFragment(b2BTransferTokensFragment, false, true)
        return b2BTransferTokensFragment
    }

    override fun register(): MerchantSelfServeFragment
    {
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        binding?.drawerLayout?.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
        binding?.toolbarLayout?.toolbar?.setVisibility(View.GONE)
        binding?.toolbarLayout?.tvTitleBar?.setText(SIGN_IN)
        val merchantSelfServeFragment: MerchantSelfServeFragment =
            getFragment<MerchantSelfServeFragment>(
                REGISTER, MerchantSelfServeFragment::class.java
            )
        updateFragment(merchantSelfServeFragment, true, false)
        return merchantSelfServeFragment
    }

    override fun token(): TokenFragment
    {
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        binding?.drawerLayout?.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
        binding?.toolbarLayout?.toolbar?.setVisibility(View.GONE)
        binding?.toolbarLayout?.tvTitleBar?.setText(TOKEN)
        val fragmentExist = supportFragmentManager?.findFragmentById(R.id.dashboard_relativeLayout_container)
        if(fragmentExist is TokenFragment) return fragmentExist as TokenFragment
        val tokenFragment: TokenFragment =
            getFragment<TokenFragment>(TOKEN, TokenFragment::class.java)
        updateFragment(tokenFragment, false, true)
        return tokenFragment
    }

    override fun scan(): ScanFragment
    {
        window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        binding?.drawerLayout?.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
        binding?.toolbarLayout?.toolbar?.setVisibility(View.VISIBLE)
        binding?.toolbarLayout?.tvTitleBar?.setText(SCAN)
        binding?.toolbarLayout?.toolbar?.getNavigationIcon()?.setColorFilter(colorOnPrimary, PorterDuff.Mode.SRC_ATOP)
        val scanFragment: ScanFragment = getFragment<ScanFragment>(SCAN, ScanFragment::class.java)
        updateFragment(scanFragment, false, true)
        return scanFragment
    }

    override fun scanSuccess(
        amount: Double,
        saleTransactionResult: SaleTransactionResult
    ): ScanSuccessFragment
    {
        window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        binding?.drawerLayout?.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
        binding?.toolbarLayout?.toolbar?.setVisibility(View.GONE)
        binding?.toolbarLayout?.tvTitleBar?.setText("")
        binding?.toolbarLayout?.toolbar?.getNavigationIcon()?.setColorFilter(colorOnPrimary, PorterDuff.Mode.SRC_ATOP)
        val scanSuccessFragment: ScanSuccessFragment = getFragment<ScanSuccessFragment>(
            SCAN_SUCCESS, ScanSuccessFragment::class.java
        )
        scanSuccessFragment.amount = amount
        scanSuccessFragment.saleTransactionResult = saleTransactionResult
        updateFragment(scanSuccessFragment, false, true)
        return scanSuccessFragment
    }

    override fun myTransaction(): MyTransactionFragment
    {
        window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        binding?.drawerLayout?.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
        binding?.toolbarLayout?.toolbar?.setVisibility(View.VISIBLE)
        binding?.toolbarLayout?.tvTitleBar?.setText(MY_TRANSACTION)
        binding?.toolbarLayout?.toolbar?.getNavigationIcon()?.setColorFilter(colorOnPrimary, PorterDuff.Mode.SRC_ATOP)
        val myTransactionFragment: MyTransactionFragment = getFragment<MyTransactionFragment>(
            MY_TRANSACTION, MyTransactionFragment::class.java
        )
        updateFragment(myTransactionFragment, false, true)
        return myTransactionFragment
    }

    override fun transferLoyaltyToken(): TransferLoyaltyTokenFragment
    {
        window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        binding?.drawerLayout?.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
        binding?.toolbarLayout?.toolbar?.setVisibility(View.VISIBLE)
        binding?.toolbarLayout?.tvTitleBar?.setText(TRANSFER_LOYALTY_TOKEN)
        binding?.toolbarLayout?.toolbar?.getNavigationIcon()?.setColorFilter(colorOnPrimary, PorterDuff.Mode.SRC_ATOP)
        val transferLoyaltyTokenFragment: TransferLoyaltyTokenFragment =
            getFragment<TransferLoyaltyTokenFragment>(
                TRANSFER_LOYALTY_TOKEN, TransferLoyaltyTokenFragment::class.java
            )
        updateFragment(transferLoyaltyTokenFragment, false, true)
        return transferLoyaltyTokenFragment
    }

    override fun myOrdersFragment(): MyOrdersFragment
    {
        window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        //binding?.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        binding?.toolbarLayout?.toolbar?.setVisibility(View.VISIBLE)
        binding?.toolbarLayout?.tvTitleBar?.setText(MY_ORDERS)
        binding?.toolbarLayout?.toolbar?.getNavigationIcon()?.setColorFilter(colorOnPrimary, PorterDuff.Mode.SRC_ATOP)
        titleList!!.add(binding?.toolbarLayout?.tvTitleBar?.getText().toString())
        val myOrdersFragment: MyOrdersFragment = getFragment<MyOrdersFragment>(
            MyOrdersFragment::class.java.getName(),
            MyOrdersFragment::class.java
        )
        updateFragment(myOrdersFragment, false, true)
        return myOrdersFragment
    }

    override fun orderDetails(orderId: String): OrderDetailsFragment
    {
        window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        binding?.drawerLayout?.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
        binding?.toolbarLayout?.toolbar?.setVisibility(View.VISIBLE)
        binding?.toolbarLayout?.tvTitleBar?.setText(ORDER_DETAILS)
        binding?.toolbarLayout?.toolbar?.getNavigationIcon()?.setColorFilter(colorOnPrimary, PorterDuff.Mode.SRC_ATOP)
        titleList!!.add(binding?.toolbarLayout?.tvTitleBar?.getText().toString())
        val bundle = Bundle()
        bundle.putString(AppUtils.BUNDLE_ORDER_ID, orderId)
        val orderDetailsFragment: OrderDetailsFragment = getFragment<OrderDetailsFragment>(
            ORDER_DETAILS, OrderDetailsFragment::class.java
        )
        orderDetailsFragment.setArguments(bundle)
        updateFragment(orderDetailsFragment, true, false)
        return orderDetailsFragment
    }

    override fun orderAtTableDetails(orderId: String): OrderAtTableDetailsFragment
    {
        window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        binding?.drawerLayout?.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
        binding?.toolbarLayout?.toolbar?.setVisibility(View.VISIBLE)
        binding?.toolbarLayout?.tvTitleBar?.setText(ORDER_DETAILS)
        binding?.toolbarLayout?.toolbar?.getNavigationIcon()?.setColorFilter(colorOnPrimary, PorterDuff.Mode.SRC_ATOP)
        titleList!!.add(binding?.toolbarLayout?.tvTitleBar?.getText().toString())
        val bundle = Bundle()
        bundle.putString(AppUtils.BUNDLE_ORDER_ID, orderId)
        val orderDetailsFragment: OrderAtTableDetailsFragment =
            getFragment<OrderAtTableDetailsFragment>(
                ORDER_AT_TABLE_DETAILS, OrderAtTableDetailsFragment::class.java
            )
        orderDetailsFragment.setArguments(bundle)
        updateFragment(orderDetailsFragment, true, false)
        return orderDetailsFragment
    }

    override fun tableBookingList(): TableBookingListFragment
    {
        window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        binding?.drawerLayout?.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
        binding?.toolbarLayout?.toolbar?.setVisibility(View.VISIBLE)
        binding?.toolbarLayout?.toolbar?.getNavigationIcon()?.setColorFilter(colorOnPrimary, PorterDuff.Mode.SRC_ATOP)
        binding?.toolbarLayout?.tvTitleBar?.setText(TABLE_BOOKING_LIST)
        val tableBookingListFragment: TableBookingListFragment =
            getFragment<TableBookingListFragment>(
                TABLE_BOOKING_LIST, TableBookingListFragment::class.java
            )
        updateFragment(tableBookingListFragment, false, true)
        return tableBookingListFragment
    }

    override fun myAppointmentList(): MyAppointmentListFragment
    {
        window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        binding?.drawerLayout?.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
        binding?.toolbarLayout?.toolbar?.setVisibility(View.VISIBLE)
        binding?.toolbarLayout?.toolbar?.getNavigationIcon()?.setColorFilter(colorOnPrimary, PorterDuff.Mode.SRC_ATOP)
        binding?.toolbarLayout?.tvTitleBar?.setText(MY_APPOINTMENT_LIST)
        val myAppointmentListFragment: MyAppointmentListFragment =
            getFragment<MyAppointmentListFragment>(
                MY_APPOINTMENT_LIST, MyAppointmentListFragment::class.java
            )
        updateFragment(myAppointmentListFragment, false, true)
        return myAppointmentListFragment
    }

    override fun scanVisitFragment(): ScanVisitFragment
    {
        window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        binding?.drawerLayout?.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
        binding?.toolbarLayout?.toolbar?.setVisibility(View.VISIBLE)
        binding?.toolbarLayout?.tvTitleBar?.setText(SCAN_TRACK_AND_TRACE)
        binding?.toolbarLayout?.toolbar?.getNavigationIcon()?.setColorFilter(colorOnPrimary, PorterDuff.Mode.SRC_ATOP)
        titleList!!.add(binding?.toolbarLayout?.tvTitleBar?.getText().toString())
        val scanVisitFragment: ScanVisitFragment = getFragment<ScanVisitFragment>(
            SCAN_TRACK_AND_TRACE, ScanVisitFragment::class.java
        )
        updateFragment(scanVisitFragment, false, true)
        return scanVisitFragment
    }

    override fun visitHistoryFragment(): VisitHistoryFragment
    {
        window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        binding?.drawerLayout?.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
        binding?.toolbarLayout?.toolbar?.setVisibility(View.VISIBLE)
        binding?.toolbarLayout?.tvTitleBar?.setText(SCAN_VISITOR_HISTORY)
        binding?.toolbarLayout?.toolbar?.getNavigationIcon()?.setColorFilter(colorOnPrimary, PorterDuff.Mode.SRC_ATOP)
        titleList!!.add(binding?.toolbarLayout?.tvTitleBar?.getText().toString())
        val visitHistoryFragment: VisitHistoryFragment = getFragment<VisitHistoryFragment>(
            SCAN_VISITOR_HISTORY, VisitHistoryFragment::class.java
        )
        updateFragment(visitHistoryFragment, false, true)
        return visitHistoryFragment
    }

    override fun driverListFragment(orderDetails: OrderDetails): DriverListFragment
    {
        window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        binding?.drawerLayout?.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
        binding?.toolbarLayout?.toolbar?.setVisibility(View.VISIBLE)
        binding?.toolbarLayout?.tvTitleBar?.setText(SELECT_DRIVER)
        binding?.toolbarLayout?.toolbar?.getNavigationIcon()?.setColorFilter(colorOnPrimary, PorterDuff.Mode.SRC_ATOP)
        titleList!!.add(binding?.toolbarLayout?.tvTitleBar?.getText().toString())
        val bundle = Bundle()
        bundle.putParcelable(AppUtils.BUNDLE_ORDER_DETAILS, orderDetails)
        val driverListFragment: DriverListFragment = getFragment<DriverListFragment>(
            DriverListFragment::class.java.getName(), DriverListFragment::class.java
        )
        driverListFragment.setArguments(bundle)
        updateFragment(driverListFragment, false, true)
        return driverListFragment
    }

    override fun signOut()
    {
        preferenceFactory.remove(PREF_RETAILER_ID, PREF_ACCOUNT_ID, PREF_EMAIL)
        dashboardViewModel.saveUser(SignInResult())
        dashboardViewModel.getUser()
    }

    override fun doGetMyTokenBalance(task: Task<TokenBalanceResult>)
    {
        callTokenBalance(object : Task<TokenBalanceResult>
        {
            override fun onSuccess(result: TokenBalanceResult)
            {
                task.onSuccess(result)
            }

            override fun onError(message: String)
            {
                task.onError(message)
                showToast(message)
            }
        })
    }

    override fun doTransferLoyaltyToken(
        currentBalance: String,
        transferAmount: String,
        balanceAfterTransfer: String,
        toAccountId: String,
        task: Task<TransferLoyaltyTokenResult>
    )
    {
        callTransferLoyaltyToken(
            currentBalance,
            transferAmount,
            balanceAfterTransfer,
            toAccountId,
            object : Task<TransferLoyaltyTokenResult>
            {
                override fun onSuccess(result: TransferLoyaltyTokenResult)
                {
                    if (Validator.isValid(result))
                    {
                        showToast("Token transferred successfully")
                        task.onSuccess(result)
                    }
                }

                override fun onError(message: String)
                {
                    task.onError(message)
                    showToast(message)
                }
            })
    }

    override fun doGetOrderList(retailerId: String, task: Task<OrderListResult>)
    {
        callOrderList(retailerId, object : Task<OrderListResult>
        {
            override fun onSuccess(result: OrderListResult)
            {
                if (Validator.isValid(result))
                {
                    showToast("Order List Received")
                    task.onSuccess(result)
                }
            }

            override fun onError(message: String)
            {
                task.onError(message)
                showToast(message)
            }
        })
    }

    override fun doGetOrderDetails(orderId: String, task: Task<OrderDetailsResult>)
    {
        callOrderDetails(orderId, object : Task<OrderDetailsResult>
        {
            override fun onSuccess(result: OrderDetailsResult)
            {
                if (Validator.isValid(result))
                {
                    showToast("Order Details Received")
                    task.onSuccess(result)
                }
            }

            override fun onError(message: String)
            {
                task.onError(message)
                showToast(message)
            }
        })
    }

    override fun doGetOrderAtTableList(retailerId: String, task: Task<OrderListResult>)
    {
        callOrderAtTableList(retailerId, object : Task<OrderListResult>
        {
            override fun onSuccess(result: OrderListResult)
            {
                if (Validator.isValid(result))
                {
                    showToast("Order List Received")
                    task.onSuccess(result)
                }
            }

            override fun onError(message: String)
            {
                task.onError(message)
                showToast(message)
            }
        })
    }

    override fun doGetOrderAtTableDetails(orderId: String, task: Task<OrderDetailsResult>)
    {
        callOrderAtTableDetails(orderId, object : Task<OrderDetailsResult>
        {
            override fun onSuccess(result: OrderDetailsResult)
            {
                if (Validator.isValid(result))
                {
                    showToast("Order Details Received")
                    task.onSuccess(result)
                }
            }

            override fun onError(message: String)
            {
                task.onError(message)
                showToast(message)
            }
        })
    }

    override fun doChangeOrderStatus(
        orderId: String,
        status: String,
        task: Task<OrderStatusChangeResult>
    )
    {
        callOrderStatusChange(orderId, status, object : Task<OrderStatusChangeResult>
        {
            override fun onSuccess(result: OrderStatusChangeResult)
            {
                showToast("Your request has been processed")
                task.onSuccess(result)
            }

            override fun onError(message: String)
            {
            }
        })
    }

    override fun doInsertVisitEntry(QRCode: String, visitDate: String, task: Task<VisitEntryResult>)
    {
        val merchantId: Int = PreferenceFactory.getInstance().getString(PREF_RETAILER_ID).toInt()
        callInsertVisitEntry(merchantId, QRCode, visitDate, object : Task<VisitEntryResult>
        {
            override fun onSuccess(result: VisitEntryResult)
            {
                if (Validator.isValid(result))
                {
                    showToast("Your request has been processed")
                    task.onSuccess(result)
                }
            }

            override fun onError(message: String)
            {
                task.onError(message)
                showToast(message)
            }
        })
    }

    override fun doGetVisitHistory(
        fromDate: String,
        toDate: String,
        task: Task<TraceHistoryReportResult>
    )
    {
        val merchantId: Int = PreferenceFactory.getInstance().getString(PREF_RETAILER_ID).toInt()
        callVisitEntryHistory(merchantId, fromDate, toDate, object : Task<TraceHistoryReportResult>
        {
            override fun onSuccess(result: TraceHistoryReportResult)
            {
                if (Validator.isValid(result))
                {
                    showToast("Your request has been processed")
                    task.onSuccess(result)
                }
            }

            override fun onError(message: String)
            {
                task.onError(message)
                showToast(message)
            }
        })
    }

    override fun doGetDriversList(retailerId: String, task: Task<DriverList>)
    {
        callDriversList(retailerId, object : Task<DriverList>
        {
            override fun onSuccess(result: DriverList)
            {
                if (Validator.isValid(result))
                {
                    showToast("Your request has been processed")
                    task.onSuccess(result)
                }
            }

            override fun onError(message: String)
            {
                task.onError(message)
                showToast(message)
            }
        })
    }

    override fun doNotifyToDrivers(
        driverId: Int,
        orderId: Int,
        retailerId: Int,
        task: Task<DriverNotificationResult>
    )
    {
        callNotifyToDrivers(driverId, orderId, retailerId, object : Task<DriverNotificationResult>
        {
            override fun onSuccess(result: DriverNotificationResult)
            {
                if (Validator.isValid(result))
                {
                    task.onSuccess(result)
                }
            }

            override fun onError(message: String)
            {
                task.onError(message)
                showToast(message)
            }
        })
    }

    override fun doGetDriverProfile(driverId: String, task: Task<DriverProfileResult>)
    {
        callDriverProfile(driverId, object : Task<DriverProfileResult>
        {
            override fun onSuccess(result: DriverProfileResult)
            {
                if (Validator.isValid(result))
                {
                    task.onSuccess(result)
                }
            }

            override fun onError(message: String)
            {
                task.onError(message)
                showToast(message)
            }
        })
    }

    override fun setPreviousTitle()
    {
        if (titleList != null)
        {
            if (titleList!!.size > 0)
            {
                binding?.toolbarLayout?.tvTitleBar?.setText(titleList!![titleList!!.size - 1])
            }
        }
    }

    override fun startService()
    {
        if(!OiyaaBackEndServiceManager.isMyServiceRunning)
        {
            val oiyaaBackEndService = Intent(this, OiyaaBackEndService::class.java)
            oiyaaBackEndService.action = AppUtils.ACTION_NOTIFICATION_START
            user?.let {
                val retailerId: String = user?.retailerId.toString()
                val bookingTypeId: String = user?.bookingTypeId.toString()
                val retailerType: String = user?.retailerType.toString()
                oiyaaBackEndService.putExtra(AppUtils.BUNDLE_RETAILER_ID, retailerId)
                oiyaaBackEndService.putExtra(AppUtils.BUNDLE_BOOKING_TYPE_ID, bookingTypeId)
                oiyaaBackEndService.putExtra(AppUtils.BUNDLE_RETAILER_TYPE, retailerType)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                {
                    startForegroundService(oiyaaBackEndService)
                }
                else
                {
                    startService(oiyaaBackEndService)
                }
            }
        }
    }

    override fun stopService()
    {
        val oiyaaBackEndService =
            Intent(this@DashBoardActivity, OiyaaBackEndService::class.java)
        oiyaaBackEndService.action = AppUtils.ACTION_NOTIFICATION_END
        stopService(oiyaaBackEndService)
    }

    override fun deviceType(): Int
    {
        return deviceIngenico
    }

    override fun showDeviceOrderDetails(orderId: String)
    {
        if (deviceType() == deviceIngenico)
        {
            orderDetailsIngenicoFragment(orderId);
        }
    }

    override fun showDeviceOrderAtTableDetails(orderId: String)
    {
        if (deviceType() == deviceIngenico)
        {
            orderDetailsIngenicoFragment(orderId);
        }
    }

    override fun doMoreOnScanTransactionSuccess(balanceToPay: String)
    {
    }

    override fun showPrintScreen(orderDetails: OrderDetails)
    {
    }

    override fun onBackButtonPressed()
    {
        onBackPressed()
    }

    private fun populateNavigationViewMenu(navigationView: NavigationView)
    {
        val menu = navigationView.menu
        menu.clear()
        menu.add(1, Menu.FIRST, Menu.NONE, SCAN).setIcon(R.drawable.image_menu_scan)
        menu.add(1, Menu.FIRST+1, Menu.NONE, DASHBOARD).setIcon(R.drawable.ic_baseline_dashboard_24)
        menu.add(1, Menu.FIRST + 2, Menu.NONE, B2CTRANSFER_TOKENS)
            .setIcon(R.drawable.image_menu_transfer_loyality_token)
        menu.add(1, Menu.FIRST + 3, Menu.NONE, B2BTRANSFER_TOKENS)
            .setIcon(R.drawable.image_menu_transfer_loyality_token)
        menu.add(1, Menu.FIRST + 4, Menu.NONE, MY_ORDERS)
            .setIcon(R.drawable.image_menu_my_transaction)
        menu.add(1, Menu.FIRST + 5, Menu.NONE, TABLE_BOOKING_LIST)
            .setIcon(R.drawable.image_menu_transfer_loyality_token)
        menu.add(1, Menu.FIRST + 6, Menu.NONE, MY_APPOINTMENT_LIST)
            .setIcon(R.drawable.image_menu_transfer_loyality_token)
        menu.add(1, Menu.FIRST + 7, Menu.NONE, MY_TRANSACTION)
            .setIcon(R.drawable.image_menu_my_transaction)
        //menu.add(1, Menu.FIRST + 8, Menu.NONE, TRANSFER_LOYALTY_TOKEN).setIcon(R.drawable.image_menu_transfer_loyality_token);
        //menu.add(1, Menu.FIRST + 9, Menu.NONE, SCAN_TRACK_AND_TRACE).setIcon(R.drawable.ic_scanner);
        //menu.add(1, Menu.FIRST + 10, Menu.NONE, SCAN_VISITOR_HISTORY).setIcon(R.drawable.ic_baseline_history_24);
        menu.add(1, Menu.FIRST + 11, Menu.NONE, SIGN_OUT).setIcon(R.drawable.ic_exit_to_app)
    }

    private fun populateData(email: String)
    {
        ImageCacheFactory.getInstance()
            .circular(navigationHeaderBinding!!.nvHeadImage, null, R.drawable.image_user)
        navigationHeaderBinding!!.nvTVUserName.text = email
        navigationHeaderBinding!!.nvTVEmail.text = email
        navigationHeaderBinding!!.nvTVUserName.visibility = View.GONE
    }

    private fun fixToolbarLayout()
    {
        if (currentFragment is ScanFragment)
        {
            supportActionBar!!.setDisplayShowHomeEnabled(true)
            supportActionBar!!.setDisplayHomeAsUpEnabled(false)
            actionBarDrawerToggle!!.isDrawerIndicatorEnabled = true
        } else
        {
            actionBarDrawerToggle!!.isDrawerIndicatorEnabled = false
            supportActionBar!!.setDisplayShowHomeEnabled(false)
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }
    }

    private fun updateFragment(fragment: Fragment, addToBackStack: Boolean, replace: Boolean)
    {
        super.updateFragment(
            R.id.dashboard_relativeLayout_container,
            fragment,
            addToBackStack,
            replace
        )
        fixToolbarLayout()
    }

    private var currentFragment: Fragment
        private get()
        = super.getCurrentFragment(R.id.dashboard_relativeLayout_container)
        set(currentFragment)
        {
            super.currentFragment = currentFragment
        }

    inner class NewRequestsCheckReceiver : BroadcastReceiver()
    {
        override fun onReceive(context: Context, intent: Intent)
        {
            when (intent.action)
            {
                AppUtils.ACTION_NOTIFICATION_END ->
                {
                    val notificationId = intent.getIntExtra("notificationId", 0)
                    // if you want cancel notification
                    val manager =
                        context.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
                    manager.cancel(notificationId)
                    stopService(
                        Intent(
                            this@DashBoardActivity,
                            OiyaaBackEndService::class.java
                        )
                    )
                }
            }
        }
    }

    override fun onDestroy()
    {
        stopService()
        super.onDestroy()
    }

    companion object
    {
        private val TAG = DashBoardActivity::class.java.simpleName
        private const val SIGN_IN = "Sign In"
        private const val REGISTER = "Register"
        private const val TOKEN = "Token"
        private const val SCAN = "Scan"
        private const val B2CTRANSFER_TOKENS = "Merchant to Consumer Transfer Tokens"
        private const val B2BTRANSFER_TOKENS = "Merchant to Merchant Transfer Tokens"
        private const val DASHBOARD = "Dashboard"
        private const val SCAN_SUCCESS = "Scan Success"
        private const val MY_TRANSACTION = "My Transaction"
        private const val MY_ORDERS = "My Orders"
        private const val VISIT_SCAN = "Visit Scan"
        private const val ORDER_DETAILS = "Order Details"
        private const val ORDER_AT_TABLE_DETAILS = "Order At Table Details"
        private const val INGENICO_TRANSACTION = "Transaction"
        private const val TABLE_BOOKING_LIST = "Table Booking List"
        private const val MY_APPOINTMENT_LIST = "My Appointments"
        private const val SELECT_DRIVER = "Select Driver"
        private const val TRANSFER_LOYALTY_TOKEN = "Transfer Loyalty Token"
        private const val SCAN_TRACK_AND_TRACE = "Track and Trace"
        private const val SCAN_VISITOR_HISTORY = "Visitor History"
        private const val SIGN_OUT = "Sign Out"
        private const val BACK_PRESSED_TIME_INTERVAL = 2000
    }

    @Override
    override fun updateFragment(fragment: Fragment,title:String, addToBackStack: Boolean, replace: Boolean)
    {
        super.updateFragment(
            R.id.dashboard_relativeLayout_container,
            fragment,
            addToBackStack,
            replace
        )
        binding!!.toolbarLayout.tvTitleBar.setText(title)
        fixToolbarLayout()
    }

    override fun orderDetailsIngenicoFragment(orderId: String): OrderDetailsIngenicoFragment
    {
        window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        binding!!.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
        binding!!.toolbarLayout.toolbar.visibility = View.VISIBLE
        binding!!.toolbarLayout.tvTitleBar.text = ORDER_DETAILS
        binding!!.toolbarLayout.toolbar.navigationIcon!!.setColorFilter(
            colorOnPrimary,
            PorterDuff.Mode.SRC_ATOP
        )
        titleList!!.add(binding!!.toolbarLayout.tvTitleBar.text.toString())
        val bundle = Bundle()
        bundle.putString(AppUtils.BUNDLE_ORDER_ID, orderId)
        val orderDetailsIngenicoFragment =
            getFragment(ORDER_DETAILS, OrderDetailsIngenicoFragment::class.java)
        orderDetailsIngenicoFragment.arguments = bundle
        updateFragment(orderDetailsIngenicoFragment, true, false)
        return orderDetailsIngenicoFragment
    }

    override fun orderAtTableDetailsIngenicoFragment(orderId: String): OrderAtTableDetailsIngenicoFragment
    {
        window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        binding!!.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
        binding!!.toolbarLayout.toolbar.visibility = View.VISIBLE
        binding!!.toolbarLayout.tvTitleBar.text = ORDER_DETAILS
        binding!!.toolbarLayout.toolbar.navigationIcon!!.setColorFilter(
            colorOnPrimary,
            PorterDuff.Mode.SRC_ATOP
        )
        titleList!!.add(binding!!.toolbarLayout.tvTitleBar.text.toString())
        val bundle = Bundle()
        bundle.putString(AppUtils.BUNDLE_ORDER_ID, orderId)
        val orderAtTableDetailsIngenicoFragment =
            getFragment(ORDER_AT_TABLE_DETAILS, OrderAtTableDetailsIngenicoFragment::class.java)
        orderAtTableDetailsIngenicoFragment.arguments = bundle
        updateFragment(orderAtTableDetailsIngenicoFragment, true, false)
        return orderAtTableDetailsIngenicoFragment
    }

    override fun paymentFragment(bundle:Bundle): PaymentFragment
    {
        binding!!.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
        binding!!.toolbarLayout.toolbar.visibility = View.VISIBLE
        binding!!.toolbarLayout.tvTitleBar.text =
            INGENICO_TRANSACTION
        binding!!.toolbarLayout.toolbar.navigationIcon!!.setColorFilter(
            colorOnPrimary,
            PorterDuff.Mode.SRC_ATOP
        )
        titleList!!.add(binding!!.toolbarLayout.tvTitleBar.text.toString())
        val paymentFragment = PaymentFragment()
        paymentFragment.arguments = bundle
        updateFragment(paymentFragment, true, false)
        return paymentFragment
    }


}