package com.oiyaa.merchant.ingenico.activity;

import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;

import com.localvalu.coremerchantapp.LocalValuActivity;
import com.oiyaa.merchant.ingenico.ActivityConfig;
import com.oiyaa.merchant.ingenico.R;
import com.utils.base.Task;
import com.utils.data.DangerousPermission;
import com.utils.helper.permission.PermissionHandler;
import com.utils.util.Util;

import java.util.List;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class SplashActivity extends LocalValuActivity implements View.OnClickListener
{
    private static final String TAG = SplashActivity.class.getSimpleName();

    private Handler handler;
    private Runnable runnable = new Runnable()
    {
        @Override
        public void run()
        {
            ActivityConfig.startDashBoardActivity(SplashActivity.this);
        }
    };

    //KIOSK
    private static final int KIOSK_MODE_REQUEST = 48;
    final int flags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
            | View.SYSTEM_UI_FLAG_FULLSCREEN
            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
    private ComponentName mAdminComponentName;
    private DevicePolicyManager mDevicePolicyManager;
    protected PowerManager.WakeLock mWakeLock;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        init();
    }

    ActivityResultLauncher<Intent> mStartForResult = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>()
    {
        @Override
        public void onActivityResult(ActivityResult result)
        {

        }
    });

    @Override
    public void init()
    {
        checkPermissionAccess(Util.asList(DangerousPermission.CAMERA, DangerousPermission.WRITE_EXTERNAL_STORAGE));
    }

    @Override
    public void onClick(View view)
    {
        int id = view.getId();
    }

    private void checkPermissionAccess(List<DangerousPermission> dangerousPermissions)
    {
        getPermissionFactory().requestPermission(this, dangerousPermissions, new PermissionHandler()
        {
            @Override
            public void result(List<DangerousPermission> granted, List<DangerousPermission> denied)
            {
                processApp();
            }

            @Override
            public boolean permissionRationale()
            {
                return false;
            }

            @Override
            public void permissionRationaleFor(List<DangerousPermission> dangerousPermissions)
            {
                checkPermissionAccess(dangerousPermissions);
            }

            @Override
            public void info(String message)
            {
                processApp();
            }
        });
    }

    private void processApp()
    {
        Util.makeDelay(1500, new Task<Boolean>()
        {
            @Override
            public void onSuccess(Boolean result)
            {
                ActivityConfig.startDashBoardActivity(SplashActivity.this);
            }

            @Override
            public void onError(String message)
            {

            }
        });
        //handler = new Handler();
        //handler.postDelayed(runnable,1500);
    }

    /*@Override
    public void onResume()
    {
        super.onResume();
        mAdminComponentName = MyAdmin.getComponentName(this);
        if (mDevicePolicyManager.isLockTaskPermitted(getPackageName()))
        {
            Log.d(TAG, "onResume: sucess");
            DevicePolicyManager mDevicePolicyManager = (DevicePolicyManager) getSystemService(
                    Context.DEVICE_POLICY_SERVICE);
            ComponentName mAdminComponentName = new ComponentName(getApplicationContext(), MyAdmin.class);
            mDevicePolicyManager.setLockTaskPackages(mAdminComponentName, new String[]{getPackageName()});
            startLockTask();
        }
        else
        {
            Log.d(TAG, "onResume: isLock Permitted no");

            try
            {
                //runShellCommand("adb shell dpm set-device-owner com.wattoeat.restaurant/.MyAdmin");
            }
            catch (Exception e)
            {
                Log.d(TAG, "onResume: Exception " + e.getMessage().toString());
                e.printStackTrace();
            }
            // Because the package isn't allowlisted, calling startLockTask() here
            // would put the activity into screen pinning mode.
        }
    }*/

    private void runShellCommand(String command) throws Exception
    {
        Process process = Runtime.getRuntime().exec(command);
        process.waitFor();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {

        if (keyCode == KeyEvent.KEYCODE_VOLUME_UP)
        {
            Toast.makeText(this, "Volume button is disabled", Toast.LENGTH_SHORT).show();
            return true;
        }

        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN)
        {
            Toast.makeText(this, "Volume button is disabled", Toast.LENGTH_SHORT).show();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    /*@Override
    protected void onStart()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            if (mDevicePolicyManager.isLockTaskPermitted(this.getPackageName()))
            {
                ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                {
                    if (am.getLockTaskModeState() == ActivityManager.LOCK_TASK_MODE_NONE)
                    {
                        setDefaultCosuPolicies(true);
                        startLockTask();
                    }
                }
            }
        }
        super.onStart();
    }*/

    @Override
    public void onDestroy()
    {
        if(this.mWakeLock!=null)
        {
            this.mWakeLock.release();
        }
        super.onDestroy();
    }

}
