package com.oiyaa.merchant.ingenico.base;

import static com.oiyaa.merchant.ingenico.tools.Keyboard.hideSoftwareKeyboard;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.CallSuper;
import androidx.fragment.app.Fragment;

import com.oiyaa.merchant.ingenico.R;


/**
 * Base class to create tab fragments.
 */
public abstract class BaseFragment extends Fragment {

    private static final String TAG = "BASE_FRAGMENT_INGENICO";

    private View root;
    private View loader;
    private View content;
    private boolean viewCreated = false;
    private boolean serviceReady = false;
    private boolean onServiceReadyCalled = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Return existing root
        if (root != null) {
            return root;
        }
        Log.d(TAG,"onCreateView");

        root = initView(inflater, container, savedInstanceState);

        loader = root.findViewById(R.id.loader);
        content = root.findViewById(R.id.content);

        if (loader == null || content == null) {
            throw new IllegalStateException("Fragment layouts must have a loader and a content");
        }

        viewCreated = true;
        setContentVisibility(serviceReady);

        // Call onServiceReady here if it was not run during callOnServiceReady()
        if (serviceReady && !onServiceReadyCalled) {
            new Thread() {
                @Override
                public void run() {
                    onServiceReady();
                }
            }.start();
        }

        return root;
    }

    public final void callOnServiceReady() {
        serviceReady = true;
        if (viewCreated && !onServiceReadyCalled) {
            onServiceReady();
        }
    }

    /**
     * This callback is called once the {@link com.oiyaa.merchant.ingenico.activity.DashBoardActivity} ended initialization of the APIs, like
     * calls to API Management. This method should be overrided to make all calls to APIs.
     * This method is not executed in the main thread but in a separate init thread, so creating
     * new threads for API calls may not be needed, but calls to update the UI should be run with
     * {@link #runOnUiThread(Runnable)}.
     */
    @CallSuper
    public void onServiceReady() {
        Log.d(TAG,"onServiceReady");
        setContentVisibility(true);
        onServiceReadyCalled = true;
    }

    @Override
    public void onResume() {
        super.onResume();
        hideSoftwareKeyboard(getContext(), getActivity().getCurrentFocus());
    }

    private void setContentVisibility(boolean visible) {
        if (viewCreated) {
            getActivity().runOnUiThread(() -> {
                if (visible) {
                    loader.setVisibility(View.GONE);
                    content.setVisibility(View.VISIBLE);
                } else {
                    loader.setVisibility(View.VISIBLE);
                    content.setVisibility(View.GONE);
                }
            });
        }
    }

    /**
     * Wrapper for the {@link com.oiyaa.merchant.ingenico.activity.DashBoardActivity#runOnUiThread(Runnable)} method.
     *
     * @param runnable
     */
    public void runOnUiThread(Runnable runnable) {
        getActivity().runOnUiThread(runnable);
    }

    /**
     * Helper to display a simple long toast.
     *
     * @param message
     */
    public void toast(String message) {
        runOnUiThread(() -> Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show());
    }

    /**
     * This method should be overrided to inflate the desired layout. This layout MUST contain
     * a view with the id "loader", and a content view with the id "content". The content view will
     * be hidden by the loader until the API management calls are done
     * (see {@link #onServiceReady()}).
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    public abstract View initView(LayoutInflater inflater, ViewGroup container,
                                  Bundle savedInstanceState);

    /**
     * This method must be overrided to return the name of the tab as shown in the tab bar.
     *
     * @return
     */
    public abstract String getName();
}
