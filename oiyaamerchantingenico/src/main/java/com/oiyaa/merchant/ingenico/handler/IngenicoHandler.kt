package com.oiyaa.merchant.ingenico.handler

import android.os.Bundle
import com.oiyaa.merchant.ingenico.order.OrderAtTableDetailsIngenicoFragment
import com.oiyaa.merchant.ingenico.order.OrderDetailsIngenicoFragment
import com.oiyaa.merchant.ingenico.payment.PaymentFragment
import com.oiyaa.merchant.ingenico.payment.TransactionIngenicoFragment

interface IngenicoHandler
{
    fun orderDetailsIngenicoFragment(orderId:String):OrderDetailsIngenicoFragment
    fun orderAtTableDetailsIngenicoFragment(orderId:String):OrderAtTableDetailsIngenicoFragment
    fun paymentFragment(bundle: Bundle):PaymentFragment
}