package com.oiyaa.merchant.ingenico.order

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.button.MaterialButton
import com.localvalu.coremerchantapp.handler.DashBoardHandler
import com.localvalu.coremerchantapp.data.output.order.OrderDetails
import com.localvalu.coremerchantapp.fragment.adapter.order.OrderedFoodItemsAdapter
import com.localvalu.coremerchantapp.data.output.SignInResult
import com.localvalu.coremerchantapp.utils.AppUtils
import com.utils.helper.preference.PreferenceFactory
import com.google.gson.Gson
import com.localvalu.coremerchantapp.LocalValuFragment
import com.oiyaa.merchant.ingenico.order.OrderDetailsIngenicoFragment
import com.utils.helper.recyclerview.RVItemAnimator
import com.utils.helper.recyclerview.RVItemDecorator
import com.localvalu.coremerchantapp.data.output.order.OrderDetailsResult
import com.localvalu.coremerchantapp.fragment.orders.MyOrdersFragment
import com.localvalu.coremerchantapp.data.output.driverOld.DriverProfileResult
import com.localvalu.coremerchantapp.utils.AppConstants
import com.utils.util.DateTimeUtil
import com.localvalu.coremerchantapp.data.output.FoodItem
import com.localvalu.coremerchantapp.data.output.order.OrderStatusChangeResult
import com.oiyaa.merchant.ingenico.R
import com.oiyaa.merchant.ingenico.handler.IngenicoHandler
import com.utils.base.Task
import com.utils.validator.Validator
import java.lang.Exception
import java.lang.StringBuilder
import java.text.SimpleDateFormat
import java.util.ArrayList

class OrderDetailsIngenicoFragment : LocalValuFragment()
{
    private var dashBoardHandler: DashBoardHandler? = null
    private var ingenicoHandler : IngenicoHandler? =null
    private var orderId: String? = null
    private var tvCustomerName: AppCompatTextView? = null
    private var tvOrderId: AppCompatTextView? = null
    private var tvOrderDate: AppCompatTextView? = null
    private var tvOrderStatus: AppCompatTextView? = null
    private var tvPaymentStatus: AppCompatTextView? = null
    private var tvSpecialInstructions: AppCompatTextView? = null
    private var tvAllergyInstructions: AppCompatTextView? = null
    private var tvNoOfItems: AppCompatTextView? = null
    private var tvTextDriverAssigned: AppCompatTextView? = null
    private var tvDriverAssigned: AppCompatTextView? = null
    private var tvTextDriverName: AppCompatTextView? = null
    private var tvDriverName: AppCompatTextView? = null
    private var tvSubTotal: AppCompatTextView? = null
    private var tvTokenApplied: AppCompatTextView? = null
    private var tvBalanceToPay: AppCompatTextView? = null
    private var tvDeliveryFee: AppCompatTextView? = null
    private var tvFinalPay: AppCompatTextView? = null
    private var rvOrderedItems: RecyclerView? = null
    private var cnlOrderEntryProcess: ConstraintLayout? = null
    private var cnlAssignDriver: ConstraintLayout? = null
    private var btnAccept: MaterialButton? = null
    private var btnReject: MaterialButton? = null
    private var btnCompleted: MaterialButton? = null
    private var btnPrint: MaterialButton? = null
    private var btnMore: MaterialButton? = null
    private var btnAssignDriver: MaterialButton? = null
    private var orderDetails: OrderDetails? = null
    private var orderedFoodItemsAdapter: OrderedFoodItemsAdapter? = null
    private var strCurrencySymbol: String? = null
    private var strCurrencyLetter: String? = null
    private var statusUpdated = false
    private var signInResult: SignInResult? = null
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        val bundle = arguments
        if (bundle != null)
        {
            orderId = bundle.getString(AppUtils.BUNDLE_ORDER_ID)
        }
        strCurrencySymbol = getString(R.string.symbol_pound)
        strCurrencyLetter = getString(R.string.currency_letter_lt)
        setSignInResult()
    }

    private fun setSignInResult()
    {
        try
        {
            val strSignInResult = PreferenceFactory.getInstance().getString(PREF_SIGN_IN_RESULT)
            if (strSignInResult != null)
            {
                if (strSignInResult !== "")
                {
                    val gson = Gson()
                    signInResult = gson.fromJson(strSignInResult, SignInResult::class.java)
                }
            }
            if (signInResult != null)
            {
                //if (BuildConfig.DEBUG)
                //{
                Log.d(TAG, "signInResult->" + signInResult!!.driverType)
                //}
            }
        } catch (ex: Exception)
        {
            ex.printStackTrace()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?
    {
        val view = inflater.inflate(R.layout.fragment_order_details, container, false)
        setHasOptionsMenu(true)
        retainInstance = true
        initView(view)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        if (Validator.isValid(orderedFoodItemsAdapter)) rvOrderedItems!!.adapter =
            orderedFoodItemsAdapter
        btnAccept!!.setOnClickListener(_OnClickListener)
        btnReject!!.setOnClickListener(_OnClickListener)
        btnCompleted!!.setOnClickListener(_OnClickListener)
        cnlAssignDriver!!.visibility = View.GONE
        tvTextDriverAssigned!!.visibility = View.GONE
        tvDriverAssigned!!.visibility = View.GONE
        tvTextDriverName!!.visibility = View.GONE
        tvDriverName!!.visibility = View.GONE
        btnPrint!!.setOnClickListener(_OnClickListener)
        btnMore!!.setOnClickListener(_OnClickListener)
        btnAssignDriver!!.setOnClickListener(_OnClickListener)
        doGetOrderDetails()
    }

    private fun initView(view: View)
    {
        tvCustomerName = view.findViewById(R.id.tvCustomerName)
        tvOrderId = view.findViewById(R.id.tvOrderId)
        tvOrderDate = view.findViewById(R.id.tvOrderDate)
        tvOrderStatus = view.findViewById(R.id.tvOrderStatus)
        tvPaymentStatus = view.findViewById(R.id.tvPaymentStatus)
        tvTextDriverAssigned =
            view.findViewById(com.localvalu.coremerchantapp.R.id.tvTextDriverAssigned)
        tvDriverAssigned = view.findViewById(com.localvalu.coremerchantapp.R.id.tvDriverAssigned)
        tvTextDriverName = view.findViewById(com.localvalu.coremerchantapp.R.id.tvTextDriverName)
        tvDriverName = view.findViewById(com.localvalu.coremerchantapp.R.id.tvDriverName)
        tvSpecialInstructions = view.findViewById(R.id.tvSpecialInstructions)
        tvAllergyInstructions = view.findViewById(R.id.tvAllergyInstructions)
        tvNoOfItems = view.findViewById(R.id.tvNoOfItems)
        rvOrderedItems = view.findViewById(R.id.rvOrderedItems)
        tvAllergyInstructions = view.findViewById(R.id.tvAllergyInstructions)
        cnlOrderEntryProcess = view.findViewById(R.id.cnlOrderEntryProcess)
        tvSubTotal = view.findViewById(R.id.tvSubTotal)
        tvTokenApplied = view.findViewById(R.id.tvTokenApplied)
        tvBalanceToPay = view.findViewById(R.id.tvBalanceToPay)
        tvDeliveryFee = view.findViewById(R.id.tvDeliveryFee)
        tvFinalPay = view.findViewById(R.id.tvFinalPay)
        btnAccept = view.findViewById(R.id.btnAccept)
        btnReject = view.findViewById(R.id.btnReject)
        btnCompleted = view.findViewById(R.id.btnCompleted)
        cnlAssignDriver = view.findViewById(com.localvalu.coremerchantapp.R.id.cnlAssignDriver)
        btnPrint = view.findViewById(R.id.btnPrint)
        btnMore = view.findViewById(R.id.btnMore)
        btnAssignDriver = view.findViewById(com.localvalu.coremerchantapp.R.id.btnAssignDriver)
        val linearLayoutManager = LinearLayoutManager(context)
        rvOrderedItems = view.findViewById(R.id.rvOrderedItems)
        rvOrderedItems?.setLayoutManager(linearLayoutManager)
        rvOrderedItems?.setItemAnimator(RVItemAnimator())
        val drawable = resources.getDrawable(R.drawable.list_divider)
        if (drawable != null)
        {
            rvOrderedItems?.addItemDecoration(RVItemDecorator(drawable))
        }
        btnPrint?.setVisibility(View.VISIBLE)
        btnMore?.setVisibility(View.VISIBLE)
    }

    override fun onAttach(context: Context)
    {
        super.onAttach(context)
        dashBoardHandler = context as DashBoardHandler
        ingenicoHandler = context as IngenicoHandler
    }

    override fun onDetach()
    {
        dashBoardHandler = null
        super.onDetach()
    }

    var _OnClickListener = View.OnClickListener { v ->
        when (v.id)
        {
            R.id.btnAccept -> doAccept()
            R.id.btnReject -> doReject()
            R.id.btnCompleted -> doComplete()
            R.id.btnPrint -> doPrint()
            R.id.btnMore -> doMore()
            R.id.btnAssignDriver -> dashBoardHandler!!.driverListFragment(orderDetails)
        }
    }

    private fun dummyData(): OrderDetails
    {
        val orderListData = OrderDetails()
        orderListData.orderId = "1"
        orderListData.businessId = "2"
        orderListData.orderTotal = "125"
        orderListData.isOrderConfirmedByRest = "1"
        orderListData.customerName = "Test"
        orderListData.currentStatus = "0"
        orderListData.allergyInstructions = "None"
        orderListData.specialInstructions = "None"
        orderListData.status = "1"
        orderListData.createdDate = "2020-02-25 14:09:12"
        orderListData.dateTime = "2020-02-22 14:09:12"
        orderListData.customerMobile = "88999000033"
        orderListData.isOrderPlaced = "1"
        orderListData.finalOrderTotal = "12"
        return orderListData
    }

    private fun doGetOrderDetails()
    {
        //Log.d(TAG, "doGetOrderDetails: order Id - " + orderId);

        //orderDetails=dummyData();
        setData()
        dashBoardHandler!!.doGetOrderDetails(orderId, object : Task<OrderDetailsResult>
        {
            override fun onSuccess(result: OrderDetailsResult)
            {
                if (Validator.isValid(result.orderDetails))
                {
                    orderDetails = result.orderDetails[0]
                    setData()
                    if (statusUpdated)
                    {
                        val fragmentManager = requireActivity().supportFragmentManager
                        if (fragmentManager != null)
                        {
                            for (fragment in fragmentManager.fragments)
                            {
                                if (fragment is MyOrdersFragment)
                                {
                                    val myOrdersFragment = fragment
                                    if (myOrdersFragment != null)
                                    {
                                        myOrdersFragment.updateStatus(result.orderDetails[0].status)
                                        break
                                    }
                                }
                            }
                        }
                    }
                } else
                {
                }
            }

            override fun onError(message: String)
            {
            }
        })
    }

    private fun doGetDriverProfile(driverId: String)
    {
        dashBoardHandler!!.doGetDriverProfile(driverId, object : Task<DriverProfileResult>
        {
            override fun onSuccess(result: DriverProfileResult)
            {
                if (Validator.isValid(result))
                {
                    tvDriverName!!.text = result.name
                } else
                {
                }
            }

            override fun onError(message: String)
            {
            }
        })
    }

    private fun setData()
    {
        if (orderDetails != null)
        {
            tvCustomerName!!.text = orderDetails!!.customerName
            tvOrderId!!.text = orderDetails!!.orderId
            tvAllergyInstructions!!.text = orderDetails!!.allergyInstructions
            tvSpecialInstructions!!.text = orderDetails!!.specialInstructions
            tvPaymentStatus!!.text = orderDetails!!.paymentStatus
            setPaymentDetails()
            when (orderDetails!!.orderOption)
            {
                AppConstants.ORDER_OPTION_DELIVERY -> if (orderDetails!!.isDriverSelected == AppConstants.ORDER_DRIVER_SELECTED)
                {
                    tvDriverAssigned!!.text =
                        getString(com.localvalu.coremerchantapp.R.string.lbl_yes)
                    tvDriverAssigned!!.setTextColor(resources.getColor(com.localvalu.coremerchantapp.R.color.greenDark))
                    tvDriverAssigned!!.visibility = View.VISIBLE
                    tvTextDriverAssigned!!.visibility = View.VISIBLE
                    tvTextDriverName!!.visibility = View.VISIBLE
                    tvDriverName!!.visibility = View.VISIBLE
                    doGetDriverProfile(orderDetails!!.driverId)
                } else
                {
                    tvDriverAssigned!!.text =
                        getString(com.localvalu.coremerchantapp.R.string.lbl_no)
                    tvDriverAssigned!!.setTextColor(resources.getColor(com.localvalu.coremerchantapp.R.color.redDark))
                    tvDriverAssigned!!.visibility = View.VISIBLE
                    tvTextDriverAssigned!!.visibility = View.VISIBLE
                    tvTextDriverName!!.visibility = View.GONE
                    tvDriverName!!.visibility = View.GONE
                }
                else ->
                {
                    tvTextDriverAssigned!!.visibility = View.GONE
                    tvDriverAssigned!!.visibility = View.GONE
                    tvTextDriverName!!.visibility = View.GONE
                    tvDriverName!!.visibility = View.GONE
                }
            }
            val dateFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
            try
            {
                val orderDate = dateFormat.parse(orderDetails!!.dateTime)
                val strDate = DateTimeUtil.getFormattedDate(orderDate.time, DateTimeUtil.dd_MM_yyyy)
                val strTime = DateTimeUtil.getFormattedDate(orderDate.time, DateTimeUtil.hh_mm_a)
                val strDateTime = StringBuilder()
                strDateTime.append(strDate).append(" ").append(strTime)
                tvOrderDate!!.text = strDateTime.toString()
            } catch (ex: Exception)
            {
                ex.printStackTrace()
            }
            if (orderDetails!!.status != null)
            {
                if (orderDetails!!.status.length > 0)
                {
                    try
                    {
                        setOrderStatus(orderDetails!!.status)
                    } catch (ex: Exception)
                    {
                        ex.printStackTrace()
                    }
                }
            }
            setAdapter()
            showHideBottom()
        }
    }

    private fun setPaymentDetails()
    {
        try
        {
            val orderTotal = orderDetails!!.orderTotal.toDouble()
            val strOrderTotal = String.format("%,.2f", orderTotal)
            val strSubTotal = StringBuilder()
            strSubTotal.append(strCurrencySymbol).append(strOrderTotal)
            tvSubTotal!!.text = strSubTotal.toString()
            val discountAmount = orderDetails!!.discountLtAmount.toDouble()
            val strDiscountAmount = String.format("%,.2f", discountAmount)
            val strTokenApply = StringBuilder()
            strTokenApply.append("-").append(strCurrencyLetter).append(strDiscountAmount)
            tvTokenApplied!!.text = strTokenApply.toString()
            val deliveryCharges = orderDetails!!.deliveryRate.toDouble()
            val strDeliveryCharges = String.format("%,.2f", deliveryCharges)
            val strDeliveryChargesTwo = StringBuilder()
            strDeliveryChargesTwo.append("+").append(strCurrencySymbol).append(strDeliveryCharges)
            tvDeliveryFee!!.text = strDeliveryChargesTwo.toString()
            val finalOrderTotal = orderDetails!!.finalOrderTotal.toDouble()
            val strFinalOrderTotal = String.format("%,.2f", finalOrderTotal)
            val strFinalPay = StringBuilder()
            strFinalPay.append(strCurrencySymbol).append(strFinalOrderTotal)
            tvFinalPay!!.text = strFinalPay.toString()
        } catch (ex: Exception)
        {
            ex.printStackTrace()
        }
    }

    private fun showHideBottom()
    {
        when (orderDetails!!.status)
        {
            AppConstants.PENDING ->
            {
                cnlOrderEntryProcess!!.visibility = View.VISIBLE
                btnCompleted!!.visibility = View.GONE
                cnlAssignDriver!!.visibility = View.GONE
                showHidePrintButton(false)
                showHideMoreButton()
            }
            AppConstants.DECLINED ->
            {
                cnlOrderEntryProcess!!.visibility = View.GONE
                btnCompleted!!.visibility = View.GONE
                cnlAssignDriver!!.visibility = View.GONE
                showHidePrintButton(false)
                showHideMoreButton()
            }
            AppConstants.ORDER_CANCELLED ->
            {
                cnlOrderEntryProcess!!.visibility = View.GONE
                btnCompleted!!.visibility = View.GONE
                cnlAssignDriver!!.visibility = View.GONE
                showHidePrintButton(false)
                btnMore!!.visibility = View.GONE
            }
            AppConstants.DELIVERED ->
            {
                btnCompleted!!.visibility = View.GONE
                cnlOrderEntryProcess!!.visibility = View.GONE
                cnlAssignDriver!!.visibility = View.GONE
                showHidePrintButton(true)
                showHideMoreButton()
            }
            AppConstants.ACCEPT ->
            {
                btnCompleted!!.visibility = View.VISIBLE
                cnlOrderEntryProcess!!.visibility = View.GONE
                cnlAssignDriver!!.visibility = View.GONE
                showHideMoreButton()
                when (orderDetails!!.orderOption)
                {
                    AppConstants.ORDER_OPTION_DELIVERY ->
                    {
                        if (orderDetails!!.isDriverSelected == AppConstants.ORDER_DRIVER_SELECTED)
                        {
                            btnCompleted!!.visibility = View.VISIBLE
                            btnMore!!.visibility = View.VISIBLE
                            cnlAssignDriver!!.visibility = View.GONE
                        } else
                        {
                            if (signInResult != null)
                            {
                                if (signInResult!!.driverType != null)
                                {
                                    if (signInResult!!.driverType.toInt() == AppConstants.NO_DRIVERS)
                                    {
                                        cnlAssignDriver!!.visibility = View.GONE
                                    } else cnlAssignDriver!!.visibility = View.VISIBLE
                                } else cnlAssignDriver!!.visibility = View.VISIBLE
                            } else cnlAssignDriver!!.visibility = View.VISIBLE
                            btnCompleted!!.visibility = View.GONE
                            btnMore!!.visibility = View.GONE
                        }
                        cnlOrderEntryProcess!!.visibility = View.GONE
                    }
                    else ->
                    {
                        cnlAssignDriver!!.visibility = View.GONE
                        cnlOrderEntryProcess!!.visibility = View.GONE
                        btnCompleted!!.visibility = View.VISIBLE
                        btnMore!!.visibility = View.VISIBLE
                    }
                }
                showHidePrintButton(true)
            }
        }
    }

    private fun showHidePrintButton(show: Boolean)
    {
        if (show) btnPrint!!.visibility = View.VISIBLE else btnPrint!!.visibility = View.GONE
    }

    private fun showHideMoreButton()
    {
        if (orderDetails!!.paymentStatus != null)
        {
            when (orderDetails!!.paymentStatus)
            {
                AppConstants.PAID -> btnMore!!.visibility = View.GONE
                AppConstants.NOT_PAID -> btnMore!!.visibility = View.VISIBLE
            }
        }
    }

    private fun setAdapter()
    {
        if (Validator.isValid(orderDetails!!.foodItems) && !orderDetails!!.foodItems.isEmpty())
        {
            if (Validator.isValid(orderedFoodItemsAdapter))
            {
                orderedFoodItemsAdapter!!.submitList(orderDetails!!.foodItems)
            } else
            {
                orderedFoodItemsAdapter = OrderedFoodItemsAdapter()
                rvOrderedItems!!.adapter = orderedFoodItemsAdapter
            }
        } else
        {
            if (Validator.isValid(orderedFoodItemsAdapter))
            {
                orderedFoodItemsAdapter!!.submitList(ArrayList())
            } else
            {
                orderedFoodItemsAdapter = OrderedFoodItemsAdapter()
                rvOrderedItems!!.adapter = orderedFoodItemsAdapter
            }
        }
    }

    private fun setOrderStatus(status: String)
    {
        when (status)
        {
            AppConstants.ACCEPT ->
            {
                tvOrderStatus!!.text = getString(R.string.lbl_accepted)
                tvOrderStatus!!.setTextColor(resources.getColor(R.color.greenDark))
                tvNoOfItems!!.text =
                    getString(R.string.lbl_ordered_items).toUpperCase()
            }
            AppConstants.DELIVERED ->
            {
                tvOrderStatus!!.text = getString(R.string.lbl_delivered)
                tvOrderStatus!!.setTextColor(resources.getColor(R.color.greenDark))
                tvNoOfItems!!.text = getString(R.string.lbl_ordered_items).toUpperCase()
            }
            AppConstants.DECLINED ->
            {
                tvOrderStatus!!.setText(R.string.lbl_declined)
                tvOrderStatus!!.setTextColor(resources.getColor(R.color.redDark))
                tvNoOfItems!!.text =
                    getString(R.string.lbl_declined_ordered_items).toUpperCase()
            }
            AppConstants.ORDER_CANCELLED ->
            {
                tvOrderStatus!!.setText(R.string.lbl_cancelled)
                tvOrderStatus!!.setTextColor(resources.getColor(R.color.redDark))
                tvNoOfItems!!.text =
                    getString(R.string.lbl_declined_ordered_items).toUpperCase()
            }
            AppConstants.PENDING ->
            {
                tvOrderStatus!!.setText(R.string.lbl_pending)
                tvOrderStatus!!.setTextColor(resources.getColor(R.color.orangeDark))
                tvNoOfItems!!.text =
                    getString(R.string.lbl_ordered_items).toUpperCase()
            }
        }
    }

    private fun doAccept()
    {
        dashBoardHandler!!.doChangeOrderStatus(
            orderId,
            AppConstants.ACCEPT,
            object : Task<OrderStatusChangeResult>
            {
                override fun onSuccess(result: OrderStatusChangeResult)
                {
                    statusUpdated = true
                    doGetOrderDetails()
                }

                override fun onError(message: String)
                {
                }
            })
    }

    private fun doReject()
    {
        dashBoardHandler!!.doChangeOrderStatus(
            orderId,
            AppConstants.DECLINED,
            object : Task<OrderStatusChangeResult>
            {
                override fun onSuccess(result: OrderStatusChangeResult)
                {
                    statusUpdated = true
                    doGetOrderDetails()
                }

                override fun onError(message: String)
                {
                }
            })
    }

    private fun doComplete()
    {
        dashBoardHandler!!.doChangeOrderStatus(
            orderId,
            AppConstants.DELIVERED,
            object : Task<OrderStatusChangeResult>
            {
                override fun onSuccess(result: OrderStatusChangeResult)
                {
                    statusUpdated = true
                    doGetOrderDetails()
                }

                override fun onError(message: String)
                {
                }
            })
    }

    private fun doPrint()
    {
        //paxHandler.printFragment(orderDetails);
    }

    private fun doMore()
    {
        var payableAmount = orderDetails!!.finalOrderTotal.toDouble()
        payableAmount = payableAmount * 100 / 100
        payableAmount = payableAmount * 100
        val payableAmountInMinorUnits = payableAmount.toInt()
        val bundle = Bundle()
        bundle.putString(
            AppUtils.BUNDLE_PAYABLE_AMOUNT,
            Integer.toString(payableAmountInMinorUnits)
        )
        bundle.putString(AppUtils.BUNDLE_CASHBACK_AMOUNT, "0")
        bundle.putString(AppUtils.BUNDLE_DISPLAY_PAYABLE_AMOUNT, orderDetails!!.finalOrderTotal)
        bundle.putString(AppUtils.BUNDLE_DISPLAY_CASHBACK_AMOUNT, "0")
        bundle.putString(AppUtils.BUNDLE_LAST_RECEIVED_UTI, "0")

        ingenicoHandler?.paymentFragment(bundle);
    }

    override fun onDestroy()
    {
        super.onDestroy()
        dashBoardHandler!!.setPreviousTitle()
    }

    companion object
    {
        private val TAG = OrderDetailsIngenicoFragment::class.java.simpleName
    }
}