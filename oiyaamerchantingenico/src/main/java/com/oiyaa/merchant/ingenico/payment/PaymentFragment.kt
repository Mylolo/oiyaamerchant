package com.oiyaa.merchant.ingenico.payment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.localvalu.coremerchantapp.R
import com.localvalu.coremerchantapp.data.output.order.OrderDetails
import com.localvalu.coremerchantapp.extensions.shorten
import com.localvalu.coremerchantapp.utils.AppUtils
import com.oiyaa.merchant.ingenico.databinding.FragmentPaymentBinding
import com.oiyaa.merchant.ingenico.payment.viewmodel.PaymentViewModel
import com.oiyaa.merchant.ingenico.payment.viewmodel.IngenicoAPIModel
import com.utils.util.DateTimeUtil
import dagger.hilt.android.AndroidEntryPoint
import java.lang.Exception
import java.lang.StringBuilder
import java.text.SimpleDateFormat

@AndroidEntryPoint
class PaymentFragment : Fragment()
{
    //private val ingenicoAPIModel: IngenicoAPIModel by activityViewModels()
    private val paymentViewModel: PaymentViewModel by activityViewModels()
    private var binding:FragmentPaymentBinding?=null
    private var orderDetails:OrderDetails?=null
    private val colorPrimary by lazy {
        resources.getColor(R.color.colorPrimary)
    }
    private val colorRed by lazy {
        resources.getColor(R.color.redDark)
    }

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        arguments?.let { bundle->
            orderDetails =bundle.getParcelable(AppUtils.BUNDLE_ORDER_DETAILS)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?
    {
        binding = FragmentPaymentBinding.inflate(inflater,container,false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        setProperties()
    }

    private fun setProperties()
    {
        observeSubscribers()
        orderDetails?.let {
            binding?.tvOrderId?.setText(it.orderId)
            binding?.tvCustomerName?.setText(it.customerName)
            val dateFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
            try
            {
                val orderDate = dateFormat.parse(orderDetails?.createdDate)
                val strDate = DateTimeUtil.getFormattedDate(orderDate.time, DateTimeUtil.dd_MM_yyyy)
                val strTime = DateTimeUtil.getFormattedDate(orderDate.time, DateTimeUtil.hh_mm_a)
                val strDateTime = StringBuilder()
                strDateTime.append(strDate).append(" ").append(strTime)
                binding?.tvOrderDate?.text = strDateTime.toString()
            }
            catch (ex: Exception)
            {
                ex.printStackTrace()
            }
            binding?.tvFinalAmount?.text = requireContext().getString(R.string.symbol_pound) + " " +
                    orderDetails?.finalOrderTotal?.toDouble()?.shorten()

            binding?.btnPay?.isEnabled=false
            binding?.btnPay?.setOnClickListener{

            }
        }
    }

    private fun observeSubscribers()
    {
        /*ingenicoAPIModel.serviceReady.observe(viewLifecycleOwner,Observer{
            if(it)
            {
                binding?.btnPay?.isEnabled=true
                binding?.tvLoadingText?.setBackgroundColor(colorPrimary)
            }
            else
            {
                binding?.btnPay?.isEnabled=false
                binding?.tvLoadingText?.setBackgroundColor(colorRed)
            }
        })*/
    }
}