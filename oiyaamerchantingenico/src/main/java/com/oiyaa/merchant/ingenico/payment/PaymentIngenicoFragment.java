package com.oiyaa.merchant.ingenico.payment;


import static com.oiyaa.merchant.ingenico.utils.ByteUtils.bytesToHexString;

import android.text.TextUtils;
import android.util.Pair;

import com.google.common.collect.Lists;
import com.ingenico.sdk.IngenicoException;
import com.ingenico.sdk.exceptions.UnavailableApiException;
import com.ingenico.sdk.transaction.ITransaction;
import com.ingenico.sdk.transaction.constants.CardTypes;
import com.ingenico.sdk.transaction.constants.MotoType;
import com.ingenico.sdk.transaction.constants.PaymentMeans;
import com.ingenico.sdk.transaction.constants.TransactionTypes;
import com.ingenico.sdk.transaction.data.PaymentMean;
import com.ingenico.sdk.transaction.data.TransactionResult;
import com.ingenico.sdk.transaction.data.TransactionType;
import com.oiyaa.merchant.ingenico.base.BaseFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class PaymentIngenicoFragment extends BaseFragment
{
    private static final String NO_DATA = "-";

    public PaymentIngenicoFragment()
    {
        // Required empty public constructor
    }

    protected String getPaymentMeanLabel(Integer paymentMean)
    {
        if (paymentMean == null) return "-";
        return PaymentMeans.getTextValue(paymentMean);
    }

    protected String getCardTypeLabel(Integer cardType)
    {
        if (cardType == null) return "-";
        if (cardType == CardTypes.ALL_CARD_TYPES) return "All (0)";
        if (cardType == CardTypes.DEBIT) return "Debit (1)";
        if (cardType == CardTypes.CREDIT) return "Credit (2)";
        if (cardType == CardTypes.GIFT) return "Gift (3)";
        if (cardType == CardTypes.EBT) return "EBT (4)";
        if (cardType == CardTypes.LTM_CARD) return "Debit (13)";
        return String.format("Card Type 0x%02x", cardType);
    }

    protected String getMotoTypeLabel(Integer motoType)
    {
        if (motoType == null) return "-";
        if (motoType == MotoType.NO) return "No (0)";
        if (motoType == MotoType.MANUAL) return "Manual (1)";
        if (motoType == MotoType.ECOM) return "eCommerce (2)";
        if (motoType == MotoType.MOTO) return "Mail/Telephone (3)";
        return String.format("Unknown type (%d)", motoType);
    }

    private String getTransactionTypeLabel(int transactionType)
    {
        return TransactionTypes.getTextValue(transactionType);
    }

    protected List<TransactionType> getAvailableTransactionTypeList(ITransaction transaction)
            throws IngenicoException
    {
        List<TransactionType> transactionTypeList;

        try
        {
            transactionTypeList = new ArrayList<>(
                    Lists.transform(transaction.getAvailableTransactionTypeList(),
                            x -> getTransactionType(x.getValue(), x.getLabel())));
        }
        catch (UnavailableApiException e)
        {
            transactionTypeList = new ArrayList<>(
                    Lists.transform(transaction.getAvailableTransactionTypes(),
                            x -> TransactionType.create(x, getTransactionTypeLabel(x))));
        }

        return transactionTypeList;
    }

    protected List<PaymentMean> getAvailablePaymentMeanList(ITransaction transaction)
            throws IngenicoException
    {
        List<PaymentMean> paymentMeanList;

        try
        {
            paymentMeanList = transaction.getAvailablePaymentMeansList();
        }
        catch (UnavailableApiException e)
        {
            paymentMeanList = new ArrayList<>(
                    Lists.transform(transaction.getAvailablePaymentMeans(),
                            x -> PaymentMean.create(x, PaymentMeans.getTextValue(x), "")));
        }

        return paymentMeanList;
    }

    protected List<String> getAvailablePaymentApplicationList(List<PaymentMean> paymentMeans)
    {
        List<String> paymentApplications = new ArrayList<>();

        for (PaymentMean paymentMean : paymentMeans)
        {
            String serviceClass = paymentMean.getServiceClass();
            if (!TextUtils.isEmpty(serviceClass) && !paymentApplications
                    .contains(serviceClass))
            {
                paymentApplications.add(serviceClass);
            }
        }

        return paymentApplications;
    }

    protected Map<String, List<Pair<Integer, String>>> getAvailableApplicationMeans(
            List<PaymentMean> paymentMeans)
    {

        Map<String, List<Pair<Integer, String>>> applicationMeans = new HashMap<>();
        for (PaymentMean paymentMean : paymentMeans)
        {
            String serviceClass = paymentMean.getServiceClass();
            if (!TextUtils.isEmpty(serviceClass))
            {
                List<Pair<Integer, String>> paymentMeanPairList = new ArrayList<>();
                if (applicationMeans.containsKey(serviceClass))
                {
                    paymentMeanPairList.addAll(applicationMeans.get(serviceClass));
                }
                paymentMeanPairList
                        .add(Pair.create(paymentMean.getValue(), paymentMean.getLabel()));
                applicationMeans.put(serviceClass, paymentMeanPairList);
            }
        }

        return applicationMeans;
    }

    protected List<Pair<Integer, String>> getAvailablePaymentMeans(
            List<PaymentMean> paymentMeanList)
    {
        List<Pair<Integer, String>> paymentMeans = new ArrayList<>();
        for (PaymentMean paymentMean : paymentMeanList)
        {
            if (!paymentMeans
                    .contains(Pair.create(paymentMean.getValue(), paymentMean.getLabel())))
            {
                paymentMeans.add(Pair.create(paymentMean.getValue(), paymentMean.getLabel()));
            }
        }

        return paymentMeans;
    }

    protected List<String> getPaymentMeanItems(List<Pair<Integer, String>> paymentMeanList)
    {
        List<String> result = new ArrayList<>();
        for (Pair<Integer, String> paymentMean : paymentMeanList)
        {
            result.add(paymentMean.second);
        }

        return result;
    }

    protected StringBuilder getTransactionResultFields(TransactionResult result)
    {
        List<Pair<String, String>> fieldsValues = new ArrayList<>();
        fieldsValues.add(Pair.create("Result status", result.getStatus().toString()));
        fieldsValues.add(Pair.create("Currency", createResult(result.getCurrency())));
        if (result.isOnlineTransaction() != null)
        {
            fieldsValues.add(Pair.create("Authorization type",
                    createResult(result.isOnlineTransaction() ? "Online" : "Offline")));
        }
        fieldsValues.add(Pair.create("Authorized amount",
                createResult(result.getAuthorizedAmount())));
        if (result.getAmount() != null)
        {
            fieldsValues.add(Pair.create("Amount", createResult(result.getAmount())));
        }
        if (result.getSurchargeAmount() != null)
        {
            fieldsValues.add(Pair.create("Surcharge amount",
                    createResult(result.getSurchargeAmount())));
        }
        fieldsValues.add(Pair.create("Cashback amount",
                createResult(result.getCashbackAmount())));
        fieldsValues.add(Pair.create("Tip amount", createResult(result.getTipAmount())));
        fieldsValues.add(Pair.create("Donation amount",
                createResult(result.getDonationAmount())));
        fieldsValues.add(Pair.create("Other fees amount",
                createResult(result.getOtherFeesAmount())));
        fieldsValues.add(Pair.create("Tendered cash amount",
                createResult(result.getCashAmount())));
        fieldsValues.add(Pair.create("Total amount", createResult(result.getTotalAmount())));
        fieldsValues.add(Pair.create("Effective payment mean",
                getPaymentMeanLabel(result.getEffectivePaymentMean())));
        fieldsValues.add(Pair.create("Transaction id",
                createResult(result.getTransactionId())));
        fieldsValues.add(Pair.create("Invoice id", createResult(result.getInvoiceId())));
        fieldsValues.add(Pair.create("Product code id",
                createResult(result.getProductCodeId())));
        fieldsValues.add(Pair.create("Action code", createResult(result.getActionCode())));
        fieldsValues.add(Pair.create("Stan", createResult(result.getStan())));
        fieldsValues.add(Pair.create("Card type", getCardTypeLabel(result.getCardType())));
        fieldsValues.add(Pair.create("Moto type", getMotoTypeLabel(result.getMotoType())));
        fieldsValues.add(Pair.create("Masked pan", createResult(result.getMaskedPan())));
        fieldsValues.add(Pair.create("Issuer identification number",
                createResult(result.getIssuerIdentificationNumber())));
        fieldsValues.add(Pair.create("Retrieval reference number",
                createResult(result.getRetrievalReferenceNumber())));
        fieldsValues.add(Pair.create("Ngo id", createResult(result.getNgoId())));
        fieldsValues.add(Pair.create("Approval code", createResult(result.getApprovalCode())));
        fieldsValues.add(Pair.create("Expiration date",
                createResult(result.getExpirationDate())));
        fieldsValues.add(Pair.create("Acquirer token",
                bytesToHexString(result.getAcquirerToken())));
        if (result.getUsedApplication() != null)
        {
            fieldsValues.add(Pair.create("Service class name",
                    result.getUsedApplication().getServiceClassName()));
            fieldsValues.add(Pair.create("Application identifier", String.format("%02X",
                    result.getUsedApplication().getApplicationIdentifier())));
        }
        fieldsValues.add(Pair.create("Customer ID", createResult(result.getCustomerId())));
        fieldsValues.add(Pair.create("Account ID", createResult(result.getAccountId())));
        fieldsValues.add(Pair.create("Customer status", createResult(result.getCustomerStatus())));

        if (result.getTerminalId() != null)
        {
            fieldsValues.add(Pair.create("Terminal id", createResult(result.getTerminalId())));
        }
        if (result.getAcquirerCode() != null)
        {
            fieldsValues.add(Pair.create("Acquirer code", createResult(result.getAcquirerCode())));
        }
        fieldsValues.add(Pair.create("Merchant id", createResult(result.getMerchantId())));
        fieldsValues.add(Pair.create("TimeStamp", createResult(result.getTimeStamp())));
        if (result.getPromotionalMessage() != null)
        {
            fieldsValues.add(Pair
                    .create("Promotional message", createResult(result.getPromotionalMessage())));
        }
        fieldsValues.add(Pair
                .create("Installment number", createResult(result.getInstallmentNumber())));
        fieldsValues.add(Pair.create("Receipt number", createResult(result.getReceiptNumber())));

        // EMV fields
        fieldsValues.add(Pair.create("EMV card AID", createResult(result.getEmvCardAid())));
        fieldsValues.add(Pair.create("EMV PAN Sequence number",
                createResult(result.getEmvPanSequenceNumber())));
        fieldsValues.add(Pair.create("EMV ATC", createResult(result.getEmvAtc())));
        fieldsValues.add(Pair.create("EMV TVR", bytesToHexString(result.getEmvTvr())));
        fieldsValues.add(Pair.create("EMV TSI", bytesToHexString(result.getEmvTsi())));
        fieldsValues.add(Pair.create("EMV CVM Results",
                bytesToHexString(result.getEmvCvmResults())));

        StringBuilder htmlBuilder = new StringBuilder();
        for (Pair<String, String> value : fieldsValues)
        {
            htmlBuilder
                    .append(forHtml(
                            String.format("<b>%s</b>\t\t%s<br>\n", value.first, value.second)));
        }

        return htmlBuilder;
    }

    private String createResult(Object content)
    {
        if (content == null)
        {
            return NO_DATA;
        }
        else
        {
            return content.toString();
        }
    }

    private String forHtml(String text)
    {
        return text.replaceAll("\t", "&nbsp;&nbsp;&nbsp;&nbsp;");
    }

    private TransactionType getTransactionType(int value, String label)
    {
        return TransactionType.create(value, !TextUtils.isEmpty(label) ? label : TransactionTypes
                .getTextValue(value));
    }
}
