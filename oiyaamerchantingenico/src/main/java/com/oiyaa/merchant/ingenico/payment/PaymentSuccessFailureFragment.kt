package com.oiyaa.merchant.ingenico.payment

import android.content.res.ColorStateList
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.localvalu.coremerchantapp.R
import com.localvalu.coremerchantapp.data.output.order.OrderDetails
import com.localvalu.coremerchantapp.utils.AppUtils
import com.oiyaa.merchant.ingenico.databinding.FragmentPaymentSuccessFailureBinding
import com.oiyaa.merchant.ingenico.payment.viewmodel.PaymentViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PaymentSuccessFailureFragment : Fragment()
{
    private val paymentViewModel: PaymentViewModel by activityViewModels()
    private var binding:FragmentPaymentSuccessFailureBinding?=null
    private var orderDetails:OrderDetails?=null
    private val colorPrimary by lazy {
        resources.getColor(R.color.colorPrimary)
    }
    private val colorRed by lazy {
        resources.getColor(R.color.redDark)
    }

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        arguments?.let { bundle->
            orderDetails =bundle.getParcelable(AppUtils.BUNDLE_ORDER_DETAILS)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?
    {
        binding = FragmentPaymentSuccessFailureBinding.inflate(inflater,container,false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        setProperties()
    }

    private fun setProperties()
    {
        binding?.ivPaymentSuccess?.setOnClickListener{
            paymentViewModel.setPaymentState(false)
        }
        observeSubscribers()
    }

    private fun observeSubscribers()
    {
        paymentViewModel.paymentState.observe(viewLifecycleOwner,Observer{
            if(it)
            {
                binding?.ivPaymentSuccess?.setImageDrawable(resources.getDrawable(R.drawable.ic_baseline_check_circle_24))
                binding?.ivPaymentSuccess?.imageTintList= ColorStateList.valueOf(colorPrimary)
                binding?.tvPaymentSuccess?.setTextColor((ColorStateList.valueOf(colorPrimary)))
                binding?.tvPaymentSuccess?.setText(getString(R.string.lbl_transaction_success))
            }
            else
            {
                binding?.ivPaymentSuccess?.setImageDrawable(resources.getDrawable(R.drawable.ic_baseline_error_24))
                binding?.ivPaymentSuccess?.imageTintList= ColorStateList.valueOf(colorRed)
                binding?.tvPaymentSuccess?.setText(getString(R.string.lbl_transaction_failed))
            }
        })
    }

}