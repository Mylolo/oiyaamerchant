package com.oiyaa.merchant.ingenico.payment

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.util.Pair
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.activityViewModels
import com.google.common.collect.ImmutableMap
import com.google.common.collect.Lists
import com.ingenico.sdk.transaction.ITransactionDoneListener
import com.oiyaa.merchant.ingenico.widget.OptionalSpinner
import com.oiyaa.merchant.ingenico.widget.OptionalMultipleSpinner
import com.oiyaa.merchant.ingenico.widget.OptionalTextInput
import com.ingenico.sdk.transaction.constants.CardTypes
import com.ingenico.sdk.transaction.constants.MotoType
import com.ingenico.sdk.transaction.ITransaction
import com.oiyaa.merchant.ingenico.utils.TransactionUtils
import com.oiyaa.merchant.ingenico.widget.SpinnerUtils
import com.oiyaa.merchant.ingenico.widget.MultiSelectionSpinner.OnMultipleItemsSelectedListener
import com.ingenico.sdk.IngenicoException
import com.ingenico.sdk.transaction.Transaction
import com.ingenico.sdk.transaction.constants.TransactionTypes
import com.ingenico.sdk.transaction.data.*
import com.ingenico.sdk.transaction.data.Currency
import com.oiyaa.merchant.ingenico.R
import com.oiyaa.merchant.ingenico.tools.Keyboard
import com.oiyaa.merchant.ingenico.payment.viewmodel.IngenicoAPIModel
import java.lang.Exception
import java.lang.IllegalArgumentException
import java.lang.NullPointerException
import java.math.BigDecimal
import java.util.*

class TransactionIngenicoFragment : PaymentIngenicoFragment(), ITransactionDoneListener
{
    private enum class PaymentState
    {
        NOT_READY, READY, IN_PROGRESS, DONE
    }

    private val ingenicoAPIModel: IngenicoAPIModel by activityViewModels()
    private var stateViews: Map<PaymentState, View>? = null
    private var amountText: EditText? = null
    private var recoveryIdLabel: TextView? = null
    private var currentRecoveryId: Int? = null
    private var currencySpinner: Spinner? = null
    private var transactionTypeSpinner: OptionalSpinner? = null
    private var transactionTypeSwitch: Switch? = null
    private var technoSpinner: OptionalMultipleSpinner? = null
    private var cardTypeSpinner: OptionalSpinner? = null
    private var motoTypeSpinner: OptionalSpinner? = null
    private var transactionIdInput: OptionalTextInput? = null
    private var invoiceIdInput: OptionalTextInput? = null
    private var productCodeIdInput: OptionalTextInput? = null
    private var stanInput: OptionalTextInput? = null
    private var acqTransactionType: OptionalTextInput? = null
    private var retrievalReferenceNumberInput: OptionalTextInput? = null
    private var ngoIdInput: OptionalTextInput? = null
    private var approvalCodeInput: OptionalTextInput? = null
    private var amountCashbackInput: OptionalTextInput? = null
    private var amountTipInput: OptionalTextInput? = null
    private var amountDonationInput: OptionalTextInput? = null
    private var amountOtherFeesInput: OptionalTextInput? = null
    private var amountSurchargeInput: OptionalTextInput? = null
    private var amountTenderedCashInput: OptionalTextInput? = null
    private var merchantIdInput: OptionalTextInput? = null
    private var timeStampInput: OptionalTextInput? = null
    private var installmentNumberInput: OptionalTextInput? = null
    private var receiptNumberInput: OptionalTextInput? = null
    private var cashierIdentifierInput: OptionalTextInput? = null
    private var cardholderNameInput: OptionalTextInput? = null
    private var paymentApplicationInput: OptionalTextInput? = null
    private var paymentApplicationsInput: OptionalMultipleSpinner? = null
    private var forceAuthoSpinner: OptionalSpinner? = null
    private var initialPreauthAmountInput: OptionalTextInput? = null
    private var buttonStartTransaction: Button? = null
    private var buttonAbortTransaction: Button? = null
    private var buttonTransactionResultOk: Button? = null
    private var buttonReloadValues: Button? = null
    private var resultContentView: TextView? = null
    private var currencies: List<Currency>? = null
    private var availableTransactionTypes: List<TransactionType>? = null
    private var allTransactionTypes: List<TransactionType>? = null
    private var availablePaymentMeans: List<Pair<Int, String>>? = null
    private var paymentApplicationMeans: Map<String, List<Pair<Int, String>>>? = null
    private val cardTypes = Arrays.asList(
        CardTypes.ALL_CARD_TYPES,
        CardTypes.DEBIT,
        CardTypes.CREDIT,
        CardTypes.GIFT,
        CardTypes.EBT,
        CardTypes.LTM_CARD
    )
    private val motoTypes = Arrays.asList(
        MotoType.NO,
        MotoType.MANUAL,
        MotoType.ECOM,
        MotoType.MOTO
    )
    private var transaction: ITransaction? = null
    private var transactionUtils: TransactionUtils? = null

    private fun setTransactionStarted(transactionStarted: Boolean)
    {
        runOnUiThread {
            if (buttonStartTransaction != null)
            {
                buttonStartTransaction!!.isEnabled = !transactionStarted
            }
        }
    }

    override fun onServiceReady()
    {
        super.onServiceReady()
        Log.d(TAG, "onServiceReady")
        transaction = Transaction.getInstance(context!!.applicationContext)
        loadValues()
        transaction?.registerTransactionDoneListener(this)
    }

    override fun onAttach(context: Context)
    {
        super.onAttach(context)
        transactionUtils = TransactionUtils(context)
    }

    override fun onDetach()
    {
        super.onDetach()
        transactionUtils = null
    }

    override fun onDestroy()
    {
        super.onDestroy()
        if (transaction != null)
        {
            transaction!!.unregisterTransactionDoneListener(this)
        }
    }

    override fun initView(
        inflater: LayoutInflater,
        container: ViewGroup,
        savedInstanceState: Bundle
    ): View
    {
        val root = inflater.inflate(R.layout.fragment_transaction, container, false)
        amountText = root.findViewById(R.id.amountText)
        recoveryIdLabel = root.findViewById(R.id.requestRecoveryId)
        currencySpinner = root.findViewById(R.id.currencySpinner)
        transactionTypeSpinner = root.findViewById(R.id.transactionTypeListSpinner)
        transactionTypeSwitch = root.findViewById(R.id.transactionTypeSwitch)
        technoSpinner = root.findViewById(R.id.technoSpinner)
        cardTypeSpinner = root.findViewById(R.id.cardTypeSpinner)
        motoTypeSpinner = root.findViewById(R.id.motoTypeSpinner)
        transactionIdInput = root.findViewById(R.id.transactionId)
        invoiceIdInput = root.findViewById(R.id.invoiceId)
        productCodeIdInput = root.findViewById(R.id.productCodeId)
        stanInput = root.findViewById(R.id.stan)
        acqTransactionType = root.findViewById(R.id.acquirerTransactionType)
        retrievalReferenceNumberInput = root.findViewById(R.id.retrievalReferenceNumber)
        ngoIdInput = root.findViewById(R.id.ngoId)
        approvalCodeInput = root.findViewById(R.id.approvalCode)
        amountCashbackInput = root.findViewById(R.id.amountCashback)
        amountTipInput = root.findViewById(R.id.amountTip)
        amountDonationInput = root.findViewById(R.id.amountDonation)
        amountOtherFeesInput = root.findViewById(R.id.amountOtherFees)
        amountSurchargeInput = root.findViewById(R.id.amountSurcharge)
        amountTenderedCashInput = root.findViewById(R.id.amountTenderedCash)
        merchantIdInput = root.findViewById(R.id.merchantId)
        timeStampInput = root.findViewById(R.id.timeStamp)
        installmentNumberInput = root.findViewById(R.id.installmentNumber)
        receiptNumberInput = root.findViewById(R.id.receiptNumber)
        cashierIdentifierInput = root.findViewById(R.id.cashierId)
        cardholderNameInput = root.findViewById(R.id.cardholderName)
        paymentApplicationInput = root.findViewById(R.id.paymentApplication)
        paymentApplicationsInput = root.findViewById(R.id.paymentApplications)
        forceAuthoSpinner = root.findViewById(R.id.forceAuthoSpinner)
        initialPreauthAmountInput = root.findViewById(R.id.initialPreAuthAmount)
        buttonStartTransaction = root.findViewById(R.id.buttonStartTransaction)
        buttonAbortTransaction = root.findViewById(R.id.buttonAbortTransaction)
        buttonTransactionResultOk = root.findViewById(R.id.buttonTransactionResultOk)
        buttonReloadValues = root.findViewById(R.id.buttonReloadValues)
        resultContentView = root.findViewById(R.id.transactionResultContent)
        stateViews = ImmutableMap.of(
            PaymentState.NOT_READY, root.findViewById(R.id.transactionNotReadyBlock),
            PaymentState.READY, root.findViewById(R.id.transactionNotStartedBlock),
            PaymentState.IN_PROGRESS, root.findViewById(R.id.transactionInProgressBlock),
            PaymentState.DONE, root.findViewById(R.id.transactionDoneBlock)
        )
        forceAuthoSpinner?.setAdapter(
            SpinnerUtils.stringListAdapter(
                context,
                Lists.newArrayList("False", "True")
            )
        )
        transactionTypeSwitch?.setOnClickListener(View.OnClickListener { view: View ->
            onTransactionTypeSwitch(
                view
            )
        })
        buttonStartTransaction?.setOnClickListener(View.OnClickListener { view: View ->
            onStartTransaction(
                view
            )
        })
        buttonAbortTransaction?.setOnClickListener(View.OnClickListener { view: View ->
            onAbortTransaction(
                view
            )
        })
        buttonTransactionResultOk?.setOnClickListener(View.OnClickListener { v: View? ->
            changePaymentState(
                PaymentState.READY
            )
        })
        buttonReloadValues?.setOnClickListener(View.OnClickListener { v: View? -> loadValues() })

        recoveryIdLabel?.setOnClickListener{
            val id = currentRecoveryId.toString()
            val clipboardManager = context?.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            val clip = ClipData.newPlainText(id, id)
            clipboardManager.setPrimaryClip(clip)
            Toast.makeText(context,"Recovery",Toast.LENGTH_LONG).show()
        }
        paymentApplicationsInput?.setListener(object : OnMultipleItemsSelectedListener
            {
                override fun selectedIndices(indices: List<Int>)
                {
                    // No need to implement
                }

                override fun selectedStrings(strings: List<String>)
                {
                    if (!strings.isEmpty())
                    {
                        technoSpinner?.setItems(getPaymentMeanItems(getFilteredPaymentMeans(strings)))
                    } else if (!paymentApplicationMeans!!.isEmpty())
                    {
                        technoSpinner?.setItems(getPaymentMeanItems(availablePaymentMeans))
                    }
                }
            })
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        super.onViewCreated(view, savedInstanceState)
        observeSubscribers()
    }

    private fun observeSubscribers()
    {
        ingenicoAPIModel.serviceReady.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            if(it)
            {
                onServiceReady()
            }
        })
    }

    override fun getName(): String
    {
        return "Transaction"
    }

    private fun onStartTransaction(view: View)
    {
        setTransactionStarted(true)
        Thread {
            try
            {
                var builder = TransactionInputData.builder()
                    .setAmount(BigDecimal(amountText!!.text.toString()))
                    .setCurrency(
                        currencies!![currencySpinner!!.selectedItemPosition]
                            .numericCode.toLong()
                    )
                if (transactionTypeSpinner!!.isChecked)
                {
                    builder = if (transactionTypeSwitch!!.isChecked)
                    {
                        builder.setTransactionType(
                            allTransactionTypes!![transactionTypeSpinner!!
                                .getSelectedItemPosition()]
                                .value
                        )
                    } else
                    {
                        builder.setTransactionType(
                            availableTransactionTypes!![transactionTypeSpinner!!
                                .getSelectedItemPosition()]
                                .value
                        )
                    }
                }
                if (transactionIdInput!!.isChecked)
                {
                    builder = builder.setTransactionId(transactionIdInput!!.text.toString())
                }
                if (invoiceIdInput!!.isChecked)
                {
                    builder = builder.setInvoiceId(invoiceIdInput!!.text.toString())
                }
                if (productCodeIdInput!!.isChecked)
                {
                    builder = builder.setProductCodeId(productCodeIdInput!!.text.toString())
                }
                if (stanInput!!.isChecked)
                {
                    builder = builder.setStan(stanInput!!.text.toString())
                }
                if (cardTypeSpinner!!.isChecked)
                {
                    builder = builder.setCardType(
                        cardTypes[cardTypeSpinner!!.selectedItemPosition]
                    )
                }
                if (motoTypeSpinner!!.isChecked)
                {
                    builder = builder.setMotoType(
                        motoTypes[motoTypeSpinner!!.selectedItemPosition]
                    )
                }
                if (acqTransactionType!!.isChecked)
                {
                    builder = builder.setAcquirerTransactionType(
                        acqTransactionType!!.text.toString().toInt()
                    )
                }
                if (forceAuthoSpinner!!.isChecked)
                {
                    builder = builder.setForceAutho(forceAuthoSpinner!!.selectedItemPosition > 0)
                }
                if (retrievalReferenceNumberInput!!.isChecked)
                {
                    builder = builder.setRetrievalReferenceNumber(
                        retrievalReferenceNumberInput!!.text.toString()
                    )
                }
                if (ngoIdInput!!.isChecked)
                {
                    builder = builder.setNgoId(ngoIdInput!!.text.toString())
                }
                if (approvalCodeInput!!.isChecked)
                {
                    builder = builder.setApprovalCode(approvalCodeInput!!.text.toString())
                }
                if (amountCashbackInput!!.isChecked)
                {
                    builder = builder.setCashbackAmount(
                        BigDecimal(amountCashbackInput!!.text.toString())
                    )
                }
                if (amountTipInput!!.isChecked)
                {
                    builder = builder.setTipAmount(
                        BigDecimal(amountTipInput!!.text.toString())
                    )
                }
                if (amountDonationInput!!.isChecked)
                {
                    builder = builder.setDonationAmount(
                        BigDecimal(amountDonationInput!!.text.toString())
                    )
                }
                if (amountOtherFeesInput!!.isChecked)
                {
                    builder = builder.setOtherFeesAmount(
                        BigDecimal(amountOtherFeesInput!!.text.toString())
                    )
                }
                if (amountSurchargeInput!!.isChecked)
                {
                    builder = builder.setSurchargeAmount(
                        BigDecimal(amountSurchargeInput!!.text.toString())
                    )
                }
                if (amountTenderedCashInput!!.isChecked)
                {
                    builder = builder.setCashAmount(
                        BigDecimal(amountTenderedCashInput!!.text.toString())
                    )
                }
                val applications: MutableList<String> = ArrayList()
                if (paymentApplicationInput!!.isChecked && !paymentApplicationInput!!.text
                        .toString().isEmpty()
                )
                {
                    applications.addAll(
                        Arrays.asList(
                            *paymentApplicationInput!!.text.toString()
                                .split(SEPARATOR.toRegex()).toTypedArray()
                        )
                    )
                }
                if (paymentApplicationsInput!!.isChecked)
                {
                    applications.addAll(paymentApplicationsInput!!.selectedStrings)
                }
                if (!applications.isEmpty())
                {
                    builder = builder.setPaymentApplications(applications)
                }
                if (technoSpinner!!.isChecked)
                {
                    for (value in technoSpinner!!.selectedIndices)
                    {
                        builder = builder.addPaymentMean(
                            availablePaymentMeans!![value].first
                        )
                    }
                }
                if (merchantIdInput!!.isChecked)
                {
                    builder = builder.setMerchantId(merchantIdInput!!.text.toString())
                }
                if (timeStampInput!!.isChecked)
                {
                    builder = builder.setTimeStamp(timeStampInput!!.text.toString())
                }
                if (installmentNumberInput!!.isChecked)
                {
                    builder = builder.setInstallmentNumber(
                        installmentNumberInput!!.text.toString()
                    )
                }
                if (receiptNumberInput!!.isChecked)
                {
                    builder = builder.setReceiptNumber(receiptNumberInput!!.text.toString())
                }
                if (cashierIdentifierInput!!.isChecked)
                {
                    builder = builder.setCashierId(cashierIdentifierInput!!.text.toString())
                }
                if (cardholderNameInput!!.isChecked)
                {
                    builder = builder.setCardholderName(cardholderNameInput!!.text.toString())
                }
                if (initialPreauthAmountInput!!.isChecked)
                {
                    builder = builder.setInitialPreauthAmount(
                        BigDecimal(initialPreauthAmountInput!!.text.toString())
                    )
                }
                val request = transaction!!.start(builder.build())
                Log.d(TAG, "Request result: $request")
                if (request.isRequestAccepted)
                {
                    currentRecoveryId = request.recoveryId
                    if (transactionUtils != null)
                    {
                        transactionUtils!!.saveLatestTransactionID(currentRecoveryId.toString())
                    }
                    runOnUiThread {
                        if (currentRecoveryId != null)
                        {
                            recoveryIdLabel!!.text =
                                String.format("Recovery ID: %d", currentRecoveryId)
                        } else
                        {
                            recoveryIdLabel!!.text = ""
                        }
                    }
                    Keyboard.hideSoftwareKeyboard(context, activity!!.currentFocus)
                    changePaymentState(PaymentState.IN_PROGRESS)
                } else
                {
                    toast("Transaction request NOT accepted: " + request.transactionStatus)
                    Log.e(
                        TAG,
                        "Transaction request not accepted: " + request.transactionStatus
                    )
                    setTransactionStarted(false)
                }
            } catch (e: IllegalArgumentException)
            {
                setTransactionStarted(false)
                toast("Exception: " + e.message)
                Log.e(TAG, "Error during start transaction", e)
            } catch (e: IngenicoException)
            {
                setTransactionStarted(false)
                toast("Exception: " + e.message)
                Log.e(TAG, "Error during start transaction", e)
            }
        }.start()
    }

    private fun onAbortTransaction(view: View)
    {
        Thread {
            try
            {
                val aborted = transaction!!.abortOngoingTransaction()
                toast("Transaction abort request result: $aborted")
            } catch (e: Exception)
            {
                toast("Exception: " + e.message)
                Log.e(TAG, "Error during transaction abortion", e)
            }
        }.start()
    }

    private fun loadValues()
    {
        object : Thread()
        {
            override fun run()
            {
                runOnUiThread { buttonReloadValues!!.isEnabled = false }
                try
                {
                    // retrieve data & populate spinners
                    currencies = transaction!!.availableCurrencies
                    val currencyItems = Lists.transform(currencies) { obj: Currency? -> obj!!.code }
                    availableTransactionTypes = getAvailableTransactionTypeList(transaction)
                    val transactionTypeItems =
                        Lists.transform(availableTransactionTypes) { obj: TransactionType? -> obj!!.label }
                    allTransactionTypes =
                        addUnavailableTransactionTypeList(availableTransactionTypes)
                    val paymentMeanList = getAvailablePaymentMeanList(transaction)
                    val paymentApplications = getAvailablePaymentApplicationList(paymentMeanList)
                    availablePaymentMeans = getAvailablePaymentMeans(paymentMeanList)
                    paymentApplicationMeans = getAvailableApplicationMeans(paymentMeanList)
                    runOnUiThread {
                        currencySpinner!!.adapter =
                            SpinnerUtils.stringListAdapter(context, currencyItems)
                        transactionTypeSpinner
                            ?.setAdapter(
                                SpinnerUtils.stringListAdapter(
                                    context,
                                    transactionTypeItems
                                )
                            )
                        paymentApplicationsInput!!.setItems(paymentApplications)
                        technoSpinner!!.setItems(getPaymentMeanItems(availablePaymentMeans))
                        cardTypeSpinner!!.setAdapter(
                            SpinnerUtils.stringListAdapter(context,
                                Lists.transform(cardTypes) { cardType: Int? ->
                                    getCardTypeLabel(
                                        cardType
                                    )
                                })
                        )
                        motoTypeSpinner!!.setAdapter(
                            SpinnerUtils.stringListAdapter(context,
                                Lists.transform(motoTypes) { motoType: Int? ->
                                    getMotoTypeLabel(
                                        motoType
                                    )
                                })
                        )
                        if (!currencies?.isEmpty()!! && !availableTransactionTypes?.isEmpty()!!
                            && !availablePaymentMeans?.isEmpty()!!
                        )
                        {
                            buttonStartTransaction!!.isEnabled = true
                        }
                    }
                    Keyboard.hideSoftwareKeyboard(context, activity!!.currentFocus)
                    changePaymentState(PaymentState.READY)
                } catch (e: IngenicoException)
                {
                    toast("Error retrieving transaction infos: " + e.message)
                    Log.e(TAG, "Error retrieving transaction infos", e)
                    Keyboard.hideSoftwareKeyboard(context, activity!!.currentFocus)
                    changePaymentState(PaymentState.NOT_READY)
                } catch (e: NullPointerException)
                {
                    toast("Error retrieving transaction infos: " + e.message)
                    Log.e(TAG, "Error retrieving transaction infos", e)
                    Keyboard.hideSoftwareKeyboard(context, activity!!.currentFocus)
                    changePaymentState(PaymentState.NOT_READY)
                }
                runOnUiThread { buttonReloadValues!!.isEnabled = true }
            }
        }.start()
    }

    override fun onTransactionDone(result: TransactionResult)
    {
        setTransactionStarted(false)
        runOnUiThread {
            val htmlBuilder = getTransactionResultFields(result)
            if (Build.VERSION.SDK_INT >= 24)
            {
                resultContentView!!.text = Html.fromHtml(
                    htmlBuilder.toString(),
                    Html.TO_HTML_PARAGRAPH_LINES_INDIVIDUAL
                )
            } else
            {
                resultContentView!!.text = Html.fromHtml(htmlBuilder.toString())
            }
            changePaymentState(PaymentState.DONE)
        }
    }

    private fun changePaymentState(newState: PaymentState)
    {
        runOnUiThread {
            for ((_, value) in stateViews!!)
            {
                value.visibility = View.GONE
            }
            stateViews!![newState]!!.visibility = View.VISIBLE
            if (newState == PaymentState.NOT_READY || newState == PaymentState.READY)
            {
                buttonReloadValues!!.visibility = View.VISIBLE
            } else
            {
                buttonReloadValues!!.visibility = View.GONE
            }
            if (newState == PaymentState.DONE)
            {
                buttonAbortTransaction!!.visibility = View.GONE
            } else
            {
                buttonAbortTransaction!!.visibility = View.VISIBLE
            }
        }
    }

    private fun getFilteredPaymentMeans(applications: List<String>): List<Pair<Int, String>>
    {
        val paymentMeanList: MutableList<Pair<Int, String>> = ArrayList()
        for (application in applications)
        {
            if (paymentApplicationMeans!!.containsKey(application))
            {
                for (paymentMean in paymentApplicationMeans!!
                    .get(application)!!)
                {
                    if (!paymentMeanList.contains(paymentMean))
                    {
                        paymentMeanList.add(paymentMean)
                    }
                }
            }
        }
        return paymentMeanList
    }

    private fun addUnavailableTransactionTypeList(
        availableTransactionTypes: List<TransactionType>?
    ): List<TransactionType>
    {
        if (availableTransactionTypes == null || availableTransactionTypes.isEmpty())
        {
            return TransactionTypes
                .getPredefinedTransactionTypeList()
        }
        val result: MutableList<TransactionType> = ArrayList(availableTransactionTypes)
        for (transactionType in TransactionTypes
            .getPredefinedTransactionTypeList())
        {
            if (!containsTransactionTypeValue(
                    availableTransactionTypes,
                    transactionType.value
                )
            )
            {
                result.add(transactionType)
            }
        }
        return result
    }

    private fun onTransactionTypeSwitch(view: View)
    {
        if (transactionTypeSwitch!!.isChecked)
        {
            toast(transactionTypeSwitch!!.textOn.toString())
            transactionTypeSpinner!!.setAdapter(
                SpinnerUtils.stringListAdapter(context,
                    Lists.transform(allTransactionTypes) { obj: TransactionType? -> obj!!.label })
            )
        } else
        {
            toast(transactionTypeSwitch!!.textOff.toString())
            transactionTypeSpinner!!.setAdapter(
                SpinnerUtils.stringListAdapter(context,
                    Lists.transform(availableTransactionTypes) { obj: TransactionType? -> obj!!.label })
            )
        }
    }

    companion object
    {
        private val TAG = TransactionIngenicoFragment::class.java.name
        private const val SEPARATOR = ";"
        private fun containsTransactionTypeValue(
            transactionTypesList: List<TransactionType>,
            value: Int
        ): Boolean
        {
            for (transactionType in transactionTypesList)
            {
                if (transactionType.value == value)
                {
                    return true
                }
            }
            return false
        }
    }
}