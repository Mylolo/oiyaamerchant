package com.oiyaa.merchant.ingenico.payment.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class IngenicoAPIModel @Inject constructor() : ViewModel()
{
    private val TAG = "IngenicoAPIModel"
    private val _serviceReady: MutableLiveData<Boolean> = MutableLiveData()

    val serviceReady: LiveData<Boolean>
        get() = _serviceReady

    fun startIngenicoService(value:Boolean)
    {
        this._serviceReady.value=value
    }

}