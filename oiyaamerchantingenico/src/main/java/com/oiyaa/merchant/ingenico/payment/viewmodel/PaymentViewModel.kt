package com.oiyaa.merchant.ingenico.payment.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch

class PaymentViewModel : ViewModel()
{
    private val _paymentState: MutableLiveData<Boolean> = MutableLiveData()

    val paymentState: LiveData<Boolean>
        get() = _paymentState

    fun setPaymentState(value: Boolean) = viewModelScope.launch {
        _paymentState.value = (value)
    }
}