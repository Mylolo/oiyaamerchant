package com.oiyaa.merchant.ingenico.utils;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class TransactionUtils {

    private static final String PREFERENCES_NAME = "transaction_preferences";
    private static final String LATEST_TRANSACTION_ID = "latest_transaction_id";

    private final SharedPreferences preferences;

    public TransactionUtils(@NonNull Context context) {
        this.preferences = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
    }

    public void saveLatestTransactionID(@Nullable String id) {
        preferences.edit()
                .putString(LATEST_TRANSACTION_ID, id)
                .apply();
    }

    @Nullable
    public String getLatestTransactionId() {
        return preferences.getString(LATEST_TRANSACTION_ID, null);
    }

}
