package com.oiyaa.merchant.ingenico.widget;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.util.AttributeSet;
import android.widget.ArrayAdapter;
import android.widget.SpinnerAdapter;

import androidx.appcompat.widget.AppCompatSpinner;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Custom spinner to have multiple selections
 * Taken from https://github.com/GunaseelanArumaikkannu/MultiSpinner
 */
public class MultiSelectionSpinner extends AppCompatSpinner implements OnMultiChoiceClickListener {

    public interface OnMultipleItemsSelectedListener {
        void selectedIndices(List<Integer> indices);

        void selectedStrings(List<String> strings);
    }

    private OnMultipleItemsSelectedListener listener;

    private String[] items = null;
    private boolean[] selection = null;
    private boolean[] selectionAtStart = null;
    private String itemsAtStart = null;

    private ArrayAdapter<String> adapter;

    public MultiSelectionSpinner(Context context) {
        this(context, null);
    }

    public MultiSelectionSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);

        adapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item);
        super.setAdapter(adapter);
    }

    public void setListener(OnMultipleItemsSelectedListener listener) {
        this.listener = listener;
    }

    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
        if (selection != null && which < selection.length) {
            selection[which] = isChecked;
            adapter.clear();
            adapter.add(buildSelectedItemString());
        } else {
            throw new IllegalArgumentException("Argument 'which' is out of bounds.");
        }
    }

    @Override
    public boolean performClick() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMultiChoiceItems(items, selection, this);
        itemsAtStart = getSelectedItemsAsString();
        builder.setPositiveButton("Submit", (dialog, which) -> {
            System.arraycopy(selection, 0, selectionAtStart, 0, selection.length);
            if (listener != null) {
                listener.selectedIndices(getSelectedIndices());
                listener.selectedStrings(getSelectedStrings());
            }
        });
        builder.setNegativeButton("Cancel", (dialog, which) -> {
            adapter.clear();
            adapter.add(itemsAtStart);
            System.arraycopy(selectionAtStart, 0, selection, 0, selectionAtStart.length);
        });
        builder.show();
        return true;
    }

    @Override
    public void setAdapter(SpinnerAdapter adapter) {
        throw new IllegalStateException("setAdapter is not supported by MultiSelectSpinner.");
    }

    public void setItems(List<String> items) {
        this.items = items.toArray(new String[items.size()]);
        selection = new boolean[this.items.length];
        selectionAtStart = new boolean[this.items.length];
        adapter.clear();
        Arrays.fill(selection, false);
    }

    @Override
    public void setSelection(int index) {
        for (int i = 0; i < selection.length; i++) {
            selection[i] = false;
            selectionAtStart[i] = false;
        }
        if (index >= 0 && index < selection.length) {
            selection[index] = true;
            selectionAtStart[index] = true;
        } else {
            throw new IllegalArgumentException("Index " + index + " is out of bounds.");
        }
        adapter.clear();
        adapter.add(buildSelectedItemString());
    }

    public void setSelection(int[] selectedIndices) {
        for (int i = 0; i < selection.length; i++) {
            selection[i] = false;
            selectionAtStart[i] = false;
        }
        for (int index : selectedIndices) {
            if (index >= 0 && index < selection.length) {
                selection[index] = true;
                selectionAtStart[index] = true;
            } else {
                throw new IllegalArgumentException("Index " + index + " is out of bounds.");
            }
        }
        adapter.clear();
        adapter.add(buildSelectedItemString());
    }

    public List<String> getSelectedStrings() {
        List<String> result = new LinkedList<>();
        for (int i = 0; i < items.length; ++i) {
            if (this.selection[i]) {
                result.add(items[i]);
            }
        }
        return result;
    }

    public List<Integer> getSelectedIndices() {
        List<Integer> result = new LinkedList<>();
        for (int i = 0; i < items.length; ++i) {
            if (this.selection[i]) {
                result.add(i);
            }
        }
        return result;
    }

    private String buildSelectedItemString() {
        StringBuilder sb = new StringBuilder();
        boolean foundOne = false;

        for (int i = 0; i < items.length; ++i) {
            if (selection[i]) {
                if (foundOne) {
                    sb.append(", ");
                }
                foundOne = true;
                sb.append(items[i]);
            }
        }
        return sb.toString();
    }

    public String getSelectedItemsAsString() {
        StringBuilder sb = new StringBuilder();
        boolean foundOne = false;

        for (int i = 0; i < items.length; ++i) {
            if (selection[i]) {
                if (foundOne) {
                    sb.append(", ");
                }
                foundOne = true;
                sb.append(items[i]);
            }
        }
        return sb.toString();
    }
}
