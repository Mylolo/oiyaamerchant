package com.oiyaa.merchant.ingenico.widget;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;


import com.oiyaa.merchant.ingenico.R;

import java.util.List;

public class OptionalMultipleSpinner extends OptionalWidget {
    private MultiSelectionSpinner spinner;

    public OptionalMultipleSpinner(
            @NonNull Context context) {
        this(context, null);
    }

    public OptionalMultipleSpinner(@NonNull Context context,
                                   @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public OptionalMultipleSpinner(@NonNull Context context, @Nullable AttributeSet attrs,
                                   int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public OptionalMultipleSpinner(@NonNull Context context, @Nullable AttributeSet attrs,
                                   int defStyleAttr,
                                   int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        spinner = findViewById(R.id.optional_spinner);
    }

    public void setListener(MultiSelectionSpinner.OnMultipleItemsSelectedListener listener) {
        spinner.setListener(listener);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.item_optional_multiplespinner;
    }

    public void setItems(List<String> items) {
        spinner.setItems(items);
    }

    public List<Integer> getSelectedIndices() {
        return spinner.getSelectedIndices();
    }

    public List<String> getSelectedStrings() {
        return spinner.getSelectedStrings();
    }
}
