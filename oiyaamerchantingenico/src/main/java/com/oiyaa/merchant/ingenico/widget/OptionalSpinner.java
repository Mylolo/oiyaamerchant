package com.oiyaa.merchant.ingenico.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.oiyaa.merchant.ingenico.R;


/**
 * Widget for arguments input, to display a checkbox, a label and a spinner.
 */
public class OptionalSpinner extends OptionalWidget {
    private Spinner spinner;

    public OptionalSpinner(@NonNull Context context) {
        this(context, null);
    }

    public OptionalSpinner(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public OptionalSpinner(@NonNull Context context, @Nullable AttributeSet attrs,
                           int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public OptionalSpinner(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr,
                           int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        spinner = findViewById(R.id.optional_spinner);
    }

    /**
     * Set the adapter on the spinner component.
     * @param adapter
     */
    public void setAdapter(SpinnerAdapter adapter) {
        spinner.setAdapter(adapter);
    }

    /**
     * Get the selected position on the spinner component.
     * @return
     */
    public int getSelectedItemPosition() {
        return spinner.getSelectedItemPosition();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.item_optional_spinner;
    }
}
