package com.oiyaa.merchant.ingenico.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.Editable;
import android.util.AttributeSet;
import android.widget.EditText;

import androidx.annotation.Nullable;

import com.oiyaa.merchant.ingenico.R;


/**
 * Widget for arguments input, to display a checkbox, a label and a text entry.
 */
public class OptionalTextInput extends OptionalWidget {
    private EditText editText;

    public OptionalTextInput(Context context) {
        this(context, null);
    }

    public OptionalTextInput(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public OptionalTextInput(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public OptionalTextInput(Context context, AttributeSet attrs, int defStyleAttr,
                             int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        editText = findViewById(R.id.optional_edit);
        initTextInputAttributes(context, attrs);
    }

    /**
     * Return the text content of the text entry component.
     * @return
     */
    public Editable getText() {
        return editText.getText();
    }

    private void initTextInputAttributes(Context context, AttributeSet attrs) {
        if (attrs == null) {
            return;
        }

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.OptionalTextInput);
        final int count = a.getIndexCount();
        for (int i = 0; i < count; i++) {
            int attr = a.getIndex(i);
            if (attr == R.styleable.OptionalTextInput_android_text) {
                editText.setText(a.getText(attr));
            } else if (attr == R.styleable.OptionalTextInput_android_inputType) {
                editText.setInputType(a.getInteger(attr, 0));
            }
        }

        a.recycle();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.item_optional_text_input;
    }
}
