package com.oiyaa.merchant.ingenico.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.oiyaa.merchant.ingenico.R;


/**
 * Base class to create widgets with a checkbox and a label.
 */
public abstract class OptionalWidget extends SaveStateFrameLayout {
    private final CheckBox checkBox;
    private final TextView textView;

    public OptionalWidget(@NonNull Context context) {
        this(context, null);
    }

    public OptionalWidget(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public OptionalWidget(@NonNull Context context, @Nullable AttributeSet attrs,
                          int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public OptionalWidget(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr,
                          int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        checkBox = findViewById(R.id.optional_checkbox);
        textView = findViewById(R.id.optional_label);
        initAttributes(context, attrs);
    }

    private void initAttributes(Context context, AttributeSet attrs) {
        if (attrs == null) {
            return;
        }

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.OptionalWidget);
        final int count = a.getIndexCount();
        for (int i = 0; i < count; i++) {
            int attr = a.getIndex(i);
            if (attr == R.styleable.OptionalWidget_android_checked) {
                checkBox.setChecked(a.getBoolean(attr, false));
            } else if (attr == R.styleable.OptionalWidget_label) {
                textView.setText(a.getString(attr));
            }
        }
        a.recycle();
    }

    /**
     * Return the state of the checkbox component.
     *
     * @return
     */
    public boolean isChecked() {
        return checkBox.isChecked();
    }
}
