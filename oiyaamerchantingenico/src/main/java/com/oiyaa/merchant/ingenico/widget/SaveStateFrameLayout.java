package com.oiyaa.merchant.ingenico.widget;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * When we have many instances of the same custom widget based on a custom layout into a
 * FrameLayout, the save/restore state mechanism can restore the value of the last one in all
 * items instead of keeping each one's values, because many views have the same id.
 *
 * To fix this, we implement onSaveInstanceState/onRestoreInstanceState ourselves.
 *
 * Example taken from https://trickyandroid.com/saving-android-view-state-correctly/
 */
public abstract class SaveStateFrameLayout extends FrameLayout {
    private final ViewGroup root;

    public SaveStateFrameLayout(@NonNull Context context, @Nullable AttributeSet attrs,
                                int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.root = (ViewGroup) LayoutInflater.from(context).inflate(getLayoutId(), null, false);
        addView(root);
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        Parcelable superState = super.onSaveInstanceState();
        SavedState ss = new SavedState(superState);
        if (root != null) {
            root.saveHierarchyState(ss.childrenStates);
        }
        return ss;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        SavedState ss = (SavedState) state;
        super.onRestoreInstanceState(ss.getSuperState());
        if (root != null) {
            root.restoreHierarchyState(ss.childrenStates);
        }
    }

    @Override
    protected void dispatchSaveInstanceState(SparseArray<Parcelable> container) {
        dispatchFreezeSelfOnly(container);
    }

    @Override
    protected void dispatchRestoreInstanceState(SparseArray<Parcelable> container) {
        dispatchThawSelfOnly(container);
    }

    /**
     * Must be overrided to return the desired layout id.
     *
     * @return
     */
    protected abstract int getLayoutId();

    private static class SavedState extends BaseSavedState {
        public static final ClassLoaderCreator<SavedState> CREATOR
                = new ClassLoaderCreator<SavedState>() {
            @Override
            public SavedState createFromParcel(Parcel source, ClassLoader loader) {
                return new SavedState(source, loader);
            }

            @Override
            public SavedState createFromParcel(Parcel source) {
                return createFromParcel(source, null);
            }

            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }
        };

        private SparseArray childrenStates = new SparseArray();

        SavedState(Parcelable superState) {
            super(superState);
        }

        private SavedState(Parcel in, ClassLoader classLoader) {
            super(in);
            childrenStates = in.readSparseArray(classLoader);
        }

        @Override
        public void writeToParcel(Parcel out, int flags) {
            super.writeToParcel(out, flags);
            out.writeSparseArray(childrenStates);
        }
    }
}
