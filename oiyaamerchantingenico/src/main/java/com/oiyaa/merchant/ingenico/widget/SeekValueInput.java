package com.oiyaa.merchant.ingenico.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.SeekBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.oiyaa.merchant.ingenico.R;


public class SeekValueInput extends SaveStateFrameLayout {

    private int minimumValue = 0;
    private int maximumValue = 100;
    private int initialValue = 0;

    private final SeekBar seekBar;
    private final EditText editText;

    private SeekBar.OnSeekBarChangeListener onSeekBarChangeListener = new SeekBar
            .OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            if (fromUser) {
                editText.setText(String.valueOf(percentToValue(progress)));
            }
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            // Do nothing
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            // Do nothing
        }

        private int percentToValue(int percent) {
            int gap = maximumValue - minimumValue;
            return (percent * gap) / 100 + minimumValue;
        }
    };

    private OnFocusChangeListener editTextFocusChangeListener = (v, hasFocus) -> {
        if (!hasFocus) {
            fixEditedValue();
        }
    };

    public SeekValueInput(@NonNull Context context) {
        this(context, null);
    }

    public SeekValueInput(@NonNull Context context,
                          @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SeekValueInput(@NonNull Context context, @Nullable AttributeSet attrs,
                          int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public SeekValueInput(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr,
                          int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        seekBar = findViewById(R.id.seekBar);
        editText = findViewById(R.id.valueEdit);

        initAttributes(context, attrs);

        if (maximumValue <= minimumValue) {
            throw new IllegalArgumentException("SeekValueInput must have minimum < maximum");
        }

        setValue(initialValue);

        seekBar.setOnSeekBarChangeListener(onSeekBarChangeListener);
        editText.setOnFocusChangeListener(editTextFocusChangeListener);
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);

        seekBar.setEnabled(enabled);
        editText.setEnabled(enabled);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.item_seek_value_input;
    }

    /**
     * Get the value displayed in the edit text.
     *
     * @return
     */
    public int getValue() {
        fixEditedValue();
        return Integer.valueOf(editText.getText().toString());
    }

    /**
     * Set the value displayed in the edit text and move the seek bar accordingly.
     *
     * @param value
     */
    public void setValue(int value) {
        seekBar.setProgress(valueToPercent(value));
        editText.setText(String.valueOf(value));
    }

    /**
     * Ensure that the value in the edit text field is in the minimum/maximum bounds.
     */
    private void fixEditedValue() {
        int value = Integer.parseInt(editText.getText().toString());
        if (value < minimumValue) {
            value = minimumValue;
        }

        if (value > maximumValue) {
            value = maximumValue;
        }

        setValue(value);
    }

    private int valueToPercent(int value) {
        int gap = maximumValue - minimumValue;
        return (value - minimumValue) * 100 / gap;
    }

    private void initAttributes(Context context, AttributeSet attrs) {
        if (attrs == null) {
            return;
        }

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.SeekValueInput);
        final int count = a.getIndexCount();
        for (int i = 0; i < count; i++) {
            int attr = a.getIndex(i);
            if (attr == R.styleable.SeekValueInput_minimum) {
                minimumValue = a.getInt(attr, 0);
            } else if (attr == R.styleable.SeekValueInput_maximum) {
                maximumValue = a.getInt(attr, 100);
            } else if (attr == R.styleable.SeekValueInput_seekValue) {
                initialValue = a.getInt(attr, 0);
            }
        }
        a.recycle();
    }
}
