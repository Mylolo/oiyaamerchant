package com.oiyaa.merchant.ingenico.widget;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.SpinnerAdapter;

import java.util.List;

/**
 * Utililty class to create spinners.
 */
public final class SpinnerUtils {

    private SpinnerUtils() {

    }

    public static SpinnerAdapter stringListAdapter(Context context, List<String> strings) {
        ArrayAdapter<String> spinnerArrayAdapter
                = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, strings);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        return spinnerArrayAdapter;
    }
}
