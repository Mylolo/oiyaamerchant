package com.oiyaa.merchant.ingenico

import org.mockito.Mockito

inline fun <reified T> mock(): T = Mockito.mock(T::class.java)