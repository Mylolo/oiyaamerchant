package com.oiyaa.merchant.ingenico.payment.viewmodel

import org.junit.Assert.*
import org.junit.Rule
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.oiyaa.merchant.ingenico.getOrAwaitValueTest
import org.junit.Before

import org.junit.Test
import org.mockito.Mockito.*

class PaymentViewModelTest
{
    @get:Rule
    var instantTaskExecutor = InstantTaskExecutorRule()

    private lateinit var viewModel:PaymentViewModel;

    @Before
    fun setUp()
    {
        viewModel = PaymentViewModel()
        viewModel.setPaymentState(true)
    }

    @Test
    fun getPaymentSuccessTest()
    {
        val expectedValue = true
        val actualValue = viewModel.paymentState.getOrAwaitValueTest()
        assertEquals(expectedValue,actualValue)
    }

    @Test
    fun getPaymentFailureTest()
    {
        val expectedValue = false
        viewModel.setPaymentState(expectedValue)
        val actualValue = viewModel.paymentState.getOrAwaitValueTest()
        assertEquals(actualValue,expectedValue)
    }


}