package com.utils;

import android.content.Context;

import com.utils.helper.boradcast.BroadcastFactory;
import com.utils.helper.gson.GsonJsonizer;
import com.utils.helper.imagecache.ImageCacheFactory;
import com.utils.helper.permission.PermissionFactory;
import com.utils.helper.preference.PreferenceFactory;
import com.utils.helper.toast.ToastFactory;
import com.utils.validator.Validator;

public class UtilFactory {

    private static UtilFactory utilFactory;

    private UtilFactory(Context context, String appName) {
        BroadcastFactory.init(context);
        // GsonJsonizer - Don't need any extra arguments
        ImageCacheFactory.init(context);
        // PermissionFactory - Don't need any extra arguments
        // PhoneNumberFactory - Don't need any extra arguments
        PreferenceFactory.init(context, appName);
        ToastFactory.init(context, true);
    }

    public static void init(Context context, String appName) {
        utilFactory = new UtilFactory(context, appName);
    }

    public static UtilFactory getInstance() throws IllegalAccessException {
        if (!Validator.isValid(utilFactory))
            throw new IllegalAccessException("Call init()");

        return utilFactory;
    }

    public BroadcastFactory getBroadcastFactory() throws IllegalAccessException {
        return BroadcastFactory.getInstance();
    }

    public GsonJsonizer getGsonJsonizer() {
        return GsonJsonizer.getInstance();
    }

    public ImageCacheFactory getImageCacheFactory() throws IllegalAccessException {
        return ImageCacheFactory.getInstance();
    }

    public PermissionFactory getPermissionFactory() {
        return PermissionFactory.getInstance();
    }

    public PreferenceFactory getPreferenceFactory() throws IllegalAccessException {
        return PreferenceFactory.getInstance();
    }

    public ToastFactory getToastFactory() throws IllegalAccessException {
        return ToastFactory.getInstance();
    }
}
