package com.utils.base;

import android.app.ProgressDialog;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresPermission;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.view.WindowManager;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.utils.R;
import com.utils.UtilFactory;
import com.utils.data.Address;
import com.utils.data.Location;
import com.utils.helper.boradcast.BroadcastFactory;
import com.utils.helper.gson.GsonJsonizer;
import com.utils.helper.imagecache.ImageCacheFactory;
import com.utils.helper.permission.PermissionActivity;
import com.utils.helper.permission.PermissionFactory;
import com.utils.helper.preference.PreferenceFactory;
import com.utils.helper.toast.ToastFactory;
import com.utils.util.Util;
import com.utils.validator.Validator;

import java.io.IOException;

public abstract class AppBaseActivity extends PermissionActivity implements AppBaseConstants {

    private ProgressDialog progressDialog;
    private FusedLocationProviderClient fusedLocationClient;

    public abstract void init();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        progressDialog = new ProgressDialog(AppBaseActivity.this, R.style.ProgressBar);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(AppBaseActivity.this);

        processBundleData();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();

        dismissProgressBar();
        getToastFactory().clearAllToast();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void showProgressBar() {
        if (!progressDialog.isShowing()) {
            progressDialog.show();
            progressDialog.setContentView(R.layout.view_progressbar);
        }
    }

    public void dismissProgressBar() {
        progressDialog.dismiss();
    }

    public AppBaseFragment getFragment(Class<AppBaseFragment> fragment) {
        AppBaseFragment retVal = null;

        try {
            String packageName = fragment.getPackage().getName();
            String className = packageName + "." + fragment.getSimpleName();

            retVal = (AppBaseFragment) Class.forName(className).newInstance();
        } catch (Exception e) {
            // Never happen
        }

        return retVal;
    }

    public AppBaseFragment getCurrentFragment(int containerViewId)
    {
        try
        {
            AppBaseFragment fragment = (AppBaseFragment)
                    getSupportFragmentManager().findFragmentById(containerViewId);
            return fragment;

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return null;
    }

    public void updateFragmentView(int containerViewId, Fragment fragment, boolean addToBackStack,boolean replace) {
        try {
            String tag = fragment.getClass().getSimpleName();
            if (!getSupportFragmentManager().popBackStackImmediate(tag, 0)) {
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                if(replace)fragmentTransaction.replace(containerViewId, fragment,tag);
                else fragmentTransaction.add(containerViewId, fragment,tag);
                fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                if(addToBackStack) fragmentTransaction.addToBackStack(tag);
                else fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commitAllowingStateLoss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @RequiresPermission(anyOf = {"android.permission.ACCESS_COARSE_LOCATION", "android.permission.ACCESS_FINE_LOCATION"})
    public void getLastKnownLocation(@NonNull final Task<Address> task) {
        fusedLocationClient.getLastLocation().addOnSuccessListener(this, data -> {
            try {
                Location location = new Location(data.getLatitude(), data.getLongitude());
                location.setAccuracy(data.getAccuracy());
                location.setBearings(data.getBearing());
                location.setSpeed(data.getSpeed());
                location.setProvider(data.getProvider());

                Address address = Util.getAddress(AppBaseActivity.this, new LatLng(data.getLatitude(), data.getLongitude()));
                address.setLocation(location);

                task.onSuccess(address);
            } catch (IOException e) {
                task.onError(NETWORK_ERROR);
            } catch (IllegalArgumentException e) {
                task.onError(e.getLocalizedMessage());
            }
        });
    }

    public UtilFactory getUtilFactory() {
        UtilFactory retVal = null;

        try {
            retVal = UtilFactory.getInstance();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return retVal;
    }

    public BroadcastFactory getBroadcastFactory() {
        return BroadcastFactory.getInstance();
    }

    public GsonJsonizer getGsonJsonizer() {
        return GsonJsonizer.getInstance();
    }

    public ImageCacheFactory getImageCacheFactory() {
        return ImageCacheFactory.getInstance();
    }

    public PermissionFactory getPermissionFactory() {
        return PermissionFactory.getInstance();
    }

    public PreferenceFactory getPreferenceFactory() {
        return PreferenceFactory.getInstance();
    }

    public ToastFactory getToastFactory() {
        ToastFactory toastFactory = ToastFactory.getInstance();
        toastFactory.setCustomView(true);
        return toastFactory;
    }

    private void processBundleData() {
        Bundle bundle = getIntent().getExtras();
        if (Validator.isValid(bundle)) {
            boolean overrideTransition = bundle.getBoolean(EXTRA_OVERRIDE_TRANSITION, true);
            if (overrideTransition)
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        }
    }
}