package com.utils.base;

public interface AppBaseConstants {

    // Image Cache
    String IMAGE_DRAWABLE = "drawable://";
    String IMAGE_CONTENT = "content://";
    String IMAGE_ASSETS = "assets://";
    String IMAGE_FILE = "file://";
    String IMAGE_STREAM = "stream://";

    // Permission
    String SOME_PERMISSION_DENIED = "Some permissions are denied, Go to app settings and enable it";
    int REQUEST_ID_PERMISSION_MULTIPLE = 100;

    // Extras
    String EXTRA_OVERRIDE_TRANSITION = "extraOverrideTransition";

    // Message
    String NETWORK_ERROR = "Oops... Looks like we have a network info! Please check your internet connection";
}
