package com.utils.base;

public interface Stringifier {

    String stringify();
}
