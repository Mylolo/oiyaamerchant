package com.utils.base;

public interface Task<T> {

    void onSuccess(T result);

    void onError(String message);
}
