package com.utils.helper.boradcast;

import android.Manifest;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresPermission;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.utils.helper.boradcast.sms.SMSHandler;
import com.utils.helper.boradcast.sms.SMSReceiver;
import com.utils.helper.boradcast.sms.SMSRetriever;
import com.utils.log.Logger;
import com.utils.validator.Validator;

public class BroadcastFactory {

    private static final String TAG = BroadcastFactory.class.getSimpleName();

    private static BroadcastFactory broadcastFactory;
    private static Context context;
    private static SMSReceiver smsReceiver;
    private static SMSRetriever smsRetriever;

    private BroadcastFactory(Context context) {
        this.context = context;
    }

    public static void init(@NonNull Context context) {
        broadcastFactory = new BroadcastFactory(context);
    }

    public static BroadcastFactory getInstance() {
        if (!Validator.isValid(broadcastFactory))
            throw new RuntimeException("Call init()");

        return broadcastFactory;
    }

    @RequiresPermission(Manifest.permission.READ_SMS)
    public void registerForSMS(String messageTemplate, SMSHandler smsHandler) throws Exception {
        if (!Validator.isValid(messageTemplate))
            throw new Exception("Message template can't be null");

        if (!Validator.isValid(smsHandler))
            throw new Exception("SMS handler can't be null");

        smsReceiver = new SMSReceiver(messageTemplate, smsHandler);
        smsReceiver.register(context, smsReceiver);
    }

    public void unregisterForSMS() {
        if (Validator.isValid(smsReceiver)) {
            try {
                smsReceiver.unregister(context, smsReceiver);
            } catch (Exception e) {
                Logger.logError(TAG, e);
            } finally {
                smsReceiver = null;
            }
        }
    }

    public void registerForSMSRetriever(String messageTemplate, SMSHandler smsHandler) throws Exception {
        if (!Validator.isValid(messageTemplate))
            throw new Exception("Message template can't be null");

        if (!Validator.isValid(smsHandler))
            throw new Exception("SMS handler can't be null");

        SmsRetrieverClient smsRetrieverClient = SmsRetriever.getClient(context);
        smsRetrieverClient.startSmsRetriever();

        smsRetriever = new SMSRetriever(messageTemplate, smsHandler);
        smsRetriever.register(context, smsRetriever);
    }

    public void unregisterForSMSRetriever() {
        if (Validator.isValid(smsRetriever)) {
            try {
                smsRetriever.unregister(context, smsRetriever);
            } catch (Exception e) {
                Logger.logError(TAG, e);
            } finally {
                smsRetriever = null;
            }
        }
    }
}
