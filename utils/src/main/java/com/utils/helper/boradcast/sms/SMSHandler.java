package com.utils.helper.boradcast.sms;

public interface SMSHandler {

    void receivedSMS(String from, String messageBody);
}
