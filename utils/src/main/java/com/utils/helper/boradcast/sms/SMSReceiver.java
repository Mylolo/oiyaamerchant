package com.utils.helper.boradcast.sms;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.telephony.SmsMessage;

import com.utils.log.Logger;
import com.utils.validator.Validator;

public class SMSReceiver extends BroadcastReceiver {

    private static final String TAG = SMSReceiver.class.getSimpleName();

    private String messageContains;
    private SMSHandler smsHandler;

    public SMSReceiver(String messageContains, SMSHandler smsHandler) {
        this.messageContains = messageContains;
        this.smsHandler = smsHandler;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();
        if (Validator.isValid(bundle)) {
            Object[] pdus = (Object[]) bundle.get("pdus");

            SmsMessage[] messages = new SmsMessage[pdus.length];
            for (int i = 0; i < pdus.length; i++)
                messages[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);

            for (final SmsMessage message : messages) {
                String from = message.getOriginatingAddress();
                long when = message.getTimestampMillis();
                String msg = message.getMessageBody();

                if (msg.contains(messageContains) && Validator.isValid(smsHandler))
                    smsHandler.receivedSMS(from, msg);
            }
        }
    }

    public void register(@NonNull Context context, @NonNull SMSReceiver smsReceiver) {
        IntentFilter intentFilter = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
        intentFilter.setPriority(999);
        context.registerReceiver(smsReceiver, intentFilter);

        Logger.logInfo(TAG, "SMS_RECEIVER registered");
    }

    public void unregister(@NonNull Context context, @NonNull SMSReceiver smsReceiver) throws Exception {
        try {
            context.unregisterReceiver(smsReceiver);
            Logger.logInfo(TAG, "SMS_RECEIVER unregistered");
        } catch (IllegalArgumentException e) {
            throw new Exception(e);
        }
    }
}