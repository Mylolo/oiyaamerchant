package com.utils.helper.boradcast.sms;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.annotation.NonNull;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.Status;
import com.utils.log.Logger;
import com.utils.validator.Validator;

public class SMSRetriever extends BroadcastReceiver {

    private static final String TAG = SMSRetriever.class.getSimpleName();

    private SMSHandler smsHandler;
    private String messageContains;

    public SMSRetriever(String messageContains, SMSHandler smsHandler) {
        this.messageContains = messageContains;
        this.smsHandler = smsHandler;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (SmsRetriever.SMS_RETRIEVED_ACTION.equals(intent.getAction())) {
            Bundle bundle = intent.getExtras();
            if (Validator.isValid(bundle)) {
                Status status = (Status) bundle.get(SmsRetriever.EXTRA_STATUS);
                switch (status.getStatusCode()) {
                    case CommonStatusCodes.SUCCESS: {
                        String message = (String) bundle.get(SmsRetriever.EXTRA_SMS_MESSAGE);

                        if (message.contains(messageContains) && Validator.isValid(smsHandler))
                            smsHandler.receivedSMS("", message);
                    }
                    break;

                    case CommonStatusCodes.TIMEOUT: {

                    }
                    break;
                }
            }
        }

        context.unregisterReceiver(this);
    }


    public void register(@NonNull Context context, @NonNull SMSRetriever smsReceiver) {
        IntentFilter intentFilter = new IntentFilter(SmsRetriever.SMS_RETRIEVED_ACTION);
        intentFilter.setPriority(999);
        context.registerReceiver(smsReceiver, intentFilter);

        Logger.logInfo(TAG, "SMS_RETRIVER registered");
    }

    public void unregister(@NonNull Context context, @NonNull SMSRetriever smsReceiver) throws Exception {
        try {
            context.unregisterReceiver(smsReceiver);
            Logger.logInfo(TAG, "SMS_RETRIVER unregistered");
        } catch (IllegalArgumentException e) {
            throw new Exception(e);
        }
    }
}