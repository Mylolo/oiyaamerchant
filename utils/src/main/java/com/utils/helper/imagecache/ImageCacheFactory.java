package com.utils.helper.imagecache;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import androidx.annotation.ColorRes;
import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat;
import android.widget.ImageView;

import com.nostra13.universalimageloader.cache.disc.DiskCache;
import com.nostra13.universalimageloader.cache.memory.MemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.LoadedFrom;
import com.nostra13.universalimageloader.core.imageaware.ImageViewAware;
import com.utils.R;
import com.utils.base.AppBaseConstants;
import com.utils.data.DimenInfo;
import com.utils.util.Util;
import com.utils.validator.Validator;

import java.io.File;
import java.io.InputStream;

public class ImageCacheFactory implements AppBaseConstants {

    private static ImageCacheFactory imageCacheFactory;
    private Context context;
    private ImageCache imageCache;

    private ImageCacheFactory(@NonNull Context context) {
        this.context = context;
        imageCache = new ImageCache(context);
    }

    public static void init(@NonNull Context context) {
        imageCacheFactory = new ImageCacheFactory(context);
    }

    public static ImageCacheFactory getInstance() {
        if (!Validator.isValid(imageCacheFactory))
            throw new RuntimeException("Call init()");

        return imageCacheFactory;
    }

    public ImageLoader getImageLoader() {
        return imageCache.getImageLoader();
    }

    public void image(@NonNull ImageView imageView, String path, @DrawableRes int defaultImage) {
        image(imageView, path, defaultImage, null);
    }

    public void image(@NonNull ImageView imageView, String path, @DrawableRes int defaultImage, @NonNull DimenInfo dimenInfo) {
        renderImage(imageView, path, defaultImage, dimenInfo);
    }

    public void image(@NonNull ImageView imageView, InputStream inputStream, @NonNull String key, @DrawableRes int defaultImage, @NonNull DimenInfo dimenInfo) {
        renderImage(imageView, inputStream, key, defaultImage, dimenInfo);
    }

    public void roundedCorner(@NonNull ImageView imageView, String path, @DrawableRes int defaultImage) {
        roundedCorner(imageView, path, defaultImage, null);
    }

    public void roundedCorner(@NonNull ImageView imageView, String path, @DrawableRes int defaultImage, @NonNull DimenInfo dimenInfo) {
        renderRoundedCorner(imageView, path, defaultImage, dimenInfo);
    }

    public void circular(@NonNull ImageView imageView, String path, @DrawableRes int defaultImage) {
        circular(imageView, path, defaultImage, null);
    }

    public void circular(@NonNull ImageView imageView, String path, @DrawableRes int defaultImage, DimenInfo dimenInfo) {
        renderCircular(imageView, path, defaultImage, dimenInfo);
    }

    public void circular(@NonNull ImageView imageView, String path, @DrawableRes int defaultImage, @ColorRes int strokeColor, @ColorRes int backgroundColor) {
        circular(imageView, path, defaultImage, strokeColor, backgroundColor, null);
    }

    public void circular(@NonNull ImageView imageView, String path, @DrawableRes int defaultImage, @ColorRes int strokeColor, @ColorRes int backgroundColor, DimenInfo dimenInfo) {
        renderCircular(imageView, path, defaultImage, strokeColor, backgroundColor, dimenInfo);
    }

    public void refresh() {
        imageCache.refreshImageLoader(context);
    }

    public void clearFromCache(@NonNull String imagePath) {
        clearFromDiscCache(imagePath);
        clearFromMemoryCache(imagePath);
    }

    public void clearFromDiscCache() {
        imageCache.getImageLoader().clearDiskCache();
    }

    public void clearFromDiscCache(@NonNull String imagePath) {
        DiskCache diskCache = imageCache.getImageLoader().getDiskCache();
        File file = diskCache.get(imagePath);
        if (file.exists())
            file.delete();
    }

    public void clearFromMemoryCache() {
        imageCache.getImageLoader().clearMemoryCache();
    }

    public void clearFromMemoryCache(@NonNull String imagePath) {
        MemoryCache memoryCache = imageCache.getImageLoader().getMemoryCache();
        memoryCache.remove(imagePath);
    }

    private void renderImage(ImageView imageView, String path, int defaultImage, DimenInfo dimenInfo) {
        DisplayImageOptions displayImageOptions;
        if (Validator.isValid(dimenInfo))
            displayImageOptions = imageCache.defaultDIO(dimenInfo);
        else
            displayImageOptions = imageCache.defaultDIO();

        if (Validator.isValid(path)) {
            if (path.startsWith("http") || path.startsWith("https")) {
                renderNormal(imageView, path, defaultImage, displayImageOptions);
            } else if (path.startsWith(IMAGE_DRAWABLE) || path.startsWith(IMAGE_CONTENT) || path.startsWith(IMAGE_ASSETS) || path.startsWith(IMAGE_FILE)) {
                if (!Validator.isValid(imageView.getTag()) || !imageView.getTag().equals(path)) {
                    imageCache.getImageLoader().displayImage(path, new ImageViewAware(imageView, false), displayImageOptions);
                    imageView.setTag(path);
                }
            } else {
                renderDefault(imageView, defaultImage, displayImageOptions);
            }
        } else {
            renderDefault(imageView, defaultImage, displayImageOptions);
        }
    }

    private void renderImage(ImageView imageView, InputStream inputStream, String key, int defaultImage, DimenInfo dimenInfo) {
        DisplayImageOptions displayImageOptions;
        if (Validator.isValid(dimenInfo))
            displayImageOptions = imageCache.defaultDIO(dimenInfo);
        else
            displayImageOptions = imageCache.defaultDIO();

        renderInputStream(imageView, inputStream, key, defaultImage, displayImageOptions);
    }

    private void renderRoundedCorner(ImageView imageView, String path, int defaultImage, DimenInfo dimenInfo) {
        DisplayImageOptions displayImageOptions;
        if (Validator.isValid(dimenInfo))
            displayImageOptions = imageCache.roundedCornerDIO(dimenInfo);
        else
            displayImageOptions = imageCache.roundedCornerDIO();

        renderNormal(imageView, path, defaultImage, displayImageOptions);
    }

    private void renderCircular(ImageView imageView, String path, int defaultImage, DimenInfo dimenInfo) {
        DisplayImageOptions displayImageOptions;
        if (Validator.isValid(dimenInfo))
            displayImageOptions = imageCache.avatarDIO(dimenInfo);
        else
            displayImageOptions = imageCache.avatarDIO();

        renderNormal(imageView, path, defaultImage, displayImageOptions);
    }

    private void renderCircular(ImageView imageView, String path, int defaultImage, int strokeColor, int backgroundColor, DimenInfo dimenInfo) {
        if (strokeColor == -1)
            strokeColor = Util.getColor(context, R.color.white);

        if (backgroundColor == -1)
            backgroundColor = Util.getColor(context, R.color.white);

        DisplayImageOptions displayImageOptions;
        if (Validator.isValid(dimenInfo))
            displayImageOptions = imageCache.avatarDIO(dimenInfo, strokeColor, backgroundColor);
        else
            displayImageOptions = imageCache.avatarDIO(strokeColor, backgroundColor);

        renderNormal(imageView, path, defaultImage, displayImageOptions);
    }

    private void renderNormal(ImageView imageView, String path, int defaultImage, DisplayImageOptions displayImageOptions) {
        if (Validator.isValid(path) && !path.equals("")) {
            if (!Validator.isValid(imageView.getTag()) || !imageView.getTag().equals(path)) {
                imageCache.getImageLoader().displayImage(path, new ImageViewAware(imageView, false), displayImageOptions);
                imageView.setTag(path);
            }
        } else {
            renderDefault(imageView, defaultImage, displayImageOptions);
        }
    }

    private void renderInputStream(ImageView imageView, InputStream inputStream, String key, int defaultImage, DisplayImageOptions displayImageOptions) {
        String tag = IMAGE_STREAM + key;

        if (Validator.isValid(inputStream)) {
            if (!Validator.isValid(imageView.getTag()) || !imageView.getTag().equals(tag)) {
                imageCache.getImageLoader().displayImage(tag, new ImageViewAware(imageView, false), displayImageOptions);
                imageView.setTag(tag);
            }
        } else {
            renderDefault(imageView, defaultImage, displayImageOptions);
        }
    }

    private void renderDefault(ImageView imageView, int defaultImage, DisplayImageOptions displayImageOptions) {
        if (!Validator.isValid(imageView.getTag()) || !imageView.getTag().equals(defaultImage)) {
            Drawable drawable = Util.getDrawable(context, defaultImage);
            if (drawable instanceof BitmapDrawable) {
                imageCache.getImageLoader().displayImage(IMAGE_DRAWABLE + defaultImage, new ImageViewAware(imageView, false), displayImageOptions);
            } else {
                VectorDrawableCompat vectorDrawableCompat = VectorDrawableCompat.create(context.getResources(), defaultImage, null);
                Bitmap bitmap = Util.getBitmap(vectorDrawableCompat);

                displayImageOptions.getDisplayer().display(bitmap, new ImageViewAware(imageView, false), LoadedFrom.DISC_CACHE);
            }

            imageView.setTag(defaultImage);
        }
    }
}
