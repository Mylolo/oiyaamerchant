package com.utils.helper.imagecache;

import androidx.recyclerview.widget.RecyclerView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.utils.validator.Validator;

public class PauseOnScrollListener extends RecyclerView.OnScrollListener {

    private final boolean pauseOnScroll;
    private final boolean pauseOnSettling;
    private final RecyclerView.OnScrollListener externalListener;
    private ImageLoader imageLoader;

    public PauseOnScrollListener(ImageLoader imageLoader, boolean pauseOnScroll, boolean pauseOnSettling) {
        this(imageLoader, pauseOnScroll, pauseOnSettling, null);
    }

    public PauseOnScrollListener(ImageLoader imageLoader, boolean pauseOnScroll, boolean pauseOnSettling, RecyclerView.OnScrollListener customListener) {
        this.imageLoader = imageLoader;
        this.pauseOnScroll = pauseOnScroll;
        this.pauseOnSettling = pauseOnSettling;
        this.externalListener = customListener;
    }

    @Override
    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
        switch (newState) {
            case RecyclerView.SCROLL_STATE_IDLE:
                imageLoader.resume();
                break;

            case RecyclerView.SCROLL_STATE_DRAGGING:
                if (pauseOnScroll)
                    imageLoader.pause();
                break;

            case RecyclerView.SCROLL_STATE_SETTLING:
                if (pauseOnSettling)
                    imageLoader.pause();
                break;
        }

        if (Validator.isValid(externalListener))
            externalListener.onScrollStateChanged(recyclerView, newState);
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        if (Validator.isValid(externalListener))
            externalListener.onScrolled(recyclerView, dx, dy);
    }
}
