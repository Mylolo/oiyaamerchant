package com.utils.helper.permission;

import android.content.pm.PackageManager;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;

import com.utils.base.AppBaseConstants;
import com.utils.data.DangerousPermission;
import com.utils.validator.Validator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class PermissionActivity extends AppCompatActivity implements PermissionRationaleHandler, AppBaseConstants {

    private Map<DangerousPermission, Integer> requestedPermissions;
    private PermissionHandler permissionHandler;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_ID_PERMISSION_MULTIPLE:
                PermissionUtil.processResult(this, permissions, grantResults, requestedPermissions, permissionHandler, this);
        }
    }

    public void setRequestedPermission(List<DangerousPermission> dangerousPermissions) {
        if (!Validator.isValid(requestedPermissions))
            requestedPermissions = new HashMap<>();

        for (DangerousPermission dangerousPermission : dangerousPermissions) {
            requestedPermissions.put(dangerousPermission, PackageManager.PERMISSION_GRANTED);
        }
    }

    public void setPermissionHandler(PermissionHandler permissionHandler) {
        this.permissionHandler = permissionHandler;
    }

    @Override
    public boolean showRequestPermissionRationale(String key) {
        return ActivityCompat.shouldShowRequestPermissionRationale(this, key);
    }
}
