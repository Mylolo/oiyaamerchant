package com.utils.helper.permission;

import com.utils.data.DangerousPermission;

import java.util.List;

public interface PermissionHandler {

    void result(List<DangerousPermission> granted, List<DangerousPermission> denied);

    boolean permissionRationale();

    void permissionRationaleFor(List<DangerousPermission> dangerousPermissions);

    void info(String message);
}
