package com.utils.helper.permission;

interface PermissionRationaleHandler {

    boolean showRequestPermissionRationale(String key);
}
