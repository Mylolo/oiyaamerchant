package com.utils.helper.permission;

import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import androidx.appcompat.app.AlertDialog;

import com.utils.base.AppBaseConstants;
import com.utils.data.DangerousPermission;
import com.utils.util.StringUtil;
import com.utils.validator.Validator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

class PermissionUtil implements AppBaseConstants {

    public static void processResult(Context context, String[] permissions, int[] grantResults, Map<DangerousPermission, Integer> requestedPermissions, PermissionHandler permissionHandler, PermissionRationaleHandler permissionRationaleHandler) {
        if (grantResults.length > 0) {
            List<DangerousPermission> granted = new ArrayList<>();
            List<DangerousPermission> denied = new ArrayList<>();

            boolean isAllPermissionsGranted = true;
            List<DangerousPermission> permissionRationale = new ArrayList<>();

            for (int i = 0; i < permissions.length; i++)
                requestedPermissions.put(DangerousPermission.toEnum(permissions[i]), grantResults[i]);

            for (DangerousPermission key : requestedPermissions.keySet()) {
                if (requestedPermissions.get(key) == PackageManager.PERMISSION_GRANTED) {
                    granted.add(key);
                } else {
                    isAllPermissionsGranted = false;
                    denied.add(key);
                }

                boolean shouldShowRequestPermissionRationale = permissionRationaleHandler.showRequestPermissionRationale(key.toString());
                if (shouldShowRequestPermissionRationale)
                    permissionRationale.add(key);
            }

            if (Validator.isValid(permissionHandler)) {
                if (isAllPermissionsGranted) {
                    permissionHandler.result(granted, denied);
                } else {
                    if (!permissionRationale.isEmpty() && permissionHandler.permissionRationale()) {
                        String permissionRationaleMessage = PermissionUtil.getPermissionRationaleMessage(permissionRationale);

                        PermissionUtil.showDialog(context, permissionRationaleMessage, (dialog, which) -> {
                            switch (which) {
                                case DialogInterface.BUTTON_POSITIVE:
                                    permissionHandler.permissionRationaleFor(permissionRationale);
                                    break;

                                case DialogInterface.BUTTON_NEGATIVE:
//                                    permissionHandler.info(SOME_PERMISSION_DENIED);
                                    break;
                            }
                        });
                    } else {
                        permissionHandler.info(SOME_PERMISSION_DENIED);
                    }
                }
            }
        }
    }

    private static String getPermissionRationaleMessage(List<DangerousPermission> permissionRationale) {
        String retVal = "";

        List<String> data = new ArrayList<>();
        for (DangerousPermission dangerousPermission : permissionRationale) {
            data.add(dangerousPermission.stringify());
        }

        if (Validator.isValid(data)) {
            retVal = StringUtil.toCSV(data);
            retVal += data.size() > 1 ? " permission is " : " permissions are ";
            retVal += "required. Did you like to grant permission now?";
        }

        return retVal;
    }

    private static void showDialog(Context context, String message, DialogInterface.OnClickListener onClickListener) {
        new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton("OK", onClickListener)
                .setNegativeButton("Cancel", onClickListener)
                .create()
                .show();
    }

    private static boolean hasPermission(Context context, String permission) throws PackageManager.NameNotFoundException {
        PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_PERMISSIONS);
        if (Validator.isValid(packageInfo.requestedPermissions)) {
            for (String tempPermission : packageInfo.requestedPermissions) {
                if (tempPermission.equals(permission))
                    return true;
            }
        }

        return false;
    }
}
