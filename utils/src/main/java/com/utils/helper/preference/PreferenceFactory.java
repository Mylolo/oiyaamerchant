package com.utils.helper.preference;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.utils.validator.Validator;

import java.lang.reflect.Type;
import java.util.Collection;

public class PreferenceFactory {

    private static PreferenceFactory preferenceFactory;
    private static String preferenceName;
    private Context context;
    private Gson gson;

    private PreferenceFactory(Context context, String preferenceName) {
        this.context = context;
        this.preferenceName = preferenceName;

        this.gson = new Gson();
    }

    public static void init(@NonNull Context context, @NonNull String preferenceName) {
        preferenceFactory = new PreferenceFactory(context, preferenceName);
    }

    public static PreferenceFactory getInstance() {
        if (!Validator.isValid(preferenceFactory))
            throw new RuntimeException("Call init()");

        return preferenceFactory;
    }

    private static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE);
    }

    public void remove(@NonNull String... key) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        for (int i = 0; i < key.length; i++) {
            editor.remove(key[i].toString());
            editor.commit();
        }
    }

    public void setString(@NonNull String key, String data) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(key, data);
        editor.commit();
    }

    public String getString(@NonNull String key) {
        return getSharedPreferences(context).getString(key, null);
    }

    public void setBoolean(@NonNull String key, boolean data) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(key, data);
        editor.commit();
    }

    public boolean getBoolean(@NonNull String key, boolean defaultValue) {
        return getSharedPreferences(context).getBoolean(key, defaultValue);
    }

    public void setCollection(@NonNull String key, Collection data) {
        String toJson = gson.toJson(data);

        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(key, toJson);
        editor.commit();
    }

    public Collection getCollection(@NonNull String key, @NonNull Type type) {
//        Type type = new TypeToken<List<String>>() { }.getType();
        return gson.fromJson(getSharedPreferences(context).getString(key, ""), type);
    }

    public Collection getCollection(@NonNull String key, @NonNull TypeToken typeToken) {
        return getCollection(key, typeToken.getType());
    }

    public void setObject(@NonNull String key, Object object) {
        String toJson = gson.toJson(object);

        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(key, toJson);
        editor.commit();
    }

    public <T> T getObject(@NonNull String key, @NonNull Class<T> aClass) {
        return gson.fromJson(getSharedPreferences(context).getString(key, ""), aClass);
    }
}
