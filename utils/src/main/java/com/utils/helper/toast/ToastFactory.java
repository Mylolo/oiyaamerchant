package com.utils.helper.toast;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import androidx.annotation.NonNull;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.utils.R;
import com.utils.validator.Validator;

import java.util.ArrayList;

public class ToastFactory {

    private static ToastFactory toastFactory;
    private Context context;
    private ArrayList<Toast> toasts = new ArrayList<>();
    private boolean customView;
    private View view;

    public enum ToastPosition {
        TOP,
        BOTTOM,
        CENTER
    }

    private ToastFactory(Context context, boolean customView) {
        this.context = context;
        this.customView = customView;
    }

    public static void init(@NonNull Context context, boolean showCustomView) {
        toastFactory = new ToastFactory(context, showCustomView);
    }

    public static ToastFactory getInstance() {
        if (!Validator.isValid(toastFactory))
            throw new RuntimeException("Call init()");

        return toastFactory;
    }

    public void setCustomView(boolean customView) {
        this.customView = customView;
    }

    public void setView(View view) {
        this.customView = true;
        this.view = view;
    }

    public void toast(@NonNull String message) {
        if (customView)
            show(Validator.isValid(view) ? view : getCustomView(message), ToastPosition.BOTTOM, Toast.LENGTH_SHORT);
        else
            show(message, ToastPosition.BOTTOM, Toast.LENGTH_SHORT, null);
    }

    public void toast(@NonNull String message, @NonNull int duration) {
        if (customView)
            show(Validator.isValid(view) ? view : getCustomView(message), ToastPosition.BOTTOM, duration);
        else
            show(message, ToastPosition.BOTTOM, duration, null);
    }

    public void toast(@NonNull String message, @NonNull ToastPosition toastPosition) {
        if (customView)
            show(Validator.isValid(view) ? view : getCustomView(message), toastPosition, Toast.LENGTH_SHORT);
        else
            show(message, toastPosition, Toast.LENGTH_SHORT, null);
    }

    public void toast(@NonNull String message, @NonNull ToastPosition toastPosition, @NonNull int duration) {
        if (customView)
            show(Validator.isValid(view) ? view : getCustomView(message), toastPosition, duration);
        else
            show(message, toastPosition, duration, null);
    }

    public void clearAllToast() {
        for (Toast toast : toasts) {
            toast.cancel();
        }

        toasts.clear();
    }

    private void show(View view, @NonNull ToastPosition toastPosition, @NonNull int duration) {
        show("", toastPosition, duration, view);
    }

    private void show(final String message, final ToastPosition toastPosition, final int duration, final View view) {
        new Handler(Looper.getMainLooper()).post(() -> {
            int gravity = Gravity.BOTTOM;
            switch (toastPosition) {
                case TOP:
                    gravity = Gravity.TOP;
                    break;

                case BOTTOM:
                    gravity = Gravity.BOTTOM;
                    break;

                case CENTER:
                    gravity = Gravity.CENTER;
                    break;
            }

            Toast toast = Toast.makeText(context, message, duration);
            toast.setGravity(gravity, 0, 100);

            if (Validator.isValid(view))
                toast.setView(view);

            toast.show();

            toasts.add(toast);
        });
    }

    private View getCustomView(@NonNull final String message) {
        View view = LayoutInflater.from(context).inflate(R.layout.view_toast, null, false);

        TextView textView = view.findViewById(R.id.toast_textView);
        textView.setText(message);

        return view;
    }
}
