package com.utils.log;

import android.util.Log;

public class Logger {

    public static void logError(String tag, Exception exception) {
        writeLog(Log.ERROR, tag, exception.getLocalizedMessage());
    }

    public static void logInfo(String tag, String message) {
        writeLog(Log.INFO, tag, message);
    }

    public static void logWarn(String tag, String message) {
        writeLog(Log.WARN, tag, message);
    }

    public static void logDebug(String tag, Exception exception) {
        writeLog(Log.DEBUG, tag, exception.getLocalizedMessage());
    }

    public static void logVerbose(String tag, Exception exception) {
        writeLog(Log.VERBOSE, tag, exception.getLocalizedMessage());
    }

    private static void writeLog(int priority, String tag, String message) {
        switch (priority) {
            case Log.ERROR:
                Log.e(tag, message);
                break;

            case Log.INFO:
                Log.i(tag, message);
                break;

            case Log.WARN:
                Log.w(tag, message);
                break;

            case Log.DEBUG:
                Log.d(tag, message);
                break;

            case Log.VERBOSE:
                Log.v(tag, message);
                break;
        }
    }
}
