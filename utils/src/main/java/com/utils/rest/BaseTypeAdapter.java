package com.utils.rest;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

public class BaseTypeAdapter implements JsonSerializer<Object>, JsonDeserializer<Object> {

    private static final String CLASS_META_KEY = "metaKey";

    @Override
    public Object deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        try {
            JsonObject jsonObject = jsonElement.getAsJsonObject();

            String packageName = ((Class) type).getPackage().getName();
            String className = jsonObject.get(CLASS_META_KEY).getAsString();

            Class<?> clazz = Class.forName(packageName + "." + className);
            return jsonDeserializationContext.deserialize(jsonElement, clazz);
        } catch (ClassNotFoundException e) {
            throw new JsonParseException(e);
        }
    }

    @Override
    public JsonElement serialize(Object object, Type type, JsonSerializationContext jsonSerializationContext) {
        JsonElement jsonElement = jsonSerializationContext.serialize(object, object.getClass());

        JsonObject jsonObject = jsonElement.getAsJsonObject();
        jsonObject.addProperty(CLASS_META_KEY, object.getClass().getSimpleName());

        return jsonElement;
    }
}
