package com.utils.rest;

public enum BodyType {

    FORM_DATA,
    JSON
}
