package com.utils.rest;

public enum MethodType {
    POST,
    PUT,
    GET,
    DELETE
}
