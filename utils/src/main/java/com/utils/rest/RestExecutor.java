package com.utils.rest;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.utils.BuildConfig;
import com.utils.base.AppBaseConstants;
import com.utils.log.Logger;
import com.utils.util.Util;
import com.utils.validator.Validator;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import javax.net.ssl.HttpsURLConnection;

class RestExecutor<T> extends AsyncTask<Param, Void, T> implements AppBaseConstants
{

    private static final String TAG = RestExecutor.class.getSimpleName();
    private final String charset = "UTF-8";
    private final String BOUNDARY = "**********";
    private final String TWO_HYPHENS = "--";
    private final String LINE_FEED = "\n";

    private final String JSON_MEDIA_TYPE = "application/json";
    private final String JSON_MULTIPART = "multipart/form-data; boundary=" + BOUNDARY;

    private final String baseUri;
    private final RestResultReceiver restResultReceiver;
    private String uri;
    private Object payload;
    private Type responseType;
    private MethodType methodType;
    private BodyType bodyType;

    private Gson gson;

    public RestExecutor(String baseUri, RestResultReceiver RestResultReceiver)
    {
        this.baseUri = baseUri;
        this.restResultReceiver = RestResultReceiver;
    }

    public void setUri(String uri)
    {
        this.uri = uri;
    }

    public void setPayload(Object payload)
    {
        this.payload = payload;
    }

    public void setResponseType(Type responseType)
    {
        this.responseType = responseType;
    }

    public void setMethodType(MethodType methodType)
    {
        this.methodType = methodType;
    }

    public void setBodyType(BodyType bodyType)
    {
        this.bodyType = bodyType;
    }

    public void setGson(Gson gson)
    {
        this.gson = gson;
    }

    @Override
    protected T doInBackground(Param... params)
    {
        T retVal = null;

        try
        {
            if (Util.isInternetEnabled())
            {
                Uri.Builder builder = Uri.parse(baseUri).buildUpon();

                if (uri.contains("/"))
                {
                    String[] split = uri.split("/");

                    for (String s : split)
                    {
                        builder.appendPath(s);
                    }
                }
                else
                {
                    builder.appendPath(uri);
                }

                if (Validator.isValid(params) && params.length > 0)
                {
                    for (Param param : params)
                    {
                        if (param instanceof QueryParam)
                            builder.appendQueryParameter(param.getName(), param.getValue());
                    }
                }

//                Authenticator.setDefault(new Authenticator() {
//                    protected PasswordAuthentication getPasswordAuthentication() {
//                        return new PasswordAuthentication("root", "5864e86b-1646-492a-86e5-c562d3bad267".toCharArray());
//                    }
//                });

                URL url = new URL(builder.build().toString());
                if (BuildConfig.DEBUG)
                {
                    Log.d(TAG, "doInBackground: url - " + url.toString());
                }
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setUseCaches(false);

                switch (bodyType)
                {
                    case JSON:
                        httpURLConnection.setRequestProperty("Content-Type", JSON_MEDIA_TYPE);
                        break;

                    case FORM_DATA:
                        httpURLConnection.setRequestProperty("Content-Type", JSON_MULTIPART);
                        break;
                }

                httpURLConnection.setRequestProperty("Accept", "application/json");
                httpURLConnection.setRequestMethod(methodType.name().toUpperCase());
                httpURLConnection.setConnectTimeout(5 * 1000);

                if (Validator.isValid(params) && params.length > 0)
                {
                    for (Param param : params)
                    {
                        if (param instanceof HeaderParam)
                            httpURLConnection.addRequestProperty(param.getName(), param.getValue());
                    }
                }

                if (Validator.isValid(payload))
                {
                    DataOutputStream dataOutputStream = new DataOutputStream(httpURLConnection.getOutputStream());

                    switch (bodyType)
                    {
                        case JSON:
                        {
                            String payloadString = gson.toJson(payload);
                            Log.d(TAG, "doInBackground: " + payloadString);
                            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(dataOutputStream, charset));
                            bufferedWriter.write(payloadString);
                            bufferedWriter.flush();
                            bufferedWriter.close();
                        }
                        break;

                        case FORM_DATA:
                        {
                            PrintWriter printWriter = new PrintWriter(new OutputStreamWriter(dataOutputStream, charset), true);

                            Class<?> componentClass = payload.getClass();
                            for (Field field : componentClass.getDeclaredFields())
                            {
                                field.setAccessible(true);

                                String filedName = field.getName();
                                Object filedValue = field.get(payload);

                                SerializedName serializedName = field.getAnnotation(SerializedName.class);
                                if (Validator.isValid(serializedName))
                                    filedName = serializedName.value();

                                if (filedValue instanceof File)
                                    addFilePart(printWriter, dataOutputStream, filedName, (File) filedValue);
                                else if (filedValue instanceof String)
                                    addFormField(printWriter, filedName, (String) filedValue);
                                else
                                    addFormField(printWriter, filedName, gson.toJson(filedValue));
                            }

                            printWriter.append(TWO_HYPHENS + BOUNDARY + TWO_HYPHENS);
                            printWriter.close();
                        }
                        break;
                    }

                    dataOutputStream.close();
                }

                try
                {
                    BufferedReader bufferedReader;

                    int responseCode = httpURLConnection.getResponseCode();

                    Log.d(TAG, "doInBackground: responseCode - " + responseCode);
                    if (responseCode == HttpsURLConnection.HTTP_OK || responseCode == HttpsURLConnection.HTTP_CREATED || responseCode == HttpsURLConnection.HTTP_ACCEPTED)
                    {
                        bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                    }
                    else
                    {
                        bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getErrorStream()));
                    }

                    StringBuilder response = new StringBuilder();
                    String line;
                    while ((line = bufferedReader.readLine()) != null)
                    {
                        response.append(line);
                        response.append('\r');
                    }

                    bufferedReader.close();

                    String responseData = response.toString();
                    if (BuildConfig.DEBUG)
                    {
                        Logger.logInfo(TAG, responseData);
                    }

                    retVal = gson.fromJson(responseData, responseType);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

                httpURLConnection.disconnect();
            }
            else
            {
                int a = 0;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return retVal;
    }

    @Override
    protected void onPostExecute(T result)
    {
        restResultReceiver.onCompleted(result);
    }

    private void addFormField(PrintWriter printWriter, String name, String value)
    {
        printWriter.append(TWO_HYPHENS + BOUNDARY + LINE_FEED);
        printWriter.append("Content-Disposition: form-data; name=\"" + name + "\"" + LINE_FEED);
        printWriter.append("Content-Type: text/plain; charset=" + charset + LINE_FEED);
        printWriter.append(LINE_FEED);
        printWriter.append(value + LINE_FEED);
        printWriter.flush();
    }

    private void addFilePart(PrintWriter printWriter, OutputStream outputStream, String
            fieldName, File uploadFile) throws IOException
    {
        String fileName = uploadFile.getName();

        printWriter.append(TWO_HYPHENS + BOUNDARY + LINE_FEED);
        printWriter.append("Content-Disposition: form-data; name=\"" + fieldName + "\"; filename=\"" + fileName + "\"" + LINE_FEED);
        printWriter.append("Content-Type: " + URLConnection.guessContentTypeFromName(fileName) + LINE_FEED);
        printWriter.append("Content-Transfer-Encoding: binary").append(LINE_FEED);
        printWriter.append(LINE_FEED);
        printWriter.flush();

        FileInputStream inputStream = new FileInputStream(uploadFile);
        byte[] buffer = new byte[4096];
        int bytesRead;

        while ((bytesRead = inputStream.read(buffer)) != -1)
        {
            outputStream.write(buffer, 0, bytesRead);
        }

        outputStream.flush();
        inputStream.close();

        printWriter.append(LINE_FEED);
        printWriter.flush();
    }
}