package com.utils.rest;

public interface RestResultReceiver<T> {

    void onCompleted(T data);
}