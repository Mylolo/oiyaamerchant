package com.utils.util;

import androidx.annotation.NonNull;
import android.text.format.DateFormat;
import android.text.format.DateUtils;

import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class DateTimeUtil {

    public static final String EEEE_MMMM_dd_hh_mm_a = "EEEE, MMMM dd 'at' hh:mm a";
    public static final String EEE_dd_MMM_yyyy_at_hh_mm_a = "EEE, dd MMM yyyy 'at' hh:mm a";
    public static final String EEE_dd_MMM_yyyy = "EEE, dd MMM yyyy";
    public static final String EEEE_dd_MMMM_yyyy = "EEEE, dd MMMM yyyy";
    public static final String dd_MMM_yyyy = "dd MMM yyyy";
    public static final String dd_MM_yyyy = "dd/MM/yyyy";
    public static final String MM_dd_yyyy = " MM/dd/yyyy";
    public static final String yyyy__MM__dd = "yyyy-MM-dd";
    public static final String hh_mm_a = "hh:mm a";

    public static String getFormattedDate(long milliseconds, @NonNull String dateFormat) {
        String data = DateFormat.format(dateFormat, milliseconds).toString();

        if (data.contains("am"))
            data.replace("am", "AM");

        if (data.contains("a.m"))
            data.replace("a.m", "AM");

        if (data.contains("a.m."))
            data.replace("a.m.", "AM");

        if (data.contains("pm"))
            data.replace("pm", "PM");

        if (data.contains("p.m"))
            data.replace("p.m", "PM");

        if (data.contains("p.m."))
            data.replace("p.m.", "PM");

        return data;
    }

    public static String getRelativeTime(long time) {
        return DateUtils.getRelativeTimeSpanString(time, System.currentTimeMillis(), DateUtils.DAY_IN_MILLIS).toString();
    }

    public static String getCountDown(long milliseconds) {
        return String.format("%02d min %02d sec", TimeUnit.MILLISECONDS.toMinutes(milliseconds), TimeUnit.MILLISECONDS.toSeconds(milliseconds) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(milliseconds)));
    }

    public static int getCurrentHour() {
        Calendar calendar = getCalendar();
        calendar.setTime(new Date());

        return calendar.get(Calendar.HOUR_OF_DAY);
    }

    public static int getHour(long milliSeconds) {
        Calendar calendar = getCalendar();
        calendar.setTime(new Date(milliSeconds));

        return calendar.get(Calendar.HOUR_OF_DAY);
    }

    public static long getSecondsFromMidNight() {
        Calendar midnightCalendar = getCalendar();
        midnightCalendar.set(Calendar.HOUR_OF_DAY, 0);
        midnightCalendar.set(Calendar.MINUTE, 0);
        midnightCalendar.set(Calendar.SECOND, 0);
        long midnightTimeInMillis = midnightCalendar.getTimeInMillis();

        Calendar currentTimeCalendar = getCalendar();
        long currentTimeInMillis = currentTimeCalendar.getTimeInMillis();

        return TimeUnit.MILLISECONDS.toSeconds(currentTimeInMillis - midnightTimeInMillis);
    }

    public static Calendar getCalendar() {
        return Calendar.getInstance();
    }
}
