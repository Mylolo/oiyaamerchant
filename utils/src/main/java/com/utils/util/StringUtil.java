package com.utils.util;

import androidx.annotation.NonNull;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;

import com.utils.validator.Validator;

import java.util.Collection;
import java.util.Map;

public class StringUtil {

    public static String getValidData(String data) {
        if (TextUtils.isEmpty(data))
            return "";
        else if (data.equalsIgnoreCase("null"))
            return "";
        else
            return data;
    }

    public static String getValidData(String data, @NonNull String returnData) {
        if (TextUtils.isEmpty(data))
            return returnData;
        else if (data.equalsIgnoreCase("null"))
            return returnData;
        else
            return data;
    }

    public static String[] stringToArray(@NonNull String data) {
        String[] retVal = new String[data.length()];
        for (int i = 0; i < data.length(); i++) {
            retVal[i] = String.valueOf(data.charAt(i));
        }

        return retVal;
    }

    public static String toCSV(@NonNull Collection<?> data) {
        return TextUtils.join(", ", data);
    }

    public static String mapValueToCSV(@NonNull Map<?, ?> data) {
        String retVal = null;

        for (Map.Entry<?, ?> entry : data.entrySet()) {
            if (Validator.isValid(retVal))
                retVal += ", " + entry.getValue();
            else
                retVal = (String) entry.getValue();
        }

        return retVal;
    }

    public static String mapKeyToCSV(@NonNull Map<?, ?> data) {
        String retVal = null;

        for (Map.Entry<?, ?> entry : data.entrySet()) {
            if (Validator.isValid(retVal))
                retVal += ", " + entry.getKey();
            else
                retVal = (String) entry.getKey();
        }

        return retVal;
    }

    public static String limitString(@NonNull String data, int length) {
        if (data.length() <= length)
            return data;
        else
            return data.substring(0, length - 2) + "...";
    }

    public static String doPadding(int data) {
        return data >= 10 ? String.valueOf(data) : ("0" + data);
    }

    public static Spanned fromHTML(String data) {
        if (Util.isNougat())
            return Html.fromHtml(data, Html.FROM_HTML_MODE_COMPACT);
        else
            return Html.fromHtml(data);
    }
}
