package com.utils.util;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.LightingColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Geocoder;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.Settings;
import androidx.annotation.AnyRes;
import androidx.annotation.AttrRes;
import androidx.annotation.ColorInt;
import androidx.annotation.ColorRes;
import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;

import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.model.LatLng;
import com.utils.R;
import com.utils.base.Task;
import com.utils.data.Address;
import com.utils.validator.Validator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.Set;

public class Util {

    public static void openCamera(@NonNull Activity activity, @NonNull File capturedImageLocation, int requestCode) {
        // Need to add FileProvider permission in app AndroidManifest, and XML file in res
        // (Already added) check for reference
        Uri uri = FileProvider.getUriForFile(activity, activity.getApplicationContext().getPackageName() + ".provider", capturedImageLocation);

        Intent intentCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intentCamera.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        activity.startActivityForResult(intentCamera, requestCode);
    }

    public static void openCamera(@NonNull Fragment fragment, @NonNull File capturedImageLocation, int requestCode) {
        Context context = fragment.getContext();

        // Need to add FileProvider permission in app AndroidManifest, and XML file in res
        // (Already added) check for reference
        Uri uri = FileProvider.getUriForFile(context, context.getPackageName() + ".provider", capturedImageLocation);

        Intent intentCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intentCamera.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        fragment.startActivityForResult(intentCamera, requestCode);
    }

    public static void openGallery(@NonNull Activity activity, boolean allowMultiSelect, int requestCode) {
        Intent intentGallery = new Intent();
        intentGallery.setType("image/*");
        intentGallery.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, allowMultiSelect);
        intentGallery.setAction(Intent.ACTION_PICK);
        activity.startActivityForResult(Intent.createChooser(intentGallery, "Select Picture"), requestCode);
    }

    public static void openGallery(@NonNull Fragment fragment, boolean allowMultiSelect, int requestCode) {
        Intent intentGallery = new Intent();
        intentGallery.setType("image/*");
        intentGallery.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, allowMultiSelect);
        intentGallery.setAction(Intent.ACTION_PICK);
        fragment.startActivityForResult(Intent.createChooser(intentGallery, "Select Picture"), requestCode);
    }

    public static void restartApp(Activity activity, Class<?> activityToStart) {
        Intent intent = new Intent(activity, activityToStart);
        PendingIntent pendingIntent = PendingIntent.getActivity(activity, 130219, intent, PendingIntent.FLAG_CANCEL_CURRENT);

        AlarmManager alarmManager = (AlarmManager) activity.getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC, System.currentTimeMillis() + 100, pendingIntent);
        System.exit(0);
    }

    public static void killApp() {
        System.exit(0);
    }

    public static void makeDelay(long milliSeconds, final Task<Boolean> task) {
        new Handler().postDelayed(() -> task.onSuccess(true), milliSeconds);
    }

    public static void requestFocus(@NonNull Context context, @NonNull View view) {
        if (view.requestFocus())
            ((Activity) context).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }

    public static void changeCursorColor(View view) {
        try {
            Field field = TextView.class.getDeclaredField("mCursorDrawableRes");
            field.setAccessible(true);
            field.set(view, R.drawable.background_cursor);
        } catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException e) {

        }
    }

    public static String getAndroidId(@NonNull Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static int getColor(@NonNull Context context, @ColorRes int color) {
        if (Util.isMarshmallow())
            return ContextCompat.getColor(context, color);
        else
            return context.getResources().getColor(color);
    }

    public static Drawable getDrawable(@NonNull Context context, @DrawableRes int resource) {
        if (Util.isMarshmallow())
            return ContextCompat.getDrawable(context, resource);
        else
            return context.getResources().getDrawable(resource);
    }

    private static int getColorFromAttr(@NonNull Context context, @AttrRes final int attributeColor) {
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(attributeColor, typedValue, true);

        return typedValue.data;
    }

    public static final Uri getUriFromResource(@NonNull Context context, @AnyRes int resId) {
        Resources resources = context.getResources();
        Uri resUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE +
                "://" + resources.getResourcePackageName(resId)
                + '/' + resources.getResourceTypeName(resId)
                + '/' + resources.getResourceEntryName(resId));
        return resUri;
    }

    public static Bitmap getBitmap(@NonNull Drawable drawable) {
        Bitmap bitmap;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if (Validator.isValid(bitmapDrawable.getBitmap()))
                return bitmapDrawable.getBitmap();
        }

        if (drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0)
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        else
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    public static Bitmap getCircularBitmap(Bitmap bitmap) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2, bitmap.getWidth() / 2, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }

    public static Drawable resizeDrawable(@NonNull Context context, @NonNull Drawable drawable, int width, int height) {
        if ((Validator.isValid(drawable)) && !(drawable instanceof BitmapDrawable))
            return drawable;

        Bitmap bitmap = Bitmap.createScaledBitmap(((BitmapDrawable) drawable).getBitmap(), width, height, false);
        drawable = new BitmapDrawable(context.getResources(), bitmap);

        return drawable;
    }

    public static void changeImageColor(@NonNull final ImageView imageView, @ColorInt int fromColor, @ColorInt int toColor) {
        ValueAnimator imageColorChangeAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), fromColor, toColor);
        imageColorChangeAnimation.addUpdateListener(animator -> imageView.setColorFilter((Integer) animator.getAnimatedValue()));
        imageColorChangeAnimation.setDuration(150);
        imageColorChangeAnimation.start();
    }

    public static Bitmap changeImageColor(@NonNull Bitmap bitmap, @ColorInt int color) {
        Bitmap resultBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth() - 1, bitmap.getHeight() - 1);

        Paint paint = new Paint();
        paint.setColorFilter(new LightingColorFilter(color, 1));

        Canvas canvas = new Canvas(resultBitmap);
        canvas.drawBitmap(resultBitmap, 0, 0, paint);

        return resultBitmap;
    }

    public static int getDeviceWidth(@NonNull Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return displayMetrics.widthPixels;
    }

    public static int getDeviceHeight(@NonNull Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return displayMetrics.heightPixels;
    }

    public static int getDeviceDpi(@NonNull Context context) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return metrics.densityDpi;
    }

    public static int dpToPx(int dp) {
        return Math.round(dp * (Resources.getSystem().getDisplayMetrics().xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public static int pxToDp(int pixel) {
        return Math.round(pixel / (Resources.getSystem().getDisplayMetrics().xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public static float dipToPixels(float dipValue) {
        final float scale = Resources.getSystem().getDisplayMetrics().density;
        return dipValue * scale + 0.5f;
    }

    public static float spToPixels(float spValue) {
        final float fontScale = Resources.getSystem().getDisplayMetrics().scaledDensity;
        return spValue * fontScale + 0.5f;
    }

    public static void hideSoftKeyboard(@NonNull Context context, @NonNull View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void showSoftKeyboard(@NonNull Context context, @NonNull View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
    }

    public static void disableScreenShot(@NonNull Activity activity) {
        activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
    }

    public static void centerToolbarTitle(Toolbar toolbar) {
        Toolbar.LayoutParams layoutParams = new Toolbar.LayoutParams(Toolbar.LayoutParams.WRAP_CONTENT, Toolbar.LayoutParams.MATCH_PARENT);
        layoutParams.gravity = Gravity.CENTER;
        getToolbarTitleTextView(toolbar).setLayoutParams(layoutParams);
    }

    public static AppCompatTextView getToolbarTitleTextView(Toolbar toolbar) {
        AppCompatTextView textView = null;

        try {
            Field field = toolbar.getClass().getDeclaredField("mTitleTextView");
            field.setAccessible(true);

            textView = (AppCompatTextView) field.get(toolbar);
        } catch (NoSuchFieldException e) {

        } catch (IllegalAccessException e) {

        }

        return textView;
    }

    public static boolean isServiceRunning(Context context, Class<? extends Service> serviceClass) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : activityManager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName()))
                return true;
        }

        return false;
    }

    public static boolean isInternetEnabled() {
        try {
            InetAddress inetAddress = InetAddress.getByName("www.google.com");
            return !inetAddress.equals("");
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isGPSEnabled(@NonNull Context context) {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public static int getRandomNumber(int end) {
        Random random = new Random();
        return random.nextInt(end);
    }

    public static int getNumberBetween(int start, int end) {
        Random random = new Random();
        return random.nextInt(end - start) + start; // This gives a random integer between start (inclusive) and end (exclusive)
    }

    public static boolean isTablet(Context context) {
        boolean xlarge = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == 4);
        boolean large = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE);

        return (xlarge || large);
    }

    public static int getStatusBarHeight(@NonNull Context context) {
        int result = 0;

        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0)
            result = context.getResources().getDimensionPixelSize(resourceId);

        return result;
    }

    public static int getNavigationBarHeight(@NonNull Context context) {
        int result = 0;

        int resourceId = context.getResources().getIdentifier("navigation_bar_height", "dimen", "android");
        if (resourceId > 0)
            result = context.getResources().getDimensionPixelSize(resourceId);

        return result;
    }

    public static void setPortraitOrientation(@NonNull Context context) {
        ((Activity) context).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    public static void setLandScapeOrientation(@NonNull Context context) {
        ((Activity) context).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
    }

    public static void setStatusBarColor(@NonNull Activity activity, int color) {
        if (Util.isLollipop()) {
            activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            activity.getWindow().setStatusBarColor(Util.getColor(activity, color));
        }
    }

    public static File compressImage(@NonNull File file, @NonNull float compressImageWidth, @NonNull float compressImageHeight) throws Exception {
        if (!file.exists())
            throw new FileNotFoundException("File does not exists");

        BitmapFactory.Options options = new BitmapFactory.Options();
        // By setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If you try the use the bitmap here, you will getByJobCode null.
        options.inJustDecodeBounds = true;

        BitmapFactory.decodeFile(file.getAbsolutePath(), options);

        int actualImageWidth = options.outWidth;
        int actualImageHeight = options.outHeight;

        float actualImageRatio = actualImageWidth / actualImageHeight;
        float compressImageRatio = compressImageWidth / compressImageHeight;

        // Width and Height values are set maintaining the aspect ratio of the image
        if (actualImageHeight > compressImageHeight || actualImageWidth > compressImageWidth) {
            if (actualImageRatio < compressImageRatio) {
                actualImageRatio = compressImageHeight / actualImageHeight;
                actualImageWidth = (int) (actualImageRatio * actualImageWidth);
                actualImageHeight = (int) compressImageHeight;
            } else if (actualImageRatio > compressImageRatio) {
                actualImageRatio = compressImageWidth / actualImageWidth;
                actualImageHeight = (int) (actualImageRatio * actualImageHeight);
                actualImageWidth = (int) compressImageWidth;
            } else {
                actualImageHeight = (int) compressImageHeight;
                actualImageWidth = (int) compressImageWidth;
            }
        }

        options.inSampleSize = calculateInSampleSize(options, actualImageWidth, actualImageHeight); // Setting inSampleSize value allows to load a scaled down version of the original image
        options.inJustDecodeBounds = false; // inJustDecodeBounds set to false to load the actual bitmap
        options.inPurgeable = true; // This options allow android to claim the bitmap memory if it runs low on memory
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), options);
        Bitmap scaledBitmap = Bitmap.createBitmap(actualImageWidth, actualImageHeight, Bitmap.Config.ARGB_8888);

        float ratioX = actualImageWidth / (float) options.outWidth;
        float ratioY = actualImageHeight / (float) options.outHeight;
        float middleX = actualImageWidth / 2.0f;
        float middleY = actualImageHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bitmap, middleX - bitmap.getWidth() / 2, middleY - bitmap.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

        Matrix matrix = new Matrix();
        ExifInterface exif = new ExifInterface(file.getAbsolutePath());
        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
        if (orientation == 6)
            matrix.postRotate(90);
        else if (orientation == 3)
            matrix.postRotate(180);
        else if (orientation == 8)
            matrix.postRotate(270);

        scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);

        File compressImageFile = file;
        if (!compressImageFile.exists())
            compressImageFile.mkdirs();

        FileOutputStream fileOutputStream = new FileOutputStream(compressImageFile);
        scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, fileOutputStream);

        return compressImageFile;
    }

    public static int getSmallImageSize(@NonNull Context context) {
        int widthInPixel = getDeviceWidth(context);
        return widthInPixel / 10;
    }

    public static int getMediumImageSize(@NonNull Context context) {
        int widthInPixel = getDeviceWidth(context);
        return widthInPixel / 7;
    }

    public static int getLargeImageSize(@NonNull Context context) {
        int widthInPixel = getDeviceWidth(context);
        return widthInPixel / 4;
    }

    public static int getExtraLargeImageSize(@NonNull Context context) {
        int widthInPixel = getDeviceWidth(context);
        return widthInPixel / 3;
    }

    public static String getVersionName(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            return null;
        }
    }

    public static String getVersionCode(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return String.valueOf(packageInfo.versionCode);
        } catch (PackageManager.NameNotFoundException e) {
            return null;
        }
    }

    public static int getOSVersion() {
        return Build.VERSION.SDK_INT;
    }

    public static boolean isHoneyComb() {
        return getOSVersion() >= Build.VERSION_CODES.HONEYCOMB;
    }

    public static boolean isHoneyCombMR1() {
        return getOSVersion() >= Build.VERSION_CODES.HONEYCOMB_MR1;
    }

    public static boolean isHoneyCombMR2() {
        return getOSVersion() >= Build.VERSION_CODES.HONEYCOMB_MR2;
    }

    public static boolean isIceCreamSandwich() {
        return getOSVersion() >= Build.VERSION_CODES.ICE_CREAM_SANDWICH;
    }

    public static boolean isIceCreamSandwichMR1() {
        return getOSVersion() >= Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1;
    }

    public static boolean isJellyBean() {
        return getOSVersion() >= Build.VERSION_CODES.JELLY_BEAN;
    }

    public static boolean isJellyBeanMR1() {
        return getOSVersion() >= Build.VERSION_CODES.JELLY_BEAN_MR1;
    }

    public static boolean isJellyBeanMR2() {
        return getOSVersion() >= Build.VERSION_CODES.JELLY_BEAN_MR2;
    }

    public static boolean isKitkat() {
        return getOSVersion() >= Build.VERSION_CODES.KITKAT;
    }

    public static boolean isLollipop() {
        return getOSVersion() >= Build.VERSION_CODES.LOLLIPOP;
    }

    public static boolean isLollipopMR1() {
        return getOSVersion() >= Build.VERSION_CODES.LOLLIPOP_MR1;
    }

    public static boolean isMarshmallow() {
        return getOSVersion() >= Build.VERSION_CODES.M;
    }

    public static boolean isNougat() {
        return getOSVersion() >= Build.VERSION_CODES.N;
    }

    public static boolean isNougatMR1() {
        return getOSVersion() >= Build.VERSION_CODES.N_MR1;
    }

    public static boolean isOreo() {
        return getOSVersion() >= Build.VERSION_CODES.O;
    }

    public static boolean isOreoMR1() {
        return getOSVersion() >= Build.VERSION_CODES.O_MR1;
    }

    public static boolean isPlayServicesAvailable(@NonNull Context context) {
        boolean retVal = false;

        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int result = googleAPI.isGooglePlayServicesAvailable(context);
        if (result == ConnectionResult.SUCCESS)
            retVal = true;

        return retVal;
    }

    public static Address getAddress(@NonNull Context context, @NonNull LatLng latLng) throws IllegalArgumentException, IOException {
        Address retVal = null;

        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        List<android.location.Address> addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
        if (Validator.isValid(addresses)) {
            android.location.Address address = addresses.get(0);
            ArrayList<String> addressList = new ArrayList<>();
            for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                addressList.add(address.getAddressLine(i));
            }

            String addressLine = StringUtil.toCSV(addressList);
            String line1 = address.getThoroughfare() + ", " + address.getSubThoroughfare();
            String line2 = address.getSubLocality();
            String city = address.getLocality();
            String state = address.getAdminArea();
            String country = address.getCountryCode();
            String pinCode = address.getPostalCode();

            retVal = new Address();
            retVal.setAddressLine(addressLine);
            retVal.setLine1(line1);
            retVal.setLine2(line2);
            retVal.setCity(city);
            retVal.setState(state);
            retVal.setCountry(country);
            retVal.setZipCode(pinCode);
        }

        return retVal;
    }

    public static LatLng getLatLng(@NonNull Context context, @NonNull String address) throws IllegalArgumentException, IOException {
        LatLng retVal = null;

        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        List<android.location.Address> addresses = geocoder.getFromLocationName(address, 1);
        if (Validator.isValid(addresses)) {
            android.location.Address tempAddress = addresses.get(0);
            retVal = new LatLng(tempAddress.getLatitude(), tempAddress.getLongitude());
        }

        return retVal;
    }

    public static <T> List<T> asList(T... values) {
        return Arrays.asList(values);
    }

    public static <T> Set<T> asSet(T... values) {
        return new HashSet<>(Arrays.asList(values));
    }

    private static int calculateInSampleSize(BitmapFactory.Options options, int requiredImageWidth, int requiredImageHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > requiredImageHeight || width > requiredImageWidth) {
            final int heightRatio = Math.round((float) height / (float) requiredImageHeight);
            final int widthRatio = Math.round((float) width / (float) requiredImageWidth);

            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }

        final float totalPixels = width * height;
        final float totalReqPixelsCap = requiredImageWidth * requiredImageHeight * 2;

        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }

    /**
     * Method checks if the app is in background or not
     */
    public static boolean isAppIsInBackground(Context context)
    {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH)
        {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses)
            {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND)
                {
                    for (String activeProcess : processInfo.pkgList)
                    {
                        if (activeProcess.equals(context.getPackageName()))
                        {
                            isInBackground = false;
                        }
                    }
                }
            }
        }
        else
        {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName()))
            {
                isInBackground = false;
            }
        }
        return isInBackground;
    }

    // Clears notification tray messages
    public static void clearNotifications(Context context)
    {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }

    public static String getCurrentSsid(Context context)
    {
        String ssid = null;
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo == null) {
            return null;
        }

        if (networkInfo.isConnected()) {
            final WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            final WifiInfo connectionInfo = wifiManager.getConnectionInfo();
            if (connectionInfo != null && !TextUtils.isEmpty(connectionInfo.getSSID())) {
                ssid = connectionInfo.getSSID();
            }

        }

        return ssid;
    }

}
