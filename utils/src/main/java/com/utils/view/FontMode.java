package com.utils.view;

public enum FontMode {

    NORMAL,
    BOLD,
    ITALIC,
    BOLD_ITALIC
}
