package com.utils.view.button;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatButton;

import com.google.android.material.button.MaterialButton;
import com.utils.R;
import com.utils.util.Util;
import com.utils.validator.Validator;
import com.utils.view.FontMode;
import com.utils.view.FontType;

public class Button extends AppCompatButton
{

    private Context context;
    private AttributeSet attributeSet;
    private ButtonType buttonType;
    private Drawable background;
    private FontType fontType;
    private FontMode fontMode;

    public Button(Context context)
    {
        super(context);

        this.context = context;
        init();
    }

    public Button(Context context, AttributeSet attrs)
    {
        super(context, attrs);

        this.context = context;
        this.attributeSet = attrs;
        init();
    }

    public Button(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);

        this.context = context;
        this.attributeSet = attrs;
        init();
    }

    public void setFontStyle(FontType fontType, FontMode fontMode)
    {
        this.fontType = fontType;
        this.fontMode = fontMode;
        applyFont();
    }

    public void setButtonType(ButtonType buttonType)
    {
        this.buttonType = buttonType;
        applyBackground();
    }

    public void setBackgroundRes(Drawable background)
    {
        this.background = background;
        applyBackground();
    }

    private void init()
    {
        getAttributeSet();
        setHeight(Util.dpToPx(48));
        applyBackground();
        applyFont();
    }

    private void getAttributeSet()
    {
        TypedArray typedArray = null;

        try
        {
            typedArray = context.obtainStyledAttributes(attributeSet, R.styleable.Button);

            buttonType = ButtonType.values()[typedArray.getInt(R.styleable.Button_type, ButtonType.NORMAL.ordinal())];
            fontType = FontType.values()[typedArray.getInt(R.styleable.Button_fontType, FontType.ROBOTO_REGULAR.ordinal())];
            fontMode = FontMode.values()[typedArray.getInt(R.styleable.Button_fontMode, FontMode.NORMAL.ordinal())];
            background = typedArray.getDrawable(R.styleable.Button_background);
        }
        finally
        {
            if (Validator.isValid(typedArray))
                typedArray.recycle();
        }
    }

    private void applyFont()
    {
        Typeface typeface;

        switch (fontType)
        {
            case ROBOTO_BLACK:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Black.ttf");
                break;

            case ROBOTO_BLACK_ITALIC:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-BlackItalic.ttf");
                break;

            case ROBOTO_BOLD:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Bold.ttf");
                break;

            case ROBOTO_BOLD_ITALIC:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-BoldItalic.ttf");
                break;

            case ROBOTO_CONDENSED_BOLD:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/RobotoCondensed-Bold.ttf");
                break;

            case ROBOTO_CONDENSED_BOLD_ITALIC:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/RobotoCondensed-BoldItalic.ttf");
                break;

            case ROBOTO_CONDENSED_ITALIC:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/RobotoCondensed-Italic.ttf");
                break;

            case ROBOTO_CONDENSED_LIGHT:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/RobotoCondensed-Light.ttf");
                break;

            case ROBOTO_CONDENSED_LIGHT_ITALIC:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/RobotoCondensed-LightItalic.ttf");
                break;

            case ROBOTO_CONDENSED_REGULAR:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/RobotoCondensed-Regular.ttf");
                break;

            case ROBOTO_ITALIC:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Italic.ttf");
                break;

            case ROBOTO_LIGHT:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Light.ttf");
                break;

            case ROBOTO_LIGHT_ITALIC:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-LightItalic.ttf");
                break;

            case ROBOTO_MEDIUM:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Medium.ttf");
                break;

            case ROBOTO_MEDIUM_ITALIC:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-MediumItalic.ttf");
                break;

            case ROBOTO_REGULAR:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Regular.ttf");
                break;

            case ROBOTO_THIN:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Thin.ttf");
                break;

            case ROBOTO_THIN_ITALIC:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-ThinItalic.ttf");
                break;

            case CALIBRI:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Calibri.ttf");
                break;

            case CALIBRI_BOLD:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Calibri-Bold.ttf");
                break;

            case CALIBRI_ITALIC:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Calibri-Itailc.ttf");
                break;

            case CALIBRI_BOLD_ITALIC:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Calibri-BoldItalic.ttf");
                break;

            default:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Black.ttf");
                break;
        }

        switch (fontMode)
        {
            case NORMAL:
                setTypeface(typeface, Typeface.NORMAL);
                break;

            case BOLD:
                setTypeface(typeface, Typeface.BOLD);
                break;

            case ITALIC:
                setTypeface(typeface, Typeface.ITALIC);
                break;

            case BOLD_ITALIC:
                setTypeface(typeface, Typeface.BOLD_ITALIC);
                break;

            default:
                setTypeface(typeface, Typeface.NORMAL);
                break;
        }
    }

    private void applyBackground()
    {
        switch (buttonType)
        {
            case NORMAL:
                break;

            case TRANSPARENT:
                setBackgroundResource(android.R.color.transparent);
                setTextColor(Util.getColor(getContext(), R.color.lightTextPrimary));
                break;

            case RED:
                setBackgroundResource(R.drawable.background_button_red);
                setTextColor(Util.getColor(getContext(), R.color.white));
                break;

            case PINK:
                setBackgroundResource(R.drawable.background_button_pink);
                setTextColor(Util.getColor(getContext(), R.color.white));
                break;

            case PURPLE:
                setBackgroundResource(R.drawable.background_button_purple);
                setTextColor(Util.getColor(getContext(), R.color.white));
                break;

            case DEEP_PURPLE:
                setBackgroundResource(R.drawable.background_button_deep_purple);
                setTextColor(Util.getColor(getContext(), R.color.white));
                break;

            case INDIGO:
                setBackgroundResource(R.drawable.background_button_indigo);
                setTextColor(Util.getColor(getContext(), R.color.white));
                break;

            case BLUE:
                setBackgroundResource(R.drawable.background_button_blue);
                setTextColor(Util.getColor(getContext(), R.color.white));
                break;

            case LIGHT_BLUE:
                setBackgroundResource(R.drawable.background_button_light_blue);
                setTextColor(Util.getColor(getContext(), R.color.white));
                break;

            case CYAN:
                setBackgroundResource(R.drawable.background_button_cyan);
                setTextColor(Util.getColor(getContext(), R.color.white));
                break;

            case TEAL:
                setBackgroundResource(R.drawable.background_button_teal);
                setTextColor(Util.getColor(getContext(), R.color.white));
                break;

            case GREEN:
                setBackgroundResource(R.drawable.background_button_green);
                setTextColor(Util.getColor(getContext(), R.color.white));
                break;

            case LIGHT_GREEN:
                setBackgroundResource(R.drawable.background_button_light_green);
                setTextColor(Util.getColor(getContext(), R.color.white));
                break;

            case LIME:
                setBackgroundResource(R.drawable.background_button_lime);
                setTextColor(Util.getColor(getContext(), R.color.white));
                break;

            case YELLOW:
                setBackgroundResource(R.drawable.background_button_yellow);
                setTextColor(Util.getColor(getContext(), R.color.white));
                break;

            case AMBER:
                setBackgroundResource(R.drawable.background_button_amber);
                setTextColor(Util.getColor(getContext(), R.color.white));
                break;

            case ORANGE:
                setBackgroundResource(R.drawable.background_button_orange);
                setTextColor(Util.getColor(getContext(), R.color.white));
                break;

            case DEEP_ORANGE:
                setBackgroundResource(R.drawable.background_button_deep_orange);
                setTextColor(Util.getColor(getContext(), R.color.white));
                break;

            case BROWN:
                setBackgroundResource(R.drawable.background_button_brown);
                setTextColor(Util.getColor(getContext(), R.color.white));
                break;

            case GRAY:
                setBackgroundResource(R.drawable.background_button_gray);
                setTextColor(Util.getColor(getContext(), R.color.white));
                break;

            case BLUE_GRAY:
                setBackgroundResource(R.drawable.background_button_blue_gray);
                setTextColor(Util.getColor(getContext(), R.color.white));
                break;

            case CUSTOM:
                setBackgroundDrawable(Validator.isValid(background) ? background : null);
                break;
        }

    }
}
