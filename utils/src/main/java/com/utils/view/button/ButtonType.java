package com.utils.view.button;

public enum ButtonType {

    NORMAL,
    TRANSPARENT,
    RED,
    PINK,
    PURPLE,
    DEEP_PURPLE,
    INDIGO,
    BLUE,
    LIGHT_BLUE,
    CYAN,
    TEAL,
    GREEN,
    LIGHT_GREEN,
    LIME,
    YELLOW,
    AMBER,
    ORANGE,
    DEEP_ORANGE,
    BROWN,
    GRAY,
    BLUE_GRAY,
    CUSTOM
}
