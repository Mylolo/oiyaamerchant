package com.utils.view.button;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

import com.google.android.material.button.MaterialButton;
import com.utils.R;
import com.utils.util.Util;
import com.utils.validator.Validator;
import com.utils.view.FontMode;
import com.utils.view.FontType;

public class CustomMaterialButton extends MaterialButton
{

    private Context context;
    private AttributeSet attributeSet;
    private FontType fontType;
    private FontMode fontMode;

    public CustomMaterialButton(Context context)
    {
        super(context);

        this.context = context;
        init();
    }

    public CustomMaterialButton(Context context, AttributeSet attrs)
    {
        super(context, attrs);

        this.context = context;
        this.attributeSet = attrs;
        init();
    }

    public CustomMaterialButton(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);

        this.context = context;
        this.attributeSet = attrs;
        init();
    }

    private void init()
    {
        getAttributeSet();
        setHeight(Util.dpToPx(48));
        applyFont();
    }

    private void getAttributeSet()
    {
        TypedArray typedArray = null;

        try
        {
            typedArray = context.obtainStyledAttributes(attributeSet, R.styleable.CustomMaterialButton);
            fontType = FontType.values()[typedArray.getInt(R.styleable.CustomMaterialButton_fontType, FontType.ROBOTO_REGULAR.ordinal())];
            fontMode = FontMode.values()[typedArray.getInt(R.styleable.CustomMaterialButton_fontMode, FontMode.NORMAL.ordinal())];
        }
        finally
        {
            if (Validator.isValid(typedArray))
                typedArray.recycle();
        }
    }

    private void applyFont()
    {
        Typeface typeface;

        switch (fontType)
        {
            case ROBOTO_BLACK:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Black.ttf");
                break;

            case ROBOTO_BLACK_ITALIC:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-BlackItalic.ttf");
                break;

            case ROBOTO_BOLD:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Bold.ttf");
                break;

            case ROBOTO_BOLD_ITALIC:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-BoldItalic.ttf");
                break;

            case ROBOTO_CONDENSED_BOLD:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/RobotoCondensed-Bold.ttf");
                break;

            case ROBOTO_CONDENSED_BOLD_ITALIC:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/RobotoCondensed-BoldItalic.ttf");
                break;

            case ROBOTO_CONDENSED_ITALIC:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/RobotoCondensed-Italic.ttf");
                break;

            case ROBOTO_CONDENSED_LIGHT:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/RobotoCondensed-Light.ttf");
                break;

            case ROBOTO_CONDENSED_LIGHT_ITALIC:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/RobotoCondensed-LightItalic.ttf");
                break;

            case ROBOTO_CONDENSED_REGULAR:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/RobotoCondensed-Regular.ttf");
                break;

            case ROBOTO_ITALIC:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Italic.ttf");
                break;

            case ROBOTO_LIGHT:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Light.ttf");
                break;

            case ROBOTO_LIGHT_ITALIC:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-LightItalic.ttf");
                break;

            case ROBOTO_MEDIUM:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Medium.ttf");
                break;

            case ROBOTO_MEDIUM_ITALIC:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-MediumItalic.ttf");
                break;

            case ROBOTO_REGULAR:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Regular.ttf");
                break;

            case ROBOTO_THIN:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Thin.ttf");
                break;

            case ROBOTO_THIN_ITALIC:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-ThinItalic.ttf");
                break;

            case CALIBRI:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Calibri.ttf");
                break;

            case CALIBRI_BOLD:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Calibri-Bold.ttf");
                break;

            case CALIBRI_ITALIC:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Calibri-Itailc.ttf");
                break;

            case CALIBRI_BOLD_ITALIC:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Calibri-BoldItalic.ttf");
                break;

            default:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Black.ttf");
                break;
        }

        switch (fontMode)
        {
            case NORMAL:
                setTypeface(typeface, Typeface.NORMAL);
                break;

            case BOLD:
                setTypeface(typeface, Typeface.BOLD);
                break;

            case ITALIC:
                setTypeface(typeface, Typeface.ITALIC);
                break;

            case BOLD_ITALIC:
                setTypeface(typeface, Typeface.BOLD_ITALIC);
                break;

            default:
                setTypeface(typeface, Typeface.NORMAL);
                break;
        }
    }

}
