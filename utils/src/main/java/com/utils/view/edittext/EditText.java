package com.utils.view.edittext;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import androidx.appcompat.widget.AppCompatEditText;
import android.util.AttributeSet;

import com.utils.R;
import com.utils.util.Util;
import com.utils.validator.Validator;
import com.utils.view.FontMode;
import com.utils.view.FontType;

public class EditText extends AppCompatEditText {

    private Context context;
    private AttributeSet attributeSet;
    private FontType fontType;
    private FontMode fontMode;
    private Drawable hintIcon;
    private Drawable background;

    public EditText(Context context) {
        super(context);

        this.context = context;
        init();
    }

    public EditText(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.context = context;
        this.attributeSet = attrs;
        init();
    }

    public EditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        this.context = context;
        this.attributeSet = attrs;
        init();
    }

    public void setFontType(FontType fontType, FontMode fontMode) {
        this.fontType = fontType;
        this.fontMode = fontMode;
        applyFont();
    }

    public void setHintIcon(Drawable hintIcon) {
        this.hintIcon = hintIcon;
        applyHintIcon();
    }

    @Override
    public void setBackground(Drawable background) {
        this.background = background;
        applyBackground();
    }

    private void init() {
        getAttributeSet();
        setHeight(Util.dpToPx(48));
        applyFont();
        applyHintIcon();
        applyBackground();
    }

    private void getAttributeSet() {
        TypedArray typedArray = null;

        try {
            typedArray = context.obtainStyledAttributes(attributeSet, R.styleable.EditText);

            fontType = FontType.values()[typedArray.getInt(R.styleable.EditText_fontType, FontType.ROBOTO_REGULAR.ordinal())];
            fontMode = FontMode.values()[typedArray.getInt(R.styleable.EditText_fontMode, FontMode.NORMAL.ordinal())];
            hintIcon = typedArray.getDrawable(R.styleable.EditText_hintIcon);
            background = typedArray.getDrawable(R.styleable.EditText_background);
        } finally {
            if (Validator.isValid(typedArray))
                typedArray.recycle();
        }
    }

    private void applyFont() {
        Typeface typeface;

        switch (fontType) {
            case ROBOTO_BLACK:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Black.ttf");
                break;

            case ROBOTO_BLACK_ITALIC:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-BlackItalic.ttf");
                break;

            case ROBOTO_BOLD:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Bold.ttf");
                break;

            case ROBOTO_BOLD_ITALIC:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-BoldItalic.ttf");
                break;

            case ROBOTO_CONDENSED_BOLD:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/RobotoCondensed-Bold.ttf");
                break;

            case ROBOTO_CONDENSED_BOLD_ITALIC:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/RobotoCondensed-BoldItalic.ttf");
                break;

            case ROBOTO_CONDENSED_ITALIC:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/RobotoCondensed-Italic.ttf");
                break;

            case ROBOTO_CONDENSED_LIGHT:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/RobotoCondensed-Light.ttf");
                break;

            case ROBOTO_CONDENSED_LIGHT_ITALIC:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/RobotoCondensed-LightItalic.ttf");
                break;

            case ROBOTO_CONDENSED_REGULAR:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/RobotoCondensed-Regular.ttf");
                break;

            case ROBOTO_ITALIC:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Italic.ttf");
                break;

            case ROBOTO_LIGHT:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Light.ttf");
                break;

            case ROBOTO_LIGHT_ITALIC:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-LightItalic.ttf");
                break;

            case ROBOTO_MEDIUM:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Medium.ttf");
                break;

            case ROBOTO_MEDIUM_ITALIC:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-MediumItalic.ttf");
                break;

            case ROBOTO_REGULAR:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Regular.ttf");
                break;

            case ROBOTO_THIN:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Thin.ttf");
                break;

            case ROBOTO_THIN_ITALIC:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-ThinItalic.ttf");
                break;

            case CALIBRI:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Calibri.ttf");
                break;

            case CALIBRI_BOLD:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Calibri-Bold.ttf");
                break;

            case CALIBRI_ITALIC:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Calibri-BoldItalic.ttf");
                break;

            case CALIBRI_BOLD_ITALIC:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Calibri-Itailc.ttf");
                break;

            default:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Black.ttf");
                break;
        }

        switch (fontMode) {
            case NORMAL:
                setTypeface(typeface, Typeface.NORMAL);
                break;

            case BOLD:
                setTypeface(typeface, Typeface.BOLD);
                break;

            case ITALIC:
                setTypeface(typeface, Typeface.ITALIC);
                break;

            case BOLD_ITALIC:
                setTypeface(typeface, Typeface.BOLD_ITALIC);
                break;

            default:
                setTypeface(typeface, Typeface.NORMAL);
                break;
        }
    }

    private void applyHintIcon() {
        if (Validator.isValid(hintIcon)) {
            setCompoundDrawablesWithIntrinsicBounds(hintIcon, null, null, null);
            setCompoundDrawablePadding(30);
        }

        int padding = Util.dpToPx(5);
        setPadding(padding, padding, padding, padding);
    }

    private void applyBackground() {
        setBackgroundDrawable(Validator.isValid(background) ? background : Util.getDrawable(context, R.drawable.background_edittext));
    }
}
