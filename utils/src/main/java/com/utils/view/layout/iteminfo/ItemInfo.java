package com.utils.view.layout.iteminfo;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.text.Spannable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.utils.R;
import com.utils.validator.Validator;
import com.utils.view.FontMode;
import com.utils.view.FontType;
import com.utils.view.textview.info.TextViewInfo;

public class ItemInfo extends LinearLayout {

    private Context context;
    private AttributeSet attributeSet;
    private int itemId;
    private Drawable itemBackground;
    private Drawable leftIcon;
    private Drawable rightIcon;
    private float titleSize;
    private int titleColor;
    private String title;
    private float subTitleSize;
    private int subTitleColor;
    private String subTitle;
    private FontType fontType;
    private FontMode fontMode;

    private LinearLayout linearLayoutContainer;
    private ImageView imageViewLeft;
    private ImageView imageViewRight;
    private TextViewInfo textViewInfo;

    private ItemInfoClickListener itemInfoClickListener;

    public ItemInfo(Context context) {
        super(context);

        this.context = context;
        init();
    }

    public ItemInfo(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.context = context;
        this.attributeSet = attrs;
        init();
    }

    public ItemInfo(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        this.context = context;
        this.attributeSet = attrs;
        init();
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public void setItemBackground(Drawable itemBackground) {
        this.itemBackground = itemBackground;

        linearLayoutContainer.setBackgroundDrawable(Validator.isValid(itemBackground) ? itemBackground : null);
    }

    public void setLeftIcon(Drawable leftIcon) {
        this.leftIcon = leftIcon;

        if (!Validator.isValid(leftIcon)) {
            imageViewLeft.setVisibility(GONE);
        } else {
            imageViewLeft.setVisibility(VISIBLE);
            imageViewLeft.setImageDrawable(leftIcon);
        }
    }

    public void setRightIcon(Drawable rightIcon) {
        this.rightIcon = rightIcon;

        if (!Validator.isValid(rightIcon)) {
            imageViewRight.setVisibility(GONE);
        } else {
            imageViewRight.setVisibility(VISIBLE);
            imageViewRight.setImageDrawable(rightIcon);
        }
    }

    public void setTitleSize(float titleSize) {
        this.titleSize = titleSize;
        textViewInfo.setHintSize(titleSize);
    }

    public void setTitleColor(int titleColor) {
        this.titleColor = titleColor;
        textViewInfo.setHintColor(titleColor);
    }

    public void setSubTitleSize(float subTitleSize) {
        this.subTitleSize = subTitleSize;
        textViewInfo.setValueSize(subTitleSize);
    }

    public void setSubTitleColor(int subTitleColor) {
        this.subTitleColor = subTitleColor;
        textViewInfo.setValueColor(subTitleColor);
    }

    public void setFontStyle(FontType fontType, FontMode fontMode) {
        this.fontType = fontType;
        this.fontMode = fontMode;

        textViewInfo.setFontStyle(fontType, fontMode);
    }

    public void setText(String title, String subTitle) {
        this.title = title;
        this.subTitle = subTitle;

        textViewInfo.setText(title, subTitle);
    }

    public void setSpannableText(Spannable title, Spannable subTitle) {
        textViewInfo.setSpannableText(title, subTitle);
    }

    public void setItemInfoClickListener(ItemInfoClickListener itemInfoClickListener) {
        this.itemInfoClickListener = itemInfoClickListener;
    }

    private void init() {
        getAttributeSet();

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.view_item_info, this, true);

        linearLayoutContainer = view.findViewById(R.id.itemInfo_linearLayout_container);
        linearLayoutContainer.setBackgroundDrawable(Validator.isValid(itemBackground) ? itemBackground : null);
        linearLayoutContainer.setOnClickListener(v -> {
            if (Validator.isValid(itemInfoClickListener))
                itemInfoClickListener.onItemInfoClicked(itemId);
        });

        imageViewLeft = view.findViewById(R.id.itemInfo_imageView_left);
        setLeftIcon(leftIcon);

        imageViewRight = view.findViewById(R.id.itemInfo_imageView_right);
        setRightIcon(rightIcon);

        textViewInfo = view.findViewById(R.id.itemInfo_textViewInfo);
        textViewInfo.setHintSize(titleSize);
        textViewInfo.setHintColor(titleColor);
        textViewInfo.setValueSize(subTitleSize);
        textViewInfo.setValueColor(subTitleColor);
        textViewInfo.setFontStyle(fontType, fontMode);
        textViewInfo.setText(title, subTitle);
    }

    private void getAttributeSet() {
        TypedArray typedArray = null;

        try {
            typedArray = context.obtainStyledAttributes(attributeSet, R.styleable.ItemInfo);

            itemId = typedArray.getInt(R.styleable.ItemInfo_itemId, 0);
            itemBackground = typedArray.getDrawable(R.styleable.ItemInfo_itemBackground);
            leftIcon = typedArray.getDrawable(R.styleable.ItemInfo_leftIcon);
            rightIcon = typedArray.getDrawable(R.styleable.ItemInfo_rightIcon);
            titleSize = typedArray.getFloat(R.styleable.ItemInfo_titleSize, 18f);
            titleColor = typedArray.getColor(R.styleable.ItemInfo_titleColor, context.getResources().getColor(R.color.lightTextHint));
            title = typedArray.getString(R.styleable.ItemInfo_title);
            subTitleSize = typedArray.getFloat(R.styleable.ItemInfo_subTitleSize, 12f);
            subTitleColor = typedArray.getColor(R.styleable.ItemInfo_subTitleColor, context.getResources().getColor(R.color.lightTextSecondary));
            subTitle = typedArray.getString(R.styleable.ItemInfo_subTitle);
            fontType = FontType.values()[typedArray.getInt(R.styleable.ItemInfo_fontType, FontType.ROBOTO_MEDIUM.ordinal())];
            fontMode = FontMode.values()[typedArray.getInt(R.styleable.ItemInfo_fontMode, FontMode.NORMAL.ordinal())];
        } finally {
            if (Validator.isValid(typedArray))
                typedArray.recycle();
        }
    }
}
