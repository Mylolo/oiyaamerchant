package com.utils.view.layout.iteminfo;

public interface ItemInfoClickListener {

    void onItemInfoClicked(int itemId);
}
