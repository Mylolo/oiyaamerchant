package com.utils.view.shimmer;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ComposeShader;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Shader;
import android.os.Build;
import android.util.AttributeSet;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;

import com.utils.R;
import com.utils.validator.Validator;

public class ShimmerLayout extends FrameLayout {

    private static final int DEFAULT_ANIMATION_DURATION = 1500;
    private static final byte DEFAULT_ANGLE = 20;
    private static final byte MIN_ANGLE_VALUE = -45;
    private static final byte MAX_ANGLE_VALUE = 45;
    private static final byte MIN_MASK_WIDTH_VALUE = 0;
    private static final byte MAX_MASK_WIDTH_VALUE = 1;
    private static final byte MIN_GRADIENT_CENTER_COLOR_WIDTH_VALUE = 0;
    private static final byte MAX_GRADIENT_CENTER_COLOR_WIDTH_VALUE = 1;

    private Context context;
    private AttributeSet attributeSet;
    private int maskOffsetX;
    private Rect rectMask;
    private ValueAnimator valueAnimatorMask;
    private Bitmap bitmapLocalMask;
    private Bitmap bitmapMask;
    private Paint paintGradientTexture;
    private Canvas canvasForShimmerMask;

    private boolean isAnimationReversed;
    private boolean isAnimationStarted;
    private boolean autoStart;
    private int animationDuration;
    private int color;
    private int angle;
    private float maskWidth;
    private float gradientCenterColorWidth;

    private ViewTreeObserver.OnPreDrawListener startAnimationPreDrawListener;

    public ShimmerLayout(Context context) {
        super(context);

        this.context = context;
        init();
    }

    public ShimmerLayout(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.context = context;
        this.attributeSet = attrs;
        init();
    }

    public ShimmerLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        this.context = context;
        this.attributeSet = attrs;
        init();
    }

    private void init() {
        getAttributeSet();

        setWillNotDraw(false);
        setMaskWidth(maskWidth);
        setGradientCenterColorWidth(gradientCenterColorWidth);
        setAngle(angle);

        if (autoStart && getVisibility() == VISIBLE)
            startShimmerAnimation();
    }

    @Override
    protected void onDetachedFromWindow() {
        resetShimmering();
        super.onDetachedFromWindow();
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        if (!isAnimationStarted || getWidth() <= 0 || getHeight() <= 0)
            super.dispatchDraw(canvas);
        else
            dispatchDrawShimmer(canvas);
    }

    @Override
    public void setVisibility(int visibility) {
        super.setVisibility(visibility);

        if (visibility == VISIBLE) {
            if (autoStart)
                startShimmerAnimation();
        } else {
            stopShimmerAnimation();
        }
    }

    private void getAttributeSet() {
        TypedArray typedArray = null;

        try {
            typedArray = context.obtainStyledAttributes(attributeSet, R.styleable.ShimmerLayout);

            angle = typedArray.getInteger(R.styleable.ShimmerLayout_angle, DEFAULT_ANGLE);
            animationDuration = typedArray.getInteger(R.styleable.ShimmerLayout_animationDuration, DEFAULT_ANIMATION_DURATION);
            color = typedArray.getColor(R.styleable.ShimmerLayout_shimmerColor, getColor(R.color.lightShimmer));
            autoStart = typedArray.getBoolean(R.styleable.ShimmerLayout_autoStart, false);
            maskWidth = typedArray.getFloat(R.styleable.ShimmerLayout_maskWidth, 0.5F);
            gradientCenterColorWidth = typedArray.getFloat(R.styleable.ShimmerLayout_gradientCenterColorWidth, 0.1F);
            isAnimationReversed = typedArray.getBoolean(R.styleable.ShimmerLayout_reverseAnimation, false);
        } finally {
            if (Validator.isValid(typedArray))
                typedArray.recycle();
        }
    }

    public void startShimmerAnimation() {
        if (isAnimationStarted)
            return;

        if (getWidth() == 0) {
            startAnimationPreDrawListener = new ViewTreeObserver.OnPreDrawListener() {
                @Override
                public boolean onPreDraw() {
                    getViewTreeObserver().removeOnPreDrawListener(this);
                    startShimmerAnimation();

                    return true;
                }
            };

            getViewTreeObserver().addOnPreDrawListener(startAnimationPreDrawListener);

            return;
        }

        Animator animator = getShimmerAnimation();
        animator.start();

        isAnimationStarted = true;
    }

    public void stopShimmerAnimation() {
        if (Validator.isValid(startAnimationPreDrawListener))
            getViewTreeObserver().removeOnPreDrawListener(startAnimationPreDrawListener);

        resetShimmering();
    }

    public void setColor(int color) {
        this.color = color;
        resetIfStarted();
    }

    public void setAnimationDuration(int durationMillis) {
        this.animationDuration = durationMillis;
        resetIfStarted();
    }

    public void setAnimationReversed(boolean animationReversed) {
        this.isAnimationReversed = animationReversed;
        resetIfStarted();
    }

    public void setAngle(int angle) {
        if (angle < MIN_ANGLE_VALUE || MAX_ANGLE_VALUE < angle)
            throw new IllegalArgumentException(String.format("Angle value must be between %d and %d", MIN_ANGLE_VALUE, MAX_ANGLE_VALUE));

        this.angle = angle;
        resetIfStarted();
    }

    public void setMaskWidth(float maskWidth) {
        if (maskWidth <= MIN_MASK_WIDTH_VALUE || MAX_MASK_WIDTH_VALUE < maskWidth)
            throw new IllegalArgumentException(String.format("Mask width value must be higher than %d and less or equal to %d", MIN_MASK_WIDTH_VALUE, MAX_MASK_WIDTH_VALUE));

        this.maskWidth = maskWidth;
        resetIfStarted();
    }

    public void setGradientCenterColorWidth(float gradientCenterColorWidth) {
        if (gradientCenterColorWidth <= MIN_GRADIENT_CENTER_COLOR_WIDTH_VALUE || MAX_GRADIENT_CENTER_COLOR_WIDTH_VALUE <= gradientCenterColorWidth)
            throw new IllegalArgumentException(String.format("Gradient center color width value must be higher than %d and less than %d", MIN_GRADIENT_CENTER_COLOR_WIDTH_VALUE, MAX_GRADIENT_CENTER_COLOR_WIDTH_VALUE));

        this.gradientCenterColorWidth = gradientCenterColorWidth;
        resetIfStarted();
    }

    private void resetIfStarted() {
        if (isAnimationStarted) {
            resetShimmering();
            startShimmerAnimation();
        }
    }

    private void dispatchDrawShimmer(Canvas canvas) {
        super.dispatchDraw(canvas);

        bitmapLocalMask = getBitmapMask();
        if (!Validator.isValid(bitmapLocalMask))
            return;

        if (!Validator.isValid(canvasForShimmerMask))
            canvasForShimmerMask = new Canvas(bitmapLocalMask);

        canvasForShimmerMask.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
        canvasForShimmerMask.save();
        canvasForShimmerMask.translate(-maskOffsetX, 0);
        super.dispatchDraw(canvasForShimmerMask);
        canvasForShimmerMask.restore();
        drawShimmer(canvas);
        bitmapLocalMask = null;
    }

    private void drawShimmer(Canvas destinationCanvas) {
        createShimmerPaint();
        destinationCanvas.save();
        destinationCanvas.translate(maskOffsetX, 0);
        destinationCanvas.drawRect(rectMask.left, 0, rectMask.width(), rectMask.height(), paintGradientTexture);
        destinationCanvas.restore();
    }

    private void resetShimmering() {
        if (Validator.isValid(valueAnimatorMask)) {
            valueAnimatorMask.end();
            valueAnimatorMask.removeAllUpdateListeners();
        }

        valueAnimatorMask = null;
        paintGradientTexture = null;
        isAnimationStarted = false;
        releaseBitMaps();
    }

    private void releaseBitMaps() {
        canvasForShimmerMask = null;

        if (Validator.isValid(bitmapMask)) {
            bitmapMask.recycle();
            bitmapMask = null;
        }
    }

    private Bitmap getBitmapMask() {
        if (!Validator.isValid(bitmapMask))
            bitmapMask = createBitmap(rectMask.width(), getHeight());

        return bitmapMask;
    }

    private void createShimmerPaint() {
        if (Validator.isValid(paintGradientTexture))
            return;

        final int edgeColor = reduceColorAlphaValueToZero(color);
        final float shimmerLineWidth = getWidth() / 2 * maskWidth;
        final float yPosition = (0 <= angle) ? getHeight() : 0;

        LinearGradient gradient = new LinearGradient(0, yPosition, (float) Math.cos(Math.toRadians(angle)) * shimmerLineWidth, yPosition + (float) Math.sin(Math.toRadians(angle)) * shimmerLineWidth, new int[]{edgeColor, color, color, edgeColor}, getGradientColorDistribution(), Shader.TileMode.CLAMP);
        BitmapShader maskBitmapShader = new BitmapShader(bitmapLocalMask, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
        ComposeShader composeShader = new ComposeShader(gradient, maskBitmapShader, PorterDuff.Mode.DST_IN);

        paintGradientTexture = new Paint();
        paintGradientTexture.setAntiAlias(true);
        paintGradientTexture.setDither(true);
        paintGradientTexture.setFilterBitmap(true);
        paintGradientTexture.setShader(composeShader);
    }

    private Animator getShimmerAnimation() {
        if (Validator.isValid(valueAnimatorMask))
            return valueAnimatorMask;

        if (!Validator.isValid(rectMask))
            rectMask = calculateBitmapMaskRect();

        final int animationToX = getWidth();
        final int animationFromX;

        if (getWidth() > rectMask.width())
            animationFromX = -animationToX;
        else
            animationFromX = -rectMask.width();

        final int shimmerBitmapWidth = rectMask.width();
        final int shimmerAnimationFullLength = animationToX - animationFromX;

        valueAnimatorMask = isAnimationReversed ? ValueAnimator.ofInt(shimmerAnimationFullLength, 0) : ValueAnimator.ofInt(0, shimmerAnimationFullLength);
        valueAnimatorMask.setDuration(animationDuration);
        valueAnimatorMask.setRepeatCount(ObjectAnimator.INFINITE);
        valueAnimatorMask.addUpdateListener(animation -> {
            maskOffsetX = animationFromX + (int) animation.getAnimatedValue();
            if (maskOffsetX + shimmerBitmapWidth >= 0)
                invalidate();
        });

        return valueAnimatorMask;
    }

    private Bitmap createBitmap(int width, int height) {
        try {
            return Bitmap.createBitmap(width, height, Bitmap.Config.ALPHA_8);
        } catch (OutOfMemoryError e) {
            System.gc();
            return null;
        }
    }

    private int getColor(int id) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            return getContext().getColor(id);
        else
            return getResources().getColor(id);
    }

    private int reduceColorAlphaValueToZero(int actualColor) {
        return Color.argb(0, Color.red(actualColor), Color.green(actualColor), Color.blue(actualColor));
    }

    private Rect calculateBitmapMaskRect() {
        return new Rect(0, 0, calculateMaskWidth(), getHeight());
    }

    private int calculateMaskWidth() {
        final double shimmerLineBottomWidth = (getWidth() / 2 * maskWidth) / Math.cos(Math.toRadians(Math.abs(angle)));
        final double shimmerLineRemainingTopWidth = getHeight() * Math.tan(Math.toRadians(Math.abs(angle)));
        return (int) (shimmerLineBottomWidth + shimmerLineRemainingTopWidth);
    }

    private float[] getGradientColorDistribution() {
        final float[] colorDistribution = new float[4];

        colorDistribution[0] = 0;
        colorDistribution[3] = 1;

        colorDistribution[1] = 0.5F - gradientCenterColorWidth / 2F;
        colorDistribution[2] = 0.5F + gradientCenterColorWidth / 2F;

        return colorDistribution;
    }
}
