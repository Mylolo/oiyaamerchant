package com.utils.view.textview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import androidx.appcompat.widget.AppCompatTextView;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewTreeObserver;

import com.utils.R;
import com.utils.util.Util;
import com.utils.validator.Validator;
import com.utils.view.FontMode;
import com.utils.view.FontType;

public class TextView extends AppCompatTextView {

    private static final String READ_MORE = "...Read More";
    private static final String READ_LESS = "...Read Less";
    private static final int DEFAULT_MAX_LINES_AT_SHRINK_STATE = 5;

    private Context context;
    private AttributeSet attributeSet;
    private FontType fontType;
    private FontMode fontMode;
    private int maxLineAtShrinkState = DEFAULT_MAX_LINES_AT_SHRINK_STATE;

    public TextView(Context context) {
        super(context);

        this.context = context;
        init();
    }

    public TextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.context = context;
        this.attributeSet = attrs;
        init();
    }

    public TextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        this.context = context;
        this.attributeSet = attrs;
        init();
    }

    public void makeResizable(int maxLineAtShrinkState) {
        this.maxLineAtShrinkState = maxLineAtShrinkState;
        makeResizable(this, maxLineAtShrinkState, true);
    }

    public void setFontStyle(FontType fontType, FontMode fontMode) {
        this.fontType = fontType;
        this.fontMode = fontMode;
        applyFont();
    }

    private void init() {
        getAttributeSet();
        applyFont();
    }

    private void getAttributeSet() {
        TypedArray typedArray = null;

        try {
            typedArray = context.obtainStyledAttributes(attributeSet, R.styleable.TextView);

            fontType = FontType.values()[typedArray.getInt(R.styleable.TextView_fontType, FontType.ROBOTO_REGULAR.ordinal())];
            fontMode = FontMode.values()[typedArray.getInt(R.styleable.TextView_fontMode, FontMode.NORMAL.ordinal())];
        } finally {
            if (Validator.isValid(typedArray))
                typedArray.recycle();
        }
    }

    private void applyFont() {
        Typeface typeface;

        switch (fontType) {
            case ROBOTO_BLACK:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Black.ttf");
                break;

            case ROBOTO_BLACK_ITALIC:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-BlackItalic.ttf");
                break;

            case ROBOTO_BOLD:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Bold.ttf");
                break;

            case ROBOTO_BOLD_ITALIC:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-BoldItalic.ttf");
                break;

            case ROBOTO_CONDENSED_BOLD:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/RobotoCondensed-Bold.ttf");
                break;

            case ROBOTO_CONDENSED_BOLD_ITALIC:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/RobotoCondensed-BoldItalic.ttf");
                break;

            case ROBOTO_CONDENSED_ITALIC:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/RobotoCondensed-Italic.ttf");
                break;

            case ROBOTO_CONDENSED_LIGHT:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/RobotoCondensed-Light.ttf");
                break;

            case ROBOTO_CONDENSED_LIGHT_ITALIC:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/RobotoCondensed-LightItalic.ttf");
                break;

            case ROBOTO_CONDENSED_REGULAR:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/RobotoCondensed-Regular.ttf");
                break;

            case ROBOTO_ITALIC:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Italic.ttf");
                break;

            case ROBOTO_LIGHT:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Light.ttf");
                break;

            case ROBOTO_LIGHT_ITALIC:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-LightItalic.ttf");
                break;

            case ROBOTO_MEDIUM:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Medium.ttf");
                break;

            case ROBOTO_MEDIUM_ITALIC:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-MediumItalic.ttf");
                break;

            case ROBOTO_REGULAR:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Regular.ttf");
                break;

            case ROBOTO_THIN:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Thin.ttf");
                break;

            case ROBOTO_THIN_ITALIC:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-ThinItalic.ttf");
                break;

            case CALIBRI:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Calibri.ttf");
                break;

            case CALIBRI_BOLD:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Calibri-Bold.ttf");
                break;

            case CALIBRI_ITALIC:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Calibri-Itailc.ttf");
                break;

            case CALIBRI_BOLD_ITALIC:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Calibri-BoldItalic.ttf");
                break;

            default:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Black.ttf");
                break;
        }

        switch (fontMode) {
            case NORMAL:
                setTypeface(typeface, Typeface.NORMAL);
                break;

            case BOLD:
                setTypeface(typeface, Typeface.BOLD);
                break;

            case ITALIC:
                setTypeface(typeface, Typeface.ITALIC);
                break;

            case BOLD_ITALIC:
                setTypeface(typeface, Typeface.BOLD_ITALIC);
                break;

            default:
                setTypeface(typeface, Typeface.NORMAL);
                break;
        }
    }

    private void makeResizable(final TextView textView, final int maxLine, final boolean readMore) {
        if (!Validator.isValid(textView.getTag()))
            textView.setTag(textView.getText());

        ViewTreeObserver viewTreeObserver = textView.getViewTreeObserver();
        viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @SuppressLint("NewApi")
            @Override
            public void onGlobalLayout() {
                ViewTreeObserver treeObserver = textView.getViewTreeObserver();
                if (Util.isJellyBean())
                    treeObserver.removeGlobalOnLayoutListener(this);
                else
                    treeObserver.removeOnGlobalLayoutListener(this);

                if (textView.getLineCount() < maxLine) {
                    String text = textView.getText().toString();

                    textView.setText(text);
                }
                // Compressed data
                else if (maxLine > 0 && textView.getLineCount() >= maxLine) {
                    int lineEndIndex = textView.getLayout().getLineEnd(maxLine - 1);
                    String text = textView.getText().subSequence(0, lineEndIndex - READ_MORE.length() + 1) + " " + READ_MORE;

                    textView.setText(text);
                    textView.setMovementMethod(LinkMovementMethod.getInstance());
                    textView.setText(addClickablePartResizable(textView.getText().toString(), textView, READ_MORE, readMore), BufferType.SPANNABLE);
                }
                // Expanded data
                else {
                    int lineEndIndex = textView.getLayout().getLineEnd(textView.getLayout().getLineCount() - 1);
                    String text = textView.getText().subSequence(0, lineEndIndex) + " " + READ_LESS;

                    textView.setText(text);
                    textView.setMovementMethod(LinkMovementMethod.getInstance());
                    textView.setText(addClickablePartResizable(textView.getText().toString(), textView, READ_LESS, readMore), BufferType.SPANNABLE);
                }
            }
        });
    }

    private SpannableStringBuilder addClickablePartResizable(final String spannedText, final TextView textView, final String spannableText, final boolean readMore) {
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(spannedText);

        if (spannedText.contains(spannableText)) {
            spannableStringBuilder.setSpan(new ClickableSpan() {
                @Override
                public void onClick(View widget) {
                    textView.setLayoutParams(textView.getLayoutParams());
                    textView.setText(textView.getTag().toString(), BufferType.SPANNABLE);
                    textView.invalidate();

                    if (readMore)
                        makeResizable(textView, -1, false); // Expanded data
                    else
                        makeResizable(textView, maxLineAtShrinkState, true); // Compressed data
                }
            }, spannedText.indexOf(spannableText), spannedText.indexOf(spannableText) + spannableText.length(), 0);
        }

        return spannableStringBuilder;
    }
}
