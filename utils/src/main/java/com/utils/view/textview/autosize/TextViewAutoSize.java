package com.utils.view.textview.autosize;

import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;

import com.utils.validator.Validator;
import com.utils.view.textview.TextView;

public class TextViewAutoSize extends TextView implements TextViewAutoSizeHelper.OnTextSizeChangeListener {

    private Context context;
    private AttributeSet attributeSet;
    private TextViewAutoSizeHelper textViewAutoSizeHelper;

    public TextViewAutoSize(Context context) {
        super(context);

        this.context = context;
        init();
    }

    public TextViewAutoSize(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.context = context;
        this.attributeSet = attrs;
        init();
    }

    public TextViewAutoSize(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        this.context = context;
        this.attributeSet = attrs;
        init();
    }

    private void init() {
        textViewAutoSizeHelper = TextViewAutoSizeHelper.create(this, attributeSet).addOnTextSizeChangeListener(this);
    }

    @Override
    public void setTextSize(int unit, float size) {
        super.setTextSize(unit, size);

        if (Validator.isValid(textViewAutoSizeHelper))
            textViewAutoSizeHelper.setTextSize(unit, size);
    }

    @Override
    public void setLines(int lines) {
        super.setLines(lines);

        if (Validator.isValid(textViewAutoSizeHelper))
            textViewAutoSizeHelper.setMaxLines(lines);
    }

    @Override
    public void setMaxLines(int maxLines) {
        super.setMaxLines(maxLines);

        if (Validator.isValid(textViewAutoSizeHelper))
            textViewAutoSizeHelper.setMaxLines(maxLines);
    }

    public TextViewAutoSizeHelper getTextViewAutoSizeHelper() {
        return textViewAutoSizeHelper;
    }

    public boolean isSizeToFit() {
        return textViewAutoSizeHelper.isEnabled();
    }

    public void setSizeToFit(boolean sizeToFit) {
        textViewAutoSizeHelper.setEnabled(sizeToFit);
    }

    public void setSizeToFit() {
        setSizeToFit(true);
    }

    public float getMaxTextSize() {
        return textViewAutoSizeHelper.getMaxTextSize();
    }

    public void setMaxTextSize(float size) {
        textViewAutoSizeHelper.setMaxTextSize(size);
    }

    public void setMaxTextSize(int unit, float size) {
        textViewAutoSizeHelper.setMaxTextSize(unit, size);
    }

    public float getMinTextSize() {
        return textViewAutoSizeHelper.getMinTextSize();
    }

    public void setMinTextSize(int minSize) {
        textViewAutoSizeHelper.setMinTextSize(TypedValue.COMPLEX_UNIT_SP, minSize);
    }

    public void setMinTextSize(int unit, float minSize) {
        textViewAutoSizeHelper.setMinTextSize(unit, minSize);
    }

    public float getPrecision() {
        return textViewAutoSizeHelper.getPrecision();
    }

    public void setPrecision(float precision) {
        textViewAutoSizeHelper.setPrecision(precision);
    }

    @Override
    public void onTextSizeChange(float textSize, float oldTextSize) {

    }
}
