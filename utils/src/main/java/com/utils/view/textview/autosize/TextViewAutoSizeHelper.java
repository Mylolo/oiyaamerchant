package com.utils.view.textview.autosize;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Build;
import android.text.Editable;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.TextWatcher;
import android.text.method.SingleLineTransformationMethod;
import android.text.method.TransformationMethod;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;

import com.utils.R;
import com.utils.validator.Validator;

import java.util.ArrayList;

class TextViewAutoSizeHelper {

    private static final int DEFAULT_TEXT_MIN_SIZE = 8; //sp
    private static final float DEFAULT_PRECISION = 0.5f;
    private TextView textView;
    private TextPaint textPaint;
    private float textSize;
    private int maxLines;
    private float minTextSize;
    private float maxTextSize;
    private float precision;
    private boolean enabled;
    private boolean isAutoFit;
    private ArrayList<OnTextSizeChangeListener> onTextSizeChangeListeners;
    private TextWatcher textWatcher = new AutoTextSizeTextWatcher();
    private View.OnLayoutChangeListener mOnLayoutChangeListener = new AutoTextSizeOnLayoutChangeListener();

    private TextViewAutoSizeHelper(TextView view) {
        final Context context = view.getContext();
        float scaledDensity = context.getResources().getDisplayMetrics().scaledDensity;

        textView = view;
        textPaint = new TextPaint();
        setRawTextSize(view.getTextSize());
        maxLines = getMaxLines(view);
        minTextSize = scaledDensity * DEFAULT_TEXT_MIN_SIZE;
        maxTextSize = textSize;
        precision = DEFAULT_PRECISION;
    }

    public static TextViewAutoSizeHelper create(TextView view, AttributeSet attributeSet) {
        TextViewAutoSizeHelper textViewAutoSizeHelper = new TextViewAutoSizeHelper(view);
        boolean textSizeToFit = true;

        if (Validator.isValid(attributeSet)) {
            Context context = view.getContext();
            int textMinSize = (int) textViewAutoSizeHelper.getMinTextSize();
            float textPrecision = textViewAutoSizeHelper.getPrecision();

            TypedArray typedArray = null;
            try {
                typedArray = context.obtainStyledAttributes(attributeSet, R.styleable.TextViewAutoSize);

                textMinSize = typedArray.getDimensionPixelSize(R.styleable.TextViewAutoSize_minSize, textMinSize);
                textSizeToFit = typedArray.getBoolean(R.styleable.TextViewAutoSize_sizeToFit, textSizeToFit);
                textPrecision = typedArray.getFloat(R.styleable.TextViewAutoSize_precision, textPrecision);
            } finally {
                if (Validator.isValid(typedArray))
                    typedArray.recycle();
            }

            textViewAutoSizeHelper.setMinTextSize(TypedValue.COMPLEX_UNIT_PX, textMinSize).setPrecision(textPrecision);
        }

        textViewAutoSizeHelper.setEnabled(textSizeToFit);
        return textViewAutoSizeHelper;
    }

    private static void autoFit(TextView view, TextPaint paint, float minTextSize, float maxTextSize, int maxLines, float precision) {
        if (maxLines <= 0 || maxLines == Integer.MAX_VALUE)
            return;

        int targetWidth = view.getWidth() - view.getPaddingLeft() - view.getPaddingRight();
        if (targetWidth <= 0)
            return;

        CharSequence text = view.getText();
        TransformationMethod method = view.getTransformationMethod();
        if (method != null)
            text = method.getTransformation(text, view);

        Context context = view.getContext();
        Resources resources = Resources.getSystem();
        DisplayMetrics displayMetrics;

        float size = maxTextSize;
        float high = size;
        float low = 0;

        if (context != null)
            resources = context.getResources();

        displayMetrics = resources.getDisplayMetrics();

        paint.set(view.getPaint());
        paint.setTextSize(size);

        if ((maxLines == 1 && paint.measureText(text, 0, text.length()) > targetWidth) || getLineCount(text, paint, size, targetWidth, displayMetrics) > maxLines)
            size = getTextSize(text, paint, targetWidth, maxLines, low, high, precision, displayMetrics);

        if (size < minTextSize)
            size = minTextSize;

        view.setTextSize(TypedValue.COMPLEX_UNIT_PX, size);
    }

    private static float getTextSize(CharSequence text, TextPaint paint, float targetWidth, int maxLines, float low, float high, float precision, DisplayMetrics displayMetrics) {
        float mid = (low + high) / 2.0f;
        int lineCount = 1;
        StaticLayout layout = null;

        paint.setTextSize(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX, mid, displayMetrics));

        if (maxLines != 1) {
            layout = new StaticLayout(text, paint, (int) targetWidth, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, true);
            lineCount = layout.getLineCount();
        }

        if (lineCount > maxLines) {
            if ((high - low) < precision)
                return low;

            return getTextSize(text, paint, targetWidth, maxLines, low, mid, precision, displayMetrics);
        } else if (lineCount < maxLines) {
            return getTextSize(text, paint, targetWidth, maxLines, mid, high, precision, displayMetrics);
        } else {
            float maxLineWidth = 0;
            if (maxLines == 1) {
                maxLineWidth = paint.measureText(text, 0, text.length());
            } else {
                for (int i = 0; i < lineCount; i++) {
                    if (layout.getLineWidth(i) > maxLineWidth)
                        maxLineWidth = layout.getLineWidth(i);
                }
            }

            if ((high - low) < precision)
                return low;
            else if (maxLineWidth > targetWidth)
                return getTextSize(text, paint, targetWidth, maxLines, low, mid, precision, displayMetrics);
            else if (maxLineWidth < targetWidth)
                return getTextSize(text, paint, targetWidth, maxLines, mid, high, precision, displayMetrics);
            else
                return mid;
        }
    }

    private static int getLineCount(CharSequence text, TextPaint paint, float size, float width, DisplayMetrics displayMetrics) {
        paint.setTextSize(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX, size, displayMetrics));
        StaticLayout layout = new StaticLayout(text, paint, (int) width, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, true);
        return layout.getLineCount();
    }

    private static int getMaxLines(TextView view) {
        int maxLines = -1; // No limit (Integer.MAX_VALUE also means no limit)

        TransformationMethod method = view.getTransformationMethod();
        if (method != null && method instanceof SingleLineTransformationMethod) {
            maxLines = 1;
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            // setMaxLines() and getMaxLines() are only available on android-16+
            maxLines = view.getMaxLines();
        }

        return maxLines;
    }

    public TextViewAutoSizeHelper addOnTextSizeChangeListener(OnTextSizeChangeListener listener) {
        if (!Validator.isValid(onTextSizeChangeListeners))
            onTextSizeChangeListeners = new ArrayList<>();

        onTextSizeChangeListeners.add(listener);

        return this;
    }

    public TextViewAutoSizeHelper removeOnTextSizeChangeListener(OnTextSizeChangeListener listener) {
        if (Validator.isValid(onTextSizeChangeListeners))
            onTextSizeChangeListeners.remove(listener);

        return this;
    }

    public float getPrecision() {
        return precision;
    }

    public TextViewAutoSizeHelper setPrecision(float precision) {
        if (this.precision != precision) {
            this.precision = precision;
            autoFit();
        }

        return this;
    }

    public float getMinTextSize() {
        return minTextSize;
    }

    public TextViewAutoSizeHelper setMinTextSize(float size) {
        return setMinTextSize(TypedValue.COMPLEX_UNIT_SP, size);
    }

    public TextViewAutoSizeHelper setMinTextSize(int unit, float size) {
        Context context = textView.getContext();
        Resources resources = Resources.getSystem();

        if (Validator.isValid(context))
            resources = context.getResources();

        setRawMinTextSize(TypedValue.applyDimension(unit, size, resources.getDisplayMetrics()));
        return this;
    }

    private void setRawMinTextSize(float size) {
        if (size != minTextSize) {
            minTextSize = size;
            autoFit();
        }
    }

    public float getMaxTextSize() {
        return maxTextSize;
    }

    public TextViewAutoSizeHelper setMaxTextSize(float size) {
        return setMaxTextSize(TypedValue.COMPLEX_UNIT_SP, size);
    }

    public TextViewAutoSizeHelper setMaxTextSize(int unit, float size) {
        Context context = textView.getContext();
        Resources resources = Resources.getSystem();

        if (Validator.isValid(context))
            resources = context.getResources();

        setRawMaxTextSize(TypedValue.applyDimension(unit, size, resources.getDisplayMetrics()));
        return this;
    }

    private void setRawMaxTextSize(float size) {
        if (size != maxTextSize) {
            maxTextSize = size;
            autoFit();
        }
    }

    public int getMaxLines() {
        return maxLines;
    }

    public TextViewAutoSizeHelper setMaxLines(int lines) {
        if (maxLines != lines) {
            maxLines = lines;
            autoFit();
        }

        return this;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public TextViewAutoSizeHelper setEnabled(boolean enabled) {
        if (this.enabled != enabled) {
            this.enabled = enabled;

            if (enabled) {
                textView.addTextChangedListener(textWatcher);
                textView.addOnLayoutChangeListener(mOnLayoutChangeListener);
                autoFit();
            } else {
                textView.removeTextChangedListener(textWatcher);
                textView.removeOnLayoutChangeListener(mOnLayoutChangeListener);
                textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            }
        }

        return this;
    }

    public float getTextSize() {
        return textSize;
    }

    public void setTextSize(float size) {
        setTextSize(TypedValue.COMPLEX_UNIT_SP, size);
    }

    public void setTextSize(int unit, float size) {
        if (isAutoFit)
            return;

        Context context = textView.getContext();
        Resources resources = Resources.getSystem();

        if (Validator.isValid(context))
            resources = context.getResources();

        setRawTextSize(TypedValue.applyDimension(unit, size, resources.getDisplayMetrics()));
    }

    private void setRawTextSize(float size) {
        if (textSize != size)
            textSize = size;
    }

    private void autoFit() {
        float oldTextSize = textView.getTextSize();
        float textSize;

        isAutoFit = true;
        autoFit(textView, textPaint, minTextSize, maxTextSize, maxLines, precision);
        isAutoFit = false;

        textSize = textView.getTextSize();

        if (textSize != oldTextSize)
            sendTextSizeChange(textSize, oldTextSize);
    }

    private void sendTextSizeChange(float textSize, float oldTextSize) {
        if (!Validator.isValid(onTextSizeChangeListeners))
            return;

        for (OnTextSizeChangeListener listener : onTextSizeChangeListeners)
            listener.onTextSizeChange(textSize, oldTextSize);
    }

    public interface OnTextSizeChangeListener {
        void onTextSizeChange(float textSize, float oldTextSize);
    }

    private class AutoTextSizeTextWatcher implements TextWatcher {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
            autoFit();
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    }

    private class AutoTextSizeOnLayoutChangeListener implements View.OnLayoutChangeListener {
        @Override
        public void onLayoutChange(View view, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
            autoFit();
        }
    }
}
