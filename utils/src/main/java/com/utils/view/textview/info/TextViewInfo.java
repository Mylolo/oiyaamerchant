package com.utils.view.textview.info;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.Spannable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.utils.R;
import com.utils.validator.Validator;
import com.utils.view.FontMode;
import com.utils.view.FontType;
import com.utils.view.textview.TextView;

public class TextViewInfo extends LinearLayout {

    private Context context;
    private AttributeSet attributeSet;
    private float hintSize;
    private int hintColor;
    private String hint;
    private float valueSize;
    private int valueColor;
    private String value;
    private FontType fontType;
    private FontMode fontMode;

    private TextView textViewHint;
    private TextView textViewValue;

    public TextViewInfo(Context context) {
        super(context);

        this.context = context;
        init();
    }

    public TextViewInfo(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.context = context;
        this.attributeSet = attrs;
        init();
    }

    public TextViewInfo(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        this.context = context;
        this.attributeSet = attrs;
        init();
    }

    public void setHintSize(float hintSize) {
        this.hintSize = hintSize;
        textViewHint.setTextSize(hintSize);
    }

    public void setHintColor(int hintColor) {
        this.hintColor = hintColor;
        textViewHint.setTextColor(hintColor);
    }

    public void setValueSize(float valueSize) {
        this.valueSize = valueSize;
        textViewValue.setTextSize(valueSize);
    }

    public void setValueColor(int valueColor) {
        this.valueColor = valueColor;
        textViewValue.setTextColor(valueColor);
    }

    public void setFontStyle(FontType fontType, FontMode fontMode) {
        this.fontType = fontType;
        this.fontMode = fontMode;

        textViewHint.setFontStyle(fontType, fontMode);
        textViewValue.setFontStyle(fontType, fontMode);
    }

    public void setText(String hint, String value) {
        this.hint = hint;
        this.value = value;

        textViewHint.setText(hint);
        textViewHint.setVisibility(Validator.isValid(hint) ? VISIBLE : GONE);

        textViewValue.setText(value);
        textViewValue.setVisibility(Validator.isValid(value) ? VISIBLE : GONE);
    }

    public void setSpannableText(Spannable hint, Spannable value) {
        if (Validator.isValid(hint))
            textViewHint.setText(hint, android.widget.TextView.BufferType.SPANNABLE);
        textViewHint.setVisibility(Validator.isValid(hint) ? VISIBLE : GONE);

        if (Validator.isValid(value))
            textViewValue.setText(value, android.widget.TextView.BufferType.SPANNABLE);
        textViewValue.setVisibility(Validator.isValid(value) ? VISIBLE : GONE);
    }

    private void init() {
        getAttributeSet();

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.view_textview_info, this, true);

        textViewHint = view.findViewById(R.id.textViewInfo_textView_hint);
        textViewHint.setTextSize(hintSize);
        textViewHint.setTextColor(hintColor);
        textViewHint.setText(hint);
        textViewHint.setFontStyle(fontType, fontMode);
        textViewHint.setVisibility(Validator.isValid(hint) ? VISIBLE : GONE);

        textViewValue = view.findViewById(R.id.textViewInfo_textView_value);
        textViewValue.setTextSize(valueSize);
        textViewValue.setTextColor(valueColor);
        textViewValue.setText(value);
        textViewValue.setFontStyle(fontType, fontMode);
        textViewValue.setVisibility(Validator.isValid(value) ? VISIBLE : GONE);
    }

    private void getAttributeSet() {
        TypedArray typedArray = null;

        try {
            typedArray = context.obtainStyledAttributes(attributeSet, R.styleable.TextViewInfo);

            hintSize = typedArray.getFloat(R.styleable.TextViewInfo_hintSize, 12f);
            hintColor = typedArray.getColor(R.styleable.TextViewInfo_hintColor, context.getResources().getColor(R.color.lightTextHint));
            hint = typedArray.getString(R.styleable.TextViewInfo_hint);
            valueSize = typedArray.getFloat(R.styleable.TextViewInfo_valueSize, 12f);
            valueColor = typedArray.getColor(R.styleable.TextViewInfo_valueColor, context.getResources().getColor(R.color.lightTextSecondary));
            value = typedArray.getString(R.styleable.TextViewInfo_value);
            fontType = FontType.values()[typedArray.getInt(R.styleable.TextViewInfo_fontType, FontType.ROBOTO_REGULAR.ordinal())];
            fontMode = FontMode.values()[typedArray.getInt(R.styleable.TextViewInfo_fontMode, FontMode.NORMAL.ordinal())];
        } finally {
            if (Validator.isValid(typedArray))
                typedArray.recycle();
        }
    }
}
